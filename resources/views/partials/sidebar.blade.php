<div id="sidebar" class="col-xs-12 col-sm-2">
    <div class="list-group mt-3">
    <a class="list-group-item {{ Request::is('settings-amazonmws')?'active':'' }}" href="{{asset('settings-amazonmws')}}">Amazon</a>
    <a class="list-group-item {{ Request::is('settings-pull')?'active':'' }}" href="{{asset('settings-pull')}}">Pull</a>
    <a class="list-group-item {{ Request::is('settings-default')?'active':'' }}" href="{{asset('settings-default')}}">Push</a>
    <a class="list-group-item {{ Request::is('settings-sync')?'active':'' }}" href="{{asset('settings-sync')}}">Sync</a>
    <a class="list-group-item {{ Request::is('settings-button')?'active':'' }}" href="{{asset('settings-button')}}">Button</a>
    <a class="list-group-item {{ Request::is('settings-reviews')?'active':'' }}" href="{{asset('settings-reviews')}}">Reviews <sup class="text-dark font-weight-bold">new</sup></a>
    <!--<a class="list-group-item text-muted {{ Request::is('settings-fba')?'active':'' }}" href="{{asset('settings-fba')}}">FBA</a>-->

      </div>
    </div>
