@if($bannerlink != '' || $linktab=='2')
    @php
        $styleval="cursor:pointer;";
    @endphp

@else
    @php
        $styleval="";
    @endphp
@endif

@if($imgformat == 'svg')
    @php
        $imgformstyle="height:".$height;
    @endphp

@else
    @php
        $imgformstyle="";
    @endphp
@endif

@if( $type == 'text' )
<div class="primebanDesktop prime-d-block @{{primebanInnerClass}} {!! $primebanDesktopclass !!} {!! $banneridclass !!} {!! $CountyClass !!} " {!! $primebantooltip !!} {!! $SelectedCountry !!} onclick="redirectLinkban('{{$bannerlink}}','{{$linktab}}','{{$primebannerid}}','{{$modalsize}}');" style="{{ $styleval }} @{{ primebanInnerStyle }}{{ $bannerstyle }} ">

    <div class="primebanImageOuter prime-d-table-cell prime-align-middle {!! $primebanImageOuterclass !!}">
        <img class="primebanImage prime-d-block prime-mx-auto @{{primebanImageClass}}" alt="{{$alttext}}" style="{{ $imgStyle }} @{{primebanImageStyle}} {{ $imgformstyle }}" src="@{{Imagesize}}">
    </div>

	<div class="primebanTexts prime-d-table-cell prime-align-middle @{{primebanTextClass}} " style=" @{{primebanTextsStyle}}">	
      <div class="primebanTitle prime-mb-1 @{{primebanTitleClass}} {!! $downtitlesty !!}" data-primeproductid='@{{product.id}}' style="{{ $bannertitlestyle }} @{{primebanTitleStyle}} {{ $nodisplay }} ">{!! $title !!}</div>
      <div class="primebanSubTitle @{{primebanSubTitleClass}} {!! $downsubtitlesty !!}" data-primeproductid='@{{product.id}}' style="{{ $bannersubtitlestyle }} @{{primebanSubTitleStyle}} {{ $nodisplaysub }}">{!! $subtitle !!}</div>
    </div>
</div>
@else
<div class="primebanDesktop prime-d-block @{{primebanInnerClass}} {!! $primebanDesktopclass !!} {!! $banneridclass !!} {!! $CountyClass !!}" {!! $primebantooltip !!} {!! $SelectedCountry !!} onclick="redirectLinkban('{{$bannerlink}}','{{$linktab}}','{{$primebannerid}}','{{$modalsize}}');" style="{{ $styleval }} @{{ primebanInnerStyle }}{{ $bannerstyle }} ">
    <div class="primebanPhotoOuter @{{primebanPhotoOuterClass}} {!! $primebanImageOuterclass !!}">
        <img class="primebanPhoto @{{primebanPhotoClass}}" alt="{{$alttext}}" style="{{ $imgStyle }} @{{primebanImageStyle}} {{ $imgformstyle }}" src="@{{Imagesize}}">
    </div>
</div>
@endif
