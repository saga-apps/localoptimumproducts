@if( empty( $userBanners ) ) 
	<script type="text/javascript">
	</script>
    @php 
		$counttime = '';
        $setcountforvisit = '';
        $weekdays = "";
        $dateformat = "";
	@endphp
@else
	@php 
        $counttime = $livecounttime;
        $setcountforvisit = $setlivevisitcount;
        $weekdays = $weekdays;
        $dateformat = $dateformat;
	@endphp

@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script type="text/javascript">

    var existingScript = document.getElementById('primebanPopper');
  	if (!existingScript) {
		var countryprimeban = 'primebannercountry';
		const scriptpopperb = document.createElement('script');
		// scriptpopperb.src = 'https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js';
		scriptpopperb.src = 'https://unpkg.com/popper.js@1';
		scriptpopperb.id = 'primebanPopper';
		document.body.appendChild(scriptpopperb);
		setTimeout(function(){ 
			const scripttippyb = document.createElement('script');
			// scripttippyb.src = 'https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js';
			scripttippyb.src = 'https://unpkg.com/tippy.js@5';
			scripttippyb.id = 'primebanTippy';
			document.body.appendChild(scripttippyb);
		}, 500);

  	}

    document.addEventListener('DOMContentLoaded', (event) => {

        var mypage = '@{{template}}';
        var myproductid = '@{{product.id}}';
        // for (var i = 0; i < 6; i++) {
            setTimeout(function(){ 
                primebansetlivevisit(mypage,myproductid,'primebantitleloop');
                primebansetlivevisit(mypage,myproductid,'primebansubtitleloop');
            }, 500); 
        // }

        var primebanTitle = document.getElementsByClassName('primebanTitle');
        if(primebanTitle[0]){
            for (var i = 0; i < primebanTitle.length; i++) {
                var value = primebanTitle[i].innerHTML;
                if( value.indexOf("CountdownTimer:") != -1  ){
                    myTimer3(value,i);
                }
            }
            
        } 

        var primebanSubTitle = document.getElementsByClassName('primebanSubTitle');
        if(primebanSubTitle[0]){
            for (var i = 0; i < primebanSubTitle.length; i++) {
                var value2 = primebanSubTitle[i].innerHTML;
                if( value2.indexOf("CountdownTimer:") != -1  ){
                    myTimersub3(value2,i);
                }
            }
            
        } 

        var primebanText = document.getElementsByClassName('primebanTitle');
        if(primebanText[0]){
            for (var i = 0; i < primebanText.length; i++) {
                var value = primebanText[i].innerHTML;
                if( value.indexOf("[[today") != -1  ){
                    expdeliverydateban(value,i,"primebanTitle");
                }
            }
            
        }

        var primebanSubText = document.getElementsByClassName('primebanSubTitle');
        if(primebanSubText[0]){
            for (var i = 0; i < primebanSubText.length; i++) {
                var value = primebanSubText[i].innerHTML;
                if( value.indexOf("[[today") != -1  ){
                    expdeliverydateban(value,i,"primebanSubTitle");
                }
            }
            
        }

        var primebansku = '@{{ProductSKU}}';
        var skuclass = document.getElementsByClassName('primebansku');
        if(primebansku == 0){
            for (var i = 0; i < skuclass.length; i++) {
                if(skuclass[i].dataset.sku == 0){
                    var parentsku = skuclass[i].parentElement;
                    var parent2sku = parentsku.parentElement;
                    var parent3sku = parent2sku.parentElement;
                    parent3sku.style.cssText = 'display:none !important';
                }
            }
        }


        // const primeinstances2 = tippy('[data-tippy-content]');
        // instances2.destroy();
        // resizeFunction();
        setTimeout(function(){ 
            var primebaninstances2 = tippy('[data-tippy-content]');
        }, 1000);

        if(countryprimeban == 'primebannercountry'){

            countryprimeban = 'newprimebannercountry';
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            
                if (this.readyState == 4 && this.status == 200) {
                    var mycountryprimeban = this.responseText;
                    var countryclassprimeban = document.getElementsByClassName('primebanCountry');
            
                    for (var i = 0; i < countryclassprimeban.length; i++) {
                        var counrtylistb = countryclassprimeban[i].getAttribute('data-countryselectedb');
                        var position = counrtylistb.search(mycountryprimeban);
                        if(position < 0){ 
                            countryclassprimeban[i].style.cssText = 'display:none !important';
                        }
                    }
                }
            };
            xhttp.open("GET", "https://geo.thalia.workers.dev/", true);
            xhttp.send();
            
        }

        document.getElementById("prime-modal-close").onclick = function() {mymodalprimeban()};
        window.onclick = function(event) {
            if (event.target.classList[1] == 'prime-modal-open') {
                mymodalprimeban();
            }
        }

        
    })

    function expdeliverydateban(value,count,className) {
        var weekdays = '<?php echo $weekdays; ?>';
        var dateformatselected = '<?php echo $dateformat; ?>';
        var primebanTextdate = document.getElementsByClassName(className);
        var realvalue = value;
        var pos = realvalue.indexOf("[[today");
        var startres = realvalue.slice(pos+8, pos+9);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        var pos2 = realvalue.indexOf("[[today");
        var endres = realvalue.slice(pos2+8, pos2+9);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        var wrongdays = weekdays.split(",");
        // var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
        var startDate = new Date(new Date().getTime()+(parseInt(startres)*24*60*60*1000));
        var today = new Date(new Date().getTime());
        var firstinc=0;
        while (today <= startDate) {
            var getday = String(today.getDay());
            if(wrongdays.indexOf(getday) != -1){
            startDate.setDate(startDate.getDate() + 1);
            firstinc++;
            }
            today.setDate(today.getDate() + 1);
        }
        value = value.replace("[[today,"+startres+"]]", moment(startDate).format(dateformatselected));

        var extradays = firstinc+parseInt(endres);
        var endDate = new Date(new Date().getTime()+(extradays*24*60*60*1000));
        var todayend = startDate;
        var secinc=0;
        while (todayend <= endDate) {
            var getday = String(todayend.getDay());
            if(wrongdays.indexOf(getday) != -1){
            endDate.setDate(endDate.getDate() + 1);
            secinc++;
            }
            todayend.setDate(todayend.getDate() + 1);
        }
        value = value.replace("[[today,"+endres+"]]", moment(endDate).format(dateformatselected));
        primebanTextdate[count].innerHTML = value;
    }

    function primebansetlivevisit(mypage,myproductid,banclassname) {
        setTimeout(function(){ 

            var settimelive = '<?php echo $counttime; ?>';
            var setvisitlimit = '<?php echo $setcountforvisit; ?>';

            var d = new Date();
            var time = setcookietimestampban(settimelive);
            var mytime = '';
            if(settimelive < 5){
                mytime = (time * 60 );
            }else if(settimelive > 4 && settimelive < 8){
                mytime =(time * 60 * 60 );
            }else if(settimelive > 7){
                mytime = (time * 24 * 60 * 60 );
            }

            var realbanclass = '';
            if(banclassname == 'primebantitleloop'){
                realbanclass = "primevisitsbantitle";
            }else{
                realbanclass = 'primevisitsbansubtitle';
            }

            var primeloop2 = document.getElementsByClassName(banclassname);
            for (var i = 0; i < primeloop2.length; i++) {
                var smallloop2 = primeloop2[i].parentElement;
                var parentloop2 = smallloop2.getAttribute('data-primeproductid');
                var myuser = getCookieprimeban("primevisits"+parentloop2);
                if (myuser != "") {
                    myhttpreqban(parentloop2,0,mytime,setvisitlimit,banclassname,realbanclass);
                } else {
                    myhttpreqban(parentloop2,1,mytime,setvisitlimit,banclassname,realbanclass);
                    setCookieprimeban("primevisits"+parentloop2, 'setprimevisits', settimelive);
                }

            }

            var primeloop = document.getElementsByClassName(banclassname);
            
            for (var i = 0; i < primeloop.length; i++) {
                if(mypage == 'product'){
                    var smallloop = primeloop[i].parentElement;
                    var parentloop = smallloop.getAttribute('data-primeproductid');
                    if(parentloop == myproductid){
                        primeloop[i].classList.add(realbanclass+parentloop);
                        // var removeloop = primeloop[i].parentElement;
                        // var removeloop2 = removeloop.parentElement;
                    }else{
                        var removeloop = primeloop[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        removeloop2.style.visibility = 'hidden';
                        primeloop[i].classList.remove(banclassname);
                    }
                }else{
                  var oldloop = primeloop[i].parentElement;
                  var newloop = oldloop.parentElement;
                  newloop.style.visibility = 'hidden';
                  primeloop[i].classList.remove(banclassname);
                  var newloop2 = newloop.parentElement;
                  newloop2.remove();
                }
            }

        }, 500);
    }

    function myhttpreqban(lastid,mynew,gettime,setvisitlimit,banclassname,realbanclass) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var pageliveview = JSON.parse(this.responseText);
                var primevisits = document.getElementsByClassName(realbanclass+lastid);
                for (var i = 0; i < primevisits.length; i++) {
                    if(setvisitlimit < pageliveview.pageviews){
                        primevisits[i].innerHTML = pageliveview.pageviews;
                        var removeloop = primevisits[i].parentElement;
                        removeloop.style.visibility = 'visible';
                        var removeloop2 = removeloop.parentElement;
                        removeloop2.style.visibility = 'visible';
                    }else{
                        var removeloop = primevisits[i].parentElement;
                        removeloop.style.visibility = 'hidden';
                        var removeloop2 = removeloop.parentElement;
                        removeloop2.style.visibility = 'hidden';
                        primevisits[i].classList.remove(banclassname);
                        var newloop2 = removeloop2.parentElement;
                        newloop2.remove();
                        // removeloop2.remove();
                    }
                        
                }
            }
        };
        xhttp.open("GET","https://visitcounterstaging.thalia-apps.com/?shopifyid="+lastid+"&new="+mynew+"&time="+gettime, true);
        xhttp.send();
    }

    function mymodalprimeban() {
      var target = document.getElementById("prime-modal-window");
      var parenttarget = target.parentElement;
      if (parenttarget.classList.contains('prime-modal-open')) {
        parenttarget.classList.remove('prime-modal-open');
        document.getElementById('prime-modal-content').innerHTML = '';
      }
    }

    function redirectLinkban(bannerval,links,primebannerid,modalsize) {

        if(bannerval != ''){

        // console.log('clicke');
            var bannerlink = bannerval;
            if(links == 0){
                window.location.href = bannerlink;
            }else{
                window.open(bannerlink,'_blank');
            }
        }

        if(links == 2){
            var target = document.getElementById("prime-modal-outer");
            var myclasses = '';
            if(modalsize == 0){
                myclasses = 'prime-modal-window prime-modal-small';
            }else if(modalsize == 2){
                myclasses = 'prime-modal-window prime-modal-large';
            }else{
                myclasses = 'prime-modal-window';
            }
            
            document.getElementById('prime-modal-outer').classList.add('prime-modal-open');
            var setclass = document.getElementById('prime-modal-window');
            setclass.className = '';
            setclass.className = myclasses;
            var xhr = new XMLHttpRequest();
            var url = '/?view=primemeta&id==banner-popup-'+primebannerid;
            xhr.open("GET", url, true);
            xhr.setRequestHeader("Cache-Control", "max-age=3600");
            xhr.onreadystatechange = function() {
                if (this.readyState === XMLHttpRequest.DONE) {
                    if (this.status === 200) {
                        document.getElementById('prime-modal-content').innerHTML = this.responseText;
                    } else {
                        console.log(this.status, this.statusText);
                    }
                }
                };
                xhr.send();
        }
    
    }

    function toValidDate2(datestring){
        return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
    }

    function myTimer3(newvalue,count) {

        var primebanTitle2 = document.getElementsByClassName('primebanTitle');
        var pos = newvalue.indexOf("CountdownTimer:");
        var res = newvalue.slice(pos+15, pos+34);
        newvalue = newvalue.replace("[[ CountdownTimer:", "" );
        newvalue = newvalue.replace("]]", "" );

        var x = setInterval(function() { 

            let end = new Date(toValidDate2(res));
            let start = new Date();
            var remaintime = end - start;
            var days = Math.floor(remaintime / (1000 * 60 * 60 * 24));
            var hours = Math.floor((remaintime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((remaintime % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((remaintime % (1000 * 60)) / 1000);
            var realtimeleft = days + "d "+hours + "h "+minutes + "m "+seconds + "s";
            if(seconds < 0){
                realtimeleft = "0d 0h 0m 0s";
            }
            primebanTitle2[count].innerHTML = newvalue.replace(res,realtimeleft);
            primebanTitle2[count].classList.remove("prime-counter");
            if (remaintime < 0) { 
                clearInterval(x); 
            } 
        }, 1000); 

    }

    function myTimersub3(newvalue,count) {

        var primebanSubTitle2 = document.getElementsByClassName('primebanSubTitle');
        var pos = newvalue.indexOf("CountdownTimer:");
        var res = newvalue.slice(pos+15, pos+34);
        newvalue = newvalue.replace("[[ CountdownTimer:", "" );
        newvalue = newvalue.replace("]]", "" );

        var x = setInterval(function() { 

            let end = new Date(toValidDate2(res));
            let start = new Date();
            var remaintime = end - start;
            var days = Math.floor(remaintime / (1000 * 60 * 60 * 24));
            var hours = Math.floor((remaintime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((remaintime % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((remaintime % (1000 * 60)) / 1000);
            var realtimeleft = days + "d "+hours + "h "+minutes + "m "+seconds + "s";
            if(seconds < 0){
                realtimeleft = "0d 0h 0m 0s";
            }
            primebanSubTitle2[count].innerHTML = newvalue.replace(res,realtimeleft);
            primebanSubTitle2[count].classList.remove("prime-counter");
            if (remaintime < 0) { 
                clearInterval(x); 
            } 
        }, 1000); 

    }

    function setCookieprimeban(cname, cvalue, exdays) {
      var d = new Date();
      var time = setcookietimestampban(exdays);
      if(exdays < 5){
        d.setTime(d.getTime() + (time * 60 * 1000));
      }else if(exdays > 4 && exdays < 8){
        d.setTime(d.getTime() + (time * 60 * 60 * 1000));
      }else if(exdays > 7){
        d.setTime(d.getTime() + (time * 24 * 60 * 60 * 1000));
      }
      
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookieprimeban(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    function setcookietimestampban(time) {
        var realtime='';
        if(time==1){
            realtime=1;
        }else if(time==2){
            realtime=5;
        }else if(time==3){
            realtime=10;
        }else if(time==4){
            realtime=30;
        }else if(time==5){
            realtime=1;
        }else if(time==6){
            realtime=6;
        }else if(time==7){
            realtime=12;
        }else if(time==8){
            realtime=1;
        }else if(time==9){
            realtime=7;
        }else if(time==10){
            realtime=15;
        }else if(time==11){
            realtime=30;
        }
        return realtime;
    }

</script> 