@if($highlightlink != '' || $linktab=='2')
    @php
        $styleval="cursor:pointer;";
    @endphp

@else
    @php
        $styleval="";
    @endphp
@endif
 
<div  class="@{{primehDesktop}} prime-px-0 {{$primehDesktop}} @{{primehInnerClass}} {!! $highlightidclass !!} {!! $CountyClass !!}" {!! $primehtooltip !!} {!! $SelectedCountry !!} style=" {{ $styleval }} @{{primehInnerStyle}} {{ $nodisplay }}" onclick="redirectHighlightLink('{{$highlightlink}}','{{$linktab}}','{{$primehighlightid}}','{{$modalsize}}');">
    <div class="primehImageOuter prime-px-2 prime-py-1  {{$OuterTextPos}} prime-align-middle">  
        <img class="primehImage prime-d-block prime-mx-auto @{{primehImageClass}}" alt="{{$alttext}}" src="{{$image}}" style="@{{primehImageStyle}} {{$imgStyle}}">
    </div>
    <div class="primehText {{$TextPos}} prime-align-middle @{{primehTextClass}} {!! $downtitlesty !!}" data-primeproductid='@{{product.id}}' style="{{ $textStyle }} @{{primehTextStyle}}">
    {!!$title !!}</div>
</div>