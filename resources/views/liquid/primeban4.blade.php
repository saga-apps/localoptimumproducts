<?php 
	use App\Helpers\LiquidBannerHelper2;
?>
@if( empty( $userBanners ) )
	<script type="text/javascript">
		//console.log( 'user no banners ');
	</script>
@else
@php 
	
	$isDeclare = false;
	$bannercount = $bannercount;
	
@endphp


@foreach( $userBanners as $index => $banner )	
	@php 
	$CountryClass = '';
	$LangClass = '';
	$display       = json_decode( $banner['display'] ,true );
  	$displayrules  = json_decode( $banner['displayrules'] ,true );
  	$appearance    = json_decode( $banner['appearance'] ,true );
	$bannerstyle    = json_decode($banner['bannerstyle'],true);

	$countdowntitle = 0;
	$countdownsubtitle = 0;

	if(strpos($banner['title'],'{{ ProductSKU }}')  !== false){
		$bannersku=str_replace("{{ ProductSKU }}",'<span class="primebansku" data-sku="{{ProductSKU}}" >{{ ProductSKU }}</span>',$banner['title']);
		$banner['title']=$bannersku;
	}else if(strpos($banner['title'],'{{ProductSKU}}')  !== false){
		$bannersku=str_replace("{{ProductSKU}}",'<span class="primebansku" data-sku="{{ProductSKU}}" >{{ProductSKU}}</span>',$banner['title']);
		$banner['title']=$bannersku;
	}else{}

	if(strpos($banner['subtitle'],'{{ ProductSKU }}')  !== false){
		$bannersku=str_replace("{{ ProductSKU }}",'<span class="primebansku" data-sku="{{ProductSKU}}" >{{ ProductSKU }}</span>',$banner['subtitle']);
		$banner['subtitle']=$bannersku;
	}else if(strpos($banner['subtitle'],'{{ProductSKU}}')  !== false){
		$bannersku=str_replace("{{ProductSKU}}",'<span class="primebansku" data-sku="{{ProductSKU}}" >{{ProductSKU}}</span>',$banner['subtitle']);
		$banner['subtitle']=$bannersku;
	}else{}
	
	if($banner['planid'] > 2){

		$displayprorules  = json_decode( $banner['displayprorules'] ,true );
		if( isset($banner['customer_location']) && $banner['customer_location'] != ''){
			$customer_location = $banner['customer_location'];
			$CountryClass = 'primebanCountry'; 
		}else{
			$customer_location = '';
		}

		if( isset($banner['shop_language']) && $banner['shop_language'] != ''){
			$shop_language = $banner['shop_language'];
			$LangClass = 'primebanlang'; 
		}else{
			$shop_language = '';
		}

		if( isset($banner['excludeproduct']) && $banner['excludeproduct'] != ''){
			$excludeproduct = $banner['excludeproduct'];
		}else{
			$excludeproduct = '';
		}

		if(strpos($banner['title'],'[[ CountdownTimer:')  !== false){
			$countdowntitle = 1;
		}else{}
		
		if(strpos($banner['subtitle'],'[[ CountdownTimer:')  !== false){
			$countdownsubtitle = 1;
		}else{}
		
		if(strpos($banner['title'],'[[CountdownTimer:')  !== false){
			$countdowntitle = 1;
		}else{}
		
		if(strpos($banner['subtitle'],'[[CountdownTimer:')  !== false){
			$countdownsubtitle = 1;
		}else{}
		
	}else{
		$displayprorules  = '';
		$excludeproduct = '';
		$customer_location = '';
		$shop_language = '';
		
		if(strpos($banner['title'], 'product.metafields') !== false){
			continue;
		}else if(strpos($banner['title'],'product.metafields')  !== false){
			continue;
		}else{}

		if(strpos($banner['title'],'{{ OrderCount }}')  !== false){
			continue;
		}else if(strpos($banner['title'],'{{OrderCount}}')  !== false){
			continue;
		}else{}

		if(strpos($banner['title'],'{{ RecentSoldCount }}')  !== false){
			continue;
		}else if(strpos($banner['title'],'{{RecentSoldCount}}')  !== false){
			continue;
		}else{}

		if(strpos($banner['title'],'[[ CountdownTimer:')  !== false){
			continue;
		}else if(strpos($banner['title'],'[[CountdownTimer:')  !== false){
			continue;
		}else{}

		if($banner['bannergroup'] == '1'){

		}else{
			continue;
		}
	}

	if($banner['planid'] > 3){
		if(strpos($banner['title'],'{{ VisitorCounter }}')  !== false){
			$bannerlive=str_replace("{{ VisitorCounter }}",'<span class="primebantitleloop" >{{ VisitorCounter }}</span>',$banner['title']);
			$banner['title']=$bannerlive;
		}else if(strpos($banner['title'],'{{VisitorCounter}}')  !== false){
			$bannerlive=str_replace("{{VisitorCounter}}",'<span class="primebantitleloop" >{{VisitorCounter}}</span>',$banner['title']);
			$banner['title']=$bannerlive;
		}else{}

		if(strpos($banner['subtitle'],'{{ VisitorCounter }}')  !== false){
			$bannerlivesub=str_replace("{{ VisitorCounter }}",'<span class="primebansubtitleloop" >{{ VisitorCounter }}</span>',$banner['subtitle']);
			$banner['subtitle']=$bannerlivesub;
		}else if(strpos($banner['subtitle'],'{{VisitorCounter}}')  !== false){
			$bannerlivesub=str_replace("{{VisitorCounter}}",'<span class="primebansubtitleloop" >{{VisitorCounter}}</span>',$banner['subtitle']);
			$banner['subtitle']=$bannerlivesub;
		}else{}

	}else{

		if(strpos($banner['title'],'{{ VisitorCounter }}')  !== false){
			continue;
		}else if(strpos($banner['title'],'{{VisitorCounter}}')  !== false){
			continue;
		}else{}

		if(strpos($banner['subtitle'],'{{ VisitorCounter }}')  !== false){
			continue;
		}else if(strpos($banner['subtitle'],'{{VisitorCounter}}')  !== false){
			continue;
		}else{}
	}

  	$condition     = $banner['condition'] == 1 ? ' and ':' or ';

    //print_r($banner);exit;
    
  	$liquidCondition = LiquidBannerHelper2::getLiquidProductCondition( $displayrules , $condition , $appearance ,  $banner,  $display, $excludeproduct, $bannerstyle, $CountryClass, $customer_location, $displayprorules, $countdowntitle, $countdownsubtitle, $shop_language, $LangClass ); 
	
  	if( $isDeclare == false ) {  		
  		$isDeclare = true;
  		echo $liquidCondition['global_variable']; 
	}  	
  	@endphp	
  	{% comment %} Start Banner: {{$index+1}} {% endcomment %}
  	@php 
  		echo $liquidCondition['condition']; 
  	@endphp
	{% comment %} End Banner: {{$index+1}} {% endcomment %}	
@endforeach
	@php 
		if( $bannercount > 200 ) {   		
	@endphp	
		{% assign hideAssets2 = hideAssets | default: '0' %}
		{% assign primebanOuterClass2 = primebanOuterClass | default: 'prime-mt-4 prime-mb-4' %}
		{% assign primebanOuterStyle2 = primebanOuterStyle | default: '' %}
		{% assign primebanInnerClass2 = primebanInnerClass | default: '' %}
		{% assign primebanInnerStyle2 = primebanInnerStyle | default: '' %}
		{% assign primebanImageClass2 = primebanImageClass | default: '' %}
		{% assign primebanImageStyle2 = primebanImageStyle | default: '' %}
		{% assign primebanTextClass2 = primebanTextClass | default: '' %}
		{% assign primebanTextsStyle2 = primebanTextsStyle | default: '' %}
		{% assign primebanTitleClass2 = primebanTitleClass | default: '' %}
		{% assign primebanTitleStyle2 = primebanTitleStyle | default: '' %}
		{% assign primebanSubTitleClass2 = primebanSubTitleClass | default: '' %}
		{% assign primebanSubTitleStyle2 = primebanSubTitleStyle | default: '' %}

		{% render 'primeban5', product: product, hideAssets: hideAssets2, primebanOuterClass: primebanOuterClass2, primebanOuterStyle: primebanOuterStyle2, primebanInnerClass: primebanInnerClass2, primebanInnerStyle: primebanInnerStyle2, primebanImageClass: primebanImageClass2, primebanImageStyle: primebanImageStyle2, primebanTextClass: primebanTextClass2, primebanTextsStyle: primebanTextsStyle2,primebanTitleClass: primebanTitleClass2,primebanTitleStyle: primebanTitleStyle2,primebanSubTitleClass: primebanSubTitleClass2,primebanSubTitleStyle: primebanSubTitleStyle2 %}

	@php } @endphp
	
@endif