<div class="primenotes prime-d-block @{{primenOuterClass}}" id="notestyle" style="{{ $noteStyle }} @{{primenOuterStyle}}" >
    <div class="primenWrapper">		
        @{{ product.metafields.primen.notecontent }}
    </div>
</div>
