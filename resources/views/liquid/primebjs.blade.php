@if( empty( $userBadges ) ) 
	<script type="text/javascript">
	</script>
    @php 
		$counttime = "";
        $setcountforvisit = "";
        $weekdays = "";
        $dateformat = "";
	@endphp
@else
	@php 
        $counttime = $livecounttime;
        $setcountforvisit = $setlivevisitcount;
        $weekdays = $weekdays;
        $dateformat = $dateformat;
	@endphp

@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script type="text/javascript">

    var existingScript = document.getElementById('primebPopper');
  	if (!existingScript) {
		var country = 'primebadgecountry';
		const scriptpopperb = document.createElement('script');
		scriptpopperb.src = 'https://unpkg.com/popper.js@1';
		scriptpopperb.id = 'primebPopper';
		document.body.appendChild(scriptpopperb);

		setTimeout(function(){ 
			const scripttippyb = document.createElement('script');
			scripttippyb.src = 'https://unpkg.com/tippy.js@5';
			scripttippyb.id = 'primebTippy';
			document.body.appendChild(scripttippyb);

            
		}, 500);
  	}
      
    document.addEventListener('DOMContentLoaded', (event) => {
        
        primebCountdownTimer();
        resizeFunction();
        var primebText = document.getElementsByClassName('primebText');
        if(primebText[0]){
            for (var i = 0; i < primebText.length; i++) {
                var value = primebText[i].innerHTML;
                if( value.indexOf("[[today") != -1  ){
                    expdeliverydate(value,i);
                }
            }
            
        }

        document.getElementById("prime-modal-close").onclick = function() {mymodalprimeb()};
        window.onclick = function(event) {
            if (event.target.classList[1] == 'prime-modal-open') {
                mymodalprimeb();
            }
        }
        
        
    })

    function expdeliverydate(value,count) {
        var weekdays = '<?php echo $weekdays; ?>';
        var dateformatselected = '<?php echo $dateformat; ?>';
        var primebTextdate = document.getElementsByClassName('primebText');
        var realvalue = value;
        var pos = realvalue.indexOf("[[today");
        var startres = realvalue.slice(pos+8, pos+9);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        var pos2 = realvalue.indexOf("[[today");
        var endres = realvalue.slice(pos2+8, pos2+9);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        var wrongdays = weekdays.split(",");
        // var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
        var startDate = new Date(new Date().getTime()+(parseInt(startres)*24*60*60*1000));
        var today = new Date(new Date().getTime());
        var firstinc=0;
        while (today <= startDate) {
            var getday = String(today.getDay());
            if(wrongdays.indexOf(getday) != -1){
            startDate.setDate(startDate.getDate() + 1);
            firstinc++;
            }
            today.setDate(today.getDate() + 1);
        }
        value = value.replace("[[today,"+startres+"]]", moment(startDate).format(dateformatselected));

        var extradays = firstinc+parseInt(endres);
        var endDate = new Date(new Date().getTime()+(extradays*24*60*60*1000));
        var todayend = startDate;
        var secinc=0;
        while (todayend <= endDate) {
            var getday = String(todayend.getDay());
            if(wrongdays.indexOf(getday) != -1){
            endDate.setDate(endDate.getDate() + 1);
            secinc++;
            }
            todayend.setDate(todayend.getDate() + 1);
        }
        value = value.replace("[[today,"+endres+"]]", moment(endDate).format(dateformatselected));
        primebTextdate[count].innerHTML = value;
    }

    function primebCountdownTimer() {
        setTimeout(function(){ 
            var primebText = document.getElementsByClassName('primebText');
            if(primebText[0]){
                for (var i = 0; i < primebText.length; i++) {
                    var value = primebText[i].innerHTML;
                    if( value.indexOf("CountdownTimer:") != -1  ){
                        myTimerprime2(value,i);
                    }
                }
                
            }
        }, 1000); 

        setTimeout(function(){ 
            var primeinstances2 = tippy('[data-tippy-content]');

            var primebsku = '@{{ProductSKU}}';
            var skuclass = document.getElementsByClassName('primebsku');
            if(primebsku == 0){
                for (var i = 0; i < skuclass.length; i++) {
                    if(skuclass[i].dataset.sku == 0){
                        var parentsku = skuclass[i].parentElement;
                        var parent2sku = parentsku.parentElement;
                        parent2sku.style.cssText = 'display:none !important';
                    }
                }
            }

        }, 1000);

        if(country == 'primebadgecountry'){
            country = 'newprimebadgecountry';
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            
                if (this.readyState == 4 && this.status == 200) {
                    var mycountry = this.responseText;
                    var countryclass = document.getElementsByClassName('primebCountry');
                    for (var i = 0; i < countryclass.length; i++) {
                        var counrtylist = countryclass[i].getAttribute('data-countryselected');
                        var position = counrtylist.search(mycountry);
                        
                        if(position < 0){ 
                            countryclass[i].style.cssText = 'display:none !important';
                            // countryclass[i].remove();
                        }
                    }
                }
            };
            xhttp.open("GET", "https://geo.thalia.workers.dev/", true);
            xhttp.send();
            
        }
        var mypage = '@{{template}}';
        var myproductid = '@{{product.id}}';
        for (var i = 0; i < 6; i++) {
            setTimeout(function(){ 
                primesetlivevisit(mypage,myproductid);
            }, 500); 
        }

    }

    function primesetlivevisit(mypage,myproductid) {
        setTimeout(function(){ 

            var settimelive = '<?php echo $counttime; ?>';
            var setvisitlimit = '<?php echo $setcountforvisit; ?>';

            var d = new Date();
            var time = setcookietimestamp(settimelive);
            var mytime = '';
            if(settimelive < 5){
                mytime = (time * 60 );
            }else if(settimelive > 4 && settimelive < 8){
                mytime =(time * 60 * 60 );
            }else if(settimelive > 7){
                mytime = (time * 24 * 60 * 60 );
            }

            var primeloop2 = document.getElementsByClassName('primebloop');
            for (var i = 0; i < primeloop2.length; i++) {
                var smallloop2 = primeloop2[i].parentElement;
                var parentloop2 = smallloop2.getAttribute('data-primeproductid');
                var myuser = getCookieprime("primevisits"+parentloop2);
                if (myuser != "") {
                    myhttpreq(parentloop2,0,mytime,setvisitlimit);
                } else {
                    myhttpreq(parentloop2,1,mytime,setvisitlimit);
                    setCookieprime("primevisits"+parentloop2, 'setprimevisits', settimelive);
                }

            }

            var primeloop = document.getElementsByClassName('primebloop');
            for (var i = 0; i < primeloop.length; i++) {
                if(mypage == 'product'){
                    var smallloop = primeloop[i].parentElement;
                    var parentloop = smallloop.getAttribute('data-primeproductid');
                    if(parentloop == myproductid){
                        primeloop[i].classList.add("primevisitsb"+parentloop);
                        // var removeloop = primeloop[i].parentElement;
                        // var removeloop2 = removeloop.parentElement;
                        // var removeloop3 = removeloop2.parentElement;
                        // removeloop3.style.visibility = 'visible';
                    }else{
                        var removeloop = primeloop[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        var removeloop3 = removeloop2.parentElement;
                        removeloop3.style.visibility = 'hidden';
                        primeloop[i].classList.remove("primebloop");
                    }
                }else{
                  var oldloop = primeloop[i].parentElement;
                  var newloop = oldloop.parentElement;
                  var newloop2 = newloop.parentElement;
                  newloop2.style.visibility = 'hidden';
                  primeloop[i].classList.remove("primebloop");
                  newloop2.remove();
                }
            }

            // var d = new Date();
            // var time = setcookietimestamp(settimelive);
            // var mytime = '';
            // if(settimelive < 5){
            //     mytime = (time * 60 );
            // }else if(settimelive > 4 && settimelive < 8){
            //     mytime =(time * 60 * 60 );
            // }else if(settimelive > 7){
            //     mytime = (time * 24 * 60 * 60 );
            // }

            // var primeloop2 = document.getElementsByClassName('primebloop');
            // for (var i = 0; i < primeloop2.length; i++) {
            //     var smallloop2 = primeloop2[i].parentElement;
            //     var parentloop2 = smallloop2.getAttribute('data-primeproductid');
            //     var myuser = getCookieprime("primevisits"+parentloop2);
            //     if (myuser != "") {
            //         myhttpreq(parentloop2,0,mytime,setvisitlimit);
            //     } else {
            //         myhttpreq(parentloop2,1,mytime,setvisitlimit);
            //         setCookieprime("primevisits"+parentloop2, 'setprimevisits', settimelive);
            //     }

            // }

        }, 1000);
    }

    function myhttpreq(lastid,mynew,gettime,setvisitlimit) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var pageliveview = JSON.parse(this.responseText);
                var primevisits = document.getElementsByClassName("primevisitsb"+lastid);
                for (var i = 0; i < primevisits.length; i++) {
                    if(setvisitlimit < pageliveview.pageviews){
                        primevisits[i].innerHTML = pageliveview.pageviews;
                        var removeloop = primevisits[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        var removeloop3 = removeloop2.parentElement;
                        removeloop3.style.visibility = 'visible';
                    }else{
                        var removeloop = primevisits[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        var removeloop3 = removeloop2.parentElement;
                        removeloop3.style.visibility = 'hidden';
                        primevisits[i].classList.remove("primebloop");
                    }
                        
                }
            }
        };
        xhttp.open("GET","https://visitcounterstaging.thalia-apps.com/?shopifyid="+lastid+"&new="+mynew+"&time="+gettime, true);
        xhttp.send();
    }

    function mymodalprimeb() {
      var target = document.getElementById("prime-modal-window");
      var parenttarget = target.parentElement;
      if (parenttarget.classList.contains('prime-modal-open')) {
        parenttarget.classList.remove('prime-modal-open');
        document.getElementById('prime-modal-content').innerHTML = '';
      }
    }


    window.addEventListener('resize', resizeFunction);

    function resizeFunction() { 
        var fontadjust = document.getElementsByClassName('prime-font-adjust');
        var heightadjust = document.getElementsByClassName('prime-height-adjust');

        var xsdevice = window.matchMedia( "(max-width: 575.98px)" );
        var smdevice = window.matchMedia( "(max-width: 767.98px)" );
        var mddevice = window.matchMedia( "(max-width: 991.98px)" );
        var lgdevice = window.matchMedia( "(max-width: 1199.98px)" );
        var xldevice = window.matchMedia( "(min-width: 1200px)" );

        for (var i = 0; i < fontadjust.length; i++) {
            var mobilesize = '';
            var tabletsize = '';
            mobilesize = fontadjust[i].getAttribute('data-mobilesize');
            tabletsize = fontadjust[i].getAttribute('data-tabletsize');

            fontadjust[i].style.fontSize = 'inherit';

            if (xsdevice.matches || smdevice.matches ){
                if(mobilesize > 0){
                    fontadjust[i].style.fontSize = mobilesize+'px';

                }
            }else if(mddevice.matches){
                if(tabletsize > 0){
                    fontadjust[i].style.fontSize = tabletsize+'px';

                }
            }
        }
        var heightadjust = document.getElementsByClassName('prime-height-adjust');
        for (var i = 0; i < heightadjust.length; i++) {
            var image = heightadjust[i].getElementsByClassName("primebImage");
            var mobileheight = '';
            var tabletheight = '';

            var defaultHeight = heightadjust[i].getAttribute('data-defaultheight');
            mobileheight = heightadjust[i].getAttribute('data-mobileheight');
            tabletheight = heightadjust[i].getAttribute('data-tabletheight');

            // image[0].style.height = defaultHeight+'px';
            var imgsrcsvg = image[0].getAttribute("src");
            var present = imgsrcsvg.search("svg");
            if(present > 0){
                image[0].style.height = defaultHeight+'px';
                image[0].style.maxHeight = defaultHeight+'px';
                if (xsdevice.matches || smdevice.matches ){
                    if(mobileheight > 0){
                        image[0].style.height = mobileheight+'px';
                        image[0].style.maxHeight = mobileheight+'px';
                    }
                }else if(mddevice.matches){
                    if(tabletheight > 0){
                        image[0].style.height = tabletheight+'px';
                        image[0].style.maxHeight = tabletheight+'px';
                    }
                }

            }else{
                image[0].style.maxHeight = defaultHeight+'px';
                if (xsdevice.matches || smdevice.matches ){
                    if(mobileheight > 0){
                        // image[0].style.height = mobileheight+'px';
                        image[0].style.maxHeight = mobileheight+'px';
                    }
                }else if(mddevice.matches){
                    if(tabletheight > 0){
                        // image[0].style.height = tabletheight+'px';
                        image[0].style.maxHeight = tabletheight+'px';
                        
                    }
                }
            }
            
        }

    } 

    function redirectLinkbadge(badgeval,links,primebadgeid,modalsize) {

        if(badgeval != ''){

            var badgelink = badgeval;
            if(links == 0){
                window.location.href = badgelink;
            }else{
                window.open(badgelink,'_blank');
            }

        }
        if(links == 2){
            var target = document.getElementById("prime-modal-outer");
            var myclasses = '';
            if(modalsize == 0){
                myclasses = 'prime-modal-window prime-modal-small';
            }else if(modalsize == 2){
                myclasses = 'prime-modal-window prime-modal-large';
            }else{
                myclasses = 'prime-modal-window';
            }
            
            document.getElementById('prime-modal-outer').classList.add('prime-modal-open');
            var setclass = document.getElementById('prime-modal-window');
            setclass.className = '';
            setclass.className = myclasses;
            var xhr = new XMLHttpRequest();
            var url = '/?view=primemeta&id==badge-popup-'+primebadgeid;
            xhr.open("GET", url, true);
            xhr.setRequestHeader("Cache-Control", "max-age=3600");
            xhr.onreadystatechange = function() {
                if (this.readyState === XMLHttpRequest.DONE) {
                    if (this.status === 200) {
                        document.getElementById('prime-modal-content').innerHTML = this.responseText;
                    } else {
                        console.log(this.status, this.statusText);
                    }
                }
                };
                xhr.send();
        }
    
    }

    function toValidDateprime2(datestring){
        return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
    }

    function myTimerprime2(newvalue,count) {

        var primebText2 = document.getElementsByClassName('primebText');
        var pos = newvalue.indexOf("CountdownTimer:");
        var res = newvalue.slice(pos+15, pos+34);
        newvalue = newvalue.replace("[[ CountdownTimer:", "" );
        newvalue = newvalue.replace("]]", "" );

        var x = setInterval(function() { 

            let end = new Date(toValidDateprime2(res));
            let start = new Date();
            var remaintime = end - start;
            var days = Math.floor(remaintime / (1000 * 60 * 60 * 24));
            var hours = Math.floor((remaintime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((remaintime % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((remaintime % (1000 * 60)) / 1000);
            var realtimeleft = days + "d "+hours + "h "+minutes + "m "+seconds + "s";
            if(seconds < 0){
                realtimeleft = "0d 0h 0m 0s";
            }
            primebText2[count].innerHTML = newvalue.replace(res,realtimeleft);
            primebText2[count].classList.remove("prime-counter");
            if (remaintime < 0) { 
                clearInterval(x); 
            } 
        }, 1000); 

    }

    function setCookieprime(cname, cvalue, exdays) {
      var d = new Date();
      var time = setcookietimestamp(exdays);
      if(exdays < 5){
        d.setTime(d.getTime() + (time * 60 * 1000));
      }else if(exdays > 4 && exdays < 8){
        d.setTime(d.getTime() + (time * 60 * 60 * 1000));
      }else if(exdays > 7){
        d.setTime(d.getTime() + (time * 24 * 60 * 60 * 1000));
      }
      
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookieprime(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    function setcookietimestamp(time) {
        var realtime='';
        if(time==1){
            realtime=1;
        }else if(time==2){
            realtime=5;
        }else if(time==3){
            realtime=10;
        }else if(time==4){
            realtime=30;
        }else if(time==5){
            realtime=1;
        }else if(time==6){
            realtime=6;
        }else if(time==7){
            realtime=12;
        }else if(time==8){
            realtime=1;
        }else if(time==9){
            realtime=7;
        }else if(time==10){
            realtime=15;
        }else if(time==11){
            realtime=30;
        }
        return realtime;
    }

</script> 