<?php 
	use App\Helpers\LiquidNoteHelper;
?>
@if( empty( $userNotes ) ) 
	<script type="text/javascript">
		console.log( 'user no notes ');
	</script>
@else
	@php 
		
		$isDeclare = false;
		$isScript = false;
		$i=0;
		$style_note = json_decode( $NoteStyle['style'] ,true );


	@endphp

	@if($userNotes[0]['planid'] > 2)
		@foreach( $userNotes as $index => $note )	
			

			@php 
			$CountryClass = '';

			$appearance    = json_decode( $NoteStyle['appearance'] ,true );

			$liquidCondition = LiquidNoteHelper::getLiquidProductCondition($appearance, $note, $style_note ); 
			
			if( $isDeclare == false ) {  		
				$isDeclare = true;
				
				echo $liquidCondition['global_variable']; 
			}  	
			@endphp	
			
			@php 
				
				if($i < 1){
					echo $liquidCondition['condition']; 
				}
				$i++;
			@endphp	
		@endforeach
		
		@if( $isDeclare == true )
		
		@endif
		@php
			if( $isScript == false ) {   		
				$isScript = true;
			@endphp	
			{% if HideAssets != '1' %}{% render 'primen-js' %}{% endif %}
		@php } @endphp	

	@else
		<script type="text/javascript">
			// console.log( 'user no notes ');
		</script>
	@endif
	
@endif