@if($badgelink != '' || $linktab=='2')
    @php
        $styleval="cursor:pointer;";
    @endphp

@else
    @php
        $styleval="";
    @endphp
@endif

@if($imgformat == 'svg')
    @php
        $imgformstyle="height:".$height;
    @endphp

@else
    @php
        $imgformstyle="";
    @endphp
@endif


@if( $type == 'text' )
<div class="primebDesktop @{{primebInnerClass}} {!! $badgeidclass !!} {!! $CountyClass !!}"  {!! $primebtooltip !!} {!! $SelectedCountry !!}  onclick="redirectLinkbadge('{{$badgelink}}','{{$linktab}}','{{$primebadgeid}}','{{$modalsize}}');" style="@{{ primebInnerStyle }}{{ $styleval }} {{ $nodisplay }} ">

    <div style="{{ $badgestyle }};">        
        <div  class="badgetitle primebText prime-font-adjust {!! $downtitlesty !!}" data-primeproductid='@{{product.id}}' {!! $primebDefaultSize !!} {!! $primebMobileSize !!} {!! $primebTabletSize !!} style="{{ $textStyle }}">
            {!! $title !!}      
        </div>
    </div>
</div>
@else
<div class="primebDesktop @{{primebInnerClass}} {!! $badgeidclass !!} {!! $CountyClass !!}"  {!! $primebtooltip !!} {!! $SelectedCountry !!} onclick="redirectLinkbadge('{{$badgelink}}','{{$linktab}}','{{$primebadgeid}}','{{$modalsize}}');" style="@{{ primebInnerStyle }}{{ $styleval }}" >

    <div style="{{ $badgestyle }};"  >      
        <div  class="badgetitle prime-height-adjust" {!! $primebDefaultHeight !!} {!! $primebMobileHeight !!} {!! $primebTabletHeight !!} style="{{ $textStyle }}">
            <img class="primebImage" src="@{{Imagesize}}" alt="{{$alttext}}" style="max-height: {{$height}};height:unset;{{ $imgformstyle }}" >
         
        </div>	   
    </div>
</div> 
@endif