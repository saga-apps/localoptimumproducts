<?php 
	use App\Helpers\LiquidHelper;
?>
@if( empty( $userBadges ) )
	<script type="text/javascript">
		console.log( 'user no badges ');
	</script>
@else
@php 
	
	$isDeclare = false;

@endphp

@foreach( $userBadges as $index => $badge )
	
	@php 
	
	$display       = json_decode( $badge['display'] ,true );
  	$displayrules  = json_decode( $badge['displayrules'] ,true );
  	$appearance    = json_decode( $badge['appearance'] ,true );

  	$condition     = $badge['condition'] == 1 ? ' and ':' or ';

  	$liquidCondition = LiquidHelper::getLiquidProductCondition( $displayrules , $condition , $appearance ,  $badge,  $display ); 

  	if( $isDeclare == false ) {
  		
  		$isDeclare = true;

  		echo $liquidCondition['global_variable']; 
  
  	@endphp {% comment %} 
		start declare Global Variable  
	{% endcomment %}
  	@php
	
	}
  	
  	@endphp
  	
	
  	{% comment %} 
	==== Badge number started : {{$index+1}} === 
	{% endcomment %}

  	{% if starbBadgeCounter <= starbMaxDisplayBadge %}
  	@php 
  		echo $liquidCondition['condition']; 
  	@endphp
	{% endif %}	

	{% comment %} 
	=== End badge number = {{$index+1}} === 
	{% endcomment %}	
	
@endforeach
@endif