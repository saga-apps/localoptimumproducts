@if( empty( $userHighlights ) ) 
	<script type="text/javascript">
	</script>
    @php 
		$height = '';
		$box = '';
        $counttime = '';
        $setcountforvisit = '';
        $weekdays = '';
        $dateformat = '';
	@endphp
@else
	@php 
		
		$isDeclare = false;
		$isScript = false;
		$display = json_decode( $HighlightStyle['display'] ,true );
		$style_highlight = json_decode( $HighlightStyle['style'] ,true );
		$height = isset( $display['height'] ) ?  $display['height'].'px' :'25px';
		$box = '';
        $counttime = $livecounttime;
        $setcountforvisit = $setlivevisitcount;
        $weekdays = $weekdays;
        $dateformat = $dateformat;
		if(isset($display['shadow']) && $display['shadow'] == 'light'){
			$box = "rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px, rgba(0, 0, 0, 0.2) 0px 1px 5px 0px";
		}

	@endphp

@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script type="text/javascript">

    removehighlight();
    function removehighlight() {
        var primeh1 = document.getElementsByClassName('1primehDesktop');
        if(primeh1[0]){
        } else {
            var remove_div1 = document.getElementsByClassName('1primeHighlights');
            if(remove_div1[0]){
                remove_div1[0].parentNode.removeChild(remove_div1[0]);
            }
        }
        var primeh2 = document.getElementsByClassName('2primehDesktop');
        if(primeh2[0]){
        } else {
            var remove_div2 = document.getElementsByClassName('2primeHighlights');
            if(remove_div2[0]){
                remove_div2[0].parentNode.removeChild(remove_div2[0]);
            }
        }
        var primeh3 = document.getElementsByClassName('3primehDesktop');
        if(primeh3[0]){
        } else {
            var remove_div3 = document.getElementsByClassName('3primeHighlights');
            if(remove_div3[0]){
                remove_div3[0].parentNode.removeChild(remove_div3[0]);
            }
        }
    }

    document.addEventListener('DOMContentLoaded', (event) => {

        var mypage = '@{{template}}';
        var myproductid = '@{{product.id}}';
        // for (var i = 0; i < 8; i++) {
            setTimeout(function(){ 
                primehsetlivevisit(mypage,myproductid);
                
            }, 500); 
        // }

        primehonload();

        var primehText = document.getElementsByClassName('primehText');
        if(primehText[0]){
            for (var i = 0; i < primehText.length; i++) {
                var value = primehText[i].innerHTML;
                if( value.indexOf("[[today") != -1  ){
                    expdeliveryhdate(value,i);
                }
            }
            
        }

        var primehsku = '@{{ProductSKU}}';
        var skuclass = document.getElementsByClassName('primehsku');
        if(primehsku == 0){
            for (var i = 0; i < skuclass.length; i++) {
                if(skuclass[i].dataset.sku == 0){
                    var parentsku = skuclass[i].parentElement;
                    var parent2sku = parentsku.parentElement;
                    parent2sku.style.cssText = 'display:none !important';
                }
            }
        }


        document.getElementById("prime-modal-close").onclick = function() {mymodalprimeh()};
        window.onclick = function(event) {
            if (event.target.classList[1] == 'prime-modal-open') {
                mymodalprimeh();
            }
        }

        

    })

    function expdeliveryhdate(value,count) {
        var weekdays = '<?php echo $weekdays; ?>';
        var dateformatselected = '<?php echo $dateformat; ?>';
        var primehTextdate = document.getElementsByClassName('primehText');
        var realvalue = value;
        var pos = realvalue.indexOf("[[today");
        var startres = realvalue.slice(pos+8, pos+9);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        var pos2 = realvalue.indexOf("[[today");
        var endres = realvalue.slice(pos2+8, pos2+9);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        var wrongdays = weekdays.split(",");
        // var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
        var startDate = new Date(new Date().getTime()+(parseInt(startres)*24*60*60*1000));
        var today = new Date(new Date().getTime());
        var firstinc=0;
        while (today <= startDate) {
            var getday = String(today.getDay());
            if(wrongdays.indexOf(getday) != -1){
            startDate.setDate(startDate.getDate() + 1);
            firstinc++;
            }
            today.setDate(today.getDate() + 1);
        }
        value = value.replace("[[today,"+startres+"]]", moment(startDate).format(dateformatselected));

        var extradays = firstinc+parseInt(endres);
        var endDate = new Date(new Date().getTime()+(extradays*24*60*60*1000));
        var todayend = startDate;
        var secinc=0;
        while (todayend <= endDate) {
            var getday = String(todayend.getDay());
            if(wrongdays.indexOf(getday) != -1){
            endDate.setDate(endDate.getDate() + 1);
            secinc++;
            }
            todayend.setDate(todayend.getDate() + 1);
        }
        value = value.replace("[[today,"+endres+"]]", moment(endDate).format(dateformatselected));
        primehTextdate[count].innerHTML = value;
    }

    function primehsetlivevisit(mypage,myproductid) {
        setTimeout(function(){ 

            var settimelive = '<?php echo $counttime; ?>';
            var setvisitlimit = '<?php echo $setcountforvisit; ?>';

            var d = new Date();
            var time = setcookietimestamph(settimelive);
            var mytime = '';
            if(settimelive < 5){
                mytime = (time * 60 );
            }else if(settimelive > 4 && settimelive < 8){
                mytime =(time * 60 * 60 );
            }else if(settimelive > 7){
                mytime = (time * 24 * 60 * 60 );
            }

            var primeloop2 = document.getElementsByClassName('primehloop');
            for (var i = 0; i < primeloop2.length; i++) {
                var smallloop2 = primeloop2[i].parentElement;
                var parentloop2 = smallloop2.getAttribute('data-primeproductid');
                var myuser = getCookieprimeh("primevisits"+parentloop2);
                if (myuser != "") {
                    myhttpreqh(parentloop2,0,mytime,setvisitlimit);
                } else {
                    myhttpreqh(parentloop2,1,mytime,setvisitlimit);
                    setCookieprimeh("primevisits"+parentloop2, 'setprimevisits', settimelive);
                }

            }

            var primeloop = document.getElementsByClassName('primehloop');
            for (var i = 0; i < primeloop.length; i++) {
                if(mypage == 'product'){
                    var smallloop = primeloop[i].parentElement;
                    var parentloop = smallloop.getAttribute('data-primeproductid');
                    if(parentloop == myproductid){
                        primeloop[i].classList.add("primevisitsh"+parentloop);
                        // var removeloop = primeloop[i].parentElement;
                        // var removeloop2 = removeloop.parentElement;
                    }else{
                        var removeloop = primeloop[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        removeloop2.style.visibility = 'hidden';
                        primeloop[i].classList.remove("primehloop");
                        removeloop2.remove();
                    }
                }else{
                  var oldloop = primeloop[i].parentElement;
                  var newloop = oldloop.parentElement;
                  newloop.style.visibility = 'hidden';
                  primeloop[i].classList.remove("primehloop");
                  newloop.remove();
                }
            }

        }, 500);
    }

    function myhttpreqh(lastid,mynew,gettime,setvisitlimit) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var pageliveview = JSON.parse(this.responseText);
                var primevisits = document.getElementsByClassName("primevisitsh"+lastid);
                for (var i = 0; i < primevisits.length; i++) {
                    if(setvisitlimit < pageliveview.pageviews){
                        primevisits[i].innerHTML = pageliveview.pageviews;
                        var removeloop = primevisits[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        removeloop2.style.visibility = 'visible';
                    }else{
                        var removeloop = primevisits[i].parentElement;
                        var removeloop2 = removeloop.parentElement;
                        removeloop2.style.visibility = 'hidden';
                        primevisits[i].classList.remove("primehloop");
                        removeloop2.remove();
                        removehighlight();
                    }
                        
                }
            }
        };
        xhttp.open("GET","https://visitcounterstaging.thalia-apps.com/?shopifyid="+lastid+"&new="+mynew+"&time="+gettime, true);
        xhttp.send();
    }

    function mymodalprimeh() {
      var target = document.getElementById("prime-modal-window");
      var parenttarget = target.parentElement;
      if (parenttarget.classList.contains('prime-modal-open')) {
        parenttarget.classList.remove('prime-modal-open');
        document.getElementById('prime-modal-content').innerHTML = '';
      }
    }

    
    function redirectHighlightLink(highlightval,links,primehighlightid,modalsize) {

        if(highlightval != ''){

        // console.log('clicke');
            var highlightlink = highlightval;
            if(links == 0){
                window.location.href = highlightlink;
            }else{
                window.open(highlightlink,'_blank');
            }

        }
        if(links == 2){
            var target = document.getElementById("prime-modal-outer");
            var myclasses = '';
            if(modalsize == 0){
                myclasses = 'prime-modal-window prime-modal-small';
            }else if(modalsize == 2){
                myclasses = 'prime-modal-window prime-modal-large';
            }else{
                myclasses = 'prime-modal-window';
            }
            
            document.getElementById('prime-modal-outer').classList.add('prime-modal-open');
            var setclass = document.getElementById('prime-modal-window');
            setclass.className = '';
            setclass.className = myclasses;
            var xhr = new XMLHttpRequest();
            var url = '/?view=primemeta&id==highlight-popup-'+primehighlightid;
            xhr.open("GET", url, true);
            xhr.setRequestHeader("Cache-Control", "max-age=3600");
            xhr.onreadystatechange = function() {
                if (this.readyState === XMLHttpRequest.DONE) {
                    if (this.status === 200) {
                        document.getElementById('prime-modal-content').innerHTML = this.responseText;
                    } else {
                        console.log(this.status, this.statusText);
                    }
                }
                };
                xhr.send();
        }
    
    }

    function toValidDateprime(datestring){
        return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
    }

    function myTimerprime(newvalue,count) {

        var primehText2 = document.getElementsByClassName('primehText');
        var pos = newvalue.indexOf("CountdownTimer:");
        var res = newvalue.slice(pos+15, pos+34);
        newvalue = newvalue.replace("[[ CountdownTimer:", "" );
        newvalue = newvalue.replace("]]", "" );

        var x2 = setInterval(function() { 

            let end = new Date(toValidDateprime(res));
            let start = new Date();
            var remaintime = end - start;
            var days = Math.floor(remaintime / (1000 * 60 * 60 * 24));
            var hours = Math.floor((remaintime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((remaintime % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((remaintime % (1000 * 60)) / 1000);
            var realtimeleft = days + "d "+hours + "h "+minutes + "m "+seconds + "s";
            if(seconds < 0){
                realtimeleft = "0d 0h 0m 0s";
            }
            primehText2[count].innerHTML = newvalue.replace(res,realtimeleft);
            primehText2[count].classList.remove("prime-counter");
            if (remaintime < 0) { 
                clearInterval(x); 
            } 
        }, 1000); 

    }

    function primehonload() {

        var primehText = document.getElementsByClassName('primehText');
        setTimeout(function(){ 
            if(primehText[0]){
                for (var i = 0; i < primehText.length; i++) {
                    var value = primehText[i].innerHTML;
                    
                    if( value.indexOf("CountdownTimer:") != -1  ){
                        myTimerprime(value,i);
                    }
                }
            } 
        }, 1000);

        var primeh1 = document.getElementsByClassName('1primehDesktop');
        if(primeh1[0]){
        } else {
            var remove_div1 = document.getElementsByClassName('1primeHighlights');
            if(remove_div1[0]){
                remove_div1[0].parentNode.removeChild(remove_div1[0]);
            }
        }
        var primeh2 = document.getElementsByClassName('2primehDesktop');
        if(primeh2[0]){
        } else {
            var remove_div2 = document.getElementsByClassName('2primeHighlights');
            if(remove_div2[0]){
                remove_div2[0].parentNode.removeChild(remove_div2[0]);
            }
        }
        var primeh3 = document.getElementsByClassName('3primehDesktop');
        if(primeh3[0]){
        } else {
            var remove_div3 = document.getElementsByClassName('3primeHighlights');
            if(remove_div3[0]){
                remove_div3[0].parentNode.removeChild(remove_div3[0]);
            }
        }

        setTimeout(function(){ 
            var primeinstances = tippy('[data-tippy-content]');
        }, 500);

        if(countryh == 'primehighlightcountry'){
            countryh = 'newprimehighlightcountry';
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            
                if (this.readyState == 4 && this.status == 200) {
                    var mycountryh = this.responseText;
                    var countryclassh = document.getElementsByClassName('primehCountry');
            
                    for (var i = 0; i < countryclassh.length; i++) {
                        var counrtylisth = countryclassh[i].getAttribute('data-countryselectedh');
                        var position = counrtylisth.search(mycountryh);
                        if(position < 0){ 
                            countryclassh[i].style.cssText = 'display:none !important';
                            countryclassh[i].classList.add("removeme");
                            // countryclassh[i].remove();
                        }
                    }
                    var removeclass = document.getElementsByClassName('removeme');
                    for (var i = 0; i < removeclass.length; i++) {
                        removeclass[i].remove();
                    }
                    var highlightclass1 = document.getElementsByClassName('1primehDesktop');
                    if(highlightclass1.length < 1){
                        var maindiv1 = document.getElementsByClassName('1primeHighlights');
                        if(maindiv1[0]){
                            maindiv1[0].remove();
                        }
                    }
                    var highlightclass2 = document.getElementsByClassName('2primehDesktop');
                    if(highlightclass2.length < 1){
                        var maindiv2 = document.getElementsByClassName('2primeHighlights');
                        if(maindiv2[0]){
                            maindiv2[0].remove();
                        }
                    }
                    var highlightclass3 = document.getElementsByClassName('3primehDesktop');
                    if(highlightclass3.length < 1){
                        var maindiv3 = document.getElementsByClassName('3primeHighlights');
                        if(maindiv3[0]){
                            maindiv3[0].remove();
                        }
                    }
                    
                }
            };
            xhttp.open("GET", "https://geo.thalia.workers.dev/", true);
            xhttp.send();
            
        }

    }

    function setCookieprimeh(cname, cvalue, exdays) {
      var d = new Date();
      var time = setcookietimestamph(exdays);
      if(exdays < 5){
        d.setTime(d.getTime() + (time * 60 * 1000));
      }else if(exdays > 4 && exdays < 8){
        d.setTime(d.getTime() + (time * 60 * 60 * 1000));
      }else if(exdays > 7){
        d.setTime(d.getTime() + (time * 24 * 60 * 60 * 1000));
      }
      
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookieprimeh(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    function setcookietimestamph(time) {
        var realtime='';
        if(time==1){
            realtime=1;
        }else if(time==2){
            realtime=5;
        }else if(time==3){
            realtime=10;
        }else if(time==4){
            realtime=30;
        }else if(time==5){
            realtime=1;
        }else if(time==6){
            realtime=6;
        }else if(time==7){
            realtime=12;
        }else if(time==8){
            realtime=1;
        }else if(time==9){
            realtime=7;
        }else if(time==10){
            realtime=15;
        }else if(time==11){
            realtime=30;
        }
        return realtime;
    }


</script>