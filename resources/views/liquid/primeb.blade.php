<?php 
	use App\Helpers\LiquidHelper;
?>
@if( empty( $userBadges ) )
	<script type="text/javascript">
		//console.log( 'user no badges ');
	</script>
@else
@php 
	
	$isDeclare = false;
	$isScript = false;
	
@endphp


@foreach( $userBadges as $index => $badge )	
	@php 
	$CountryClass = '';
	$LangClass = '';
	$display       = json_decode( $badge['display'] ,true );
  	$displayrules  = json_decode( $badge['displayrules'] ,true );
	
  	$appearance    = json_decode( $badge['appearance'] ,true );
	$badgestyle    = json_decode($badge['badgestyle'],true);

	$countdowntitle = 0;

	if( isset($badge['excludeproduct']) && $badge['excludeproduct'] != ''){
		$excludeproduct = $badge['excludeproduct'];
	}else{
		$excludeproduct = '';
	}

	if(strpos($badge['title'],'{{ ProductSKU }}')  !== false){
		$badgesku=str_replace("{{ ProductSKU }}",'<span class="primebsku" data-sku="{{ProductSKU}}" >{{ ProductSKU }}</span>',$badge['title']);
		$badge['title']=$badgesku;
	}else if(strpos($badge['title'],'{{ProductSKU}}')  !== false){
		$badgesku=str_replace("{{ProductSKU}}",'<span class="primebsku" data-sku="{{ProductSKU}}" >{{ProductSKU}}</span>',$badge['title']);
		$badge['title']=$badgesku;
	}else{}


	if($badge['planid'] > 2){

		$displayprorules  = json_decode( $badge['displayprorules'] ,true );
		
		if( isset($badge['customer_location']) && $badge['customer_location'] != ''){
			$customer_location = $badge['customer_location'];
			$CountryClass = 'primebCountry'; 
		}else{
			$customer_location = '';
		}

		if( isset($badge['shop_language']) && $badge['shop_language'] != ''){
			$shop_language = $badge['shop_language'];
			$LangClass = 'primeblang'; 
		}else{
			$shop_language = '';
		}

		if(strpos($badge['title'],'[[ CountdownTimer:')  !== false){
			$countdowntitle = 1;
		}else{}

		if(strpos($badge['title'],'[[CountdownTimer:')  !== false){
			$countdowntitle = 1;
		}else{}

	}else{
		$displayprorules  = '';

		$customer_location = '';

		$shop_language = '';

		if($badge['badgegroup'] == '1'){

		}else{
			continue;
		}

		if(strpos($badge['title'],'[[ CountdownTimer:')  !== false){
			continue;
		}else if(strpos($badge['title'],'[[CountdownTimer:')  !== false){
			continue;
		}else{}

		if(strpos($badge['title'], 'product.metafields') !== false){
			continue;
		}else if(strpos($badge['title'],'product.metafields')  !== false){
			continue;
		}else{}
		
		if(strpos($badge['title'],'{{ OrderCount }}')  !== false){
			continue;
		}else if(strpos($badge['title'],'{{OrderCount}}')  !== false){
			continue;
		}else{}

		if(strpos($badge['title'],'{{ RecentSoldCount }}')  !== false){
			continue;
		}else if(strpos($badge['title'],'{{RecentSoldCount}}')  !== false){
			continue;
		}else{}

		if(strpos($badge['title'],'[[today')  !== false){
			continue;
		}else if(strpos($badge['title'],'[[ today')  !== false){
			continue;
		}else{}
		
	}

	if($badge['planid'] > 3){

		if(strpos($badge['title'],'{{ VisitorCounter }}')  !== false){
			$badgelive=str_replace("{{ VisitorCounter }}",'<span class="primebloop" >{{ VisitorCounter }}</span>',$badge['title']);
			$badge['title']=$badgelive;
		}else if(strpos($badge['title'],'{{VisitorCounter}}')  !== false){
			$badgelive=str_replace("{{VisitorCounter}}",'<span class="primebloop" >{{VisitorCounter}}</span>',$badge['title']);
			$badge['title']=$badgelive;
		}else{}

	}else{
		if(strpos($badge['title'],'{{ VisitorCounter }}')  !== false){
			continue;
		}else if(strpos($badge['title'],'{{VisitorCounter}}')  !== false){
			continue;
		}else{}
	}
	

  	$condition = $badge['condition'] == 1 ? ' and ':' or ';

  	$liquidCondition = LiquidHelper::getLiquidProductCondition( $displayrules , $condition , $appearance ,  $badge,  $display, $excludeproduct, $badgestyle, $CountryClass, $customer_location, $displayprorules, $countdowntitle, $shop_language, $LangClass ); 
	

  	if( $isDeclare == false ) {  		
  		$isDeclare = true;
  		echo $liquidCondition['global_variable']; 
	}  	
  	@endphp	
  	{% comment %} Start Badge: {{$index+1}} {% endcomment %}
  	{% if primebBadgeCounter <= primebMaxDisplayBadge %}
  	@php 
  		echo $liquidCondition['condition']; 
  	@endphp
	{% endif %}
	{% comment %} End Badge: {{$index+1}} {% endcomment %}	
@endforeach

@if( $isDeclare == true )
    </div>
@endif
	@php
	if( $isScript == false ) {  		
  		$isScript = true;
	@endphp	
	{% if HideAssets != '1' %}{% render 'primeb-js' %}{% endif %}
	@php } @endphp	

@endif