<?php 
	use App\Helpers\LiquidHighlightHelper2;
?>
@if( empty( $userHighlights ) ) 
	<script type="text/javascript">
		// console.log( 'user no highlights ');
	</script>
@else
	@php 
		$isDeclare = false;
		
        $highlightcount = $highlightcount;
		$display = json_decode( $HighlightStyle['display'] ,true );

		$style_highlight = json_decode( $HighlightStyle['style'] ,true );

		$height = isset( $display['height'] ) ?  $display['height'].'px' :'25px';

		$box = '';
		if(isset($display['shadow']) && $display['shadow'] == 'light'){
			$box = "rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px, rgba(0, 0, 0, 0.2) 0px 1px 5px 0px";
		}

	@endphp

	@foreach( $userHighlights as $index => $highlight )	
		@php 
		$LangClass = '';
		$CountryClass = '';
		$displayrules  = json_decode( $highlight['displayrules'] ,true );
		$displayprorules  = json_decode( $highlight['displayprorules'] ,true );
		$countdowntitle = 0;
	//	$appearance    = json_decode( $highlight['appearance'] ,true );
	
		if( isset($highlight['excludeproduct']) && $highlight['excludeproduct'] != ''){
          	$excludeproduct = $highlight['excludeproduct'];
        }else{
			$excludeproduct = '';
		}

		if( isset($highlight['customer_location']) && $highlight['customer_location'] != ''){
			$customer_location = $highlight['customer_location'];
			$CountryClass = 'primehCountry'; 
		}else{
			$customer_location = '';
		}

		if( isset($highlight['shop_language']) && $highlight['shop_language'] != ''){
			$shop_language = $highlight['shop_language'];
			$LangClass = 'primehlang'; 
		}else{
			$shop_language = '';
		}

		if(strpos($highlight['title'],'[[ CountdownTimer:')  !== false){
			$countdowntitle = 1;
		}else{}

		if(strpos($highlight['title'],'[[CountdownTimer:')  !== false){
			$countdowntitle = 1;
		}else{}

		if(strpos($highlight['title'],'{{ ProductSKU }}')  !== false){
			$highlightsku=str_replace("{{ ProductSKU }}",'<span class="primehsku" data-sku="{{ProductSKU}}" >{{ ProductSKU }}</span>',$highlight['title']);
			$highlight['title']=$highlightsku;
		}else if(strpos($highlight['title'],'{{ProductSKU}}')  !== false){
			$highlightsku=str_replace("{{ProductSKU}}",'<span class="primehsku" data-sku="{{ProductSKU}}" >{{ProductSKU}}</span>',$highlight['title']);
			$highlight['title']=$highlightsku;
		}else{}

		if($highlight['planid'] > 3){
			if(strpos($highlight['title'],'{{ VisitorCounter }}')  !== false){
				$highlightlive=str_replace("{{ VisitorCounter }}",'<span class="primehloop" >{{ VisitorCounter }}</span>',$highlight['title']);
				$highlight['title']=$highlightlive;
			}else if(strpos($highlight['title'],'{{VisitorCounter}}')  !== false){
				$highlightlive=str_replace("{{VisitorCounter}}",'<span class="primehloop" >{{VisitorCounter}}</span>',$highlight['title']);
				$highlight['title']=$highlightlive;
			}else{}

		}else{

			if(strpos($highlight['title'],'{{ VisitorCounter }}')  !== false){
				continue;
			}else if(strpos($highlight['title'],'{{VisitorCounter}}')  !== false){
				continue;
			}else{}
		}
		
		// print_R( $highlight);exit;
		$appearance    = json_decode( $HighlightStyle['appearance'] ,true );

		$condition     = $highlight['condition'] == 1 ? ' and ':' or ';

		$liquidCondition = LiquidHighlightHelper2::getLiquidProductCondition( $displayrules , $condition , $appearance ,  $highlight,  $display,$style_highlight,$excludeproduct, $CountryClass, $customer_location, $displayprorules, $countdowntitle, $shop_language, $LangClass  ); 

		if( $isDeclare == false ) {  		
			$isDeclare = true;
			echo $liquidCondition['global_variable']; 
		}

		@endphp	
		{% comment %} Start Highlight: {{$index+1}} {% endcomment %}
		@php 
			echo $liquidCondition['condition']; 
		@endphp
		{% comment %} End Highlight: {{$index+1}} {% endcomment %}	
	@endforeach

	@php 
		if( $highlightcount > 400 ) {   		
	@endphp	
		{% assign hideAssets2 = hideAssets | default: '0' %}
		{% assign primehOuterClass2 = primehOuterClass | default: '' %}
		{% assign primehOuterStyle2 = primehOuterStyle | default: '' %}
		{% assign primehInnerClass2 = primehInnerClass | default: '' %}
		{% assign primehInnerStyle2 = primehInnerStyle | default: '' %}
		{% assign primehImageClass2 = primehImageClass | default: '' %}
		{% assign primehImageStyle2 = primehImageStyle | default: '' %}
		{% assign primehTextClass2 = primehTextClass | default: '' %}
		{% assign primehTextStyle2 = primehTextStyle | default: '' %}
		{% render 'primeh5', product: product, hideAssets: hideAssets2, primehOuterClass: primehOuterClass2, primehOuterStyle: primehOuterStyle2, primehInnerClass: primehInnerClass2, primehInnerStyle: primehInnerStyle2, primehImageClass: primehImageClass2, primehImageStyle: primehImageStyle2, primehTextClass: primehTextClass2, primehTextStyle: primehTextStyle2 %}
	@php } @endphp

@endif