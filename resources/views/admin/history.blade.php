@extends('admin.master')

@section('title')
@parent
<title>InstaShop History</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">                   
            <div class="panel-body">                       
                <div class="form-group">                               
                    <label for="comtag" class="col-xs-3 control-label">
                        userid
                    </label>
                    <div class="col-xs-9">
                        <input id="userid" for="userid" type="text" name="userid" class="form-control"></input>                                   
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        @php //dd(isset($param['logs'])); @endphp 
        @if(!empty($logs))
        <table id="ordertable" class="table table-bordered table-striped text-left" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th class="text-center">Admin</th>
            <th class="text-center">User</th>
            <th class="text-center">File Name</th>
            <th class="text-center">Updated At</th>
            <th class="text-center">Action</th>   
        
        </tr>
        </thead>
    
        <tbody class="text-center">
            @foreach($logs as $log)
            <tr>
                <td class="vcenter">
                {{$log->adminid}}
                </td>
                <td class="vcenter">
                {{$log->shop}}
                </td>
                <td class="vcenter">
                {{$log->file}}
                </td>
                <td class="vcenter">
                {{date( 'd-M-Y H:m:i',strtotime( $log->created_at ) )}}
               
                </td>
                
                <td class="vcenter">
                <button class="btn btn-primary btn-xs" onclick="comparecode({{$log->id}});" class="btn">Compare code</button>
                </td>
            </tr>
            
            @endforeach
        </tbody>
        </table>
        @endif
          
          
    </div>
</div>  

<div class="modal" id="comparecode">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Code Difference</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <div class="filename"></div>
            <pre id="display"></pre>   
            
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
      
          </div>
        </div>
      </div>
    </div>

<script>
// const diffString = `diff --git a/sample.js b/sample.js
//       index 0000001..0ddf2ba
//       --- a/sample.js
//       +++ b/sample.js
//       @@ -1 +1 @@
//       -console.log("Hello World!")
//       +console.log("Hello from Diff2Html!")`;

//     $( document ).ready(function() {
//       const targetElement = document.getElementById('newdata');
//       const configuration = {
//         drawFileList: true,
//         fileListToggle: false,
//         fileListStartVisible: false,
//         fileContentToggle: false,
//         matching: 'lines',
//         outputFormat: 'side-by-side',
//         synchronisedScroll: true,
//         highlight: true,
//         renderNothingWhenEmpty: false,
//       };
//       const diff2htmlUi = new Diff2HtmlUI(targetElement, diffString, configuration);
//       diff2htmlUi.draw();
//       diff2htmlUi.highlightCode();
//     });


    function comparecode(id){
        $('#comparecode').modal('show');
        console.log(id);
        $('#loadingalert').show();
        $.ajax({
            method  :'GET',
            data: {'adminlogid':id},
            url     :'{{ asset("admin/getDiffCode" )}}',
            dataType: "html",
            async: "false"
            })
            .done(function( response ) {   
                $('#display').html('');
                var response = jQuery.parseJSON(response);
                $('.filename').html('<p>'+response.file+'</p>'); 
                var rawoldchanges = decodeHtml(response.oldchanges);
                var rawnewchanges = decodeHtml(response.newchanges);
                if(!rawnewchanges ){
                    $('#loadingalert').hide();
                    console.log(rawnewchanges);
                    $('#display').html("No new chnages pushed");
                    return false;
                }
                // rawoldchanges = rawoldchanges.replace('\ \n', '');
                // console.log(rawoldchanges);
                // console.log(rawnewchanges);
                const one = rawoldchanges,
                other = rawnewchanges,
                color = '';
                
                let span = null;

                const diff = Diff.diffChars(one, other),
                    display = document.getElementById('display'),
                    fragment = document.createDocumentFragment();

                diff.forEach((part) => {
                // green for additions, red for deletions
                // grey for common parts
                const color = part.added ? 'green' :
                    part.removed ? 'red' : 'grey';
                span = document.createElement('span');
                span.style.color = color;
                span.appendChild(document
                    .createTextNode(part.value));
                fragment.appendChild(span);
                });

                display.appendChild(fragment);
                $('#loadingalert').hide();
                // console.log(rawldchanges);

                // const diffString = `diff --git a/sample.js b/sample.js
                //                     index 0000001..0ddf2ba
                //                     --- a/sample.js
                //                     +++ b/sample.js
                //                     @@ -1 +1 @@
                //                     -console.log(asvb);
                //                     +console.log(wqwqeqwkeo);`;


                // const diffString = `diff --git a/sample.js b/sample.js
                //         index 0000001..0ddf2ba
                //         --- a/sample.js
                //         +++ b/sample.js
                //         @@ -1 +1 @@
                //         -`+rawoldchanges+`
                //         +`+rawnewchanges;

               // console.log(diffString);
                    // document.addEventListener('DOMContentLoaded', function () {
                        // const targetElement = document.getElementById('newdata');
                        // console.log(targetElement);
                        // var configuration = {
                        // drawFileList: true,
                        // fileListToggle: false,
                        // fileListStartVisible: false,
                        // fileContentToggle: false,
                        // matching: 'lines',
                        // outputFormat: 'side-by-side',
                        // synchronisedScroll: true,
                        // highlight: true,
                        // renderNothingWhenEmpty: false,
                        // matchingMaxComparisons: '9999999',
                        // maxLineSizeInBlockForComparison: '9999999'
                        //     };
                            
                        // const diff2htmlUi = new Diff2HtmlUI(targetElement, diffString, configuration);
                        // console.log(diff2htmlUi);
                        // console.log(diff2htmlUi.draw());
                        // diff2htmlUi.highlightCode();
                    // });
                        

                });
    }

    function decodeHtml(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        // console.log( txt.value);
        return txt.value;
    }

    
    
    // });

// document.addEventListener('DOMContentLoaded', () => {
//   var diffHtml = Diff2Html.html('<Unified Diff String>', {
//     drawFileList: true,
//     matching: 'lines',
//     outputFormat: 'side-by-side',
//   });
//   document.getElementById('newdata').innerHTML = diffHtml;
// });


    $('#userid').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            var userid = $('#userid').val();
            let url = new URL(window.location);
            url.searchParams.set('userid', userid);
            window.location = url.href;
        }
    });

</script>

@stop
 
