@extends('admin.master')

@section('title')
@parent
<title>InstaShop Dashboard</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent


                        <table id="" class="table table-bordered table-responsive table-striped text-center" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center">cronid</th>
                                    <th class="text-center">Start Time</th>
                                    <th class="text-center">Grouid</th>
                                    <th class="text-center">Userid</th>
                                    <th class="text-center">Product Count</th>
                                    {{-- <th class="text-center"></th>
                                    <th class="text-center"></th> --}}
                                </tr>
                            </thead>       
                            <tbody>
                                <?php
                                $i = 1;
//                                date_default_timezone_set('Asia/Kolkata');
                                if( !empty( $param['currentcron'] ) ) {
                                foreach ($param['currentcron'] as $value) {
                                    ?>    
                                    <tr>    
                                        <td class="vcenter" >{{ $value->taskid }}</td> 
                                        <td class="vcenter" >{{ $value->createtime }}</td> 
                                        <td class="vcenter" >{{ $value->groupid }}</td>
                                        <td class="vcenter" >{{ $value->userid }}</td>
                                        <td class="vcenter" >{{ $value->productcount }}</td>
                                        {{-- <td class="vcenter" >
                                            <button class="btn btn-link" onclick="deletecron({{ $value->cronid }})">Delete</button> | 
                                            <button class="btn btn-link" onclick="pausecron({{ $value->cronid }})">Pause</button>
                                        </td> --}}
                                    </tr>
                                   <?php
                                    $i++;
                                }
                            }
                                ?>
                            </tbody>
                        </table>
                    <!--</div>-->
                <!--</div>-->
<!--            </div>
        </div>-->
<!--    </div>
</div>-->

<!--<div class="row">
    <div class="col-xs-12 col-sm-10">-->
<!--        <div class="row">
            <div class="col-xs-12">-->
                <!--<div class="panel panel-default">-->
                    <!--<div class="panel-body">-->
                        <table id="usersList" class="table table-bordered table-responsive table-striped text-center" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center">Cronid</th>
                                    <th class="text-center">Start Time</th>
                                    <th class="text-center">End Time</th>
                                    <th class="text-center">Grouid</th>
                                    <th class="text-center">Userid</th>
                                    <th class="text-center">Product Count</th>
                                    <th class="text-center">Time</th>
                                </tr>
                            </thead>       
                            <tbody>
                                <?php
                                $i = 1;
                                date_default_timezone_set('Asia/Kolkata');
                            if( !empty( $param['cronstatus'] ) ) {
                                foreach ($param['cronstatus'] as $value) {
                                    ?>   
                                    <tr>    
                                        <td class="vcenter" >{{ $value->taskid }}</td>    
                                        <td class="vcenter">
                                            {{ date('d-m-Y H:i:s a',strtotime( $value->createtime)  ) }}
                                        </td>
                                        <td class="vcenter">
                                            {{ date('d-m-Y H:i:s a',strtotime( $value->endtime) ) }}
                                        </td>
                                        @php
                                        $color = substr(md5($value->groupid), 0, 6);
                                        @endphp
                                        <td class="vcenter" bgcolor="#{{$color}}">
                                                {{ $value->groupid }}
                                        </td>
                                        <td class="vcenter">
                                            {{ $value->userid }}
                                        </td>
                                        <td class="vcenter">
                                            {{ $value->productcount }}
                                        </td>
                                     

                                        <td class="vcenter">
                                            {{ gmdate("H:i:s",strtotime($value->endtime ) - strtotime($value->createtime) ) }}
                                        </td>

                                       
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                                ?>
                               
                            </tbody>
                        </table>


<script type="text/javascript">
    
    function pausecron(cronid)
    {
        console.log(cronid);
        
         var result = confirm("Confirm pause?");
         if (result) {  
        
          $.ajax({                        
                        method: "POST",
                        url: "{{ asset('admin/pausecron')}}",            
                        data: "_token={{csrf_token()}}&cronid="+cronid,
//                        dataType: "html",
                        async: "false"                        
                      })
                        .done(function( data ) {                            
                           // location.reload();
                        });        
        }
    }
    
      function deletecron(cronid)
    {
        console.log(cronid);
        
         var result = confirm("Confirm delete?");
         if (result) {  
        
          $.ajax({                        
                        method: "POST",
                        url: "{{asset('admin/deletecron')}}",            
                        data: "_token={{csrf_token()}}&cronid="+cronid,
//                        dataType: "html",
                        async: "false"                        
                      })
                        .done(function( data ) {                            
                           // location.reload();
                        });        
        }
    }
  </script>

@stop