@extends('admin.master') 

@section('css')
@parent
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.ThemeSelected{
  background-color: #cccccc;
}
</style>
@stop

@section('js')
@parent
<!-- <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>-->
@stop

@section('title')
@parent
<title>MWS Dashboard</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent
<?php 
    $arrPlanNameById = array( 1 =>'Trial',2=>'Basic',3=>'Pro');
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
        <input id="searchbox" type="search" class="form-control" value="{{$param['query']}}">
        <br>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 ">

      <table id="usersList" class="table table-bordered table-responsive table-striped text-center" cellspacing="0" width="100%">
    <thead>
        <tr>                               
            <th class="text-center">Userid</th>
            <th class="text-center">Email</th>
            <th class="text-center">Shop</th>
            <th class="text-center">Trial</th>       
            <th class="text-center">Installed</th>      
            <th class="text-center">Payment</th>       
            <th class="text-center">IsReview</th>
            <th class="text-center">IsIntegrated</th>
            <!-- <th class="text-center">IsIntegratedHighlight</th> -->
<!--            <th class="text-center">LastInstalledOn</th> -->
            <!--<th class="text-center">Themes</th>-->
            <!--<th class="text-center">Plans</th>-->
            {{-- <th class="text-center">Plan Details</th>  --}}
            
            <th class="text-center">Action</th>
        </tr>
    </thead>  
        <tbody>
        
        @if($users->count() > 0)
            @foreach($users as $user)

                <tr>
                    <td class="vcenter">{{ $user->userid }}</td>
                    <td class="vcenter">{{ $user->email }}</td>
                    <td class="vcenter">
                        <a target="_blank" href="https://{{ $user->shop }}"> {{ $user->shop }} </a>
                        <br>Prime -{{!empty( $arrPlanNameById[$user->planid] ) ? $arrPlanNameById[$user->planid] :'' }} | Shopify -{{$user->shopifyplanname}}<br>
                        <select id="" class="" name="plan" onchange="changeplan(this,{{$user->userid}});" >
                        <option value="2" {{ !empty( $user->planid ) && $user->planid == 2 ? 'selected' :''}} >Basic</option>
                        <option value="3" {{ !empty( $user->planid ) && $user->planid == 3 ? 'selected' :''}} >Pro</option>
                        <option value="4" {{ !empty( $user->planid ) && $user->planid == 4 ? 'selected' :''}} >Gold</option>
                      </select>
                        <br>
                          <select class="themesValue_{{$user->userid}}" name="" >
                      <option value="" >Select Your Theme</option>
                        @foreach($user->themes as $themesValue)
                          <option value="{{ $themesValue['id'] }}" class={{ !empty( $themesValue['role'] ) && $themesValue['role'] == 'main' ? 'ThemeSelected' :''}} >{{$themesValue['name']}}</option>
                        @endforeach
                      </select>
                      <button class="btn btn-primary btn-xs"  onclick="downloadthemes({{$user->userid}});" class="btn">Download Theme</button>
                      <br>
                      <a href="javascript:void(0)" onclick="updateUser({{ $user->userid }},{{ $user->isinstalled }},{{ $user->ispaymentok }});"> Update Status </a>
                
                    
                </td>
                <td class="vcenter">
                    {{ $user->istrial }}                  
                </td>
                <td class="vcenter">
                    {{ $user->isinstalled }}
                </td>
                <td class="vcenter">
                    {{ $user->ispaymentok }}
                </td>
                    <td class="vcenter">
                      <label class="switch">
                        <input type="checkbox" class="isreviewchk" userid='{{$user->userid}}' @if($user->isreview == 1)checked @endif>
                        <span class="slider round"></span>
                      </label>
                    </td>
                    <td class="vcenter">
                      <div class="row">
                        <label for="text" class="" style="margin-right: 15px;">Badge</label>
                        <label class="switch"> 
                          <input type="checkbox" class="isintegratedchk" userid='{{$user->userid}}' @if($user->isintegrated == 1)checked @endif>
                          <span class="slider round"></span>
                        </label>
                      <br>
                        <label for="text" class="" >Highlight</label>
                        <label class="switch ">
                          <input type="checkbox" class="isintegratedhighlightchk" userid='{{$user->userid}}' @if($user->isintegratedhighlight == 1)checked @endif>
                          <span class="slider round"></span>
                        </label>
                      <br>
                        <label for="text" class="" style="margin-right: 30px;">Note</label>
                        <label class="switch ">
                          <input type="checkbox" class="isintegratednotechk" userid='{{$user->userid}}' @if($user->isintegratednote == 1)checked @endif>
                          <span class="slider round"></span>
                        </label>
                      <br>
                        <label for="text" class="" style="margin-right: 15px;">Banner</label>
                        <label class="switch ">
                          <input type="checkbox" class="isintegratedbannerchk" userid='{{$user->userid}}' @if($user->isintegratedbanner == 1)checked @endif>
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </td>
                    

                    <?php 

                        $date  = date( "Y-m-d H:i:s",strtotime( $user->createtime )+18000 );
                        $t1    = new DateTime($date, new DateTimeZone('UTC'));
                        $now   = new DateTime();
                    ?>
                    <!--<td class="vcenter">{{ $t1->format('d-M-Y A: H:m ')." UTC +5:30"  }}</td>-->
                    {{-- <td class="vcenter">
                           Credits : {{ $user->productcredit }} <br>  Product Count : {{ $user->productcount }}  <br> <button data-url="http://pawanmore/wordpress" onclick="changeCredits({{$user->userid}},{{$user->productcredit}},{{$user->productcount}})" class="btn btn-default btn-xs">Change</button>
                    </td> --}}

<!--                    <td class="vcenter">

                    
                    </td>-->

<!--                    <td class="vcenter">

                    
                        
                    </td>-->

                    <td class="vcenter">

                        
                    
                        <a href="{{asset('')}}autologin/{{ $user->userid }}" target="_blank" class="btn btn-default btn-xs">Login</a>



                        
                    </td>
                   
                </tr>

            @endforeach


        @endif
         </tbody>
    </table>
    <center>
              {{ $users->links() }}
                  
    </center>

    </div>
</div>



<div id="modalSettings" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div id="deleteconfirm">              
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<!--                        <h1 class="text-danger">&#9986;</h1>                      
                        <p>Settings Modal</p>   -->
                        
                       
                        <table class="table table-bordered table-responsive table-striped text-left">
                            
                        <col width="30%">
                        <col width="70%">
                        <form id="userUpdate" >
                      
                            <tbody>
                                <tr>
                                    <td class="vcenter" style="vertical-align: middle;">
                                        IsInstalled:
                                    </td>
                                    <td class="vcenter">
                                        <div class="radio">
                                            <label><input id="autosyncon" type="radio" name="isinstalled"  value="1">ON</label>
                                          </div>
                                          <div class="radio">
                                            <label><input id="autosyncoff" type="radio" name="isinstalled" value="0">OFF</label>
                                         </div>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vcenter" style="vertical-align: middle;">
                                        IsPaymentOK:
                                    </td>
                                    <td class="vcenter">
                                        <div class="radio">
                                            <label><input id="autosyncon" type="radio" name="ispaymentok"  value="1">ON</label>
                                          </div>
                                          <div class="radio">
                                            <label><input id="autosyncoff" type="radio" name="ispaymentok" value="0">OFF</label>
                                         </div>
                                    </td>
                                </tr>                               
                            </tbody>
                            </form>                           
                        </table>                         
                    </div>
                </div>
                <br>
                <br>
                 <button id="btnSaveSettings" 
                type="button" class="btn btn-primary">Save</button>
                <button id="btnCancelSettings" 
                type="button" class="btn btn-default">Cancel</button>
                <br>
                 <br>
                 <div id="lastautosync" class="text-center"></div>
                
            </div>
        </div>
    </div>
</div>
<div class="text-center p-1 m-auto text-center p3 col-12 collapse" id="requestalert_collection" style="top:40px;background-color: black;" >
  <div class="alert bg-dark">
    <h4 class="text-white" id="requestmessage_collection" style="color:white;"></h4>
  </div>
</div>

<div class="modal" id="changeCreditsModal">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Change Credits / Product Count</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <!-- Modal body -->
            <div class="modal-body">
            <form method="POST" action="{{'updatestore'}}">
                    {{ csrf_field() }}
    
                    <div class="form-group">
                            <label for="usr">UserId:</label>
                            <input  readonly name="userid" id="modaluserid" type="text" class="form-control" >
                          </div>
    
    
                    <div class="form-group">
                            <label for="usr">Credits:</label>
                            <input  name="productcredit" id="modalcredits" type="text" class="form-control">
                          </div>
    
                          <div class="form-group">
                                <label for="usr">Product Count:</label>
                                <input name="productcount"  id="modalproductcount" type="text" class="form-control">
                              </div>
                   
               
            
                    <button type="submit" class="btn btn-primary" >Save</button>
            </div>
      
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
      
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">

// var table = $('#usersList').DataTable({
                
//               "pageLength": 10,
//               responsive: true,
//               dom: '<"bottom">rt<"text-center col-xs-12"i><"text-center col-xs-12"p><"clear">',
//               "paging": true,
//               "pagingType": "numbers",
//               "searching" : false,
//               "ordering": false,
//               "bServerSide":false,
//           });

var $settingsmod = $("#modalSettings");

var newuserid      = '';
var newisinstalled = '';
var newispaymentok ='';

function updateUser( userid , isinstalled , ispaymentok ){

    $settingsmod.modal('show');
    newuserid      = userid;
    newisinstalled = isinstalled;
    newispaymentok = ispaymentok;

}

$("#btnSaveSettings").off('click').click(function () {            
    
    var formData = $("#userUpdate").serialize();

    var objPrepareRequestData = {
               'userid': newuserid,
                'data':formData,
                '_token': '{{csrf_token()}}'
                    //.//'actiontype':actiontype
            };

            $.ajax({
                method: "POST",
                url: '{{asset('admin/updateuser')}}',
                data: objPrepareRequestData,
                dataType: 'json',
                success: function(data) {
                //    console.log(data)
                    if( data.status == true ){
                        
                        $settingsmod.modal("hide");
                        location.reload(); 
                    }    

                } 
            });


  
            
    $settingsmod.modal("hide");
});

$("#btnCancelSettings").off('click').click(function () {      
    $settingsmod.modal("hide");
});

$('input[type=search]').on('keydown', function(e) {
    if (e.which == 13) {
        e.preventDefault();
        window.location = "{{asset('')}}admin/dashboard?query="+encodeURIComponent(document.getElementById("searchbox").value.trim());
    }
});

function downloadthemes(userid) {
  var themeid = $('.themesValue_'+userid).val();
  var themeName = $('.themesValue_'+userid).find('option:selected').text();
  // $('#loadingalert').show();
  url = '{{asset('downloadtheme')}}/'+themeid+'/'+themeName+'/'+userid,
  window.open( url, '_blank');
  // $.ajax( {
  //   method:'GET',
  //   url : '{{asset('downloadtheme')}}/'+themeid+'/'+themeName+'/'+userid,
  //   // data :{'userid':userid,'themeid':themeid,'themeName':themeName,'_token':'{{csrf_token()}}'},
  //   success:function( response ){
  //     var awsurl = 'https://s3.amazonaws.com/files.thalia-apps.com/'+response;
  //     console.log(awsurl);
      // $('#loadingalert').hide();
      // $('#requestalert_collection').show();
      // $('#requestmessage_collection').text('Plan Edited successfully');
      // $("#requestalert_collection").delay(2500).hide(0);
  //     window.open( awsurl, '_blank');
  //   }
  // });

}


function changeplan(full,userid) {
  var plan = full.value;  
  $.ajax( {
    url : '{{asset('changeplan')}}',
    method:'POST',
    data :{'userid':userid,'plan':plan,'_token':'{{csrf_token()}}'},
    success:function( response ){
      $('#requestalert_collection').show();
      $('#requestmessage_collection').text('Plan Edited successfully');
      $("#requestalert_collection").delay(2500).hide(0);
      
    }

  });

}

function changeCredits(userid,credits,productcount) {
    $('#modaluserid').val(userid);  
    $('#modalcredits').val(credits);  
    $('#modalproductcount').val(productcount);  
    $('#changeCreditsModal').modal('show');  
}

$(document).on('change', '.isreviewchk', function() {
 if($(this).prop("checked") == true){
             url = '{{asset('updatereview')}}/'+$(this).attr('userid')+'/1';
          }
          else if($(this).prop("checked") == false){
            url = '{{asset('updatereview')}}/'+$(this).attr('userid')+'/0';
          }
          window.location=url;

});

$(document).on('change', '.isintegratedchk', function() {

  if($(this).prop("checked") == true){
    url = '{{asset('updateintegration')}}/'+$(this).attr('userid')+'/1';
  }
  else if($(this).prop("checked") == false){
  url = '{{asset('updateintegration')}}/'+$(this).attr('userid')+'/0';
  }
  window.location=url;

});

$(document).on('change', '.isintegratedhighlightchk', function() {

if($(this).prop("checked") == true){
  url = '{{asset('updateintegrationhighlight')}}/'+$(this).attr('userid')+'/1';
}
else if($(this).prop("checked") == false){
url = '{{asset('updateintegrationhighlight')}}/'+$(this).attr('userid')+'/0';
}
window.location=url;

});

$(document).on('change', '.isintegratednotechk', function() {

if($(this).prop("checked") == true){
  url = '{{asset('updateintegrationnote')}}/'+$(this).attr('userid')+'/1';
}
else if($(this).prop("checked") == false){
url = '{{asset('updateintegrationnote')}}/'+$(this).attr('userid')+'/0';
}
window.location=url;

});

$(document).on('change', '.isintegratedbannerchk', function() {

if($(this).prop("checked") == true){
  url = '{{asset('updateintegrationbanner')}}/'+$(this).attr('userid')+'/1';
}
else if($(this).prop("checked") == false){
url = '{{asset('updateintegrationbanner')}}/'+$(this).attr('userid')+'/0';
}
window.location=url;

});

</script>
@stop