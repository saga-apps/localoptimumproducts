@extends('admin.master')

@section('css')
@parent
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

#usersList_paginate{
    text-align: center;
}

</style>
@stop

@section('js')
@parent
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
@stop

@section('title')
@parent
<title>MWS Dashboard</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent
<?php 
    $arrPlanNameById = array( 1 =>'Trial',2=>'Basic',3=>'Pro');
?>
<!-- <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
        <input id="searchbox" type="search" class="form-control" value="{{$param['query']}}">
        <br>
    </div>
</div> -->
<div class="row">
    <div class="col-xs-12 col-sm-12 ">
        <div class="">
            <btn  onclick="addshowcase('#addshowcase-model');"  type="text" class="btn btn-primary" >ADD </btn>
            <div class="form-group row">
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <select name="showcasetype" id="showcasetype" onchange="selectedType()"; class="form-control metafieldcond form-control-sm" >
                        <option {{ $type == 'badge' ? 'selected':'' }} value="badge">Badges</option>
                        <option {{ $type == 'highlight' ? 'selected':'' }} value="highlight" >Highlights</option>
                    </select>
                </div>
            </div>
        </div>
        
      <table id="showcaseList" class="table table-bordered table-responsive table-striped text-center" cellspacing="0" width="100%">
    <thead>
        <tr>                               
            <th class="text-center">#</th>
            <th class="text-center">Store</th>
            <th class="text-center">Action</th>
            <th class="text-center">Priority</th>  
        </tr>
    </thead>  
        <tbody style="cursor: move;" >
        
        @if(count($showcases) > 0)
        <?php $i=1;
              $showcasePriority = array(); ?>
            @foreach($showcases as $showcase)
            @php
                $showcasePriority[$showcase['id']] = $showcase['priority'];
            @endphp

                <tr showcaseid="{{$showcase['id']}}" id="row_{{$showcase['id']}}">
                    <td class="text-center index" showcaseid="{{$showcase['id']}}" >{{ $i }}</td>
                    <td class="text-center">{{$showcase['storename'] }}</td>
                    <td  class="text-center" >
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-sm btn-default "  onclick="changeStaus('{{$showcase['id']}}' , '{{$showcase['isactive'] }}' );" >
                            <i class="fa fa-toggle-on {{ $showcase['isactive'] == 1 ? 'text-primary':'text-muted' }} "></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-default" onclick="editShowcase('{{$showcase['id']}}','{{$showcase['storeurl'] }}','{{$showcase['storename'] }}','{{$showcase['type'] }}','{{$showcase['homeurl'] }}','{{$showcase['producturl'] }}','{{$showcase['collectionurl'] }}','{{$showcase['miscurl'] }}' );">Edit</button>
                            <button type="button" class="btn btn-sm btn-default" onclick="deletePop('{{$showcase['id']}}');">Delete</button>
                        </div>
                    </td>

                    <td class="Priority">
                        <div class="text-center text-muted">
                            <a href="#"><i class="fa fa-bars"></i></a>
                        </div>
                    </td>
                    
                   
                </tr>
                <?php $i++; ?>
            @endforeach


        @endif
         </tbody>
    </table>

    </div>
</div>

<div class="modal fade " id="addshowcase-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="">
       <h5 class="modal-title">Add/Edit Showcase
            <!-- <span class="small text-secondary ml-2">(Highlights will not be displayed on these products)</span> -->
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
                <form id="formRule" name="formRule" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="col-12 ">
                        <div class="form-group row">
                            <input type="hidden" name="showcaseid" id="showcaseid" value="">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control" name="storeurl" id="storeurl" value="" placeholder="Storeurl">
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control" name="storename" id="storename" value="" placeholder="StoreName">
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <select name="showcaseaddtype" id="showcaseaddtype" class="form-control " >
                                    <option  value="badge">Badges</option>
                                    <option  value="highlight" >Highlights</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control" name="producturl" id="producturl" value="" placeholder="Producturl">
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control" name="collectionurl" id="collectionurl" value="" placeholder="Collectionurl">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control" name="homeurl" id="homeurl" value="" placeholder="Homeurl">
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control" name="miscurl" id="miscurl" value="" placeholder="Miscurl">
                            </div>
                        </div>
                    </div>
                </form>
            <div>
              </br>
              </br>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="ExtableDataModel">
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" onclick="saveRule('#formRule');" class="btn btn-primary">Add</button>
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<div id="modalConfirmYesNo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <p class="modal-title">Delete Showcase</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="deleteproduct" >
                    <div class="font-weight-bold">
                        Are your sure you want delete the Showcase?
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button id="btnYesConfirmYesNo" type="button" class="btn btn-primary text-right">Confirm</button>
                    <button id="btnNoConfirmYesNo" type="button" class="btn btn-default">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var $confirm = $("#modalConfirmYesNo");
    var showcaseid = '';
    var showcasePriority = @json( isset($showcasePriority) ? $showcasePriority : '' );
  
    function deletePop( id ){
        
        showcaseid = id;

        $confirm.modal('show');

    }

    function addshowcase( modelid ){
        $(modelid).modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#showcaseid').val('');
        $('#storeurl').val('');
        $('#storename').val('');
        $('#homeurl').val('');
        $('#producturl').val('');
        $('#collectionurl').val('');
        $('#miscurl').val('');
        
    }

    function editShowcase( id,storeurl='',storename='',type='badge',homeurl='',producturl='',collectionurl='',miscurl='' ){
        
        $('#addshowcase-model').modal('show');
        $('#showcaseid').val(id);
        $('#storeurl').val(storeurl);
        $('#storename').val(storename);
        $('#showcaseaddtype').val(type).change();
        $('#homeurl').val(homeurl);
        $('#producturl').val(producturl);
        $('#collectionurl').val(collectionurl);
        $('#miscurl').val(miscurl);
        
    }

    $('input[type=search]').on('keydown', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            window.location = "{{asset('')}}admin/dashboard?query="+encodeURIComponent(document.getElementById("searchbox").value.trim());
        }
    });

    var table = $('#showcaseList').DataTable({
                
                "pageLength": 20,
                responsive: true,
                dom: '<"search pull-left"f><"bottom"l>rt<"text-center col-xs-12"i><"text-center col-xs-12"p><"clear">',
                "paging": false,
                "pagingType": "numbers",
                "searching" : true,
                "ordering": false,
                "bServerSide":false,
                // "bInfo" : true,
            });

    function saveRule( id ){
        let formData = $(id).serialize();
        $.ajax( {
            url:"{{asset('admin/addshowcase')}}",
            method:'POST',
            data :formData,
            success:function( response ){
                $('#addshowcase-model').modal("hide");
                setTimeout( function(){
                    location.reload();
                }, 500);
            }
        });
    }

    function changeStaus( id , isactive  ){
        let objBadge = {'id':id, 'isactive':isactive, '_token':'{{csrf_token()}}'};
        // $('#loadingalert').show();
        $.ajax( {
            url:"{{asset('admin/updateshowcase-status')}}",
            method:'POST',
            data :objBadge,
            success:function( response ){
                setTimeout( function(){
                location.reload();
                }, 2000);
            }
        });

    }

    function selectedType(){
        var type = $('#showcasetype').val();
        if(type == 'highlight'){
            window.location = "{{asset('')}}admin/showcaselist/highlight";
        }else{
            window.location = "{{asset('')}}admin/showcaselist/badge";
        }
        
    }

    $("#btnYesConfirmYesNo").click(function () {
        $confirm.modal("hide");
        // $('#loadingalert').show();
        $.ajax( {
            url:"{{asset('admin/deleteshowcase')}}",
            method:'POST',
            data :{'id':showcaseid,'_token':'{{csrf_token()}}'},
            success:function( response ){

            // $('#loadingalert').hide();
            // $('#requestmessage').text(response.msg);

            $("#row_"+showcaseid).remove();
            // toggleAlert();

            }

        })

    });

    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
        $(this).width($originals.eq(index).width())
        });
        return $helper;
    },
    updateIndex = function(e, ui) {
      $('td.index', ui.item.parent()).each(function (i) {
        $(this).html(i+1);
      });
      getPriorityOrder( );
    };

    $("#showcaseList tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex
    }).disableSelection();

    $("tbody").sortable({
        distance: 5,
        delay: 100,
        opacity: 0.6,
        cursor: 'move',
        update: function( data ) {
        }
    });

    function getPriorityOrder( ){
        let objBadgeOrder = {};
        var type = $('#showcasetype').val();
        $('table tr ').each(function(){
            let showcaseid = $(this).attr('showcaseid');
            if( typeof showcaseid != 'undefined'){
                let index = $(this).find('td.index').text();
                objBadgeOrder[showcaseid] = parseInt( index );
                
            }
        });

        if( JSON.stringify( showcasePriority ) === JSON.stringify(objBadgeOrder) ){
            return false;
        }
        $.ajax( {
            url:"{{asset('admin/updateshowcase-priority')}}",
            method:'POST',
            data :{'showcase':JSON.stringify( objBadgeOrder),'type':type,'_token':'{{csrf_token()}}'},
            success:function( response ){
                showcasePriority = objBadgeOrder;
                setTimeout( function(){
                    location.reload();
                }, 1000);
            }

        });


  }


</script>
@stop