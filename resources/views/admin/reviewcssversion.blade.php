@extends('admin.master')

@section('title')
@parent
<title> Reviews css versions</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent
<div class="row" >
    <div class="col-xs-12 col-lg-12 col-lg-12">
        <table class="table table-bordered table-responsive table-striped">
            <thead>
                <tr> 
                    <th>Userid </th>
                    <th>shop </th>
                </tr>
            </thead>
            <thead>
                <tr>
                    <td>{{$param['user']->userid }}</td>
                    <td>{{$param['user']->shop }} </td>
                </tr>
                
            </thead>
        </table>
     </div>
    </div>   
@if( empty( $param['reviewcssversion'] ) )

<h4> No Reviews Versions.</h4>
@else
<div class="row" >
    <div class="col-xs-12 col-lg-12 col-lg-12">
        <table class="table table-bordered table-responsive table-striped text-center" cellspacing="0" width="100%">

        <thead>
        <tr>                               
            <th class="text-center">#</th>
            <th class="text-center">Duration</th>
            <th class="text-center">Css</th>
            
        </tr>
        </thead>

        <colgroup>
            <col width="5%"/>
            <col width="30%"/>   
                     
            <col width="70%"/>       
             </colgroup>
             <thead>
            @foreach( $param['reviewcssversion'] as $index =>  $reviewVersion )

            <tr>
                <td class="vcenter">{{$index+1}}</td>
                <td class="vcenter">{{date('d-M-y H:i:a', strtotime( $reviewVersion['startdate'] ) )}}  - {{ !empty( $reviewVersion['enddate'] ) ? date('d-M-y H:i:a', strtotime( $reviewVersion['enddate'] ) ) :' current' }}</td>
                <td class="vcenter">{{$reviewVersion->customcss}}</td>
            </tr>

            @endforeach
            </thead>
        </table>
        <center>
            {{ $param['reviewcssversion']->links() }}
                      
        </center>
@endif
@stop