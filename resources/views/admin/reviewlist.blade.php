@if(!empty($reviews->count()))
<table class="table table-striped table-bordered table-responsive">
    <thead>
       <tr>
          <th scope="col">#</th>
          <th scope="col">Review</th>
          <th scope="col">Stars</th>
          <th scope="col">Actions</th>
       </tr>
    </thead>
    <colgroup>
       <col width="10%">
       <col width="65%">
       <col width="10%">
       <col width="15%">
    </colgroup>
    <tbody>
      @foreach ($reviews  as $rkey => $review)
      @php
      $reviewdata = json_decode($review->reviewsdata,true);    
      @endphp
      <tr id="reviewrow{{$review->reviewid}}">
      <th scope="row">{{$rkey+1}}</th>
        <td>
           <div class="content-{{$review->reviewid}}">
            <h6 class="rev-title" id="title-{{$review->reviewid}}">{{$reviewdata['Title']}}</h6>
            <h5 class="author" id="author-{{$review->reviewid}}">{{$reviewdata['Author']}}</h5>
            <p class="text-dark" id="body-{{$review->reviewid}}" >{{$reviewdata['Body']}}</p>
            <span class="date" id="date-{{$review->reviewid}}" >{{$reviewdata['Date']}}</span>
           </div>
           <div class="inputform-{{$review->reviewid}}">

           </div>

        </td>
        <td>
            <div class="content-{{$review->reviewid}}">  
            <span id="star-{{$review->reviewid}}">{{$review->star}}</span> stars
            </div>

         </td>
            
        <td>
           <div class="btn-group">
              {{-- <button type="button" id="editbutton-{{$review->reviewid}}" onclick="editReviewContent({{$review->reviewid}})" class="btn btn-sm btn-default">Edit</button> --}}
              <button type="button" style="display:none" id="savebutton-{{$review->reviewid}}" onclick="saveReviewContent({{$review->reviewid}})" class="btn btn-sm btn-default">Save</button>
             
           <button type="button" style="{{$review->ishidden == 1 ? 'display:none' : ''}}"  id="hidebutton-{{$review->reviewid}}" class="btn btn-sm btn-default" onclick="hideReview({{$review->reviewid}})" >Hide</button>
              <button type="button" style="{{$review->ishidden == 1 ? '' : 'display:none'}}" id="unhidebutton-{{$review->reviewid}}" class="btn btn-sm btn-default" onclick="unhideReview({{$review->reviewid}})" >Unhide</button>
             
           </div>
        </td>
     </tr>
      @endforeach
     
   
    </tbody>
 </table>
 <div class="justify-content-center reviewpagintion">
 {{$reviews->render("pagination::bootstrap-4")}}
 </div>

 @else

No Reviews Found
 @endif
 
 