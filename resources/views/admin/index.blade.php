<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        <title>Admin Prime</title>
        <meta charset="utf-8" />        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />  
        <meta content="" name="author" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        

        @yield('css')
        <!--<link rel="shortcut icon" href="favicon.ico" />-->
    </head>

    <body>  
        <div class="container" style="margin-top:50px">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">

                    <div class="panel-heading">
                        <h3 class="panel-title">Optimum Admin Login</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                           <div class = "alert alert-danger">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                        @endif
                        <form id="admin-login-form" method="POST" action="{{asset('admin')}}" >
                         {{ csrf_field() }}
                          
                            <fieldset>
                                <div class="form-group text-center">
                                    @if(Session::get('flash_message') == 'invalidpassword') 
                                    <span class="label label-sm label-danger">username or password is invalid!</span>
                                    @endif 
                                </div>
                                <div class="form-group ">
                                    <input class="form-control" required="" placeholder="E-mail" name="email" type="email" autofocus="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" required="" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-block btn-success register">Sign In</button>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
