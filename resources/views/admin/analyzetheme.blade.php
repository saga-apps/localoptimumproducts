@extends('admin.master')

@section('title')
@parent
<title>InstaShop Dashboard</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent

<div class="row">
  
<div class="col-md-3 col-xs-12">
                <div class="panel panel-default">                   
                    <div class="panel-body">                       
                             

                            <div class="form-group">                               
                                <label for="comtag" class="col-xs-3 control-label">
                                   userid
                                </label>
                                <div class="col-xs-9">
                                    <input id="userid" for="userid" type="text" name="userid" class="form-control"></input>                                   
                                </div>   
                                                           
                            </div>

                            <div class="form-group"> 
                              <div class="col-xs-3">
                                <button  onclick="getthemes();" class="btn btn-info">Theme</button>                                
                              </div>   

                              <div class="col-xs-9">
                                <select id="themes" class="form-control" name="plan"  >
                                  
                                </select>                               
                              </div>      
                            </div>
                          

                            <div class="form-group">                               
                                <label for="catag" class="col-xs-3 control-label">
                                    asset key
                                </label>
                                <div class="col-xs-9">
                                    <input id="assetkey" for="assetkey" type="text" name="assetkey" class="form-control"></input>                                   
                                </div>                               
                            </div>  
                           
                                <button style="margin-bottom: 5px;" onclick="writeproductliquid()" class="btn btn-info btn-block">templates/product.liquid</button>
                           
                            
                            <button style="margin-bottom: 5px;" onclick="writeproducttemplateliquid()" class="btn btn-info btn-block">sections/product-template.liquid</button>
                            
                            <button onclick="writeformtemplateliquid()" class="btn btn-info btn-block">snippets/product-form.liquid</button>
                            <br><br>
                        
                            <!-- <textarea id="p1" class="col-xs-12 js-copytextarea" rows="3">
                              {% render 'primeb', product: product, hideAssets:'0', primebGroup:'1', primebOuterClass:'', primebOuterStyle:'', primebInnerStyle:'', primebInnerClass: 'prime-d-inline-block prime-mr-1 prime-mb-1' %}
                            </textarea>
                            <p>
                              <button class="js-textareacopybtn">Copy</button>
                            </p>

                            <textarea id="p1" class="col-xs-12 js-copytextarea2" rows="3">{% render 'primeh', hideAssets:'0',product: product %}
                            </textarea>
                            <p>
                              <button class="js-textareacopybtn2">Copy</button>
                            </p>

                            <textarea id="p1" class="col-xs-12 js-copytextarea4" rows="3">&lt;!--Prime Banner default---&gt;
                            {% render 'primeban', product: product, primebanGroup:'1', primebanOuterClass:'prime-mb-2 prime-mt-2 prime-text-left', primebanOuterStyle:'', primebanInnerStyle:'', primebanInnerClass: 'prime-d-block prime-mb-1' %}
                            </textarea>
                            <p>
                              <button class="js-textareacopybtn4">Copy</button>
                            </p> -->
                    
                    
                    </div>
                </div>

                <div class="panel panel-default">                   
                    <div class="panel-body">      
                    <h5>Theme Download path is </h5>
  <h5><a href="https://s3.console.aws.amazon.com/s3/buckets/files.thalia-apps.com/prime/themes/" style="word-break: break-all;" >https://s3.console.aws.amazon.com/s3/buckets/files.thalia-apps.com/prime/themes/</a></h5>
                    </div>
                </div>

            </div>
                         <div class="col-md-9">
                                                    
                                        
                             <div class="col-xs-12 col-md-4" style="margin-bottom: 5px;">
                                <button name="analyze" value="analyze" class="btn btn-block btn-info" type="button" onclick="analyzebuttonclick()">Analyze</button>
                             </div>
                              <div class="col-xs-12 col-md-4" style="margin-bottom: 5px;">
                                <button name="getdata" value="getdata" class="btn btn-block btn-info " type="button" onclick="getdatabuttonclick()">Get Data</button>                            
                             </div>
                             <div class="col-xs-12 col-md-4" style="margin-bottom: 5px;">
                                <button name="putnewdata" value="putnewdata" class="btn btn-block btn-danger" type="button" onclick="newdatabuttonclick()">Put New Data</button>                            
                             </div>
                           
                                <div class="col-xs-12">
                                    <textarea rows="12" id="newdata" for="newdata" type="text" name="newdata" class="form-control"></textarea>                                   
                                    <button onclick="copyresult()" class="pull-left btn btn-primary">Copy in text area</button>
                                    <input type="text" id="adminlogid" hidden>
                                </div>                               
                           
                          
                          
                             <div id="result" data-myValue="1" class="col-xs-12">
                  
                            </div>
                </div>
                
               
               
            
               
        
    
   
</div>

<script>
    function getthemes(){
      var userid = $('#userid').val();
      $('#themes').empty();
      if(userid){

        $.ajax({                        
          method: "POST",
          url: "{{ asset('admin/getusertheme') }}",            
          data: "&_token={{csrf_token()}}&userid="+userid,
          // dataType: "html",
          async: "false",                   
          success: function(data) {
            for(i=0; i<data.length; i++){
              console.log(data[i]);
              $('#themes').append('<option value="'+data[i]['id']+'" >'+data[i]['name']+'</option>');
            }
            
          }   

        });
      }

    }

    function AnalyzeKeySelect(value)
    {
      console.log(value);
      var totext = $('.assetvalue_'+value).text();
      document.getElementById('assetkey').value = totext;
    }

    function analyzebuttonclick()
    {
         //clear text area 
          document.getElementById('newdata').value = "";
          var themeid = '';
          themeid = $( "#themes" ).val();
          var themename = ''
          themename = $( "#themes option:selected" ).text();
          if(themeid == null){
            themeid = '';
            themename = '';
          }
          
          $.ajax({                        
                        method: "POST",
                        url: "{{ asset('admin/analyzetheme') }}",            
                        data: "&_token={{csrf_token()}}&userid="+document.getElementById('userid').value+"&themeid="+themeid+"&themename="+themename,
                        dataType: "html",
                        async: "false"                        
                      })
                        .done(function( data ) {                            
                           $('#result').html(data);
                           $('#example').DataTable({
                            "pageLength": 10,
                              responsive: true,
                              dom: '<"search pull-right"f><"bottom"l>rt<"text-center col-xs-12"i><"text-center col-xs-12"p><"clear">',
                              "paging": true,
                              "pagingType": "numbers",
                              "searching" : true,
                              "ordering": false,
                              "bServerSide":false,
                           });
                        });        
    }
     function getdatabuttonclick()
    {
        
        //clear text area 
          document.getElementById('newdata').value = "";
          var themeid = '';
          themeid = $( "#themes" ).val();
          var themename = ''
          themename = $( "#themes option:selected" ).text();
          if(themeid == null){
            themeid = '';
            themename = '';
          }
        
          $.ajax({                        
                        method: "POST",
                        url: "{{ asset('admin/getthemedata') }}",            
                        data: "&_token={{csrf_token()}}&userid="+document.getElementById('userid').value+"&assetkey="+document.getElementById('assetkey').value+"&themeid="+themeid+"&themename="+themename,
                        dataType: "html",
                        async: "false"                        
                      })
                        .done(function( data ) {  
                          var data = jQuery.parseJSON(data);
                          console.log(data.htmldata);
                           $('#result').html(data.htmldata);
                           $('#adminlogid').val(data.id);
                        });        
    }
    
      function newdatabuttonclick()                 
    {
      console.l;
        var result = confirm("Confirm put new data?");
        if (result) {  

          var themeid = '';
          themeid = $( "#themes" ).val();
          var themename = ''
          themename = $( "#themes option:selected" ).text();
          if(themeid == null){
            themeid = '';
            themename = '';
          }
          var adminlogid = $("#adminlogid").val();
          $.ajax({                        
                        method: "POST",
                        url: "{{ asset('admin/putnewdata') }}",            
                        data: "&_token={{csrf_token()}}&userid="+document.getElementById('userid').value+"&assetkey="+document.getElementById('assetkey').value+"&newdata="+encodeURIComponent(document.getElementById('newdata').value)+"&themeid="+themeid+"&themename="+themename+"&adminlogid="+adminlogid,
                        dataType: "html",
                        async: "false"                        
                      })
                        .done(function( data ) {                            
                           $('#result').html(data);
                        });        
        }
    }
    
    function writeproductliquid()
    {
        document.getElementById('assetkey').value = "templates/product.liquid";
    }
    
     function writeproducttemplateliquid()
    {
        document.getElementById('assetkey').value = "sections/product-template.liquid";
    }
     function writeformtemplateliquid()
    {
        document.getElementById('assetkey').value = "snippets/product-form.liquid";
    }
    function copyresult()
    {
        var html = document.getElementById("filedatapre").innerHTML;         
      //  alert(decodeHtml(html));
        document.getElementById('newdata').value = decodeHtml(html);
    }
    
    function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}


    var copyTextareaBtn = document.querySelector('.js-textareacopybtn');
    var copyTextareaBtn2 = document.querySelector('.js-textareacopybtn2');
     var copyTextareaBtn3 = document.querySelector('.js-textareacopybtn3');
     var copyTextareaBtn4 = document.querySelector('.js-textareacopybtn4');

    copyTextareaBtn.addEventListener('click', function(event) {
      var copyTextarea = document.querySelector('.js-copytextarea');
      copyTextarea.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }
    });
    
     copyTextareaBtn2.addEventListener('click', function(event) {
      var copyTextarea = document.querySelector('.js-copytextarea2');
      copyTextarea.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }
    });
    
    // copyTextareaBtn3.addEventListener('click', function(event) {
    //   var copyTextarea = document.querySelector('.js-copytextarea3');
    //   copyTextarea.select();

    //   try {
    //     var successful = document.execCommand('copy');
    //     var msg = successful ? 'successful' : 'unsuccessful';
    //     console.log('Copying text command was ' + msg);
    //   } catch (err) {
    //     console.log('Oops, unable to copy');
    //   }
    // });

    copyTextareaBtn4.addEventListener('click', function(event) {
      var copyTextarea = document.querySelector('.js-copytextarea4');
      copyTextarea.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }
    });
    
    
</script>
@stop