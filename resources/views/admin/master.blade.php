<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> 
<head>
    @yield('title')
    <meta charset="utf-8" />        
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />  
    <meta content="" name="author" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" > -->

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css
">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script> -->

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/adapters/jquery.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" /> 
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

   <!-- Stylesheet -->
   <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.1/styles/github.min.css" />
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdn.jsdelivr.net/npm/diff2html/bundles/css/diff2html.min.css"/>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/diff2html/bundles/js/diff2html-ui.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jsdiff/4.0.0/diff.min.js" integrity="sha512-zWaNyvugUw0v2iiN6Rswz5Wk9KRmy74Mw9s7rKj1Nwlw+snemXfZWdPqe6ReKZbR5jplG1wnUr99NXYJeyJk6A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jsdiff/4.0.0/diff.js" integrity="sha512-fwHYlmColD6w/mx6W4/8n0BwKyh26WI0dDOV8A7myI6l94sOFSwndBgOmqlXLpgnLQZ8ZvOisvBAq2UDF1wS9w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @yield('css')

    

</head>

<body>  

    <input type="hidden" id="base_url" value="{{ Config::get('app.base_url') }}">
    <input type="hidden" id="business_base_url" value="{{ Config::get('app.business_base_url') }}">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{asset('')}}admin/dashboard">Prime</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <li class="@if($param['activeMenu'] == 'userlist') active @endif" >
                        <a href="{{asset('admin/buserslist')}}">
                            User list<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="@if($param['activeMenu'] == 'analyzetheme') active @endif" >
                        <a href="{{asset('admin/analyzetheme')}}">
                            Analyze Theme<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="@if($param['activeMenu'] == 'bagdes') active @endif" >
                        <a href="{{asset('admin/badgeslist')}}">
                        Badges List<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="@if($param['activeMenu'] == 'highlights') active @endif" >
                        <a href="{{asset('admin/highlightslist')}}">
                            Highlights List<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="@if($param['activeMenu'] == 'userreview') active @endif" >
                        <a href="{{asset('admin/userreviewlist')}}">
                        User Review List<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="@if($param['activeMenu'] == 'showcase') active @endif" >
                        <a href="{{asset('admin/showcaselist/badge')}}">
                        Showcase<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="@if($param['activeMenu'] == 'history') active @endif" >
                        <a href="{{asset('admin/history')}}">
                        History<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    {{-- <li class="@if($param['activeMenu'] == 'cronstatus') active @endif" ><a href="{{asset('admin/cronstatus')}}">Cron status<span class="sr-only">(current)</span></a></li>
                    <li class="@if($param['activeMenu'] == 'userlist') active @endif" >
                            <a href="{{asset('admin/runningtasks')}}">
                                Tasks Inprogress<span class="sr-only">(current)</span>
                            </a>
                        </li> --}}
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{session('email')}} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{asset('admin/logout')}}">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="container-fluid">
        @yield('content')

        <div id="loadingalert" style="top:0; left:0; height: 100vh; width: 100vw; position: fixed; z-index: 999999999999;opacity: 0.8;" class="position-fixed navbar collapse">
            <div class="alert  m-auto text-center m-auto" style=" width:300px; z-index: 999999999999; margin-top:20%;margin-left: 40%;background-color: black;">
                <h3 class=" mb-0" style="color:white;">
                <!-- <span class="fa fa-sync-alt fa-2x fa-spin"></span> -->
                Theme Downloading please wait ...
                </h3>
            </div>
        </div>
    </div>

    
<!-- <footer class="container-fluid" style="text-align: center;background-color: #e7e7e7;">
  <h5>Theme Download path is </h5>
  <h5><a href="https://s3.console.aws.amazon.com/s3/buckets/files.thalia-apps.com/prime/themes/">https://s3.console.aws.amazon.com/s3/buckets/files.thalia-apps.com/prime/themes/</a></h5>
</footer>
     -->
    
</body>
</html>