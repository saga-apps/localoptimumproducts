@extends('admin.master')

@section('css')
@parent
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

#usersList_paginate{
    text-align: center;
}

</style>
@stop

@section('js')
@parent
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
@stop

@section('title')
@parent
<title>MWS Dashboard</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent
<?php 
    $arrPlanNameById = array( 1 =>'Trial',2=>'Basic',3=>'Pro');
?>
<!-- <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
        <input id="searchbox" type="search" class="form-control" value="{{$param['query']}}">
        <br>
    </div>
</div> -->
<div class="row">
    <div class="col-xs-12 col-sm-12 ">

      <table id="usersList" class="table table-bordered table-responsive table-striped text-center" cellspacing="0" width="100%">
    <thead>
        <tr>                               
            <th class="text-center">Badgeid</th>
            <th class="text-center">Badges</th>
            <th class="text-center">Status</th>
            <th class="text-center">Userid</th>  
            <!-- <th class="text-center">IsInstalled</th>
            <th class="text-center">IsPaymentOk</th>
            <th class="text-center">IsReview</th>
            <th class="text-center">LastInstalledOn</th>  -->
            <th class="text-center">Action</th>
        </tr>
    </thead>  
        <tbody>
        
        @if(count($bagdes) > 0)
            @foreach($bagdes as $bagde)
                <tr>
                    <td class="vcenter">{{ $bagde->productbadgeid }}</td>
                    <td class="vcenter">
                    @if($bagde->badge_type == 'image')
                      @if( isset($bagde->imgsrc))
                        <img src="{{ isset( $bagde->imgsrc ) ? $bagde->imgsrc :'' }}" style="max-height:20px; " />
                      @else
                        <img src="{{ isset( $bagde->productbadgeid ) ? env('AWS_PATH').$bagde->productbadgeid :'' }}" style="max-height:20px; " />
                      @endif
                    @else 
                        {{isset( $bagde->title ) ? $bagde->title : ''}}
                    @endif
                    </td>
                    <td class="vcenter">{{ $bagde->isactive }}</td>
                    <td class="vcenter">{{ $bagde->userid }}</td>
                    <td class="vcenter">

                        <a href="{{asset('')}}autologinbadge/{{ $bagde->userid }}" target="_blank" class="btn btn-default btn-xs">Login</a>
                        
                    </td>
                   
                </tr>

            @endforeach


        @endif
         </tbody>
    </table>
    <center>
              {{ $bagdes->links() }}
                  
    </center>

    </div>
</div>


<script type="text/javascript">

    $('input[type=search]').on('keydown', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            window.location = "{{asset('')}}admin/dashboard?query="+encodeURIComponent(document.getElementById("searchbox").value.trim());
        }
    });

    // var table = $('#usersList').DataTable({
                
    //             "pageLength": 20,
    //             responsive: true,
    //             dom: '<"search pull-right"f><"bottom"l>rt<"text-center col-xs-12"i><"text-center col-xs-12"p><"clear">',
    //             "paging": true,
    //             "pagingType": "numbers",
    //             "searching" : true,
    //             "ordering": false,
    //             "bServerSide":false,
    //             // "bInfo" : true,
    //         });


</script>
@stop