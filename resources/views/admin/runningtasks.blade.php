@extends('admin.master')

@section('css')
@parent

@stop

@section('js')
@parent
<!-- <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>-->
@stop

@section('title')
@parent
<title>MWS Dashboard</title>
@stop

@section('description')
@parent
<meta content="InstaShop - Built custom affiliate shop for your audience" name="description" />
@stop

@section('content')
@parent

<table class="table" style="width: 30%">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Task</th>
        <th scope="col">Userid</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($tasks as $task)
        <tr>
        <th scope="row">{{$task->taskid}}</th>
            <td>{{$task->name}}</td>
            <td>{{$task->userid}}</td>
            @if($task->type == 6 || $task->type == 7)
            <td>
                <a href="{{asset('forceresume')}}/{{$task->taskid}}" class="btn btn-outline-danger">Restart</button>
            </td>
            @else
            <td> <a href="{{asset('admin/pulltaskaction')}}/{{$task->userid}}/{{$task->taskid}}" class="btn btn-outline-danger">Restart</button> </td>
            @endif
          </tr>
        @endforeach
     
     
    </tbody>
  </table>    
        <center>
                 {{ $tasks->links() }}
                      
        </center>

    </div>
</div>
<script type="text/javascript">

$('input[type=search]').on('keydown', function(e) {
    if (e.which == 13) {
        e.preventDefault();
        window.location = "{{asset('')}}admin/dashboard?query="+encodeURIComponent(document.getElementById("searchbox").value.trim());
    }
});
    </script>
@stop