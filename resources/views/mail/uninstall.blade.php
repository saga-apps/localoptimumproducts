<html>
    <body>
        Hey,<br><br>

This is Gaurav the developer of Prime App.<br><br>

Our system detected that you just uninstalled the app.<br><br>

Our app isn't perfect yet, but we believe that your valuable feedback could help us get there. Let us know if it’s the performance of the app that’s causing the problem, or is it the app's pricing, customer support, a missing feature or something else?<br><br>

I’d love to personally review it and we'll work hard to solve your problem.<br><br>

Just reply to this email.<br><br>

Looking forward to your reply. :)<br><br>

Gaurav<br>
Prime Product Badges App
    </body>
</html>