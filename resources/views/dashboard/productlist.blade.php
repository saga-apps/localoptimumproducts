<table id="productlist" class="table table-bordered table-striped text-left reduced-margin-bottom" cellspacing="0" width="100%">
    <thead>
        <tr class="text-center">

            <!--      <th scope="col"></th>-->
            <th scope="col ">Products</th>
            <th scope="col " >Action</th>

        </tr>
    </thead>

    <colgroup>
        <col width="75%">
            <!--<col width="15%">-->
            <col width="25%">

    </colgroup>

    <tbody id="">
        @if( !empty( $products ) ) @foreach( $products as $product )
        <?php 

        $isBlackList = false;

        if( !empty( $arrProductId ) & in_array( $product['id'], $arrProductId ) ){
            $isBlackList = true;
        }

        ?>
        <tr class="text-center">
            <td>
                <div class="row">
                    <div class="col-4 col-md-2 text-center">
                        <img style="max-height: 40px;" alt="" src="{{ !empty( $product['images'][0]['src'] ) ? $product['images'][0]['src'] :'' }}" class="mr-2 img-fluid img-responsive">

                    </div>
                    <div class="col-8 col-sm-10 col-md-10 text-left">
                        <a onclick='window.open("https://{{Auth::User()->shop}}/products/{{$product['handle']}}");return false;'  href="javascript:void(0);">{{trim( $product['title'] ) }}</a>

                    </div>
                </div>
            </td>

            <td>
                {{-- <div class="btn-group productall">
                <button class="btn btn-sm btn-outline-secondary" productid="{{$product['id']}}"  handle="{{$product['handle']}}" producttitle="{{$product['title']}}" productimage="{{ !empty( $product['images'][0]['src'] ) ? $product['images'][0]['src'] :'' }}"  onchange="addProduct(this,'{{$product['id']}}')"  >

                </button>
                
              </div> --}}

              <div class="btn-group productall">
                    <button {{ is_array( $arrProducts ) && in_array( $product['id'] , $arrProducts ) ? "added=1" :"" }} productid="{{$product['id']}}"  handle="{{$product['handle']}}" producttitle="{{$product['title']}}" productimage="{{ !empty( $product['images'][0]['src'] ) ? $product['images'][0]['src'] :'' }}"  onclick="addProduct(this,'{{$product['id']}}')" class="btn csv_button btn-sm {{ is_array( $arrProducts ) && in_array( $product['id'] , $arrProducts ) ? "btn-outline-danger" :"btn-outline-secondary" }}" title="Edit product parameters">
                        {{ is_array( $arrProducts ) && in_array( $product['id'] , $arrProducts ) ? "Remove" :"Select" }}
                        
                    </button>

                </div>


                {{-- <div class="btn-group">
                    <button   class="btn btn-sm btn-outline-secondary" title="Edit product parameters">
                        Add Product
                    </button>

                    

                </div> --}}
                {{-- <span id="statusloadermark-{{$product['id']}}" class="text-danger text-left" style="display: none;"> &nbsp;&nbsp;&nbsp;<img id="" class="loadermark" src="{{asset('img/ajax-loader.gif')}}" align="middle"></span>
                <span id="successmark-{{$product['id']}}" class="fa fa-ok-sign text-success successmark" style="display: {{ is_array( $arrProducts ) && in_array( $product['id'] , $arrProducts ) ? "" :"none" }};"> <i class="fa fa-check" aria-hidden="true"></i> </span>
                <span id="failmark-{{$product['id']}}" class="fa fa-remove-sign text-danger failmark" style="display: none;"> <i class="fa fa-times" aria-hidden="true"></i> </span> </span> --}}
            </td>

        </tr>
        @endforeach @endif
    </tbody>
</table>

@if( !empty( $csv ) )
    <script>

        $( document ).ready(function() {

            addProduct('test','1','clear');

            $( ".csv_button" ).trigger( "click" );

            var current= <?php echo json_encode($products); ?>;

            var count = current.length;

            $(".count_by_csv").text( count );
           
            
           
        });

    </script>
@endif
