<table id="collectionlist" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr class="text-center">
        <th scope="col ">Collections</th>
        <th scope="col " >Action</th>
        </tr>
    </thead>
    <colgroup>
        <col width="75%">
        <col width="25%">
    </colgroup>
    <tbody id="">
        @if( !empty( $collections ) ) @foreach( $collections as $collection )
        <tr class="text-center {{ is_array( $arrCollections ) && in_array( $collection['id'] , $arrCollections ) ? "prime_selected" :"prime_not" }}">
            <td>
            <div class="row">
                <div class="col-12 col-md-12 text-center"><a onclick='window.open("https://{{Auth::User()->shop}}/collections/{{$collection['handle']}}");return false;'  href="javascript:void(0);">
                {{ $collection['title'] }}
                </div>
            </div>
            </td>

            <td>
            <div class="btn-group collectionall">
                <button {{ is_array( $arrCollections ) && in_array( $collection['id'] , $arrCollections ) ? "added=1" :"" }} collectionid="{{$collection['id']}}" collectiontitle="{{$collection['title']}}" onclick="addCollections(this,'{{$collection['id']}}')" class="btn btn-sm {{ is_array( $arrCollections ) && in_array( $collection['id'] , $arrCollections ) ? "btn-outline-danger" :"btn-outline-secondary" }} " title="Edit Collection parameters">
                {{ is_array( $arrCollections ) && in_array( $collection['id'] , $arrCollections ) ? "Remove" :"Select" }}
                </button>

            </div>
            </td>

        </tr>
        @endforeach @endif
    </tbody>
</table>