
    <div class="w-100">
        @if($rawerrors->total() != 0)
        <table id="updatedproductlist" class="table table-bordered table-striped text-left table-responsive" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-left">Errors</th>                            
                    
                </tr>
            </thead>
            <colgroup>
                <col width="10%" />
                <col width="80%" />
                                  
            </colgroup>
            <tbody class="text-center">
                @if( !empty( $errors ) ) @foreach( $errors as $index => $product )

                <?php
                if(isset($product['createdate'])) {
                     $t1 = new DateTime( date("Y-m-d H:i:s",strtotime( $product['createdate'] ) ), new DateTimeZone('UTC') );

        $now = new DateTime();
        //log::info($t1->diff($now)->format("%d days %h hrs %i mins"));

        $lastsync = array(
            'timedifference' => $t1->diff($now)->format("%d days %h hrs %i mins"),
            'timestring' => $t1->format('d M Y h:m:s')." UTC"
        );

        if( !empty( $product['pusheddate'] ) ){
        $t2 = new DateTime( date("Y-m-d H:i:s",strtotime( $product['pusheddate'] ) ), new DateTimeZone('UTC') );

        $now = new DateTime();
        //log::info($t1->diff($now)->format("%d days %h hrs %i mins"));
        $lastsync['pusheddate'] = $t2->diff($now)->format("%d days %h hrs %i mins");




        $t2 = new DateTime( date("Y-m-d H:i:s",strtotime( $product['syncdate'] ) ), new DateTimeZone('UTC') );

        $lastsync['syncdate'] = $t2->diff($now)->format("%d days %h hrs %i mins");

                }
        
        }

    ?>

                    <tr>
                        <td><img id="img-1442249080932" src="{{$product['imageurl'] or ''}}" height="40"></td>
                        <td class="text-left">
                            <div class="edit-title-container text-left">
                                   <p> {{$product['title'] or ''}} </p>
                                @if(is_array($product['error']) )
                                    @foreach ($product['error'] as $ekey => $error)
                                        <p class="text-danger">{{$ekey}} : {{$error}} </p>
                                    @endforeach
                                    
                                @else 
                                <p class="text-danger">{{$product['error']}} </p>
                                @endif
                               
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <table class="table table-bordered mt-2 mb-0 bg-light">

                                            <tbody class="small">
                                                @if(isset($product['asin']))
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">ASIN</td>
                                                    <td class="px-1 py-0 bg-light">{{$product['asin'] or ''}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">SKU</td>
                                                    <td class="px-1 py-0 bg-light">{{$product['sellersku'] or ''}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">Fulfilled By</td>
                                                    <td class="px-1 py-0 bg-light">{{isset($product['fulfillment']) && $product['fulfillment'] == 0 ? 'Merchant':'Amazon'}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">VaritantID</td>
                                                    <td class="px-1 py-0 bg-light">{{ $product['shopifyvariantid'] or ''}}</td>
                                                </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <table class="table table-bordered mt-2 mb-0 bg-light">

                                            <tbody class="small">
                                            @if(isset($lastsync))
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">Pulled</td>
                                                    <td class="px-1 py-0 bg-light">{{ $lastsync['timedifference']}} ago</td>
                                                </tr>
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">Pushed</td>
                                                    <td class="px-1 py-0 bg-light">@if( !empty( $lastsync['pusheddate'] ) ) {{ $lastsync['pusheddate'] }} ago @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="px-1 py-0 bg-light">Synced</td>
                                                    <td class="px-1 py-0 bg-light">{{isset($lastsync['syncdate']) && $lastsync['syncdate'] != "" ? $lastsync['syncdate'].' ago' : '-' }}</td>
                                                </tr>
                                                <tr>
                                                    @if( $product['ispushed'] == 1 )
                                                    <td class="px-1 py-0 bg-light">Shopify</td>
                                                    <td class="px-1 py-0 bg-light"><a href="shopify.com" target="_blank">View</a> | <a href="shopify.com" target="_blank">Edit</a></td>
                                                    @endif
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </td>

                        <td>
                            
                        </td>
                    </tr>
                    @endforeach @endif
            </tbody>
        </table>
       
        <div class="justify-content-center">

                <center>
                    Activity completed with {{ $rawerrors->total() }} errors<br>
                    {{$rawerrors->render("pagination::bootstrap-4")}}
                    
                    <br>
                    <br>
        
                </center>
        
            </div>
            @else

            <div class="justify-content-center">

                    <center>
                      Activity completed with zero errors.
            
                    </center>
            
            </div>


            @endif

    </div>
    

  




