@extends('layouts.master')


@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')

@parent

<style>

</style>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css
">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

@stop
 
@section('content')
@parent


<div class="row ">

    <div id="sidebar" class="col-xs-12 col-sm-2">
        <div class="list-group mt-3">
            <a class="list-group-item {{$activeMenu =='ordersetting' ? 'active':'' }}" href="/settings-order-count">Orders</a>
            <a class="list-group-item {{$activeMenu =='deliverydate' ? 'active':'' }}" href="/settings-delivery-date">Estimated Delivery</a>
            <a class="list-group-item {{$activeMenu =='livecount' ? 'active':'' }}" href="/settings-livevisitcount">Visitor Count</a>
        </div>
    </div>

    @section('title')
        @parent
        <title>Visitor Count Settings</title>
        @stop
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7 mt-3">
            @if(Auth::User()->planid < 4)
                <div class="row">
                    <div class="col-md-12">
                        <p class="alert alert-danger w-100">This feature is available only on the Gold Plan.
                            <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
                        </p>
                    </div>
                </div>
            @endif

            <div class="form-group row">
                <div id="userreview" class="col-md-12 mt-1">
                    <div class="card">
                        <div class="card-header">Visitor Count Settings</div>
                    <div class="card-body">   
                        <div class="form-group row badge_text_show">
                            <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Visitor Count Timeframe </label>
                            <div class="col-12 col-sm-5 col-md-4">
                                <select id="livecounttime" class="form-control" name="livecounttime" >
                                    <option value="1" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '1') ? 'selected' :''}}>1 min</option>
                                    <option value="2" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '2') ? 'selected' :''}}>5 min</option>
                                    <option value="3" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '3') ? 'selected' :''}}>10 min</option>
                                    <option value="4" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '4') ? 'selected' :''}}>30 min</option>
                                    <option value="5" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '5') ? 'selected' :''}}>1 hr</option>
                                    <option value="6" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '6') ? 'selected' :''}}>6 hr</option>
                                    <option value="7" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '7') ? 'selected' :''}}>12 hr</option>
                                    <option value="8" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '8') ? 'selected' :''}}>1 day</option>
                                    <option value="9" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '9') ? 'selected' :''}}>1 week</option>
                                    <option value="10" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '10') ? 'selected' :''}}>15 days</option>
                                    <option value="11" {{ (isset(Auth::User()->livecounttime) && Auth::User()->livecounttime == '11') ? 'selected' :''}}>30 days</option>

                                </select>   
                            </div>
                        </div>
                        <div class="form-group row badge_text_show">
                            <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Show badge when visitor count is greater than </label>
                            <div class="col-12 col-sm-5 col-md-4">
                                <input type="number" min="0" id="setlivevisitcount" name="setlivevisitcount" value="{{ (isset(Auth::User()->setlivevisitcount))  ? Auth::User()->setlivevisitcount : '0' }}" class="form-control mt-3" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >   
                            </div>
                        </div>
                        <hr>
                        @if(Auth::User()->planid > 3)
                        <div class="col-12"><a class="btn btn-primary float-right" id="updatenow" href="JavaScript:Void(0);">Save</a></div>
                        @else
                        <div class="col-12"><a class="btn btn-primary float-right" href="JavaScript:Void(0);">Save</a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        </div>
  
        <div class="col-12 col-sm-3 mb-2 mt-3">
            <div class="card bg-light">
                <!-- <div class="card-body">
                    <div>
                        <div class="font-weight-bold">Live Visitor Count</div>
                        <div class="small mt-1">
                            Increase social proof by displaying badges with a counter of total orders for any given product. Example:
                            <br>
                            <div class="badge badge-success my-2 p-2">987 sold | only 13 left</div>
                            <br>
                            Configure the app to auto calculate orders for each product, or simply provide the count manually. 
                            <div class="mt-1">
                                <a href="https://getinstashop.freshdesk.com/support/solutions/articles/81000385290-order-count" target="_blank" class="">Know More<span class="fa fa-caret-right ml-1"></span></a>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="card-body">
                    <div>
                        <div class="font-weight-bold">Visitor Counter</div>
                        <div class="small mt-1">
                            Create urgency by displaying badges with a count of recent/live visitors for any given product. Example:
                            <br>
                            <div class="badge badge-success my-2 p-2">25 visitors in last 5 minutes</div>
                            <br>Configure the timeframe for calculating the visitor count
                            <div class="mt-1">
                                <a href="https://getinstashop.freshdesk.com/support/solutions/articles/81000390548-visitor-counter" target="_blank" class="">Know More<span class="fa fa-caret-right ml-1"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- <div class="card bg-light mt-2">
                <div class="card-body">
                    <div>
                        <div class="font-weight-bold">Recent Order Count</div>
                        <div class="small mt-1">Highlight trending products by displaying the count of recent orders.<br>
                            <div class="badge badge-danger my-2 p-2">Trending Alert | 77 sold recently</div>
                            <br>The app will auto calculate the recent orders for each product based on the lookback period configured.
                        </div>
                    </div>
                </div>
            </div> -->
            
        </div>
    </div>
</div>

@stop

@section('scripts')
@parent

<script type="text/javascript">

  $('#updatenow').on('click', function(e) {

    var livecounttime = $("#livecounttime").val();
    var setlivevisitcount = $("#setlivevisitcount").val();
    if(setlivevisitcount == ''){
        setlivevisitcount = 2;
    }

    $('#loadingalert').show();
    $.ajax({
      method  :'POST',
      url     :'{{ asset("livevisitcount")}}/'+livecounttime+'/'+setlivevisitcount,
      data: {
          _token: '{{csrf_token()}}' 
      },
      success: function(response){
        console.log(response);
        if(response=="done")
        $('#loadingalert').hide();
        $('#requestmessage').text("Time Updated Successfully! ");
        toggleAlert();
        setTimeout(function(){ 
          location.reload();
        }, 2000);
      },
      error: function(error){

        console.log(error);
        $('#loadingalert').hide();
        $('#requestmessage').text("Something Went Wrong");
        toggleAlert();

      }

    });

  });


</script>

@stop
