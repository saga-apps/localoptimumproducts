@extends('layouts.master')


@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')

@parent

<style>

  .dataTables_filter{
    float: right !important;
  }

  .table-responsive {
        overflow-x: hidden !important;
  }

  .paginate_button{
      border-color: #ffc107;
  }
  .dataTables_wrapper .dataTables_paginate  {    
    /* border: 1px solid #d39e00 !important; */
    background-color: #e0a800 !important;
    background: none !important;
    color: #ffc107 !important;
    }   
  .pagination .active {
    padding: 5px;
    background-color: #ffc107 !important;
  }

  a.paginate_button:hover {
    background-color: #ffc107 !important;
    color: #ffc107 !important;
  }
  
  .active a{
    color: #fff !important;
  } 

  .dataTables_filter{
    padding: 0 0 0 65%;
  }
  .dataTables_paginate{
      margin:0 auto !important;
  }

  @media (max-width: 767px) { 
    div.dataTables_paginate ul.pagination{
      max-width: 85%;
    }
    .dataTables_filter{
      padding: 0 0 0 50%;
    }
    div.dataTables_filter input{
      max-width: 45%;
    }
  }


  /* .paging_numbers{
    min-width:100%;
  } */

  
/* .pagination{
  padding: 0 0 0 184px;
} */
/* #orderslist_wrapper.form-inline{display: block !important;} */
/* #orderslist_wrapper.form-inline{overflow-y: none !important;} */
</style>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css
">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

@stop
 
@section('content')
@parent


<div class="row ">

  <div id="sidebar" class="col-xs-12 col-sm-2">
    <div class="list-group mt-3">
        <a class="list-group-item {{$activeMenu =='ordersetting' ? 'active':'' }}" href="/settings-order-count">Orders</a>
        <a class="list-group-item {{$activeMenu =='deliverydate' ? 'active':'' }}" href="/settings-delivery-date">Estimated Delivery</a>
        <a class="list-group-item {{$activeMenu =='livecount' ? 'active':'' }}" href="/settings-livevisitcount">Visitor Count</a>
    </div>
  </div>

  @section('title')
    @parent
    <title>Order Count Settings</title>
    @stop
  <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
    @if(Auth::User()->planid < 3)

      <div class="row">
        <div class="col-md-12 mt-3">
          
          <p class="alert alert-danger w-100">This feature is available only on the Pro Plan.
            <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
          </p>
                
        </div>
      </div>

    @endif

    <div class="form-group row">
      <div id="userreview" class="col-md-12 mt-1">
        <div class="card">
          
          <div class="card-header">Sync orders <span class="float-right small text-muted">Last updated on {{ (isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '') ? date("dS F Y", strtotime(Auth::User()->orderupdatetime)) : ' - never'}}</span>
          </div>

          <div class="card-body">   
            <div class="form-group row badge_text_show">
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Recent Order Lookback </label>
              <div class="col-12 col-sm-5 col-md-4">
                <select id="recentcount" class="form-control" name="recentcount" >
                  <option value="1" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '1') ? 'selected' :''}}>1 day</option>
                  <option value="3" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '3') ? 'selected' :''}}>3 days</option>
                  <option value="7" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '7') ? 'selected' :''}}>7 days</option>
                  <option value="10" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '10') ? 'selected' :''}}>10 days</option>
                  <option value="15" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '15') ? 'selected' :''}}>15 days</option>
                  <option value="21" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '21') ? 'selected' :''}}>21 days</option>
                  <option value="30" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '30') ? 'selected' :''}}>30 days</option>
                  <option value="45" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '45') ? 'selected' :''}}>45 days</option>
                  <option value="60" {{ (isset(Auth::User()->recentcountdays) && Auth::User()->recentcountdays == '60') ? 'selected' :''}}>60 days</option>
                </select>   
              </div>
            </div>
            <hr>
            @if(Auth::User()->planid > 2)
            <div class="col-12"><a class="btn btn-primary float-right" id="updatenow" href="JavaScript:Void(0);">Update Now</a></div>
            @else
            <div class="col-12"><a class="btn btn-primary float-right" href="JavaScript:Void(0);">Update Now</a></div>
            @endif
            <div class="small text-muted float-right col-12 text-right mt-3 col-md-9 offset-md-3">This will read orders from Shopify and update <b>total order count</b> and <b>recent order count</b> for each product.</div>
            <!-- <a class="btn btn-primary " id="updatenow" href="JavaScript:Void(0);">Update Now</a> -->
            <!-- <span class="ml-2 small text-muted">This will read the latest order data and increment order count for all products.</span> -->
          </div>
          
        </div>
      </div>
    </div>

  
    
    <div class="card">
      
        <div class="card-header">Edit counter for any product</div>
        <div class="card-body">
            
            <div class="input-group">
            <input id="searchbox" type="search" class="form-control" value="" placeholder="Search product title with minimum 3 characters">
            <div class="input-group-append">
              <button id="searchItem"  class="btn btn-primary" type="button">Search</button>
            </div>
          </div>           
             
      <div class="mt-3 text-center" id="OrdertableDataModel">
              
      </div>
    
        </div>
    </div>

   
        
    
   


  </div>

  

  <div class="col-12 col-sm-3 mb-2 mt-3">
    <div class="card bg-light">
      <div class="card-body">
        <div>
        <div class="font-weight-bold">Order Count</div>
          <div class="small mt-1">
            Increase social proof by displaying badges with a counter of total orders for any given product. Example:
            <br>
            <div class="badge badge-success my-2 p-2">987 sold | only 13 left</div>
            <br>
            Configure the app to auto calculate orders for each product, or simply provide the count manually. 
            <div class="mt-1">
              <a href="https://getinstashop.freshdesk.com/support/solutions/articles/81000385290-order-count" target="_blank" class="">Know More<span class="fa fa-caret-right ml-1"></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="card bg-light mt-2">
      <div class="card-body">
        <div>
          <div class="font-weight-bold">Recent Order Count</div>
          <div class="small mt-1">Highlight trending products by displaying the count of recent orders.<br>
            <div class="badge badge-danger my-2 p-2">Trending Alert | 77 sold recently</div>
            <br>The app will auto calculate the recent orders for each product based on the lookback period configured.
          </div>
        </div>
      </div>
    </div>


  <!-- <div class="col-xs-12 col-sm-3">
  </div> -->


</div>
</div>

@stop

@section('scripts')
@parent

<script type="text/javascript">

  $(document).ready(function(){
  });

  $("#searchItem").click( function(){
    searchbox = $("#searchbox").val().trim();
    if (searchbox.length < 3) {
      return false;
    }
    $('#loadingalert').show();
    var exproductidsValue = '';
    getOrderProducts( '',searchbox ,exproductidsValue );
  });

  function getOrderProducts( param ='', searchbox='' , strProduct='' ){
    $("#OrdertableDataModel").html('');
    
    var page =1;
    var exproductidsValue = '';
    var filterType = 0;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
      method:"GET",
      url : '{{asset('shopify-orderproducts')}}?page='+page+'&searchbox='+searchbox+'&products='+exproductidsValue+"&filterType="+filterType,
      dataType: 'html',
      success : function( data ){
        $('#loadingalert').hide();
        $('#OrdertableDataModel').html( data );
        Producttable = $('#orderslist').DataTable({
            dom: "Bfrtip",
            responsive: true,
            "paging": true,
            "pagingType": "numbers",
            "searching" : false,
            "ordering": false,
            "bServerSide":false,
            "bInfo" : false,
            "pageLength": 10,
          });
          $('#orderslist').removeClass("dataTable no-footer");
      }
    });

  }


  function addOrderProduct( instance, productid){
    var ordercount = $('.'+productid).val();
    $('#loadingalert').show();
    $.ajax({
      method  :'POST',
      url     :'{{ asset("saveproductedit" )}}',
      data: {
          productid:productid,
          ordercount:ordercount,
          _token: '{{csrf_token()}}' 
      },
      success: function(response){
      if(response==1)
        $('#loadingalert').hide();
        $("#orderItems").modal('hide');
        $('#requestmessage').text("OrderCount Saved Sucessfully! ");
        toggleAlert();

      },
      error: function(error){
        console.log(error);
        $('#loadingalert').hide();
        $('#requestmessage').text("Something Went Wrong");
        toggleAlert();
      }

    });

  }

  $('#updatenow').on('click', function(e) {

    var recentcount = $("#recentcount").val();

    $('#loadingalert').show();
    $.ajax({
      method  :'POST',
      url     :'{{ asset("userorders")}}/'+recentcount,
      data: {
          _token: '{{csrf_token()}}' 
      },
      success: function(response){
        console.log(response);
        // if(response=="done")
        $('#loadingalert').hide();
        $('#requestmessage').text("Orders Refresh Successfully! ");
        toggleAlert();
        setTimeout(function(){ 
          location.reload();
        }, 2000);

      },

      error: function(error){

        console.log(error);
        $('#loadingalert').hide();
        $('#requestmessage').text("Something Went Wrong");
        toggleAlert();

      }

    });

  });

  // $('#updatenowRecent').on('click', function(e) {

  //   var recentcount = $("#recentcount").val();

  //   $('#loadingalert').show();
  //   $.ajax({
  //     method  :'POST',
  //     url     :'{{ asset("userrecentorders")}}/'+recentcount,
  //     data: {
  //         _token: '{{csrf_token()}}' 
  //     },
  //     // cache:false,
  //     // processData: false,
  //     // contentType:false,
  //     success: function(response){
  //       console.log(response);
  //       if(response=="done")
  //       $('#loadingalert').hide();
  //       $('#requestmessage').text("Orders Refresh Successfully! ");
  //       toggleAlert();
  //       setTimeout(function(){ 
  //         location.reload();
  //       }, 2000);

  //     },

  //     error: function(error){

  //       console.log(error);
  //       $('#loadingalert').hide();
  //       $('#requestmessage').text("Something Went Wrong");
  //       toggleAlert();

  //     }

  //   });

  // });


</script>

@stop
