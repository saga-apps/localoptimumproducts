@extends('layouts.master') @section('title') @parent
<title>{{ Request::is('editnote*') ? "Edit note" :"Create note" }}</title>
@stop @section('description') @parent
<meta content="" name="description" />
@stop
@section('css')
 @parent

@stop
@section('js')
@parent

<style>

#Countrylist_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}
  #Countrylistadded_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}

  .dataTables_filter{
    float: right !important;
  }

    .accordion .card {
          overflow: visible;
  }
  .table-responsive {
        overflow-x: hidden !important;
  }
  .sp-replacer {  
      padding: 8px  !important;
      color: #212529 !important;
      height: 38px !important;
      border: 1px solid #ced4da !important;
      /*border-radius: .25rem !important;*/
      width: 100%;
  }
  .bootstrap-tagsinput input {
      display: block;
      width: 100%;
      padding: .375rem .75rem;
      font-size: 1rem;
      line-height: 1.5;
      color: #495057;
      background-color: #fff;
      background-clip: padding-box;
      border: 1px solid #ced4da;
      border-radius: .25rem;
      transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
      min-height: 28px;
  }
  .intro{
    display: none !important;
  }

  .metanameval{
    padding-left:0;
  }

  #textStyle {
      white-space: pre-wrap;
  }

  #wrapper {
    padding-left: 0;
   
    
  }

  
 .sidebar-nav li {
    text-indent: 15px;
    line-height: 40px;
    padding-right:14px;
    
 }

 ul {
  list-style-type: none;
 }

  .vl {
    display:inline-block;
    border-left: 1px solid;
    height: 400px;
    color:#dee2e6;
    float:left;
  }

  #colorval{

   /* margin-left:20%; */
       margin-top: 1%;

  }

  .column {
    float: left;
    width: 10%;
    padding-right: 15%;
    padding-bottom: 4%;
  }

  .icon-div {
    width: 100%;
    margin: auto;
    padding: 10px;
  }


  #menu a{

    color:black;
    margin-left: -20px;
  }

  .flex-column {
    flex-direction: column!important;
    width: 100px;
    text-align: center;
}

.img-fluid{
  margin-bottom:0% !important;
}
 
  .paginate_button{
      border-color: #ffc107;
  }
  .dataTables_wrapper .dataTables_paginate  {    
    /* border: 1px solid #d39e00 !important; */
    background-color: #e0a800 !important;
    background: none !important;
    color: #ffc107 !important;
}   
  .pagination .active {
    padding: 5px;
    background-color: #ffc107 !important;
  }

  a.paginate_button:hover {
    background-color: #ffc107 !important;
    color: #ffc107 !important;
  }
  
  .active a{
    color: #fff !important;
  } 

  .dataTables_filter{
    padding: 0 0 0 65%;
  }
  .dataTables_paginate{
      margin:0 auto !important;
  }
   .iconbox {   
    border: 1px solid #fff;
}

 .iconbox:hover {
    opacity: 0.75;
    border: 1px solid #ddd;
}

.scrollable-menu {
    height: auto;
    width: 200px !important;
    max-height: 250px;
    overflow-x: hidden;
}


.pcr-button{

  height:2.4em !important;
  width: 3em !important;

}

@media (max-width: 767px) { 
    div.dataTables_paginate ul.pagination{
      max-width: 85%;
    }
    .dataTables_filter{
      padding: 0 0 0 50%;
    }
    div.dataTables_filter input{
      max-width: 45%;
    }
}


.note{display:inline-block;padding:.25em .4em;font-size:75%;font-weight:700;line-height:1;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}
.note-primary{color:#212529;background-color:#ffc107}
a.note-primary:focus,a.note-primary:hover{color:#212529;background-color:#d39e00}
a.note-primary.focus,a.note-primary:focus{outline:0;box-shadow:0 0 0 .2rem rgba(255,193,7,.5)}


</style>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>


<!-- <link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css"> -->
<script src="https://cdn.tiny.cloud/1/wrz7bhvawkamdmyyvadqxu19hm5et399aifwiejk5hhc1fjk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


@stop
@section('content')
@parent

<?php
$inventoryHide = '';
if( !empty( $note ) ) {
  // $display       = json_decode( $note_style['display'] ,true );
  // $displayrules  = json_decode( $note['displayrules'] ,true );
  // $notestyle    = json_decode( $note['notestyle'] ,true );

}

$arrNameById = array( 'Etc/GMT+12' =>'International Date Line West','Pacific/Pago_Pago'=>'American Samoa','Pacific/Midway'=>'Midway Island','Pacific/Honolulu' =>'Hawaii','America/Juneau'=>'Alaska','America/Los_Angeles'=>'Pacific Time','America/Tijuana' =>'Tijuana','America/Phoenix'=>'Arizona','America/Chihuahua'=>'Chihuahua','America/Mazatlan'=>'Mazatlan','Denver'=>'Mountain Time','America/Guatemala'=>'Central America','America/Chicago'=>'Central Time','America/Mexico_City'=>'Mexico City','America/Monterrey'=>'Monterrey','America/Regina'=>'Saskatchewan','America/Bogota'=>'Bogota','America/New_York'=>'Eastern Time','America/Indiana/Indianapolis'=>'Indiana','America/Lima'=>'Quito','America/Halifax'=>'Atlantic Time','America/Caracas'=>'Caracas','America/Guyana'=>'Georgetown','America/La_Paz'=>'La Paz','America/Puerto_Rico'=>'Puerto Rico','America/Santiago'=>'Santiago','America/St_Johns'=>'Newfoundland','America/Sao_Paulo'=>'Brasilia','America/Argentina/Buenos_Aires'=>'Buenos Aires','America/Godthab'=>'Greenland','America/Montevideo'=>'Montevideo','Atlantic/South_Georgia'=>'Mid-Atlantic','Atlantic/Azores'=>'Azores','Atlantic/Cape_Verde'=>'Cape Verde Is.','Africa/Casablanca'=>'Casablanca','Europe/Dublin'=>'Dublin','Europe/London'=>'London','Europe/Lisbon'=>'Lisbon','Africa/Monrovia'=>'Monrovia','Etc/UTC'=>'UTC','Europe/Amsterdam'=>'Amsterdam','Europe/Belgrade'=>'Belgrade','Europe/Berlin'=>'Berlin','Europe/Zurich'=>'Zurich','Europe/Bratislava'=>'Bratislava','Europe/Brussels'=>'Brussels','Europe/Budapest'=>'Budapest','Europe/Copenhagen'=>'Copenhagen','Europe/Ljubljana'=>'Ljubljana','Europe/Madrid'=>'Madrid','Europe/Paris'=>'Paris','Europe/Prague'=>'Prague','Europe/Rome'=>'Rome','Europe/Sarajevo'=>'Sarajevo','Europe/Skopje'=>'Skopje','Europe/Stockholm'=>'Stockholm','Europe/Vienna'=>'Vienna','Europe/Warsaw'=>'Warsaw','Africa/Algiers'=>'West Central Africa','Europe/Zagreb'=>'Zagreb','Europe/Athens'=>'Athens','Europe/Bucharest'=>'Bucharest','Africa/Cairo'=>'Cairo','Africa/Harare'=>'Harare','Europe/Helsinki'=>'Helsinki','Asia/Jerusalem'=>'Jerusalem','Europe/Kaliningrad'=>'Kaliningrad','Europe/Kiev'=>'Kyiv','Africa/Johannesburg'=>'Pretoria','Europe/Riga'=>'Riga','Europe/Sofia'=>'Sofia','Europe/Tallinn'=>'Tallinn','Europe/Vilnius'=>'Vilnius','Asia/Baghdad'=>'Baghdad','Europe/Istanbul'=>'Istanbul','Asia/Kuwait'=>'Kuwait','Europe/Minsk'=>'Minsk','Europe/Moscow'=>'Moscow','Africa/Nairobi'=>'Nairobi','Asia/Riyadh'=>'Riyadh','Europe/Volgograd'=>'Volgograd','Asia/Tehran'=>'Tehran','Asia/Muscat'=>'Muscat','Asia/Baku'=>'Baku','Europe/Samara'=>'Samara','Asia/Tbilisi'=>'Tbilisi','Asia/Yerevan'=>'Yerevan','Asia/Kabul'=>'Kabul','Asia/Yekaterinburg'=>'Ekaterinburg','Asia/Karachi'=>'Karachi','Asia/Tashkent'=>'Tashkent','Asia/Kolkata'=>'New Delhi','Asia/Calcutta'=>'New Delhi','Asia/Colombo'=>'Sri Jayawardenepura','Asia/Kathmandu'=>'Kathmandu','Asia/Almaty'=>'Almaty','Asia/Dhaka'=>'Dhaka','Asia/Urumqi'=>'Urumqi','Asia/Rangoon'=>'Rangoon','Asia/Bangkok'=>'Hanoi','Asia/Jakarta'=>'Jakarta','Asia/Krasnoyarsk'=>'Krasnoyarsk','Asia/Novosibirsk'=>'Novosibirsk','Asia/Shanghai'=>'Beijing','Asia/Chongqing'=>'Chongqing','Asia/Hong_Kong'=>'Hong Kong','Asia/Irkutsk'=>'Irkutsk','Asia/Kuala_Lumpur'=>'Kuala Lumpur','Australia/Perth'=>'Perth','Asia/Singapore'=>'Singapore','Asia/Taipei'=>'Taipei','Asia/Ulaanbaatar'=>'Ulaanbaatar','Asia/Tokyo'=>'Tokyo','Asia/Seoul'=>'Seoul','Asia/Yakutsk'=>'Yakutsk','Australia/Adelaide'=>'Adelaide','Australia/Darwin'=>'Darwin','Australia/Brisbane'=>'Brisbane','Australia/Melbourne'=>'Melbourne','Pacific/Guam'=>'Guam','Australia/Hobart'=>'Hobart','Pacific/Port_Moresby'=>'Port Moresby','Australia/Sydney'=>'Sydney','Asia/Vladivostok'=>'Vladivostok','Asia/Magadan'=>'Magadan','Pacific/Noumea'=>'New Caledonia','Pacific/Guadalcanal'=>'Solomon Is.','Australia/Srednekolymsk'=>'Srednekolymsk','Pacific/Auckland'=>'Wellington','Pacific/Fiji'=>'Fiji','Asia/Kamchatka'=>'Kamchatka','Pacific/Majuro'=>'Marshall Is.','Pacific/Chatham'=>'Chatham Is.','Pacific/Tongatapu'=>'Nuku alofa','Pacific/Apia'=>'Samoa','Pacific/Fakaofo'=>'Tokelau Is.');

if(isset($arrNameById[Auth::User()->timezone])){
  $mytimezone=Auth::User()->timezone;
}else{
  $mytimezone='Etc/UTC';
}

?>

<div class="row">
  <div class="col-12 col-lg-8 offset-lg-2 mb-3">
    <form id="formRule" name="formRule" enctype="multipart/form-data">
      {{csrf_field()}}
      @if( !empty( $note ) )
        <input type="hidden" name="productnoteid" id="productnoteid" value="{{$note['productnoteid']}}">
      @else
        <input type="hidden" id="productnoteid" value="0">
      @endif
      <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <button class="btn btn-link" type="button" data-toggle="" data-target="#One" aria-expanded="true" aria-controls="One" style="cursor: default;">
              1. Product
            </button>
          </div>
          <div id="One" class=" show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">

                <div class="form-group row note_text_show mb-0">
                  <div for="text" class="col-12 col-sm-auto mb-2">
                        <a href="javascript:void(0)" onclick="openProductModel('#blacklistProduct-model');" class="btn btn-primary" >Pick a Product</a> 
                    <input type="hidden"  name="productid" type="text" class="form-control" id="addProduct" placeholder="Selected products " value="{{ isset( $note['productid'] ) ? $note['productid'] :'' }}">
                    <input type="hidden"  name="productimg" type="text" class="form-control" id="Productimg" value="{{ isset( $note['productimg'] ) ? $note['productimg'] :'' }}">
                    
                  </div> 
                  <div class=" col-12 col-md-8">
                 <div class="">
                    <img style="max-height: 100px;" id="setproductimg" alt="" src="{{ !empty( $note['productimg'] ) ? $note['productimg'] :'' }}" class="mr-2 img-fluid img-responsive border float-left">
                 </div>
                      <div class="">
                    <a id="productcount" target="_blank" href="{{ !empty( $note['producthandle'] ) ? $note['producthandle'] :'' }}">{{ isset( $note['productname'] ) ? $note['productname'] :'' }}</a>
                      </div>
                  </div>
                  
                </div>
            </div>
          </div>
    
      <div class="card">
        <div class="card-header" id="headingThree">
          <h2 class="mb-0">
            <button class="btn btn-link d" type="button" data-toggle="" data-target="#Three" aria-expanded="false" aria-controls="Three" style="cursor: default;">
              2. Content
            </button>
          </h2>
        </div>
        <div id="Three" class="" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                <div class="form-group row note_text_show">
                    <!-- <label for="staticEmail" class="col-lg-4 col-sm-4 col-md-4 col-4 col-form-label mt-2">Select Products</label> -->
                    <div  class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- <input type="text" name="description" id="mytextarea" class="form-control" value="{{ isset( $note['description'] ) ? $note['description'] :'' }}" > -->
                        <textarea name="description" id="mytextarea" value="{{ isset( $note['description'] ) ? $note['description'] :'' }}" >{{ isset( $note['description'] ) ? $note['description'] :'' }}</textarea>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link d" type="button" data-toggle="" data-target="#Four" aria-expanded="false" aria-controls="Four" style="cursor: default;">
          3. Activate
        </button>
      </h2>
    </div>
    <div id="Four" class="" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">

        <div class="form-group row">
          <label for="Activate" class="col-12 col-sm-5 col-md-4 col-form-label">Activate</label>
          <div class="col-12 col-sm-7 col-md-8 col-lg-8">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              @if( !empty( $note )  )
                <label class="btn btn-outline-primary {{  $note['isactive'] == 1 ? 'active':'' }} ">
                  <input  {{  $note['isactive'] == 1 ? 'checked':'' }}  type="radio" name="active" value="1" autocomplete="off" > ON
                </label>
                <label class="btn btn-outline-primary {{ $note['isactive'] == 0 ? 'active':'' }}">
                  <input {{ $note['isactive'] == 0 ? 'checked':'' }} type="radio" name="active" value="0" autocomplete="off" > OFF
                </label>
              @else
                <label class="btn btn-outline-primary active ">
                  <input checked type="radio" name="active" value="1" autocomplete="off" > ON
                </label>
                <label class="btn btn-outline-primary ">
                  <input  type="radio" name="active" value="0" autocomplete="off" > OFF
                </label>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body text-right">
      <button type="button" onclick="location.href='{{asset('notes')}}'" class="btn btn-default">Cancel</button>
      <!-- <button type="button" onclick="saveRule('#formRule');" class="btn btn-primary savebtn">Save</button> -->
      <button type="button" onclick="checkproductid('#formRule');" class="btn btn-primary savebtn">Save</button>
    </div>
  </div>
</div>

    </form>
</div>
 


</div>


<div class="modal fade " id="blacklistProduct-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light" id="thewatchlystModalHeader">
        <h5 class="modal-title">Pick a Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 ">
              <div class="input-group">
                <div class="input-group-prepend">
                  <button id="idtype_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Products</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item idtype" value="1" href="#">All Products </a>
                    <a class="dropdown-item idtype" value="2" href="#">Selected Products</a>
                  </div>
                </div>
                <input id="searchbox" type="search" class="form-control" value="" placeholder="Search product title with minimum 3 characters">
                <div class="input-group-append">
                  <button id="searchItem" class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>
            <div>
              </br>
              </br>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="tableDataModel">
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Select</button>
      </div>
    </div>
  </div>
</div>

<div id="checkoverride" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Note already exists</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="deleteproduct" >
            <div class="font-weight-bold">
              Do you want to replace the existing note for this product?
            </div>
            <div class="small mt-2">
              This will remove the previous note and replace it with this note.
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="ConfirmYesOverride" type="button" class="btn btn-primary text-right">Confirm</button>
            <button id="ConfirmNoOverride" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>

@include('dashboard.iconmodal')
@include('dashboard.gradientshadow')

<script>
  tinymce.init({
    selector: '#mytextarea',
    menubar: false,
    plugins: ['advlist lists code image table autoresize','fullscreen','textcolor','emoticons'],
    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent bullist numlist |' + ' code image table | alignleft aligncenter ' + 'alignright |  ' + 'forecolor backcolor casechange permanentpen formatpainter removeformat | emoticons',
    a11y_advanced_options: true,
    height: 600,
    toolbar_sticky: true,
    toolbar_mode: 'sliding',
    image_title: true,
    automatic_uploads: true,
    file_picker_types: 'image',
    file_picker_callback: function (cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function () {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = function () {
          var id = 'blobid' + (new Date()).getTime();
          var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      };

      input.click();
    },
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js" ></script> 
<script type="text/javascript">

    $(document).ready(function(){
        
    });
  
    function alert_error_message(text){
        $('#requestalert').show();
        $('#requestmessage').text(text);
        $("#requestalert").delay(2500).hide(0);
    }

    function checkproductid(id){
      var productid = $("#addProduct").val();
      var productnoteid = $("#productnoteid").val();
      if(productnoteid < 1){
        $.ajax( {
          url:"{{asset('checkproductid')}}",
          method:'POST',
          data :{'productid':productid,'productnoteid':productnoteid,'_token':'{{csrf_token()}}'},
          success:function( response ){
            if(response.msg == 'Notes exist'){
              $('#checkoverride').modal('show');
            }else{
              saveRule( id );
            }
        
          }
        });
      }
      else{
        // saveRule( id );
        $.ajax( {
          url:"{{asset('checkeditproductid')}}",
          method:'POST',
          data :{'productid':productid,'productnoteid':productnoteid,'_token':'{{csrf_token()}}'},
          success:function( response ){
            if(response.msg == 'Notes exist'){
              $('#checkoverride').modal('show');
            }else{
              saveRule( id );
            }
        
          }
        });
      }
    }

    $("#ConfirmYesOverride").click(function () {
      $('#checkoverride').modal("hide");
      saveRule('#formRule');
    });

    $("#ConfirmNoOverride").click(function () {
      $('#checkoverride').modal("hide");
    });
  
    function saveRule( id ){
      var mytext = tinymce.get('mytextarea').getContent();
      // console.log(mytext);
      var productname = $("#productcount").text();
      var producthandle = $("#productcount").attr("href");
      let formData = $(id).serialize();
      formData +='&description='+encodeURIComponent(mytext);
      formData +='&productname='+encodeURIComponent(productname);
      formData +='&producthandle='+encodeURIComponent(producthandle);

      $('.savebtn').prop('disabled', true);  
      $('#loadingalert').show();
      var isrevew = <?php echo Auth::User()->isreview; ?>;
      $.ajax( {
        url:"{{asset('noterule')}}",
        method:'POST',
        data :formData,
        success:function( response ){
          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);
          if( response.submitType == 'created' ){
            setTimeout(function(){
              location.href = '{{asset('note')}}';
              $('.savebtn').prop('disabled', false);
            },2000);
          } else {
            $('.savebtn').prop('disabled', false);
          }
          toggleAlert();
        }
      });
    }

    var data ={};
    var productidsValue = '';
    var countervalue   = 0;

    function openProductModel( modelid ,counter='0',selected=1 ){
      $(modelid).modal({
        backdrop: 'static',
        keyboard: false
      });
      filterType = selected;
      $("#idtype_selection").text( filterType == 1 ? "All Products" :"Selected Products" );
      productidsValue = $("#addProduct").val();
      countervalue = counter;
      // console.log(productidsValue);
      getProducts('',"", productidsValue );
      $('#searchbox').val('');
      $('#searchinputicon').val('');
    }

    var searchbox = '';
    var filterType = '';
    var Producttable = '';

    function getProducts( param ='', searchbox='' , strProduct='' ){
        $("#tableDataModel").html('');
        $('#loadingalert').show();
        var page =1;
        if( param != ''){
        let params = (new URL(param)).searchParams;
        page = params.get("page");
        }
        $.ajax({
                method:"GET",
                url : '{{asset('shopify-products')}}?page='+page+'&searchbox='+searchbox+'&products='+productidsValue+"&filterType="+filterType,
                dataType: 'html',
                success : function( data ){
                $('#loadingalert').hide();
                $('#tableDataModel').html( data );
                Producttable = $('#productlist').DataTable({
                    dom: "Bfrtip",
                    responsive: true,
                    "paging": true,
                    "pagingType": "numbers",
                    "searching" : false,
                    "ordering": false,
                    "bServerSide":false,
                    "bInfo" : false,
                    "pageLength": 10,
                    });
                }
            });
    }

    $('.modal-content').keypress(function(e){
        if($(this).closest('#iconModal').length == 0){
            if(e.which == 13) {
                $("#filterModal").modal("hide");
                searchbox = $("#searchbox").val().trim();
                getProducts( '',searchbox ,productidsValue );
            }
        }
    });

    $("#searchItem").click( function(){
        searchbox = $("#searchbox").val().trim();
        if (searchbox.length < 3) {
            return false;
        }
        getProducts( '',searchbox ,productidsValue );
    });

    function getNextList( param ){
      getProducts( param , searchbox , productidsValue);
    }

    var arrProductData = [];
    var objProductData = {};

    function addProduct( instance , productid ){
      if( objProductData[countervalue] ){
        arrProductData = objProductData[countervalue];
      }else{
        arrProductData = [];
        let produtValue = $("#addProduct").val();
        if( produtValue != ''){
            // arrProductData = produtValue.split(",");
            arrProductData = produtValue;
            objProductData[countervalue] = arrProductData;
        }
      }
      let productidnew = $(instance).attr('productid');
      // if( $(instance).attr('added') ){
      // $(instance).text('Select');
      // $(instance).removeAttr('added');
      // $(instance).removeClass('btn-outline-danger');
      // $(instance).addClass('btn-outline-secondary');
      // let index = arrProductData.indexOf(productidnew);
      // arrProductData.splice(index, 1);
      // objProductData[countervalue] = arrProductData;
      // console.log(objProductData);
      // }else{
        $(instance).text('Remove');
        $(instance).attr('added',"1");
        $(instance).removeClass('btn-outline-secondary');
        $(instance).addClass('btn-outline-danger');
        // arrProductData.push( productidnew );
        arrProductData=productidnew;
        var finded = $(".productall").find(`[productid='`+objProductData[countervalue]+`']`);
        finded.text('Select');
        finded.removeAttr('added');
        finded.removeClass('btn-outline-danger');
        finded.addClass('btn-outline-secondary');
        objProductData[countervalue] = arrProductData;
        var newest = $(".productall").find(`[productid='`+objProductData[countervalue]+`']`);
        var imgpath = newest.attr("productimage");
        $("#Productimg").val(imgpath);
        $("#setproductimg").attr("src",imgpath);

        var link = newest.attr("handle");
        var shop = "<?php echo Auth::User()->shop; ?>";
        var handle = "https://"+shop+"/products/"+link; 

        $("#productcount").attr("href",handle);
        $("#productcount").text( newest.attr("producttitle") );
      // }
      $("#addProduct").val(objProductData[countervalue]);
    }

    $(".pagination").addClass('justify-content-center');

    $("#blacklistProduct-model").on("hidden.bs.modal", function () {
        $('.productall input:checkbox:checked').removeAttr('checked');
    });

    function replaceSpecialSym( value ){
        value = value.replace("\{\{","");
        value = value.replace("\}\}","");
        value = value.replace("|","");
        value = value.replace("money","");
        value = value.replace("round","");
        value = value.replace("%",'');
        return value;
    }

    $(".idtype").click( function(){
        filterType = $(this).attr('value');
        $("#idtype_selection").text( $(this).text() );
        getProducts('',"", productidsValue );
    });

</script>
@stop
