<div class="col-xs-12 col-sm-9">
    <div class="card">
      <div class="card-header">

          
      </div>
      <div class="card-body">
        <form onsubmit="event.preventDefault();  return saveSetting('#form-default-settings','{{asset('defaultsetting')}}');" id="form-default-settings" name="form-button-settings">
          {{csrf_field() }}
          <div class="form-group row">
            <label for="Offset" class="col-4 col-sm-3 col-md-3 col-form-label">Location
             <div class="small">
                  <a class="text-muted" href="https://thewatchlyst.freshdesk.com/support/solutions/articles/19000091746-offset-settings" target="_blank">
                      Help
                      <span class="fa fa-caret-right"></span>
                  </a>
              </div>
            </label>
            <div class="col-8 col-sm-9 col-md-6">
                {{
                    Form::select('locationid', $locations, isset($usersettings->locationid) ? $usersettings->locationid : '', array('id' => 'locationid', 'class' => 'form-control', 'type' => 'select'))
                  }}
            </div>

            

          </div>
        
     

      


        

          
          <button type="submit" class="btn btn-primary float-right">Save</button>
        </form>
      </div>
  
    </div>
  
  
  
   