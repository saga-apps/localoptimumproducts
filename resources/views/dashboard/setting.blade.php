@extends('layouts.master')

@section('title')
@parent
<title>Settings</title>
@stop

@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')

@parent

@stop

@section('content')
@parent


<div class="row ">

    <!--/.col-xs-12.col-sm-9-->

    @include('dashboard.setting.settingmenu')

    @if($subpage == 'defaultsetting')
    @include('dashboard.setting.defaultsetting')
    @elseif($subpage == 'monitorsetting')
    @include('dashboard.setting.monitorsetting')
    @endif


</div>





@stop

@section('scripts')
@parent

<script type="text/javascript">


    function saveSetting( fromId , Url ){

        // var fromData = $(fromId).serialize();
        $('#loadingalert').show();
        var form = $(fromId)[0];
         // var formData = new FormData($(fromId)[0]);
         var objformData = new FormData(form);
            console.log(objformData);
        $.ajax({
                method:"POST",
                url : Url,
                data : objformData,
                contentType: false,
                processData: false,

                success : function( data ){
                    $('#loadingalert').hide();
                    $('#requestmessage').text(data.msg);
                    if(data.logoimg) {
                        $('#emailLogo').attr("src", data.logoimg);
                    }
                    toggleAlert();
                }

            });

    }


</script>

@stop
