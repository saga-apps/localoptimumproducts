<table id="orderslist" class="table table-responsive table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr class="text-center">
            <th scope="col">#</th>
            <th scope="col">Products</th>
            <th scope="col">Total Order Count</th>
            <th scope="col">Recent Sold Count</th>
            <th scope="col" >Action</th>
        </tr>
    </thead>

    <colgroup>

        <col width="10%">
        <col width="55%">
        <col width="15%">
        <col width="10%">
        <col width="10%">

    </colgroup>

    <tbody id="">
        <form id="savecount" name="savecount">
        {{ csrf_field() }}
        @if(!empty( $products )) @foreach( $products as $product )
        
        
        <tr class="">
        
        <td class="text-center">
            
            <img style="max-height: 40px;" alt="{{$product['title']}}" src="{{ !empty( $product['images'][0]['src'] ) ? $product['images'][0]['src'] :'' }}" class=" img-fluid img-responsive">
                
        </td>
        <td class="text-left small">
               
            <a onclick='window.open("https://{{Auth::User()->shop}}/products/{{$product['handle']}}");return false;'  href="javascript:void(0);">{{trim( $product['title'] ) }}</a>
               
        </td>
            
        <td>
           
                <?php if( isset( $product['ordercount'] ) && ( $product['ordercount'] > 0 ) ){
                        $order = $product['ordercount'];
                    }else{
                        $order = '0';
                    } 
                ?>
                    <input type="number" class="form-control form-control-sm {{$product['id']}}" id="ordercount-{{$product['id']}}" name="ordercount" style="max-width: 60px;" value={{( isset( $product['ordercount'] ) && ( $product['ordercount'] > 0 )) ? $product['ordercount'] : '0'}} >


        </td>
            <td>
            <?php if( isset( $product['recentcount'] ) && ( $product['recentcount'] > 0 ) ){
                    $recentcount = $product['recentcount'];
                }else{
                    $recentcount = '0';
                } 
            ?>
            <p> {{$recentcount}} </p>
            </td>
        <td>
               
           
            @if(Auth::User()->planid > 2)
                <button type="button" onclick="addOrderProduct(this,'{{$product['id']}}')" class="btn btn-outline-primary btn-sm">Save</button>
            @else
                <button type="button" class="btn btn-outline-primary btn-sm" disabled>Save</button>
            @endif
          
        
          

        </td>


        </tr>
        @endforeach  
        @endif
    </form>
    </tbody>
</table>