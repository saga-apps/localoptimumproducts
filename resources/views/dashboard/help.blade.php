@extends('layouts.master')

@section('title')
@parent
<title>Help</title>
@stop

@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop

@section('content')
@parent
<div class="row pt-2">
    <div class="col-12 col-sm-8 offset-sm-2 mb-4">
<div class="card bg-light">
<div class="card-body border-top">
<div class="small">
Support email: <b>prime.app.shopify@gmail.com</b>
</div>
</div>
</div>
</div>
    <div class="col-12 col-sm-12 col-md-8 offset-md-2">
<div class="accordion border" id="accordionExample">
  <div class="">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Integrating Prime App
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
          <p>
              Adding Prime App code into your theme is a one-time activity which once done you do not have to worry about later.
              If you are not comfortable adding the code simply email us at <b>prime.app.shopify@gmail.com</b> and we will take care of it.
          </p>
          <h6>
              Steps:
          </h6>
          <ol>
              <li>
                  Go to your Shopify admin.
              </li>
                <li>
                  Click Online Store > Themes > Actions
                  <br>
                  <img class="img img-fluid mt-1 mb-3" src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/48000862600/original/g2uvwxi1BrDBa71qPyy6sG01QoCfjWqayw.png?1562222541">
                  <br>
                </li>
                <li>
                   Click Edit code. 
                    <br>
                  <img class="img img-fluid mt-1 mb-3" src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/48000862632/original/n0b3Niu2nQk_3nYQrCtZDsFUsd5xMasP6w.png?1562222589">
                  <br>
              </li>
                <li>
                    Add the following code where you'd like the badges to show:
                    <br>
                    <div class="alert alert-primary mt-2">
                    <code>{% render 'primeb', product: product, primebInnerClass: 'prime-d-inline-block prime-mr-1 prime-mb-1' %}</code>
                    </div>
                    Note: The actual place where you insert the code will differ as per the installed theme.
                    <br>
                    <ul>
                        <li>
                            For showing the badges on the product pages place the code in the <b>sections/product-template.liquid</b> file.
                            Search for <code> &#123;&#123; product.title &#125;&#125; </code> and place the above mentioned code in the next line. 
                            This will display the badges below product title.
                            <br>
                            <img class="img img-fluid mt-1 mb-3" src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/48036128816/original/Ap-89MZ6DnQYPCJSj9KgnE3H3zo2TYSE2w.png?1586954762">
                            <br>
                        </li>
                        <li>
                            Similarly the Prime app code can be placed in collection page. Look for <b>snippets/product-card-grid-item.liquid</b> or <b>snippets/product-grid-item.liquid</b> file.
                            <br>
                            <img class="img img-fluid mt-1 mb-3" src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/48036131085/original/zWKTN3PbtmW4Pdp_6AqSh9ySQub65gGKIQ.png?1586955618">
                            <br>
                        </li>
                    </ul> 
                    
              </li>
              <div class="alert alert-danger mt-3">
                  If you have problems with the installation or want to change the way the badges look, please email us at <b>prime.app.shopify@gmail.com</b> and we will take care of it.
              </div>
          </ol>
      </div>
    </div>
  </div>
  <div class="">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Uninstalling the app
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
          <p>
              If you directly uninstall the app from the Shopify admin then the badges will continue to display on your store.
          </p>
        <p>
            To completely remove the badges and delete the app code from your store follow the steps below before uninstalling the app:
        </p>
          <h6>
              Steps:
          </h6>
          <ol>
              <li>
                  Deactivate each badge by clicking the OFF switch on the dashboard home page.
                   <br>
                        <img class="img img-fluid mt-1 mb-3" src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/48036855587/original/9f1yJKz_-rvYgkEQSVnWINF-2J7gZUhduA.png?1587470075">
                    <br>
              </li>
               <li>
                  Open the <b>snippets/primeb.liquid</b> in the Shopify Theme Editor. Clear the contents and hit Save.
              </li>
              <li>
                  (Optional) Remove the 1-line integration code from your theme which you added as per the integration guide mentioned above. Keeping the code will not have negative impact on your store.
              </li>
               <li>
                  (Optional) Delete the <b>snippets/primeb.liquid</b> file in your Shopify Theme Editor. Do not delete this file if you have not removed the code mentioned in step3.
              </li>
              <li>
                  Uninstall the app from Shopify admin by clicking the Delete button.
                   <br>
                        <img class="img img-fluid mt-1 mb-3" src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/48037297853/original/BsUPMnKAf8hiADRlOkTy50duCtb0cYeAYA.png?1587703885">
                    <br>
              </li>
            
          </ol>
      </div>
    </div>
  </div>
<!--  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>-->
</div>
   </div>
    </div>

<script type="text/javascript">

</script>


@stop
