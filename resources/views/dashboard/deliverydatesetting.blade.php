@extends('layouts.master')


@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')

@parent

<style>
.selectedday {
  text-decoration: line-through !important;
}
</style>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css
">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

@stop
 
@section('content')
@parent


<div class="row ">

    <div id="sidebar" class="col-xs-12 col-sm-2">
        <div class="list-group mt-3">
            <a class="list-group-item {{$activeMenu =='ordersetting' ? 'active':'' }}" href="/settings-order-count">Orders</a>
            <a class="list-group-item {{$activeMenu =='deliverydate' ? 'active':'' }}" href="/settings-delivery-date">Estimated Delivery</a>
            <a class="list-group-item {{$activeMenu =='livecount' ? 'active':'' }}" href="/settings-livevisitcount">Visitor Count</a>
        </div>
    </div>

    @section('title')
        @parent
        <title>Estimated Delivery Date Settings</title>
        @stop
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7 mt-3">
            @if(Auth::User()->planid < 3)
                <div class="row">
                    <div class="col-md-12">
                        <p class="alert alert-danger w-100">This feature is available only on the Pro Plan.
                            <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
                        </p>
                    </div>
                </div>
            @endif

            <div class="form-group row">
                <div id="userreview" class="col-md-12 mt-1">
                    <form id="formRule" name="formRule" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="card">
                        <div class="card-header">Estimated Delivery Date Settings</div>
                    <div class="card-body">   
                        <div class="form-group row badge_text_show">
                            <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label ">Week off Days </label>
                            <div class="col-12 col-sm-7 col-md-8">
                                <button type="button" onclick="setdate(this,'1');" value="1" class="btn btn-sm btn-outline-warning one1 mb-1">Monday</button>
                                <button type="button" onclick="setdate(this,'2');" value="2" class="btn btn-sm btn-outline-warning one2 mb-1">Tuesday</button>
                                <button type="button" onclick="setdate(this,'3');" value="3" class="btn btn-sm btn-outline-warning one3 mb-1">Wednesday</button>
                                <button type="button" onclick="setdate(this,'4');" value="4" class="btn btn-sm btn-outline-warning one4 mb-1">Thursday</button>
                                <button type="button" onclick="setdate(this,'5');" value="5" class="btn btn-sm btn-outline-warning one5 mb-1">Friday</button>
                                <button type="button" onclick="setdate(this,'6');" value="6" class="btn btn-sm btn-outline-warning one6 mb-1">Saturday</button>
                                <button type="button" onclick="setdate(this,'0');" value="0" class="btn btn-sm btn-outline-warning one0 mb-1">Sunday</button>
                            </div>
                        </div>
                        <input type="hidden" id="weekdays" name="weekdays" value="{{ !empty( $weekdays ) ? $weekdays :'7'}}">

                        <div class="form-group row badge_text_show">
                            <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label ">Date Format </label>
                            <div class="col-12 col-sm-5 col-md-4">
                                <select id="dateformat" class="form-control" name="dateformat" >
                                    <option value="ddd, MMM DD" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'ddd, MMM DD') ? 'selected' :''}} >Sat, Jul 09</option>

                                    <option value="ddd, MMM DD yyyy" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'ddd, MMM DD yyyy') ? 'selected' :''}} >Sat, Jul 09 2021</option>

                                    <option value="dddd, MMMM D" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'dddd, MMMM D') ? 'selected' :''}} >Saturday, July 9</option>

                                    <option value="dddd, MMMM D, yyyy" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'dddd, MMMM D, yyyy') ? 'selected' :''}} >Saturday, July 9, 2021</option>

                                    <option value="DD-MM-y" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'DD-MM-y') ? 'selected' :''}} >09-07-2021</option>

                                    <option value="MM-DD-y" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'MM-DD-y') ? 'selected' :''}} >07-09-2021</option>

                                    <option value="MMMM D, y" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'MMMM D, y') ? 'selected' :''}} >July 9, 2021</option>

                                    <option value="MMMM D" {{ (isset(Auth::User()->dateformat) && Auth::User()->dateformat == 'MMMM DD') ? 'selected' :''}} >July 9</option>

                                </select>   
                            </div>
                        </div>
                        <hr>
                        @if(Auth::User()->planid > 2)
                        <div class="col-12"><a class="btn btn-primary float-right" id="updatenow" href="JavaScript:Void(0);">Save</a></div>
                        @else
                        <div class="col-12"><a class="btn btn-primary float-right" href="JavaScript:Void(0);">Save</a></div>
                        @endif
                    </div>
                </div>
                </form>
            </div>
        </div>
        </div>
  
        <div class="col-12 col-sm-3 mb-2 mt-3">
            <div class="card bg-light">
                <div class="card-body">
                    <div>
                        <div class="font-weight-bold">Estimated Delivery Date</div>
                        <div class="small mt-1">
                        Display the expected delivery date message right inside the badge. Improve shopping experience by letting the customers know when their orders will arrive.
                            <br>
                            <div class="badge badge-success my-2 p-2">Expected delivery between 12 Jan and 14 Jan</div>
                            <br>Configure the off days and date format as per your requirements
                            <div class="mt-1">
                                <a href="https://getinstashop.freshdesk.com/support/solutions/articles/81000392649-estimated-delivery-dates" target="_blank" class="">Know More<span class="fa fa-caret-right ml-1"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            
        </div>
    </div>
</div>

@stop

@section('scripts')
@parent

<script type="text/javascript">

    $(document).ready(function (){  
        font_style();
    });

    $('#updatenow').on('click', function(e) {

        var font_values = [];
        var font_style_class = $(".selectedday");
        for(var i = 0; i < font_style_class.length; i++){
        if( !$(font_style_class[i]).val() ) {                    
    
        }else{
            font_values.push($(font_style_class[i]).val() );
        }
        }
        var week = '';
        if(!font_values){
        week = '7';
        }else{
        week = font_values;
        }

        if(week.length > 6){
            $('#requestmessage').text("Max 6 days off allowed");
            toggleAlert();
            return false;
        }
        
        let formData = $('#formRule').serialize();
        formData +='&week='+ week;
        // console.log(formData);
        // return false;
        $('#loadingalert').show();
        $.ajax({
        url:"{{asset('deliverydatesetting')}}",
        method:'POST',
        data :formData,
        success: function(response){
            console.log(response);
            if(response=="done")
            $('#loadingalert').hide();
            $('#requestmessage').text("Delivery Date Updated Successfully! ");
            toggleAlert();
            setTimeout(function(){ 
            location.reload();
            }, 2000);
        },
        error: function(error){

            console.log(error);
            $('#loadingalert').hide();
            $('#requestmessage').text("Something Went Wrong");
            toggleAlert();

        }

        });

    });

    function font_style(){
        var checkvalue = $('#weekdays').val();
        var real_values = checkvalue.split(",");
        for(var i = 0; i < real_values.length; i++){
            setdate('',real_values[i]);
        }
    } 

    function setdate( instance,value ){
        if(instance==''){
            instance='.one'+value;
        }
        if( value == 1 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }
        if( value == 2 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }
        if( value == 3 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }
        if( value == 4 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }
        if( value == 5 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }
        if( value == 6 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }
        if( value == 0 ){
            if($(instance).hasClass("selectedday")){
                $(instance).removeClass("selectedday");
            }else{
                $(instance).addClass("selectedday");
            }
        }

    }


</script>

@stop
