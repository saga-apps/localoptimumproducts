@extends('layouts.master') @section('title') @parent
<title>{{ Request::is('editbanner*') ? "Edit Banner" :"Create Banner" }}</title>
@stop @section('description') @parent
<meta content="" name="description" />
@stop
@section('css')
 @parent

@stop
@section('js')
@parent

<style>

.primenWrapper ul, ol {
    display: block;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    padding-inline-start: 40px;
    list-style-type: revert;
}
.primenWrapper li {
    list-style: inherit;
    list-style-position: inside;
}

.prime-p-0{padding: 0rem;} 
.prime-pr-0{padding-right: 0rem;} 
.prime-pl-0{padding-left: 0rem;}
.prime-pt-0{padding-top: 0rem;}
.prime-pb-0{padding-bottom: 0rem;}
  
.prime-p-1{padding: .25rem;} 
.prime-pr-1{padding-right: .25rem;} 
.prime-pl-1{padding-left: .25rem;}
.prime-pt-1{padding-top: .25rem;}
.prime-pb-1{padding-bottom: .25rem;}

.prime-p-2{padding: .5rem;} 
.prime-pr-2{padding-right: .5rem;} 
.prime-pl-2{padding-left: .5rem;}
.prime-pt-2{padding-top: .5rem;}
.prime-pb-2{padding-bottom: .5rem;}

.prime-p-3{padding: 1em;} 
.prime-pr-3{padding-right: 1rem;} 
.prime-pl-3{padding-left: 1rem;}
.prime-pt-3{padding-top: 1rem;}
.prime-pb-3{padding-bottom: 1rem;}

.prime-p-4{padding: 1.5rem;} 
.prime-pr-4{padding-right: 1.5rem;} 
.prime-pl-4{padding-left: 1.5rem;}
.prime-pt-4{padding-top: 1.5rem;}
.prime-pb-4{padding-bottom: 1.5rem;}

.prime-p-5{padding: 3rem;} 
.prime-pr-5{padding-right: 3rem;} 
.prime-pl-5{padding-left: 3rem;}
.prime-pt-5{padding-top: 3rem;}
.prime-pb-5{padding-bottom: 3rem;}

.prime-d-block{display:block!important;}
.prime-d-inline-block{display:inline-block;}
.prime-align-middle{vertical-align:middle;}

.prime-px-2{padding-left:.5rem;padding-right:.5rem;}
.prime-py-1{padding-top:.25rem;padding-bottom:.25rem;}
.prime-mx-auto{margin-left: auto;margin-right: auto;}
.prime-text-center{text-align:center;}
.prime-text-left{text-align:left;}

.prime-d-table-cell {
  display: table-cell!important;
}

.primebanPhotoOuter {line-height: 0px;}

.prime-px-0{padding-left:0px !important;padding-right:0px !important;}
.prime-col,.prime-col-1,.prime-col-10,.prime-col-11,.prime-col-12,.prime-col-2,.prime-col-3,.prime-col-4,.prime-col-5,.prime-col-6,.prime-col-7,.prime-col-8,.prime-col-9,.prime-col-auto,.prime-col-lg,.prime-col-lg-1,.prime-col-lg-10,.prime-col-lg-11,.prime-col-lg-12,.prime-col-lg-2,.prime-col-lg-3,.prime-col-lg-4,.prime-col-lg-5,.prime-col-lg-6,.prime-col-lg-7,.prime-col-lg-8,.prime-col-lg-9,.prime-col-lg-auto,.prime-col-md,.prime-col-md-1,.prime-col-md-10,.prime-col-md-11,.prime-col-md-12,.prime-col-md-2,.prime-col-md-3,.prime-col-md-4,.prime-col-md-5,.prime-col-md-6,.prime-col-md-7,.prime-col-md-8,.prime-col-md-9,.prime-col-md-auto,.prime-col-sm,.prime-col-sm-1,.prime-col-sm-10,.prime-col-sm-11,.prime-col-sm-12,.prime-col-sm-2,.prime-col-sm-3,.prime-col-sm-4,.prime-col-sm-5,.prime-col-sm-6,.prime-col-sm-7,.prime-col-sm-8,.prime-col-sm-9,.prime-col-sm-auto,.prime-col-xl,.prime-col-xl-1,.prime-col-xl-10,.prime-col-xl-11,.prime-col-xl-12,.prime-col-xl-2,.prime-col-xl-3,.prime-col-xl-4,.prime-col-xl-5,.prime-col-xl-6,.prime-col-xl-7,.prime-col-xl-8,.prime-col-xl-9,.prime-col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.prime-col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}

#ShopLanguagelist_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}
  #ShopLanguagelistadded_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}
#Countrylist_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}
  #Countrylistadded_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}

  .dataTables_filter{
    float: right !important;
  }

  .table-responsive {
        overflow-x: hidden !important;
  }
  .sp-replacer {  
      padding: 8px  !important;
      color: #212529 !important;
      height: 38px !important;
      border: 1px solid #ced4da !important;
      border-radius: .25rem !important;
      width: 100%;
  }
  .bootstrap-tagsinput input {
      display: block;
      width: 100%;
      padding: .375rem .75rem;
      font-size: 1rem;
      line-height: 1.5;
      color: #495057;
      background-color: #fff;
      background-clip: padding-box;
      border: 1px solid #ced4da;
      border-radius: .25rem;
      transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
      min-height: 28px;
  }
  .intro{
    display: none !important;
  }

  .metanameval{
    padding-left:0;
  }

  .paginate_button{
      border-color: #ffc107;
  }
  .dataTables_wrapper .dataTables_paginate  {    
    /* border: 1px solid #d39e00 !important; */
    background-color: #e0a800 !important;
    background: none !important;
    color: #ffc107 !important;
}   
  .pagination .active {
    padding: 5px;
    background-color: #ffc107 !important;
  }

  a.paginate_button:hover {
    background-color: #ffc107 !important;
    color: #ffc107 !important;
  }
  
  .active a{
    color: #fff !important;
  } 

  .dataTables_filter{
    padding: 0 0 0 65%;
  }
  .dataTables_paginate{
      margin:0 auto !important;
  }

   #colorval{

   /* margin-left:20%; */
       margin-top: 1%;

  }

  .iconbox {   
    border: 1px solid #fff;
  }

 .iconbox:hover {
    opacity: 0.75;
    border: 1px solid #ddd;
  }

  .scrollable-menu {
      height: auto;
      width: 200px !important;
      max-height: 250px;
      overflow-x: hidden;
  }

  .pcr-button{

    height:2.4em !important;
    width: 3em !important;

  }

  @media (max-width: 767px) { 
    div.dataTables_paginate ul.pagination{
      max-width: 85%;
    }
    .dataTables_filter{
      padding: 0 0 0 50%;
    }
    div.dataTables_filter input{
      max-width: 45%;
    }
  }


  /* .paging_numbers{
    min-width:100%;
  } */

  
/* .pagination{
  padding: 0 0 0 184px;
} */

.dropdown-item.active div{
  background-color: #777;
}
.mybannershape{
    background-color: #ddd;
    margin: 3px auto;
    width: 100px;
    height: 30px;
    border: 1px solid black;
}

.dropdown-menu {
  transform: translate(0px,38px) !important;
}



.intro{
    display: none !important;
  }
</style>


<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css
">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.tiny.cloud/1/wrz7bhvawkamdmyyvadqxu19hm5et399aifwiejk5hhc1fjk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

@stop
@section('content')
@parent

<?php
$inventoryHide = '';
if( !empty( $banner ) ) {
  $display       = json_decode( $banner['display'] ,true );
  $displayrules  = json_decode( $banner['displayrules'] ,true );
  $bannerstyle    = json_decode( $banner['bannerstyle'] ,true );

  if(strpos($banner['title'], 'product.metafields') !== false){
    $banner['title'] = str_replace("product.metafields","primebanm",$banner['title']);
  }

  if(Auth::User()->planid > 2){
    $displayprorules  = json_decode( $banner['displayprorules'] ,true );
  }else{
    $displayprorules  = '';
  }

}

$arrNameById = array( 'Etc/GMT+12' =>'International Date Line West','Pacific/Pago_Pago'=>'American Samoa','Pacific/Midway'=>'Midway Island','Pacific/Honolulu' =>'Hawaii','America/Juneau'=>'Alaska','America/Los_Angeles'=>'Pacific Time','America/Tijuana' =>'Tijuana','America/Phoenix'=>'Arizona','America/Chihuahua'=>'Chihuahua','America/Mazatlan'=>'Mazatlan','America/Denver'=>'Mountain Time','America/Guatemala'=>'Central America','America/Chicago'=>'Central Time','America/Mexico_City'=>'Mexico City','America/Monterrey'=>'Monterrey','America/Regina'=>'Saskatchewan','America/Bogota'=>'Bogota','America/New_York'=>'Eastern Time','America/Indiana/Indianapolis'=>'Indiana','America/Lima'=>'Quito','America/Halifax'=>'Atlantic Time','America/Caracas'=>'Caracas','America/Guyana'=>'Georgetown','America/La_Paz'=>'La Paz','America/Puerto_Rico'=>'Puerto Rico','America/Santiago'=>'Santiago','America/St_Johns'=>'Newfoundland','America/Sao_Paulo'=>'Brasilia','America/Argentina/Buenos_Aires'=>'Buenos Aires','America/Godthab'=>'Greenland','America/Montevideo'=>'Montevideo','Atlantic/South_Georgia'=>'Mid-Atlantic','Atlantic/Azores'=>'Azores','Atlantic/Cape_Verde'=>'Cape Verde Is.','Africa/Casablanca'=>'Casablanca','Europe/Dublin'=>'Dublin','Europe/London'=>'London','Europe/Lisbon'=>'Lisbon','Africa/Monrovia'=>'Monrovia','Etc/UTC'=>'UTC','Europe/Amsterdam'=>'Amsterdam','Europe/Belgrade'=>'Belgrade','Europe/Berlin'=>'Berlin','Europe/Zurich'=>'Zurich','Europe/Bratislava'=>'Bratislava','Europe/Brussels'=>'Brussels','Europe/Budapest'=>'Budapest','Europe/Copenhagen'=>'Copenhagen','Europe/Ljubljana'=>'Ljubljana','Europe/Madrid'=>'Madrid','Europe/Paris'=>'Paris','Europe/Prague'=>'Prague','Europe/Rome'=>'Rome','Europe/Sarajevo'=>'Sarajevo','Europe/Skopje'=>'Skopje','Europe/Stockholm'=>'Stockholm','Europe/Vienna'=>'Vienna','Europe/Warsaw'=>'Warsaw','Africa/Algiers'=>'West Central Africa','Europe/Zagreb'=>'Zagreb','Europe/Athens'=>'Athens','Europe/Bucharest'=>'Bucharest','Africa/Cairo'=>'Cairo','Africa/Harare'=>'Harare','Europe/Helsinki'=>'Helsinki','Asia/Jerusalem'=>'Jerusalem','Europe/Kaliningrad'=>'Kaliningrad','Europe/Kiev'=>'Kyiv','Africa/Johannesburg'=>'Pretoria','Europe/Riga'=>'Riga','Europe/Sofia'=>'Sofia','Europe/Tallinn'=>'Tallinn','Europe/Vilnius'=>'Vilnius','Asia/Baghdad'=>'Baghdad','Europe/Istanbul'=>'Istanbul','Asia/Kuwait'=>'Kuwait','Europe/Minsk'=>'Minsk','Europe/Moscow'=>'Moscow','Africa/Nairobi'=>'Nairobi','Asia/Riyadh'=>'Riyadh','Europe/Volgograd'=>'Volgograd','Asia/Tehran'=>'Tehran','Asia/Muscat'=>'Muscat','Asia/Baku'=>'Baku','Europe/Samara'=>'Samara','Asia/Tbilisi'=>'Tbilisi','Asia/Yerevan'=>'Yerevan','Asia/Kabul'=>'Kabul','Asia/Yekaterinburg'=>'Ekaterinburg','Asia/Karachi'=>'Karachi','Asia/Tashkent'=>'Tashkent','Asia/Kolkata'=>'New Delhi','Asia/Calcutta'=>'New Delhi','Asia/Colombo'=>'Sri Jayawardenepura','Asia/Kathmandu'=>'Kathmandu','Asia/Almaty'=>'Almaty','Asia/Dhaka'=>'Dhaka','Asia/Urumqi'=>'Urumqi','Asia/Rangoon'=>'Rangoon','Asia/Bangkok'=>'Hanoi','Asia/Jakarta'=>'Jakarta','Asia/Krasnoyarsk'=>'Krasnoyarsk','Asia/Novosibirsk'=>'Novosibirsk','Asia/Shanghai'=>'Beijing','Asia/Chongqing'=>'Chongqing','Asia/Hong_Kong'=>'Hong Kong','Asia/Irkutsk'=>'Irkutsk','Asia/Kuala_Lumpur'=>'Kuala Lumpur','Australia/Perth'=>'Perth','Asia/Singapore'=>'Singapore','Asia/Taipei'=>'Taipei','Asia/Ulaanbaatar'=>'Ulaanbaatar','Asia/Tokyo'=>'Tokyo','Asia/Seoul'=>'Seoul','Asia/Yakutsk'=>'Yakutsk','Australia/Adelaide'=>'Adelaide','Australia/Darwin'=>'Darwin','Australia/Brisbane'=>'Brisbane','Australia/Melbourne'=>'Melbourne','Pacific/Guam'=>'Guam','Australia/Hobart'=>'Hobart','Pacific/Port_Moresby'=>'Port Moresby','Australia/Sydney'=>'Sydney','Asia/Vladivostok'=>'Vladivostok','Asia/Magadan'=>'Magadan','Pacific/Noumea'=>'New Caledonia','Pacific/Guadalcanal'=>'Solomon Is.','Australia/Srednekolymsk'=>'Srednekolymsk','Pacific/Auckland'=>'Wellington','Pacific/Fiji'=>'Fiji','Asia/Kamchatka'=>'Kamchatka','Pacific/Majuro'=>'Marshall Is.','Pacific/Chatham'=>'Chatham Is.','Pacific/Tongatapu'=>'Nuku alofa','Pacific/Apia'=>'Samoa','Pacific/Fakaofo'=>'Tokelau Is.');

if(isset($arrNameById[Auth::User()->timezone])){
  $mytimezone=Auth::User()->timezone;
}else{
  $mytimezone='Etc/UTC';
}

?>

<div class="row">
  <div class="col-12 col-lg-9 offset-lg-2 mb-3">
    <form id="formRule" name="formRule" style="margin-bottom: 200px;" enctype="multipart/form-data">
      {{csrf_field()}}
      @if( !empty( $banner ) )
        <input type="hidden" name="bannerid" value="{{$banner['productbannerid']}}">
      @endif

      <div class="card" style="position: fixed;bottom: 0px;left: 5px;z-index: 3;right: 5px;">
        <div class="card-body bg-light">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-4 offset-md-4">
            <div id="bannerpreview" class="" style="">

              <div class="primeBanners  prime-d-block" id="" style="">
              @if(isset($banner['banner_type']))
                @if( $banner['banner_type'] == 'text' )
                <!--  For Text type  -->
                <div class="primebanDesktop prime-d-block tooltips" data-toggle="tooltip" id="bannerStyle" style="display:none;">
                  <div class="primebanImageOuter prime-d-table-cell prime-align-middle ">
                  @if( !isset($banner['imgsrc']))
                    <img class="primebanImage prime-d-block  prime-mx-auto banner_image_preview" onerror="errorimgmsg(this);" id="banner_image_preview" style="height:50px;" src="{{ isset( $banner['productbannerid'] )  ? env('AWS_PATH').'ban'.$banner['productbannerid'] :'https://img.icons8.com/444444/color/8x/acacia.png' }}">
                  @else
                    <img class="primebanImage prime-d-block prime-mx-auto banner_image_preview" onerror="errorimgmsg(this);" id="banner_image_preview" style="height:50px;" src="{{ isset( $banner['productbannerid'] )  ? $banner['imgsrc'] :'https://img.icons8.com/444444/color/8x/acacia.png' }}">
                  @endif
                  </div>
                  <div class="primebanTexts prime-d-table-cell prime-align-middle" style="">	
                    <div class="primebanTitle prime-mb-1 bannertitle" id="bannertitleStyle" style="font-weight: 900; font-size: 20px; ">This is the title</div>
                    <div class="primebanSubTitle bannersubtitle" id="bannersubtitleStyle" style="font-size: 12px; ">This is the sub-title</div>
                  </div>
                </div>  
                @endif 

                @if( $banner['banner_type'] == 'image' )
                <!--  For Image type  -->
                <div class="primebanDesktop  tooltips" data-toggle="tooltip" id="bannerStyle_image" style="display:none;">
                  <div class="primebanPhotoOuter">
                  @if( !isset($banner['imgsrc']))
                    <img class="primebanPhoto banner_image_preview" id="banner_image_preview" onerror="errorimgmsg(this);"  style="height:50px;max-width:100%" src="{{ isset( $banner['productbannerid'] ) && $banner['banner_type'] == 'image'  ? env('AWS_PATH').'ban'.$banner['productbannerid'] : 'https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/banner.png' }}">
                  @else
                    <img class="primebanPhoto banner_image_preview" id="banner_image_preview" onerror="errorimgmsg(this);"  style="height:50px;max-width:100%" src="{{ isset( $banner['productbannerid'] ) && $banner['banner_type'] == 'image'  ? $banner['imgsrc'] :'https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/banner.png' }}">
                  @endif
                  </div>
                </div>
                @endif
              @else

              <div class="primebanDesktop prime-d-block tooltips" data-toggle="tooltip" id="bannerStyle" style="display:none;">
                  <div class="primebanImageOuter prime-d-table-cell prime-align-middle ">
                    <img class="primebanImage prime-d-block  prime-mx-auto banner_image_preview" onerror="errorimgmsg(this);"  style="height:50px;" src="https://img.icons8.com/444444/color/8x/acacia.png">
                  </div>
                  <div class="primebanTexts prime-d-table-cell prime-align-middle" style="">	
                    <div class="primebanTitle prime-mb-1 bannertitle" id="bannertitleStyle" style="font-weight: 900; font-size: 20px; ">This is the title</div>
                    <div class="primebanSubTitle bannersubtitle" id="bannersubtitleStyle" style="font-size: 12px; ">This is the sub-title</div>
                  </div>
                </div>  


                <div class="primebanDesktop tooltips" data-toggle="tooltip" id="bannerStyle_image" style="display:none;">
                  <div class="primebanPhotoOuter">
                  <img class="primebanPhoto banner_image_preview" onerror="errorimgmsg(this);"  id="banner_image_preview" style="height:50px;max-width:100%" src="https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/banner.png">
                  </div>
                </div>

              @endif

              </div>

            </div>
          </div>
        </div>
      </div>





      <!-- <div class="mb-2 text-center border bg-white" style="position: fixed; top: 72px; right: 20px; z-index: 2;">
          <div class="d-inline-block">
        <div class="m-2 tooltips" id="bannerStyle" style="display:none;" data-toggle="tooltip"  title="">
          <span class="spanStyleleft" ></span>
          <div id="textStyle" class="bannertitle">Text</div>
          <span id="spanStyle"></span>
        </div>
        <div class="m-2 tooltips" id="bannerStyle_image" style="display:none;" data-toggle="tooltip"  title="">
          <span class="spanStyleleft" ></span>

           @if( !isset($banner['imgsrc']))
              <img src="{{ isset( $banner['productbannerid'] ) && $banner['banner_type'] == 'image'  ? env('AWS_PATH').$banner['productbannerid'] :'https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/banner_image.png' }}" id="banner_image_preview" style="max-height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'50px' }}" />
           @else
              <img src="{{ isset( $banner['imgsrc'] ) && $banner['banner_type'] == 'image'  ? $banner['imgsrc'] :'https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/banner_image.png' }}" id="banner_image_preview" style="max-height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'50px' }}" />
              
          @endif

          <span id="spanStyle"></span>
        </div>
      </div></div> -->

      <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              1. Content
            </button>
          </div>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
              <div class="form-group row">
                <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Banner Type</label>
                <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                  <div class="mt-2">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" name="banner_type" id="banner_text" checked value="text"  class="custom-control-input bannertypeclass"  onclick="check_banner_type()" {{ ( isset( $banner['banner_type'] ) && ( $banner['banner_type'] == 'text' )) ? 'checked' :'unchecked' }}  {{ ( isset( $banner['productbannerid'] ) ) ? 'disabled' :'' }} >
                        @if( isset( $banner['banner_type'] ) && ( $banner['banner_type'] == 'text' ) )
                          <input type="hidden" name="banner_type" value="text">
                        @endif
                        <label class="custom-control-label" for="banner_text">Text</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" name="banner_type" id="banner_image" value="image"  class="custom-control-input bannertypeclass" onclick="check_banner_type()" {{ ( isset( $banner['banner_type'] ) && ( $banner['banner_type'] == 'image' )) ? 'checked' :'' }} {{ ( isset( $banner['productbannerid'] ) ) ? 'disabled' :'' }} >
                        @if( isset( $banner['banner_type'] ) && ( $banner['banner_type'] == 'image' ) )
                          <input type="hidden" name="banner_type" value="image">
                        @endif
                        <label class="custom-control-label" for="banner_image">Image</label>
                      </div>
                      
<!--                    <label class="radio">
                      <input type="radio" name="banner_type" id="banner_text" checked  onclick="check_banner_type()" 
                      {{ ( isset( $banner['banner_type'] ) && ( $banner['banner_type'] == 'text' )) ? 'checked' :'unchecked' }} 
                      value="text">
                      Text
                    </label>
                    <label class="radio">
                      <input type="radio" name="banner_type" id="banner_image" onclick="check_banner_type()" 
                      {{ ( isset( $banner['banner_type'] ) && ( $banner['banner_type'] == 'image' )) ? 'checked' :'' }}
                       value="image">
                      Image
                    </label>-->
                  </div>
                  <!-- <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" id="banner_text" onclick="check_banner_type()" checked value="banner_text">
                      <label for="text">Text</label>
                    </label>
                  </div>
                  <div class="form-check-inline ">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" id="banner_image" onclick="check_banner_type()" value="banner_image"><label for="image">Image</label>
                    </label>
                  </div> -->
                </div>
              </div>
              <div class="form-group row banner_text_show">
                <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Banner Title</label>
                <div class="col-12 col-sm-7 col-md-8 col-lg-8">

                  <?php $simpletext = "IT'S BUY-1-GET-1-TREE"; ?>

                  <input type="text" class="form-control" name="display[bannertitle]" id="bannertitle" placeholder="Enter the Banner Title" value="{{isset( $banner['title'] ) ? $banner['title'] : $simpletext }}"> 

                  <a class="float-right btn btn-sm  btn-secondary mt-2" href="javascript:void(0);" onclick="openDynamicVariableModel('#openDynamicVariableModel-model');" >
                    <span class="">
                      Add Dynamic Data Fields 
                    </span>
                  </a>

                  <a href="javascript:void(0)" class="float-right btn btn-sm btn-secondary mt-2 mr-1" id="emoji">
                  <span class="">&#128526;</span></a>

                  <!-- <a class="float-right btn btn-sm  btn-secondary mt-2 mr-1" href="javascript:void(0);" onclick="openemojis('#openemojis-model');" >
                    <span class="">
                      Add Emojis
                    </span>
                  </a> -->

                </div>
              </div>
              <div class="form-group row banner_text_show">
                <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Banner Sub-title</label>
                <div class="col-12 col-sm-7 col-md-8 col-lg-8">

                  <?php $simplesubtext = "For every product you buy from us, we plant a tree for you!"; ?>

                  <input type="text" class="form-control" name="display[bannersubtitle]" id="bannersubtitle" placeholder="Enter the Banner Sub-title" value="{{isset( $banner['subtitle'] ) ? $banner['subtitle'] : $simplesubtext }}"> 

                  <a class="float-right btn btn-sm  btn-secondary mt-2" href="javascript:void(0);" onclick="openDynamicVariableModel('#openSubDynamicVariableModel-model');" >
                    <span class="">
                      Add Dynamic Data Fields 
                    </span>
                  </a>

                  <a href="javascript:void(0)" class="float-right btn btn-sm btn-secondary mt-2 mr-1" id="subemoji">
                  <span class="">&#128526;</span></a>

                  <!-- <a class="float-right btn btn-sm  btn-secondary mt-2 mr-1" href="javascript:void(0);" onclick="openemojis('#openemojis-model');" >
                    <span class="">
                      Add Emojis
                    </span>
                  </a> -->

                </div>
              </div>
              <div class="form-group row">
                <label for="Banner_Image" class="col-12 col-sm-5 col-md-4 col-form-label banner_text_show">Logo</label> 
                <label for="Banner_Image" class="col-12 col-sm-5 col-md-4 col-form-label banner_image_show">Image</label> 
                <div class="col-auto banner_text_show">
                  <a class="btn btn-secondary"  href="javascript:void(0)" onclick="openProductModel('#iconModal');" >Pick</a>
                  <span class="ml-4">OR</span>
                </div>
                <div class="col"> 
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" value =""  id="image" name="image"  accept="image/*" aria-describedby="Upload">
                      <label class="custom-file-label" style="overflow: hidden;" for="image">Choose file</label>
                    </div>
                  </div>
                  <div class="small text-muted">(Max size: 500KB | Type: jpg, png, gif, svg)</div>
                </div>
              </div>
                <div class="form-group row">
                 <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Banner Link
                         <span class="text-muted small">(optional)</span>
                </label>
                <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                  <div class="input-group">
                    <input type="text" class="form-control" name="" id="bannerlink" value="{{isset( $banner['bannerlink'] ) && $banner['bannerlink'] !=''  ? $banner['bannerlink'] :'http://' }}" placeholder="(Optional) Enter the Banner Link">

                    <select class="form-control" name="linktab" id="linktab">
                      <option value="0" {{ !empty( $banner['linktab'] ) && $banner['linktab'] == 0 ? 'selected' :''}} >Open in same tab</option>
                      <option value="1" {{ !empty( $banner['linktab'] ) && $banner['linktab'] == 1 ? 'selected' :''}} >Open in new tab</option>
                      <option value="2" {{ !empty( $banner['linktab'] ) && $banner['linktab'] == 2 ? 'selected' :''}} >Open in popup modal</option>
                    </select>

                  </div>
                  <btn  onclick="openLinkTabModel('#Linktab-model');" id="linktabmodal"  type="text" class="float-right btn btn-secondary mt-2 mr-1" >Modal Content </btn>

                </div>
              </div>

              <div class="form-group row 
                ">
                
                  <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Tooltip
                    <span class="text-muted small">(optional. Max 100 chars)</span>
                  </label>
                  
                  <div class="col-12 col-sm-7 col-md-8 col-lg-8">

                    <input type="text" class="form-control" maxlength="100" name="banner[tooltip]" id="tooltip" placeholder="Enter Text for Tooltip" value="{{isset( $banner['tooltip'] ) ? $banner['tooltip'] :'' }}"> 

                  </div>
                </div>


            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                2. Design
              </button>
            </h2>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body ">
                 <div class="form-group row banner_text_show">
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Body</label>
              <div class="col-auto">

                <div class="input-group">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Border Radius</label>
                  </div>
                    <select id="bannerborderradius" class="form-control" name="bannerstyle[borderradius]" onchange="applyBannerStyle(this,'borderradius');" >
                      <option value="no"  {{ !empty( $bannerstyle['borderradius'] ) && $bannerstyle['borderradius'] == 'no' ? 'selected' :''}} >No</option>
                      <option value="small"  {{ !empty( $bannerstyle['borderradius'] ) && $bannerstyle['borderradius'] == 'small' ? 'selected' :''}} >Small</option>
                      <option value="medium"  {{ !empty( $bannerstyle['borderradius'] ) && $bannerstyle['borderradius'] == 'medium' ? 'selected' :''}} {{ empty( $bannerstyle['borderradius']) ? 'selected' :'' }} >Medium</option>
                      <option value="large"  {{ !empty( $bannerstyle['borderradius'] ) && $bannerstyle['borderradius'] == 'large' ? 'selected' :''}} >Large</option>
                      
                    </select>
                </div>
              </div>
              <div class="col-auto">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Spacing</label>
                  </div>
                    <select id="bannerspacing" class="form-control" name="bannerstyle[spacing]" onchange="applyBannerStyle(this,'spacing');" >
                      <option value="1"  {{ !empty( $bannerstyle['spacing'] ) && $bannerstyle['spacing'] == '1' ? 'selected' :''}} >1</option>
                      <option value="2"  {{ !empty( $bannerstyle['spacing'] ) && $bannerstyle['spacing'] == '2' ? 'selected' :''}} >2</option>
                      <option value="3"  {{ !empty( $bannerstyle['spacing'] ) && $bannerstyle['spacing'] == '3' ? 'selected' :''}} {{ empty( $bannerstyle['spacing']) ? 'selected' :'' }} >3</option>
                      <option value="4"  {{ !empty( $bannerstyle['spacing'] ) && $bannerstyle['spacing'] == '4' ? 'selected' :''}} >4</option>
                      <option value="5"  {{ !empty( $bannerstyle['spacing'] ) && $bannerstyle['spacing'] == '5' ? 'selected' :''}} >5</option>
                      
                    </select>
                </div>


                <div class="dropdown d-none">
                  <button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Shape
                  </button>
                  <div class="dropdown-menu w-100" id="bannershape" aria-labelledby="dropdownMenuButton" style="max-height:235px;overflow:auto;">
                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 1 ? 'active' :''}}" data-value="1" href="#"><div class="mybannershape" data-value="1" style=""></div></a>
                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 2 ? 'active' :''}}" data-value="2"  href="#"><div class="mybannershape" data-value="2" style="border-radius: 2em 2em 2em 2em;"></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 3 ? 'active' :''}}" data-value="3"  href="#"><div class="mybannershape" data-value="3" style="border-radius: 0.3em 0.3em 0.3em 0.3em;" ></div></a>
                    <hr>
                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 4 ? 'active' :''}}" data-value="4"  href="#"><div class="mybannershape" data-value="4" style="border-radius: 2em 0em 0em 2em;" ></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 5 ? 'active' :''}}" data-value="5"  href="#"><div class="mybannershape" data-value="5" style="border-radius: 0em 2em 2em 0em;" ></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 6 ? 'active' :''}}" data-value="6"  href="#"><div class="mybannershape" data-value="6" style="border-radius: 2em 0em 2em 0em;" ></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 7 ? 'active' :''}}" data-value="7"  href="#"><div class="mybannershape" data-value="7" style="border-radius: 0em 2em 0em 2em;" ></div></a>

                    <!-- <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 8 ? 'active' :''}}" data-value="8"  href="#"><div class="mybannershape" data-value="8" style="border-radius: 0em 0em 3em 0em;" ></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 9 ? 'active' :''}}" data-value="9"  href="#"><div class="mybannershape" data-value="9" style="border-radius: 0em 3em 0em 0em;" ></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 10 ? 'active' :''}}" data-value="10"  href="#"><div class="mybannershape" data-value="10" style="border-radius: 3em 0em 0em 0em;" ></div></a>

                    <a class="dropdown-item {{ !empty( $bannerstyle['bannershape'] ) && $bannerstyle['bannershape'] == 11 ? 'active' :''}}" data-value="11"  href="#"><div class="mybannershape" data-value="11" style="border-radius: 0em 0em 0em 3em;" ></div></a> -->

                    <input type="hidden" id="" name="bannerstyle[bannershape]" value="{{ !empty( $bannerstyle['bannershape'] ) ? $bannerstyle['bannershape'] :'1'}}">

                  </div>
                </div>
              </div>
               <div class="col-sm d-none">
                <select id="bannerinternalpadding" class="form-control" name="bannerstyle[bannershape_padding]" onchange="applyBannerStyle(this,'Shape_padding');" >
                  <option value="medium"  {{ !empty( $bannerstyle['bannershape_padding'] ) && $bannerstyle['bannershape_padding'] == 'medium' ? 'selected' :''}} >Medium</option>
                  <option value="small"  {{ !empty( $bannerstyle['bannershape_padding'] ) && $bannerstyle['bannershape_padding'] == 'small' ? 'selected' :''}} >Small</option>
                </select>
              </div>
            </div>
             <div class="form-group row banner_text_show d-none">
              <label for="staticEmail" class="col-6 col-sm-5 col-md-4 col-form-label ">Left Shape</label>
              <div class="col-sm">
                <select id="bannerleftStyle" class="form-control" name="bannerstyle[left]" onchange="applyBannerStyle(this,'Left');">
                  <option value="1" {{ isset( $bannerstyle['left'] ) && $bannerstyle['left'] == 1 ? 'selected':'' }} >Square</option>
                  <option {{ isset( $bannerstyle['left'] ) && $bannerstyle['left'] == 2 ? 'selected':'' }} value="2" {{ empty( $bannerstyle ) ? 'selected':'' }}>Round</option>
                  {{-- <option {{ isset( $bannerstyle['left'] ) && $bannerstyle['left'] == 3 ? 'selected':'' }} value="3">Angled</option>
                  <option {{ isset( $bannerstyle['left'] ) && $bannerstyle['left'] == 4 ? 'selected':'' }} value="4">Ribbon</option> --}}
                </select>
              </div>
            </div>
            <div class="form-group row banner_text_show d-none">
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Right Shape</label>
              <div class="col-sm">
                <select id="bannerrightStyle" class="form-control" name="bannerstyle[right]" onchange="applyBannerStyle(this,'Right');">
                  <option {{ isset( $bannerstyle['right'] ) && $bannerstyle['right'] == 1 ? 'selected':'' }} value="1">Square</option>
                  <option {{ isset( $bannerstyle['right'] ) && $bannerstyle['right'] == 2 ? 'selected':'' }} value="2" {{ empty( $bannerstyle ) ? 'selected':'' }} >Round</option>
                  {{-- <option {{ isset( $bannerstyle['right'] ) && $bannerstyle['right'] == 3 ? 'selected':'' }} value="3">Angled</option>
                  <option {{ isset( $bannerstyle['right'] ) && $bannerstyle['right'] == 4 ? 'selected':'' }} value="4" {{ empty( $bannerstyle ) ? 'selected':'' }} >Ribbon</option> --}}
                </select>
              </div>
            </div>
              <div class="form-group row banner_text_show">
                <label for="text" class="col-12 col-md-4 col-form-label">Background</label>
                <div class="col-auto">
                <div class="row ">
                  <div class="col-4 col-md-3 d-none">
                    <div class="form-check ">
                    <input class="form-check-input bgcolortype" type="radio" name="display[bgcolortype]" id="bgcolortype" value="1" {{ !empty( $display['bgcolortype'] ) && $display['bgcolortype'] == 1 ? 'checked' :''}} {{ empty( $display['bgcolortype'] ) ? 'checked' :''}}>
                      <label class="form-check-label" for="inlineRadio1">Simple</label>
                    </div>
                  </div>
                  <div class="col-auto">
                    <div id="bg-color-picker" class="input-group colorpicker-element">
                      <input type="text" name="display[bgcolor]" class="form-control buttonsettingBgcol " id="buttonsettingBgcol" value="{{ !empty( $display['bgcolor'] ) ? $display['bgcolor'] :'#b6d7a8'}}">
                    </div>
                  </div>
                </div>
                <div class="row d-none">
                  <div class="col-4 col-md-3 d-none">
                    <div class="form-check">
                    <input class="form-check-input bgcolortype" type="radio" name="display[bgcolortype]" id="bgcolortype" value="2" {{ !empty( $display['bgcolortype'] ) && $display['bgcolortype'] == 2 ? 'checked' :''}} >
                      <label class="form-check-label" for="inlineRadio2">Gradient</label>
                    </div>
                  </div>
                  <div class="col-sm">
                    <div id="bg-color-picker" class="input-group colorpicker-element">
                      <div class="w-50 ">
                        <input type="text" name="display[gradbgcolorfirst]" class="form-control gradientBgcol_first " id="gradientBgcol_first" value="{{ !empty( $display['gradbgcolorfirst'] ) ? $display['gradbgcolorfirst'] :'#ffc107'}}">
                      </div>
                      <div class="w-50 ">
                        <input type="text" name="display[gradbgcolorsecond]" class="form-control gradientBgcol_second " id="gradientBgcol_second" value="{{ !empty( $display['gradbgcolorsecond'] ) ? $display['gradbgcolorsecond'] :'#ffc107'}}">
                      </div>
                    </div>
                  </div>
                </div>            
              </div>
              <div class="col-auto">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Opacity</label>
                  </div>
                  <input step="0.01" type="number" min="0" max="1" id="banneropacity" class="form-control" name="bannerstyle[opacity]"  value="{{ !empty( $bannerstyle['opacity'] ) ? $bannerstyle['opacity'] :'1'}}">
                </div>
              </div>

                <div class="col-auto">
                      <select id="bannershadow" class="form-control" name="bannerstyle[shadow]">
                    <option value="" selected="">No Shadow</option>
                    <option {{  isset( $bannerstyle['shadow'] ) && $bannerstyle['shadow'] == 'light'  ? 'selected':'' }} {{  empty( $bannerstyle['shadow'] ) ? 'selected':'' }} value="light">Shadow</option>
                  </select>
                </div>
            </div>
            <div class="form-group row banner_text_show">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Title</label>
              <div class="col-auto">
                <div id="text-color-picker" class="input-group colorpicker-element">
                  <input name="display[textcolor]" type="text" class="form-control buttonsettingtextcol override" id="buttonsettingtextcol" value="{{ !empty( $display['textcolor'] ) ? $display['textcolor'] :'#212529'}}">
                </div>
              </div>
               <div class="col-auto">
                <select id="bannertextsize" class="form-control" name="bannerstyle[textsize]" onchange="applyBannerStyle(this,'Text');">
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '8px' ? 'selected':'' }} value="8px">8px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '9px' ? 'selected':'' }} value="9px">9px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '10px' ? 'selected':'' }} value="10px">10px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '11px' ? 'selected':'' }} value="11px">11px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '12px' ? 'selected':'' }} value="12px">12px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '13px' ? 'selected':'' }} value="13px">13px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '14px' ? 'selected':'' }} value="14px"  >14px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '15px' ? 'selected':'' }} value="15px">15px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '16px' ? 'selected':'' }} value="16px">16px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '17px' ? 'selected':'' }} value="17px">17px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '18px' ? 'selected':'' }} value="18px" >18px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '19px' ? 'selected':'' }} value="19px" >19px</option>
                  <option {{ isset( $bannerstyle['textsize'] ) && $bannerstyle['textsize'] == '20px' ? 'selected':'' }} value="20px" {{ empty( $bannerstyle ) ? 'selected':'' }} >20px</option>
                </select>
              </div>
              <div class="col-auto">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                  <button type="button" id="bold" value="2" onclick="applyBannerStyle(this,'Font','2');" class="btn btn-outline-secondary"><span class="fa fa-bold"></span></button>
                  <button type="button" id="italic" value="3" onclick="applyBannerStyle(this,'Font','3');" class="btn btn-outline-secondary"><span class="fa fa-italic"></span></button>
                  <button type="button" id="underline" value="4" onclick="applyBannerStyle(this,'Font','4');" class="btn btn-outline-secondary"><span class="fa fa-underline"></span></button>
                  <button type="button" id="strike" value="5" onclick="applyBannerStyle(this,'Font','5');" class="btn btn-outline-secondary"><span class="fa fa-strikethrough"></span></button>
                </div>
                <input type="hidden" id="fontstyle_banner" name="bannerstyle[fontstyle]" value="{{ !empty( $bannerstyle['fontstyle'] ) ? $bannerstyle['fontstyle'] :'1'}}">
              </div>
            </div>
                
            <div class="form-group row banner_text_show">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Sub-title</label>
              <div class="col-auto">
                <div id="text-color-picker" class="input-group colorpicker-element">
                  <input name="display[subtextcolor]" type="text" class="form-control buttonsettingsubtextcol override" id="buttonsettingsubtextcol" value="{{ !empty( $display['subtextcolor'] ) ? $display['subtextcolor'] :'#212529'}}">
                </div>
              </div>
               <div class="col-auto">
                <select id="bannersubtextsize" class="form-control" name="bannerstyle[subtextsize]" onchange="applyBannerStyle(this,'SubText');">
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '8px' ? 'selected':'' }} value="8px">8px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '9px' ? 'selected':'' }} value="9px">9px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '10px' ? 'selected':'' }} value="10px">10px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '11px' ? 'selected':'' }} value="11px">11px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '12px' ? 'selected':'' }} value="12px"  >12px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '13px' ? 'selected':'' }} value="13px">13px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '14px' ? 'selected':'' }} value="14px" >14px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '15px' ? 'selected':'' }} value="15px" {{ empty( $bannerstyle ) ? 'selected':'' }} >15px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '16px' ? 'selected':'' }} value="16px">16px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '17px' ? 'selected':'' }} value="17px">17px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '18px' ? 'selected':'' }} value="18px" >18px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '19px' ? 'selected':'' }} value="19px" >19px</option>
                  <option {{ isset( $bannerstyle['subtextsize'] ) && $bannerstyle['subtextsize'] == '20px' ? 'selected':'' }} value="20px" >20px</option>
                </select>
              </div>
              <div class="col-auto">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                  <button type="button" id="subbold" value="2" onclick="applyBannerStyle(this,'SubFont','2');" class="btn btn-outline-secondary"><span class="fa fa-bold"></span></button>
                  <button type="button" id="subitalic" value="3" onclick="applyBannerStyle(this,'SubFont','3');" class="btn btn-outline-secondary"><span class="fa fa-italic"></span></button>
                  <button type="button" id="subunderline" value="4" onclick="applyBannerStyle(this,'SubFont','4');" class="btn btn-outline-secondary"><span class="fa fa-underline"></span></button>
                  <button type="button" id="substrike" value="5" onclick="applyBannerStyle(this,'SubFont','5');" class="btn btn-outline-secondary"><span class="fa fa-strikethrough"></span></button>
                </div>
                <input type="hidden" id="subfontstyle_banner" name="bannerstyle[subfontstyle]" value="{{ !empty( $bannerstyle['subfontstyle'] ) ? $bannerstyle['subfontstyle'] :'1'}}">
              </div>
            </div>

            <div class="form-group row ">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label banner_text_show">Logo Height</label>
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label banner_image_show">Image Height</label>
              <div class="col-auto">
                  <div class="input-group">                     
                    <input class="form-control height" type="number" maxlength="3" onKeyPress="if(this.value.length >=3) return false;" placeholder="Enter height in px" name="display[height]" id="height" value="{{ isset( $display['height'] ) ?  $display['height'] :'50' }}" onfocusout="check_banner_height()" >
                    <div class="input-group-append">
                      <span class="input-group-text" id="basic-addon2">px</span>
                    </div>
                  </div>
                <div class="small text-muted">(Max height 300px)</div>
              </div>
              <!-- <span class="mt-2 mx-1"> px </span>-->
            </div>


            <div class="form-group row ">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Border</label>
              <div class="col-auto">
                <select id="bannerbordersize" class="form-control" name="bannerstyle[bordersize]">
                  <option {{  empty( $bannerstyle['bordersize'] ) ? 'selected':'' }} value="" selected="">No</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '1px'  ? 'selected':'' }} value="1px">1px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '1.5px'  ? 'selected':'' }} value="1.5px">1.5px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '2px'  ? 'selected':'' }} value="2px">2px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '2.5px'  ? 'selected':'' }} value="2.5px">2.5px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '3px'  ? 'selected':'' }} value="3px">3px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '3.5px'  ? 'selected':'' }} value="3.5px">3.5px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '4px'  ? 'selected':'' }} value="4px">4px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '4.5px'  ? 'selected':'' }} value="4.5px">4.5px</option>
                  <option {{  isset( $bannerstyle['bordersize'] ) && $bannerstyle['bordersize'] == '5px'  ? 'selected':'' }} value="5px">5px</option>
                </select>
              </div>
              <div class="col-auto">
                <div id="border-color-picker" class="input-group colorpicker-element">
                  <input type="text" name="bannerstyle[bordercolor]" class="form-control borderBgcol" id="borderBgcol" value="{{ !empty( $bannerstyle['bordercolor'] ) ? $bannerstyle['bordercolor'] :'#000000'}}">
                </div>
              </div>
               <div class="col-auto banner_text_show">
                <select id="bannerborderstyle" class="form-control" name="bannerstyle[borderstyle]">
                  <option {{  isset( $bannerstyle['borderstyle'] ) && $bannerstyle['borderstyle'] == 'solid'  ? 'selected':'' }} value="solid" >Solid</option>
                  <option {{  isset( $bannerstyle['borderstyle'] ) && $bannerstyle['borderstyle'] == 'dotted'  ? 'selected':'' }} value="dotted" >Dotted</option>
                  <option {{  isset( $bannerstyle['borderstyle'] ) && $bannerstyle['borderstyle'] == 'dashed'  ? 'selected':'' }} value="dashed" >Dashed</option>
                
                </select>
              </div>

              <div class="col-auto">
                <select id="bannershadowimage" class="form-control" name="bannerstyle[shadowimage]">
                  <option value="" selected="">No Shadow</option>
                  <option {{  isset( $bannerstyle['shadowimage'] ) && $bannerstyle['shadowimage'] == 'light'  ? 'selected':'' }} {{  empty( $bannerstyle['shadowimage'] ) ? 'selected':'' }} value="light">Shadow</option>
                </select>
              </div>

            </div>  
             <!-- <div class="form-group row banner_text_show">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Text Variations</label>

              <div class="col-sm">
                  <div class="input-group">
                <div class="input-group-prepend">
                  <label class="input-group-text" for="inputGroupSelect01">Mobile</label>
                </div>
                <select id="" class="form-control" name="bannerstyle[textmobilesize]" >
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '0' ? 'selected':'' }} value="0" {{ empty( $bannerstyle ) ? 'selected':'' }} >no change</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '6' ? 'selected':'' }} value="6">6px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '7' ? 'selected':'' }} value="7">7px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '8' ? 'selected':'' }} value="8">8px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '9' ? 'selected':'' }} value="9">9px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '10' ? 'selected':'' }} value="10">10px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '11' ? 'selected':'' }} value="11">11px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '12' ? 'selected':'' }} value="12">12px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '13' ? 'selected':'' }} value="13">13px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '14' ? 'selected':'' }} value="14"  >14px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '15' ? 'selected':'' }} value="15">15px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '16' ? 'selected':'' }} value="16">16px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '17' ? 'selected':'' }} value="17">17px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '18' ? 'selected':'' }} value="18" >18px</option>
                  <option {{ isset( $bannerstyle['textmobilesize'] ) && $bannerstyle['textmobilesize'] == '19' ? 'selected':'' }} value="19" >19px</option>
                </select>
               </div>
              </div>
               <div class="col-sm">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Tablet</label>
                    </div>
                    <select id="" class="form-control" name="bannerstyle[texttabletsize]" >
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '0' ? 'selected':'' }} value="0" {{ empty( $bannerstyle ) ? 'selected':'' }} >no change</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '6' ? 'selected':'' }} value="6">6px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '7' ? 'selected':'' }} value="7">7px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '8' ? 'selected':'' }} value="8">8px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '9' ? 'selected':'' }} value="9">9px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '10' ? 'selected':'' }} value="10">10px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '11' ? 'selected':'' }} value="11">11px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '12' ? 'selected':'' }} value="12">12px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '13' ? 'selected':'' }} value="13">13px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '14' ? 'selected':'' }} value="14" >14px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '15' ? 'selected':'' }} value="15">15px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '16' ? 'selected':'' }} value="16">16px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '17' ? 'selected':'' }} value="17">17px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '18' ? 'selected':'' }} value="18" >18px</option>
                      <option {{ isset( $bannerstyle['texttabletsize'] ) && $bannerstyle['texttabletsize'] == '19' ? 'selected':'' }} value="19" >19px</option>
                    </select>
                  </div>
              </div>
              
            </div> -->
      <!--       <div class="form-group row banner_text_show" id="animationdiv" > 
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Effects</label>
              <div class="col-4 ">
                <select id="banneranimation" class="form-control" name="bannerstyle[animation]" onchange="seteffect();">
                  <option {{  empty( $bannerstyle['animation'] ) ? 'selected':'' }} value="No Animation" selected="">No Animation</option>
                  <option {{  isset( $bannerstyle['animation'] ) && $bannerstyle['animation'] == 'slide'  ? 'selected':'' }} value="slide">Slide In</option>
                  <option {{  isset( $bannerstyle['animation'] ) && $bannerstyle['animation'] == 'pop'  ? 'selected':'' }} value="pop">Pop</option>
                  <option {{  isset( $bannerstyle['animation'] ) && $bannerstyle['animation'] == 'groove'  ? 'selected':'' }} value="groove">Groove</option>
                  <option {{  isset( $bannerstyle['animation'] ) && $bannerstyle['animation'] == 'fly'  ? 'selected':'' }} value="fly">Fly</option>
                  <option {{  isset( $bannerstyle['animation'] ) && $bannerstyle['animation'] == 'swerve'  ? 'selected':'' }} value="swerve">Swerve</option>
                </select>
              </div>

            </div> -->
           
<!--            <div class="form-group row banner_text_show d-none">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Shadow</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
               
                  <select id="bannershadow" class="form-control" name="bannerstyle[shadow]">
                    <option {{  empty( $bannerstyle['shadow'] ) ? 'selected':'' }} value="" selected="">None</option>
                    <option {{  isset( $bannerstyle['shadow'] ) && $bannerstyle['shadow'] == 'light'  ? 'selected':'' }} value="light">Light</option>
                    <option {{  isset( $bannerstyle['shadow'] ) && $bannerstyle['shadow'] == 'medium'  ? 'selected':'' }} value="medium">Medium</option>
                    <option {{  isset( $bannerstyle['shadow'] ) && $bannerstyle['shadow'] == 'heavy'  ? 'selected':'' }} value="heavy">Heavy</option>
                  </select>
               
              </div>
            </div>-->

            <!-- <div class="form-group row d-none">
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Font Style</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                <select  id="bannerfontstyle" class="form-control" name="bannerstyle[fontstyle]" onchange="applyBannerStyle(this,'Font');">
                  <option value="1" {{ isset( $bannerstyle['fontstyle'] ) && $bannerstyle['fontstyle'] == 1 ? 'selected':'' }} >Normal</option>
                  <option value="2" {{ isset( $bannerstyle['fontstyle'] ) && $bannerstyle['fontstyle'] == 2 ? 'selected':'' }} >Bold</option>
                  <option value="3" {{ isset( $bannerstyle['fontstyle'] ) && $bannerstyle['fontstyle'] == 3 ? 'selected':'' }} >Italic</option>
                  <option value="4" {{ isset( $bannerstyle['fontstyle'] ) && $bannerstyle['fontstyle'] == 4 ? 'selected':'' }} >Bold/Italic</option>
                </select>
              </div> -->
            </div>
            <div class="form-group row d-none">
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Mobile Text Size</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                <select id="mobiletextsize" class="form-control" name="bannerstyle[mtextsize]" onchange="applyMobleBannerStyle(this,'Text');">
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '12px' ? 'selected':'' }} value="12px">12px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '13px' ? 'selected':'' }} {{ empty( $bannerstyle ) ? 'selected':'' }}  value="13px">13px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '14px' ? 'selected':'' }} value="14px">14px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '15px' ? 'selected':'' }} value="15px">15px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '16px' ? 'selected':'' }} value="16px" >16px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '17px' ? 'selected':'' }} value="17px">17px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '18px' ? 'selected':'' }} value="18px" >18px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '19px' ? 'selected':'' }} value="19px" >19px</option>
                  <option {{ isset( $bannerstyle['mtextsize'] ) && $bannerstyle['mtextsize'] == '20px' ? 'selected':'' }} value="20px" >20px</option>
                </select>
              </div>
            </div>

           
            
            <!-- THIS IS FOR BADGE WITH IMAGE TYPE... -->
            
            
             <!-- <div class="form-group row banner_image_show ml-1 mr-1">
              <label for="text" class="col-12 col-sm-4 col-md-4 col-form-label">Height Variations</label>
              <div class="col">
                <div class="input-group">
                    <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Tablet</label>
                    </div>
                  <input class="form-control height" type="number" maxlength="3" onKeyPress="if(this.value.length >=3) return false;" placeholder="height" name="bannerstyle[imagetabletsize]" id="tabletheight" value="{{ isset( $bannerstyle['imagetabletsize'] )  ? $bannerstyle['imagetabletsize']:'' }}" onfocusout="check_tabletheight()" >
                  <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">px</span>
                  </div>
                </div>
                <div class="small text-muted">(optional)</div>
              </div>
                <div class="col">
                <div class="input-group">
                    <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Mobile</label>
                    </div>
                  <input class="form-control height" type="number" maxlength="3" onKeyPress="if(this.value.length >=3) return false;" placeholder="height" name="bannerstyle[imagemobilesize]" id="moblieheight" value="{{ isset( $bannerstyle['imagemobilesize'] )  ? $bannerstyle['imagemobilesize']:'' }}" onfocusout="check_moblieheight()" >
                  <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">px</span>
                  </div>
                </div>
                <div class="small text-muted">(optional)</div>
              </div>
            </div> -->

            
           

            <div class="form-group row banner_image_show d-none">
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Spacing (px)</label>
              <div class="col-auto">
                <div class="input-group">
                  <input type="text" class="form-control rounded-0" placeholder="Top" value="Top" disabled="">
                  <input type="text" class="form-control" placeholder="Top" value="Bottom" disabled="">
                  <input type="text" class="form-control" placeholder="Top" value="Left" disabled="">
                  <input type="text" class="form-control rounded-0" placeholder="Top" value="Right" disabled="">
                </div>
                <div class="input-group">
                  <input style="border-top-left-radius: 0;" type="number" class="form-control " name="bannerstyle[margintop]" id="bannerleftStyleMargintop" placeholder="" onchange="applyBannerStyle(this,'Margin');" value="{{ isset( $bannerstyle['margintop'] ) ? $bannerstyle['margintop'] : '0' }}">
                  <input type="number" class="form-control" name="bannerstyle[marginbottom]" id="bannerleftStyleMarginbottom" placeholder="" onchange="applyBannerStyle(this,'Margin');" value="{{ isset( $bannerstyle['marginbottom'] ) ? $bannerstyle['marginbottom'] : '0' }}">
                  <input type="number" class="form-control" name="bannerstyle[marginleft]" id="bannerleftStyleMarginleft" placeholder="" onchange="applyBannerStyle(this,'Margin');" value="{{ isset( $bannerstyle['marginleft'] ) ? $bannerstyle['marginleft'] : '0' }}">
                  <input style="border-top-right-radius: 0;" type="number" class="form-control" name="bannerstyle[marginright]" id="bannerleftStyleMarginright" placeholder="" onchange="applyBannerStyle(this,'Margin');" value="{{ isset( $bannerstyle['marginright'] ) ? $bannerstyle['marginright'] : '0' }}">
                </div>
              </div>
            </div>

            <!-- UPTO HERE FOR BADGE WITH IMAGE TYPE... -->
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingThree">
          <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              3. Rules
            </button>
          </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
          <div class="card-body">
            <div class="form-group row mb-1">
            <label for="staticEmail" class="col-12 col-sm-6 col-md-6 col-form-label">Display Conditions</label>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
              <div class="form-check form-check-inline mt-1">
                <input class="form-check-input" type="radio" name="condition" id="condition" value="1" {{isset( $banner['condition'] ) && $banner['condition'] == 1 ? 'checked':'checked'  }}>
                <label class="form-check-label" for="inlineRadio1">All Condition</label>
              </div>
              <div class="form-check form-check-inline">
                <input {{isset( $banner['condition'] ) && $banner['condition'] == 2 ? 'checked':''  }} class="form-check-input" type="radio" name="condition" id="condition" value="2">
                <label class="form-check-label" for="condition">Any Condition</label>
              </div>
            </div>
          </div>
          <div class="row mx-0">
            <table class="table table-sm table-bordered table-striped">
              <colgroup>
                <col width="80%">
                <col width="5%">
              </colgroup>
              <tbody id="appendHtmlAll">
                @if( !empty( $displayrules ) )
                  @foreach( $displayrules as $index => $displayrule )
                    <tr id="appendHtml{{$index== 0 ? '': $index }}">
                      <td>
                        <div class="row px-0 mx-0">
                          <div class="mx-1"  >
                            <select class="form-control" name="rule[{{$index}}][conditiontype]" onchange="addRule(this,'{{$index}}');">
                              <option {{ isset( $displayrule['all_products'] ) ? 'selected':''  }}  value="all_products">All Products</option>
                              <option {{ isset( $displayrule['product'] ) ? 'selected':''  }} value="product" >Select Products</option>
                              <option {{ isset( $displayrule['product_type'] ) ? 'selected':''  }}  value="product_type">Product Type</option>
                              <option {{ isset( $displayrule['product_price'] ) ? 'selected':''  }}  value="product_price">Product Price</option>
                              <option {{ isset( $displayrule['product_vendor'] ) ? 'selected':''  }}  value="product_vendor">Product Vendor</option>
                              <option {{ isset( $displayrule['title'] ) ? 'selected':''  }}  value="title">Product Title</option>
                              <option {{ isset( $displayrule['weight'] ) ? 'selected':''  }}  value="weight">Product Weight</option>
                              <option {{ isset( $displayrule['collection'] ) ? 'selected':''  }} value="collection">Collection</option> 
                              <option {{ isset( $displayrule['page_type'] ) ? 'selected':''  }} value="page_type">Page Type</option>                             
                              <option {{ isset( $displayrule['saleprice'] ) ? 'selected':''  }} value="saleprice" >Sale price</option>
                              <option {{ isset( $displayrule['inventory'] ) ? 'selected':''  }} value="inventory" >Inventory</option>
                              <option {{ isset( $displayrule['tags'] ) ? 'selected':''  }} value="tags" >Product Tags</option>
                              <option {{ isset( $displayrule['createddate'] ) ? 'selected':''  }}  value="createddate">Product Created Date</option>
                              <option {{ isset( $displayrule['publishdate'] ) ? 'selected':''  }}  value="publishdate">Product Published Date</option>
                              <option {{ isset( $displayrule['productmetafield'] ) ? 'selected':''  }}  value="product_metafield">Product Metafield</option>
                              <option {{ isset( $displayrule['product_option'] ) ? 'selected':''  }}  value="product_option">Variants</option>
                              <option {{ isset( $displayrule['customer'] ) ? 'selected':''  }}  value="customer">Customer</option>
                              <option {{ isset( $displayrule['customer_tag'] ) ? 'selected':''  }}  value="customer_tag">Customer Tags</option>
                            </select>
                          </div>
                          
                          @if( isset( $displayrule['createddate'] ) )
                            <div class="mx-1 mt-2"> is within the past </div>
                            <div class="mx-0 px-1 inventorytype" >
                              <input type="number" min="0" max="10000" name="rule[{{$index}}][conditiontype][createddate]" value="{{$displayrule['createddate']}}" class="form-control">
                            </div>
                            <span class="mt-2 mx-1"> days </span>

                          @elseif( isset( $displayrule['publishdate'] ) )
                          <div class="mx-1 mt-2"> is within the past </div>
                            <div class="mx-0 px-1 inventorytype" >
                              <input type="number" min="0" max="10000" name="rule[{{$index}}][conditiontype][publishdate]" value="{{$displayrule['publishdate']}}" class="form-control">
                            </div>
                            <span class="mt-2 mx-1"> days </span>

                          @elseif( isset( $displayrule['all_products'] ) )
                            <div class="mx-1 mt-2"> Banner will be displayed on all products </div>
                            <div class="mx-0 px-1 col inventorytype" >
                              <input type="hidden" name="rule[{{$index}}][conditiontype][all_products]" value="{{$displayrule['all_products']}}" class="form-control">
                            </div>
                            <span class="mt-2 mx-1">  </span> 
                          @elseif( isset( $displayrule['title'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 title" >
                                <select name="rule[{{$index}}][conditiontype][title][type]" class="form-control"  >
                                  <option {{ $displayrule['title']['type'] == '1' ? 'selected':'' }} value="1" >Starts With</option>
                                  <option {{ $displayrule['title']['type'] == '2' ? 'selected':'' }} value="2" >Ends With</option>
                                  <option {{ $displayrule['title']['type'] == '3' ? 'selected':'' }} value="3" >Contains</option>
                                  <option {{ $displayrule['title']['type'] == '4' ? 'selected':'' }} value="4" >Not Contains</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 titlediv col" >
                                <input type="text" name="rule[{{$index}}][conditiontype][title][value]" value="{{ $displayrule['title']['value'] }}" class="form-control" >
                              </div> 
                          @elseif( isset( $displayrule['product_type'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_type][type]" class="form-control"  >
                                  <option {{ $displayrule['product_type']['type'] == '1' ? 'selected':'' }}  value="1">Equal To</option>
                                  <option {{ $displayrule['product_type']['type'] == '2' ? 'selected':'' }} value="2" >Not Equal To</option>
                                  <option {{ $displayrule['product_type']['type'] == '3' ? 'selected':'' }} value="3" >Starts With</option>
                                  <option {{ $displayrule['product_type']['type'] == '4' ? 'selected':'' }} value="4" >Ends With</option>
                                  <option {{ $displayrule['product_type']['type'] == '5' ? 'selected':'' }} value="5" >Contains</option>
                                  <option {{ $displayrule['product_type']['type'] == '6' ? 'selected':'' }} value="6" >Not Contains</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                                <input type="text" name="rule[{{$index}}][conditiontype][product_type][value]" value="{{ $displayrule['product_type']['value'] }}" class="form-control" >
                              </div>

                              @elseif( isset( $displayrule['product_price'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_price][type]" class="form-control"  >
                                  <option {{ $displayrule['product_price']['type'] == '1' ? 'selected':'' }}  value="1">Equal To</option>
                                  <option {{ $displayrule['product_price']['type'] == '2' ? 'selected':'' }} value="2" >Not Equal To</option>
                                  <option {{ $displayrule['product_price']['type'] == '3' ? 'selected':'' }} value="3" >Is Greater Than</option>
                                  <option {{ $displayrule['product_price']['type'] == '4' ? 'selected':'' }} value="4" >Is Less Than</option>
                                 
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                                <input type="number" min="1" name="rule[{{$index}}][conditiontype][product_price][value]" value="{{ $displayrule['product_price']['value'] }}" class="form-control" >
                              </div>
                            
                            @elseif( isset( $displayrule['product_vendor'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_vendor][type]" class="form-control"  >
                                  <option {{ $displayrule['product_vendor']['type'] == '1' ? 'selected':'' }}  value="1">Equal To</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '2' ? 'selected':'' }} value="2" >Not Equal To</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '3' ? 'selected':'' }} value="3" >Starts With</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '4' ? 'selected':'' }} value="4" >Ends With</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '5' ? 'selected':'' }} value="5" >Contains</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '6' ? 'selected':'' }} value="6" >Not Contains</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                                <input type="text" name="rule[{{$index}}][conditiontype][product_vendor][value]" value="{{ $displayrule['product_vendor']['value'] }}" class="form-control" >
                              </div>
                              @elseif( isset( $displayrule['product_option'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_option][type]" class="form-control"  >
                                <option {{ $displayrule['product_option']['type'] == '1' ? 'selected':'' }}  value="1">Product Has Variants</option>
                                  <option {{ $displayrule['product_option']['type'] == '2' ? 'selected':'' }} value="2" >Product Does Not Have Variants</option>
                                  
                                </select>
                              </div>
                              @elseif( isset( $displayrule['customer'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][customer][type]" class="form-control"  >
                                  <option {{ $displayrule['customer']['type'] == '1' ? 'selected':'' }}  value="1">Is Logged In</option>
                                  <option {{ $displayrule['customer']['type'] == '2' ? 'selected':'' }} value="2" >Not Logged In</option>
                                  
                                </select>
                              </div>
                              @elseif( isset( $displayrule['customer_tag'] ) )
                              @if( isset( $displayrule['customer_tag']['type'] ) )
                                  <div class="mx-1 mt-2"> </div>
                                  <div class="mx-0 px-1 inventorytype" >
                                    <select name="rule[{{$index}}][conditiontype][customer_tag][type]" class="form-control"  >
                                      <option {{ $displayrule['customer_tag']['type'] == '1' ? 'selected':'' }} value="1">Equals To</option>
                                      <option {{ $displayrule['customer_tag']['type'] == '2' ? 'selected':'' }} value="2" >Not Equals To</option>
                                    </select>
                                  </div>
                                  <div class="mx-0 col">
                                    <input data-role="customer_taginput" name="rule[{{$index}}][conditiontype][customer_tag][value]" size="1" type="text" class=" form-control customer_tag tags" id="customer_tag" placeholder="Enter customer tags" value="{{$displayrule['customer_tag']['value']}}">
                                  </div>
                                @else
                                <div class="mx-1 mt-2"> </div>
                                  <div class="mx-0 px-1 inventorytype" >
                                    <select name="rule[{{$index}}][conditiontype][customer_tag][type]" class="form-control"  >
                                      <option selected value="1">Equals To</option>
                                      <option value="2" >Not Equals To</option>
                                    </select>
                                  </div>
                                  <div class="mx-0 col">
                                    <input data-role="customer_taginput" name="rule[{{$index}}][conditiontype][customer_tag]" size="1" type="text" class=" form-control customer_tag tags" id="customer_tag" placeholder="Enter customer tags" value="{{$displayrule['customer_tag']}}">
                                  </div>
                                @endif
                            @elseif( isset( $displayrule['weight'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][weight][type]" class="form-control"  >
                                  <option {{ $displayrule['weight']['type'] == '1' ? 'selected':'' }} value="1">Equal To</option>
                                  <option {{ $displayrule['weight']['type'] == '2' ? 'selected':'' }} value="2" >Less Than</option>
                                  <option {{ $displayrule['weight']['type'] == '3' ? 'selected':'' }} value="3" >Greater Than</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}}" >
                                <input type="number" min="1" name="rule[{{$index}}][conditiontype][weight][value]" value="{{ $displayrule['weight']['value'] }}" class="form-control" >
                              </div>
                              <span class="mt-2 mx-1"> grams </span>
                            @elseif( isset( $displayrule['collection'] ) )
                              @if( isset( $displayrule['collection']['type'] ) )
                                <div class="mx-1 mt-2"> </div>
                                <div class="mx-0 px-1 inventorytype" >
                                  <select name="rule[{{$index}}][conditiontype][collection][type]" class="form-control"  >
                                    <option {{ $displayrule['collection']['type'] == '1' ? 'selected':'' }} value="1">Equals</option>
                                    <option {{ $displayrule['collection']['type'] == '2' ? 'selected':'' }} value="2" >Excludes</option>
                                  </select>
                                </div>

                                <div class="mx-0 col">
                                  <btn  onclick="openCollectionModel('#Collection-model',{{$index}});"  type="text" class="btn btn-secondary" >Select collections</btn>
                                  <input type="hidden"  name="rule[{{$index}}][conditiontype][collection][value]" type="text" class="form-control" id="addCollection_{{$index}}" placeholder="Selecte Collections " value="{{$displayrule['collection']['value']}}">
                                </div>
                                
                                <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                                  <i><span id="collectionscount_{{$index}}">
                                  @if( !empty( array_filter($displayrule['collection'] ) ) )
                                    {{count( explode(",",$displayrule['collection']['value'] )  ) }}
                                  @else
                                    0
                                  @endif
                                  </span> selected</i>
                                </span>
                              @else
                                <div class="mx-1 mt-2"> </div>
                                <div class="mx-0 px-1 inventorytype" >
                                  <select name="rule[{{$index}}][conditiontype][collection][type]" class="form-control"  >
                                    <option selected value="1">Equals To</option>
                                    <option value="2" >Excludes</option>
                                  </select>
                                </div>

                                <div class="mx-0 col">
                                  <btn  onclick="openCollectionModel('#Collection-model',{{$index}});"  type="text" class="btn btn-secondary" >Select collections</btn>
                                  <input type="hidden"  name="rule[{{$index}}][conditiontype][collection][value]" type="text" class="form-control" id="addCollection_{{$index}}" placeholder="Selecte Collections " value="{{$displayrule['collection'][0]}}">
                                </div>
                                <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                                  <i><span id="collectionscount_{{$index}}">
                                  @if( !empty( $displayrule['collection'][0] ) )
                                    {{count( explode(",",$displayrule['collection'][0] )  ) }}
                                  @else
                                    0
                                  @endif
                                  </span> selected</i>
                                </span>
                              @endif
                               
                          @elseif( isset( $displayrule['page_type'] ) )
                          @php
                            $counter = 0;
                          @endphp
                          <span class="mt-2 mx-1">equals</span>
                          <div class="mx-1">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                -- Select --
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="page_list" style="max-height:300px;overflow:auto;">
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'product' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="product" > Product Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                    <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput" 
                                    @php
                                      if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                        foreach( $displayrule['page_type'] as $key => $value ) {
                                          if($value == 'index' ){ $counter++; echo 'checked'; }
                                        }
                                      }
                                    @endphp
                                      value="index" > Index Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'collection' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="collection" > Collection Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'article' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="article" > Article Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'blog' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="blog" > Blog Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'cart' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="cart" > Cart Page
                                      </span>
                                  </li>
                                </ul>
                                <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                                <i>
                                <span id="pagescount">
                                    {{$counter}}
                                </span> selected</i>
                                </span>
                            </div>
                          </div>
                        
                          @elseif( isset( $displayrule['product'] ) )
                            <span class="mt-2 mx-1">equals</span>
                            <div class="mx-1">
                              {{-- <input name="rule[{{$index}}][conditiontype][product][]" type="text" class="form-control" id="bannertitle" placeholder="Selected products " value=""> --}}
                              <btn  onclick="openProductModel('#blacklistProduct-model',{{$index}});"  type="text" class="btn btn-secondary selectproduct" >Select products</btn>
                              <input type="hidden"  name="rule[{{$index}}][conditiontype][product][]" type="text" class="form-control product_csv" id="addProduct_{{$index}}" placeholder="Selected products " value="{{implode(",", $displayrule['product'])}}">
                            </div>
                            <a href="javascript:void(0);" class="mt-2 selectedtext" onclick="openProductModel('#blacklistProduct-model','{{$index}}',2);" >
                              <span class="text-primary pointer small" >
                                <i>
                                  <span id="productcount_{{$index}}" class="count_by_csv">
                                    @if( !empty( array_filter($displayrule['product'] ) ) )
                                      {{count( explode(",",implode(",", $displayrule['product'] ) )  ) }}
                                    @else
                                      0
                                    @endif
                                  </span>selected
                                </i>
                              </span>
                            </a>
                          @elseif( isset( $displayrule['saleprice'] ) )

                          <?php 
                            if( strpos($displayrule['saleprice'], ',') !== false ) {
                              $newsalerule = explode(",",$displayrule['saleprice']);
                              $sale_start = $newsalerule[0];
                              $sale_end = $newsalerule[1];
                            }else{
                              $sale_start = $displayrule['saleprice'];
                              $sale_end = '100';
                            }

                          ?>

                          <span class="mt-2 mx-1"> is between </span>
                            <div class="col mx-0 px-0 " >

                              <input type="number" min="1" max="100" name="rule[{{$index}}][conditiontype][saleprice][start]" value="{{isset( $sale_start ) && $sale_start !=''  ? $sale_start :'' }}" class="form-control" required>
                            </div>
                            <span class="mt-2 mx-1"> % and </span>
                            <div class="col mx-0 px-0 " >
                              

                              <input type="number" min="1" max="100" name="rule[{{$index}}][conditiontype][saleprice][end]" value="{{isset( $sale_end ) && $sale_end !=''  ? $sale_end :'' }}" class="form-control" required>
                            </div>
                            <span class="mt-2 mx-1"> %</span>
                            <input type="hidden" name="rule[{{$index}}][conditiontype][saleprice]" value="" class="form-control" >
                          @elseif( isset( $displayrule['inventory'] ) )
                          <span class="mt-2 mx-1">is</span>
                            <div class="mx-1" >
                              <select name="rule[{{$index}}][conditiontype][inventory][type]" class="form-control selectedinventory" onchange="inventoryType(this,{{$index}});">
                                <option {{ $displayrule['inventory']['type'] == 1 ? 'selected':'' }} value="1">between</option>
                                <option {{ $displayrule['inventory']['type'] == 2 ? 'selected':'' }} value="2" >in stock</option>
                                <option {{ $displayrule['inventory']['type'] == 3 ? 'selected':'' }} value="3" >out of stock</option>
                                <option {{ $displayrule['inventory']['type'] == 4 ? 'selected':'' }} value="4" >out of stock but continue selling</option>
                              </select>
                            </div>

                            <div class="col mx-0 px-0 inventorytype inventoryhide" >
                              <input type="number" min="1" max="100" name="rule[{{$index}}][conditiontype][inventory][start]" value="{{ isset( $displayrule['inventory']['start'] ) && $displayrule['inventory']['start'] !='' ?  $displayrule['inventory']['start'] : 1 }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )">
                            </div>
                            <span class="inventorytype {{$inventoryHide}} mx-1 mt-2 inventoryhide">and </span>
                            
                            <div class="col mx-0 px-0 inventorytype inventoryhide" >
                              <input type="number" min="1"  name="rule[{{$index}}][conditiontype][inventory][value]" value="{{ $displayrule['inventory']['value'] }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                             
                            </div>
                          @elseif( isset( $displayrule['tags'] ) )
                            @if( isset( $displayrule['tags']['type'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][tags][type]" class="form-control"  >
                                  <option {{ $displayrule['tags']['type'] == '1' ? 'selected':'' }} value="1">Equals To</option>
                                  <option {{ $displayrule['tags']['type'] == '2' ? 'selected':'' }} value="2" >Excludes To</option>
                                </select>
                              </div>
                              <div class="mx-0 col">
                                <input data-role="tagsinput" name="rule[{{$index}}][conditiontype][tags][value]" size="1" type="text" class=" form-control tags tags" id="tags" placeholder="Enter product tags" value="{{$displayrule['tags']['value']}}">
                              </div>
                            @else
                            <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][tags][type]" class="form-control"  >
                                  <option selected value="1">Equals To</option>
                                  <option value="2" >Excludes To</option>
                                </select>
                              </div>
                              <div class="mx-0 col">
                                <input data-role="tagsinput" name="rule[{{$index}}][conditiontype][tags][value]" size="1" type="text" class=" form-control tags tags" id="tags" placeholder="Enter product tags" value="{{$displayrule['tags']}}">
                              </div>
                            @endif
                          
                          @elseif( isset( $displayrule['productmetafield'] ) )
                          <span class="mt-2 mx-1"><a href="#" data-toggle="modal" data-target="#metafieldModal">name</a></span>
                            <div class="col mx-0 px-0 productmetafield metanameval" >
                              <input type="text" min="1" max="100" name="rule[{{$index}}][conditiontype][productmetafield][metaname]" value="{{ $displayrule['productmetafield']['metaname'] }}" class="form-control" required>
                            </div>
                            <div class="mx-0 px-0 productmetafield " >
                              <select name="rule[{{$index}}][conditiontype][productmetafield][metacondition]" id="metafieldcond" class="form-control metafieldcond"  >
                                <option {{ $displayrule['productmetafield']['metacondition'] == 1 ? 'selected':'' }} value="1">Equals</option>
                                <option {{ $displayrule['productmetafield']['metacondition'] == 2 ? 'selected':'' }} value="2" >Found</option>
                                <option {{ $displayrule['productmetafield']['metacondition'] == 3 ? 'selected':'' }} value="3" >Not Found</option>
                              </select>
                            </div>
                            <div class="col mx-0 px-0 productmetafield metafieldkey" id="metafieldkey" >
                            <input type="text" min="1" max="100" name="rule[{{$index}}][conditiontype][productmetafield][metaval]" value="{{ $displayrule['productmetafield']['metaval'] }}" class="form-control" required>
                            </div>
                          @endif
                        </div>
                      </td>
                      <td>
                        <div class="text-center mt-2">
                          <a disabled onclick="deleteRule('#appendHtml{{ $index== 0 ? '': $index }}');" href="javascript:void(0)"><i class="fa fa-times text-danger " aria-hidden="true"></i></a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                @else
                  <tr id="appendHtml">
                    <td>
                      <div class="row mx-0 px-0">
                        <div class="mx-1"  >
                          <select class="form-control" name="rule[0][conditiontype]" onchange="addRule(this,0);">
                            <option  selected value="all_products">All Products</option>
                            <option value="product" >Select Products</option>
                            <option value="product_type" >Product Type</option>
                            <option value="product_price" >Product Price</option>
                            <option value="product_vendor" >Product Vendor</option>
                            <option value="title" >Product Title</option>
                            <option value="weight" >Product Weight</option>
                            <option value="collection">Collection</option>  
                            <option value="page_type">Page Type</option>                            
                            <option value="saleprice" >Sale price</option>
                            <option value="inventory" >Inventory</option>
                            <option value="tags" >Product Tags</option>
                            <option  value="createddate">Product Created Date</option>
                            <option  value="publishdate">Product Published Date</option>
                            <option  value="product_metafield">Product Metafield</option>
                            <option  value="product_option">Variants</option>
                            <option  value="customer">Customer</option>
                            <option  value="customer_tag">Customer Tags</option>
                          </select>
                        </div>
                        <span class="mt-2 mx-1"> Banner will be displayed on all products </span>
                        <div class="mx-0 px-1 col inventorytype" >
                          <input type="hidden" name="rule[0][conditiontype][all_products]" value="all_products" class="form-control">
                        </div>
                        <span class="mt-2 mx-1"> </span>

                      </div>
                    </td>
                    <td>
                      <div class="text-center mt-2">
                        <a disabled onclick="deleteRule('#appendHtml');" href="javascript:void(0)">
                          <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>

          <div class="float-left ExProduct">
          
              <btn  onclick="openExProductModel('#ExProduct-model');"  type="text" class="btn btn-outline-secondary" >Exclude products</btn>
              <input type="hidden"  name="excludeproduct" type="text" class="form-control" id="addExProduct" placeholder="Selected products " value="{{ ( isset( $banner['excludeproduct'] ) && ( $banner['excludeproduct'] != '' )) ? implode(",", $banner['excludeproduct']) :''}}">
           
            <a href="javascript:void(0);" class="mt-2 ml-2" onclick="openExProductModel('#ExProduct-model','',2);" >
              <span class="text-primary pointer small" >
                <i>
                  <span id="Exproductcount">
                  @if( ( isset( $banner['excludeproduct'] ) && ( $banner['excludeproduct'] != '' )) )
                    @if( !empty( array_filter($banner['excludeproduct'] ) ) )
                      {{count( explode(",",implode(",", $banner['excludeproduct'] ) )  ) }}
                    @else
                      0
                    @endif
                  @else
                    0
                  @endif
                  </span> excluded
                </i>
              </span>
            </a>
            <!-- <button type="button" class="btn btn-secondary" onclick="addRuleHtml();">Add Rule</button> -->
          </div>

          <div class="mt-0 text-right">
            <button type="button" class="btn btn-secondary" onclick="addRuleHtml();">Add Rule</button>
          </div>

          <div class="row">
            <div class="col-12">
              <hr>
              <p>Pro Conditions 
              @if(Auth::User()->planid < 3)
              <span class="fa fa-lock text-muted ml-2"></span>
              @endif
              </p>
              <div class="location">
                <btn  onclick="openCountryModel('#Country-model');"  type="text" class="btn btn-outline-secondary" >Country Restriction <span id="country_count" class="badge badge-primary"></span></btn>
                <input type="hidden"  name="customer_location" type="text" class="form-control" id="addCountry_counter" placeholder="Selected products " value="{{ ( isset( $banner['customer_location'] ) && ( $banner['customer_location'] != '' )) ? implode(",", $banner['customer_location']) :''}}">

                <btn  onclick="openShopLanguageModel('#ShopLanguage-model');"  type="text" class="btn btn-outline-secondary" >Language <span id="language_count" class="badge badge-primary"></span></btn>
                <input type="hidden"  name="shop_language" type="text" class="form-control" id="addShopLanguage_counter" placeholder="Select Languages " value="{{ ( isset( $banner['shop_language'] ) && ( $banner['shop_language'] != '' )) ? implode(",", $banner['shop_language']) :''}}" >

                <btn  onclick="openOrderCountModel('#OrderCount-model');"  type="text" class="btn btn-outline-secondary" >Order Count</btn>

                <!-- <span class="text-primary small" >
                  <i>
                    <span id="customer_location_count">
                    @if( ( isset( $banner['customer_location'] ) && ( $banner['customer_location'] != '' )) )
                      @if( !empty( array_filter($banner['customer_location'] ) ) )
                        {{ count( explode(",",implode(",", $banner['customer_location'] ) )  ) }}
                      @else
                        0
                      @endif
                    @else
                      0
                    @endif
                    </span> Selected
                  </i>
                </span> -->
              </div>
            </div>
          </div>

        </div>
      </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          4. Activate
        </button>
      </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Banner Visibility</label>
          <div class="col-12 col-sm-7 col-md-8 col-lg-8">
            <div class="mt-2">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" name="enabledate" id="startingnow"  class="custom-control-input"  onclick="check_enable_date()" value="0" checked  {{ ( isset( $banner['enabledate'] ) && ( $banner['enabledate'] == 0 )) ? 'checked' :'' }}>
                  <label class="custom-control-label" for="startingnow">Visible</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" name="enabledate" id="enabledate"  class="custom-control-input" onclick="check_enable_date()" value="1"  {{ ( isset( $banner['enabledate'] ) && ( $banner['enabledate'] == 1 )) ? 'checked' :'unchecked' }} >
                  
                  <label class="custom-control-label" for="enabledate">Set Visibility Date</label>
                </div>
            </div>
          </div>
        </div>
        <!--  <div class="form-group row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="control">
              <label class="radio">
                <input type="radio" name="enabledate" id="banner_image" 
                {{ ( isset( $banner['enabledate'] ) && ( $banner['enabledate'] == 0 )) ? 'checked' :'' }}
                  value="image" onclick="check_enable_date();" >
                  Show banner all the time
              </label>
              <label class="radio">
                <input type="radio" name="enabledate" id="enabledate" checked 
                {{ ( isset( $banner['enabledate'] ) && ( $banner['enabledate'] == 1 )) ? 'checked' :'unchecked' }} 
                value="text" onclick="check_enable_date();" >
                  Schedule banner display
              </label>
            </div>
          </div>
        </div>-->
        
            <div class="form-group row date">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Start</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                <div class="flatpickr form-group input-group date" id='datetimepicker1'>
                <input type="text" class="form-control" id="input_datetimepicker1"  name="starttime" placeholder="Select Start Date" 
                  <?php date_default_timezone_set($mytimezone); ?>
                value="{{isset( $banner['starttime'] ) ? date('d-m-Y H:i A', $banner['starttime']) :'' }}" data-input>
              </div>
              </div>
            </div>
            <div class="form-group row date">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">End</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                   <div class="flatpickr form-group input-group date" id='datetimepicker2'>
                <input type="text" class="form-control" id="input_datetimepicker2" name="endtime" value="{{isset( $banner['endtime'] ) ? date('d-m-Y H:i A', $banner['endtime']) :'' }}" placeholder="Select End Date" data-input>
              </div>
              </div>
            </div>

<!--        <div class="form-group row">
          <div class="col-5 col-sm-5 col-md-5 col-lg-5">
            <div class="form-group">
              <label for="starttime" class="col-12 col-sm-6 col-md-6 col-form-label">Start Time</label>  
              <div class="flatpickr form-group input-group date" id='datetimepicker1'>
                <input type="text" class="form-control" disabled id="input_datetimepicker1"  name="starttime" placeholder="Select Start Date" 
                  <?php date_default_timezone_set($mytimezone); ?>
                value="{{isset( $banner['starttime'] ) ? date('d-m-Y H:i A', $banner['starttime']) :'' }}" data-input>
              </div>
            </div>
          </div>
          <div class='col-sm-2 form-group'></div>
          <div class="col-5 col-sm-5 col-md-5 col-lg-5">
            <div class="form-group ">
              <label for="endtime" class="col-12 col-sm-6 col-md-6 col-form-label">End Time</label>  
              <div class="flatpickr form-group input-group date" id='datetimepicker2'>
                <input type="text" class="form-control" id="input_datetimepicker2"  disabled name="endtime" value="{{isset( $banner['endtime'] ) ? date('d-m-Y H:i A', $banner['endtime']) :'' }}" placeholder="Select End Date" data-input>
              </div>
            </div>
          </div>
        </div>-->

        <div class="form-group row">
        
        <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Banner Group<div><a href="https://primeappassist.freshdesk.com/support/solutions/articles/81000385793-banner-groups-coming-soon- 2" target="_blank" class="small" style="">More info<span class="fa fa-caret-right ml-1"></span></a></div></label>
          <div class="col-12 col-sm-5 col-md-4 col-form-label ">
            <div class="mt-0">
              <div class="input-group">
                @if( !empty( $banner )  )
                  @if( Auth::User()->planid > 2 )
                    <select name="bannergroup" id="" class="form-control metafieldcond" >
                      <option {{ $banner['bannergroup'] == 1 ? 'selected':'' }} value="1">Group 1</option>
                      <option {{ $banner['bannergroup'] == 2 ? 'selected':'' }} value="2" >Group 2</option>
                      <option {{ $banner['bannergroup'] == 3 ? 'selected':'' }} value="3" >Group 3</option>
                    </select>
                  @else
                    <select name="bannergroup" id="" class="form-control metafieldcond" disabled >
                      <option {{ $banner['bannergroup'] == 1 ? 'selected':'' }} value="1">Group 1</option>
                      <!-- <option {{ $banner['bannergroup'] == 2 ? 'selected':'' }} value="2" >2</option>
                      <option {{ $banner['bannergroup'] == 3 ? 'selected':'' }} value="3" >3</option> -->
                    </select>
                  @endif
                @else
                  @if( Auth::User()->planid > 2 )
                    <select name="bannergroup" id="" class="form-control metafieldcond"  >
                      <option selected value="1">Group 1</option>
                      <option value="2" >Group 2</option>
                      <option value="3" >Group 3</option>
                    </select>
                  @else
                    <select name="bannergroup" id="" class="form-control metafieldcond" disabled >
                      <option selected value="1">Group 1</option>
                      <option value="2" >Group 2</option>
                      <option value="3" >Group 3</option>
                    </select>
                  @endif
                @endif
              </div>
                
            </div>
          </div>
        </div>
        
        <div class="form-group row">
          <label for="Activate" class="col-12 col-sm-5 col-md-4 col-form-label">Activate</label>
          <div class="col-12 col-sm-7 col-md-8 col-lg-8">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              @if( !empty( $banner )  )
                <label class="btn btn-outline-primary {{  $banner['isactive'] == 1 ? 'active':'' }} ">
                  <input  {{  $banner['isactive'] == 1 ? 'checked':'' }}  type="radio" name="active" value="1" autocomplete="off" > ON
                </label>
                <label class="btn btn-outline-primary {{ $banner['isactive'] == 0 ? 'active':'' }}">
                  <input {{ $banner['isactive'] == 0 ? 'checked':'' }} type="radio" name="active" value="0" autocomplete="off" > OFF
                </label>
              @else
                <label class="btn btn-outline-primary active ">
                  <input checked type="radio" name="active" value="1" autocomplete="off" > ON
                </label>
                <label class="btn btn-outline-primary ">
                  <input  type="radio" name="active" value="0" autocomplete="off" > OFF
                </label>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body text-right">
      <button type="button" onclick="location.href='{{asset('banner')}}'" class="btn btn-default">Cancel</button>
      <button type="button" onclick="saveRule('#formRule');" class="btn btn-primary savebtn">Save</button>
    </div>
  </div>
</div>

<div class="modal fade " id="OrderCount-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Order Count Condition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if(Auth::User()->planid > 2)
        <div class="modal-body">
          <div class="container-fluid">
            
            <div class="">
              <div class="card-group">
                  <p class="text-muted">Show banners when</p>
                <!-- <div class="card"> -->
                  <!-- <div class="card-header">
                      List of countries (<span id=addedcount>0</span>)
                      <button   onclick="addCountryAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div>                -->
                  <!-- <div class="card-body p-1">
                    <div class="row px-0 mx-0"> -->
                      @if( !empty( $displayprorules ) )
                        @foreach( $displayprorules as $index => $displayProrule )
                         
                            @if( isset( $displayProrule['order_count'] ) )
                             <div class="row mx-0 px-0">
                              <div class="mx-1 mt-1"  >
                                <p class="my-1">Total Order Count&nbsp;</p>
                                <!-- <p class="my-1">Order Count</p> -->
                              </div>
                                <div class="mx-1 mt-2 mt-1"> </div>
                                <div class="mx-0 px-1 Proinventorytype mt-1" >
                                  <select name="prorule[0][conditiontype][order_count][type]" class="form-control"  >
                                    <option {{ $displayProrule['order_count']['type'] == '1' ? 'selected':'' }}  selected value="1" >Is Greater Than</option>
                                    <option {{ $displayProrule['order_count']['type'] == '2' ? 'selected':'' }} value="2" >Is Less Than</option>
                                  </select>
                                </div>
                                <span class="mt-2 mx-1 mt-1">  </span>
                                <div class="mx-1 Proinventorytype col mt-1" >
                                  <input type="number" min="0" id="saveordercount" name="saveordercount" value="{{ $displayProrule['order_count']['value'] }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                                  <input type="hidden" min="0" name="prorule[0][conditiontype][order_count][value]" value="{{ $displayProrule['order_count']['value'] }}" class="form-control" >
                                </div>
                              </div>
                            @endif
                          
                          
                            @if( isset( $displayProrule['recentorder_count'] ) )
                              <div class="row mx-0 px-0">
                                <div class="mx-1 mt-1"  >
                                  <!-- <p class="my-1">Recent Sold </p> -->
                                  <p class="my-1">Recent Sold Count</p>
                                </div>
                                <div class="mx-1 mt-2 mt-1"> </div>
                                <div class="mx-0 px-1 Proinventorytype mt-1" >
                                  <select name="prorule[1][conditiontype][recentorder_count][type]" class="form-control"  >
                                    <option {{ $displayProrule['recentorder_count']['type'] == '1' ? 'selected':'' }}  selected value="1" >Is Greater Than</option>
                                    <option {{ $displayProrule['recentorder_count']['type'] == '2' ? 'selected':'' }} value="2" >Is Less Than</option>
                                  </select>
                                </div>
                                <span class="mt-2 mx-1 mt-1">  </span>
                                <div class="mx-1 Proinventorytype col mt-1" >
                                  <input type="number" min="0" id="saverecentordercount" name="saverecentordercount" value="{{ $displayProrule['recentorder_count']['value'] }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                                  <input type="hidden" min="0" name="prorule[1][conditiontype][recentorder_count][value]" value="{{ $displayProrule['recentorder_count']['value'] }}" class="form-control" >
                                </div>
                              </div>
                            @endif
                        @endforeach
                      @else

                        <div class="row mx-0 px-0">
                          <div class="mx-1 mt-1"  >
                            <p class="my-1">Total Order Count&nbsp;</p>
                            <!-- <p class="my-1">Order Count</p> -->
                          </div>
                          <div class="mx-1 mt-2 mt-1"> </div>
                          <div class="mx-0 px-1 Proinventorytype mt-1" >
                            <select name="prorule[0][conditiontype][order_count][type]" class="form-control"  >
                              <option selected value="1" >Is Greater Than</option>
                              <option value="2" >Is Less Than</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1 mt-1">  </span>
                          <div class="mx-1 Proinventorytype col mt-1" >
                            <input type="number" min="0" id="saveordercount" name="saveordercount" value="" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                            <input type="hidden" min="0"  name="prorule[0][conditiontype][order_count][value]" value="" class="form-control" >
                          </div>
                        </div>
                        <div class="row mx-0 px-0">
                          <div class="mx-1 mt-1"  >
                            <!-- <p class="my-1">Recent Sold </p> -->
                            <p class="my-1">Recent Sold Count</p>
                          </div>
                          <div class="mx-1 mt-2 mt-1"> </div>
                          <div class="mx-0 px-1 Proinventorytype mt-1" >
                            <select name="prorule[1][conditiontype][recentorder_count][type]" class="form-control"  >
                              <option selected value="1" >Is Greater Than</option>
                              <option value="2" >Is Less Than</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1 mt-1">  </span>
                          <div class="mx-1 Proinventorytype col mt-1" >
                            <input type="number" min="0" id="saverecentordercount" name="saverecentordercount" value="" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                            <input type="hidden" min="0"  name="prorule[1][conditiontype][recentorder_count][value]" value="" class="form-control" >
                          </div>
                        </div>
                      @endif
                    <!-- </div>
                  </div>
                </div> -->
              </div>

            </div>
            <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
              <div class="alert bg-dark">
                <h4 class="text-white" id="requestmessage_collection"></h4>
              </div>
            </div>

          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" onclick="location.href='{{asset('settings-order-count')}}'" class="btn btn-outline-primary">Settings</button>
          <button type="button" onclick="saveOrderCount();" class="btn btn-primary">Save</button>
          
        </div>
      @else
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
          <p class="alert alert-danger w-100">This feature is available only on the Pro Plan.
                  <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
              </p>
              
         </div>
        </div>
      </div>

      @endif
    </div>
  </div>
</div>

    </form>
</div>
 

<div class="col-12 col-sm-12 col-md-5 mb-3 d-none">
  <!--style="position:fixed;top:0;z-index: 999;"-->
  <div class="card" style="">
    <div class="card-header">Preview
    </div>
    <div class="card-body text-center">
      <div class="d-none">
        <img src="https://dummyimage.com/200x200/ddd/000&text=Product+Image" class="img-fluid">
      </div>
      <div class="font-weight-bold d-none">Product Title
      </div>
  <!--      <div class="m-2 " id="bannerStyle" >
        <span class="spanStyleleft" ></span>
          <div id="textStyle" class="bannertitle">Text</div>
        <span id="spanStyle"></span>
      </div>
      <div class="m-2 " id="bannerStyle_image" >
        <span class="spanStyleleft" ></span>
          <img src="{{ isset( $banner['productbannerid'] ) ? env('AWS_PATH').$banner['productbannerid'] :'' }}" id="banner_image_preview" style="max-height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'50px' }}" />
          <span id="spanStyle"></span>
      </div>-->
      <div class="card-text d-none">$5.99
      </div>
    </div>
  </div>
  <div class="card mt-3 bg-light d-none">
    <div class="card-body">
      <h5>
        Banner Options
      </h5>
      <div class="small">
        <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000389299-showing-multiple-banners">
            Show multiple banners for a product <span class="fa fa-caret-right"></span>
        </a>
        <br>
        <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000419412--positioning-the-banners">
            Customize the position of the banners <span class="fa fa-caret-right"></span>
        </a>
        <br>
        <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000419487-diplay-the-banners-on-top-of-the-product-images">
            Diplay the banners on top of the product images <span class="fa fa-caret-right"></span>
        </a>
        <br>
        <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000430925-dynamic-banner-text">
            Dynamic banner text <span class="fa fa-caret-right"></span>
        </a>
      </div>
      <!-- <p>Here are some example Star banners you can try:</p>
      <li>Promote New Arrivals using the <b>Create date</b> banner type</li>
      <li>Promote On Sale items using the <b>Sale price</b> banner type</li>
      <li>Promote Scarcity by using the <b>Inventory</b> banner type for items with low or out of stock inventory</li>
      <li>Use the <b>Tag</b> banner type with product tags to promote <em>Best Sellers</em> or <em>Staff Picks</em>.</li>
      <li>Use the drag handle <i class="fa fa-bars"></i> to set the priority for banners to be shown.</li><br>
      <p>For more banner ideas <a href="https://starapp.freshdesk.com/a/solutions/folders/48000253184">click here</a>.</p>
      </div>-->
    </div>
    <div class="mt-3 d-none">
      <div class="card">
        <div class="card-header">Mobile View
        </div>
        <div class="card-body text-center">
        <div>
          <img src="https://dummyimage.com/150x150/ddd/000" class="img-fluid">
        </div>
        <div class="m-2" id="mobibannerStyle">
          <span class="spanStyleleft" ></span>
          <div id="mobitextStyle" class="bannertitle">Text</div>
          <span id="mobispanStyle"></span>
        </div>
        <div class="card-title font-weight-bold">Product Title
        </div>
        <div class="card-text">$5.99
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade bd-example-modal-xl" id="Country-model" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Show the banner for selected countries only</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if(Auth::User()->planid > 2)
        <div class="modal-body">
          <div class="container-fluid">
            
            <div class="">
              <div class="card-group">
                <div class="card">
                  <div class="card-header">
                      List of countries (<span id=addedcount>0</span>)
                      <button   onclick="addCountryAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div>               
                  <div class="card-body p-1">
                    <table id="Countrylist" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Countries</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                        @if( !empty( $banner['customer_location'] ) )
                          <?php
                            $CountryArrayValues2=explode(",",implode(",", $banner['customer_location'] ) );
                          ?>
                        @else
                          <?php
                            $CountryArrayValues2=array();
                          ?>
                        @endif
                        @if( !empty( $countries ) ) 
                          @if( isset( $banner['customer_location'] ) )
                            @foreach( $countries as $country )
                              @if (!in_array($country['code'], $CountryArrayValues2))
                                <tr class="">
                                    <td class="py-1">
                                  {{$country['countryname']}}
                                  </td>
                                  <td class="py-1">                                  
                                    <button  countryid="{{$country['code']}}"   onclick="addCountry(this,'{{$country['code']}}','{{$country['countryname']}}')" class="btn  btn-sm btn-outline-secondary" >
                                      Add
                                    </button>
                                  </td>
                                </tr>
                              @endif
                            @endforeach 
                          @else
                            @foreach( $countries as $country )
                              <tr class="">
                                <td class="py-1">
                                  {{$country['countryname']}}
                                </td>
                                <td class="py-1">
                                  <button  countryid="{{$country['code']}}"   onclick="addCountry(this,'{{$country['code']}}','{{$country['countryname']}}')" class="btn  btn-sm btn-outline-secondary" >
                                        Add
                                    </button>
                                </td>
                              </tr>
                            @endforeach 
                          @endif
                        @endif
                      </tbody>
                    </table>

                 

                  </div>
                   

                </div>
                    <div class="card">
                  <div class="card-header">
                      Selected countries (<span id=removecount>0</span>)
                       <button   onclick="removeCountryAll();" class="btn  btn-sm btn-outline-danger float-right" >
                      Remove All
                    </button>
                  </div>        
                  <div class="card-body p-1">
                    <table id="Countrylistadded" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Countries</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                      @if( !empty( $banner['customer_location'] ) )
                        <?php
                          $CountryArrayValues=explode(",",implode(",", $banner['customer_location'] ) );
                        ?>
                          @foreach( $countries as $country )
                            @if (in_array($country['code'], $CountryArrayValues))
                              <tr class="">
                                <td class="py-1">
                                  {{$country['countryname']}}
                                </td>

                                <td class="py-1">
                                  <button  countryid="{{$country['code']}}"  onclick="RemoveCountry(this,'{{$country['code']}}','{{$country['countryname']}}')" class="btn  btn-sm btn-outline-danger" >
                                    Remove
                                  </button>
                                </td>
                              </tr>
                            @endif
                          @endforeach 
                      @endif
                      </tbody>
                    </table>
                  </div>
                          
                </div>
              </div>
          

            

            </div>

            <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
              <div class="alert bg-dark">
                <h4 class="text-white" id="requestmessage_collection"></h4>
              </div>
            </div>
              
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" onclick="saveCountry();" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      @else
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
              <p class="">Display banners based on user location. This feature is available only on the Pro Plan. <a class="btn btn-danger" href="/plans">Upgrade Now</a>
              </p>
              
         </div>
        </div>
      </div>

      @endif
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-xl" id="ShopLanguage-model" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Show the banner for selected languages only</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if(Auth::User()->planid > 2)
        <div class="modal-body">
          <div class="container-fluid">
            <div class="">
              <div class="card-group">
                <div class="card">
                  <div class="card-header">
                      List of languages (<span id=langaddedcount>0</span>)
                      <button   onclick="addShopLanguageAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div>               
                  <div class="card-body p-1">
                    <table id="ShopLanguagelist" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Languages</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                        @if( !empty( $banner['shop_language'] ) )
                          <?php
                            $ShopLanguageArrayValues2=explode(",",implode(",", $banner['shop_language'] ) );
                          ?>
                        @else
                          <?php
                            $ShopLanguageArrayValues2=array();
                          ?>
                        @endif
                        @if( !empty( $languages ) ) 
                          @if( isset( $banner['shop_language'] ) )
                            @foreach( $languages as $ShopLanguage )
                              @if (!in_array($ShopLanguage['languagecode'], $ShopLanguageArrayValues2))
                                <tr class="">
                                    <td class="py-1">
                                  {{$ShopLanguage['languagename']}}
                                  </td>
                                  <td class="py-1">                                  
                                    <button  ShopLanguageid="{{$ShopLanguage['languagecode']}}"   onclick="addShopLanguage(this,'{{$ShopLanguage['languagecode']}}','{{$ShopLanguage['languagename']}}')" class="btn  btn-sm btn-outline-secondary" >
                                      Add
                                    </button>
                                  </td>
                                </tr>
                              @endif
                            @endforeach 
                          @else
                            @foreach( $languages as $ShopLanguage )
                              <tr class="">
                                <td class="py-1">
                                  {{$ShopLanguage['languagename']}}
                                </td>
                                <td class="py-1">
                                  <button  ShopLanguageid="{{$ShopLanguage['languagecode']}}"   onclick="addShopLanguage(this,'{{$ShopLanguage['languagecode']}}','{{$ShopLanguage['languagename']}}')" class="btn  btn-sm btn-outline-secondary" >
                                        Add
                                    </button>
                                </td>
                              </tr>
                            @endforeach 
                          @endif
                        @endif
                      </tbody>
                    </table>

                  </div>
                   

                </div>
                    <div class="card">
                  <div class="card-header">
                      Selected languages (<span id=langremovecount>0</span>)
                       <button   onclick="RemoveShopLanguageAll();" class="btn  btn-sm btn-outline-danger float-right" >
                      Remove All
                    </button>
                  </div>        
                  <div class="card-body p-1">
                    <table id="ShopLanguagelistadded" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Languages</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                      @if( !empty( $banner['shop_language'] ) )
                        <?php
                          $ShopLanguageArrayValues=explode(",",implode(",", $banner['shop_language'] ) );
                        ?>
                          @foreach( $languages as $ShopLanguage )
                            @if (in_array($ShopLanguage['languagecode'], $ShopLanguageArrayValues))
                              <tr class="">
                                <td class="py-1">
                                  {{$ShopLanguage['languagename']}}
                                </td>

                                <td class="py-1">
                                  <button  ShopLanguageid="{{$ShopLanguage['languagecode']}}"  onclick="RemoveShopLanguage(this,'{{$ShopLanguage['languagecode']}}','{{$ShopLanguage['languagename']}}')" class="btn  btn-sm btn-outline-danger" >
                                    Remove
                                  </button>
                                </td>
                              </tr>
                            @endif
                          @endforeach 
                      @endif
                      </tbody>
                    </table>
                  </div>
                          
                </div>
              </div>

            </div>

            <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
              <div class="alert bg-dark">
                <h4 class="text-white" id="requestmessage_collection"></h4>
              </div>
            </div>
              
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" onclick="saveShopLanguage();" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      @else
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <p class="alert alert-danger w-100">Display banners based on customer language. This feature is available only on the Pro Plan.
                  <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
              </p>
                
          </div>
          </div>
        </div>

      @endif

    </div>
  </div>
</div>


<div class="modal fade " id="Collection-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light" id="thewatchlystModalHeader">
        <h5 class="modal-title">Add Collections</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="container-fluid">
          <!-- <div class="row" >

            <div class="col-12" id="selectviasearchdiv">
              <div class="input-group">
                <div class="input-group-prepend" >
                  <button id="collection_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Collections</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style=" transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item collect_type" onclick="collection_select_option(1);" value="1" href="#">All Collections</a>
                    <a class="dropdown-item collect_type" onclick="collection_select_option(2);" value="2" href="#">Selected Collections</a>
                  </div>
                </div>
              </div>
            </div>

            <div>
                </br>
                </br>
            </div>
          </div> -->

          <div class="row">
            <div class="col-12" id="selectviasearchdiv">
              <div class="">
                <div class="input-group-prepend" style="position: absolute;">
                  <button id="collection_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Collections</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style=" transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item collect_type" onclick="collection_select_option(1);" value="1" href="#">All Collections</a>
                    <a class="dropdown-item collect_type" onclick="collection_select_option(2);" value="2" href="#">Selected Collections</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-12" id="Collection-tableDataModel">
            </div>
          </div>

          <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
            <div class="alert bg-dark">
              <h4 class="text-white" id="requestmessage_collection"></h4>
            </div>
          </div>
            
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade " id="ExProduct-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light" id="">
        <h5 class="modal-title">Exclude products
            <span class="small text-secondary ml-2">(Banner will not be displayed on these products)</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 ">
              <div class="input-group">
                <div class="input-group-prepend">
                  <button id="Exidtype_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Products</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item exidtype" value="1" href="#">All Products </a>
                    <a class="dropdown-item exidtype" value="2" href="#">Excluded Products</a>
                  </div>
                </div>
                <input id="Exsearchbox" type="search" class="form-control" value="" placeholder="Search product title with minimum 3 characters">
                <div class="input-group-append">
                  <button id="exsearchItem" class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>
            <div>
              </br>
              </br>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="ExtableDataModel">
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade " id="Linktab-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="">
        <h5 class="modal-title">Modal Content</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 ">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="">Modal Size</label>
              </div>
              <select id="modalsize" class="form-control" name="modalsize">
                <option {{ (isset( $banner['modalsize'] ) && $banner['modalsize'] == 0) ? 'selected':'' }} value="0">Small</option>
                <option {{ (isset( $banner['modalsize'] ) && $banner['modalsize'] == 1) ? 'selected':'' }} value="1">Medium</option>
                <option {{ (isset( $banner['modalsize'] ) && $banner['modalsize'] == 2) ? 'selected':'' }} value="2">large</option>
              </select>
            </div>

              <textarea name="bannerdescription" id="mytextarea" value="{{ isset( $banner['bannerdescription'] ) ? $banner['bannerdescription'] :'' }}" >{{ isset( $banner['bannerdescription'] ) ? $banner['bannerdescription'] :'' }}</textarea>
            </div>
            <div>
              </br>
              </br>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="saveModalContent();" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

 @include('dashboard.iconmodal')



<div class="modal fade " id="blacklistProduct-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light" id="thewatchlystModalHeader">
        <h5 class="modal-title">Add products</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>



      <div class="modal-body">
        <div class="container-fluid">
           
            
          <div class="row">

            <div class="col-12" id="selectviasearchdiv">
              <div class="input-group">
                <div class="input-group-prepend">
                  <button id="idtype_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Products</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item idtype" value="1" href="#">All Products </a>
                    <a class="dropdown-item idtype" value="2" href="#">Selected Products</a>
                    <!-- <a class="dropdown-item idtype" value="3" href="#">Bulk Select Products</a> -->
                  </div>
                </div>
                <input id="searchbox" type="search" class="form-control" value="" placeholder="Search product title with minimum 3 characters">
                <div class="input-group-append">
                  <button id="searchItem" class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>

            <!-- <div class="col-12 col-md-8 offset-md-2 mt-4 card card-block pt-2 mb-4" id="selectviafilediv">
            
              <form id="upload_csv" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                @if( !empty( $banner ) )
                  <input type="hidden" name="bannerid" value="{{$banner['productbannerid']}}">
                @endif
                <div class="form-group row ">
                    <div class="col-12">
                        <p>Upload a .txt file containing a list of ProductIDs to bulk select products. More info <a href="{{ asset('help') }}" target="_blank">here.</a></p>
                    </div>
                  <div class="col-12"> 
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="csv_file" name="file"  accept=".txt" aria-describedby="Upload">
                        <label class="custom-file-label custom-file-csv" for="file">Choose file</label>
                      </div>
                    </div>
                    <div class="small text-muted">(Max size: 200KB | Type: .txt)</div>
                  </div>

                  <div class="col-12 text-right mt-2"> 
                    <input type='submit' class="btn btn-primary" name='submit'  >
                  </div>

                </div>
              </form>
            </div> -->

            <div>
              </br>
              </br>
            </div>

          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="tableDataModel">
            </div>
          </div>

          <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_text" style="bottom:20px;" >
            <div class="alert bg-dark">
              <h5 class="text-white" id="requestmessage_text"></h5>
            </div>
          </div>

        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="metafieldModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header bg-light" id="">
        <h5 class="modal-title">
          Product Metafield Name
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              The app accepts the metafield name in the following format:<br>
              <b>Namespace</b>.<b>Key</b><br><br>
              Example Metafield Names:<br>
              1. global.wash<br>
              2. sizing.large
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>

<div class="modal fade " id="openDynamicVariableModel-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">
          Dynamic Data Fields
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="">
            Display product data as part of the banner text.
            <!-- <div class="mt-1 mb-3">
              <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000430925-dynamic-banner-text">
                More info <span class="fa fa-caret-right"></span>
              </a>
              </span>
            </div>-->
            <div class="row mt-3">
              <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="tableDataModel">
                <table id="productlist" class="table table-bordered table-striped text-left table-sm" cellspacing="0" width="100%">
                  <thead>
                    <tr class="text-center">
                      <!--<th scope="col"></th>-->
                      <th class="text-left" scope="col ">Dynamic Data Fields</th>
                      <th scope="col ">Action</th>
                    </tr>
                  </thead>
                  <colgroup>
                    <col width="75%">
                    <col width="25%">
                  </colgroup>
                  <tbody id="">
                    @foreach( $dynamicVariables as $index => $variable )
                      <tr class="text-center">
                        <td class="text-left">
                          <div class="">
                            {{$variable["var"]}}
                          </div>
                          <div class="small text-muted">
                            {{$variable['des']}}
                          </div>
                        </td>
                        <td>
                          <div class="btn-group productall">
                            <!-- <button value="{{$variable["exm"]}}" onclick="addVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="Edit product parameters"> -->
                            <button value="{{$variable["exm"]}}" onclick="addVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="Edit product parameters">
                              Select
                            </button>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ primebanm";$dynamicVariableTitle2 = "}}";$dynamicVariableExm = "{{ primebanm.namespace.metafield }} "; ?>
                          {!! $dynamicVariableTitle !!}.<span style="font-style: italic;">namespace.metafield </span>{!! $dynamicVariableTitle2 !!}
                          <span class="banner banner-info ml-2">Pro</span>
                        </div>
                    
                        <div class="small text-muted">
                          Product metafield. <a target="_blank" href="https://primeappassist.freshdesk.com/support/solutions/articles/81000385684-dynamic-metafields"> More info<span class="fa fa-caret-right ml-1"></span> </a>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ OrderCount }}";$dynamicVariableDesc = "Total order count";$dynamicVariableExm = "{{ OrderCount }} sold "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="banner banner-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-order-count')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          @if(isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '')
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                          @else
                            <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                              Disable
                            </button>
                          @endif
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ RecentSoldCount }}";$dynamicVariableDesc = "Recent sold count";$dynamicVariableExm = "{{ RecentSoldCount }} Sold "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="banner banner-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-order-count')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          @if(isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '')
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                          @else
                            <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                              Disable
                            </button>
                          @endif
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>
                    

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ CountdownTimer }}";$dynamicVariableDesc = "Countdown timer";$dynamicVariableExm = "{{ CountdownTimer }}"; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="banner banner-info ml-2">Pro</span>
                        </div>
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!} <a target="_blank" href="https://primeappassist.freshdesk.com/support/solutions/articles/81000386551-countdown-timer"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                        <div class="flatpickr input-group input-group-sm mt-1" id="datetimepicker3"> 
                          <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-sm">{{ $arrNameById[$mytimezone] }}</span>
                            <!-- <span class="input-group-text" id="inputGroup-sizing-sm">New Delhi Time</span> -->
                          </div>
                          <input type="text" class="form-control flatpickr-input" id="input_datetimepicker3" name="dynamicendtime" value="" placeholder="Select End Date" data-input="" readonly="readonly"><div class="input-group-append">
                          <!-- <span class="input-group-text" id="inputGroup-sizing-sm">IST</span> -->
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariableDate(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ ExpectedDeliveryDate }}";$dynamicVariableDesc = "Expected Delivery Date";$dynamicVariableExm = "{{ ExpectedDeliveryDate }} "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-delivery-date')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                        <div class="small text-muted ">
                          {!! $dynamicVariableDesc !!} <a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000392649-estimated-delivery-dates"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>

                        <div class="small text-muted mt2">
                          From Current date + 
                          <select class="form-control form-control-sm w-auto d-inline" name="firstdate" id="firstdate" >
                            <option value="0" >0</option>
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                            <option value="7" >7</option>
                            <option value="8" >8</option>
                            <option value="9" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                            <option value="13" >13</option>
                            <option value="14" >14</option>
                            <option value="15" >15</option>
                            <option value="16" >16</option>
                            <option value="17" >17</option>
                            <option value="18" >18</option>
                            <option value="19" >19</option>
                            <option value="20" >20</option>
                            <option value="21" >21</option>
                            <option value="22" >22</option>
                            <option value="23" >23</option>
                            <option value="24" >24</option>
                            <option value="25" >25</option>
                            <option value="26" >26</option>
                            <option value="27" >27</option>
                            <option value="28" >28</option>
                            <option value="29" >29</option>
                            <option value="30" >30</option>
                          </select>
                          to + 
                          <select class="form-control form-control-sm w-auto d-inline" name="seconddate" id="seconddate" >
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                            <option value="7" >7</option>
                            <option value="8" >8</option>
                            <option value="9" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                            <option value="13" >13</option>
                            <option value="14" >14</option>
                            <option value="15" >15</option>
                            <option value="16" >16</option>
                            <option value="17" >17</option>
                            <option value="18" >18</option>
                            <option value="19" >19</option>
                            <option value="20" >20</option>
                            <option value="21" >21</option>
                            <option value="22" >22</option>
                            <option value="23" >23</option>
                            <option value="24" >24</option>
                            <option value="25" >25</option>
                            <option value="26" >26</option>
                            <option value="27" >27</option>
                            <option value="28" >28</option>
                            <option value="29" >29</option>
                            <option value="30" >30</option>
                          </select> days.
                        </div>

                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariableExpectedDate(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ VisitorCounter }}";$dynamicVariableDesc = "Visitor counter";$dynamicVariableExm = "{{ VisitorCounter }} recent visitors "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-dark ml-2">Gold</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-livevisitcount')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}<a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000390548-live-visitor-counter"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 3)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addGoldVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>  



<div class="modal fade " id="openSubDynamicVariableModel-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">
          Dynamic Data Fields
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="">
            Display product data as part of the banner sub text.
            <!-- <div class="mt-1 mb-3">
              <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000430925-dynamic-banner-text">
                More info <span class="fa fa-caret-right"></span>
              </a>
              </span>
            </div>-->
            <div class="row mt-3">
              <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="tableDataModel">
                <table id="productlist" class="table table-bordered table-striped text-left table-sm" cellspacing="0" width="100%">
                  <thead>
                    <tr class="text-center">
                      <!--<th scope="col"></th>-->
                      <th class="text-left" scope="col ">Dynamic Data Fields</th>
                      <th scope="col ">Action</th>
                    </tr>
                  </thead>
                  <colgroup>
                    <col width="75%">
                    <col width="25%">
                  </colgroup>
                  <tbody id="">
                    @foreach( $dynamicVariables as $index => $variable )
                      <tr class="text-center">
                        <td class="text-left">
                          <div class="">
                            {{$variable["var"]}}
                          </div>
                          <div class="small text-muted">
                            {{$variable['des']}}
                          </div>
                        </td>
                        <td>
                          <div class="btn-group productall">
                            <!-- <button value="{{$variable["exm"]}}" onclick="addVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="Edit product parameters"> -->
                            <button value="{{$variable["exm"]}}" onclick="addsubVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="Edit product parameters">
                              Select
                            </button>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ primebanm";$dynamicVariableTitle2 = "}}";$dynamicVariableExm = "{{ primebanm.namespace.metafield }} "; ?>
                          {!! $dynamicVariableTitle !!}.<span style="font-style: italic;">namespace.metafield </span>{!! $dynamicVariableTitle2 !!}
                          <span class="banner banner-info ml-2">Pro</span>
                        </div>
                    
                        <div class="small text-muted">
                          Product metafield. <a target="_blank" href="https://primeappassist.freshdesk.com/support/solutions/articles/81000385684-dynamic-metafields"> More info<span class="fa fa-caret-right ml-1"></span> </a>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addsubProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ OrderCount }}";$dynamicVariableDesc = "Total order count";$dynamicVariableExm = "{{ OrderCount }} sold "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="banner banner-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-order-count')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          @if(isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '')
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addsubProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                          @else
                            <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                              Disable
                            </button>
                          @endif
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ RecentSoldCount }}";$dynamicVariableDesc = "Recent sold count";$dynamicVariableExm = "{{ RecentSoldCount }} Sold "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="banner banner-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-order-count')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          @if(isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '')
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addsubProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                          @else
                            <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                              Disable
                            </button>
                          @endif
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>
                    

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ CountdownTimer }}";$dynamicVariableDesc = "Countdown timer";$dynamicVariableExm = "{{ CountdownTimer }}"; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="banner banner-info ml-2">Pro</span>
                        </div>
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!} <a target="_blank" href="https://primeappassist.freshdesk.com/support/solutions/articles/81000386551-countdown-timer"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                        <div class="flatpickr input-group input-group-sm mt-1" id="datetimepicker4"> 
                          <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-sm">{{ $arrNameById[$mytimezone] }}</span>
                            <!-- <span class="input-group-text" id="inputGroup-sizing-sm">New Delhi Time</span> -->
                          </div>
                          <input type="text" class="form-control flatpickr-input" id="input_datetimepicker4" name="dynamicendtime" value="" placeholder="Select End Date" data-input="" readonly="readonly"><div class="input-group-append">
                          <!-- <span class="input-group-text" id="inputGroup-sizing-sm">IST</span> -->
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addsubProVariableDate(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ ExpectedDeliveryDate }}";$dynamicVariableDesc = "Expected Delivery Date";$dynamicVariableExm = "{{ ExpectedDeliveryDate }} "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-delivery-date')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                        <div class="small text-muted ">
                          {!! $dynamicVariableDesc !!} <a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000392649-estimated-delivery-dates"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>

                        <div class="small text-muted mt2">
                          From Current date + 
                          <select class="form-control form-control-sm w-auto d-inline" name="firstdatesub" id="firstdatesub" >
                            <option value="0" >0</option>
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                            <option value="7" >7</option>
                            <option value="8" >8</option>
                            <option value="9" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                            <option value="13" >13</option>
                            <option value="14" >14</option>
                            <option value="15" >15</option>
                            <option value="16" >16</option>
                            <option value="17" >17</option>
                            <option value="18" >18</option>
                            <option value="19" >19</option>
                            <option value="20" >20</option>
                            <option value="21" >21</option>
                            <option value="22" >22</option>
                            <option value="23" >23</option>
                            <option value="24" >24</option>
                            <option value="25" >25</option>
                            <option value="26" >26</option>
                            <option value="27" >27</option>
                            <option value="28" >28</option>
                            <option value="29" >29</option>
                            <option value="30" >30</option>
                          </select>
                          to + 
                          <select class="form-control form-control-sm w-auto d-inline" name="seconddatesub" id="seconddatesub" >
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                            <option value="7" >7</option>
                            <option value="8" >8</option>
                            <option value="9" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                            <option value="13" >13</option>
                            <option value="14" >14</option>
                            <option value="15" >15</option>
                            <option value="16" >16</option>
                            <option value="17" >17</option>
                            <option value="18" >18</option>
                            <option value="19" >19</option>
                            <option value="20" >20</option>
                            <option value="21" >21</option>
                            <option value="22" >22</option>
                            <option value="23" >23</option>
                            <option value="24" >24</option>
                            <option value="25" >25</option>
                            <option value="26" >26</option>
                            <option value="27" >27</option>
                            <option value="28" >28</option>
                            <option value="29" >29</option>
                            <option value="30" >30</option>
                          </select> days.
                        </div>

                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addsubProVariableExpectedDate(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ VisitorCounter }}";$dynamicVariableDesc = "Visitor counter";$dynamicVariableExm = "{{ VisitorCounter }} recent visitors "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-dark ml-2">Gold</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-livevisitcount')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}<a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000390548-live-visitor-counter"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 3)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addsubGoldVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div> 


<div class="modal fade " id="openemojis-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light" id="thewatchlystModalHeader">
        <h5 class="modal-title">
          Add Emojis
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid mb-3 h4">
          @foreach( $emojis as $emojis_values )
            <a href="#" onclick="enableEmoji(this, event)" class="emojis ">&#{{$emojis_values}};</a>
          @endforeach  
        </div>
        <div class="modal-footer">
            <!--should be dismissed in 1 second--> 
           <div id="emojiadded" class="banner banner-success p-2">Added</div>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div> 
</div>
@include('dashboard.bannergradientshadow')

<script>
  tinymce.init({
    selector: '#mytextarea',
    menubar: false,
    plugins: ['advlist lists code image table autoresize','fullscreen','textcolor','emoticons'],
    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent bullist numlist |' + ' code image table | alignleft aligncenter ' + 'alignright |  ' + 'forecolor backcolor casechange permanentpen formatpainter removeformat | emoticons',
    a11y_advanced_options: true,
    height: 600,
    toolbar_sticky: true,
    toolbar_mode: 'sliding',
    image_title: true,
    automatic_uploads: true,
    file_picker_types: 'image',
    file_picker_callback: function (cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function () {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = function () {
          var id = 'blobid' + (new Date()).getTime();
          var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      };

      input.click();
    },
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js" ></script> 

<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
<script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>

<script>

    

    $("#tooltip").change(function(){
      var content = $('#tooltip').val();
      if(content.length > 100){
        $('#requestalert').show();
        $('#requestmessage').text("100 char max limit on tooltip");
        $("#requestalert").delay(2500).hide(0);
        $('#tooltip').val('');
      }else{
        $('.tooltips').attr('data-tippy-content', content);
        const instances = tippy('[data-tippy-content]');
      }
      
    });

    var content_val = $('#tooltip').val();
    if(content_val.length > 100){
      $('#requestalert').show();
      $('#requestmessage').text("100 char max limit on tooltip");
      $("#requestalert").delay(2500).hide(0);
      $('#tooltip').val('');
    }else{
      $('.tooltips').attr('data-tippy-content', content_val);
      const instances = tippy('[data-tippy-content]');
    }


  var input = document.querySelector('#bannertitle');
  var subinput = document.querySelector('#bannersubtitle');
  var emojibutton = document.querySelector('#emoji');
  var subemojibutton = document.querySelector('#subemoji');
  var textStyle = document.getElementById('bannertitleStyle');
  var subtextStyle = document.getElementById('bannersubtitleStyle');


  var picker = new EmojiButton({
    position:'bottom',
  });

  picker.on('emoji', function(emoji){
    input.value += emoji;
    textStyle.innerHTML += emoji;
  });

  emojibutton.addEventListener('click', function(){
    picker.pickerVisible ? picker.hidePicker() : picker.showPicker(emojibutton);
    picker.pickerVisible ? $('.wrapper').css('z-index',2) : '';
  });

// Sub Title Emoji ....................
  var subpicker = new EmojiButton({
    position:'bottom',
  });

  subpicker.on('emoji', function(emoji){
    subinput.value += emoji;
    subtextStyle.innerHTML += emoji;
  });

  subemojibutton.addEventListener('click', function(){
    subpicker.pickerVisible ? subpicker.hidePicker() : subpicker.showPicker(subemojibutton);
    subpicker.pickerVisible ? $('.wrapper').css('z-index',2) : '';
  });

</script>

@if( empty( $banner ) )
  <script type="text/javascript">
    $(document).ready(function(){ 
      applyBannerfontStyle( 2 , 'Font');
    });
  </script>
@endif
<script type="text/javascript">

  $(document).ready(function(){

    var selectedinventory = $('.selectedinventory').val();
    if (selectedinventory === undefined || selectedinventory < 1 ) {
      
    }else{
      if(selectedinventory>1){
        // alert()
        $(".inventoryhide").css({"display": "none"});
        
      }
    }

    // var table =   $('#collectionlist').DataTable({

    // dom: 'Bfrtip',
    // responsive: true,
    // "paging": true,
    // "pagingType": "simple_numbers",
    // "searching" : false,
    // "ordering": false,
    // "bServerSide":false,
    // });  

    // $(".selectproduct").on('click',function(){
    //   filterType = $('.idtype').attr('value');
    //   if((filterType) == 3 ){
    //     $("#selectviafilediv").removeClass('d-none');
    //     $("#tableDataModel").addClass('d-none');
    //   }else{
    //     $("#selectviafilediv").addClass('d-none');
    //     $("#tableDataModel").removeClass('d-none');
    //     $(".custom-file-csv").text('Choose file');
        
    //   }
    //   getProducts('',"", productidsValue );
    // });

    // $(".selectedtext").on('click',function(){
    //   filterType = $('.idtype').attr('value');
    //   if((filterType) == 3 ){
    //     $("#selectviafilediv").removeClass('d-none');
    //     $("#tableDataModel").addClass('d-none');
    //   }else{
    //     $("#selectviafilediv").addClass('d-none');
    //     $("#tableDataModel").removeClass('d-none');
    //     $(".custom-file-csv").text('Choose file');
        
    //   }
    // });

    setTimeout( function(){

      //console.log('loadeddd');
      $('#animationdiv').css('display','none');
    },500);

    var bannershape = '';
    var itemType = $('#bannershape').find('.active').data('value');
    if(itemType === undefined || itemType === null){
      $("#bannershape .dropdown-item:first").addClass( "active" );
    }
    bannershape = $('#bannershape').find('.active');
    applyBannerStyle(bannershape,'Shape');

  });

  // function enableEmoji(elem) {
  //   document.querySelector('#bannertitle').value += elem.textContent;
  //   var div = document.getElementById('textStyle');
  //   div.innerHTML += elem.textContent;
  // }

  $('#bannershape').on('click', function (e) {
    var previous = $('#bannershape').find(".active");
    // console.log(previous);
    previous.removeClass('active');
    $(e.target).addClass('active');
    applyBannerStyle(previous,'Shape');
  });

  $('#image').on('change',function(){
    var fileName = $(this).val();
    $(this).next('.custom-file-label').html(fileName);
  });

  $('#csv_file').on('change',function(){
    var fileName = $(this).val();
    $(this).next('.custom-file-csv').html(fileName);
  });

  $(function () {
    $("#datetimepicker1").flatpickr({enableTime: true,
      dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,});
    $("#datetimepicker2").flatpickr({enableTime: true,
      dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,});
      $("#datetimepicker3").flatpickr({enableTime: true,
      dateFormat: "d/m/Y h:i K", wrap: true,defaultDate: new Date().fp_incr(6),minDate: "today"});
      $("#datetimepicker4").flatpickr({enableTime: true,
      dateFormat: "d/m/Y h:i K", wrap: true,defaultDate: new Date().fp_incr(6),minDate: "today"});

  });

  $(document).ready(function (){
    font_style();
    check_enable_date();
    check_banner_type();
    applyBannerSpaceClasses();
    linktab();
    
    var nlangcount = $("#addShopLanguage_counter").val();
    if( nlangcount != '' ){
      var nows = nlangcount;
      var narr =  nows.split(',');
      nlangcount = narr;
      $('#language_count').text(nlangcount.length);
    }

    var ncountrycount = $("#addCountry_counter").val();
    if( ncountrycount != '' ){
      var nows = ncountrycount;
      var narr2 =  nows.split(',');
      ncountrycount = narr2;
      $('#country_count').text(ncountrycount.length);
    }
  
    $('.metafieldcond').each(function(i, obj) {

       var conditionvalue = $(this).val();
      //console.log(conditionvalue);
      if( (conditionvalue== 2) || (conditionvalue == 3) ){

        var metakeydiv = $(this).parent().next();
       // console.log($(this).parent().next());
        $(metakeydiv).find('input').val('');
        $(metakeydiv).hide();

      } else {

        var metakeydiv = $(this).parent().next();
        $(metakeydiv).show();
      }
      

    });

    // var visibility2 = $('#input_datetimepicker2').val();
    // var visibility1 = $('#input_datetimepicker1').val();
    // if(visibility2 == '01-02-2035 12:00 PM' && visibility1 == '01-02-1990 12:00 PM' ){
    //   $("#startingnow").prop("checked", true);
    //   $("#enabledate").prop("checked", false);
    //   $('#input_datetimepicker1').val('');
    //   $('#input_datetimepicker2').val('');
    //   $('#input_datetimepicker1').prop("disabled", true);
    //   $('#input_datetimepicker2').prop("disabled", true);
    //   $('.date').hide();
    // }

    //$('#metafieldcond').on('change',function(){
    $(document).on("change", ".metafieldcond", function() {
      
      var conditionvalue = $(this).val();
      //console.log(conditionvalue);
      if( (conditionvalue== 2) || (conditionvalue == 3) ){

        var metakeydiv = $(this).parent().next();
       // console.log($(this).parent().next());
        $(metakeydiv).find('input').val('');
        $(metakeydiv).hide();

      } else {

        var metakeydiv = $(this).parent().next();
        $(metakeydiv).show();
      }
      
    });
      
  });

  $("#banneropacity").change( function(){
    var banneropacity = $("#banneropacity").val();
    $("#bannerStyle").css('opacity',banneropacity);
  });

  function alert_error_message(text){
    $('#requestalert').show();
    $('#requestmessage').text(text);
    $("#requestalert").delay(2500).hide(0);
  }

  $("#linktab").change( function(){
    linktab();
  });

  function linktab(){
    var linktab = $("#linktab").val();
    if(linktab == 2){
      $( "#bannerlink" ).prop( "disabled", true );
      $( "#bannerlink" ).val('');
      $("#linktabmodal").show();
    }else{
      $( "#bannerlink" ).prop( "disabled", false  );
      $("#linktabmodal").hide();
    }
  }

  function errorimgmsg(error){
    console.log('inerror');
    // error.onerror=null;
    var strerror = error.src;
    var free = strerror.replace("data:image/svg+xml", "data:image/png");
    error.src = free;
  }

  function font_style(){
    var checkvalue = $('#fontstyle_banner').val();
    var real_values = checkvalue.split(",");
    for(var i = 0; i < real_values.length; i++){
      applyBannerStyle(this,'Font',real_values[i]);
    }

    var checkvalue2 = $('#subfontstyle_banner').val();
    var real_values2 = checkvalue2.split(",");
    for(var i = 0; i < real_values2.length; i++){
      applyBannerStyle(this,'SubFont',real_values2[i]);
    }
  } 

  function check_enable_date(){
    var checkvalue = $('#enabledate').prop('checked');
    if(checkvalue == true){
      $('.date').show();
      $('#input_datetimepicker1').prop("disabled", false);
      $('#input_datetimepicker2').prop("disabled", false);
      var visibility2 = $('#input_datetimepicker2').val();
      var visibility1 = $('#input_datetimepicker1').val();
      if(visibility2 == '01-02-2035 12:00 PM' && visibility1 == '01-02-1990 12:00 PM' ){
        var d = new Date();
        var strDate =addZero(d.getDate()) +  "-" + addZero(d.getMonth()+1) + "-"+ d.getFullYear()  +  " 1:00 AM" ;
        var strDate2 =addZero(d.getDate()+1) +  "-" + addZero(d.getMonth()+1) + "-"+ d.getFullYear()  +  " 1:00 AM" ;
        $("#datetimepicker1").flatpickr({enableTime: true,
        dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,defaultDate: strDate,});
        $('#input_datetimepicker1').val(strDate);
        $("#datetimepicker2").flatpickr({enableTime: true,
        dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,defaultDate: strDate2,});
        $('#input_datetimepicker2').val(strDate2);
      }

    }else{
      $('#input_datetimepicker1').prop("disabled", true);
      $('#input_datetimepicker2').prop("disabled", true);
      // $('#input_datetimepicker1').val('');
      // $('#input_datetimepicker2').val('');
      $('.date').hide();
    }
  }

  function check_banner_type(){
    var banner_text = $('#banner_text').prop('checked');
    var banner_image = $('#banner_image').prop('checked');
    if(banner_text == true){
      $('.banner_image_show').hide();
      $('.banner_text_show').show();
      $("#banner_image").prop("checked", false);
      $("#bannerStyle").css({"padding": "0px 12px 0px 12px"});
      // $("#height").val('');
      $("#bannerleftStyleMargintop").val('0');
      $("#bannerleftStyleMarginbottom").val('0');
      $("#bannerleftStyleMarginleft").val('0');
      $("#bannerleftStyleMarginright").val('0');

      // FOR PREVIEW
      $("#bannerStyle").removeClass("intro");
      $("#bannerStyle_image").addClass("intro");
      $('#bannerStyle_image').hide();
      $("#bannerStyle").show();
      updateBannerstyle();
    }else if(banner_image == true){
      $('.banner_image_show').show();
      $('.banner_text_show').hide();
      $("#banner_text").prop("checked", false);
      // $( "#buttonsettingBgcol" ).val('none');
      // FOR PREVIEW
      $("#bannerStyle").addClass("intro");
      $("#bannerStyle_image").removeClass("intro");
      $('#bannerStyle_image').show();
      $("#bannerStyle").hide();
      updateBannerstyle();
    }
  }

  function check_banner_height(){
    var height = $('#height').val();
    if(height > 300 ){
      var text = "Height cannot be more than 300px";
      alert_error_message(text);
      $('#height').val('300');
      $('.banner_image_preview').css('height','300px');
      return false;
    }else if(height < 1){
      var text = "Height cannot be less than 1px";
      alert_error_message(text);
      $('#height').val('50');
      $('.banner_image_preview').css('height','50px');
      return false;
    } else if(height == ''){
      var text = "Height should not be empty";
      alert_error_message(text);
      $('#height').val('50');
      $('.banner_image_preview').css('height','50px');
      return false;
    } 
    // else if(height < 1){
    //   var text = "Height should not be empty";
    //   alert_error_message(text);
    //   $('#height').val('22');
    //   return false;
    // }
    return true;
  }

  function check_moblieheight(){
    var height = $('#moblieheight').val();
    if(height > 300){
      var text = "Mobile Height cannot be more than 300px";
      alert_error_message(text);
      $('#moblieheight').val('300');
      return false;
    }else if(height == ''){
      // var text = "Mobile Height blank";
      // alert_error_message(text);
      // $('#moblieheight').val('22');
      return true;
    }
    else if(height < 1){
      var text = "Mobile Height be less than 1px";
      alert_error_message(text);
      $('#moblieheight').val('80');
      return false;
    }
    // 
    return true;
  }

  function check_tabletheight(){
    var height = $('#tabletheight').val();
    if(height > 300){
      var text = "Tablet Height cannot be more than 300px";
      alert_error_message(text);
      $('#tabletheight').val('300');
      return false;
    } else if(height == ''){
      // var text = "Mobile Height blank";
      // alert_error_message(text);
      // $('#moblieheight').val('22');
      return true;
    }
    else if(height < 1){
      var text = "Tablet Height be less than 1px";
      alert_error_message(text);
      $('#tabletheight').val('80');
      return false;
    }
    // else if(height == ''){
    //   var text = "Tablet Height should not be empty";
    //   alert_error_message(text);
    //   $('#tabletheight').val('22');
    //   return false;
    // }
    return true;
  }

  $("#setcurrentdate").click( function(){
    var d = new Date();
    var strDate =addZero(d.getDate()) +  "-" + addZero(d.getMonth()+1) + "-"+ d.getFullYear()  +  " 1:00 AM" ;
    console.log(strDate);
    $("#datetimepicker1").flatpickr({enableTime: true,
      dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,defaultDate: strDate,});

    $('#input_datetimepicker1').val(strDate);
  });

  function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
  }

  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          var file = input.files[0];
          var fileType = file["type"];
          var validImageTypes = ["image/gif", "image/png", "image/jpeg", "image/svg+xml"];
          if ($.inArray(fileType, validImageTypes) < 0) {
            $('#image').val('');
            var text = "Only png, jpeg, gif, svg is allowed.";
            alert_error_message(text);
            return false;
          }else{
            if(input.files[0].size > 500000) {
              var text = "File size is more than 500 KB";
              alert_error_message(text);
              $('#image').val('');
              return false;
            };
          }
          // $('#bannerStyle').hide();
          // $('#bannerStyle_image').show();
          $('.banner_image_preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }

  $("#image").change(function(){
    readURL(this);
  });

  $("#height").change(function(){
    var height = $('#height').val();
    console.log(height);
    if(height){
      // $('.banner_image_preview').attr('style','max-height:'+ height +'px');
      $('.banner_image_preview').css('height',height +'px');
    }else{
      // $('.banner_image_preview').attr('style','max-height:80px');
      $('.banner_image_preview').css('height','80px');
    }
    
  });

  function openOrderCountModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
      backdrop: 'static',
      keyboard: false
    }); 
  }

  function saveOrderCount(){
    var orderrulevalue = $('#saveordercount').val();
    $('input[name="prorule[0][conditiontype][order_count][value]"]').val(orderrulevalue);
    var recentorderrulevalue = $('#saverecentordercount').val();
    $('input[name="prorule[1][conditiontype][recentorder_count][value]"]').val(recentorderrulevalue);
    $("#OrderCount-model").modal('hide');
    
  };

  var Countrylisttable = $('#Countrylist').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );

  var Countrylistaddedtable = $('#Countrylistadded').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );
  
  var CountryValue = '';
  var countervalue   = 0;

  function openCountryModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
      backdrop: 'static',
      keyboard: false
    }); 

    filterType = 1;
    CountryValue = $("#addCountry_counter").val();
    countervalue = counter;
    if( CountryValue != '' ){
      var nows = CountryValue;
      arr =  nows.split(',');
      read_data = unique4(arr);
      pro_uni_data = read_data.join(",");
      CountryValue = pro_uni_data;
    }
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());
  }

  function addCountryAll(){
    Countrylisttable.search('').draw();
    $("#Countrylist > tbody > tr").each(function () {
      CountryName = $.trim($(this).find('td').eq(0).text());
      CountryButton = $(this).find('td').eq(1);
      Countryid = CountryButton.find('button').attr('countryid');
      Countrylisttable.row( $(CountryButton).parents('tr') ).remove().draw();

      Countrylistaddedtable.row.add( [
        ''+CountryName+'',
        '<button  countryid="'+Countryid+'"  onclick="RemoveCountry(this,\''+Countryid+'\',\''+CountryName+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
        
    });
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());

  }

  function removeCountryAll(){
    Countrylistaddedtable.search('').draw();
    $("#Countrylistadded > tbody > tr").each(function () {
      CountryName = $.trim($(this).find('td').eq(0).text());
      CountryButton = $(this).find('td').eq(1);
      Countryid = CountryButton.find('button').attr('countryid');
      Countrylistaddedtable.row( $(CountryButton).parents('tr') ).remove().draw();
      
      Countrylisttable.row.add( [
        ''+CountryName+'',
        '<button  countryid="'+Countryid+'"  onclick="addCountry(this,\''+Countryid+'\',\''+CountryName+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');


    });
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());
    

  }

  function addCountry( instance , countryid, countryname ){
    let Countrynew = $(instance).attr('countryid');
    Countrylisttable.row( $(instance).parents('tr') ).remove().draw();
    Countrylistaddedtable.row.add( [
      ''+countryname+'',
      '<button  countryid="'+countryid+'"  onclick="RemoveCountry(this,\''+countryid+'\',\''+countryname+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
      ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');

      Countrylisttable.search('').draw();
      $('#removecount').text(Countrylistaddedtable.rows().count());
      $('#addedcount').text(Countrylisttable.rows().count());
  }

  function RemoveCountry( instance , countryid, countryname ){
    let Countrynew = $(instance).attr('countryid');
    Countrylistaddedtable.row( $(instance).parents('tr') ).remove().draw();
    var selectedCountryName = '';
    var selectedid = '';
    var CountriesArray = <?php echo json_encode($countries); ?>;
    
    $.each( CountriesArray, function( index, value ){
      if(value['code'] == countryid){
        Countrylisttable.row.add( [
          ''+countryname+'',
          '<button  countryid="'+countryid+'"   onclick="addCountry(this,\''+countryid+'\',\''+countryname+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
          ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
      }
    });
    Countrylistaddedtable.search('').draw();
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());

  }

  
  function saveCountry(){
    var countryArray = [];
    var savecountryids = '';
    $("#Countrylistadded > tbody > tr").each(function () {
      selectedCountryName = $(this).find('td').eq(1);
      selectedid = selectedCountryName.find('button').attr('countryid');
      if(selectedid != undefined){
        countryArray.push(selectedid);
      }
      if( countryArray.length > 0 ){
        savecountryids = countryArray.join(",");
      }else{
        savecountryids = "";
      }
      $("#Country-model").modal('hide');
      $("#addCountry_counter").val(savecountryids);
      $("#customer_location_count").text(countryArray.length);
      if(countryArray.length > 0){
        $('#country_count').text(countryArray.length);
      }else{
        $('#country_count').text('');
      }

    });    

  }

  
    // Only For Customer language Rule ................

    var ShopLanguagelisttable = $('#ShopLanguagelist').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );

  var ShopLanguagelistaddedtable = $('#ShopLanguagelistadded').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );
  
  var ShopLanguageValue = '';
  var countervalue   = 0;

  function openShopLanguageModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
      backdrop: 'static',
      keyboard: false
    }); 

    filterType = 1;
    ShopLanguageValue = $("#addShopLanguage_counter").val();
    countervalue = counter;
    if( ShopLanguageValue != '' ){
      var nows = ShopLanguageValue;
      arr =  nows.split(',');
      read_data = unique4(arr);
      pro_uni_data = read_data.join(",");
      ShopLanguageValue = pro_uni_data;
    }
    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());
  }

  function addShopLanguageAll(){
    $("#ShopLanguagelist > tbody > tr").each(function () {
      ShopLanguageName = $.trim($(this).find('td').eq(0).text());
      ShopLanguageButton = $(this).find('td').eq(1);
      ShopLanguageid = ShopLanguageButton.find('button').attr('ShopLanguageid');
      ShopLanguagelisttable.row( $(ShopLanguageButton).parents('tr') ).remove().draw();

      ShopLanguagelistaddedtable.row.add( [
        ''+ShopLanguageName+'',
        '<button  ShopLanguageid="'+ShopLanguageid+'"  onclick="RemoveShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguageName+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
        
    });
    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());

  }

  function RemoveShopLanguageAll(){
    
    $("#ShopLanguagelistadded > tbody > tr").each(function () {
      ShopLanguageName = $.trim($(this).find('td').eq(0).text());
      ShopLanguageButton = $(this).find('td').eq(1);
      ShopLanguageid = ShopLanguageButton.find('button').attr('ShopLanguageid');
      ShopLanguagelistaddedtable.row( $(ShopLanguageButton).parents('tr') ).remove().draw();
      
      ShopLanguagelisttable.row.add( [
        ''+ShopLanguageName+'',
        '<button  ShopLanguageid="'+ShopLanguageid+'"  onclick="addShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguageName+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');


    });
    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());
    

  }

  function addShopLanguage( instance , ShopLanguageid, ShopLanguagename ){
    let ShopLanguagenew = $(instance).attr('ShopLanguageid');
    ShopLanguagelisttable.row( $(instance).parents('tr') ).remove().draw();
    ShopLanguagelistaddedtable.row.add( [
      ''+ShopLanguagename+'',
      '<button  ShopLanguageid="'+ShopLanguageid+'"  onclick="RemoveShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguagename+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
      ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');

      $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
      $('#langaddedcount').text(ShopLanguagelisttable.rows().count());
  }

  function RemoveShopLanguage( instance , ShopLanguageid, ShopLanguagename ){
    let ShopLanguagenew = $(instance).attr('ShopLanguageid');
    ShopLanguagelistaddedtable.row( $(instance).parents('tr') ).remove().draw();
    var selectedShopLanguageName = '';
    var selectedid = '';
    var CountriesArray = <?php echo json_encode($languages); ?>;
    
    $.each( CountriesArray, function( index, value ){
      if(value['languagecode'] == ShopLanguageid){
        ShopLanguagelisttable.row.add( [
          ''+ShopLanguagename+'',
          '<button  ShopLanguageid="'+ShopLanguageid+'"   onclick="addShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguagename+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
          ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
      }
    });

    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());

  }

  
  function saveShopLanguage(){
    var ShopLanguageArray = [];
    var saveShopLanguageids = '';
    $("#ShopLanguagelistadded > tbody > tr").each(function () {
      selectedShopLanguageName = $(this).find('td').eq(1);
      selectedid = selectedShopLanguageName.find('button').attr('ShopLanguageid');
      if(selectedid != undefined){
        ShopLanguageArray.push(selectedid);
      }
      if( ShopLanguageArray.length > 0 ){
        saveShopLanguageids = ShopLanguageArray.join(",");
      }else{
        saveShopLanguageids = "";
      }
      $("#ShopLanguage-model").modal('hide');
      $("#addShopLanguage_counter").val(saveShopLanguageids);
      $("#shop_languagecount_count").text(ShopLanguageArray.length);
      if(ShopLanguageArray.length > 0){
        $('#language_count').text(ShopLanguageArray.length);
      }else{
        $('#language_count').text('');
      }

    });    

  }

  // End of Customer Language ................

  function openLinkTabModel( modelid  ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    }); 
    return true;
  };


  var collectionidsValue = '';
  var countervalue   = 0;
  
  function openCollectionModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    }); 

    filterType = 1;
    $("#collection_selection").text( filterType == 1 ? "All Collections" :"Selected Collections" );
    collectionidsValue = $("#addCollection_"+counter).val();
    countervalue = counter;
    if( collectionidsValue != '' ){
      var nows = collectionidsValue;
      arr =  nows.split(',');
      read_data = unique4(arr);
      pro_uni_data = read_data.join(",");
      collectionidsValue = pro_uni_data;
    }
    
    getCollections('',"", collectionidsValue );
    // $('#searchItemCollection').val('');
  }

  $('#Collection-model').on('hidden.bs.modal', function (e) {
      var table = $('#collectionlist').DataTable();
      table.clear();
      // table.destroy();
  });

  function unique4(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
  }

  var searchbox = '';
  var filterType = '';
  var table = '';

  function getCollections( param ='', searchbox='' , strProduct='' ){
    $("#Collection-tableDataModel").html('');
    $('#loadingalert').show();
    var page =1;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
            method:"GET",
            url : '{{asset('shopify-collections')}}?page='+page+'&searchbox='+searchbox+'&collections='+collectionidsValue+"&filterType="+filterType,
            dataType: 'html',
            success : function( data ){
              if(data.length < 50){
                $('#loadingalert').hide();
                $('#collection_selection').prop('disabled', true);
                $('#requestalert_collection').show();
                $('#requestmessage_collection').text(data);
                $("#requestalert_collection").delay(2500).hide(0);

              }else{
                $('#Collection-tableDataModel').html( data );
                table = $('#collectionlist').DataTable({
                  // dom: "Bfrtip",
                  dom: "Bfrtip",
                  responsive: true,
                  "paging": true,
                  "pagingType": "numbers",
                  "searching" : true,
                  "ordering": false,
                  "bServerSide":false,
                  "bInfo" : false,
                  "pageLength": 10,
                });
                collection_select_option(1);
                $('#loadingalert').hide();
                // $('#requestalert_text').show();
                // $('#requestmessage_text').text('File uploaded Successfully');
                // $("#requestalert_text").delay(2500).hide(0);

              }
            }
          });
  }

    
    
    // $('.collect_type').on('click',function(){
    function collection_select_option(value){
      var selected_button = 1;
      selected_button = value;
      if((selected_button) == 2 ){
        $("#collection_selection").text( 'Selected Collections' );
          $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
              return $(table.row(dataIndex).node()).hasClass('prime_selected');
            }
          );
          table.draw();
          
      }else{
        $.fn.dataTable.ext.search.pop();
        table.draw();
        $("#collection_selection").text( 'All Collections' );
      }
    };
   
  // $.fn.dataTable.ext.search.push(
      //  function(settings, data, dataIndex) {
      //   var collectionlist = $(table.row(dataIndex).node());  
      //   console.log(collectionlist.find('.btn-outline-danger'));
      //   var newest = $(collectionlist.find('.btn-outline-danger').closest('tr'));
      //     return newest;
      //   }
      // );

  // $("#searchItemCollection").click( function(){
  //   searchbox = $("#searchboxCollection").val().trim();
  //   if (searchbox.length < 3) {
  //     return false;
  //   }
  //   getCollections( '',searchbox ,collectionidsValue );
  // });

  var arrCollectionData = [];
  var objCollectionData = {};

  function addCollections( instance , collectionid,condition='pass'){
    if(condition == 'pass'){
  
      if( objCollectionData[countervalue] ){
        arrCollectionData = objCollectionData[countervalue];
      }else{
        arrCollectionData = [];
        let collectionValue = $("#addCollection_"+countervalue).val();
        if( collectionValue != '' && typeof collectionValue != "undefined"){
          arrCollectionData = collectionValue.split(",");
        }
      }
      let collectionidnew = $(instance).attr('collectionid');
      if( $(instance).attr('added') ){
        $(instance).text('Select');
        $(instance).removeAttr('added');
        $(instance).removeClass('btn-outline-danger');
        $(instance).addClass('btn-outline-secondary');
        $(instance).closest('tr').addClass('prime_not');
        $(instance).closest('tr').removeClass('prime_selected');
        let index = arrCollectionData.indexOf(collectionidnew);
        arrCollectionData.splice(index, 1);
        objCollectionData[countervalue] = arrCollectionData;
      }else{
        $(instance).text('Remove');
        $(instance).attr('added',"1");
        $(instance).removeClass('btn-outline-secondary');
        $(instance).addClass('btn-outline-danger');
        $(instance).closest('tr').addClass('prime_selected');
        $(instance).closest('tr').removeClass('prime_not');

        arrCollectionData.push( collectionidnew );
        objCollectionData[countervalue] = arrCollectionData;
      }

      if( arrCollectionData.length > 0 ){
        var my_array = arrCollectionData;
        arrCollectionData = getUnique(my_array);
      }

    }else{
      objCollectionData = {};
      arrCollectionData = [];
    }
    
    let totalCount = arrCollectionData.length;
    $("#collectionscount_"+countervalue).text( totalCount );
    if( arrCollectionData.length > 0 ){
      collectionidsValue = arrCollectionData.join(",");
    }else{
      collectionidsValue = "";
    }
    $("#addCollection_"+countervalue).val(collectionidsValue);
  }

  function getUnique(array){
      var uniqueArray = [];
      for(i=0; i < array.length; i++){
          if(uniqueArray.indexOf(array[i]) === -1) {
              uniqueArray.push(array[i]);
          }
      }
      return uniqueArray;
  }

  $("#Collection-model").on("hidden.bs.modal", function () {
    $('.productall input:checkbox:checked').removeAttr('checked');
  });


</script>

<script>
  var objTextSizeMapper = { "12":19.5, "13":21, "14":23, "15":24, "16":27, "17":28, "18":29, "19":32, "20":33.5 };
  var objLineHeightMapper = { "12":17.5, "13":19, "14":21, "15":22, "16":25, "17":26, "18":27, "19":30, "20":31.5 };
  var bgcolor   = $( "#buttonsettingBgcol" ).val();
  var textColor = $( "#buttonsettingtextcol" ).val();
  var subtextColor = $( "#buttonsettingsubtextcol" ).val();
  var objBannerStyle     = '';
  var objBannerSpanStyle = '';
  var bannerStyleLeft    = '';
  // var objBannerTextStyle = { 'white-space': 'normal', 'overflow': 'hidden', 'text-align':'center' };
  // var objBannersubTextStyle = { 'white-space': 'normal', 'overflow': 'hidden', 'text-align':'center' };
  var objBannerTextStyle = {  };
  var objBannersubTextStyle = {  };

  // objBannerStyle.leftStyle  ='';
  // objBannerStyle.rightStyle =
  // objBannerStyle.textSize   =
  // objBannerStyle.fontStyle  =
  // var objDynamincMapper = { 'primebInventory'      :5, 'product.price_min'   :"$5.00", 'product.price_max'   :"$6.00", 'primebMinSaleAmount'  :"$6.00", 'primebMaxSaleAmount'  :"$7.00", 'primebMinSalePercent' :"10", 'primebMaxSalePercent' :"20" } ;

    var objDynamincMapper = {
                            'Inventory'      :5,
                            // 'product.price_min'   :"$5.00",
                            // 'product.price_max'   :"$6.00",
                            'SaleAmount'  :"$6.00",
                            // 'primebMaxSaleAmount'  :"$7.00",
                            'SalePercent' :"10",
                            'Vendor': 'Allen Solly',
                            'Type': 'Tshirt',
                            'variantCount':2,
                            'OrderCount':10,
                            'RecentSoldCount':15,
                            'ProductSKU':"ABCD1234",
                            'VisitorCounter':15,

                            // 'primebMaxSalePercent' :"20",
                      } ;

  $(function() {

    $(".bannertitle").text( updateTextValue( $("#bannertitle").val() ) );
    $(".bannersubtitle").text( updateTextValue( $("#bannersubtitle").val() ) );

    $("#bannertitle").keyup(function(){
      value =  updateTextValue( $(this).val() );
      setTimeout( function(){
        $(".bannertitle").text("").text( value );
      },500);
    });

    $("#bannersubtitle").keyup(function(){
      value =  updateTextValue( $(this).val() );
      setTimeout( function(){
        $(".bannersubtitle").text("").text( value );
      },500);
    });

    $("#buttonsettingBgcol").change(function(){
      bgcolor = $(this).val();
      updateBannerstyle( bgcolor , textColor ,lineHeight, height, subtextColor);
      // $("#bannerStyle").css('background-color',bgcolor );
      // $("#spanStyle").css({'border-top':'12px solid '+bgcolor,'border-bottom':'12px solid '+bgcolor } );
      // $("#mobispanStyle").css({'border-top':'12px solid '+bgcolor,'border-bottom':'12px solid '+bgcolor } );
      // $("#mobibannerStyle").css('background-color',bgcolor);
      // $(".spanStyleleft").css({'border-top':'12px solid '+bgcolor,'border-bottom':'12px solid '+bgcolor });
    });

    $(".buttonsettingtextcol").change(function(){
      textColor  = $(this).val();
      // $("#bannerStyle").css('color',textColor );
      $("#bannertitleStyle").css('color',textColor );
      // $("#mobibannerStyle").css('color',textColor);
      $("#mobibannertitleStyle").css('color',textColor);
    });

    $(".buttonsettingsubtextcol").change(function(){
      subtextColor  = $(this).val();
      // $("#bannerStyle").css('color',subtextColor );
      $("#bannersubtitleStyle").css('color',subtextColor );
      // $("#mobibannerStyle").css('color',subtextColor);
      $("#mobibannersubtitleStyle").css('color',subtextColor);
    });
    
    $("#bannertitle").keyup(function(){
      $(".bannertitle").text( $(this).val() );
    });

    $("#bannersubtitle").keyup(function(){
      $(".bannersubtitle").text( $(this).val() );
    });
    

  });

  var lineHeight = 16;
  var height=16;
  
  function applyBannerStyle( instance , type , value='' ){
    
    if( value == ''){
      value = $(instance).val();
    }
    if( type == 'Left' || type == 'Right' || type == 'Shape' || type ==  'Margin' || type ==  'Shape_padding' || type == 'borderradius' ){
      updateBannerstyle( bgcolor , textColor ,lineHeight, height, subtextColor );
    }else if( type == 'Font' ||  type == 'SubFont' ){
      applyBannerfontStyle( value , type );
    }else if( type == 'Text' ||  type == 'SubText'){
      applyBannerTextStyle( value );
    }else if( type == 'spacing'){
      applyBannerSpaceClasses();
    }
    // else if( type == 'Margin' ){
    //   applyBannerTextStyle( value );
    // }

    $("#bannertitleStyle").css( objBannerTextStyle );
    // $("#mobitextStyle").css( objBannerTextStyle );
    $("#bannersubtitleStyle").css( objBannersubTextStyle );
  }


  function applyBannerSpaceClasses(){
    var space = $("#bannerspacing").val();
    var primebanDesktopclass = '';
    var primebanImageOuterclass = '';
    $('.primebanDesktop').removeClass('prime-p-1 prime-p-2 prime-p-3 prime-p-4 prime-p-5');
    $('.primebanImageOuter').removeClass('prime-pr-1 prime-pr-2 prime-pr-3 prime-pr-4 prime-pr-5');
    if( space == 1 ){
      primebanDesktopclass = 'prime-p-1';
      primebanImageOuterclass = 'prime-pr-1';
    }else if(space == 2){
      primebanDesktopclass = 'prime-p-2';
      primebanImageOuterclass = 'prime-pr-2';
    }else if(space == 3){
      primebanDesktopclass = 'prime-p-3';
      primebanImageOuterclass = 'prime-pr-3';
    }else if(space == 4){
      primebanDesktopclass = 'prime-p-4';
      primebanImageOuterclass = 'prime-pr-4';
    }else if(space == 5){
      primebanDesktopclass = 'prime-p-5';
      primebanImageOuterclass = 'prime-pr-5';
    }
    var bannertype = $('#banner_text').prop('checked');
    if(bannertype == true){
      $('.primebanDesktop').addClass(primebanDesktopclass);
      $('.primebanImageOuter').addClass(primebanImageOuterclass);
    }
    
      
  }

  function applyBannerfontStyle( value, type ){
    if(type == 'Font'){
      if( value == 2 ){

        if($("#bold").hasClass("btn-outline-secondary")){
          $("#bold").removeClass("btn-outline-secondary");
          $("#bold").addClass("btn-secondary");
          $("#bold").addClass("fonttitleselected");
          objBannerTextStyle['font-weight'] = 'bold';
        }else{
          $("#bold").removeClass("btn-secondary");
          $("#bold").addClass("btn-outline-secondary");
          $("#bold").removeClass("fonttitleselected");
          objBannerTextStyle['font-weight'] = '';
        }
        
      }
      if( value == 3 ){
        
        if($("#italic").hasClass("btn-outline-secondary")){
          $("#italic").removeClass("btn-outline-secondary");
          $("#italic").addClass("btn-secondary");
          $("#italic").addClass("fonttitleselected");
          objBannerTextStyle['font-style']  =  'italic';
        }else{
          $("#italic").removeClass("btn-secondary");
          $("#italic").addClass("btn-outline-secondary");
          $("#italic").removeClass("fonttitleselected");
          objBannerTextStyle['font-style']  =  '';
        }

      }
      if( value == 4 ){
        
        if($("#underline").hasClass("btn-outline-secondary")){
          $("#underline").removeClass("btn-outline-secondary");
          $("#underline").addClass("btn-secondary");
          $("#underline").addClass("fonttitleselected");
          // objBannerTextStyle['border-bottom']  =  '1px solid';
          if($("#strike").hasClass("fonttitleselected")){
            objBannerTextStyle['text-decoration']  =  'underline line-through';
          }else{
            objBannerTextStyle['text-decoration']  =  'underline';
          }

        }else{
          $("#underline").removeClass("btn-secondary");
          $("#underline").addClass("btn-outline-secondary");
          $("#underline").removeClass("fonttitleselected");
          // objBannerTextStyle['border-bottom']  =  '';
          if($("#strike").hasClass("fonttitleselected")){
            objBannerTextStyle['text-decoration']  =  'line-through';
          }else{
            objBannerTextStyle['text-decoration']  =  '';
          }
        }

      }
      if( value == 5 ){
        
        if($("#strike").hasClass("btn-outline-secondary")){
          $("#strike").removeClass("btn-outline-secondary");
          $("#strike").addClass("btn-secondary");
          $("#strike").addClass("fonttitleselected");
          // objBannerTextStyle['text-decoration']  =  'line-through';
          if($("#underline").hasClass("fonttitleselected")){
            objBannerTextStyle['text-decoration']  =  'underline line-through';
          }else{
            objBannerTextStyle['text-decoration']  =  'line-through';
          }
        }else{
          $("#strike").removeClass("btn-secondary");
          $("#strike").addClass("btn-outline-secondary");
          $("#strike").removeClass("fonttitleselected");
          // objBannerTextStyle['text-decoration']  =  '';
          if($("#underline").hasClass("fonttitleselected")){
            objBannerTextStyle['text-decoration']  =  'underline';
          }else{
            objBannerTextStyle['text-decoration']  =  '';
          }

        }

      }
    }else{
      if( value == 2 ){
        if($("#subbold").hasClass("btn-outline-secondary")){
          $("#subbold").removeClass("btn-outline-secondary");
          $("#subbold").addClass("btn-secondary");
          $("#subbold").addClass("fontsubtitleselected");
          objBannersubTextStyle['font-weight'] = 'bold';
        }else{
          $("#subbold").removeClass("btn-secondary");
          $("#subbold").addClass("btn-outline-secondary");
          $("#subbold").removeClass("fontsubtitleselected");
          objBannersubTextStyle['font-weight'] = '';
        }
      }
      if( value == 3 ){

        if($("#subitalic").hasClass("btn-outline-secondary")){
          $("#subitalic").removeClass("btn-outline-secondary");
          $("#subitalic").addClass("btn-secondary");
          $("#subitalic").addClass("fontsubtitleselected");
          objBannersubTextStyle['font-style']  =  'italic';
        }else{
          $("#subitalic").removeClass("btn-secondary");
          $("#subitalic").addClass("btn-outline-secondary");
          $("#subitalic").removeClass("fontsubtitleselected");
          objBannersubTextStyle['font-style']  =  '';
        }

      }
      if( value == 4 ){

        if($("#subunderline").hasClass("btn-outline-secondary")){
          $("#subunderline").removeClass("btn-outline-secondary");
          $("#subunderline").addClass("btn-secondary");
          $("#subunderline").addClass("fontsubtitleselected");

          if($("#substrike").hasClass("fontsubtitleselected")){
            // objBannersubTextStyle['border-bottom']  =  '1px solid';
            objBannersubTextStyle['text-decoration']  =  'underline line-through';
          }else{
            objBannersubTextStyle['text-decoration']  =  'underline';
          }
          
        }else{
          $("#subunderline").removeClass("btn-secondary");
          $("#subunderline").addClass("btn-outline-secondary");
          $("#subunderline").removeClass("fontsubtitleselected");

          if($("#substrike").hasClass("fontsubtitleselected")){
            objBannersubTextStyle['text-decoration']  =  'line-through';
          }else{
            objBannersubTextStyle['text-decoration']  =  '';
          }
         
        }

      }
      if( value == 5 ){

        if($("#substrike").hasClass("btn-outline-secondary")){
          $("#substrike").removeClass("btn-outline-secondary");
          $("#substrike").addClass("btn-secondary");
          $("#substrike").addClass("fontsubtitleselected");
          // objBannersubTextStyle['text-decoration']  =  'line-through';
          if($("#subunderline").hasClass("fontsubtitleselected")){
            objBannersubTextStyle['text-decoration']  =  'underline line-through';
          }else{
            objBannersubTextStyle['text-decoration']  =  'line-through';
          }

        }else{
          $("#substrike").removeClass("btn-secondary");
          $("#substrike").addClass("btn-outline-secondary");
          $("#substrike").removeClass("fontsubtitleselected");
          // objBannersubTextStyle['text-decoration']  =  '';
          if($("#subunderline").hasClass("fontsubtitleselected")){
            objBannersubTextStyle['text-decoration']  =  'underline';
          }else{
            objBannersubTextStyle['text-decoration']  =  '';
          }

        }

      }

    }

  }

  function applyBannerTextStyle( value ){
    // let value = $(instance).val();
    // if( typeof value == 'undefined' ){
    //    console.log(" text value "+ value);
    //   return; 
    // }
    let oldvalue = value;
    value = value.replace("px","");
    value = parseInt( value );
    lineHeight = value;
    height   = value;
    // console.log(">>>>"+lineHeight );
    updateBannerstyle( bgcolor , textColor ,lineHeight, height, subtextColor );

    $("#bannerStyle").css('font-size',oldvalue);
    //$("#mobibannerStyle").css('font-size',value);
  }

  // =================================== add rule =========================================================================

  function addRule( instance , counter='' ){

   // console.log($(instance).val());
    let value = $(instance).val();
    if(value == 'product'){

// console.log('in');

    $('.ExProduct').css('display', 'none');
    // $('.ExProduct').setAttribute("style", "display: none !important;");
    $('#addExProduct').val('');
    }else{
    $('.ExProduct').css('display', 'block');
    }
    let htmlrule = `<td>
                      <div class="row mx-0 px-0">
                        <div class="mx-1">
                          <select name="rule[${counter}][conditiontype]" class="form-control" onchange="addRule(this,${counter});">
                            <option ${value =='all_products'?'selected':''} value="all_products">All Products</option>
                            <option ${value =='product'?'selected':''} value="product" >Select Products</option>
                            <option ${value =='product_type'?'selected':''} value="product_type">Product Type</option>
                            <option ${value =='product_price'?'selected':''} value="product_price">Product Price</option>
                            <option ${value =='product_vendor'?'selected':''} value="product_vendor">Product Vendor</option>
                            <option ${value =='title'?'selected':''} value="title">Product Title</option>
                            <option ${value =='weight'?'selected':''} value="weight">Product Weight</option>
                            <option ${value =='collection'?'selected':''} value="collection">Collection</option>
                            <option ${value =='page_type'?'selected':''} value="page_type">Page Type</option>                            
                            <option ${value =='saleprice'?'selected':''} value="saleprice" >Sale price</option>
                            <option ${value =='inventory'?'selected':''} value="inventory" >Inventory</option>
                            <option ${value =='tags'?'selected':''} value="tags" >Product Tags</option>
                            <option ${value =='createddate'?'selected':''} value="createddate">Product Create date</option>
                            <option ${value =='publishdate'?'selected':''} value="publishdate">Product Published date</option>
                            <option ${value =='product_metafield'?'selected':''} value="product_metafield">Product Metafield</option>
                            <option ${value =='product_option'?'selected':''} value="product_option">Variants</option>
                            <option ${value =='customer'?'selected':''} value="customer">Customer</option>
                            <option ${value =='customer_tag'?'selected':''} value="customer_tag">Customer Tags</option>
                          </select>
                        </div>`;

    switch( $(instance).val() ) {
      case 'collection':
        let customCollections = @json( $customCollections );
        let collectionDrop = '';
        if( customCollections != false ){
          customCollections.forEach( function( element ){
            //console.log( element );
            collectionDrop +=  `<li class="ml-2"> <span>
                    <input onchange="countCheck(${counter});" name="rule[${counter}][conditiontype][collection][]" type="checkbox" class="collection_listinput" value="${element.id}" id="collection"> ${element.title}
                    </span>
                    </li>`;
          });
        }
        // htmlrule += ` <span class="mt-2 mx-1">equals</span>
        //               <div class="mx-1">
        //                 <div class="btn-group">
        //                   <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        //                   -- Select --
        //                     <span class="caret"></span>
        //                   </button>
        //                   <ul class="dropdown-menu" id="collection_list" style="max-height:300px;overflow:auto;">
        //                     ${collectionDrop}
        //                   </ul>
        //                   <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
        //                     <i><span id="collectionscount">0</span> selected</i>
        //                   </span>
        //                 </div>
        //               </div>`;
        
        htmlrule += ` <div class="mx-1 mt-2"> </div>
                      <div class="mx-0 px-1 inventorytype" >
                        <select name="rule[${counter}][conditiontype][collection][type]" class="form-control"  >
                          <option  value="1">Equals</option>
                          <option  value="2" >Excludes</option>
                        </select>
                      </div>
                    <div class="mx-1">
                        <btn  onclick="openCollectionModel('#Collection-model',${counter});"  type="text" class="btn btn-secondary selectproduct" >Select collections</btn>

                        <input type="hidden"  name="rule[${counter}][conditiontype][collection][value]" type="text" class="form-control " id="addCollection_${counter}" placeholder="Selected Collections" value="">
                    </div>
                      <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                        <i><span id="collectionscount_${counter}">0</span> selected</i>
                      </span>
                        
                      `;
        break;

      case 'page_type':
        
        htmlrule += ` <span class="mt-2 mx-1">equals</span>
                      <div class="mx-1">
                      <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          -- Select --
                          <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" id="page_list" style="max-height:300px;overflow:auto;">
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="product" > Product Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="index" > Index Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="collection" > Collection Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="article" > Article Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="blog" > Blog Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="cart" > Cart Page
                              </span>
                          </li>
                          </ul>
                          <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                          <i><span id="pagescount">0</span> selected</i>
                          </span>
                      </div>
                      </div>`;
        break;
      
      case 'product':
        // let strProduct = $("#addProduct_"+counter).val();
        // console.log("#addProduct_"+counter);
        // console.log(strProduct);
        //strProduct = strProduct.split(",");
        // strProduct = strProduct.length;
        htmlrule += `<span class="mt-2 mx-1">equals</span>
                      <div class="mx-1">
                        <btn  onclick="openProductModel('#blacklistProduct-model',${counter});"  type="text" class="btn btn-secondary selectproduct" >Select products</btn>
                        <input type="hidden"  name="rule[${counter}][conditiontype][product][]" type="text" class="form-control product_csv" id="addProduct_${counter}" placeholder="Selected products " value="">
                      </div>
                      <a href="javascript:void(0);" class="mt-2 mx-1 selectedtext" onclick="openProductModel('#blacklistProduct-model',${counter},2);" >
                        <span class="text-primary pointer small">
                          <i><span id="productcount_${counter}" class="count_by_csv" >0</span> selected</i>
                        </span>
                      </a>
                      `;
          break;
      case 'inventory':
            htmlrule += `<span class="mt-2 mx-1">is</span>
                  <div class="mx-1" >
                    <select name="rule[${counter}][conditiontype][inventory][type]" class="form-control" onchange="inventoryType(this,${counter});">
                      <option  value="1">between</option>
                      <option value="2" >in stock</option>
                      <option value="3" >out of stock</option>
                      <option value="4" >out of stock but continue selling</option>
                    </select>
                  </div>

                  <div class="col mx-0 px-0 inventorytype" >
                    <input type="number" min="1" max="100" name="rule[${counter}][conditiontype][inventory][start]" value="" class="form-control " required>
                  </div>
                  <span class="inventorytype mt-2 mx-1">and </span>
                  <div class="col mx-0 px-0 inventorytype" >
                    <input type="number" min="1"  name="rule[${counter}][conditiontype][inventory][value]" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )">
                  </div>
                </div>`;
        break;
      case 'tags':
            htmlrule += ` <div class="mx-1 mt-2"> </div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][tags][type]" class="form-control"  >
                              <option  value="1">Equals To</option>
                              <option  value="2" >Excludes To</option>
                            </select>
                          </div>
                          <div class="mx-0 col">
                            <input data-role="tagsinput" name="rule[${counter}][conditiontype][tags][value]" size="1" type="text" class=" form-control tags tags" id="tags" placeholder="Enter product tags" value="">
                          </div>`;
        break;
        case 'customer_tag':
          htmlrule += ` <div class="mx-1 mt-2"> </div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][customer_tag][type]" class="form-control"  >
                              <option  value="1">Equals To</option>
                              <option  value="2" >Not Equals To</option>
                            </select>
                          </div>
                          <div class="mx-0 col">
                            <input data-role="customer_taginput" name="rule[${counter}][conditiontype][customer_tag][value]" size="1" type="text" class=" form-control customer_tag tags" id="customer_tag" placeholder="Enter customer tags" value="">
                          </div>`;
        break;
      case 'createddate':
            htmlrule += ` <span class="mt-2 mx-1"> is within the past </span>
                          <div class="mx-0 px-1 inventorytype " >
                            <input type="number" min="0" max="10000" name="rule[${counter}][conditiontype][createddate]" value="180" class="form-control">
                          </div>
                          <span class="mt-2 mx-1"> days </span>`;
        break;
      case 'publishdate':
          htmlrule += ` <span class="mt-2 mx-1"> is within the past </span>
                        <div class="mx-0 px-1 inventorytype " >
                          <input type="number" min="0" max="10000" name="rule[${counter}][conditiontype][publishdate]" value="180" class="form-control">
                        </div>
                        <span class="mt-2 mx-1"> days </span>`;
        break
      case 'all_products':
            htmlrule += ` <span class="mt-2 mx-1"> Banner will be displayed on all products </span>
                          <div class="mx-0 px-1 col inventorytype " >
                            <input type="hidden" name="rule[${counter}][conditiontype][all_products]" value="all_products" class="form-control">
                          </div>
                          <span class="mt-2 mx-1">  </span>`;
        break; 
      case 'title':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 title" >
                            <select name="rule[${counter}][conditiontype][title][type]" class="form-control"  >
                              <option value="1" >Starts With</option>
                              <option value="2" >Ends With</option>
                              <option value="3" >Contains</option>
                              <option value="4" >Not Contains</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 titlediv col" >
                            <input type="text" name="rule[${counter}][conditiontype][title][value]"  class="form-control" >
                          </div>`;
      break;
      case 'product_type':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_type][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Not Equal To</option>
                              <option value="3" >Starts With</option>
                              <option value="4" >Ends With</option>
                              <option value="5" >Contains</option>
                              <option value="6" >Not Contains</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                            <input type="text" name="rule[${counter}][conditiontype][product_type][value]"  class="form-control" >
                          </div>`;
        break;
        case 'product_price':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_price][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Not Equal To</option>
                              <option value="3" >Is Greater Than</option>
                              <option value="4" >Is Less Than</option>
                             
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                            <input type="number" min="1" name="rule[${counter}][conditiontype][product_price][value]"  class="form-control" >
                          </div>`;
        break;
      case 'product_vendor':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_vendor][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Not Equal To</option>
                              <option value="3" >Starts With</option>
                              <option value="4" >Ends With</option>
                              <option value="5" >Contains</option>
                              <option value="6" >Not Contains</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                            <input type="text" name="rule[${counter}][conditiontype][product_vendor][value]"  class="form-control" >
                          </div>`;
        break;
        case 'product_option':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_option][type]" class="form-control"  >
                            <option value="1">Product Has Variants</option>
                              <option value="2" >Product Does Not Have Variants</option>
                              
                            </select>
                          </div>`;
        break;
        case 'customer':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][customer][type]" class="form-control"  >
                              <option value="1">Is Logged In</option>
                              <option value="2" >Not Logged In</option>
                              
                            </select>
                          </div>`;
        break;
      case 'weight':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][weight][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Less Than</option>
                              <option value="3" >Greater Than</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}}" >
                            <input type="number" min="1" name="rule[${counter}][conditiontype][weight][value]"  class="form-control" >
                          </div><span class="mt-2 mx-1"> grams </span>`;
        break;    
           
      case 'saleprice':
            htmlrule += ` <span class="mt-2 mx-1"> is between </span>
                          <div class="col mx-0 px-0 " >
                            <input type="number" min="1" max="100" name="rule[${counter}][conditiontype][saleprice][start]" value="" class="form-control" required>
                          </div>
                          <span class="mt-2 mx-1">  % and </span>
                          <div class="col mx-0 px-0 " >
                            <input type="number" min="1" max="100" name="rule[${counter}][conditiontype][saleprice][end]" value="" class="form-control" required>
                          </div> <span class="mt-2 mx-1"> %</span>
                          <input type="hidden" name="rule[${counter}][conditiontype][saleprice]" value="" class="form-control" >`;
        
        break;

        case 'product_metafield':
            htmlrule += ` <span class="mt-2 mx-1"><a href="#" data-toggle="modal" data-target="#metafieldModal">name</a></span>
                          <div class="col mx-0 px-0 productmetafield metanameval" >
                            <input type="text" min="1" max="100" name="rule[${counter}][conditiontype][productmetafield][metaname]" value="" class="form-control" required>
                          </div>
                          <div class="mx-0 px-0 productmetafield " >
                            <select name="rule[${counter}][conditiontype][productmetafield][metacondition]" id="metafieldcond" class="form-control metafieldcond"  >
                              <option value="1">Equals</option>
                              <option value="2" >Found</option>
                              <option value="3" >Not Found</option>
                            </select>
                          </div>
                          <div class="col mx-0 px-0 productmetafield metafieldkey" id="metafieldkey" >
                          <input type="text" min="1" max="100" name="rule[${counter}][conditiontype][productmetafield][metaval]" value="" class="form-control" required>
                          </div>`;
        break;
    }

    htmlrule +=`</div></td>`;

    htmlrule += ` <td>
                    <div class="text-center mt-2">
                      <a onclick="deleteRule('${"#appendHtml"+counter}');" href="javascript:void(0)">
                        <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                      </a>
                    </div>
                  </td>`;
    if( counter == 0 ){
      counter = '';
    }
    $("#appendHtml"+counter).html( htmlrule );
    $('.tags').tagsinput({
      tagClass: function(item) {
        return 'banner banner-primary';
      }
    });
  }

  function inventoryType( instance , index ){
    let id = index == 0 ? '':index;
    id = '#appendHtml'+id;
    $(instance).parent().closest('div').removeClass('mx-1' );
    $(id+" .inventorytype").removeClass('d-none');
    if( $(instance).val() == '1' ){
      $(instance).parent().closest('div').addClass('mx-1' );
      $(id+" .inventorytype").show();
    }else{
      $(instance).parent().closest('div').addClass('mx-1' );
      $( id+" .inventorytype").hide();
    }
  }

  var intCounterRule = '{{!empty( $displayrules ) ? count( $displayrules ) -1 :0 }}';
  // var appendCounter = ''; 

  function addRuleHtml( ){
    intCounterRule++;
    // if( intCounterRule > 1 ){
    //   appendCounter = intCounterRule-1;
    // }
    // if( intCounterRule >= 5 ){
    //   return false;
    // }
    let htmlContainer = `<tr id="appendHtml${intCounterRule}" > </tr>`;
    // document.getElementById('appendHtml'+).insertAdjacentHTML('afterend', htmlContainer );
    // $( "#appendHtml" ).after().last();
    if( $("table #appendHtmlAll").children('tr').length > 5 ){
      return false;
    }
    $("table #appendHtmlAll").append( htmlContainer );
    // $(  htmlContainer ).insertAfter('');
    var defaultHtmlRule= `<td>
                            <div class="row mx-0 px-0">
                              <div class="mx-1">
                                <select class="form-control" name="rule[${intCounterRule}][conditiontype]" index=${intCounterRule} onchange="addRule(this,${intCounterRule});">
                                  <option selected value="all_products">All Products</option>
                                  <option value="product" >Select Products</option>
                                  <option value="product_type">Product Type</option>
                                  <option value="product_price">Product Price</option>
                                  <option value="product_vendor">Product Vendor</option>
                                  <option value="title">Title</option>
                                  <option value="weight">Weight</option>
                                  <option value="collection">Collection</option>
                                  <option value="page_type">Page Type</option>
                                  <option value="saleprice" >Sale price</option>
                                  <option value="inventory" >Inventory</option>
                                  <option value="tags" >Tags</option>
                                  <option value="createddate">Product Created Date</option>
                                  <option value="publishdate">Product Published Date</option>
                                  <option value="product_metafield">Product Metafield</option>
                                  <option value="product_option">Variants</option>
                                  <option value="customer">Customer</option>
                                  <option value="customer_tag">Customer Tag</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1"> Banner will be displayed on all products </span>
                              <div class="mx-0 px-1 col inventorytype " >
                                <input type="hidden" name="rule[${intCounterRule}][conditiontype][all_products]" value="all_products" class="form-control">
                              </div>
                              <span class="mt-2 mx-1"></span>
                            </div>
                          </td>
                          <td>
                            <div class="text-center mt-2">
                              <a onclick="deleteRule('${"#appendHtml"+intCounterRule}');" href="javascript:void(0)">
                                <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                              </a>
                            </div>
                          </td>`;
    $("#appendHtml"+intCounterRule).html(defaultHtmlRule );
  }

  function deleteRule( rulehtmlId ){
    // intCounterRule = ( intCounterRule-1 );
    //alert( intCounterRule );
    if( $("table #appendHtmlAll").children('tr').length == 1 ){
      return false;
    }
    $(rulehtmlId).remove();
  }

  var mytexttiny = '';
  var modalsize = '';
  function saveModalContent(){
    modalsize = $('#modalsize').val();
    mytexttiny = tinymce.get('mytextarea').getContent();
    $('#Linktab-model').modal('toggle');
  }

  function validateDate(number){
    if(number==0){
      var realvalue = $('#bannertitle').val();
    }else{
      var realvalue = $('#bannersubtitle').val();
    }
    
    if( realvalue.indexOf("[[today") != -1  ){
      var pos = realvalue.indexOf("[[today");
      var pos2 = realvalue.indexOf("]]");
      var startres = realvalue.slice(pos+8, pos2);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );

      if( realvalue.indexOf("[[today") != -1  ){
        var pos3 = realvalue.indexOf("[[today");
        var pos4 = realvalue.indexOf("]]");
        var endres = realvalue.slice(pos3+8, pos4);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        if(startres >= endres){
          $('#requestalert').show();
          $('#requestmessage').text('Delivery end date should be bigger than start date');
          $("#requestalert").delay(2500).hide(0);
          return "wrong";
        }

      }

    }
    return true;
  }

  function saveRule( id ){
    var image = '';
    image = $('.banner_image_preview').attr('src');

    var type = '';
    type = $("input[name='banner_type']:checked").val();
    var type_image = '';
    type_image = $('#image').val();
    var bannerlink = $('#bannerlink').val();

    if(mytexttiny == ''){
      var mytext = <?PHP echo  (!empty($banner['bannerdescription']) ? json_encode($banner['bannerdescription']) : '""'); ?>;
      var mybannerid = <?PHP echo ( isset($banner['productbannerid'] ) ? $banner['productbannerid'] : '""'); ?>;
      if(mybannerid != '' && mytexttiny == ''){
        mytext = tinymce.get('mytextarea').getContent();
      }
    }else{
      var mytext = mytexttiny;
    }

    if(modalsize == ''){
      var mymodalsize = <?PHP echo ( isset($banner['modalsize'])  ? $banner['modalsize'] : '1'); ?>;
    }else{
      var mymodalsize = modalsize;
    }

    if(type == 'image' ){
      image = $('.primebanPhotoOuter img').attr('src');
    }
    // alert(image);
    var banner_type = '';
    banner_type = <?PHP echo (!empty($banner['banner_type']) ? json_encode($banner['banner_type']) : '""'); ?>; 

    // if(banner_type == 'image' ){
      
    // }else{
    //   if(type == 'image'){
    //     if( type_image == "" || type_image == null){
    //       var text = "Image should not be empty.";
    //       alert_error_message(text);
    //       return false;
    //     }
    //   }
    // }
    
    // let formData = $(id).serialize();

    var font_style_class = $(".fonttitleselected");
    var font_values = [];
    for(var i = 0; i < font_style_class.length; i++){
      if( !$(font_style_class[i]).val() ) {                    
  
      }else{
        font_values.push($(font_style_class[i]).val() );
      }
    }
    if(!font_values){
      $('input[name="bannerstyle[fontstyle]"]').val('1');
    }else{
      $('input[name="bannerstyle[fontstyle]"]').val(font_values);
    }

    var subfont_style_class = $(".fontsubtitleselected");
    var subfont_values = [];
    for(var i = 0; i < subfont_style_class.length; i++){
      if( !$(subfont_style_class[i]).val() ) {                    
  
      }else{
        subfont_values.push($(subfont_style_class[i]).val() );
      }
    }
    if(!subfont_values){
      $('input[name="bannerstyle[subfontstyle]"]').val('1');
    }else{
      $('input[name="bannerstyle[subfontstyle]"]').val(subfont_values);
    }

    var shapevalue = $('#bannershape').find('.active').data('value');
    $('input[name="bannerstyle[bannershape]"]').val(shapevalue);

    var pro_uni_data = '';
    var read_data = '';
    if( $('.product_csv').val() ){
      var nows = $('.product_csv').val()
      arr =  nows.split(',');
      read_data = unique(arr);
      pro_uni_data = read_data.join(",");
      $('.product_csv').val(pro_uni_data);
    }

    // console.log($('.product_csv').val());

    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
    }

    var rulesList = ['product','saleprice','collection','product_type','product_price','product_vendor','title','weight','tags','inventory','customer_tag','page_type','product_metafield'];
    // console.log($(id).serializeArray());
    // return false;
    for(var k = 0; k <= intCounterRule; k++){
      var ruletype = $('[name="rule['+k+'][conditiontype]"]').val();

      for(var m=0; m<rulesList.length; m++){
        var rulename = rulesList[m];

        if(rulename == ruletype){
          var rulevalue = '';
          if(rulename == 'customer_tag'){
            rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+']"]').val();
          }else if(rulename == 'tags'){
            rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+']"]').val();
          }else if(rulename == 'product'){
            rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][]"]').val();
          }else if(rulename == 'collection'){
            rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][]"]').val();
          }else if(rulename == 'page_type'){
            if(parseInt($('#pagescount').text()) < '1'){
              rulevalue = '';
            }else{
              rulevalue = '1';
            }
          }else if(rulename == 'inventory'){
            var inventorytype = $('[name="rule['+k+'][conditiontype]['+rulename+'][type]"]').val();
            if(inventorytype == '1'){
              rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][value]"]').val();
              if(rulevalue < '1'){
                rulevalue = '';
              }
            }else{
              rulevalue = '1';
            }
          }else if(rulename == 'saleprice'){
            var salepricestart = $('input[name="rule['+k+'][conditiontype]['+rulename+'][start]"]').val();
            var salepriceend = $('input[name="rule['+k+'][conditiontype]['+rulename+'][end]"]').val();
            if(salepricestart == ''){
              rulevalue = '';
            }else if(salepriceend == ''){
              rulevalue = '';
            }else{
              $('input[name="rule['+k+'][conditiontype]['+rulename+']"]').val(salepricestart+','+salepriceend);
              rulevalue = '1';
            }
          }else if(rulename == 'product_metafield'){
            var metaCondition = $('input[name="rule['+k+'][conditiontype][productmetafield][metacondition]"]').val();
            
            var metaname = $('input[name="rule['+k+'][conditiontype][productmetafield][metaname]"]').val();
            if(metaname == ''){
              rulevalue = '';
            }else{
              rulevalue = '1';
            }
            if(metaCondition == '1'){
              var metaval = $('input[name="rule['+k+'][conditiontype][productmetafield][metaval]"]').val();
              if(metaval == ''){
                rulevalue = '';
              }
            }
          }else{
            rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][value]"]').val();
          }

          if(rulevalue == ''){
            $('#requestalert').show();
            $('#requestmessage').text('Missing data in display conditions');
            $("#requestalert").delay(2500).hide(0);
            return false;
          }
          break;
        }
      }
    }

    var validate = validateDate(0);
    if(validate=='wrong'){
      return false;
    }

    var validate2 = validateDate(1);
    if(validate2=='wrong'){
      return false;
    }
    
    // console.log($(id).serializeArray());
    // return false;
    
    let formData = $(id).serialize()+ "&image="+encodeURIComponent(image);

    var bannerpreview = encodeURIComponent($('#bannertitle').val());

    var bannersubpreview = encodeURIComponent($('#bannersubtitle').val());

    $('#bannerStyle').css('display', '');
    // console.log(type);
    if(type == 'image' ){
     console.log($('#bannerStyle_image').attr('style'));
      var appearance = {
                      'desktop':{
                        'spanStyleleft': $('.spanStyleleft').attr('style'),
                        'textStyle'    : $('#textStyle').attr('style'),
                        'spanStyle'    : $('#spanStyle').attr('style'),
                        'bannerStyle'   : $('#bannerStyle_image').attr('style'),
                        'bannertitleStyle':$('#bannertitleStyle').attr('style'),
                        'bannersubtitleStyle':$('#bannersubtitleStyle').attr('style'),
                      },
                      
                    };
    }else{
      var appearance = {
                      'desktop':{
                        'spanStyleleft': $('.spanStyleleft').attr('style'),
                        'textStyle'    : $('#textStyle').attr('style'),
                        'spanStyle'    : $('#spanStyle').attr('style'),
                        'bannerStyle'   : $('#bannerStyle').attr('style'),
                        'bannertitleStyle':$('#bannertitleStyle').attr('style'),
                        'bannersubtitleStyle':$('#bannersubtitleStyle').attr('style'),
                      },
                      
                    };
    }
    formData +='&appearance='+JSON.stringify( appearance );
    formData +='&bannerlink='+bannerlink;
    formData +='&bannertitle='+bannerpreview;
    formData +='&bannersubtitle='+bannersubpreview;
    formData +='&modalsize='+mymodalsize;
    formData +='&bannerdescription='+encodeURIComponent(mytext);
    // console.log(formData);
    // return false;
    $('#loadingalert').show();
    $('.savebtn').prop('disabled', true);  
      //console.log( formData );
      // return false;
 
    $.ajax( {
      url:"{{asset('bannerrule')}}",
      method:'POST',
      data :formData,
      success:function( response ){
       
        let productHtml= '';
        $('#loadingalert').hide();
        $('#requestmessage').text(response.msg);

        //console.log(response.submitType);
        if( response.submitType == 'created' ){
           setTimeout(function(){

            //  $('.savebtn').prop('disabled', true);  
            location.href = '{{asset('banner')}}';
            $('.savebtn').prop('disabled', false);
          },2000);
        
        } else {


          $('.savebtn').prop('disabled', false);


        }
        
        toggleAlert();
      }
    });
  }

  function getCollectionList( instance , index ){
    return false;
    $.ajax( {
      url:"{{asset('getcollection')}}",
      method:'GET',
      data :{'collection':''},
      success:function( response ){
        let productHtml= '';
        $('#loadingalert').hide();
        $('#requestmessage').text(response.msg);
        if( response.status == true ){
          setTimeout(function(){
            location.reload();
          },2000);
        }
        toggleAlert();
      }
    });
  }

  function countCheck( index  ){
    index = index == 0 ? '': index;
    let lengthCount = $("#appendHtml"+index+ ' #collection_list :checked').length;
    $( "#appendHtml"+index+ ' #collectionscount').text( lengthCount );
  }

  function countPageCheck( index  ){
    index = index == 0 ? '': index;
    let lengthCount = $("#appendHtml"+index+ ' #page_list :checked').length;
    $( "#appendHtml"+index+ ' #pagescount').text( lengthCount );
  }

  function bannerCssConfig( bcolor , tcolor , lineHeight , height , textsize, margintop= 0, marginright= 0, marginbottom= 0, marginleft=0, shape_padding='medium' ){
    var banner_padding = 'padding:0.5em 1em;';
    var banner_text = $('#banner_text').prop('checked');
    var banner_image = $('#banner_image').prop('checked');
    var banner_type = 'text';
    var subtcolor = '';
    if(banner_text == true){
      banner_type = 'text';
      subtcolor = $('#buttonsettingsubtextcol').val();
    }else if(banner_image == true){
      banner_type = 'image';
//      set line height 0 for image banners
      lineHeight = 0;
      tcolor = 'transparent';
      subtcolor = 'transparent';
      bcolor = 'transparent';
    }

    if(shape_padding == 'small'){
      banner_padding = 'padding:0.25em 0.5em;';
    }
    banner_padding = '';

    let bannerBorderRadius =  $("#bannerborderradius").val();

    var givenborder = '';
    if(bannerBorderRadius == 'no'){
      givenborder = 'border-radius: 0px;'
    }else if(bannerBorderRadius == 'small'){
      givenborder = 'border-radius: .25em;'
    }else if(bannerBorderRadius == 'medium'){
      givenborder = 'border-radius: .5em;'
    }else if(bannerBorderRadius == 'large'){
      givenborder = 'border-radius: 1em;'
    }
    // console.log(givenborder);
//    removed height & line-height from text banners
//    height:'+height+'px;line-height:'+lineHeight+'px;display: inline-block;vertical-align: middle;

    var InText =  {
            '11':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },

            '21':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '12':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;border-top-right-radius:1em;border-bottom-right-radius:1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '22':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            // Temporary Changes for Rectangle and Oval
            // '1':{
            //   'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;padding-right: 12px;',
            //   'spanStyle':'',
            //   'spanStyleleft':''
            // },
            // '2':{
            //   'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
            //   'spanStyle':'',
            //   'spanStyleleft':''
            // }, 

            '1':{
                'bannerStyle':'box-sizing: border-box;background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;padding-right: 12px;',
                'bannerStyle':'box-sizing: border-box;background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;'+banner_padding+givenborder,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
              },
            '2':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 2em;border-bottom-left-radius: 2em;border-top-right-radius: 2em;border-bottom-right-radius: 2em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '3':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 0.3em;border-bottom-left-radius: 0.3em;border-top-right-radius: 0.3em;border-bottom-right-radius: 0.3em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            }, 
            '4':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 2em 0em 0em 2em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            }, 
            '5':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 2em 2em 0em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '6':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 2em 0em 2em 0em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            }, 
            '7':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 2em 0em 2em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '8':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 0em 3em 0em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '9':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 3em 0em 0em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '10':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 3em 0em 0em 0em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '11':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 0em 0em 3em;'+banner_padding,
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
                  
          };

    var InImage =  {
            
            '11':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';font-size: 16px;position: relative;line-height:'+lineHeight+'px;max-width: 100%;padding-left: 12px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''

            },
            '21':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;line-height:'+lineHeight+'px;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '12':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;line-height:'+lineHeight+'px;max-width: 100%;padding-left: 12px;border-top-right-radius:1em;border-bottom-right-radius:1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '22':{
              'bannerStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;line-height:'+lineHeight+'px;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            // Temporary Changes for Rectangle and Oval
            '1':{
              'bannerStyle':'box-sizing: border-box;background-color:'+bcolor+';font-size: 16px;position: relative;line-height:'+lineHeight+'px;max-width: 100%;padding-left: 12px;padding-right: 12px;padding: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'bannertitleStyle': 'color: '+tcolor,
              'bannersubtitleStyle': 'color: '+subtcolor,
              'spanStyle':'',
              'spanStyleleft':''
            },
            '2':{
              'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;line-height:'+lineHeight+'px;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;padding: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'bannertitleStyle': 'color: '+tcolor,
              'bannersubtitleStyle': 'color: '+subtcolor,
              'spanStyle':'',
              'spanStyleleft':''
            },
            '3':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 0.3em;border-bottom-left-radius: 0.3em;border-top-right-radius: 0.3em;border-bottom-right-radius: 0.3em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },  
            '4':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 2em 0em 0em 2em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },  
            '5':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 2em 2em 0em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '6':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 2em 0em 2em 0em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },  
            '7':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 2em 0em 2em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },  
            '8':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 0em 3em 0em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },  
            '9':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 3em 0em 0em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },  
            '10':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 3em 0em 0em 0em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '11':{
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'bannerStyle':'box-sizing: border-box;background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-radius: 0em 0em 0em 3em;padding:0.5em 1em;',
                'bannertitleStyle': 'color: '+tcolor,
                'bannersubtitleStyle': 'color: '+subtcolor,
                'spanStyle':'',
                'spanStyleleft':''
            },    

          };

    if(banner_type == 'image'){
      console.log(InImage);
      return InImage;
    }else{
      return InText;
      console.log(InText);
    }     

  }

  function updateBannerstyle( bcolor , tcolor ,lineHeight,height,subtcolor){
    // let bannerleftStyle  = $("#bannerleftStyle").val();
    // let bannerrightStyle = $("#bannerrightStyle").val();
    // let indexCss        = bannerleftStyle+''+bannerrightStyle;

    var banner_text = $('#banner_text').prop('checked');
    var banner_image = $('#banner_image').prop('checked');
    var banner_type = 'text';
    if(banner_text == true){
      banner_type = 'text';
    }else if(banner_image == true){
      banner_type = 'image';
    }
    
    let bannerStyle  = $('#bannershape').find('.active').data('value');
    if(bannerStyle === undefined || bannerStyle === null){
      var indexCss  = 1;
    }else{
      var indexCss    = bannerStyle;
    }

    let textsize        = $("#bannertextsize").val();
    let subtextsize        = $("#bannersubtextsize").val();
    let bannerBorderSize =  $("#bannerbordersize").val();
    let bannerBorderStyle =  $("#bannerborderstyle").val();
       

    
    height = objTextSizeMapper[height];
    lineHeight = objLineHeightMapper[lineHeight];

    if( bannerBorderSize != "" ){
      bannerBorderSize = bannerBorderSize.replace("px");
      bannerBorderSize = parseInt( bannerBorderSize );
      bannerBorderSize = bannerBorderSize*2;
      height += bannerBorderSize;
    }

    let margintop  = $("#bannerleftStyleMargintop").val();
    let marginright  = $("#bannerleftStyleMarginright").val();
    let marginbottom  = $("#bannerleftStyleMarginbottom").val();
    let marginleft  = $("#bannerleftStyleMarginleft").val();

    let shape_padding  = $("#bannerinternalpadding").val();
    
    let objCssBanner     = bannerCssConfig( bcolor, tcolor, lineHeight, height, textsize, margintop, marginright, marginbottom, marginleft, shape_padding);
    // let objCssBanner     = bannerCssConfig( bcolor , tcolor ,lineHeight ,height,textsize );
    let objBannerStyle     = objCssBanner[indexCss]['bannerStyle'];
    let objBannertitleStyle     = objCssBanner[indexCss]['bannertitleStyle'];
    let objBannersubtitleStyle     = objCssBanner[indexCss]['bannersubtitleStyle'];
    let objBannerSpanStyle = objCssBanner[indexCss]['spanStyle'];
    let bannerStyleLeft    = objCssBanner[indexCss]['spanStyleleft'];


    $("#bannerStyle").attr('style', objBannerStyle);
    $("#bannertitleStyle").attr('style', objBannertitleStyle);
    $("#bannersubtitleStyle").attr('style', objBannersubtitleStyle);
    // $("#textStyle").css( objBannerTextStyle );

    $("#bannertitleStyle").css( objBannerTextStyle );
    $("#bannersubtitleStyle").css( objBannersubTextStyle );
    $("#spanStyle").attr('style', objBannerSpanStyle);
    // $("#bannerStyle").css('font-size',textsize);
    // $("#bannertitleStyle").css('font-weight','900');
    $("#bannertitleStyle").css('font-size',textsize);
    $("#bannertitleStyle").css('line-height',textsize);
    $("#bannersubtitleStyle").css('font-size',subtextsize);
    $("#bannersubtitleStyle").css('line-height',subtextsize);
    $(".spanStyleleft").attr( 'style',bannerStyleLeft );
    // $("#mobibannerStyle").attr( 'style', objBannerStyle);
    // $("#mobitextStyle").attr( 'style', objBannerTextStyle);
    // $("#mobispanStyle").attr( 'style', objBannerSpanStyle);
    updateBannerGradientwithBorderShadow("desktop");

    var banneropacity = $("#banneropacity").val();
    $("#bannerStyle").css('opacity',banneropacity);
    
    //reset image bgcolor, color and textsize
    if(banner_type == 'image')
    {
         $("#bannerStyle").css('background-color','transparent');
          $("#bannerStyle").css('color','transparent');
           $("#bannerStyle").css('font-size',0);
    }
    
  }

  function applyMobleBannerStyle( instance ){
    applyMoblieStyle( $(instance).val() );
  }

  function updateTextValue( value ){
    // console.log(value);
    if( value.indexOf("Inventory") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("Inventory", objDynamincMapper['Inventory'] );
    }
     if( value.indexOf("SaleAmount") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("SaleAmount", objDynamincMapper['SaleAmount'] );
    }
     if( value.indexOf("SalePercent") != -1 ){
      let percent = "";
      if( value.indexOf("%") != -1){
        percent = "%";
      }
      value = replaceSpecialSym( value );
      value = value.replace("SalePercent", objDynamincMapper['SalePercent']+percent );
    
    } 
     if( value.indexOf("product.vendor") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("product.vendor", objDynamincMapper['Vendor'] );
    
    }  
     if( value.indexOf("product.type") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("product.type", objDynamincMapper['Type'] );
    
    } 
     if( value.indexOf("VariantCount") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("VariantCount", objDynamincMapper['variantCount'] );
    }
    if( value.indexOf("OrderCount") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("OrderCount", objDynamincMapper['OrderCount'] );
    }
    if( value.indexOf("ProductSKU") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("ProductSKU", objDynamincMapper['ProductSKU'] );
    }
    if( value.indexOf("RecentSoldCount") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("RecentSoldCount", objDynamincMapper['RecentSoldCount'] );
    }
    if( value.indexOf("VisitorCounter") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("VisitorCounter", objDynamincMapper['VisitorCounter'] );
    }
    if( value.indexOf("primebanm") != -1  ){
      value = replaceSpecialSym( value );
      // value = value.replace("product.metafields", objDynamincMapper['product.metafields'] );
    }
    if( value.indexOf("[[today") != -1  ){
      var weekdays = <?php echo json_encode(Auth::User()->weekdays); ?>;
      var dateformatselected = '<?php echo Auth::User()->dateformat; ?>';
      var realvalue = value; 

      var pos = realvalue.indexOf("[[today");
      var pos2 = realvalue.indexOf("]]");
      var startres = realvalue.slice(pos+8, pos2);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );
      var pos3 = realvalue.indexOf("[[today");
      var pos4 = realvalue.indexOf("]]");
      var endres = realvalue.slice(pos3+8, pos4);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );

      var wrongdays = weekdays.split(",");
      // var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
      var startDate = new Date(new Date().getTime()+(parseInt(startres)*24*60*60*1000));
      var today = new Date(new Date().getTime());
      var firstinc=0;
      while (today <= startDate) {
        var getday = String(today.getDay());
        if(wrongdays.indexOf(getday) != -1){
          startDate.setDate(startDate.getDate() + 1);
          firstinc++;
        }
        today.setDate(today.getDate() + 1);
      }
      value = value.replace("[[today,"+startres+"]]", moment(startDate).format(dateformatselected));

      var extradays = firstinc+parseInt(endres);
      var endDate = new Date(new Date().getTime()+(extradays*24*60*60*1000));
      var todayend = startDate;
      var secinc=0;
      while (todayend <= endDate) {
        var getday = String(todayend.getDay());
        if(wrongdays.indexOf(getday) != -1){
          endDate.setDate(endDate.getDate() + 1);
          secinc++;
        }
        todayend.setDate(todayend.getDate() + 1);
      }
      value = value.replace("[[today,"+endres+"]]", moment(endDate).format(dateformatselected));
      
    }

    return value;
  }


  var data ={};
  //   $.get("https://next.json-generator.com/api/json/get/V1cGoKmDV", function(response ){
  //     console.log(response);

  //     data  = response;
  //     // use a data source with 'id' and 'name' keys
  //     // $("#query").typeahead({ source:data });
  // },'json');

  $(".buttonsettingBgcol").spectrum({
    color: bgcolor,
    flat: false,
     showInput: true,
     allowEmpty:true,
     showButtons: false,
    //className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxSelectionSize: 10,
    preferredFormat: "hex",
    clickoutFiresChange: true,
    move: function (color) {
      //$("#bannerStyle").css('color', );
      //$("#mobibannerStyle").css('color',color.toHexString());
      bgcolor = color.toHexString();
      updateBannerstyle( bgcolor , textColor ,lineHeight, height, subtextColor);
    },
    show: function () {
    },
    beforeShow: function () {
    },
    hide: function () {
    },
    change: function() {
    },
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
  });

  $("#buttonsettingtextcol").spectrum({
    flat: false,
    showInput: true,
    allowEmpty:true,
    showButtons: false,
    //className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxSelectionSize: 10,
    preferredFormat: "hex",
    clickoutFiresChange: true,
    // showAlpha :true,
    //localStorageKey: "spectrum.homepage",
    // togglePaletteMoreText: 'more',
    // togglePaletteLessText: 'less',
    //selectionPalette: [textColor],
    move: function (color) {
      textColor = color.toHexString();
      $("#bannertitleStyle").css('color',color.toHexString() );
      $("#mobibannerStyle").css('color',color.toHexString());
      // console.log("move"+color.toHexString());
    },
    // show: function () {
    // },
    // beforeShow: function () {
    // },
    hide: function () {
      // el.spectrum("reflow");
    },
    // change: function( value ) {
    // },
    localStorageKey: false,
    color: textColor,
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
  });

  $("#buttonsettingsubtextcol").spectrum({
    flat: false,
    showInput: true,
    allowEmpty:true,
    showButtons: false,
    //className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxSelectionSize: 10,
    preferredFormat: "hex",
    clickoutFiresChange: true,
    // showAlpha :true,
    //localStorageKey: "spectrum.homepage",
    // togglePaletteMoreText: 'more',
    // togglePaletteLessText: 'less',
    //selectionPalette: [subtextColor],
    move: function (color) {
      subtextColor = color.toHexString();
      $("#bannersubtitleStyle").css('color',color.toHexString() );
      $("#mobibannerStyle").css('color',color.toHexString());
      // console.log("move"+color.toHexString());
    },
    // show: function () {
    // },
    // beforeShow: function () {
    // },
    hide: function () {
      // el.spectrum("reflow");
    },
    // change: function( value ) {
    // },
    localStorageKey: false,
    color: subtextColor,
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
  });

  var exproductidsValue = '';
  // var excountervalue   = 0;

  function openExProductModel( modelid ,counter=0,selected=1 ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    });
    filterType = selected;
    $("#Exidtype_selection").text( filterType == 1 ? "All Products" :"Selected Products" );
    exproductidsValue = $("#addExProduct").val();
   
    getExProducts('',"", exproductidsValue );
    $('#Exsearchbox').val('');
    // $('#searchinputicon').val('');
  }

  function getExProducts( param ='', searchbox='' , strProduct='' ){
    $("#ExtableDataModel").html('');
    $('#loadingalert').show();
    var page =1;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
            method:"GET",
            url : '{{asset('shopify-exproducts')}}?page='+page+'&searchbox='+searchbox+'&products='+exproductidsValue+"&filterType="+filterType,
            dataType: 'html',
            success : function( data ){
              $('#loadingalert').hide();
              $('#ExtableDataModel').html( data );
              Producttable = $('#exproductlist').DataTable({
                  dom: "Bfrtip",
                  responsive: true,
                  "paging": true,
                  "pagingType": "numbers",
                  "searching" : false,
                  "ordering": false,
                  "bServerSide":false,
                  "bInfo" : false,
                  "pageLength": 10,
                });
            }
          });
  }

  $('.modal-content').keypress(function(e){

    if($(this).closest('#iconModal').length == 0){
      if(e.which == 13) {
        $("#filterModal").modal("hide");
        searchbox = $("#Exsearchbox").val().trim();
        getExProducts( '',searchbox ,exproductidsValue );
      }
    }
  });

  $("#exsearchItem").click( function(){
    searchbox = $("#Exsearchbox").val().trim();
    if (searchbox.length < 3) {
      return false;
    }
    getExProducts( '',searchbox ,exproductidsValue );
  });

  var arrExProductData = [];
  var objExProductData = {};

  function addExProduct( instance , productid ){
    if( objExProductData.length > 0){
      arrExProductData = objExProductData;
    }else{
      
      arrExProductData = [];
      let produtValue = $("#addExProduct").val();
      if( produtValue != ''){
        arrExProductData = produtValue.split(",");
      }
    }
    
    let productidnew = $(instance).attr('productid');
    if( $(instance).attr('added') ){
      $(instance).text('Select');
      $(instance).removeAttr('added');
      $(instance).removeClass('btn-outline-danger');
      $(instance).addClass('btn-outline-secondary');
      let index = arrExProductData.indexOf(productidnew);
      arrExProductData.splice(index, 1);
      objExProductData = arrExProductData;
    }else{
      
      $(instance).text('Remove');
      $(instance).attr('added',"1");
      $(instance).removeClass('btn-outline-secondary');
      $(instance).addClass('btn-outline-danger');
      // $("#successmark-"+productidnew).show();
      arrExProductData.push( productidnew );
      objExProductData = arrExProductData;
    }
    let totalCount = arrExProductData.length;
    $("#Exproductcount").text( totalCount );
    if( arrExProductData.length > 0 ){
      exproductidsValue = arrExProductData.join(",");
    }else{
      exproductidsValue = "";
    }
    $("#addExProduct").val(exproductidsValue);
  }

  $(".exidtype").click( function(){
    filterType = $(this).attr('value');
    $("#Exidtype_selection").text( $(this).text() );
    getExProducts('',"", exproductidsValue );
  });

  var productidsValue = '';
  var countervalue   = 0;

  function openProductModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    });
    filterType = selected;
    $("#idtype_selection").text( filterType == 1 ? "All Products" :"Selected Products" );
    productidsValue = $("#addProduct_"+counter).val();
    countervalue = counter;

    if( productidsValue != '' ){
      var nows = productidsValue;

      if(nows !=undefined){
        arr =  nows.split(',');
        read_data = unique2(arr);
        pro_uni_data = read_data.join(",");
        productidsValue = pro_uni_data;
      }
    }

    getProducts('',"", productidsValue );
    show_text_box();
    $('#searchbox').val('');
  }

  
  function addtohighlight(instance){

    var imgobj = $(instance).find('img').clone();
    imgobj= imgobj.removeAttr('onclick');
    imgobj = imgobj.css('margin','');
    imgobj = imgobj.css('cursor','none');
    imgobj = imgobj.css('max-height','25px');
    //imgobj = imgobj.css('height','25px');
    //imgobj = imgobj.css('width','25px');
    imgsrc = imgobj.attr('src');
    //console.log(imgsrc);

    
    $('.close').trigger('click');
    $('.banner_image_preview').attr('src', imgsrc );
  }


  function unique2(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
  }

  function show_text_box(){
    filterType = $('.idtype').attr('value');
    if((filterType) == 3 ){
      $("#selectviafilediv").removeClass('d-none');
      $("#tableDataModel").addClass('d-none');
    }else{
      $("#selectviafilediv").addClass('d-none');
      $("#tableDataModel").removeClass('d-none');
      $(".custom-file-csv").text('Choose file');
      $("#csv_file").val('');
      
    }
    return true;
  }

  var searchbox = '';
  var filterType = '';
  var Producttable = '';

  function getProducts( param ='', searchbox='' , strProduct='' ){
    $("#tableDataModel").html('');
    $('#loadingalert').show();
    var page =1;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
            method:"GET",
            url : '{{asset('shopify-products')}}?page='+page+'&searchbox='+searchbox+'&products='+productidsValue+"&filterType="+filterType,
            dataType: 'html',
            success : function( data ){
              $('#loadingalert').hide();
              $('#tableDataModel').html( data );
              Producttable = $('#productlist').DataTable({
                  dom: "Bfrtip",
                  responsive: true,
                  "paging": true,
                  "pagingType": "numbers",
                  "searching" : false,
                  "ordering": false,
                  "bServerSide":false,
                  "bInfo" : false,
                  "pageLength": 10,
                });
            }
          });
  }

  $('.modal-content').keypress(function(e){

    //console.log($(this).closest('#iconModal').length);

    if($(this).closest('#iconModal').length == 0){
      if(e.which == 13) {
        $("#filterModal").modal("hide");
        searchbox = $("#searchbox").val().trim();
        getProducts( '',searchbox ,productidsValue );
      }
    }
  });

  $("#searchItem").click( function(){
    searchbox = $("#searchbox").val().trim();
    if (searchbox.length < 3) {
      return false;
    }
    getProducts( '',searchbox ,productidsValue );
  });

  function getNextList( param ){
    getProducts( param , searchbox , productidsValue);
  }
  
//Function for CSV upload ..........
  $('#upload_csv').on('submit', function(event){
    event.preventDefault();
    $("#tableDataModel").html('');
    $('#loadingalert').show();
    $.ajax({
      url:"{{asset('csvimport')}}",
      method:"POST",
      data:new FormData(this),
      dataType: 'html',
      contentType:false,
      cache:false,
      processData:false,
      success:function(response)
      {
        if(response.length < 50){
          $('#loadingalert').hide();
          $('#requestalert_text').show();
          $('#requestmessage_text').text(response);
          $("#requestalert_text").delay(2500).hide(0);

        }else{
          $("#tableDataModel").removeClass('d-none');
          $('#tableDataModel').html(response);
          $('#loadingalert').hide();

          $('#requestalert_text').show();
          $('#requestmessage_text').text('File uploaded Successfully');
          $("#requestalert_text").delay(2500).hide(0);
        }
        
        
      }
    });

  });

  // function addCSV( ){

  //   var csv = uploadFile[0].files;
  //   var formData = new FormData($(this)[0]);
  //   formData.append('uploaded_file', $("#uploaded_file")[0].files[0]);
  //   formData.append('lastModifed', csv[0].lastModified);
  //   formData.append('fileName', csv[0].name);
  //   console.log(formData);

  //   // var file = document.getElementById("csv_file").files[0];
  //   var files = $('#csv_file')[0].files[0];
  //   console.log(files);

  //   var dataval = {'files':files, '_token': '{{csrf_token()}}'};
  //   console.log(dataval);

  //   $.ajax({
  //       method  :'POST',
  //       url     :"{{asset('csvimport')}}",
  //       data    : dataval,
  //       success: function( response ){

  //       }

  //     });

  //   // $.ajax({
  //   //   headers: {
  //   //      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //   //  },
  //   //     method:"POST",
  //   //     url : '{{asset('csvimport')}}',
  //   //     data: dataval,
  //   //     datatype: 'json',
  //   //     cache: false,
  //   //     contentType: false,
  //   //     processData: false,
  //   //     success : function( data ){
  //   //       $('#loadingalert').hide();
  //   //       // $('#requestmessage').text(data.msg);
  //   //       // toggleAlert();
  //   //       $('#tableDataModel').html( data );
  //   //     }
  //   //   });
  // }

  var arrProductData = [];
  var objProductData = {};

  function addProduct( instance , productid,condition='pass'){
    if(condition == 'pass'){
      if( objProductData[countervalue] ){
        arrProductData = objProductData[countervalue];
      }else{
        arrProductData = [];
        let produtValue = $("#addProduct_"+countervalue).val();
        if( produtValue != ''){
          arrProductData = produtValue.split(",");
        }
      }
      let productidnew = $(instance).attr('productid');
      
      if( $(instance).attr('added') ){
        $(instance).text('Select');
        $(instance).removeAttr('added');
        $(instance).removeClass('btn-outline-danger');
        $(instance).addClass('btn-outline-secondary');
        //$("#successmark-"+productidnew).hide();
        //delete objProductData[intCounterRule][productidnew];
        let index = arrProductData.indexOf(productidnew);
        // delete objProductData[index];
        arrProductData.splice(index, 1);
        objProductData[countervalue] = arrProductData;
      }else{
        $(instance).text('Remove');
        $(instance).attr('added',"1");
        $(instance).removeClass('btn-outline-secondary');
        $(instance).addClass('btn-outline-danger');
        // $("#successmark-"+productidnew).show();
        arrProductData.push( productidnew );
        objProductData[countervalue] = arrProductData;
      }
      if( arrProductData.length > 0 ){
        var my_array = arrProductData;
        arrProductData = getUnique(my_array);
      }

    }else{
      objProductData = {};
      arrProductData = [];
    }
    // console.log(arrProductData);
    // arrProductData = objProductData[intCounterRule];
    
    let totalCount = arrProductData.length;
    $("#productcount_"+countervalue).text( totalCount );
    if( arrProductData.length > 0 ){
      productidsValue = arrProductData.join(",");
    }else{
      productidsValue = "";
    }
    
    $("#addProduct_"+countervalue).val(productidsValue);
  }

  function getUnique(array){
      var uniqueArray = [];
      for(i=0; i < array.length; i++){
          if(uniqueArray.indexOf(array[i]) === -1) {
              uniqueArray.push(array[i]);
          }
      }
      return uniqueArray;
  }

  $(".pagination").addClass('justify-content-center');

  $("#blacklistProduct-model").on("hidden.bs.modal", function () {
    $('.productall input:checkbox:checked').removeAttr('checked');
  });

  function replaceSpecialSym( value ){
    value = value.replace("\{\{","");
    value = value.replace("\}\}","");
    value = value.replace("|","");
    value = value.replace("money","");
    value = value.replace("round","");
    value = value.replace("%",'');
    return value;
  }



  $(".idtype").click( function(){
    filterType = $(this).attr('value');
    if((filterType) == 3 ){
      $("#selectviafilediv").removeClass('d-none');
      $("#tableDataModel").addClass('d-none');
      
    }else{
      $("#selectviafilediv").addClass('d-none');
      $("#tableDataModel").removeClass('d-none');
      $(".custom-file-csv").text('Choose file');
      $("#csv_file").val('');
      
    }
    $("#idtype_selection").text( $(this).text() );
    getProducts('',"", productidsValue );
  });

  function openDynamicVariableModel( modelid ){
    $(modelid).modal({
          backdrop: 'static',
          keyboard: false
    });
  }

  function openemojis( modelid ){
    $(modelid).modal({
          backdrop: 'static',
          keyboard: false
    });
  }

  //$(document).on( 'change', '.color_picker', function( event, color ) {
  //  console.log( "ajay" );
  //});
  
  function addVariable( instance ){
    var mydynamicvariable = '';
    // $(".variableBtnText").text("Select");
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mydynamicvariable = mydynamicvariable + realvalue;
    // $("#bannertitle").val( $(instance).attr("value") );
    $("#bannertitle").val( mydynamicvariable );
    $("#bannertitle").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  function addProVariable( instance ){
    var mydynamicvariable = '';
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#bannertitle").val( mydynamicvariable );
    $("#bannertitle").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  function addProVariableDate( instance ){
    var mydynamicvariable = '';
    var mydynamicdate = $('#input_datetimepicker3').val();
    // console.log(mydynamicdate);
    $(instance).text("Selected");
    // var realvalue = $(instance).attr("value");
    // var realvalue = '\{\{ CountdownTimer:'+mydynamicdate+' \}\}';
    var realvalue = 'Sale ends in [[ CountdownTimer:'+mydynamicdate+' ]]';
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#bannertitle").val( mydynamicvariable );
    $("#bannertitle").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  function addProVariableExpectedDate( instance ){
    var mydynamicvariable = '';
    // $(instance).text("Selected");
    var first = $('#firstdate').val();
    var second = $('#seconddate').val();
    var realvalue = 'Expected delivery between [[today,'+first+']] and [[today,'+second+']]';
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#bannertitle").val( mydynamicvariable );
    $("#bannertitle").keyup();
    $("#openDynamicVariableModel-model").modal('hide');

  }

  function addGoldVariable( instance ){
    var mydynamicvariable = '';
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#bannertitle").val( mydynamicvariable );
    $("#bannertitle").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  
  function addsubVariable( instance ){
    var mysubdynamicvariable = '';
    // $(".variableBtnText").text("Select");
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mysubdynamicvariable = mysubdynamicvariable + realvalue;
    // $("#bannersubtitle").val( $(instance).attr("value") );
    $("#bannersubtitle").val( mysubdynamicvariable );
    $("#bannersubtitle").keyup();
    $("#openSubDynamicVariableModel-model").modal('hide');
  }

  function addsubProVariable( instance ){
    var mysubdynamicvariable = '';
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mysubdynamicvariable = mysubdynamicvariable + realvalue;
    $("#bannersubtitle").val( mysubdynamicvariable );
    $("#bannersubtitle").keyup();
    $("#openSubDynamicVariableModel-model").modal('hide');
  }

  function addsubProVariableDate( instance ){
    var mysubdynamicvariable = '';
    var mysubdynamicdate = $('#input_datetimepicker4').val();
    // console.log(mydynamicdate);
    $(instance).text("Selected");
    var realvalue = 'Sale ends in [[ CountdownTimer:'+mysubdynamicdate+' ]]';
    mysubdynamicvariable = mysubdynamicvariable + realvalue;
    $("#bannersubtitle").val( mysubdynamicvariable );
    $("#bannersubtitle").keyup();
    $("#openSubDynamicVariableModel-model").modal('hide');
  }

  function addsubProVariableExpectedDate( instance ){
    var mysubdynamicvariable = '';
    // $(instance).text("Selected");
    var first = $('#firstdatesub').val();
    var second = $('#seconddatesub').val();
    var realvalue = 'Expected delivery between [[today,'+first+']] and [[today,'+second+']]';
    mysubdynamicvariable = mysubdynamicvariable + realvalue;
    $("#bannersubtitle").val( mysubdynamicvariable );
    $("#bannersubtitle").keyup();
    $("#openSubDynamicVariableModel-model").modal('hide');

  }

  function addsubGoldVariable( instance ){
    var mysubdynamicvariable = '';
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mysubdynamicvariable = mysubdynamicvariable + realvalue;
    $("#bannersubtitle").val( mysubdynamicvariable );
    $("#bannersubtitle").keyup();
    $("#openSubDynamicVariableModel-model").modal('hide');
  }


  function seteffect(){

   
    //var supportedFlag = $.keyframe.isSupported();
    //console.log(supportedFlag);
    var animationType = $('#banneranimation').val();
    console.log(animationType);

    if(animationType == 'slide'){

     
       // document.getElementById('bannerStyle').classList.add("animate__slideInDown");

     //  const element = document.querySelector('#bannerStyle');
     // element.classList.add('animate__slideInDown');

      $.keyframe.define({
          name: 'slidedown',
          from: {
              '-webkit-transform': 'translate3d(0, -100%, 0)',
              'transform'       : 'translate3d(0, -100%, 0)',
              'visibility'      : 'visible'
          },
          to: {
              'transform': 'rotate(360deg)'
          }
      });

      $("#bannerStyle").playKeyframe({
          name: 'slidedown', 
          duration: '1s', 
          timingFunction: 'linear', 
          delay: '0s', 
          complete: function(){} 
      }); 

    } else if (animationType == 'pop'){

        $.keyframe.define({
          name: 'pop',
          from: {
              'opacity': '0',
              '-webkit-transform': 'scale3d(0.3, 0.3, 0.3)',
              'transform' : 'scale3d(0.3, 0.3, 0.3)'
          },
          '50%': {
               'opacity': '1'
          }
        });

        $("#bannerStyle").playKeyframe({
            name: 'pop', 
            duration: '1s', 
            timingFunction: 'linear', 
            delay: '0s', 
            complete: function(){} 
        });

    } else if (animationType == 'groove'){

        $.keyframe.define({
          name: 'groove',
          from: {
            'opacity': '0',
            '-webkit-transform': 'scale(0.1) rotate(30deg)',
            'transform': 'scale(0.1) rotate(30deg)',
            '-webkit-transform-origin': 'center bottom',
            'transform-origin': 'center bottom'
          },

          '50%': {
            '-webkit-transform': 'rotate(-10deg)',
            'transform': 'rotate(-10deg)'
          },

          '70%': {
            '-webkit-transform': 'rotate(3deg)',
             'transform': 'rotate(3deg)'
          },

          to: {
            'opacity': '1',
            '-webkit-transform': 'scale(1)',
            'transform': 'scale(1)'
          }
        });

        $("#bannerStyle").playKeyframe({
            name: 'groove', 
            duration: '1s', 
            timingFunction: 'linear', 
            delay: '0s', 
            complete: function(){} 
        });

    } else if (animationType == 'fly'){

        $.keyframe.define({
          name: 'fly',
           '0%': {
              '-webkit-transform': 'translateY(-1200px) scale(0.7)',
              'transform': 'translateY(-1200px) scale(0.7)',
              'opacity': '0.7'
            },

            '80%': {
              '-webkit-transform': 'translateY(0px) scale(0.7)',
              'transform': 'translateY(0px) scale(0.7)',
              'opacity': '0.7'
            },

            '100%': {
              '-webkit-transform': 'scale(1)',
              'transform': 'scale(1)',
              'opacity': '1'
            }
        });

        $("#bannerStyle").playKeyframe({
            name: 'fly', 
            duration: '1s', 
            timingFunction: 'linear', 
            delay: '0s', 
            complete: function(){} 
        });

    }  else if (animationType == 'swerve'){

        $.keyframe.define({
          name: 'swerve',
          '20%': {
            '-webkit-transform': 'rotate3d(0, 0, 1, 15deg)',
            'transform': 'rotate3d(0, 0, 1, 15deg)'
          },

          '40%': {
            '-webkit-transform': 'rotate3d(0, 0, 1, -10deg)',
            'transform': 'rotate3d(0, 0, 1, -10deg)'
          },

          '60%': {
            '-webkit-transform': 'rotate3d(0, 0, 1, 5deg)',
            'transform': 'rotate3d(0, 0, 1, 5deg)'
          },

          '80%': {
            '-webkit-transform': 'rotate3d(0, 0, 1, -5deg)',
            'transform': 'rotate3d(0, 0, 1, -5deg)'
          },

          to: {
            '-webkit-transform': 'rotate3d(0, 0, 1, 0deg)',
            'transform': 'rotate3d(0, 0, 1, 0deg)'
          }
        });

        $("#bannerStyle").playKeyframe({
            name: 'swerve', 
            duration: '1s', 
            timingFunction: 'linear', 
            delay: '0s', 
            complete: function(){} 
        });

    }



 
  }

  function applyMoblieStyle( value ){
    let oldvalue = value;
    value = value.replace("px","");
    value = parseInt( value );
    // if( value % 2 == 0 ){
    //   value +=8;
    // }else{
    //    value +=8+1;
    // }
    lineHeightvalue = objLineHeightMapper[value];
    value = objTextSizeMapper[value];
    let lineHeight = lineHeightvalue;
    let height     = value;
    let bannerleftStyle  = $("#bannerleftStyle").val();
    let bannerrightStyle = $("#bannerrightStyle").val();
    let indexCss        = bannerleftStyle+''+bannerrightStyle;
    let textsize        = oldvalue;
    let objCssBanner     = bannerCssConfig( bgcolor , textColor ,lineHeight ,height ,textsize, subtextColor );
    //console.log(objCssBanner);
    let objBannerStyle     = objCssBanner[indexCss]['bannerStyle'];
    let objBannerSpanStyle = objCssBanner[indexCss]['spanStyle'];
    let bannerStyleLeft    = objCssBanner[indexCss]['spanStyleleft'];
    $("#mobibannerStyle").attr( 'style', objBannerStyle);
    $("#mobitextStyle").css( objBannerTextStyle );
    $("#mobispanStyle").attr( 'style', objBannerSpanStyle);
    $(".spanStyleleft").attr( 'style',bannerStyleLeft );
    $("#mobibannerStyle").css('font-size',oldvalue);
    updateBannerGradientwithBorderShadow("mobile");
  }

  applyMoblieStyle( $("#mobiletextsize").val() );

</script>

@if( !empty( $banner ) && is_array( $bannerstyle ) )
  <script type="text/javascript">

    $(function() {
      applyBannerStyle('','Left','{{$bannerstyle['left']}}' );
      applyBannerStyle('','Right','{{$bannerstyle['right']}}' );
      // applyBannerStyle('','Font','{{$bannerstyle['fontstyle']}}' );
      applyBannerStyle('','Text','{{$bannerstyle['textsize']}}' );
      applyBannerStyle('','SubText','{{$bannerstyle['subtextsize']}}' );
      $('.tags').tagsinput({
        tagClass: function(item) {
          return 'banner banner-primary';
        }
      });
    });

  </script>
@else
  <script type="text/javascript">
    $(function() {
      applyBannerStyle('','Left',$("#bannerleftStyle").val() );
      applyBannerStyle('','Right',$("#bannerrightStyle").val() );
      // applyBannerStyle('','Font',$("#bannerfontstyle").val() );
      applyBannerStyle('','Text',$("#bannertextsize").val() );
      applyBannerStyle('','SubText',$("#bannersubtextsize").val() );

    });
  </script>
@endif

</script>
@stop

