
<div class="modal fade bd-example-modal-lg" id="iconModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header bg-primary" id="">
        <h5 class="modal-title">
         Icon Picker
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="input-group container mb-1" >
            <input class="form-control" id="searchinputicon" type="text" placeholder="Search Icons" aria-label="Search" >
            <div class="dropdown input-group-append">
                  <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="btn dropdown-toggle btn-outline-secondary" href="#">
                  <span id="menu" styletype="" >All</span><span class="caret ml-1"></span></a>
                    <ul class="dropdown-menu scrollable-menu  multi-level " id="icontype" aria-labelledby="dropdownMenu">
                      <li><a class="dropdown-item active" styletype="" href="#">All</a></li>
                      <li><a class="dropdown-item" styletype="Color" href="#">Color</a></li>
                      <li><a class="dropdown-item" styletype="Ultraviolet" href="#">Blue UI</a></li>
                      <li><a class="dropdown-item" styletype="Bubbles" href="#">Circle Bubbles</a></li>
                      <li><a class="dropdown-item" styletype="Clouds" href="#">Cloud</a></li>
                      <li><a class="dropdown-item" styletype="Plasticine" href="#">Color Hand Drawn</a></li>
                      <li><a class="dropdown-item" styletype="Cute-clipart" href="#">Cute Clipart</a></li>
                      <li><a class="dropdown-item" styletype="Dusk" href="#">Cute Color</a></li>
                      <li><a class="dropdown-item" styletype="Wired" href="#">Cute Outline</a></li>
                      <li><a class="dropdown-item" styletype="Doodle" href="#">Doodle</a></li>
                      <li><a class="dropdown-item" styletype="Dotty" href="#">Dotted</a></li>
                      <li><a class="dropdown-item" styletype="Emoji" href="#">Emoji</a></li>
                      <li><a class="dropdown-item" styletype="Fluent" href="#">Fluent</a></li>
                      <li><a class="dropdown-item" styletype="Nolan" href="#">Gradient Line</a></li> 
                      <li><a class="dropdown-item" styletype="Carbon-copy" href="#">Hand Drawn</a></li>
                      <li><a class="dropdown-item" styletype="Flat_round" href="#">Infographic</a></li>
                      <li><a class="dropdown-item" styletype="IOS" href="#">IOS</a></li>
                      <li><a class="dropdown-item" styletype="IOS-filled" href="#">IOS Filled</a></li> 
                      <li><a class="dropdown-item" styletype="IOS-glyphs" href="#">IOS Glyph</a></li>
                      <li><a class="dropdown-item" styletype="Android" href="#">Ice Cream</a></li>
                      <li><a class="dropdown-item" styletype="Material" href="#">Material</a>
                      <li><a class="dropdown-item" styletype="Material-outlined" href="#">Material Outlined</a></li>
                      <li><a class="dropdown-item" styletype="Material-rounded" href="#">Material Rounded</a></li>
                      <li><a class="dropdown-item" styletype="Material-sharp" href="#">Material Sharp</a></li>
                      <li><a class="dropdown-item" styletype="Material-two-tone" href="#">Material Two Tone</a></li>
                      <li><a class="dropdown-item" styletype="Office" href="#">Office</a>
                      <li><a class="dropdown-item" styletype="Officel" href="#">Office L</a></li>
                      <li><a class="dropdown-item" styletype="Offices" href="#">Office S</a></li>
                      <li><a class="dropdown-item" styletype="Officexs" href="#">Office XS</a></li>
                      <li><a class="dropdown-item" styletype="Cotton" href="#">Pastel</a></li>
                      <li><a class="dropdown-item" styletype="Pastel-glyph" href="#">Pastel Glyph</a></li>
                      <li><a class="dropdown-item" styletype="Small" href="#">Simple Small</a></li>
                      <li><a class="dropdown-item" styletype="Windows" href="#">Windows 10</a></li>
                      <li><a class="dropdown-item" styletype="Metro" href="#">Windows Metro</a></li>
                     
                    </ul>
                  </div>
          
           
            <div id="bg-color-picker" class="colorpicker-element" >
              
              <div class="pickr"></div>
               {{-- <input type="text" name="display[bgcolor]" class="form-control iconBgcol" id="iconBgcol" value=""> --}}
               
            </div> 
             <label class="form-check-label d-none" id="colorval" for="inlineRadio1">Simple</label>
            <div class="input-group-append">
              <span class="input-group-text" id="searchicon">Search</span>
            </div>
            
          </div>

        
          <div id="wrapper">
            <section class="container icon-div text-center">
              
              {{-- <div class="vl"></div> --}}
              <div class="row imglist mt-2">
               
              </div>
            </section>
          
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/nano.min.css"/> 
<script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>

<script>

var inputsearch = document.getElementById("searchinputicon");

// Execute a function when the user releases a key on the keyboard
  inputsearch.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      document.getElementById("searchicon").click();
    }
  });

</script>

<script type="text/javascript">

$(document).ready(function(){

var globalcolor = '#444444';

$('.dropdown-menu a').click(function(){
  $('#menu').text($(this).text());
  $('#menu').attr('styletype',$(this).attr('styletype'));
});

$('#searchicon').css('cursor','pointer');

$('#colorval').text('#444444');


function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

var decodedCookie = decodeURIComponent(document.cookie);
var imgcolor = getCookie("primeimgcolor");
var imgstyle = getCookie("primeimgstyle");
if (imgcolor != "") {
  globalcolor = imgcolor;
  $('#colorval').text(globalcolor);
} 
if (imgstyle != "") {
  var styletext = imgstyle.trim().toUpperCase();
  $('#icontype a').each(function(key) {
    $(this).removeClass('active');
    if($(this).text().toUpperCase() == styletext){
      $(this).addClass('active');
      $('#menu').text($(this).text());

      $('#menu').attr('styletype',$(this).attr('styletype').trim());
    }
  });
} 

$('#searchicon').on('click',function(){
  getsearchedicon();
});


$('#icontype li').on('click',function(){

  $('#icontype').find('.active').removeClass('active');
  $(this).find('a').addClass('active');
  var imgstyle= $(this).find('a').text().trim().toLowerCase();
  var imgcolor= $('#colorval').text().replace('#','');

  document.cookie = "primeimgcolor="+imgcolor;
  document.cookie = "primeimgstyle="+imgstyle;
  // var decodedCookie = decodeURIComponent(document.cookie);
  var imglist='';

  var searchvalue = $('#searchinputicon').val().trim();

  if(searchvalue != ''){

    getsearchedicon();
     
  } else {
   
  }

   $('.imglist').html(imglist);
  
});

const pickr = Pickr.create({
    el: '.pickr',
    theme: 'nano', 
    default: globalcolor,

    swatches: [
        'rgba(244, 67, 54, 1)',
        'rgba(233, 30, 99, 1)',
        'rgba(37, 57, 174, 1)',
        'rgba(76, 21, 86, 1)',
        'rgba(63, 81, 181, 1)',
        'rgba(33, 150, 243, 1)',
        'rgba(3, 169, 244, 1)',
        'rgba(0, 188, 212, 1)',
        'rgba(0, 150, 136, 1)',
        'rgba(76, 175, 80, 1)',
        'rgba(139, 195, 74, 1)',
        'rgba(205, 220, 57, 1)',
        'rgba(255, 235, 59, 1)',
        'rgba(255, 193, 7, 1)'
    ],

    components: {

        // Main components
        preview: true,
        opacity: false,
        hue: true,

        // Input / output Options
        interaction: {
            hex: true,
            rgba: true,
        // hsla: true,
        // hsva: true,
        // cmyk: true,
            input: true,
            clear: true,
            save: true
        }
    }
});

pickr.on('save', (color, instance) => {
    //console.log(color);

    if(color != null){

      $('#colorval').text(color.toHEXA().toString(3).toLowerCase());
      document.cookie = "primeimgcolor="+$('#colorval').text(color.toHEXA().toString(3).toLowerCase()).text();
    } else {

      $('#colorval').text('');
    }

    
    var searchvalue = $('#searchinputicon').val().trim();
    var imgstyle= $('#menu .active').find('a').text().trim().toLowerCase();
    //var imgcolor= $(this).val().replace('#','');
    var imglist='';

    if(searchvalue != ''){

        // console.log('testttttt');
        getsearchedicon();

    } else {
    
    
    }

    $('.imglist').html(imglist);
    
});




});

 






    function getsearchedicon(){

    var searchvalue = $('#searchinputicon').val().trim().toLowerCase().replace(' ','-');
    var imgstyle= $('#menu').attr('styletype').trim().toLowerCase();
    var imgcolor= $('#colorval').text().replace('#','');
    var imglist='';

    var imagetypearr = {'color':'Color','ios-glyphs':'IOS Glyphs','ios':'IOS','ios-filled':'IOS Filled', 'material':'Material','material-outlined':'Material Outlined','material-rounded':'Material Rounded','material-two-tone':'Material Two Tone', 'material-sharp':'Material Sharp','windows':'Windows 10','office':'Office','officel':'Office L','offices':'Office S','officexs':'Office XS','wired':'Cute Outline','dusk':'Cute Color','ultraviolet':'Blue UI','dotty' :'Dotted','nolan':'Gradient Line','small':'Simple Small','cotton':'Pastel','android':'Ice Cream','emoji' : 'Emoji','fluent' : 'Fluent','doodle' :'Doodle','flat_round':'Infographic','metro':'Windows Metro','clouds':'Clouds','bubbles':'Circle Bubbles','plasticine': 'Color Hand Drawn','carbon-copy':'Hand Drawn','pastel-glyph':'Pastel Glyph','cute-clipart':'Cute Clipart'};

    //console.log(imgstyle);
    $('#loadingalert').show();
    $('.imglist').empty();
    $('#wrapper .descriptionalert').remove();
    $('#iconmessage').remove();
    

    $.ajax({
        method:"POST",
        url : "{{ asset('searchicons') }}",
        data: "&_token={{csrf_token()}}&searchval="+searchvalue+"&imgstyle="+imgstyle+"&imgcolor="+imgcolor,
        
        success : function( data ){
         
          if(data['iconlistname'] != undefined){
 
            var iconlist = data['iconlistname'];
            $(iconlist).each(function(index, obj) {

              var image = new Image(); 

              if(imgstyle == ''){

                image.src = iconlist[index];

              } else {

                image.src = 'https://img.icons8.com/'+imgcolor+'/'+imgstyle+'/3x/'+iconlist[index]+'.png';

              }
              
              image.onload = function (evt){

                if(imgstyle != ''){

                  imglist +='<div class="col-4 col-sm-3 col-md-2 iconbox" style="cursor:pointer;" onclick="addtohighlight(this);" >\n\
                            <img class="" src="https://img.icons8.com/'+imgcolor+'/'+imgstyle+'/3x/'+iconlist[index]+'.png" style="height:50px;" >\n\
                            <p class="small text-muted">'+iconlist[index]+'</p>\n\
                          </div>';

               
                  $('.imglist').html(imglist);
             

                } else {

                    var str = iconlist[index];
                    var n = str.lastIndexOf('/');
                    var result = str.substring(n + 1);
                    result= result.replace('.png','');

                    var icontype= str.split('/');
                    //console.log(icontype);

                    iconlist[index] = iconlist[index].replace('icons8.com/','icons8.com/'+imgcolor+'/');

                    imglist +='<div class="col-4 col-sm-3 col-md-2 iconbox" style="cursor:pointer;" onclick="addtohighlight(this);" >\n\
                              <img class="" src="'+iconlist[index]+'" style="height:50px;" >\n\
                              <p class="small text-muted">'+result+'</br><span class="small"><i>'+imagetypearr[icontype[3]]+'</i></span></p>\n\
                            </div>';

                    
                    $('.imglist').html(imglist);

                 

                }
 
              
              } 

              
              $('#loadingalert').hide();
            

            });

            if(imgstyle == ''){
              $('#wrapper').append('<div class="text-center alert alert-secondary mb-0 descriptionalert ">For more options select a style and search again</div>');
            } 
            
          } else {

            $('.imglist').after('<p id="iconmessage">No Icons found</p>');
            $('#loadingalert').hide();
          }

         
        }

    });
       
  }

  


</script>