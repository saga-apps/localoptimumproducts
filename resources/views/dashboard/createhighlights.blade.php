@extends('layouts.master') @section('title') @parent
<title>{{ Request::is('edithighlight*') ? "Edit Highlight" :"Create Highlight" }}</title>
@stop @section('description') @parent
<meta content="" name="description" />
@stop
@section('css')
 @parent

@stop
@section('js')
@parent

<style>

  #ShopLanguagelist_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}
  #ShopLanguagelistadded_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}

  #Countrylist_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}
  #Countrylistadded_wrapper{display:block;overflow-y: scroll;max-height: 450px;overflow-x: hidden;}

  .dataTables_filter{
    float: right !important;
  }

    .accordion .card {
          overflow: visible;
  }
  .table-responsive {
        overflow-x: hidden !important;
  }
  .sp-replacer {  
      padding: 8px  !important;
      color: #212529 !important;
      height: 38px !important;
      border: 1px solid #ced4da !important;
      /*border-radius: .25rem !important;*/
      width: 100%;
  }
  .bootstrap-tagsinput input {
      display: block;
      width: 100%;
      padding: .375rem .75rem;
      font-size: 1rem;
      line-height: 1.5;
      color: #495057;
      background-color: #fff;
      background-clip: padding-box;
      border: 1px solid #ced4da;
      border-radius: .25rem;
      transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
      min-height: 28px;
  }
  .intro{
    display: none !important;
  }

  .metanameval{
    padding-left:0;
  }

  #textStyle {
      white-space: pre-wrap;
  }

  #wrapper {
    padding-left: 0;
   
    
  }

  
 .sidebar-nav li {
    text-indent: 15px;
    line-height: 40px;
    padding-right:14px;
    
 }

 ul {
  list-style-type: none;
 }

  .vl {
    display:inline-block;
    border-left: 1px solid;
    height: 400px;
    color:#dee2e6;
    float:left;
  }

  #colorval{

   /* margin-left:20%; */
       margin-top: 1%;

  }

  .column {
    float: left;
    width: 10%;
    padding-right: 15%;
    padding-bottom: 4%;
  }

  .icon-div {
    width: 100%;
    margin: auto;
    padding: 10px;
  }


  #menu a{

    color:black;
    margin-left: -20px;
  }

  .flex-column {
    flex-direction: column!important;
    width: 100px;
    text-align: center;
}

.img-fluid{
  margin-bottom:0% !important;
}
 
  .paginate_button{
      border-color: #ffc107;
  }
  .dataTables_wrapper .dataTables_paginate  {    
    /* border: 1px solid #d39e00 !important; */
    background-color: #e0a800 !important;
    background: none !important;
    color: #ffc107 !important;
}   
  .pagination .active {
    padding: 5px;
    background-color: #ffc107 !important;
  }

  a.paginate_button:hover {
    background-color: #ffc107 !important;
    color: #ffc107 !important;
  }
  
  .active a{
    color: #fff !important;
  } 

  .dataTables_filter{
    padding: 0 0 0 65%;
  }
  .dataTables_paginate{
      margin:0 auto !important;
  }
   .iconbox {   
    border: 1px solid #fff;
}

 .iconbox:hover {
    opacity: 0.75;
    border: 1px solid #ddd;
}

.scrollable-menu {
    height: auto;
    width: 200px !important;
    max-height: 250px;
    overflow-x: hidden;
}


.pcr-button{

  height:2.4em !important;
  width: 3em !important;

}

@media (max-width: 767px) { 
    div.dataTables_paginate ul.pagination{
      max-width: 85%;
    }
    .dataTables_filter{
      padding: 0 0 0 50%;
    }
    div.dataTables_filter input{
      max-width: 45%;
    }
}


.highlight{display:inline-block;padding:.25em .4em;font-size:75%;font-weight:700;line-height:1;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}
.highlight-primary{color:#212529;background-color:#ffc107}
a.highlight-primary:focus,a.highlight-primary:hover{color:#212529;background-color:#d39e00}
a.highlight-primary.focus,a.highlight-primary:focus{outline:0;box-shadow:0 0 0 .2rem rgba(255,193,7,.5)}


</style>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.tiny.cloud/1/wrz7bhvawkamdmyyvadqxu19hm5et399aifwiejk5hhc1fjk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

@stop
@section('content')
@parent

<?php
$inventoryHide = '';
if( !empty( $highlight ) ) {
  $display       = json_decode( $highlight_style['display'] ,true );
  $displayrules  = json_decode( $highlight['displayrules'] ,true );
  // $highlightstyle    = json_decode( $highlight['highlightstyle'] ,true );
  if(Auth::User()->planid > 2){
    $displayprorules  = json_decode( $highlight['displayprorules'] ,true );
  }else{
    $displayprorules  = '';
  }
  if(strpos($highlight['title'], 'product.metafields') !== false){
    $highlight['title'] = str_replace("product.metafields","primehm",$highlight['title']);
  }


}

$arrNameById = array( 'Etc/GMT+12' =>'International Date Line West','Pacific/Pago_Pago'=>'American Samoa','Pacific/Midway'=>'Midway Island','Pacific/Honolulu' =>'Hawaii','America/Juneau'=>'Alaska','America/Los_Angeles'=>'Pacific Time','America/Tijuana' =>'Tijuana','America/Phoenix'=>'Arizona','America/Chihuahua'=>'Chihuahua','America/Mazatlan'=>'Mazatlan','America/Denver'=>'Mountain Time','America/Guatemala'=>'Central America','America/Chicago'=>'Central Time','America/Mexico_City'=>'Mexico City','America/Monterrey'=>'Monterrey','America/Regina'=>'Saskatchewan','America/Bogota'=>'Bogota','America/New_York'=>'Eastern Time','America/Indiana/Indianapolis'=>'Indiana','America/Lima'=>'Quito','America/Halifax'=>'Atlantic Time','America/Caracas'=>'Caracas','America/Guyana'=>'Georgetown','America/La_Paz'=>'La Paz','America/Puerto_Rico'=>'Puerto Rico','America/Santiago'=>'Santiago','America/St_Johns'=>'Newfoundland','America/Sao_Paulo'=>'Brasilia','America/Argentina/Buenos_Aires'=>'Buenos Aires','America/Godthab'=>'Greenland','America/Montevideo'=>'Montevideo','Atlantic/South_Georgia'=>'Mid-Atlantic','Atlantic/Azores'=>'Azores','Atlantic/Cape_Verde'=>'Cape Verde Is.','Africa/Casablanca'=>'Casablanca','Europe/Dublin'=>'Dublin','Europe/London'=>'London','Europe/Lisbon'=>'Lisbon','Africa/Monrovia'=>'Monrovia','Etc/UTC'=>'UTC','Europe/Amsterdam'=>'Amsterdam','Europe/Belgrade'=>'Belgrade','Europe/Berlin'=>'Berlin','Europe/Zurich'=>'Zurich','Europe/Bratislava'=>'Bratislava','Europe/Brussels'=>'Brussels','Europe/Budapest'=>'Budapest','Europe/Copenhagen'=>'Copenhagen','Europe/Ljubljana'=>'Ljubljana','Europe/Madrid'=>'Madrid','Europe/Paris'=>'Paris','Europe/Prague'=>'Prague','Europe/Rome'=>'Rome','Europe/Sarajevo'=>'Sarajevo','Europe/Skopje'=>'Skopje','Europe/Stockholm'=>'Stockholm','Europe/Vienna'=>'Vienna','Europe/Warsaw'=>'Warsaw','Africa/Algiers'=>'West Central Africa','Europe/Zagreb'=>'Zagreb','Europe/Athens'=>'Athens','Europe/Bucharest'=>'Bucharest','Africa/Cairo'=>'Cairo','Africa/Harare'=>'Harare','Europe/Helsinki'=>'Helsinki','Asia/Jerusalem'=>'Jerusalem','Europe/Kaliningrad'=>'Kaliningrad','Europe/Kiev'=>'Kyiv','Africa/Johannesburg'=>'Pretoria','Europe/Riga'=>'Riga','Europe/Sofia'=>'Sofia','Europe/Tallinn'=>'Tallinn','Europe/Vilnius'=>'Vilnius','Asia/Baghdad'=>'Baghdad','Europe/Istanbul'=>'Istanbul','Asia/Kuwait'=>'Kuwait','Europe/Minsk'=>'Minsk','Europe/Moscow'=>'Moscow','Africa/Nairobi'=>'Nairobi','Asia/Riyadh'=>'Riyadh','Europe/Volgograd'=>'Volgograd','Asia/Tehran'=>'Tehran','Asia/Muscat'=>'Muscat','Asia/Baku'=>'Baku','Europe/Samara'=>'Samara','Asia/Tbilisi'=>'Tbilisi','Asia/Yerevan'=>'Yerevan','Asia/Kabul'=>'Kabul','Asia/Yekaterinburg'=>'Ekaterinburg','Asia/Karachi'=>'Karachi','Asia/Tashkent'=>'Tashkent','Asia/Kolkata'=>'New Delhi','Asia/Calcutta'=>'New Delhi','Asia/Colombo'=>'Sri Jayawardenepura','Asia/Kathmandu'=>'Kathmandu','Asia/Almaty'=>'Almaty','Asia/Dhaka'=>'Dhaka','Asia/Urumqi'=>'Urumqi','Asia/Rangoon'=>'Rangoon','Asia/Bangkok'=>'Hanoi','Asia/Jakarta'=>'Jakarta','Asia/Krasnoyarsk'=>'Krasnoyarsk','Asia/Novosibirsk'=>'Novosibirsk','Asia/Shanghai'=>'Beijing','Asia/Chongqing'=>'Chongqing','Asia/Hong_Kong'=>'Hong Kong','Asia/Irkutsk'=>'Irkutsk','Asia/Kuala_Lumpur'=>'Kuala Lumpur','Australia/Perth'=>'Perth','Asia/Singapore'=>'Singapore','Asia/Taipei'=>'Taipei','Asia/Ulaanbaatar'=>'Ulaanbaatar','Asia/Tokyo'=>'Tokyo','Asia/Seoul'=>'Seoul','Asia/Yakutsk'=>'Yakutsk','Australia/Adelaide'=>'Adelaide','Australia/Darwin'=>'Darwin','Australia/Brisbane'=>'Brisbane','Australia/Melbourne'=>'Melbourne','Pacific/Guam'=>'Guam','Australia/Hobart'=>'Hobart','Pacific/Port_Moresby'=>'Port Moresby','Australia/Sydney'=>'Sydney','Asia/Vladivostok'=>'Vladivostok','Asia/Magadan'=>'Magadan','Pacific/Noumea'=>'New Caledonia','Pacific/Guadalcanal'=>'Solomon Is.','Australia/Srednekolymsk'=>'Srednekolymsk','Pacific/Auckland'=>'Wellington','Pacific/Fiji'=>'Fiji','Asia/Kamchatka'=>'Kamchatka','Pacific/Majuro'=>'Marshall Is.','Pacific/Chatham'=>'Chatham Is.','Pacific/Tongatapu'=>'Nuku alofa','Pacific/Apia'=>'Samoa','Pacific/Fakaofo'=>'Tokelau Is.');

if(isset($arrNameById[Auth::User()->timezone])){
  $mytimezone=Auth::User()->timezone;
}else{
  $mytimezone='Etc/UTC';
}

?>

<div class="row">
  <div class="col-12 col-lg-8 offset-lg-2 mb-3">
    <form id="formRule" name="formRule" enctype="multipart/form-data">
      {{csrf_field()}}
      @if( !empty( $highlight ) )
        <input type="hidden" name="highlightid" value="{{$highlight['producthighlightid']}}">
      @endif
      <!-- <div class="mb-2 border bg-white" id="hightlightdiv" style="position: fixed;top: 72px;right: 80px;z-index: 1;width: 25%;height: 7%;">
          <div class="d-inline-block">
      
        <div class="m-2 tooltips" id="highlightStyle" style="display:block;width: 100%;" data-toggle="tooltip" title="">
          <span class="spanStyleleft" ></span>
          <div id="textStyle" class="highlighttitle"  style="">{!! isset( $highlight['image'] ) ? $highlight['image'] :'<img class="img-fluid" src="https://img.icons8.com/000000/ios-filled/2x/blushing.png" style="max-height:25px;">' !!} {!! isset( $highlight['title'] ) ? $highlight['title'] :'Highlight Text' !!}</div>
          <span id="spanStyle"></span>
        </div>
      
      </div></div> -->
      <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              1. Content
            </button>
          </div>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">

              <div class="form-group row highlight_text_show">
                <label for="staticEmail" class="col-lg-4 col-sm-4 col-md-4 col-4 col-form-label mt-2">Icon</label>
                
                  <div  class="col-lg-2 col-sm-3 col-md-3 col-8 mt-2">
                      
                      <a href="javascript:void(0)" onclick="openProductModel('#iconModal');" class="btn btn-secondary" >Pick</a> 
                      <span class="ml-1">OR</span>
                    

                  </div>
                  <div  class=" col-lg-4 col-9 col-sm-3 col-md-3  mt-2 m-0 ">
                      
                      <div class="input-group">
                
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" value =""  id="image" name="image"  accept="image/*" aria-describedby="Upload">
                          <label class="custom-file-label" style="overflow: hidden;" for="image">Choose file</label>
                        </div>
                        
                      </div>
                       <div class="small text-muted">(Max size: 500KB | Type: jpg, png, gif, svg)</div>
                      &nbsp;
                     

                  </div>
                  <div id="icondiv"  class="col-lg-2 col-3 col-sm-2 mt-2"> 

                      @if( isset($highlight['producthighlightid']) && !isset($highlight['image']) )
                        <img src="{{ env('AWS_PATH').'h'.$highlight['producthighlightid'] }}" onerror="errorimgmsg(this);" style="height: 40px;" /> 
                  
                      @else 
                        <img src="{{ isset( $highlight['image'] ) ? $highlight['image'] :'https://img.icons8.com/color/48/000000/free-shipping.png'}}" onerror="errorimgmsg(this);" style="max-height:38px;" class="border rounded p-1">

                      @endif
             
                  </div>
                
              </div>
              
              <div class="form-group row highlight_text_show">
                <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Text</label>
                <div class="col-12 col-sm-7 col-md-8 col-lg-8">

                {{-- <div  class="form-control" name="display[highlighttitle]" class="highlighttitle">{ isset( $highlight['title'] ) ? $highlight['title'] :' Highlight Text' !!}</div> --}}

                <input type="text" class="form-control highlighttitle" name="display[highlighttitle]" id="editdiv" value="{{ isset( $highlight['title'] ) ? $highlight['title'] :' 24-hour Fast Free Shipping' }}" placeholder="Enter the highlight Text">
                <a class="float-right btn btn-sm  btn-secondary mt-2" href="javascript:void(0);" onclick="openDynamicVariableModel('#openDynamicVariableModel-model');" >
                    <span class="">
                      Add Dynamic Data Fields 
                    </span>
                  </a>

                <a href="javascript:void(0)" class="float-right btn btn-sm btn-secondary mt-2 mr-1" id="emoji">
                  <span class="">&#128526;</span></a>
                
           
                </div>
                
              </div>
              
                <div class="form-group row">
                 <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Link
                         <span class="text-muted small">(optional)</span>
                </label>
                <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                  <div class="input-group">
                    <input type="text" class="form-control" name="" id="highlightlink" value="{{isset( $highlight['highlightlink'] ) && $highlight['highlightlink'] !=''  ? $highlight['highlightlink'] :'http://' }}" placeholder="(Optional) Enter the highlight Link">

                    <select class="form-control" name="linktab" id="linktab" >
                      <option value="0" {{ !empty( $highlight['linktab'] ) && $highlight['linktab'] == 0 ? 'selected' :''}} >Open in same tab</option>
                      <option value="1" {{ !empty( $highlight['linktab'] ) && $highlight['linktab'] == 1 ? 'selected' :''}} >Open in new tab</option>
                      <option value="2" {{ !empty( $highlight['linktab'] ) && $highlight['linktab'] == 2 ? 'selected' :''}} >Open in popup modal</option>
                    </select>

                  </div>

                  <btn  onclick="openLinkTabModel('#Linktab-model');" id="linktabmodal"  type="text" class="float-right btn btn-secondary mt-2 mr-1" >Modal Content </btn>
                </div>
                
              </div>

              <div class="form-group row 
                ">
                  <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Tooltip
                    <span class="text-muted small">(optional. Max 100 chars)</span>
                  </label>
                  
                  <div class="col-12 col-sm-7 col-md-8 col-lg-8">

                    <input type="text" class="form-control" maxlength="100" name="highlight[tooltip]" id="tooltip" placeholder="Enter Text for Tooltip" value="{{isset( $highlight['tooltip'] ) ? $highlight['tooltip'] :'' }}"> 

                  </div>
                </div>


            </div>
          </div>
        </div>
    
      <div class="card">
        <div class="card-header" id="headingThree">
          <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              2. Rules
            </button>
          </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
          <div class="card-body">
            <div class="form-group row mb-1">
            <label for="staticEmail" class="col-12 col-sm-6 col-md-6 col-form-label">Display Conditions</label>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
              <div class="form-check form-check-inline mt-1">
                <input class="form-check-input" type="radio" name="condition" id="condition" value="1" {{isset( $highlight['condition'] ) && $highlight['condition'] == 1 ? 'checked':'checked'  }}>
                <label class="form-check-label" for="inlineRadio1">All Condition</label>
              </div>
              <div class="form-check form-check-inline">
                <input {{isset( $highlight['condition'] ) && $highlight['condition'] == 2 ? 'checked':''  }} class="form-check-input" type="radio" name="condition" id="condition" value="2">
                <label class="form-check-label" for="condition">Any Condition</label>
              </div>
            </div>
          </div>
          <div class="row mx-0">
            <table class="table table-sm table-bordered table-striped">
              <colgroup>
                <col width="80%">
                <col width="5%">
              </colgroup>
              <tbody id="appendHtmlAll">
                @if( !empty( $displayrules ) )
                  @foreach( $displayrules as $index => $displayrule )
                    <tr id="appendHtml{{$index== 0 ? '': $index }}">
                      <td>
                        <div class="row px-0 mx-0">
                          <div class="mx-1"  >
                            <select class="form-control" name="rule[{{$index}}][conditiontype]" onchange="addRule(this,'{{$index}}');">
                              <option {{ isset( $displayrule['all_products'] ) ? 'selected':''  }}  value="all_products">All Products</option>
                              <option {{ isset( $displayrule['product'] ) ? 'selected':''  }} value="product" >Select Products</option>
                              <option {{ isset( $displayrule['product_type'] ) ? 'selected':''  }}  value="product_type">Product Type</option>
                              <option {{ isset( $displayrule['product_price'] ) ? 'selected':''  }}  value="product_price">Product Price</option>
                              <option {{ isset( $displayrule['product_vendor'] ) ? 'selected':''  }}  value="product_vendor">Product Vendor</option>
                              <option {{ isset( $displayrule['title'] ) ? 'selected':''  }}  value="title">Product Title</option>
                              <option {{ isset( $displayrule['weight'] ) ? 'selected':''  }}  value="weight">Product Weight</option>
                              <option {{ isset( $displayrule['collection'] ) ? 'selected':''  }} value="collection">Collection</option> 
                              <!-- <option {{ isset( $displayrule['page_type'] ) ? 'selected':''  }} value="page_type">Page Type</option>                              -->
                              <option {{ isset( $displayrule['saleprice'] ) ? 'selected':''  }} value="saleprice" >Sale price</option>
                              <option {{ isset( $displayrule['inventory'] ) ? 'selected':''  }} value="inventory" >Inventory</option>
                              <option {{ isset( $displayrule['tags'] ) ? 'selected':''  }} value="tags" >Product Tags</option>
                              <option {{ isset( $displayrule['createddate'] ) ? 'selected':''  }}  value="createddate">Product Created Date</option>
                              <option {{ isset( $displayrule['publishdate'] ) ? 'selected':''  }}  value="publishdate">Product Published Date</option>
                              <option {{ isset( $displayrule['productmetafield'] ) ? 'selected':''  }}  value="product_metafield">Product Metafield</option>
                              <option {{ isset( $displayrule['product_option'] ) ? 'selected':''  }}  value="product_option">Variants</option>
                              <option {{ isset( $displayrule['customer'] ) ? 'selected':''  }}  value="customer">Customer</option>
                              <option {{ isset( $displayrule['customer_tag'] ) ? 'selected':''  }}  value="customer_tag">Customer Tags</option>
                            </select>
                          </div>
                          
                          @if( isset( $displayrule['createddate'] ) )
                            <div class="mx-1 mt-2"> is within the past </div>
                            <div class="mx-0 px-1 inventorytype" >
                              <input type="number" min="0" max="10000" name="rule[{{$index}}][conditiontype][createddate]" value="{{$displayrule['createddate']}}" class="form-control">
                            </div>
                            <span class="mt-2 mx-1"> days </span>

                          @elseif( isset( $displayrule['publishdate'] ) )
                            <div class="mx-1 mt-2"> is within the past </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <input type="number" min="0" max="10000" name="rule[{{$index}}][conditiontype][publishdate]" value="{{$displayrule['publishdate']}}" class="form-control">
                              </div>
                              <span class="mt-2 mx-1"> days </span>

                          @elseif( isset( $displayrule['all_products'] ) )
                            <div class="mx-1 mt-2"> Highlight will be displayed on all products </div>
                            <div class="mx-0 px-1 col inventorytype" >
                              <input type="hidden" name="rule[{{$index}}][conditiontype][all_products]" value="{{$displayrule['all_products']}}" class="form-control">
                            </div>
                            <span class="mt-2 mx-1">  </span> 
                          @elseif( isset( $displayrule['title'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 title" >
                                <select name="rule[{{$index}}][conditiontype][title][type]" class="form-control"  >
                                  <option {{ $displayrule['title']['type'] == '1' ? 'selected':'' }} value="1" >Starts With</option>
                                  <option {{ $displayrule['title']['type'] == '2' ? 'selected':'' }} value="2" >Ends With</option>
                                  <option {{ $displayrule['title']['type'] == '3' ? 'selected':'' }} value="3" >Contains</option>
                                  <option {{ $displayrule['title']['type'] == '4' ? 'selected':'' }} value="4" >Not Contains</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 titlediv col" >
                                <input type="text" name="rule[{{$index}}][conditiontype][title][value]" value="{{ $displayrule['title']['value'] }}" class="form-control" >
                              </div> 
                          @elseif( isset( $displayrule['product_type'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_type][type]" class="form-control"  >
                                  <option {{ $displayrule['product_type']['type'] == '1' ? 'selected':'' }}  value="1">Equal To</option>
                                  <option {{ $displayrule['product_type']['type'] == '2' ? 'selected':'' }} value="2" >Not Equal To</option>
                                  <option {{ $displayrule['product_type']['type'] == '3' ? 'selected':'' }} value="3" >Starts With</option>
                                  <option {{ $displayrule['product_type']['type'] == '4' ? 'selected':'' }} value="4" >Ends With</option>
                                  <option {{ $displayrule['product_type']['type'] == '5' ? 'selected':'' }} value="5" >Contains</option>
                                  <option {{ $displayrule['product_type']['type'] == '6' ? 'selected':'' }} value="6" >Not Contains</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                                <input type="text" name="rule[{{$index}}][conditiontype][product_type][value]" value="{{ $displayrule['product_type']['value'] }}" class="form-control" >
                              </div>

                              @elseif( isset( $displayrule['product_price'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_price][type]" class="form-control"  >
                                  <option {{ $displayrule['product_price']['type'] == '1' ? 'selected':'' }}  value="1">Equal To</option>
                                  <option {{ $displayrule['product_price']['type'] == '2' ? 'selected':'' }} value="2" >Not Equal To</option>
                                  <option {{ $displayrule['product_price']['type'] == '3' ? 'selected':'' }} value="3" >Is Greater Than</option>
                                  <option {{ $displayrule['product_price']['type'] == '4' ? 'selected':'' }} value="4" >Is Less Than</option>
                                 
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                                <input type="number" min="1" name="rule[{{$index}}][conditiontype][product_price][value]" value="{{ $displayrule['product_price']['value'] }}" class="form-control" >
                              </div>
                            
                            @elseif( isset( $displayrule['product_vendor'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_vendor][type]" class="form-control"  >
                                  <option {{ $displayrule['product_vendor']['type'] == '1' ? 'selected':'' }}  value="1">Equal To</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '2' ? 'selected':'' }} value="2" >Not Equal To</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '3' ? 'selected':'' }} value="3" >Starts With</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '4' ? 'selected':'' }} value="4" >Ends With</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '5' ? 'selected':'' }} value="5" >Contains</option>
                                  <option {{ $displayrule['product_vendor']['type'] == '6' ? 'selected':'' }} value="6" >Not Contains</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                                <input type="text" name="rule[{{$index}}][conditiontype][product_vendor][value]" value="{{ $displayrule['product_vendor']['value'] }}" class="form-control" >
                              </div>
                              @elseif( isset( $displayrule['product_option'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][product_option][type]" class="form-control"  >
                                <option {{ $displayrule['product_option']['type'] == '1' ? 'selected':'' }}  value="1">Product Has Variants</option>
                                  <option {{ $displayrule['product_option']['type'] == '2' ? 'selected':'' }} value="2" >Product Does Not Have Variants</option>
                                  
                                </select>
                              </div>
                              @elseif( isset( $displayrule['customer'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][customer][type]" class="form-control"  >
                                  <option {{ $displayrule['customer']['type'] == '1' ? 'selected':'' }}  value="1">Is Logged In</option>
                                  <option {{ $displayrule['customer']['type'] == '2' ? 'selected':'' }} value="2" >Not Logged In</option>
                                  
                                </select>
                              </div>
                              @elseif( isset( $displayrule['customer_tag'] ) )
                                @if( isset( $displayrule['customer_tag']['type'] ) )
                                  <div class="mx-1 mt-2"> </div>
                                  <div class="mx-0 px-1 inventorytype" >
                                    <select name="rule[{{$index}}][conditiontype][customer_tag][type]" class="form-control"  >
                                      <option {{ $displayrule['customer_tag']['type'] == '1' ? 'selected':'' }} value="1">Equals To</option>
                                      <option {{ $displayrule['customer_tag']['type'] == '2' ? 'selected':'' }} value="2" >Not Equals To</option>
                                    </select>
                                  </div>
                                  <div class="mx-0 col">
                                    <input data-role="customer_taginput" name="rule[{{$index}}][conditiontype][customer_tag][value]" size="1" type="text" class=" form-control customer_tag tags" id="customer_tag" placeholder="Enter customer tags" value="{{$displayrule['customer_tag']['value']}}">
                                  </div>
                                @else
                                  <div class="mx-1 mt-2"> </div>
                                  <div class="mx-0 px-1 inventorytype" >
                                    <select name="rule[{{$index}}][conditiontype][customer_tag][type]" class="form-control"  >
                                      <option selected value="1">Equals To</option>
                                      <option value="2" >Not Equals To</option>
                                    </select>
                                  </div>
                                  <div class="mx-0 col">
                                    <input data-role="customer_taginput" name="rule[{{$index}}][conditiontype][customer_tag][value]" size="1" type="text" class=" form-control customer_tag tags" id="customer_tag" placeholder="Enter customer tags" value="{{$displayrule['customer_tag']}}">
                                  </div>
                                @endif
                            @elseif( isset( $displayrule['weight'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][weight][type]" class="form-control"  >
                                  <option {{ $displayrule['weight']['type'] == '1' ? 'selected':'' }} value="1">Equal To</option>
                                  <option {{ $displayrule['weight']['type'] == '2' ? 'selected':'' }} value="2" >Less Than</option>
                                  <option {{ $displayrule['weight']['type'] == '3' ? 'selected':'' }} value="3" >Greater Than</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1">  </span>
                              <div class="mx-1 inventorytype {{$inventoryHide}}" >
                                <input type="number" min="0" name="rule[{{$index}}][conditiontype][weight][value]" value="{{ $displayrule['weight']['value'] }}" class="form-control" >
                              </div>
                              <span class="mt-2 mx-1"> grams </span>
                          @elseif( isset( $displayrule['collection'] ) )
                            @if( isset( $displayrule['collection']['type'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][collection][type]" class="form-control"  >
                                  <option {{ $displayrule['collection']['type'] == '1' ? 'selected':'' }} value="1">Equals</option>
                                  <option {{ $displayrule['collection']['type'] == '2' ? 'selected':'' }} value="2" >Excludes</option>
                                </select>
                              </div>

                              <div class="mx-0 col">
                                <btn  onclick="openCollectionModel('#Collection-model',{{$index}});"  type="text" class="btn btn-secondary" >Select collections</btn>
                                <input type="hidden"  name="rule[{{$index}}][conditiontype][collection][value]" type="text" class="form-control" id="addCollection_{{$index}}" placeholder="Selecte Collections " value="{{$displayrule['collection']['value']}}">
                              </div>
                              
                              <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                                <i><span id="collectionscount_{{$index}}">
                                @if( !empty( array_filter($displayrule['collection'] ) ) )
                                  {{count( explode(",",$displayrule['collection']['value'] )  ) }}
                                @else
                                  0
                                @endif
                                </span> selected</i>
                              </span>
                            @else
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][collection][type]" class="form-control"  >
                                  <option selected value="1">Equals To</option>
                                  <option value="2" >Excludes</option>
                                </select>
                              </div>

                              <div class="mx-0 col">
                                <btn  onclick="openCollectionModel('#Collection-model',{{$index}});"  type="text" class="btn btn-secondary" >Select collections</btn>
                                <input type="hidden"  name="rule[{{$index}}][conditiontype][collection][value]" type="text" class="form-control" id="addCollection_{{$index}}" placeholder="Selecte Collections " value="{{$displayrule['collection'][0]}}">
                              </div>
                              <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                                <i><span id="collectionscount_{{$index}}">
                                @if( !empty( $displayrule['collection'][0] ) )
                                  {{count( explode(",",$displayrule['collection'][0] )  ) }}
                                @else
                                  0
                                @endif
                                </span> selected</i>
                              </span>
                            @endif

                          @elseif( isset( $displayrule['page_type'] ) )
                          @php
                            $counter = 0;
                          @endphp
                          <span class="mt-2 mx-1">equals</span>
                          <div class="mx-1">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                -- Select --
                                <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="page_list" style="max-height:300px;overflow:auto;">
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'product' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="product" > Product Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                    <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput" 
                                    @php
                                      if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                        foreach( $displayrule['page_type'] as $key => $value ) {
                                          if($value == 'index' ){ $counter++; echo 'checked'; }
                                        }
                                      }
                                    @endphp
                                      value="index" > Index Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'collection' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="collection" > Collection Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'article' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="article" > Article Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'blog' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="blog" > Blog Page
                                      </span>
                                  </li>
                                  <li class="ml-2"> <span>
                                      <input name="rule[{{$index}}][conditiontype][page_type][]" onchange="countPageCheck({{$index}});" type="checkbox" class="collection_listinput"
                                      @php
                                        if(is_array( $displayrule['page_type'] ) && !empty($displayrule['page_type']) ){
                                          foreach( $displayrule['page_type'] as $key => $value ) {
                                            if($value == 'cart' ){ $counter++; echo 'checked'; }
                                          }
                                        }
                                      @endphp
                                      value="cart" > Cart Page
                                      </span>
                                  </li>
                                </ul>
                                <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                                <i>
                                <span id="pagescount">
                                    {{$counter}}
                                </span> selected</i>
                                </span>
                            </div>
                          </div>
                        
                          @elseif( isset( $displayrule['product'] ) )
                            <span class="mt-2 mx-1">equals</span>
                            <div class="mx-1">
                             
                              <btn  onclick="openProductModel('#blacklistProduct-model',{{$index}});"  type="text" class="btn btn-secondary" >Select products</btn>
                              <input type="hidden"  name="rule[{{$index}}][conditiontype][product][]" type="text" class="form-control" id="addProduct_{{$index}}" placeholder="Selected products " value="{{implode(",", $displayrule['product'])}}">
                              <input type="hidden"  name="shopproductmeta" type="text" class="form-control" id="shopproductmeta" placeholder="Selected products " value="{{implode(",", $displayrule['product'])}}">
                            </div>
                            <a href="javascript:void(0);" class="mt-2" onclick="openProductModel('#blacklistProduct-model','{{$index}}',2);" >
                              <span class="text-primary pointer small" >
                                <i>
                                  <span id="productcount_{{$index}}">
                                    @if( !empty( array_filter($displayrule['product'] ) ) )
                                      {{count( explode(",",implode(",", $displayrule['product'] ) )  ) }}
                                    @else
                                      0
                                    @endif
                                  </span>selected
                                </i>
                              </span>
                            </a>
                          @elseif( isset( $displayrule['saleprice'] ) )
                          <?php 
                            if( strpos($displayrule['saleprice'], ',') !== false ) {
                              $newsalerule = explode(",",$displayrule['saleprice']);
                              $sale_start = $newsalerule[0];
                              $sale_end = $newsalerule[1];
                            }else{
                              $sale_start = $displayrule['saleprice'];
                              $sale_end = '100';
                            }

                          ?>

                          <span class="mt-2 mx-1"> is between </span>
                            <div class="col mx-0 px-0 " >

                              <input type="number" min="1" max="100" name="rule[{{$index}}][conditiontype][saleprice][start]" value="{{isset( $sale_start ) && $sale_start !=''  ? $sale_start :'' }}" class="form-control" required>
                            </div>
                            <span class="mt-2 mx-1"> % and </span>
                            <div class="col mx-0 px-0 " >
                              

                              <input type="number" min="1" max="100" name="rule[{{$index}}][conditiontype][saleprice][end]" value="{{isset( $sale_end ) && $sale_end !=''  ? $sale_end :'' }}" class="form-control" required>
                            </div>
                            <span class="mt-2 mx-1"> %</span>
                            <input type="hidden" name="rule[{{$index}}][conditiontype][saleprice]" value="" class="form-control" >
                          @elseif( isset( $displayrule['inventory'] ) )
                          <span class="mt-2 mx-1">is</span>
                            <div class="mx-1" >
                              <select name="rule[{{$index}}][conditiontype][inventory][type]" class="form-control selectedinventory" onchange="inventoryType(this,{{$index}});">
                                <option {{ $displayrule['inventory']['type'] == 1 ? 'selected':'' }} value="1">between</option>
                                <option {{ $displayrule['inventory']['type'] == 2 ? 'selected':'' }} value="2" >in stock</option>
                                <option {{ $displayrule['inventory']['type'] == 3 ? 'selected':'' }} value="3" >out of stock</option>
                                <option {{ $displayrule['inventory']['type'] == 4 ? 'selected':'' }} value="4" >out of stock but continue selling</option>
                              </select>
                            </div>

                            <div class="col mx-0 px-0 inventorytype inventoryhide" >
                              <input type="number" min="1" max="100" name="rule[{{$index}}][conditiontype][inventory][start]" value="{{ isset( $displayrule['inventory']['start'] ) && $displayrule['inventory']['start'] !='' ?  $displayrule['inventory']['start'] : 1 }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )">
                            </div>
                            <span class="inventorytype {{$inventoryHide}} mx-1 mt-2 inventoryhide">and </span>
                            
                            <div class="col mx-0 px-0 inventorytype inventoryhide" >
                              <input type="number" min="1"  name="rule[{{$index}}][conditiontype][inventory][value]" value="{{ $displayrule['inventory']['value'] }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                             
                            </div>
                          @elseif( isset( $displayrule['tags'] ) )
                            @if( isset( $displayrule['tags']['type'] ) )
                              <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][tags][type]" class="form-control"  >
                                  <option {{ $displayrule['tags']['type'] == '1' ? 'selected':'' }} value="1">Equals To</option>
                                  <option {{ $displayrule['tags']['type'] == '2' ? 'selected':'' }} value="2" >Excludes To</option>
                                </select>
                              </div>
                              <div class="mx-0 col">
                                <input data-role="tagsinput" name="rule[{{$index}}][conditiontype][tags][value]" size="1" type="text" class=" form-control tags tags" id="tags" placeholder="Enter product tags" value="{{$displayrule['tags']['value']}}">
                              </div>
                            @else
                            <div class="mx-1 mt-2"> </div>
                              <div class="mx-0 px-1 inventorytype" >
                                <select name="rule[{{$index}}][conditiontype][tags][type]" class="form-control"  >
                                  <option selected value="1">Equals To</option>
                                  <option value="2" >Excludes To</option>
                                </select>
                              </div>
                              <div class="mx-0 col">
                                <input data-role="tagsinput" name="rule[{{$index}}][conditiontype][tags][value]" size="1" type="text" class=" form-control tags tags" id="tags" placeholder="Enter product tags" value="{{$displayrule['tags']}}">
                              </div>
                            @endif
                          
                          @elseif( isset( $displayrule['productmetafield'] ) )
                          <span class="mt-2 mx-1"><a href="#" data-toggle="modal" data-target="#metafieldModal">name</a></span>
                            <div class="col mx-0 px-0 productmetafield metanameval" >
                              <input type="text" min="1" max="100" name="rule[{{$index}}][conditiontype][productmetafield][metaname]" value="{{ $displayrule['productmetafield']['metaname'] }}" class="form-control" required>
                            </div>
                            <div class="mx-0 px-0 productmetafield " >
                              <select name="rule[{{$index}}][conditiontype][productmetafield][metacondition]" id="metafieldcond" class="form-control metafieldcond"  >
                                <option {{ $displayrule['productmetafield']['metacondition'] == 1 ? 'selected':'' }} value="1">Equals</option>
                                <option {{ $displayrule['productmetafield']['metacondition'] == 2 ? 'selected':'' }} value="2" >Found</option>
                                <option {{ $displayrule['productmetafield']['metacondition'] == 3 ? 'selected':'' }} value="3" >Not Found</option>
                              </select>
                            </div>
                            <div class="col mx-0 px-0 productmetafield metafieldkey" id="metafieldkey" >
                            <input type="text" min="1" max="100" name="rule[{{$index}}][conditiontype][productmetafield][metaval]" value="{{ $displayrule['productmetafield']['metaval'] }}" class="form-control" required>
                            </div>
                          @endif
                        </div>
                      </td>
                      <td>
                        <div class="text-center mt-2">
                          <a disabled onclick="deleteRule('#appendHtml{{ $index== 0 ? '': $index }}');" href="javascript:void(0)"><i class="fa fa-times text-danger " aria-hidden="true"></i></a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                @else
                  <tr id="appendHtml">
                    <td>
                      <div class="row mx-0 px-0">
                        <div class="mx-1"  >
                          <select class="form-control" name="rule[0][conditiontype]" onchange="addRule(this,0);">
                            <option  selected value="all_products">All Products</option>
                            <option value="product" >Select Products</option>
                            <option value="product_type" >Product Type</option>
                            <option value="product_price" >Product Price</option>
                            <option value="product_vendor" >Product Vendor</option>
                            <option value="title" >Product Title</option>
                            <option value="weight" >Product Weight</option>
                            <option value="collection">Collection</option>  
                            <!-- <option value="page_type">Page Type</option>    -->
                            <option value="saleprice" >Sale price</option>
                            <option value="inventory" >Inventory</option>
                            <option value="tags" >Product Tags</option>
                            <option  value="createddate">Product Created Date</option>
                            <option  value="publishdate">Product Published Date</option>
                            <option  value="product_metafield">Product Metafield</option>
                            <option  value="product_option">Variants</option>
                            <option  value="customer">Customer</option>
                            <option  value="customer_tag">Customer Tags</option>
                          </select>
                        </div>
                        <span class="mt-2 mx-1"> Highlight will be displayed on all products </span>
                        <div class="mx-0 px-1 col inventorytype" >
                          <input type="hidden" name="rule[0][conditiontype][all_products]" value="all_products" class="form-control">
                        </div>
                        <span class="mt-2 mx-1"> </span>

                      </div>
                    </td>
                    <td>
                      <div class="text-center mt-2">
                        <a disabled onclick="deleteRule('#appendHtml');" href="javascript:void(0)">
                          <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
         
          <div class="float-left ExProduct">
            
           
                              
              <btn  onclick="openExProductModel('#ExProduct-model');"  type="text" class="btn btn-outline-secondary" >Exclude products</btn>
              <input type="hidden"  name="excludeproduct" type="text" class="form-control" id="addExProduct" placeholder="Selected products " value="{{ ( isset( $highlight['excludeproduct'] ) && ( $highlight['excludeproduct'] != '' )) ? implode(",", $highlight['excludeproduct']) :''}}">
          
            <a href="javascript:void(0);" class="mt-2 ml-2" onclick="openExProductModel('#ExProduct-model','',2);" >
              <span class="text-primary pointer small" >
                <i>
                  <span id="Exproductcount">
                  @if( ( isset( $highlight['excludeproduct'] ) && ( $highlight['excludeproduct'] != '' )) )
                    @if( !empty( array_filter($highlight['excludeproduct'] ) ) )
                      {{count( explode(",",implode(",", $highlight['excludeproduct'] ) )  ) }}
                    @else
                      0
                    @endif
                  @else
                    0
                  @endif
                  </span> excluded
                </i>
              </span>
            </a>
            <!-- <button type="button" class="btn btn-secondary" onclick="addRuleHtml();">Add Rule</button> -->
          </div>

          <div class="mt-0 text-right">
            <button type="button" class="btn btn-secondary" onclick="addRuleHtml();">Add Rule</button>
          </div>

          <div class="row">
            <div class="col-12">
              <hr>
              <p>Pro Conditions 
              @if(Auth::User()->planid < 3)
              <span class="fa fa-lock text-muted ml-2"></span>
              @endif
              </p>
              <div class="location">
                <btn  onclick="openCountryModel('#Country-model');"  type="text" class="btn btn-outline-secondary" >Country Restriction <span id="country_count" class="badge badge-primary"></span></btn>
                <input type="hidden"  name="customer_location" type="text" class="form-control" id="addCountry_counter" placeholder="Selected products " value="{{ ( isset( $highlight['customer_location'] ) && ( $highlight['customer_location'] != '' )) ? implode(",", $highlight['customer_location']) :''}}">

                <btn  onclick="openShopLanguageModel('#ShopLanguage-model');"  type="text" class="btn btn-outline-secondary" >Language <span id="language_count" class="badge badge-primary"></span></btn>
                <input type="hidden"  name="shop_language" type="text" class="form-control" id="addShopLanguage_counter" placeholder="Select Languages " value="{{ ( isset( $highlight['shop_language'] ) && ( $highlight['shop_language'] != '' )) ? implode(",", $highlight['shop_language']) :''}}" >

                <btn  onclick="openOrderCountModel('#OrderCount-model');"  type="text" class="btn btn-outline-secondary" >Order Count</btn>
                
              </div>
              </div>
            </div>
          </div>

        </div>
      </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          3. Activate
        </button>
      </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Highlight Visibility</label>
          <div class="col-12 col-sm-7 col-md-8 col-lg-8">
            <div class="mt-2">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" name="enabledate" id="startingnow"  class="custom-control-input"  onclick="check_enable_date()" value="0" checked  {{ ( isset( $highlight['enabledate'] ) && ( $highlight['enabledate'] == 0 )) ? 'checked' :'' }}>
                  <label class="custom-control-label" for="startingnow">Visible</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" name="enabledate" id="enabledate"  class="custom-control-input" onclick="check_enable_date()" value="1" {{ ( isset( $highlight['enabledate'] ) && ( $highlight['enabledate'] == 1 )) ? 'checked' :'unchecked' }} >
                  
                  <label class="custom-control-label" for="enabledate">Set Visibility Date</label>
                </div>
            </div>
          </div>
        </div>
     
            <div class="form-group row date">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Start</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                <div class="flatpickr form-group input-group date" id='datetimepicker1'>
                <input type="text" class="form-control"  id="input_datetimepicker1"  name="starttime" placeholder="Select Start Date" 
                  <?php date_default_timezone_set($mytimezone); ?>
                value="{{isset( $highlight['starttime'] ) ? date('d-m-Y H:i A', $highlight['starttime']) :'' }}" data-input>
              </div>
              </div>
            </div>
            <div class="form-group row date">
              <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">End</label>
              <div class="col-12 col-sm-7 col-md-8 col-lg-8">
                   <div class="flatpickr form-group input-group date" id='datetimepicker2'>
                <input type="text" class="form-control" id="input_datetimepicker2"   name="endtime" value="{{isset( $highlight['endtime'] ) ? date('d-m-Y H:i A', $highlight['endtime']) :'' }}" placeholder="Select End Date" data-input>
              </div>
              </div>
            </div>
        
            <div class="form-group row">
        
              <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Highlight Group<div><a href="https://getinstashop.freshdesk.com/support/solutions/articles/81000389401-highlight-groups" target="_blank" class="small" style="">More info<span class="fa fa-caret-right ml-1"></span></a></div></label>
              <div class="col-12 col-sm-5 col-md-4 col-form-label ">
                <div class="mt-0">
                  <div class="input-group">
                    @if( !empty( $highlight )  )
                      @if( Auth::User()->planid > 2 )
                        <select name="highlightgroup" id="" class="form-control metafieldcond" >
                          <option {{ $highlight['highlightgroup'] == 1 ? 'selected':'' }} value="1">Group 1</option>
                          <option {{ $highlight['highlightgroup'] == 2 ? 'selected':'' }} value="2" >Group 2</option>
                          <option {{ $highlight['highlightgroup'] == 3 ? 'selected':'' }} value="3" >Group 3</option>
                        </select>
                      @else
                        <select name="highlightgroup" id="" class="form-control metafieldcond" disabled >
                          <option {{ $highlight['highlightgroup'] == 1 ? 'selected':'' }} value="1">Group 1</option>
                        </select>
                      @endif
                    @else
                      @if( Auth::User()->planid > 2 )
                        <select name="highlightgroup" id="" class="form-control metafieldcond"  >
                          <option selected value="1">Group 1</option>
                          <option value="2" >Group 2</option>
                          <option value="3" >Group 3</option>
                        </select>
                      @else
                        <select name="highlightgroup" id="" class="form-control metafieldcond" disabled >
                          <option selected value="1">Group 1</option>
                          <option value="2" >Group 2</option>
                          <option value="3" >Group 3</option>
                        </select>
                      @endif
                    @endif
                  </div>
                    
                </div>
              </div>
            </div>

        <div class="form-group row">
          <label for="Activate" class="col-12 col-sm-5 col-md-4 col-form-label">Activate</label>
          <div class="col-12 col-sm-7 col-md-8 col-lg-8">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              @if( !empty( $highlight )  )
                <label class="btn btn-outline-primary {{  $highlight['isactive'] == 1 ? 'active':'' }} ">
                  <input  {{  $highlight['isactive'] == 1 ? 'checked':'' }}  type="radio" name="active" value="1" autocomplete="off" > ON
                </label>
                <label class="btn btn-outline-primary {{ $highlight['isactive'] == 0 ? 'active':'' }}">
                  <input {{ $highlight['isactive'] == 0 ? 'checked':'' }} type="radio" name="active" value="0" autocomplete="off" > OFF
                </label>
              @else
                <label class="btn btn-outline-primary active ">
                  <input checked type="radio" name="active" value="1" autocomplete="off" > ON
                </label>
                <label class="btn btn-outline-primary ">
                  <input  type="radio" name="active" value="0" autocomplete="off" > OFF
                </label>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body text-right">
      <button type="button" onclick="location.href='{{asset('highlights')}}'" class="btn btn-default">Cancel</button>
      <button type="button" onclick="saveRule('#formRule');" class="btn btn-primary savebtn">Save</button>
    </div>
  </div>
</div>


<div class="modal fade " id="OrderCount-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Order Count Condition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if(Auth::User()->planid > 2)
        <div class="modal-body">
          <div class="container-fluid">
            
            <div class="">
              <div class="card-group">
                  <p class="text-muted">Show highlights when</p>
                <!-- <div class="card"> -->
                  <!-- <div class="card-header">
                      List of countries (<span id=addedcount>0</span>)
                      <button   onclick="addCountryAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div>                -->
                  <!-- <div class="card-body p-1">
                    <div class="row px-0 mx-0"> -->
                      @if( !empty( $displayprorules ) && $displayprorules != '' )
                        @foreach( $displayprorules as $index => $displayProrule )
                          
                          @if( isset( $displayProrule['order_count'] ) )
                            <div class="row mx-0 px-0">
                              <div class="mx-1 mt-1"  >
                                <p class="my-1">Total Order Count&nbsp;</p>
                              </div>
                              <div class="mx-1 mt-2 mt-1"> </div>
                              <div class="mx-0 px-1 Proinventorytype mt-1" >
                                <select name="prorule[0][conditiontype][order_count][type]" class="form-control"  >
                                  <option {{ $displayProrule['order_count']['type'] == '1' ? 'selected':'' }}  selected value="1" >Is Greater Than</option>
                                  <option {{ $displayProrule['order_count']['type'] == '2' ? 'selected':'' }} value="2" >Is Less Than</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1 mt-1">  </span>
                              <div class="mx-1 Proinventorytype col mt-1" >
                                <input type="number" min="0" id="saveordercount" name="saveordercount" value="{{ $displayProrule['order_count']['value'] }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                                <input type="hidden" min="0" name="prorule[0][conditiontype][order_count][value]" value="{{ $displayProrule['order_count']['value'] }}" class="form-control" >
                              </div>
                            </div>
                          @endif
                          @if( isset( $displayProrule['recentorder_count'] ) )
                              <div class="row mx-0 px-0">
                                <div class="mx-1 mt-1"  >
                                  <!-- <p class="my-1">Recent Sold </p> -->
                                  <p class="my-1">Recent Sold Count</p>
                                </div>
                                <div class="mx-1 mt-2 mt-1"> </div>
                                <div class="mx-0 px-1 Proinventorytype mt-1" >
                                  <select name="prorule[1][conditiontype][recentorder_count][type]" class="form-control"  >
                                    <option {{ $displayProrule['recentorder_count']['type'] == '1' ? 'selected':'' }}  selected value="1" >Is Greater Than</option>
                                    <option {{ $displayProrule['recentorder_count']['type'] == '2' ? 'selected':'' }} value="2" >Is Less Than</option>
                                  </select>
                                </div>
                                <span class="mt-2 mx-1 mt-1">  </span>
                                <div class="mx-1 Proinventorytype col mt-1" >
                                  <input type="number" min="0" id="saverecentordercount" name="saverecentordercount" value="{{ $displayProrule['recentorder_count']['value'] }}" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                                  <input type="hidden" min="0" name="prorule[1][conditiontype][recentorder_count][value]" value="{{ $displayProrule['recentorder_count']['value'] }}" class="form-control" >
                                </div>
                              </div>
                            @endif
                        @endforeach
                      @else
                        <div class="row mx-0 px-0">
                          <div class="mx-1 mt-1"  >
                            <p class="my-1">Order Count</p>
                          </div>
                          <div class="mx-1 mt-2 mt-1"> </div>
                          <div class="mx-0 px-1 Proinventorytype mt-1" >
                            <select name="prorule[0][conditiontype][order_count][type]" class="form-control"  >
                              <option selected value="1" >Is Greater Than</option>
                              <option value="2" >Is Less Than</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1 mt-1">  </span>
                          <div class="mx-1 Proinventorytype col mt-1" >
                            <input type="number" min="0" id="saveordercount" name="saveordercount" value="" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                            <input type="hidden" min="0"  name="prorule[0][conditiontype][order_count][value]" value="" class="form-control" >
                          </div>
                        </div>
                        
                        <div class="row mx-0 px-0">
                          <div class="mx-1 mt-1"  >
                            <!-- <p class="my-1">Recent Sold </p> -->
                            <p class="my-1">Recent Sold Count</p>
                          </div>
                          <div class="mx-1 mt-2 mt-1"> </div>
                          <div class="mx-0 px-1 Proinventorytype mt-1" >
                            <select name="prorule[1][conditiontype][recentorder_count][type]" class="form-control"  >
                              <option selected value="1" >Is Greater Than</option>
                              <option value="2" >Is Less Than</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1 mt-1">  </span>
                          <div class="mx-1 Proinventorytype col mt-1" >
                            <input type="number" min="0" id="saverecentordercount" name="saverecentordercount" value="" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )" >
                            <input type="hidden" min="0"  name="prorule[1][conditiontype][recentorder_count][value]" value="" class="form-control" >
                          </div>
                        </div>
                      @endif
                    <!-- </div>
                  </div>
                </div> -->
              </div>

            </div>
            <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
              <div class="alert bg-dark">
                <h4 class="text-white" id="requestmessage_collection"></h4>
              </div>
            </div>

          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" onclick="location.href='{{asset('settings-order-count')}}'" class="btn btn-outline-primary">Settings</button>
          <button type="button" onclick="saveOrderCount();" class="btn btn-primary">Save</button>
          
        </div>
      @else
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
          <p class="alert alert-danger w-100">This feature is available only on the Pro Plan.
                  <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
              </p>
              
         </div>
        </div>
      </div>

      @endif
    </div>
  </div>
</div>


    </form>
</div>
 


</div>


<div class="modal fade bd-example-modal-xl" id="Country-model" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Show the highlight for selected countries only</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if(Auth::User()->planid > 2)
        <div class="modal-body">
          <div class="container-fluid">
            
            <div class="">
              <div class="card-group">
                <div class="card">
                  <div class="card-header">
                      List of countries (<span id=addedcount>0</span>)
                      <button   onclick="addCountryAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div>               
                  <div class="card-body p-1">
                    <table id="Countrylist" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Countries</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                        @if( !empty( $highlight['customer_location'] ) )
                          <?php
                          
                            $CountryArrayValues2=explode(",",implode(",", $highlight['customer_location'] ) );
                          ?>
                        @else
                          <?php
                         
                            $CountryArrayValues2=array();
                          ?>
                        @endif
                        @if( !empty( $countries ) ) 
                          @if( isset( $highlight['customer_location'] ) )
                            @foreach( $countries as $country )
                              @if (!in_array($country['code'], $CountryArrayValues2))
                                <tr class="">
                                    <td class="py-1">
                                  {{$country['countryname']}}
                                  </td>
                                  <td class="py-1">                                  
                                    <button  countryid="{{$country['code']}}"   onclick="addCountry(this,'{{$country['code']}}','{{$country['countryname']}}')" class="btn  btn-sm btn-outline-secondary" >
                                      Add
                                    </button>
                                  </td>
                                </tr>
                              @endif
                            @endforeach 
                          @else
                            @foreach( $countries as $country )
                              <tr class="">
                                <td class="py-1">
                                  {{$country['countryname']}}
                                </td>
                                <td class="py-1">
                                  <button  countryid="{{$country['code']}}"   onclick="addCountry(this,'{{$country['code']}}','{{$country['countryname']}}')" class="btn  btn-sm btn-outline-secondary" >
                                        Add
                                    </button>
                                </td>
                              </tr>
                            @endforeach 
                          @endif
                        @endif
                      </tbody>
                    </table>

                    <!-- <div  class="col-12 mt-4 card-footer" style="text-align:center;">
                      Count:<span id=addedcount>0</span>
                    </div> -->

                  </div>
                  <!-- <div class="card-footer">
                    <button  countryid="{{$country['code']}}"  onclick="addCountryAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div> -->

                </div>
                    <div class="card">
                  <div class="card-header">
                      Selected countries (<span id=removecount>0</span>)
                      <button   onclick="removeCountryAll();" class="btn  btn-sm btn-outline-danger float-right" >
                        Remove All
                      </button>
                  </div>        
                  <div class="card-body  p-1">
                    <table id="Countrylistadded" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Countries</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                      @if( !empty( $highlight['customer_location'] ) )
                        <?php
                          $CountryArrayValues=explode(",",implode(",", $highlight['customer_location'] ) );
                        ?>
                          @foreach( $countries as $country )
                            @if (in_array($country['code'], $CountryArrayValues))
                              <tr class="">
                                <td class="py-1">
                                  {{$country['countryname']}}
                                </td>

                                <td class="py-1">
                                  <button  countryid="{{$country['code']}}"  onclick="RemoveCountry(this,'{{$country['code']}}','{{$country['countryname']}}')" class="btn  btn-sm btn-outline-danger" >
                                    Remove
                                  </button>
                                </td>
                              </tr>
                            @endif
                          @endforeach 
                      @endif
                      </tbody>
                    </table>

                    <div  class="col-12 mt-4 card-footer" style="text-align:center;">
                      Count:<span id=removecount>0</span>
                    </div>

                  </div>

                  <!-- <div class="card-footer">
                    <button  countryid="{{$country['code']}}"  onclick="removeCountryAll();" class="btn  btn-sm btn-outline-danger" >
                      Remove All
                    </button>
                  </div> -->

                </div>
              </div>
          

            

            </div>

            <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
              <div class="alert bg-dark">
                <h4 class="text-white" id="requestmessage_collection"></h4>
              </div>
            </div>
              
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" onclick="saveCountry();" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      @else
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
          <p>Display highlights based on user location. This feature is available only on the Pro Plan. [[Upgrade Now]].</p>
          </div>
        </div>
      </div>

      @endif
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-xl" id="ShopLanguage-model" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Show the highlight for selected languages only</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if(Auth::User()->planid > 2)
        <div class="modal-body">
          <div class="container-fluid">
            <div class="">
              <div class="card-group">
                <div class="card">
                  <div class="card-header">
                      List of languages (<span id=langaddedcount>0</span>)
                      <button   onclick="addShopLanguageAll();" class="btn  btn-sm btn-outline-secondary float-right" >
                        Add All
                      </button>
                  </div>               
                  <div class="card-body p-1">
                    <table id="ShopLanguagelist" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Languages</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                        @if( !empty( $highlight['shop_language'] ) )
                          <?php
                            $ShopLanguageArrayValues2=explode(",",implode(",", $highlight['shop_language'] ) );
                          ?>
                        @else
                          <?php
                            $ShopLanguageArrayValues2=array();
                          ?>
                        @endif
                        @if( !empty( $languages ) ) 
                          @if( isset( $highlight['shop_language'] ) )
                            @foreach( $languages as $ShopLanguage )
                              @if (!in_array($ShopLanguage['languagecode'], $ShopLanguageArrayValues2))
                                <tr class="">
                                    <td class="py-1">
                                  {{$ShopLanguage['languagename']}}
                                  </td>
                                  <td class="py-1">                                  
                                    <button  ShopLanguageid="{{$ShopLanguage['languagecode']}}"   onclick="addShopLanguage(this,'{{$ShopLanguage['languagecode']}}','{{$ShopLanguage['languagename']}}')" class="btn  btn-sm btn-outline-secondary" >
                                      Add
                                    </button>
                                  </td>
                                </tr>
                              @endif
                            @endforeach 
                          @else
                            @foreach( $languages as $ShopLanguage )
                              <tr class="">
                                <td class="py-1">
                                  {{$ShopLanguage['languagename']}}
                                </td>
                                <td class="py-1">
                                  <button  ShopLanguageid="{{$ShopLanguage['languagecode']}}"   onclick="addShopLanguage(this,'{{$ShopLanguage['languagecode']}}','{{$ShopLanguage['languagename']}}')" class="btn  btn-sm btn-outline-secondary" >
                                        Add
                                    </button>
                                </td>
                              </tr>
                            @endforeach 
                          @endif
                        @endif
                      </tbody>
                    </table>

                  </div>
                   

                </div>
                    <div class="card">
                  <div class="card-header">
                      Selected languages (<span id=langremovecount>0</span>)
                       <button   onclick="RemoveShopLanguageAll();" class="btn  btn-sm btn-outline-danger float-right" >
                      Remove All
                    </button>
                  </div>        
                  <div class="card-body p-1">
                    <table id="ShopLanguagelistadded" class="table table-sm table-bordered table-striped text-center" cellspacing="0" width="100%" >
                      <thead>
                        <tr>
                          <th>Languages</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="">
                      @if( !empty( $highlight['shop_language'] ) )
                        <?php
                          $ShopLanguageArrayValues=explode(",",implode(",", $highlight['shop_language'] ) );
                        ?>
                          @foreach( $languages as $ShopLanguage )
                            @if (in_array($ShopLanguage['languagecode'], $ShopLanguageArrayValues))
                              <tr class="">
                                <td class="py-1">
                                  {{$ShopLanguage['languagename']}}
                                </td>

                                <td class="py-1">
                                  <button  ShopLanguageid="{{$ShopLanguage['languagecode']}}"  onclick="RemoveShopLanguage(this,'{{$ShopLanguage['languagecode']}}','{{$ShopLanguage['languagename']}}')" class="btn  btn-sm btn-outline-danger" >
                                    Remove
                                  </button>
                                </td>
                              </tr>
                            @endif
                          @endforeach 
                      @endif
                      </tbody>
                    </table>
                  </div>
                          
                </div>
              </div>

            </div>

            <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
              <div class="alert bg-dark">
                <h4 class="text-white" id="requestmessage_collection"></h4>
              </div>
            </div>
              
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" onclick="saveShopLanguage();" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      @else
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <p class="alert alert-danger w-100">Display highlights based on customer language. This feature is available only on the Pro Plan.
                  <br><a class="btn btn-danger mt-2" href="/plans">Upgrade Now</a>
              </p>
                
          </div>
          </div>
        </div>

      @endif

    </div>
  </div>
</div>


<div class="modal fade " id="Collection-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Add Collections</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="container-fluid">
          <!-- <div class="row" >

            <div class="col-12" id="selectviasearchdiv">
              <div class="input-group">
                <div class="input-group-prepend" >
                  <button id="collection_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Collections</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style=" transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item collect_type" onclick="collection_select_option(1);" value="1" href="#">All Collections</a>
                    <a class="dropdown-item collect_type" onclick="collection_select_option(2);" value="2" href="#">Selected Collections</a>
                  </div>
                </div>
              </div>
            </div>

            <div>
                </br>
                </br>
            </div>
          </div> -->

          <div class="row">
            <div class="col-12" id="selectviasearchdiv">
              <div class="">
                <div class="input-group-prepend" style="position: absolute;">
                  <button id="collection_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Collections</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style=" transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item collect_type" onclick="collection_select_option(1);" value="1" href="#">All Collections</a>
                    <a class="dropdown-item collect_type" onclick="collection_select_option(2);" value="2" href="#">Selected Collections</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-12" id="Collection-tableDataModel">
            </div>
          </div>

          <div class="text-center p-1 m-auto text-center p3 col-12 col-md-8 collapse" id="requestalert_collection" style="top:40px;" >
            <div class="alert bg-dark">
              <h4 class="text-white" id="requestmessage_collection"></h4>
            </div>
          </div>
            
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade " id="ExProduct-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="">
       <h5 class="modal-title">Exclude products
            <span class="small text-secondary ml-2">(Highlights will not be displayed on these products)</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 ">
              <div class="input-group">
                <div class="input-group-prepend">
                  <button id="Exidtype_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Products</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item exidtype" value="1" href="#">All Products </a>
                    <a class="dropdown-item exidtype" value="2" href="#">Excluded</a>
                  </div>
                </div>
                <input id="Exsearchbox" type="search" class="form-control" value="" placeholder="Search product title with minimum 3 characters">
                <div class="input-group-append">
                  <button id="exsearchItem" class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>
            <div>
              </br>
              </br>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="ExtableDataModel">
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade " id="blacklistProduct-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">Add products</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 ">
              <div class="input-group">
                <div class="input-group-prepend">
                  <button id="idtype_selection" class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Products</button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item idtype" value="1" href="#">All Products </a>
                    <a class="dropdown-item idtype" value="2" href="#">Selected Products</a>
                  </div>
                </div>
                <input id="searchbox" type="search" class="form-control" value="" placeholder="Search product title with minimum 3 characters">
                <div class="input-group-append">
                  <button id="searchItem" class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>
            <div>
              </br>
              </br>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="tableDataModel">
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="metafieldModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header bg-primary" id="">
        <h5 class="modal-title">
          Product Metafield Name
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              The app accepts the metafield name in the following format:<br>
              <b>Namespace</b>.<b>Key</b><br><br>
              Example Metafield Names:<br>
              1. global.wash<br>
              2. sizing.large
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>

<div class="modal fade " id="Linktab-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="">
        <h5 class="modal-title">Modal Content</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 ">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="">Modal Size</label>
              </div>
              <select id="modalsize" class="form-control" name="modalsize">
                <option {{ (isset( $highlight['modalsize'] ) && $highlight['modalsize'] == 0) ? 'selected':'' }} value="0">Small</option>
                <option {{ (isset( $highlight['modalsize'] ) && $highlight['modalsize'] == 1) ? 'selected':'' }} value="1">Medium</option>
                <option {{ (isset( $highlight['modalsize'] ) && $highlight['modalsize'] == 2) ? 'selected':'' }} value="2">large</option>
              </select>
            </div>

              <textarea name="highlightdescription" id="mytextarea" value="{{ isset( $highlight['highlightdescription'] ) ? $highlight['highlightdescription'] :'' }}" >{{ isset( $highlight['highlightdescription'] ) ? $highlight['highlightdescription'] :'' }}</textarea>
            </div>
            <div>
              </br>
              </br>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="saveModalContent();" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

 @include('dashboard.iconmodal')

<div class="modal fade " id="openDynamicVariableModel-model" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary" id="thewatchlystModalHeader">
        <h5 class="modal-title">
          Dynamic Data Fields
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="">
            Display product data as part of the highlight text.
            <!-- <div class="mt-1 mb-3">
              <a class="" target="_blank" href="https://starapp.freshdesk.com/support/solutions/articles/48000430925-dynamic-badge-text">
                More info <span class="fa fa-caret-right"></span>
              </a>
              </span>
            </div>-->
            <div class="row mt-3">
              <div class="col-xs-12 col-sm-12 col-lg-12 text-center" id="tableDataModel">
                <table id="productlist" class="table table-bordered table-striped text-left table-sm" cellspacing="0" width="100%">
                  <thead>
                    <tr class="text-center">
                      <!--<th scope="col"></th>-->
                      <th class="text-left" scope="col ">Dynamic Data Fields</th>
                      <th scope="col ">Action</th>
                    </tr>
                  </thead>
                  <colgroup>
                    <col width="75%">
                    <col width="25%">
                  </colgroup>
                  <tbody id="">
                    @foreach( $dynamicVariables as $index => $variable )
                      <tr class="text-center">
                        <td class="text-left">
                          <div class="">
                            {{$variable["var"]}}
                          </div>
                          <div class="small text-muted">
                            {{$variable['des']}}
                          </div>
                        </td>
                        <td>
                          <div class="btn-group productall">
                            <button value="{{$variable["exm"]}}" onclick="addVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="Edit product parameters">
                              Select
                            </button>
                          </div>
                        </td>
                      </tr>
                    @endforeach

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ primehm";$dynamicVariableTitle2 = "}}";$dynamicVariableExm = "{{ primehm.namespace.metafield }} "; ?>
                            {!! $dynamicVariableTitle !!}.<span style="font-style: italic;">namespace.metafield </span>{!! $dynamicVariableTitle2 !!}
                          <span class="badge badge-info ml-2">Pro</span>
                        </div>
                    
                        <div class="small text-muted">
                          Product metafield. <a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000385708-dynamic-metafields"> More info<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                        <td class="text-left">
                          <div class="">
                            <?php $dynamicVariableTitle = "{{ OrderCount }}";$dynamicVariableDesc = "Total order count";$dynamicVariableExm = "{{ OrderCount }} sold "; ?>
                            {!! $dynamicVariableTitle !!}
                            <span class="badge badge-info ml-2">Pro</span>
                            <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-order-count')}}'" title="Edit product parameters">
                              Setting
                            </button>
                          </div>
                      
                          <div class="small text-muted">
                            {!! $dynamicVariableDesc !!}
                          </div>
                        </td>
                        <td>
                          <div class="btn-group productall">
                          @if(Auth::User()->planid > 2)
                            @if(isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '')
                              <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                                Select
                              </button>
                            @else
                              <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                                Disable
                              </button>
                            @endif
                          @else
                            <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                              Disable
                            </button>
                          @endif
                          </div>
                        </td>
                      </tr>

                      <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ RecentSoldCount }}";$dynamicVariableDesc = "Recent sold count";$dynamicVariableExm = "{{ RecentSoldCount }} Sold "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-order-count')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          @if(isset(Auth::User()->orderupdatetime) && Auth::User()->orderupdatetime != '')
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                          @else
                            <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                              Disable
                            </button>
                          @endif
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                      <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ CountdownTimer }}";$dynamicVariableDesc = "Countdown timer";$dynamicVariableExm = "{{ CountdownTimer }}"; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-info ml-2">Pro</span>
                        </div>
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!} <a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000386626-countdown-timer"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                        <div class="flatpickr input-group input-group-sm mt-1" id="datetimepicker3"> 
                          <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-sm">{{ $arrNameById[$mytimezone] }}</span>
                            <!-- <span class="input-group-text" id="inputGroup-sizing-sm">New Delhi Time</span> -->
                          </div>
                          <input type="text" class="form-control flatpickr-input" id="input_datetimepicker3" name="dynamicendtime" value="" placeholder="Select End Date" data-input="" readonly="readonly"><div class="input-group-append">
                          <!-- <span class="input-group-text" id="inputGroup-sizing-sm">IST</span> -->
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariableDate(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ ExpectedDeliveryDate }}";$dynamicVariableDesc = "Expected Delivery Date";$dynamicVariableExm = "{{ ExpectedDeliveryDate }} "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-info ml-2">Pro</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-delivery-date')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                        <div class="small text-muted ">
                          {!! $dynamicVariableDesc !!} <a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000392649-estimated-delivery-dates"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>

                        <div class="small text-muted mt2">
                          From Current date + 
                          <select class="form-control form-control-sm w-auto d-inline" name="firstdate" id="firstdate" >
                            <option value="0" >0</option>
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                            <option value="7" >7</option>
                            <option value="8" >8</option>
                            <option value="9" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                            <option value="13" >13</option>
                            <option value="14" >14</option>
                            <option value="15" >15</option>
                            <option value="16" >16</option>
                            <option value="17" >17</option>
                            <option value="18" >18</option>
                            <option value="19" >19</option>
                            <option value="20" >20</option>
                            <option value="21" >21</option>
                            <option value="22" >22</option>
                            <option value="23" >23</option>
                            <option value="24" >24</option>
                            <option value="25" >25</option>
                            <option value="26" >26</option>
                            <option value="27" >27</option>
                            <option value="28" >28</option>
                            <option value="29" >29</option>
                            <option value="30" >30</option>
                          </select>
                          to + 
                          <select class="form-control form-control-sm w-auto d-inline" name="seconddate" id="seconddate" >
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                            <option value="7" >7</option>
                            <option value="8" >8</option>
                            <option value="9" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                            <option value="13" >13</option>
                            <option value="14" >14</option>
                            <option value="15" >15</option>
                            <option value="16" >16</option>
                            <option value="17" >17</option>
                            <option value="18" >18</option>
                            <option value="19" >19</option>
                            <option value="20" >20</option>
                            <option value="21" >21</option>
                            <option value="22" >22</option>
                            <option value="23" >23</option>
                            <option value="24" >24</option>
                            <option value="25" >25</option>
                            <option value="26" >26</option>
                            <option value="27" >27</option>
                            <option value="28" >28</option>
                            <option value="29" >29</option>
                            <option value="30" >30</option>
                          </select> days.
                        </div>

                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 2)
                            <button value="{!! $dynamicVariableExm !!}"  onclick="addProVariableExpectedDate(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                              Select
                            </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>

                    <tr class="text-center">
                      <td class="text-left">
                        <div class="">
                          <?php $dynamicVariableTitle = "{{ VisitorCounter }}";$dynamicVariableDesc = "Visitor counter";$dynamicVariableExm = "{{ VisitorCounter }} recent visitors "; ?>
                          {!! $dynamicVariableTitle !!}
                          <span class="badge badge-dark ml-2">Gold</span>
                          <button  class="btn btn-sm btn-outline-secondary" style="float: right;" onclick="location.href='{{asset('settings-livevisitcount')}}'" title="Edit product parameters">
                            Setting
                          </button>
                        </div>
                    
                        <div class="small text-muted">
                          {!! $dynamicVariableDesc !!}<a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000390548-live-visitor-counter"> Know more<span class="fa fa-caret-right ml-1"></span></a>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group productall">
                        @if(Auth::User()->planid > 3)
                          <button value="{!! $dynamicVariableExm !!}"  onclick="addGoldVariable(this)" class="btn btn-sm btn-outline-secondary variableBtnText" title="">
                            Select
                          </button>
                        @else
                          <button class="btn btn-sm btn-outline-secondary btn-disable " title="">
                            Disable
                          </button>
                        @endif
                        </div>
                      </td>
                    </tr>


                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="producttitle" id="thewatchlystAlertUrl">
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>  

@include('dashboard.gradientshadow')

<script>
  tinymce.init({
    selector: '#mytextarea',
    menubar: false,
    plugins: ['advlist lists code image table autoresize','fullscreen','textcolor','emoticons'],
    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent bullist numlist |' + ' code image table | alignleft aligncenter ' + 'alignright |  ' + 'forecolor backcolor casechange permanentpen formatpainter removeformat | emoticons',
    a11y_advanced_options: true,
    height: 600,
    toolbar_sticky: true,
    toolbar_mode: 'sliding',
    image_title: true,
    automatic_uploads: true,
    file_picker_types: 'image',
    file_picker_callback: function (cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function () {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = function () {
          var id = 'blobid' + (new Date()).getTime();
          var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      };

      input.click();
    },
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js" ></script> 
<script>

  var input = document.querySelector('#highlighttitle');
  var textStyle = document.getElementById('textStyle');
  var emojibutton = document.querySelector('#emoji');



</script>
<script type="text/javascript">

  $(document).ready(function(){

    var input = document.querySelector('#editdiv');
    var emojibutton = document.querySelector('#emoji');

    var picker = new EmojiButton({
      position:'bottom',
    });

    picker.on('emoji', function(emoji){
      input.value += emoji;
      
    });

    emojibutton.addEventListener('click', function(){
      picker.pickerVisible ? picker.hidePicker() : picker.showPicker(emojibutton);
    });


    var selectedinventory = $('.selectedinventory').val();
    if (selectedinventory === undefined || selectedinventory < 1) {
      
    }else{
      if(selectedinventory>1){
        // alert()
        $(".inventoryhide").css({"display": "none"});
        
      }
    }
   
   // $('#editdiv').prop('contenteditable',true);
    
    //var content = $('#highlighttitle').val().replace(/<img[^>]*>/g,"");
    var content = $('#highlighttextval').text();
    $('#highlighttitle').val(content);
    //console.log(content);
    
    setTimeout( function(){

      //console.log('loadeddd');
      $('#animationdiv').css('display','none');
    },500);

    
    // $("#tooltip").mouseout(function(){
    //   var content_val = $('#tooltip').val();
    //   $('.tooltips').attr('data-original-title', content_val);
    // });

    // var content_val = $('#tooltip').val();
    // if( content_val ) {
      
    // }
    


    $("#tooltip").mouseout(function(){
      var content = $('#tooltip').val();
      if(content.length > 100){
        $('#requestalert').show();
        $('#requestmessage').text("100 char max limit on tooltip");
        $("#requestalert").delay(2500).hide(0);
        $('#tooltip').val('');
      }else{
        $('.tooltips').attr('data-original-title', content_val);
      }
      
    });

    var content_val = $('#tooltip').val();
    if(content_val.length > 100){
      $('#requestalert').show();
      $('#requestmessage').text("100 char max limit on tooltip");
      $("#requestalert").delay(2500).hide(0);
      $('#tooltip').val('');
    }else{
      $('.tooltips').attr('data-original-title', content_val);
    }
    
    $('[data-toggle="tooltip"]').tooltip();

  
    

   
  });

 

  $('#image').on('change',function(){
    var fileName = $(this).val();
    $(this).next('.custom-file-label').html(fileName);
  });

  $(function () {
    $("#datetimepicker1").flatpickr({enableTime: true,
      dateFormat: "d-m-Y h:i K", wrap: true});
    $("#datetimepicker2").flatpickr({enableTime: true,
      dateFormat: "d-m-Y h:i K", wrap: true});
    $("#datetimepicker3").flatpickr({enableTime: true,
      dateFormat: "d/m/Y h:i K", wrap: true,defaultDate: new Date().fp_incr(6),minDate: "today"});

  });

  $(document).ready(function (){
    //font_style();
    check_enable_date();
    linktab();
    //check_highlight_type();

    var nlangcount = $("#addShopLanguage_counter").val();
    if( nlangcount != '' ){
      var nows = nlangcount;
      var narr =  nows.split(',');
      nlangcount = narr;
      $('#language_count').text(nlangcount.length);
    }

    var ncountrycount = $("#addCountry_counter").val();
    if( ncountrycount != '' ){
      var nows = ncountrycount;
      var narr2 =  nows.split(',');
      ncountrycount = narr2;
      $('#country_count').text(ncountrycount.length);
    }

  
    $('.metafieldcond').each(function(i, obj) {

       var conditionvalue = $(this).val();
      //console.log(conditionvalue);
      if( (conditionvalue== 2) || (conditionvalue == 3) ){

        var metakeydiv = $(this).parent().next();
       // console.log($(this).parent().next());
        $(metakeydiv).find('input').val('');
        $(metakeydiv).hide();

      } else {

        var metakeydiv = $(this).parent().next();
        $(metakeydiv).show();
      }
      

    });

    // var visibility2 = $('#input_datetimepicker2').val();
    // var visibility1 = $('#input_datetimepicker1').val();
    // if(visibility2 == '01-02-2035 12:00 PM' && visibility1 == '01-02-1990 12:00 PM' ){
    //   $("#startingnow").prop("checked", true);
    //   $("#enabledate").prop("checked", false);
    //   $('#input_datetimepicker1').val('');
    //   $('#input_datetimepicker2').val('');
    //   $('#input_datetimepicker1').prop("disabled", true);
    //   $('#input_datetimepicker2').prop("disabled", true);
    //   $('.date').hide();
    // }

    $(document).on("change", ".metafieldcond", function() {
      
      var conditionvalue = $(this).val();
      //console.log(conditionvalue);
      if( (conditionvalue== 2) || (conditionvalue == 3) ){

        var metakeydiv = $(this).parent().next();
       // console.log($(this).parent().next());
        $(metakeydiv).find('input').val('');
        $(metakeydiv).hide();

      } else {

        var metakeydiv = $(this).parent().next();
        $(metakeydiv).show();
      }
      
    });
      
  });

  function errorimgmsg(error){
    console.log('inerror');
    // error.onerror=null;
    var strerror = error.src;
    var free = strerror.replace("data:image/svg+xml", "data:image/png");
    error.src = free;
  }


  
  function addVariable( instance ){
    var mydynamicvariable = '';
    // $(".variableBtnText").text("Select");
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mydynamicvariable = mydynamicvariable + realvalue;
    // $("#badgetitle").val( $(instance).attr("value") );
    $("#editdiv").val( mydynamicvariable );
    $("#editdiv").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  function addProVariable( instance ){
    var mydynamicvariable = '';
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#editdiv").val( mydynamicvariable );
    $("#editdiv").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  function addProVariableDate( instance ){
    var mydynamicvariable = '';
    var mydynamicdate = $('#input_datetimepicker3').val();
    // console.log(mydynamicdate);
    $(instance).text("Selected");
    // var realvalue = $(instance).attr("value");
    // var realvalue = '\{\{ CountdownTimer:'+mydynamicdate+' \}\}';
    var realvalue = 'Sale ends in [[ CountdownTimer:'+mydynamicdate+' ]]';
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#editdiv").val( mydynamicvariable );
    $("#editdiv").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  function addProVariableExpectedDate( instance ){
    var mydynamicvariable = '';
    var first = $('#firstdate').val();
    var second = $('#seconddate').val();
    var realvalue = 'Expected delivery between [[today,'+first+']] and [[today,'+second+']]';
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#editdiv").val( mydynamicvariable );
    $("#editdiv").keyup();
    $("#openDynamicVariableModel-model").modal('hide');

  }

  function addGoldVariable( instance ){
    var mydynamicvariable = '';
    $(instance).text("Selected");
    var realvalue = $(instance).attr("value");
    mydynamicvariable = mydynamicvariable + realvalue;
    $("#editdiv").val( mydynamicvariable );
    $("#editdiv").keyup();
    $("#openDynamicVariableModel-model").modal('hide');
  }

  // function addVariable( instance ){
  //   $(".variableBtnText").text("Select");
  //   $(instance).text("Selected");

  //   //console.log($(instance).attr("value"));
  //   $("#editdiv").val( $(instance).attr("value") );
  //   $("#editdiv").keyup();
  //   $("#openDynamicVariableModel-model").modal('hide');
  // }

    function alert_error_message(text){
    $('#requestalert').show();
    $('#requestmessage').text(text);
    $("#requestalert").delay(2500).hide(0);
  }

  $("#linktab").change( function(){
    linktab();
  });

  function linktab(){
    var linktab = $("#linktab").val();
    if(linktab == 2){
      $( "#highlightlink" ).prop( "disabled", true );
      $( "#highlightlink" ).val('');
      $("#linktabmodal").show();
    }else{
      $( "#highlightlink" ).prop( "disabled", false  );
      $("#linktabmodal").hide();
    }
  }

  function check_enable_date(){
    var checkvalue = $('#enabledate').prop('checked');
    if(checkvalue == true){
      $('.date').show();
      $('#input_datetimepicker1').prop("disabled", false);
      $('#input_datetimepicker2').prop("disabled", false);
      var visibility2 = $('#input_datetimepicker2').val();
      var visibility1 = $('#input_datetimepicker1').val();
      if(visibility2 == '01-02-2035 12:00 PM' && visibility1 == '01-02-1990 12:00 PM' ){
        var d = new Date();
        var strDate =addZero(d.getDate()) +  "-" + addZero(d.getMonth()+1) + "-"+ d.getFullYear()  +  " 1:00 AM" ;
        var strDate2 =addZero(d.getDate()+1) +  "-" + addZero(d.getMonth()+1) + "-"+ d.getFullYear()  +  " 1:00 AM" ;
        $("#datetimepicker1").flatpickr({enableTime: true,
        dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,defaultDate: strDate,});
        $('#input_datetimepicker1').val(strDate);
        $("#datetimepicker2").flatpickr({enableTime: true,
        dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,defaultDate: strDate2,});
        $('#input_datetimepicker2').val(strDate2);
      }
    }else{
      $('#input_datetimepicker1').prop("disabled", true);
      $('#input_datetimepicker2').prop("disabled", true);
      // $('#input_datetimepicker1').val('');
      // $('#input_datetimepicker2').val('');
      $('.date').hide();
    }
  }

  
  function check_highlight_height(){
    var height = $('#height').val();
    if(height > 99){
      var text = "Height cannot be more than 99px";
      alert_error_message(text);
      $('#height').val('99');
      return false;
    }
    return true;
  }

  $("#setcurrentdate").click( function(){
    var d = new Date();
    var strDate =addZero(d.getDate()) +  "-" + addZero(d.getMonth()+1) + "-"+ d.getFullYear()  +  " 1:00 AM" ;
    console.log(strDate);
    $("#datetimepicker1").flatpickr({enableTime: true,
      dateFormat: "d-m-Y h:i K", wrap: true, disableMobile: false,defaultDate: strDate,});

    $('#input_datetimepicker1').val(strDate);
  });

  function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
  }

  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          var file = input.files[0];
          var fileType = file["type"];
          var validImageTypes = ["image/gif", "image/png", "image/jpeg", "image/svg+xml"];
          if ($.inArray(fileType, validImageTypes) < 0) {
            $('#image').val('');
            var text = "Only png, jpeg, gif, svg is allowed.";
            alert_error_message(text);
            return false;
          }else{
            if(input.files[0].size > 500000) {
              var text = "File size is more than 500 KB";
              alert_error_message(text);
              $('#image').val('');
              return false;
            };
          }
         // $('#highlightStyle').hide();
          $('#highlightStyle_image').show();
          $('#icondiv img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
  }

  $("#image").change(function(){
    readURL(this);
  });

  $("#height").change(function(){
    var height = $('#height').val();
    if(height){
      $('#highlight_image_preview').attr('style','height:'+ height +'px');
    }else{
      $('#highlight_image_preview').attr('style','height:25px');
    }
    
  });


  function openOrderCountModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
      backdrop: 'static',
      keyboard: false
    }); 
  }

  function saveOrderCount(){
    var orderrulevalue = $('#saveordercount').val();
    $('input[name="prorule[0][conditiontype][order_count][value]"]').val(orderrulevalue);
    var recentorderrulevalue = $('#saverecentordercount').val();
    $('input[name="prorule[1][conditiontype][recentorder_count][value]"]').val(recentorderrulevalue);
    $("#OrderCount-model").modal('hide');
    
  };


  var Countrylisttable = $('#Countrylist').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );

  var Countrylistaddedtable = $('#Countrylistadded').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );
  
  var CountryValue = '';
  var countervalue   = 0;

  function openCountryModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
      backdrop: 'static',
      keyboard: false
    }); 

    filterType = 1;
    CountryValue = $("#addCountry_counter").val();
    countervalue = counter;
    if( CountryValue != '' ){
      var nows = CountryValue;
      arr =  nows.split(',');
      read_data = unique4(arr);
      pro_uni_data = read_data.join(",");
      CountryValue = pro_uni_data;
    }
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());
  }

  function addCountryAll(){
    Countrylisttable.search('').draw();
    $("#Countrylist > tbody > tr").each(function () {
      CountryName = $.trim($(this).find('td').eq(0).text());
      CountryButton = $(this).find('td').eq(1);
      Countryid = CountryButton.find('button').attr('countryid');
      Countrylisttable.row( $(CountryButton).parents('tr') ).remove().draw();

      Countrylistaddedtable.row.add( [
        ''+CountryName+'',
        '<button  countryid="'+Countryid+'"  onclick="RemoveCountry(this,\''+Countryid+'\',\''+CountryName+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
      
    });
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());

  }

  function removeCountryAll(){
    Countrylistaddedtable.search('').draw();
    $("#Countrylistadded > tbody > tr").each(function () {
      CountryName = $.trim($(this).find('td').eq(0).text());
      CountryButton = $(this).find('td').eq(1);
      Countryid = CountryButton.find('button').attr('countryid');
      Countrylistaddedtable.row( $(CountryButton).parents('tr') ).remove().draw();
      
      Countrylisttable.row.add( [
        ''+CountryName+'',
        '<button  countryid="'+Countryid+'"  onclick="addCountry(this,\''+Countryid+'\',\''+CountryName+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');

    });
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());

  }

  function addCountry( instance , countryid, countryname ){
    let Countrynew = $(instance).attr('countryid');
    Countrylisttable.row( $(instance).parents('tr') ).remove().draw();
    Countrylistaddedtable.row.add( [
      ''+countryname+'',
      '<button  countryid="'+countryid+'"  onclick="RemoveCountry(this,\''+countryid+'\',\''+countryname+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
      ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');

    Countrylisttable.search('').draw();
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());

  }

  function RemoveCountry( instance , countryid, countryname ){
    let Countrynew = $(instance).attr('countryid');
    Countrylistaddedtable.row( $(instance).parents('tr') ).remove().draw();
    var selectedCountryName = '';
    var selectedid = '';
    var CountriesArray = <?php echo json_encode($countries); ?>;
    
    $.each( CountriesArray, function( index, value ){
      if(value['code'] == countryid){
        Countrylisttable.row.add( [
          ''+countryname+'',
          '<button  countryid="'+countryid+'"   onclick="addCountry(this,\''+countryid+'\',\''+countryname+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
          ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
      }
    });
    Countrylistaddedtable.search('').draw();
    $('#removecount').text(Countrylistaddedtable.rows().count());
    $('#addedcount').text(Countrylisttable.rows().count());

  }

  
  function saveCountry(){
    var countryArray = [];
    var savecountryids = '';
    $("#Countrylistadded > tbody > tr").each(function () {
      selectedCountryName = $(this).find('td').eq(1);
      selectedid = selectedCountryName.find('button').attr('countryid');
      if(selectedid != undefined){
        countryArray.push(selectedid);
      }
      if( countryArray.length > 0 ){
        savecountryids = countryArray.join(",");
      }else{
        savecountryids = "";
      }
      $("#Country-model").modal('hide');
      $("#addCountry_counter").val(savecountryids);
      $("#customer_location_count").text(countryArray.length);
      if(countryArray.length > 0){
        $('#country_count').text(countryArray.length);
      }else{
        $('#country_count').text('');
      }

    });    

  }


  // Only For Customer language Rule ................

  var ShopLanguagelisttable = $('#ShopLanguagelist').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );

  var ShopLanguagelistaddedtable = $('#ShopLanguagelistadded').DataTable( {
      responsive: true,
      // "order": [[ 0, "asc" ]],
      "bSort" : false,
      "paging": false,
      "info":   false,
  } );
  
  var ShopLanguageValue = '';
  var countervalue   = 0;

  function openShopLanguageModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
      backdrop: 'static',
      keyboard: false
    }); 

    filterType = 1;
    ShopLanguageValue = $("#addShopLanguage_counter").val();
    countervalue = counter;
    if( ShopLanguageValue != '' ){
      var nows = ShopLanguageValue;
      arr =  nows.split(',');
      read_data = unique4(arr);
      pro_uni_data = read_data.join(",");
      ShopLanguageValue = pro_uni_data;
    }
    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());
  }

  function addShopLanguageAll(){
    $("#ShopLanguagelist > tbody > tr").each(function () {
      ShopLanguageName = $.trim($(this).find('td').eq(0).text());
      ShopLanguageButton = $(this).find('td').eq(1);
      ShopLanguageid = ShopLanguageButton.find('button').attr('ShopLanguageid');
      ShopLanguagelisttable.row( $(ShopLanguageButton).parents('tr') ).remove().draw();

      ShopLanguagelistaddedtable.row.add( [
        ''+ShopLanguageName+'',
        '<button  ShopLanguageid="'+ShopLanguageid+'"  onclick="RemoveShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguageName+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
        
    });
    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());

  }

  function RemoveShopLanguageAll(){
    
    $("#ShopLanguagelistadded > tbody > tr").each(function () {
      ShopLanguageName = $.trim($(this).find('td').eq(0).text());
      ShopLanguageButton = $(this).find('td').eq(1);
      ShopLanguageid = ShopLanguageButton.find('button').attr('ShopLanguageid');
      ShopLanguagelistaddedtable.row( $(ShopLanguageButton).parents('tr') ).remove().draw();
      
      ShopLanguagelisttable.row.add( [
        ''+ShopLanguageName+'',
        '<button  ShopLanguageid="'+ShopLanguageid+'"  onclick="addShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguageName+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
        ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');


    });
    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());
    

  }

  function addShopLanguage( instance , ShopLanguageid, ShopLanguagename ){
    let ShopLanguagenew = $(instance).attr('ShopLanguageid');
    ShopLanguagelisttable.row( $(instance).parents('tr') ).remove().draw();
    ShopLanguagelistaddedtable.row.add( [
      ''+ShopLanguagename+'',
      '<button  ShopLanguageid="'+ShopLanguageid+'"  onclick="RemoveShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguagename+'\')" class="btn  btn-sm btn-outline-danger" >Remove</button>'
      ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');

      $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
      $('#langaddedcount').text(ShopLanguagelisttable.rows().count());
  }

  function RemoveShopLanguage( instance , ShopLanguageid, ShopLanguagename ){
    let ShopLanguagenew = $(instance).attr('ShopLanguageid');
    ShopLanguagelistaddedtable.row( $(instance).parents('tr') ).remove().draw();
    var selectedShopLanguageName = '';
    var selectedid = '';
    var CountriesArray = <?php echo json_encode($languages); ?>;
    
    $.each( CountriesArray, function( index, value ){
      if(value['languagecode'] == ShopLanguageid){
        ShopLanguagelisttable.row.add( [
          ''+ShopLanguagename+'',
          '<button  ShopLanguageid="'+ShopLanguageid+'"   onclick="addShopLanguage(this,\''+ShopLanguageid+'\',\''+ShopLanguagename+'\')" class="btn  btn-sm btn-outline-secondary" >Add</button>'
          ] ).draw( false ).columns().nodes().flatten().to$().addClass('py-1');
      }
    });

    $('#langremovecount').text(ShopLanguagelistaddedtable.rows().count());
    $('#langaddedcount').text(ShopLanguagelisttable.rows().count());

  }

  
  function saveShopLanguage(){
    var ShopLanguageArray = [];
    var saveShopLanguageids = '';
    $("#ShopLanguagelistadded > tbody > tr").each(function () {
      selectedShopLanguageName = $(this).find('td').eq(1);
      selectedid = selectedShopLanguageName.find('button').attr('ShopLanguageid');
      if(selectedid != undefined){
        ShopLanguageArray.push(selectedid);
      }
      if( ShopLanguageArray.length > 0 ){
        saveShopLanguageids = ShopLanguageArray.join(",");
      }else{
        saveShopLanguageids = "";
      }
      $("#ShopLanguage-model").modal('hide');
      $("#addShopLanguage_counter").val(saveShopLanguageids);
      $("#shop_languagecount_count").text(ShopLanguageArray.length);

      if(ShopLanguageArray.length > 0){
        $('#language_count').text(ShopLanguageArray.length);
      }else{
        $('#language_count').text('');
      }
      
    });    

  }

  // End of Customer Language ................
  function openLinkTabModel( modelid  ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    }); 
    return true;
  };

  var collectionidsValue = '';
  var countervalue   = 0;
  
  function openCollectionModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    }); 

    filterType = 1;
    $("#collection_selection").text( filterType == 1 ? "All Collections" :"Selected Collections" );
    collectionidsValue = $("#addCollection_"+counter).val();
    countervalue = counter;
    if( collectionidsValue != '' ){
      var nows = collectionidsValue;
      arr =  nows.split(',');
      read_data = unique4(arr);
      pro_uni_data = read_data.join(",");
      collectionidsValue = pro_uni_data;
    }
    
    getCollections('',"", collectionidsValue );
    // $('#searchItemCollection').val('');
  }

  $('#Collection-model').on('hidden.bs.modal', function (e) {
      var table = $('#collectionlist').DataTable();
      table.clear();
      // table.destroy();
  });

  function unique4(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
  }

  var searchbox = '';
  var filterType = '';
  var table = '';

  function getCollections( param ='', searchbox='' , strProduct='' ){
    $("#Collection-tableDataModel").html('');
    $('#loadingalert').show();
    var page =1;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
            method:"GET",
            url : '{{asset('shopify-collections')}}?page='+page+'&searchbox='+searchbox+'&collections='+collectionidsValue+"&filterType="+filterType,
            dataType: 'html',
            success : function( data ){
              if(data.length < 50){
                $('#loadingalert').hide();
                $('#collection_selection').prop('disabled', true);
                $('#requestalert_collection').show();
                $('#requestmessage_collection').text(data);
                $("#requestalert_collection").delay(2500).hide(0);

              }else{
                $('#Collection-tableDataModel').html( data );
                table = $('#collectionlist').DataTable({
                  // dom: "Bfrtip",
                  dom: "Bfrtip",
                  responsive: true,
                  "paging": true,
                  "pagingType": "numbers",
                  "searching" : true,
                  "ordering": false,
                  "bServerSide":false,
                  "bInfo" : false,
                  "pageLength": 10,
                });
                collection_select_option(1);
                $('#loadingalert').hide();
                // $('#requestalert_text').show();
                // $('#requestmessage_text').text('File uploaded Successfully');
                // $("#requestalert_text").delay(2500).hide(0);

              }
            }
          });
  }

    
    
    // $('.collect_type').on('click',function(){
    function collection_select_option(value){
      var selected_button = 1;
      selected_button = value;
      if((selected_button) == 2 ){
        $("#collection_selection").text( 'Selected Collections' );
          $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
              return $(table.row(dataIndex).node()).hasClass('prime_selected');
            }
          );
          table.draw();
          
      }else{
        $.fn.dataTable.ext.search.pop();
        table.draw();
        $("#collection_selection").text( 'All Collections' );
      }
    };
   
  // $.fn.dataTable.ext.search.push(
      //  function(settings, data, dataIndex) {
      //   var collectionlist = $(table.row(dataIndex).node());  
      //   console.log(collectionlist.find('.btn-outline-danger'));
      //   var newest = $(collectionlist.find('.btn-outline-danger').closest('tr'));
      //     return newest;
      //   }
      // );

  // $("#searchItemCollection").click( function(){
  //   searchbox = $("#searchboxCollection").val().trim();
  //   if (searchbox.length < 3) {
  //     return false;
  //   }
  //   getCollections( '',searchbox ,collectionidsValue );
  // });

  var arrCollectionData = [];
  var objCollectionData = {};

  function addCollections( instance , collectionid,condition='pass'){
    if(condition == 'pass'){
  
      if( objCollectionData[countervalue] ){
        arrCollectionData = objCollectionData[countervalue];
      }else{
        arrCollectionData = [];
        let collectionValue = $("#addCollection_"+countervalue).val();
        if( collectionValue != '' && typeof collectionValue != "undefined"){
          arrCollectionData = collectionValue.split(",");
        }
      }
      let collectionidnew = $(instance).attr('collectionid');
      if( $(instance).attr('added') ){
        $(instance).text('Select');
        $(instance).removeAttr('added');
        $(instance).removeClass('btn-outline-danger');
        $(instance).addClass('btn-outline-secondary');
        $(instance).closest('tr').addClass('prime_not');
        $(instance).closest('tr').removeClass('prime_selected');
        let index = arrCollectionData.indexOf(collectionidnew);
        arrCollectionData.splice(index, 1);
        objCollectionData[countervalue] = arrCollectionData;
      }else{
        $(instance).text('Remove');
        $(instance).attr('added',"1");
        $(instance).removeClass('btn-outline-secondary');
        $(instance).addClass('btn-outline-danger');
        $(instance).closest('tr').addClass('prime_selected');
        $(instance).closest('tr').removeClass('prime_not');

        arrCollectionData.push( collectionidnew );
        objCollectionData[countervalue] = arrCollectionData;
      }

      if( arrCollectionData.length > 0 ){
        var my_array = arrCollectionData;
        arrCollectionData = getUnique(my_array);
      }

    }else{
      objCollectionData = {};
      arrCollectionData = [];
    }
    
    let totalCount = arrCollectionData.length;
    $("#collectionscount_"+countervalue).text( totalCount );
    if( arrCollectionData.length > 0 ){
      collectionidsValue = arrCollectionData.join(",");
    }else{
      collectionidsValue = "";
    }
    $("#addCollection_"+countervalue).val(collectionidsValue);
  }

  function getUnique(array){
      var uniqueArray = [];
      for(i=0; i < array.length; i++){
          if(uniqueArray.indexOf(array[i]) === -1) {
              uniqueArray.push(array[i]);
          }
      }
      return uniqueArray;
  }

  $("#Collection-model").on("hidden.bs.modal", function () {
    $('.productall input:checkbox:checked').removeAttr('checked');
  });

</script>

<script>
  var textColor = $( "#buttonsettingtextcol" ).val();
  var objhighlightStyle     = '';
  var objhighlightSpanStyle = '';
  var highlightStyleLeft    = '';
  var objhighlightTextStyle = { 'white-space': 'normal', 'overflow': 'hidden', 'text-align':'center', 'display':'inline-block' };
  
  
  
  $(function() {
   
  //  $(".highlighttitle").text( updateTextValue( $("#highlighttitle").val() ) );

    $("#editdiv").keyup(function(event){
    
      value =  updateTextValue( $(this).val() );
      var previmg='';
      var nextimg='';
      
     // console.log('innnnnnn')
      setTimeout( function(){

        if($('#editdiv').val() == ''){

          $("#textStyle").html('');
          var imgsrc = $('#icondiv img').attr('src');
          $('#textStyle').html('<img class="img-fluid" src="'+imgsrc+'" style="height:25px;">' );
        
        
        } else {

          var editdivclone= $('#editdiv').val();
          var imgsrc = $('#icondiv img').attr('src');
          $('#textStyle').html('<img class="img-fluid" src="'+imgsrc+'" style="height:25px;">' + editdivclone);


          //$('#editdiv').css('height',$('#textStyle').height()+10);
          $('#hightlightdiv').css('height',$('#textStyle').height()+10);

        }
        
        
      },500);

     
    });

    $("#buttonsettingBgcol").change(function(){
      bgcolor = $(this).val();
      updatehighlightstyle( bgcolor , textColor ,lineHeight, height);
    
    });

    $(".buttonsettingtextcol").change(function(){
      textColor  = $(this).val();
      $("#highlightStyle").css('color',textColor );
      $("#mobihighlightStyle").css('color',textColor);
    });
  
  
    $(document).on("keypress", "#highlighttitle", function(e){
      
      
        
        if(e.which == 13){
            var inputVal = $(this).val();

            setTimeout( function(){

              //$("#textStyle").append('<div></br></div>');
      
            }, 500);
            
        }
     
    });
  
  });

  var lineHeight = 16;
  var height=16;

  function updateTextValue( value ){
    
    return value;
  }
  
  // =================================== add rule ===========================================================

  function addRule( instance , counter='' ){

   // console.log($(instance).val(''));
    let value = $(instance).val();
    if(value == 'product'){

     // console.log('in');

      $('.ExProduct').css('display', 'none');
   // $('.ExProduct').setAttribute("style", "display: none !important;");
      $('#addExProduct').val('');
    }else{
      $('.ExProduct').css('display', 'block');
    }
    //console.log(value);
    let htmlrule = `<td>
                      <div class="row mx-0 px-0">
                        <div class="mx-1">
                          <select name="rule[${counter}][conditiontype]" class="form-control" onchange="addRule(this,${counter});">
                            <option ${value =='all_products'?'selected':''} value="all_products">All Products</option>
                            <option ${value =='product'?'selected':''} value="product" >Select Products</option>
                            <option ${value =='product_type'?'selected':''} value="product_type">Product Type</option>
                            <option ${value =='product_price'?'selected':''} value="product_price">Product Price</option>
                            <option ${value =='product_vendor'?'selected':''} value="product_vendor">Product Vendor</option>
                            <option ${value =='title'?'selected':''} value="title">Product Title</option>
                            <option ${value =='weight'?'selected':''} value="weight">Product Weight</option>
                            <option ${value =='collection'?'selected':''} value="collection">Collection</option>
                                                       
                            <option ${value =='saleprice'?'selected':''} value="saleprice" >Sale price</option>
                            <option ${value =='inventory'?'selected':''} value="inventory" >Inventory</option>
                            <option ${value =='tags'?'selected':''} value="tags" >Product Tags</option>
                            <option ${value =='createddate'?'selected':''} value="createddate">Product Create date</option>
                            <option ${value =='publishdate'?'selected':''} value="publishdate">Product Published date</option>
                            <option ${value =='product_metafield'?'selected':''} value="product_metafield">Product Metafield</option>
                            <option ${value =='product_option'?'selected':''} value="product_option">Variants</option>
                            <option ${value =='customer'?'selected':''} value="customer">Customer</option>
                            <option ${value =='customer_tag'?'selected':''} value="customer_tag">Customer Tags</option>
                          </select>
                        </div>`;

                        switch( $(instance).val() ) {
      case 'collection':
        let customCollections = @json( $customCollections );
        let collectionDrop = '';
        if( customCollections != false ){
          customCollections.forEach( function( element ){
            //console.log( element );
            collectionDrop +=  `<li class="ml-2"> <span>
                    <input onchange="countCheck(${counter});" name="rule[${counter}][conditiontype][collection][]" type="checkbox" class="collection_listinput" value="${element.id}" id="collection"> ${element.title}
                    </span>
                    </li>`;
          });
        }
        
        htmlrule += ` <div class="mx-1 mt-2"> </div>
                      <div class="mx-0 px-1 inventorytype" >
                        <select name="rule[${counter}][conditiontype][collection][type]" class="form-control"  >
                          <option  value="1">Equals</option>
                          <option  value="2" >Excludes</option>
                        </select>
                      </div>
                    <div class="mx-1">
                        <btn  onclick="openCollectionModel('#Collection-model',${counter});"  type="text" class="btn btn-secondary selectproduct" >Select collections</btn>

                        <input type="hidden"  name="rule[${counter}][conditiontype][collection][value]" type="text" class="form-control " id="addCollection_${counter}" placeholder="Selected Collections" value="">
                    </div>
                      <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                        <i><span id="collectionscount_${counter}">0</span> selected</i>
                      </span>
                        
                      `;
        break;

      case 'page_type':
        
        htmlrule += ` <span class="mt-2 mx-1">equals</span>
                      <div class="mx-1">
                      <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          -- Select --
                          <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" id="page_list" style="max-height: 300px;overflow: auto;">
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="product" > Product Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="index" > Index Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="collection" > Collection Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="collection" > Collection Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="article" > Article Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="blog" > Blog Page
                              </span>
                          </li>
                          <li class="ml-2"> <span>
                              <input onchange="countPageCheck(${counter});" name="rule[${counter}][conditiontype][page_type][]" type="checkbox" class="collection_listinput" value="cart" > Cart Page
                              </span>
                          </li>
                          </ul>
                          <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                          <i><span id="pagescount">0</span> selected</i>
                          </span>
                      </div>
                      </div>`;
        break;
      
      case 'product':
      
        htmlrule += `<span class="mt-2 mx-1">equals</span>
                      <div class="mx-1">
                        <btn  onclick="openProductModel('#blacklistProduct-model',${counter});"  type="text" class="btn btn-secondary" >Select products</btn>
                        <input type="hidden"  name="rule[${counter}][conditiontype][product][]" type="text" class="form-control" id="addProduct_${counter}" placeholder="Selected products " value="">
                        <input type="hidden"  name="shopproductmeta" type="text" class="form-control" id="shopproductmeta" placeholder="Selected products " value="">
                      </div>
                      <a href="javascript:void(0);" class="mt-2 mx-1" onclick="openProductModel('#blacklistProduct-model',${counter},2);" >
                        <span class="text-primary pointer small">
                          <i><span id="productcount_${counter}">0</span> selected</i>
                        </span>
                      </a>
                      `;
          break;
      case 'inventory':
            htmlrule += `<span class="mt-2 mx-1">is</span>
                  <div class="mx-1" >
                    <select name="rule[${counter}][conditiontype][inventory][type]" class="form-control" onchange="inventoryType(this,${counter});">
                      <option  value="1">between</option>
                      <option value="2" >in stock</option>
                      <option value="3" >out of stock</option>
                      <option value="4" >out of stock but continue selling</option>
                    </select>
                  </div>

                  <div class="col mx-0 px-0 inventorytype" >
                    <input type="number" min="1" max="100" name="rule[${counter}][conditiontype][inventory][start]" value="" class="form-control " required>
                  </div>
                  <span class="inventorytype mt-2 mx-1">and </span>
                  <div class="col mx-0 px-0 inventorytype" >
                    <input type="number" min="1"  name="rule[${counter}][conditiontype][inventory][value]" class="form-control" onkeypress="return (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57) ? false : true )">
                  </div>
                </div>`;
        break;
      case 'tags':
            htmlrule += ` <div class="mx-1 mt-2"> </div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][tags][type]" class="form-control"  >
                              <option  value="1">Equals To</option>
                              <option  value="2" >Excludes To</option>
                            </select>
                          </div>
                          <div class="mx-0 col">
                            <input data-role="tagsinput" name="rule[${counter}][conditiontype][tags][value]" size="1" type="text" class=" form-control tags tags" id="tags" placeholder="Enter product tags" value="">
                          </div>`;
        break;
        case 'customer_tag':
            htmlrule += ` <div class="mx-1 mt-2"> </div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][customer_tag][type]" class="form-control"  >
                              <option  value="1">Equals To</option>
                              <option  value="2" >Not Equals To</option>
                            </select>
                          </div>
                          <div class="mx-0 col">
                            <input data-role="customer_taginput" name="rule[${counter}][conditiontype][customer_tag][value]" size="1" type="text" class=" form-control customer_tag tags" id="customer_tag" placeholder="Enter customer tags" value="">
                          </div>`;
        break;
      case 'createddate':
            htmlrule += ` <span class="mt-2 mx-1"> is within the past </span>
                          <div class="mx-0 px-1 inventorytype " >
                            <input type="number" min="0" max="10000" name="rule[${counter}][conditiontype][createddate]" value="180" class="form-control">
                          </div>
                          <span class="mt-2 mx-1"> days </span>`;
        break;
      case 'publishdate':
            htmlrule += ` <span class="mt-2 mx-1"> is within the past </span>
                          <div class="mx-0 px-1 inventorytype " >
                            <input type="number" min="0" max="10000" name="rule[${counter}][conditiontype][publishdate]" value="180" class="form-control">
                          </div>
                          <span class="mt-2 mx-1"> days </span>`;
        break
      case 'all_products':
            htmlrule += ` <span class="mt-2 mx-1"> Highlight will be displayed on all products </span>
                          <div class="mx-0 px-1 col inventorytype " >
                            <input type="hidden" name="rule[${counter}][conditiontype][all_products]" value="all_products" class="form-control">
                          </div>
                          <span class="mt-2 mx-1">  </span>`;
        break; 
      case 'title':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 title" >
                            <select name="rule[${counter}][conditiontype][title][type]" class="form-control"  >
                              <option value="1" >Starts With</option>
                              <option value="2" >Ends With</option>
                              <option value="3" >Contains</option>
                              <option value="4" >Not Contains</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 titlediv col" >
                            <input type="text" name="rule[${counter}][conditiontype][title][value]"  class="form-control" >
                          </div>`;
      break;
      case 'product_type':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_type][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Not Equal To</option>
                              <option value="3" >Starts With</option>
                              <option value="4" >Ends With</option>
                              <option value="5" >Contains</option>
                              <option value="6" >Not Contains</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                            <input type="text" name="rule[${counter}][conditiontype][product_type][value]"  class="form-control" >
                          </div>`;
        break;
        case 'product_price':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_price][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Not Equal To</option>
                              <option value="3" >Is Greater Than</option>
                              <option value="4" >Is Less Than</option>
                             
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                            <input type="number" min="1" name="rule[${counter}][conditiontype][product_price][value]"  class="form-control" >
                          </div>`;
        break;
      case 'product_vendor':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_vendor][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Not Equal To</option>
                              <option value="3" >Starts With</option>
                              <option value="4" >Ends With</option>
                              <option value="5" >Contains</option>
                              <option value="6" >Not Contains</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}} col" >
                            <input type="text" name="rule[${counter}][conditiontype][product_vendor][value]"  class="form-control" >
                          </div>`;
        break;
        case 'product_option':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][product_option][type]" class="form-control"  >
                            <option value="1">Product Has Variants</option>
                              <option value="2" >Product Does Not Have Variants</option>
                              
                            </select>
                          </div>`;
        break;
        case 'customer':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][customer][type]" class="form-control"  >
                              <option value="1">Is Logged In</option>
                              <option value="2" >Not Logged In</option>
                              
                            </select>
                          </div>`;
        break;
      case 'weight':
            htmlrule += `<div class="mx-1 mt-2"></div>
                          <div class="mx-0 px-1 inventorytype" >
                            <select name="rule[${counter}][conditiontype][weight][type]" class="form-control"  >
                              <option value="1">Equal To</option>
                              <option value="2" >Less Than</option>
                              <option value="3" >Greater Than</option>
                            </select>
                          </div>
                          <span class="mt-2 mx-1">  </span>
                          <div class="mx-1 inventorytype {{$inventoryHide}}" >
                            <input type="number" min="0" name="rule[${counter}][conditiontype][weight][value]"  class="form-control" >
                          </div><span class="mt-2 mx-1"> grams </span>`;
        break;    
           
       case 'saleprice':
            htmlrule += ` <span class="mt-2 mx-1"> is between </span>
                          <div class="col mx-0 px-0 " >
                            <input type="number" min="1" max="100" name="rule[${counter}][conditiontype][saleprice][start]" value="" class="form-control" required>
                          </div>
                          <span class="mt-2 mx-1">  % and </span>
                          <div class="col mx-0 px-0 " >
                            <input type="number" min="1" max="100" name="rule[${counter}][conditiontype][saleprice][end]" value="" class="form-control" required>
                          </div> <span class="mt-2 mx-1"> %</span>
                          <input type="hidden" name="rule[${counter}][conditiontype][saleprice]" value="" class="form-control" >`;
        break;

        case 'product_metafield':
            htmlrule += ` <span class="mt-2 mx-1"><a href="#" data-toggle="modal" data-target="#metafieldModal">name</a></span>
                          <div class="col mx-0 px-0 productmetafield metanameval" >
                            <input type="text" min="1" max="100" name="rule[${counter}][conditiontype][productmetafield][metaname]" value="" class="form-control" required>
                          </div>
                          <div class="mx-0 px-0 productmetafield " >
                            <select name="rule[${counter}][conditiontype][productmetafield][metacondition]" id="metafieldcond" class="form-control metafieldcond"  >
                              <option value="1">Equals</option>
                              <option value="2" >Found</option>
                              <option value="3" >Not Found</option>
                            </select>
                          </div>
                          <div class="col mx-0 px-0 productmetafield metafieldkey" id="metafieldkey" >
                          <input type="text" min="1" max="100" name="rule[${counter}][conditiontype][productmetafield][metaval]" value="" class="form-control" required>
                          </div>`;
        break;
    }

    htmlrule +=`</div></td>`;

    htmlrule += ` <td>
                    <div class="text-center mt-2">
                      <a onclick="deleteRule('${"#appendHtml"+counter}');" href="javascript:void(0)">
                        <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                      </a>
                    </div>
                  </td>`;
    if( counter == 0 ){
      counter = '';
    }
    $("#appendHtml"+counter).html( htmlrule );
    $('.tags').tagsinput({
      tagClass: function(item) {
        return 'highlight highlight-primary';
      }
    });
  }

  function inventoryType( instance , index ){
    let id = index == 0 ? '':index;
    id = '#appendHtml'+id;
    $(instance).parent().closest('div').removeClass('mx-1' );
    $(id+" .inventorytype").removeClass('d-none');
    if( $(instance).val() == '1' ){
      $(instance).parent().closest('div').addClass('mx-1' );
      $(id+" .inventorytype").show();
    }else{
      $(instance).parent().closest('div').addClass('mx-1' );
      $( id+" .inventorytype").hide();
    }
  }

  var intCounterRule = '{{!empty( $displayrules ) ? count( $displayrules ) -1 :0 }}';
  // var appendCounter = ''; 

  function addRuleHtml( ){
    intCounterRule++;
  
    let htmlContainer = `<tr id="appendHtml${intCounterRule}" > </tr>`;
    if( $("table #appendHtmlAll").children('tr').length > 5 ){
      return false;
    }
    $("table #appendHtmlAll").append( htmlContainer );
    // $(  htmlContainer ).insertAfter('');
    var defaultHtmlRule= `<td>
                            <div class="row mx-0 px-0">
                              <div class="mx-1">
                                <select class="form-control" name="rule[${intCounterRule}][conditiontype]" index=${intCounterRule} onchange="addRule(this,${intCounterRule});">
                                  <option selected value="all_products">All Products</option>
                                  <option value="product" >Select Products</option>
                                  <option value="product_type">Product Type</option>
                                  <option value="product_price">Product Price</option>
                                  <option value="product_vendor">Product Vendor</option>
                                  <option value="title">Title</option>
                                  <option value="weight">Weight</option>
                                  <option value="collection">Collection</option>
                                  
                                  <option value="saleprice" >Sale price</option>
                                  <option value="inventory" >Inventory</option>
                                  <option value="tags" >Tags</option>
                                  <option value="createddate">Product Created Date</option>
                                  <option value="publishdate">Product Published Date</option>
                                  <option value="product_metafield">Product Metafield</option>
                                  <option value="product_option">Variants</option>
                                  <option value="customer">Customer</option>
                                  <option value="customer_tag">Customer Tag</option>
                                </select>
                              </div>
                              <span class="mt-2 mx-1"> Highlight will be displayed on all products </span>
                              <div class="mx-0 px-1 col inventorytype " >
                                <input type="hidden" name="rule[${intCounterRule}][conditiontype][all_products]" value="all_products" class="form-control">
                              </div>
                              <span class="mt-2 mx-1"></span>
                            </div>
                          </td>
                          <td>
                            <div class="text-center mt-2">
                              <a onclick="deleteRule('${"#appendHtml"+intCounterRule}');" href="javascript:void(0)">
                                <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                              </a>
                            </div>
                          </td>`;
    $("#appendHtml"+intCounterRule).html(defaultHtmlRule );
  }

  function deleteRule( rulehtmlId ){
    // intCounterRule = ( intCounterRule-1 );
    //alert( intCounterRule );
    if( $("table #appendHtmlAll").children('tr').length == 1 ){
      return false;
    }
    $(rulehtmlId).remove();
  }

  function addtohighlight(instance){

    var imgobj = $(instance).find('img').clone();
    imgobj= imgobj.removeAttr('onclick');
    imgobj = imgobj.css('margin','');
    imgobj = imgobj.css('cursor','none');
    imgobj = imgobj.css('height','25px');
    //imgobj = imgobj.css('height','25px');
    //imgobj = imgobj.css('width','25px');
    imgsrc = imgobj.attr('src');
    //console.log(imgsrc);

    
    $('.close').trigger('click');
    $('#textStyle').html(imgobj);
    $('#textStyle').append($('#editdiv').val());
    $('#icondiv img').attr('src', imgsrc);
    $('#hightlightdiv').css('height',$('#textStyle').height()+10);
    
  }

  var mytexttiny = '';
  var modalsize = '';
  function saveModalContent(){
    modalsize = $('#modalsize').val();
    mytexttiny = tinymce.get('mytextarea').getContent();
    $('#Linktab-model').modal('toggle');
  }

  function validateDate(){
    var realvalue = $('#editdiv').val();
    
    if( realvalue.indexOf("[[today") != -1  ){
      var pos = realvalue.indexOf("[[today");
      var pos2 = realvalue.indexOf("]]");
      var startres = realvalue.slice(pos+8, pos2);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );

      if( realvalue.indexOf("[[today") != -1  ){
        var pos3 = realvalue.indexOf("[[today");
        var pos4 = realvalue.indexOf("]]");
        var endres = realvalue.slice(pos3+8, pos4);
        realvalue = realvalue.replace("[[today,", "" );
        realvalue = realvalue.replace("]]", "" );
        if(startres >= endres){
          $('#requestalert').show();
          $('#requestmessage').text('Delivery end date should be bigger than start date');
          $("#requestalert").delay(2500).hide(0);
          return "wrong";
        }

      }

    }
    return true;
  }

  function saveRule( id ){
  
    var highlightlink = $('#highlightlink').val();

    if(mytexttiny == ''){
      var mytext = <?PHP echo  (!empty($highlight['highlightdescription']) ? json_encode($highlight['highlightdescription']) : '""'); ?>;
      var myhighlightid = <?PHP echo ( isset($highlight['producthighlightid'] ) ? $highlight['producthighlightid'] : '""'); ?>;
      if(myhighlightid != '' && mytexttiny == ''){
        mytext = tinymce.get('mytextarea').getContent();
      }
    }else{
      var mytext = mytexttiny;
    }

    if(modalsize == ''){
      var mymodalsize = <?PHP echo ( isset($highlight['modalsize'])   ? $highlight['modalsize'] : '1'); ?>;
    }else{
      var mymodalsize = modalsize;
    }

  var rulesList = ['product','saleprice','collection','product_type','product_price','product_vendor','title','weight','tags','inventory','customer_tag','product_metafield'];

  for(var k = 0; k <= intCounterRule; k++){
    var ruletype = $('[name="rule['+k+'][conditiontype]"]').val();

    for(var m=0; m<rulesList.length; m++){
      var rulename = rulesList[m];

      if(rulename == ruletype){
        var rulevalue = '';
        if(rulename == 'customer_tag'){
          rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+']"]').val();
        }else if(rulename == 'tags'){
          rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+']"]').val();
        }else if(rulename == 'product'){
          rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][]"]').val();
        }else if(rulename == 'collection'){
          rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][]"]').val();
        }
        // else if(rulename == 'page_type'){
        //   if(parseInt($('#pagescount').text()) < '1'){
        //     rulevalue = '';
        //   }else{
        //     rulevalue = '1';
        //   }
        // }
        else if(rulename == 'inventory'){
          var inventorytype = $('[name="rule['+k+'][conditiontype]['+rulename+'][type]"]').val();
          if(inventorytype == '1'){
            rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][value]"]').val();
            if(rulevalue < '1'){
              rulevalue = '';
            }
          }else{
            rulevalue = '1';
          }
        }else if(rulename == 'saleprice'){
          var salepricestart = $('input[name="rule['+k+'][conditiontype]['+rulename+'][start]"]').val();
          var salepriceend = $('input[name="rule['+k+'][conditiontype]['+rulename+'][end]"]').val();
          if(salepricestart == ''){
            rulevalue = '';
          }else if(salepriceend == ''){
            rulevalue = '';
          }else{
            $('input[name="rule['+k+'][conditiontype]['+rulename+']"]').val(salepricestart+','+salepriceend);
            rulevalue = '1';
          }
        }else if(rulename == 'product_metafield'){
          var metaCondition = $('input[name="rule['+k+'][conditiontype][productmetafield][metacondition]"]').val();
          var metaname = $('input[name="rule['+k+'][conditiontype][productmetafield][metaname]"]').val();
          if(metaname == ''){
            rulevalue = '';
          }else{
            rulevalue = '1';
          }
          if(metaCondition == '1'){
            var metaval = $('input[name="rule['+k+'][conditiontype][productmetafield][metaval]"]').val();
            if(metaval == ''){
              rulevalue = '';
            }
          }
        }else{
          rulevalue = $('input[name="rule['+k+'][conditiontype]['+rulename+'][value]"]').val();
        }

        if(rulevalue == ''){
          $('#requestalert').show();
          $('#requestmessage').text('Missing data in display conditions');
          $("#requestalert").delay(2500).hide(0);
          return false;
        }
        break;
      }
    }
  }

  var validate = validateDate();
  if(validate=='wrong'){
    return false;
  }
    
    let formData = $(id).serialize();//+ "&image="+image;
    var highlightpreview = encodeURIComponent($('#editdiv').val());
    var highlightimg = $("#icondiv img").attr('src');  
    //var highlightimg = $('<div>').append($("#icondiv img").clone()).html();  

   // var highlightpreview = $('#textStyle').html();
    //highlightpreview = highlightpreview.replace('&nbsp;',' ');
   // console.log(highlightpreview);
    let appearance = {
                      'desktop':{
                       // 'spanStyleleft': $('.spanStyleleft').attr('style'),
                       // 'textStyle'    : $('#textStyle').attr('style'),
                       // 'spanStyle'    : $('#spanStyle').attr('style'),
                        'highlightStyle'   : $('#highlightStyle').attr('style'),
                        'title'        : $('#textStyle').text(),
                      },
                      'mobile':{
                       // 'spanStyleleft': $('.spanStyleleft').attr('style'),
                       // 'mobitextStyle'    : $('#mobitextStyle').attr('style'),
                       // 'mobispanStyle'    : $('#mobispanStyle').attr('style'),
                       // 'mobihighlightStyle'   : $('#mobihighlightStyle').attr('style')
                      },
                    };
    formData +='&appearance='+JSON.stringify( appearance );

    

    formData +='&highlightlink='+highlightlink;
    formData +='&highlighttitle='+highlightpreview;
    formData +='&highlightimg='+encodeURIComponent(highlightimg);
    formData +='&modalsize='+mymodalsize;
    formData +='&highlightdescription='+encodeURIComponent(mytext);
    // console.log(formData);
    // return false;

    $('.savebtn').prop('disabled', true);  
    $('#loadingalert').show();
    //console.log( formData );
    //return false;
 
    $.ajax( {
      url:"{{asset('highlightrule')}}",
      method:'POST',
      data :formData,
      success:function( response ){
       
        let productHtml= '';
        $('#loadingalert').hide();
        $('#requestmessage').text(response.msg);

        //console.log(response.submitType);
        if( response.submitType == 'created' ){
           setTimeout(function(){

            //  $('.savebtn').prop('disabled', true);  
            location.href = '{{asset('highlights')}}';
            $('.savebtn').prop('disabled', false);
          },2000);
        
        } else {

          $('.savebtn').prop('disabled', false);
        }
        
        toggleAlert();
      }
    });
  }

  function getCollectionList( instance , index ){
    return false;
    $.ajax( {
      url:"{{asset('getcollection')}}",
      method:'GET',
      data :{'collection':''},
      success:function( response ){
        let productHtml= '';
        $('#loadingalert').hide();
        $('#requestmessage').text(response.msg);
        if( response.status == true ){
          setTimeout(function(){
            location.reload();
          },2000);
        }
        toggleAlert();
      }
    });
  }

  function countCheck( index  ){
    index = index == 0 ? '': index;
    let lengthCount = $("#appendHtml"+index+ ' #collection_list :checked').length;
    $( "#appendHtml"+index+ ' #collectionscount').text( lengthCount );
  }

  function countPageCheck( index  ){
    index = index == 0 ? '': index;
    let lengthCount = $("#appendHtml"+index+ ' #page_list :checked').length;
    $( "#appendHtml"+index+ ' #pagescount').text( lengthCount );
  }

  function highlightCssConfig( bcolor , tcolor , lineHeight , height , textsize, margintop= 0, marginright= 0, marginbottom= 0, marginleft=0, shape_padding='medium' ){
    
    var highlight_padding = 'padding:0.5em 1em';
    var highlight_text = $('#highlight_text').prop('checked');
    var highlight_image = $('#highlight_image').prop('checked');
    var highlight_type = 'text';
    if(highlight_text == true){
      highlight_type = 'text';
    }else if(highlight_image == true){
      highlight_type = 'image';
//      set line height 0 for image highlights
      lineHeight = 0;
      tcolor = 'transparent';
      bcolor = 'transparent';
    }

    if(shape_padding == 'small'){
      highlight_padding = 'padding:0.25em 0.5em';
    }

    var InText =  {
            '11':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },

            '21':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '12':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;border-top-right-radius:1em;border-bottom-right-radius:1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '22':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
          

            '1':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;padding-left: 12px;padding-right: 12px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';font-size: 16px;position: relative;max-width: 100%;'+highlight_padding,
                'spanStyle':'',
                'spanStyleleft':''
              },
            '2':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 2em;border-bottom-left-radius: 2em;border-top-right-radius: 2em;border-bottom-right-radius: 2em;'+highlight_padding,
                'spanStyle':'',
                'spanStyleleft':''
            },
            '3':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';font-size: 16px;position: relative;max-width: 100%;border-top-left-radius: 0.3em;border-bottom-left-radius: 0.3em;border-top-right-radius: 0.3em;border-bottom-right-radius: 0.3em;'+highlight_padding,
                'spanStyle':'',
                'spanStyleleft':''
            }, 
                  
          };

    return InText;
    

  }

  function openDynamicVariableModel( modelid ){
    $(modelid).modal({
          backdrop: 'static',
          keyboard: false
    });
  }

  function updatehighlightstyle( bcolor , tcolor ,lineHeight,height ){
   
    var highlight_text = $('#highlight_text').prop('checked');
    var highlight_image = $('#highlight_image').prop('checked');
    var highlight_type = 'text';
    if(highlight_text == true){
      highlight_type = 'text';
    }else if(highlight_image == true){
      highlight_type = 'image';
    }
    
    let highlightStyle  = $("#highlightshape").val();
    let indexCss        = highlightStyle;

    let textsize        = $("#highlighttextsize").val();
    let highlightBorderSize =  $("#highlightbordersize").val();
    let highlightBorderStyle =  $("#highlightborderstyle").val();
    
    height = objTextSizeMapper[height];
    lineHeight = objLineHeightMapper[lineHeight];

    if( highlightBorderSize != "" ){
      highlightBorderSize = highlightBorderSize.replace("px");
      highlightBorderSize = parseInt( highlightBorderSize );
      highlightBorderSize = highlightBorderSize*2;
      height += highlightBorderSize;
    }

    let margintop  = $("#highlightleftStyleMargintop").val();
    let marginright  = $("#highlightleftStyleMarginright").val();
    let marginbottom  = $("#highlightleftStyleMarginbottom").val();
    let marginleft  = $("#highlightleftStyleMarginleft").val();

    let shape_padding  = $("#highlightinternalpadding").val();
    
    let objCsshighlight     = highlightCssConfig( bcolor, tcolor, lineHeight, height, textsize, margintop, marginright, marginbottom, marginleft, shape_padding);
    // let objCsshighlight     = highlightCssConfig( bcolor , tcolor ,lineHeight ,height,textsize );
    let objhighlightStyle     = objCsshighlight[indexCss]['highlightStyle'];
    let objhighlightSpanStyle = objCsshighlight[indexCss]['spanStyle'];
    let highlightStyleLeft    = objCsshighlight[indexCss]['spanStyleleft'];


    $("#highlightStyle").attr('style', objhighlightStyle);
    $("#textStyle").css( objhighlightTextStyle );
    $("#spanStyle").attr('style', objhighlightSpanStyle);
    $("#highlightStyle").css('font-size',textsize);
    $(".spanStyleleft").attr( 'style',highlightStyleLeft );
     updatehighlightGradientwithBorderShadow("desktop");
    
    //reset image bgcolor, color and textsize
    if(highlight_type == 'image')
    {
         $("#highlightStyle").css('background-color','transparent');
          $("#highlightStyle").css('color','transparent');
           $("#highlightStyle").css('font-size',0);
    }
    
  }



  var exproductidsValue = '';
  // var excountervalue   = 0;

  function openExProductModel( modelid ,counter=0,selected=1 ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    });
    filterType = selected;
    $("#Exidtype_selection").text( filterType == 1 ? "All Products" :"Selected Products" );
    exproductidsValue = $("#addExProduct").val();
   
    getExProducts('',"", exproductidsValue );
    $('#Exsearchbox').val('');
    // $('#searchinputicon').val('');
  }

  function getExProducts( param ='', searchbox='' , strProduct='' ){
    $("#ExtableDataModel").html('');
    $('#loadingalert').show();
    var page =1;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
            method:"GET",
            url : '{{asset('shopify-exproducts')}}?page='+page+'&searchbox='+searchbox+'&products='+exproductidsValue+"&filterType="+filterType,
            dataType: 'html',
            success : function( data ){
              $('#loadingalert').hide();
              $('#ExtableDataModel').html( data );
              Producttable = $('#exproductlist').DataTable({
                  dom: "Bfrtip",
                  responsive: true,
                  "paging": true,
                  "pagingType": "numbers",
                  "searching" : false,
                  "ordering": false,
                  "bServerSide":false,
                  "bInfo" : false,
                  "pageLength": 10,
                });
            }
          });
  }

  $('.modal-content').keypress(function(e){

    if($(this).closest('#iconModal').length == 0){
      if(e.which == 13) {
        $("#filterModal").modal("hide");
        searchbox = $("#Exsearchbox").val().trim();
        getExProducts( '',searchbox ,exproductidsValue );
      }
    }
  });

  $("#exsearchItem").click( function(){
    searchbox = $("#Exsearchbox").val().trim();
    if (searchbox.length < 3) {
      return false;
    }
    getExProducts( '',searchbox ,exproductidsValue );
  });

  // function getNextList( param ){
  //   getExProducts( param , searchbox , exproductidsValue);
  // }

  var arrExProductData = [];
  var objExProductData = {};

  function addExProduct( instance , productid ){
    if( objExProductData.length > 0){
      arrExProductData = objExProductData;
    }else{
      
      arrExProductData = [];
      let produtValue = $("#addExProduct").val();
      if( produtValue != ''){
        arrExProductData = produtValue.split(",");
      }
    }
    
    let productidnew = $(instance).attr('productid');
    if( $(instance).attr('added') ){
      $(instance).text('Select');
      $(instance).removeAttr('added');
      $(instance).removeClass('btn-outline-danger');
      $(instance).addClass('btn-outline-secondary');
      let index = arrExProductData.indexOf(productidnew);
      arrExProductData.splice(index, 1);
      objExProductData = arrExProductData;
    }else{
      
      $(instance).text('Remove');
      $(instance).attr('added',"1");
      $(instance).removeClass('btn-outline-secondary');
      $(instance).addClass('btn-outline-danger');
      // $("#successmark-"+productidnew).show();
      arrExProductData.push( productidnew );
      objExProductData = arrExProductData;
    }
    let totalCount = arrExProductData.length;
    $("#Exproductcount").text( totalCount );
    if( arrExProductData.length > 0 ){
      exproductidsValue = arrExProductData.join(",");
    }else{
      exproductidsValue = "";
    }
    $("#addExProduct").val(exproductidsValue);
  }

  $(".exidtype").click( function(){
    filterType = $(this).attr('value');
    $("#Exidtype_selection").text( $(this).text() );
    getExProducts('',"", exproductidsValue );
  });

  
  var data ={};
 


  var productidsValue = '';
  var countervalue   = 0;

  function openProductModel( modelid ,counter='',selected=1 ){
    $(modelid).modal({
        backdrop: 'static',
        keyboard: false
    });
    filterType = selected;
    $("#idtype_selection").text( filterType == 1 ? "All Products" :"Selected Products" );
    productidsValue = $("#addProduct_"+counter).val();
    countervalue = counter;
    getProducts('',"", productidsValue );
    $('#searchbox').val('');
    $('#searchinputicon').val('');
  }

  var searchbox = '';
  var filterType = '';
  var Producttable = '';

  function getProducts( param ='', searchbox='' , strProduct='' ){
    $("#tableDataModel").html('');
    $('#loadingalert').show();
    var page =1;
    if( param != ''){
      let params = (new URL(param)).searchParams;
      page = params.get("page");
    }
    $.ajax({
            method:"GET",
            url : '{{asset('shopify-products')}}?page='+page+'&searchbox='+searchbox+'&products='+productidsValue+"&filterType="+filterType,
            dataType: 'html',
            success : function( data ){
              $('#loadingalert').hide();
              $('#tableDataModel').html( data );
              Producttable = $('#productlist').DataTable({
                  dom: "Bfrtip",
                  responsive: true,
                  "paging": true,
                  "pagingType": "numbers",
                  "searching" : false,
                  "ordering": false,
                  "bServerSide":false,
                  "bInfo" : false,
                  "pageLength": 10,
                });
            }
          });
  }

  $('.modal-content').keypress(function(e){

    if($(this).closest('#iconModal').length == 0){
      if(e.which == 13) {
        $("#filterModal").modal("hide");
        searchbox = $("#searchbox").val().trim();
        getProducts( '',searchbox ,productidsValue );
      }
    }
  });

  $("#searchItem").click( function(){
    searchbox = $("#searchbox").val().trim();
    if (searchbox.length < 3) {
      return false;
    }
    getProducts( '',searchbox ,productidsValue );
  });

  function getNextList( param ){
    getProducts( param , searchbox , productidsValue);
  }

  var arrProductData = [];
  var objProductData = {};

  function addProduct( instance , productid ){
    if( objProductData[countervalue] ){
      arrProductData = objProductData[countervalue];
    }else{
      arrProductData = [];
      let produtValue = $("#addProduct_"+countervalue).val();
      if( produtValue != ''){
        arrProductData = produtValue.split(",");
      }
    }
    let productidnew = $(instance).attr('productid');
    if( $(instance).attr('added') ){
      $(instance).text('Select');
      $(instance).removeAttr('added');
      $(instance).removeClass('btn-outline-danger');
      $(instance).addClass('btn-outline-secondary');
      let index = arrProductData.indexOf(productidnew);
      arrProductData.splice(index, 1);
      objProductData[countervalue] = arrProductData;
    }else{
      $(instance).text('Remove');
      $(instance).attr('added',"1");
      $(instance).removeClass('btn-outline-secondary');
      $(instance).addClass('btn-outline-danger');
      // $("#successmark-"+productidnew).show();
      arrProductData.push( productidnew );
      objProductData[countervalue] = arrProductData;
    }
    let totalCount = arrProductData.length;
    $("#productcount_"+countervalue).text( totalCount );
    if( arrProductData.length > 0 ){
      productidsValue = arrProductData.join(",");
    }else{
      productidsValue = "";
    }
  //  console.log( productidsValue );
    $("#addProduct_"+countervalue).val(productidsValue);
    $("#shopproductmeta").val(productidsValue);
  }

  $(".pagination").addClass('justify-content-center');

  $("#blacklistProduct-model").on("hidden.bs.modal", function () {
    $('.productall input:checkbox:checked').removeAttr('checked');
  });

  function replaceSpecialSym( value ){
    value = value.replace("\{\{","");
    value = value.replace("\}\}","");
    value = value.replace("|","");
    value = value.replace("money","");
    value = value.replace("round","");
    value = value.replace("%",'');
    return value;
  }

  $(".idtype").click( function(){
    filterType = $(this).attr('value');
    $("#idtype_selection").text( $(this).text() );
    getProducts('',"", productidsValue );
  });


  

</script>

@if( !empty( $highlight ) )
  <script type="text/javascript">

    $(function() {
    
      $('.tags').tagsinput({
        tagClass: function(item) {
          return 'highlight highlight-primary';
        }
      });
    });

  </script>
@else
  <script type="text/javascript">
    $(function() {
     // applyhighlightStyle('','Left',$("#highlightleftStyle").val() );
     
    });
  </script>
@endif

</script>
@stop
