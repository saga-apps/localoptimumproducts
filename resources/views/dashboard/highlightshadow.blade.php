<!-- Highlight.Shadow.php -->
<script type="text/javascript">

$(document).ready(function () {

 /*setTimeout(function(){
    var shadowCss = '0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2)';
    //$("#badgeStyle").css('-webkit-box-shadow', shadowCss );

   
    document.getElementById('badgeStyle').style.boxShadow = shadowCss;
    console.log('set');

   }, 2000); */
});
    
var borderBgColor      = '';
// var gradientBgfirst    = '';
// var gradientBgSecond   = '';
var bgcolorType        = '';
	
$(function() {	

    borderBgColor      = $( "#borderBgcolhighlight" ).val();
    // gradientBgfirst    = $( "#gradientBgcol_first" ).val();
    // gradientBgSecond   = $( "#gradientBgcol_second" ).val();
    bgcolorType        = $(".bgcolortype:checked").val();



});


// $("#gradientBgcol_first").spectrum({
//     // color: gradientBgfirst,
//     flat: false,
//     showInput: true,
//     allowEmpty:true,
//     showButtons: false,
//     //className: "full-spectrum",
//     showInitial: true,
//     showPalette: true,
//     showSelectionPalette: true,
//     maxSelectionSize: 10,
//     preferredFormat: "hex",
//     clickoutFiresChange: true,
//     move: function (color) {
//       //gradientBgfirst = color.toHexString();
//      // $("#badgeStyle").css('background-image',color.toHexString() );
//       //$("#mobibadgeStyle").css('color',color.toHexString());
        
//       gradientBgfirst = color.toHexString();
//       $("#gradientBgcol_first").val( gradientBgfirst );
//       updateBadgeGradientwithBorderShadow();
      
//     },
//     change: function(color) {
//         gradientBgfirst = color.toHexString();
//       $("#gradientBgcol_first").val( gradientBgfirst );
//       updateBadgeGradientwithBorderShadow();
//     },
//     hide: function () {
//     },
    
//     localStorageKey: false,
//     color: gradientBgfirst,
//     palette: [
//         ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
//         "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
//         ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
//         "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
//         ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
//         "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
//         "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
//         "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
//         "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
//         "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
//         "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
//         "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
//         "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
//         "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
//     ]
// });



// $("#gradientBgcol_second").spectrum({
//     //color: gradientBgSecond,
//     flat: false,
//     showInput: true,
//     allowEmpty:true,
//     showButtons: false,
//     //className: "full-spectrum",
//     showInitial: true,
//     showPalette: true,
//     showSelectionPalette: true,
//     maxSelectionSize: 10,
//     preferredFormat: "hex",
//     clickoutFiresChange: true,
//     move: function (color ) {
//        gradientBgSecond = color.toHexString();
//        $("#gradientBgcol_second").val( gradientBgSecond );
//        updateBadgeGradientwithBorderShadow();
//     },
//     change: function(color) {
//         gradientBgSecond = color.toHexString();
//         $("#gradientBgcol_second").val( gradientBgSecond );
//         updateBadgeGradientwithBorderShadow();
//     },
//     hide: function () {
//     },
//     localStorageKey: false,
//     color: gradientBgSecond,
//     palette: [
//         ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
//         "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
//         ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
//         "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
//         ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
//         "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
//         "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
//         "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
//         "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
//         "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
//         "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
//         "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
//         "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
//         "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
//     ]
// });


$("#borderBgcolhighlight").spectrum({
    // color: borderBgColor,
    flat: false,
    showInput: true,
    allowEmpty:true,
    showButtons: false,
    //className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxSelectionSize: 10,
    preferredFormat: "hex",
    clickoutFiresChange: true,
    move: function (color) {

       borderBgColor = color.toHexString();
       $("#borderBgcolhighlight").val( borderBgColor );
       updateBadgeGradientwithBorderShadow();

    },
    change: function(color) {
        borderBgColor = color.toHexString();
        $("#borderBgcolhighlight").val( borderBgColor );
        updateBadgeGradientwithBorderShadow();
    },
    hide: function () {
    },
    localStorageKey: false,
    color: borderBgColor,
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
});



function updateBadgeGradientwithBorderShadow( type='' ){

    // var badge_text = $('#badge_text').prop('checked');
    // var badge_image = $('#badge_image').prop('checked');
    
    
    gradientBgfirst    = $( "#gradientBgcol_first" ).val();
    gradientBgSecond   = $( "#gradientBgcol_second" ).val();
    borderBgColor      = $( "#borderBgcolhighlight" ).val();
    
    let borderSize    = $("#highlightbordersize").val();
    let borderstyle    = $("#highlightborderstyle").val();

    
    if( bgcolorType == 2 ){


        // $("#highlightStyle").css('background-image','linear-gradient(to right, '+gradientBgfirst+' , '+gradientBgSecond+')');

        $("#mobibadgeStyle").css('background-image','linear-gradient(to right, '+gradientBgfirst+' , '+gradientBgSecond+')');

        // if(badge_image == true){
        //     $("#badgeStyle").css('background-image','');

        //     $("#mobibadgeStyle").css('background-image','');
        // }

        $("#highlightStyle").css('background-color','');
        $("#mobibadgeStyle").css('background-color','');

     
    }else{
        
        $("#highlightStyle").css('background-image','');
        $("#highlightStyle").css('background-color',bgcolor);

         $("#mobibadgeStyle").css('background-image','');
        $("#mobibadgeStyle").css('background-color',bgcolor);

    }

    if( borderSize != "" ){

        let borderCss = $("#borderBgcolhighlight").val()+' '+borderstyle+' '+borderSize;

        $("#highlightStyle").css('border', borderCss );
        $("#highlightStyle").css('border', borderCss );

    }else{
        $("#mobibadgeStyle").css('border','');
         $("#mobibadgeStyle").css('border','');
    }


    let shadow = $("#highlightshadow").val();
   
    var shadowCss ='';

    if( shadow == 'light' ){
 
    shadowCss = '0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2)';
    
    }else if( shadow == 'medium' ){

        shadowCss = 'rgba(0, 0, 0, 0.2) 0px 1px 5px 0px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px';

    }else if( shadow == 'heavy' ){

        shadowCss = 'rgba(0, 0, 0, 0.2) 0px 1px 8px 0px, rgba(0, 0, 0, 0.14) 0px 3px 4px 0px, rgba(0, 0, 0, 0.12) 0px 3px 3px -2px';
    }


       
    document.getElementById('highlightStyle').style.webkitBoxShadow = shadowCss;
    document.getElementById('highlightStyle').style.mozBoxShadow = shadowCss;
    document.getElementById('highlightStyle').style.boxShadow = shadowCss;

    // document.getElementById('mobibadgeStyle').style.boxShadow = shadowCss;
    // document.getElementById('mobibadgeStyle').style.webkitBoxShadow = shadowCss;
    // document.getElementById('mobibadgeStyle').style.mozBoxShadow = shadowCss;

    updateHighlightImageShadow();
}

function updateHighlightImageShadow( type='' ){


    let shadow = $("#highlightImageshadow").val();

    var shadowCss ='';

    if( shadow == 'light' ){

    shadowCss = '0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2)';

    }else if( shadow == 'medium' ){

        shadowCss = 'rgba(0, 0, 0, 0.2) 0px 1px 5px 0px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px';

    }else if( shadow == 'heavy' ){

        shadowCss = 'rgba(0, 0, 0, 0.2) 0px 1px 8px 0px, rgba(0, 0, 0, 0.14) 0px 3px 4px 0px, rgba(0, 0, 0, 0.12) 0px 3px 3px -2px';
    }

    $('.primehDesktop').find('img.primehImage').css('box-shadow', shadowCss);
    
    // document.getElementById('highlightImageStyle').style.webkitBoxShadow = shadowCss;
    // document.getElementById('highlightImageStyle').style.mozBoxShadow = shadowCss;
    // document.getElementById('highlightImageStyle').style.boxShadow = shadowCss;

    // document.getElementById('mobibadgeStyle').style.boxShadow = shadowCss;
    // document.getElementById('mobibadgeStyle').style.webkitBoxShadow = shadowCss;
    // document.getElementById('mobibadgeStyle').style.mozBoxShadow = shadowCss;

 
}

// $(".bgcolortype").change( function(){

//     bgcolorType = $(this).val();

//     updateBadgeGradientwithBorderShadow();
// });


$("#highlightbordersize").change( function(){

    updateHighlightstyle( bgcolor , textColor ,lineHeight, height);
});

$("#highlightshadow").change( function(){

    updateBadgeGradientwithBorderShadow();
});

$("#highlightImageshadow").change( function(){

    updateHighlightImageShadow();
});

$("#highlightborderstyle").change( function(){

    updateBadgeGradientwithBorderShadow();
});




</script>