@extends('layouts.master') @section('title') @parent
<title>{{ Request::is('edithighlight*') ? "Edit Highlight" :"Create Highlight" }}</title>
@stop @section('description') @parent
<meta content="" name="description" />
@stop
@section('css')
 @parent

@stop
@section('js')
@parent
<style type="text/css">
  @media (max-width: 576px) { 
    .responsive-imagep{
      height: 22px;
    }
    #groupbadge{
      width: 60px;
    }
  }
</style>
<!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">


<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script> -->

@stop
@section('content')
@parent
<?php 
    $arrPlanNameById = array( 1 =>'Trial',2=>'Basic',3=>'Pro');
?>

<div class="row">
    <div class="col-12">
        <h2>Prime Showcase</h2>
        <p class="text-muted">See how some of the merchants have been using Prime badges and highlights on their stores.</p>
        <div class="">
            <div class="form-group row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <select name="showcasetype" id="showcasetype" onchange="selectedType()"; class="form-control metafieldcond" >
                        <option {{ $type == 'badge' ? 'selected':'' }} value="badge">Badges</option>
                        <option {{ $type == 'highlight' ? 'selected':'' }} value="highlight" >Highlights</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
        <div class="row">            
                
                @foreach($showcases as $showcase)
                
                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{$showcase['storename']}}</b>
                        </div>
                        <div class="card-body bg-light">
                            @if( isset($showcase['homeurl']) && !empty($showcase['homeurl']) )
                                <img src="{{$showcase['homeurl']}}" class="border my-2 w-100" style="">
                            @endif
                            @if( isset($showcase['producturl']) && !empty($showcase['producturl']) )
                                <img src="{{$showcase['producturl']}}" class="border my-2 w-100" style="">
                            @endif
                            @if( isset($showcase['collectionurl']) && !empty($showcase['collectionurl']) )
                                <img src="{{$showcase['collectionurl']}}" class="border my-2 w-100" style="">
                            @endif
                            @if( isset($showcase['miscurl']) && !empty($showcase['miscurl']) )
                                <img src="{{$showcase['miscurl']}}" class="border my-2 w-100" style="">
                            @endif
                        </div>
                    </div>
                </div>

                    @endforeach
              
           
                    <div class="col-12 text-left my-2">
                {{ $showcases->links() }}
                  
            <//div>
        </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js" ></script> 
<script type="text/javascript">

// table = $('#collectionlist').DataTable({
//                   // dom: "Bfrtip",
//                   dom: "Bfrtip",
//                   responsive: true,
//                   "paging": true,
//                   "pagingType": "simple",
//                   "searching" : false,
//                   "ordering": false,
//                   "bServerSide":false,
//                   "bInfo" : false,
//                   "pageLength": 1,
//                 });

    function selectedType(){    
        var type = $('#showcasetype').val();
        if(type == 'badge'){
            window.location = "{{asset('showcasebadge')}}";
        }else{
            window.location = "{{asset('showcasehighlight')}}";
        }
        
    }

</script>
@stop