@extends('layouts.master')

@section('title')
@parent
<title>Dashboard Home</title>
@stop

@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop

@section('content')
@parent

<div class="row pt-2">
  <div class="col-12 col-sm-8">
    <h3>
      <div class="float-left">
        Description List
      </div>
      <div class="float-left ml-3">
        <a href="{{asset('createdescription')}} "class="btn btn-primary float-right">Create New Description</a>
      </div>
    </h3>

    <div class="pt-5">
      <table id="myTable" class="table table-bordered table-striped table-sm table-responsive">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th  class="text-center"> Description Name</th>
            <th class="text-center">Actions</th>
            <th class="text-center">Priority</th>
          </tr>
        </thead>
        <colgroup>
          <col width="5%">
          <col width="40%">
          <col width="15%">
          <col width="5%">
        </colgroup>
        <tbody>
          <tr>
            <td class="text-center">
              1
            </td>
            <td class="text-center">
              <div class="m-2">
              Test 1
              </div>
            </td>
            <td  class="text-center" >
              <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-sm btn-default">
                  <i class="fa fa-power-off"></i>
                </button>

                <button type="button" class="btn btn-sm btn-default">Edit</button>
                <button type="button" class="btn btn-sm btn-default">Delete</button>
              </div>
            </td>
            <td class="Priority">
              <div class="text-center text-muted">
                <a href="#"><i class="fa fa-bars"></i></a>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-12 col-sm-4">
    <div class="card bg-light">
      <div class="card-body ">
          <h5>
            Install App Snippet
          </h5>
          <div class="small">
            To display the description on your store you need to add a line of app installation code in your Shopify store theme.
          </div>
          <a class="btn btn-primary mt-3" href="#" target="_blank">Install Guide</a>
      </div>
    </div>
    <div class="card bg-light mt-4">
      <div class="card-body ">
        <h5>
          Description Display
        </h5>
        <div class="small">
          Show or hide the description on your store by flipping the switch below.
        </div>
        <div class="btn-group btn-group-toggle mt-3" data-toggle="buttons">
          <label class="btn btn-outline-primary ">
            <input type="radio" name="options" autocomplete="off" checked=""> ON
          </label>
          <label class="btn btn-outline-primary active">
            <input type="radio" name="options" id="option3" autocomplete="off"> OFF
          </label>
        </div>
      </div>
    </div>
  </div>
</div>
    @stop
