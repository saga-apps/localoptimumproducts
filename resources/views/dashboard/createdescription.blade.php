@extends('layouts.master')
@section('title')
@parent
<title>{{ Request::is('editdescription*') ? "Edit Description" :"Create Description" }}</title>
@stop
@section('description')
@parent
<meta content="" name="description" />
@stop
@section('css')
 @parent

@stop
@section('js')

@stop
@section('content')
@parent

<div class="row">
  <div class="col-12 col-sm-12 col-md-7 mb-4">
        <div class="card mt-3">
          <div class="card-header">Smart Description</div>
          <div class="card-body">
            <div class="form-group row">
              <label for="staticEmail" class="col-12 col-sm-6 col-md-4 col-form-label font-weight-bold">Create Description </label>
            </div>
            <div class="form-group row">
              <table class="table table-striped table-borderless table-sm">
              	<thead></thead>
              	<colgroup>
              		<col width="10%">
              		<col width="60%">
              		<col width="10%">
              	</colgroup>
                <tbody>
                  <tr>
                    <td>
                      <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-2" data-toggle="modal" data-target="#exampleModal">
                    </td>
                    <td>
                      <div class="col-12 ml-0 mt-2">
                        <input type="text" class="form-control" placeholder="Type your text">
                      </div>
                    </td>
                    <td>
                    	<div class="mt-3">
                    		<i class="fa fa-times text-danger" aria-hidden="true"></i>
                    	</div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-2" data-toggle="modal" data-target="#exampleModal">
                    </td>
                    <td>
                      <div class="col-12 ml-0 mt-2">
                        <input type="text" class="form-control" name="display[badgetitle]" id="badgetitle" placeholder="Type your text">
                      </div>
                      <td>
                      	<div class="mt-3">
                      		<i class="fa fa-times  text-danger" aria-hidden="true"></i>
                      	</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-2" data-toggle="modal" data-target="#exampleModal">
                      </td>
                      <td>
                        <div class="col-12 mt-2">
                          <input type="text" class="form-control" name="display[badgetitle]" id="badgetitle" placeholder="Type your text">
                        </div>

                      </td>
                      <td>
                      	<div class="mt-3">
                      		<i class="fa fa-times  text-danger" aria-hidden="true"></i>
                      	</div></td>
                    </tr>
                    <tr>
                      <td>
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-2" data-toggle="modal" data-target="#exampleModal">
                      </td>
                      <td>
                        <div class="col-12 mt-2">
                          <input type="text" class="form-control" name="display[badgetitle]" id="badgetitle" placeholder="Type your text">
                        </div>
                      </td>
                      <td>
                      	<div class="mt-3">
                      		<i class="fa fa-times  text-danger" aria-hidden="true"></i>
                      	</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="form-group row">
                <button type="button" class="btn btn-secondary ml-3" onclick="addRuleHtml();">+ &nbsp;Add More</button>
              </div>
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Display Payment Icons at the bottom row</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                <label class="form-check-label" for="inlineCheckbox1">Paypal</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                <label class="form-check-label" for="inlineCheckbox2">Visa</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                <label class="form-check-label" for="inlineCheckbox2">Mastercard</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                <label class="form-check-label" for="inlineCheckbox2">Google Play</label>
              </div>
              <hr>
              <div class="form-group row">
                <label for="rules" class="col-12 col-sm-6 col-md-4 col-form-label font-weight-bold">Rules</label>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-12 col-sm-6 col-md-6 col-form-label">Product must match</label>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="condition" id="condition" value="1" checked>
                    <label class="form-check-label" for="inlineRadio1">All Condition</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="condition" id="condition" value="2">
                    <label class="form-check-label" for="condition">Any Condition</label>
                  </div>
                </div>
              </div>
              <div class="row mx-0">
                <table class="table table-sm table-bordered table-striped">
                  <colgroup>
                    <col width="80%">
                    <col width="5%">
                  </colgroup>
                  <tbody id="appendAll">

                    @if( !empty( $displayrules ) )

                    @foreach( $displayrules as $index => $displayrule )

                    <tr id="append{{$index== 0 ? '': $index }}">

                      <td>
                        <div class="row mx-0 px-0">
                          <div class="mx-1">
                            <select class="form-control" name="rule[{{$index}}][conditiontype]" onchange="addDesRule(this,'{{$index}}');">
                              <option {{ isset( $displayrule['collection'] ) ? 'selected':''  }} value="collection">Collection</option>
                              <option {{ isset( $displayrule['product'] ) ? 'selected':''  }} value="product">Product</option>
                              <option {{ isset( $displayrule['saleprice'] ) ? 'selected':''  }} value="saleprice">Sale price</option>
                              <option {{ isset( $displayrule['inventory'] ) ? 'selected':''  }} value="inventory">Inventory</option>
                              <option {{ isset( $displayrule['tags'] ) ? 'selected':''  }} value="tags">Tags</option>
                              <option {{ isset( $displayrule['createddate'] ) ? 'selected':''  }}  value="createddate">Created Date</option>
                            </select>
                          </div>

                          @if( isset( $displayrule['createddate'] ) )
                            <span class="mt-2 mx-1"> is within the past </span>
                            <div class="mx-1 inventorytype">
                              <input type="number" min="1" max="30" name="rule[{{$index}}][conditiontype][createddate]" value="{{$displayrule['createddate']}}" class="form-control">
                            </div>
                            <span class="mt-2 mx-1"> days </span>
                            @elseif( isset( $displayrule['collection'] ) )
                              <span class="mt-2 mx-1">equals</span>

                              <div class="mx-1">
                              <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            -- Select --
                            <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="collection_list">
                              @if( !empty( $customCollections ) )

                              @php

                              $counter = 0;
                              @endphp
                              @foreach( $customCollections as $indexcollection => $collection )

                               @php


                              if( in_array( $collection['id'] , $displayrule['collection'] )  ) {

                                $counter +=1;
                              }

                             @endphp
                               <li class="ml-2"> <span>
                              <input onchange="countCheck({{$index}});" name="rule[{{$index}}][conditiontype][collection][]" type="checkbox" class="collection_listinput" {{ is_array( $displayrule['collection'] ) && in_array( $collection['id'] , $displayrule['collection'] ) ? 'checked':'' }} value="{{$collection['id']}}" id="collection">&nbsp; {{$collection['title']}}
                              </span>`
                              </li>
                              @endforeach
                              @endif
                            </ul>
                            <span class="text-muted small" style="padding-top: 8px;margin-left: 5px;float: left;">
                            <i>


                              <span id="collectionscount">
                                {{$counter}}
                              </span> selected</i>
                            </span>
                            </div>
                          </div>
                          @elseif( isset( $displayrule['product'] ) )
                          <span class="mt-2 mx-1">equals</span>
                            <div class="mx-1">

                              {{-- <input name="rule[{{$index}}][conditiontype][product][]" type="text" class="form-control" id="badgetitle" placeholder="Selected products " value=""> --}}

                              <btn  onclick="openProductModel('#blacklistProduct-model',{{$index}});"  type="text" class="btn btn-secondary" >Select products</btn>

                                <input type="hidden"  name="rule[{{$index}}][conditiontype][product][]" type="text" class="form-control" id="addProduct_{{$index}}" placeholder="Selected products " value="{{implode(",", $displayrule['product'])}}">

                            </div>
                            <a href="javascript:void(0);" class="mt-2" onclick="openProductModel('#blacklistProduct-model','{{$index}}',2);" >
                            <span class="text-primary pointer small" >
                              <i>
                                <span id="productcount_{{$index}}">{{count( explode(",",implode(",", $displayrule['product'] ) )  ) }}</span>
                              selected</i>
                            </span>
                            </a>
                            @elseif( isset( $displayrule['saleprice'] ) )
                           <span class="mt-2 mx-1"> is </span>
                              <div class="mx-1 inventorytype " >
                                      <select name="rule[{{$index}}][conditiontype][saleprice]" class="form-control">
                                        <option  value="10">10%</option>
                                        <option value="15" >15%</option>
                                        <option value="20" >20%</option>
                                        <option value="30" >30%</option>
                                        <option value="40" >40%</option>
                                        <option value="50" >50%</option>
                                        <option value="60" >60%</option>
                                        <option value="70" >70%</option>
                                        <option value="80" >80%</option>
                                        <option value="90" >90%</option>
                                      </select>


                            </div>
                            <span class="mt-2 mx-1"> off or more </span>
                            @elseif( isset( $displayrule['inventory'] ) )
                              <span class="mt-2 mx-1">is</span>
                              <div class="mx-1" >
                                <select name="rule[{{$index}}][conditiontype][inventory][type]" class="form-control" onchange="inventoryType(this,{{$index}});">
                                  <option {{ $displayrule['inventory']['type'] == 1 ? 'selected':'' }} value="1">between</option>
                                  <option {{ $displayrule['inventory']['type'] == 2 ? 'selected':'' }} value="2" >in stock</option>
                                  <option {{ $displayrule['inventory']['type'] == 3 ? 'selected':'' }} value="3" >out of stock</option>
                                </select>
                              </div>
                              @if( isset( $displayrule['inventory']['value'] ) && $displayrule['inventory']['type'] != 1 )
                              @php
                              $inventoryHide = 'd-none';
                              @endphp
                              @endif
                                <span class="inventorytype {{$inventoryHide}} mx-1 mt-2">1 and </span>
                                <div class="mx-1 inventorytype {{$inventoryHide}}" >
                                  <select name="rule[{{$index}}][conditiontype][inventory][value]" class="form-control">
                                    <option {{ $displayrule['inventory']['value'] == 1 ? 'selected':'' }} value="1">1</option>
                                    <option {{ $displayrule['inventory']['value'] == 2 ? 'selected':'' }} value="2" >2</option>
                                    <option {{ $displayrule['inventory']['value'] == 3 ? 'selected':'' }} value="3" >3</option>
                                    <option {{ $displayrule['inventory']['value'] == 4 ? 'selected':'' }} value="4" >4</option>
                                    <option {{ $displayrule['inventory']['value'] == 5 ? 'selected':'' }} value="5" >5</option>
                                    <option {{ $displayrule['inventory']['value'] == 6 ? 'selected':'' }} value="6" >6</option>
                                    <option {{ $displayrule['inventory']['value'] == 7 ? 'selected':'' }} value="7" >7</option>
                                    <option {{ $displayrule['inventory']['value'] == 8 ? 'selected':'' }} value="8" >8</option>
                                    <option {{ $displayrule['inventory']['value'] == 9 ? 'selected':'' }} value="9" >9</option>
                                    <option {{ $displayrule['inventory']['value'] == 10 ? 'selected':'' }} value="10">10</option>
                                  </select>
                                </div>
                                @elseif( isset( $displayrule['tags'] ) )

                                  <span class="mt-2 mx-1">equals</span>
                                  <div class="mx-1">
                                    <input data-role="tagsinput" name="rule[{{$index}}][conditiontype][tags]" size="1" type="text" class="form-control tags" id="tag" placeholder="Enter Product tags" value="{{$displayrule['tags']}}">

                                  </div>

                                @endif
                        </div>
                      </td>
                      <td>
                        <div class="text-center mt-2">
                          <a disabled onclick="deleteRule('#appendHtml');" href="javascript:void(0)">
                            <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                    @else


                            <tr id="appendHtml">
                              <td>
                                <div class="row mx-0 px-0">
                                  <div class="mx-1"  >
                                    <select class="form-control" name="rule[0][conditiontype]" onchange="addRule(this,0);">
                                      <option value="collection">Collection</option>
                                      <option value="product" >Product</option>
                                      <option value="saleprice" >Sale price</option>
                                      <option value="inventory" >Inventory</option>
                                      <option value="tags" >Tags</option>
                                      <option selected value="createddate">Created Date</option>
                                    </select>
                                  </div>
                                  <span class="mt-2 mx-1"> is within the past </span>
                                  <div class="mx-1 inventorytype" >
                                    <input type="number" min="1" max="30" name="rule[0][conditiontype][createddate]" value="7" class="form-control">

                                    </div>
                                    <span class="mt-2 mx-1"> days </span>
                                  </div>
                                </td>
                                <td>
                                  <div class="text-center mt-2">
                                    <a disabled onclick="deleteRule('#appendHtml');" href="javascript:void(0)">
                                      <i class="fa fa-times  text-danger" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                </td>
                              </tr>
                              @endif
                  </tbody>
                </table>
              </div>
              <div class="col-sm-12 mt-3">
                <button type="button" class="btn btn-secondary" onclick="addRuleHtml();">+ &nbsp;Add Rule</button>
              </div>
              <div class="col-12 text-right">
                <button type="button" onclick="saveRule('#formRule');" class="btn btn-primary">Save</button>
                <button type="button" onclick="location.href='https://starstaging.thalia-apps.com/dashboard'" class="btn btn-default">Cancel</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-5">
          <div class="card mt-3">
            <div class="card-body text-center">
              <div>
              	<h6 class="font900 font-weight-bold pb-3">Preview</h6>
                <div class="card text-center">
                  <div class="row mt-3">
                    <div class="col">
                      <div class="form-check form-check-inline">
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-3 mt-2">
                        <label class="form-check-label m-3" for="inlineCheckbox1">Paypal</label>
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-check form-check-inline">
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-3 mt-2">
                        <label class="form-check-label ml-3" for="inlineCheckbox1">Paypal</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-check form-check-inline">
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-3 mt-2">
                        <label class="form-check-label m-3" for="inlineCheckbox1">Paypal</label>
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-check form-check-inline">
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-3 mt-2">
                        <label class="form-check-label ml-3" for="inlineCheckbox1">Paypal</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-check form-check-inline">
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-3 mt-2">
                        <label class="form-check-label m-3" for="inlineCheckbox1">Paypal</label>
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-check form-check-inline">
                        <img src="https://dummyimage.com/50x50/ddd/000/" class="img-fluid ml-3 mt-2">
                        <label class="form-check-label ml-3" for="inlineCheckbox1">Paypal</label>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">

                  </div>
                  <div class="card-footer text-muted">
                    ..
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Icon Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header"><div class="form-check form-check-inline">
                <button type="button" class="btn btn-light">Colorable Icons</button>
              </div>
              <div class="form-check form-check-inline">
                <button type="button" class="btn btn-light">Graphic Icons</button>
              </div>
              <div class="form-check form-check-inline">
                <button type="button" class="btn btn-light">Graphic Icons</button>
              </div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">

              <button type="button" class="btn btn-primary">Use This</button>
            </div>
          </div>
        </div>
      </div>
@stop
