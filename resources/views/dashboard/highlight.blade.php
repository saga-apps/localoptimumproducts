<!-- Highlight.blade.php -->
@extends('layouts.master')
@section('title')
    @parent
    <title>Dashboard Home</title>
@stop
@section('description')
    @parent
    <meta content="" name="description" />
@stop
@section('css')
    @parent

@stop

@section('js')
    @parent

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.js" type="text/javascript"> </script>

@stop
  <style>

    @media (max-width: 576px) { 
      .responsive-imagep{
        height: 22px;
      }
      #grouphighlight{
        width: 60px;
      }
      
    }
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

     .sp-replacer {
      padding: 8px  !important;
      color: #212529 !important;
      height: 38px !important;
      border: 1px solid #ced4da !important;
      border-radius: .25rem !important;
      width: 100%;
    }

  
  .prime-align-middle{vertical-align:middle;}

  .prime-m-0{margin: 0rem;} 
  .prime-mr-0{margin-right: 0rem;} 
  .prime-ml-0{margin-left: 0rem;}
  .prime-mt-0{margin-top: 0rem;}
	.prime-mb-0{margin-bottom: 0rem;}
  					
  .prime-m-1{margin: .25rem;} 
  .prime-mr-1{margin-right: .25rem;} 
  .prime-ml-1{margin-left: .25rem;}
  .prime-mt-1{margin-top: .25rem;}
  .prime-mb-1{margin-bottom: .25rem;}
  
  .prime-m-2{margin: .5rem;} 
  .prime-mr-2{margin-right: .5rem;} 
  .prime-ml-2{margin-left: .5rem;}
  .prime-mt-2{margin-top: .5rem;}
  .prime-mb-2{margin-bottom: .5rem;}
  
  .prime-m-3{margin: 1em;} 
  .prime-mr-3{margin-right: 1rem;} 
  .prime-ml-3{margin-left: 1rem;}
  .prime-mt-3{margin-top: 1rem;}
  .prime-mb-3{margin-bottom: 1rem;}
  
  .prime-m-4{margin: 1.5rem;} 
  .prime-mr-4{margin-right: 1.5rem;} 
  .prime-ml-4{margin-left: 1.5rem;}
  .prime-mt-4{margin-top: 1.5rem;}
	.prime-mb-4{margin-bottom: 1.5rem;}
  
  .prime-m-5{margin: 3rem;} 
  .prime-mr-5{margin-right: 3rem;} 
  .prime-ml-5{margin-left: 3rem;}
  .prime-mt-5{margin-top: 3rem;}
	.prime-mb-5{margin-bottom: 3rem;}
  
  .prime-p-0{padding: 0rem;} 
  .prime-pr-0{padding-right: 0rem;} 
  .prime-pl-0{padding-left: 0rem;}
  .prime-pt-0{padding-top: 0rem;}
	.prime-pb-0{padding-bottom: 0rem;}
  					
  .prime-p-1{padding: .25rem;} 
  .prime-pr-1{padding-right: .25rem;} 
  .prime-pl-1{padding-left: .25rem;}
  .prime-pt-1{padding-top: .25rem;}
	.prime-pb-1{padding-bottom: .25rem;}
  
  .prime-p-2{padding: .5rem;} 
  .prime-pr-2{padding-right: .5rem;} 
  .prime-pl-2{padding-left: .5rem;}
  .prime-pt-2{padding-top: .5rem;}
  .prime-pb-2{padding-bottom: .5rem;}
  
  .prime-p-3{padding: 1em;} 
  .prime-pr-3{padding-right: 1rem;} 
  .prime-pl-3{padding-left: 1rem;}
  .prime-pt-3{padding-top: 1rem;}
  .prime-pb-3{padding-bottom: 1rem;}
  
  .prime-p-4{padding: 1.5rem;} 
  .prime-pr-4{padding-right: 1.5rem;} 
  .prime-pl-4{padding-left: 1.5rem;}
  .prime-pt-4{padding-top: 1.5rem;}
  .prime-pb-4{padding-bottom: 1.5rem;}

  .prime-p-5{padding: 3rem;} 
  .prime-pr-5{padding-right: 3rem;} 
  .prime-pl-5{padding-left: 3rem;}
  .prime-pt-5{padding-top: 3rem;}
  .prime-pb-5{padding-bottom: 3rem;}

  .prime-px-2{padding-left:.5rem;padding-right:.5rem;}					  
  .prime-py-1{padding-top:.25rem;padding-bottom:.25rem;}
  .prime-mx-auto{margin-left: auto;margin-right: auto;}
  .prime-text-center{text-align:center;}
  .prime-text-left{text-align:left;}


  .prime-px-0{padding-left:0px !important;padding-right:0px !important;}



  @-ms-viewport{width:device-width}html{box-sizing:border-box;-ms-overflow-style:scrollbar}*,::after,::before{box-sizing:inherit}.prime-container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.prime-container{max-width:540px}}@media (min-width:768px){.prime-container{max-width:720px}}@media (min-width:992px){.prime-container{max-width:960px}}@media (min-width:1200px){.prime-container{max-width:1140px}}.prime-container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.prime-row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.prime-no-gutters{margin-right:0;margin-left:0}.prime-no-gutters>.col,.prime-no-gutters>[class*=col-]{padding-right:0;padding-left:0}.prime-col,.prime-col-1,.prime-col-10,.prime-col-11,.prime-col-12,.prime-col-2,.prime-col-3,.prime-col-4,.prime-col-5,.prime-col-6,.prime-col-7,.prime-col-8,.prime-col-9,.prime-col-auto,.prime-col-lg,.prime-col-lg-1,.prime-col-lg-10,.prime-col-lg-11,.prime-col-lg-12,.prime-col-lg-2,.prime-col-lg-3,.prime-col-lg-4,.prime-col-lg-5,.prime-col-lg-6,.prime-col-lg-7,.prime-col-lg-8,.prime-col-lg-9,.prime-col-lg-auto,.prime-col-md,.prime-col-md-1,.prime-col-md-10,.prime-col-md-11,.prime-col-md-12,.prime-col-md-2,.prime-col-md-3,.prime-col-md-4,.prime-col-md-5,.prime-col-md-6,.prime-col-md-7,.prime-col-md-8,.prime-col-md-9,.prime-col-md-auto,.prime-col-sm,.prime-col-sm-1,.prime-col-sm-10,.prime-col-sm-11,.prime-col-sm-12,.prime-col-sm-2,.prime-col-sm-3,.prime-col-sm-4,.prime-col-sm-5,.prime-col-sm-6,.prime-col-sm-7,.prime-col-sm-8,.prime-col-sm-9,.prime-col-sm-auto,.prime-col-xl,.prime-col-xl-1,.prime-col-xl-10,.prime-col-xl-11,.prime-col-xl-12,.prime-col-xl-2,.prime-col-xl-3,.prime-col-xl-4,.prime-col-xl-5,.prime-col-xl-6,.prime-col-xl-7,.prime-col-xl-8,.prime-col-xl-9,.prime-col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.prime-col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-first{-ms-flex-order:-1;order:-1}.prime-order-last{-ms-flex-order:13;order:13}.prime-order-0{-ms-flex-order:0;order:0}.prime-order-1{-ms-flex-order:1;order:1}.prime-order-2{-ms-flex-order:2;order:2}.prime-order-3{-ms-flex-order:3;order:3}.prime-order-4{-ms-flex-order:4;order:4}.prime-order-5{-ms-flex-order:5;order:5}.prime-order-6{-ms-flex-order:6;order:6}.prime-order-7{-ms-flex-order:7;order:7}.prime-order-8{-ms-flex-order:8;order:8}.prime-order-9{-ms-flex-order:9;order:9}.prime-order-10{-ms-flex-order:10;order:10}.prime-order-11{-ms-flex-order:11;order:11}.prime-order-12{-ms-flex-order:12;order:12}.prime-offset-1{margin-left:8.333333%}.prime-offset-2{margin-left:16.666667%}.prime-offset-3{margin-left:25%}.prime-offset-4{margin-left:33.333333%}.prime-offset-5{margin-left:41.666667%}.prime-offset-6{margin-left:50%}.prime-offset-7{margin-left:58.333333%}.prime-offset-8{margin-left:66.666667%}.prime-offset-9{margin-left:75%}.prime-offset-10{margin-left:83.333333%}.prime-offset-11{margin-left:91.666667%}@media (min-width:576px){.prime-col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-sm-first{-ms-flex-order:-1;order:-1}.prime-order-sm-last{-ms-flex-order:13;order:13}.prime-order-sm-0{-ms-flex-order:0;order:0}.prime-order-sm-1{-ms-flex-order:1;order:1}.prime-order-sm-2{-ms-flex-order:2;order:2}.prime-order-sm-3{-ms-flex-order:3;order:3}.prime-order-sm-4{-ms-flex-order:4;order:4}.prime-order-sm-5{-ms-flex-order:5;order:5}.prime-order-sm-6{-ms-flex-order:6;order:6}.prime-order-sm-7{-ms-flex-order:7;order:7}.prime-order-sm-8{-ms-flex-order:8;order:8}.prime-order-sm-9{-ms-flex-order:9;order:9}.prime-order-sm-10{-ms-flex-order:10;order:10}.prime-order-sm-11{-ms-flex-order:11;order:11}.prime-order-sm-12{-ms-flex-order:12;order:12}.prime-offset-sm-0{margin-left:0}.prime-offset-sm-1{margin-left:8.333333%}.prime-offset-sm-2{margin-left:16.666667%}.prime-offset-sm-3{margin-left:25%}.prime-offset-sm-4{margin-left:33.333333%}.prime-offset-sm-5{margin-left:41.666667%}.prime-offset-sm-6{margin-left:50%}.prime-offset-sm-7{margin-left:58.333333%}.prime-offset-sm-8{margin-left:66.666667%}.prime-offset-sm-9{margin-left:75%}.prime-offset-sm-10{margin-left:83.333333%}.prime-offset-sm-11{margin-left:91.666667%}}@media (min-width:768px){.prime-col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-md-first{-ms-flex-order:-1;order:-1}.prime-order-md-last{-ms-flex-order:13;order:13}.prime-order-md-0{-ms-flex-order:0;order:0}.prime-order-md-1{-ms-flex-order:1;order:1}.prime-order-md-2{-ms-flex-order:2;order:2}.prime-order-md-3{-ms-flex-order:3;order:3}.prime-order-md-4{-ms-flex-order:4;order:4}.prime-order-md-5{-ms-flex-order:5;order:5}.prime-order-md-6{-ms-flex-order:6;order:6}.prime-order-md-7{-ms-flex-order:7;order:7}.prime-order-md-8{-ms-flex-order:8;order:8}.prime-order-md-9{-ms-flex-order:9;order:9}.prime-order-md-10{-ms-flex-order:10;order:10}.prime-order-md-11{-ms-flex-order:11;order:11}.prime-order-md-12{-ms-flex-order:12;order:12}.prime-offset-md-0{margin-left:0}.prime-offset-md-1{margin-left:8.333333%}.prime-offset-md-2{margin-left:16.666667%}.prime-offset-md-3{margin-left:25%}.prime-offset-md-4{margin-left:33.333333%}.prime-offset-md-5{margin-left:41.666667%}.prime-offset-md-6{margin-left:50%}.prime-offset-md-7{margin-left:58.333333%}.prime-offset-md-8{margin-left:66.666667%}.prime-offset-md-9{margin-left:75%}.prime-offset-md-10{margin-left:83.333333%}.prime-offset-md-11{margin-left:91.666667%}}@media (min-width:992px){.prime-col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-lg-first{-ms-flex-order:-1;order:-1}.prime-order-lg-last{-ms-flex-order:13;order:13}.prime-order-lg-0{-ms-flex-order:0;order:0}.prime-order-lg-1{-ms-flex-order:1;order:1}.prime-order-lg-2{-ms-flex-order:2;order:2}.prime-order-lg-3{-ms-flex-order:3;order:3}.prime-order-lg-4{-ms-flex-order:4;order:4}.prime-order-lg-5{-ms-flex-order:5;order:5}.prime-order-lg-6{-ms-flex-order:6;order:6}.prime-order-lg-7{-ms-flex-order:7;order:7}.prime-order-lg-8{-ms-flex-order:8;order:8}.prime-order-lg-9{-ms-flex-order:9;order:9}.prime-order-lg-10{-ms-flex-order:10;order:10}.prime-order-lg-11{-ms-flex-order:11;order:11}.prime-order-lg-12{-ms-flex-order:12;order:12}.prime-offset-lg-0{margin-left:0}.prime-offset-lg-1{margin-left:8.333333%}.prime-offset-lg-2{margin-left:16.666667%}.prime-offset-lg-3{margin-left:25%}.prime-offset-lg-4{margin-left:33.333333%}.prime-offset-lg-5{margin-left:41.666667%}.prime-offset-lg-6{margin-left:50%}.prime-offset-lg-7{margin-left:58.333333%}.prime-offset-lg-8{margin-left:66.666667%}.prime-offset-lg-9{margin-left:75%}.prime-offset-lg-10{margin-left:83.333333%}.prime-offset-lg-11{margin-left:91.666667%}}@media (min-width:1200px){.prime-col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-xl-first{-ms-flex-order:-1;order:-1}.prime-order-xl-last{-ms-flex-order:13;order:13}.prime-order-xl-0{-ms-flex-order:0;order:0}.prime-order-xl-1{-ms-flex-order:1;order:1}.prime-order-xl-2{-ms-flex-order:2;order:2}.prime-order-xl-3{-ms-flex-order:3;order:3}.prime-order-xl-4{-ms-flex-order:4;order:4}.prime-order-xl-5{-ms-flex-order:5;order:5}.prime-order-xl-6{-ms-flex-order:6;order:6}.prime-order-xl-7{-ms-flex-order:7;order:7}.prime-order-xl-8{-ms-flex-order:8;order:8}.prime-order-xl-9{-ms-flex-order:9;order:9}.prime-order-xl-10{-ms-flex-order:10;order:10}.prime-order-xl-11{-ms-flex-order:11;order:11}.prime-order-xl-12{-ms-flex-order:12;order:12}.prime-offset-xl-0{margin-left:0}.prime-offset-xl-1{margin-left:8.333333%}.prime-offset-xl-2{margin-left:16.666667%}.prime-offset-xl-3{margin-left:25%}.prime-offset-xl-4{margin-left:33.333333%}.prime-offset-xl-5{margin-left:41.666667%}.prime-offset-xl-6{margin-left:50%}.prime-offset-xl-7{margin-left:58.333333%}.prime-offset-xl-8{margin-left:66.666667%}.prime-offset-xl-9{margin-left:75%}.prime-offset-xl-10{margin-left:83.333333%}.prime-offset-xl-11{margin-left:91.666667%}}.prime-d-none{display:none!important}.prime-d-inline{display:inline!important}.prime-d-inline-block{display:inline-block!important}.prime-d-block{display:block!important}.prime-d-table{display:table!important}.prime-d-table-row{display:table-row!important}.prime-d-table-cell{display:table-cell!important}.prime-d-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}@media (min-width:576px){.prime-d-sm-none{display:none!important}.prime-d-sm-inline{display:inline!important}.prime-d-sm-inline-block{display:inline-block!important}.prime-d-sm-block{display:block!important}.prime-d-sm-table{display:table!important}.prime-d-sm-table-row{display:table-row!important}.prime-d-sm-table-cell{display:table-cell!important}.prime-d-sm-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-sm-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media (min-width:768px){.prime-d-md-none{display:none!important}.prime-d-md-inline{display:inline!important}.prime-d-md-inline-block{display:inline-block!important}.prime-d-md-block{display:block!important}.prime-d-md-table{display:table!important}.prime-d-md-table-row{display:table-row!important}.prime-d-md-table-cell{display:table-cell!important}.prime-d-md-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-md-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media (min-width:992px){.prime-d-lg-none{display:none!important}.prime-d-lg-inline{display:inline!important}.prime-d-lg-inline-block{display:inline-block!important}.prime-d-lg-block{display:block!important}.prime-d-lg-table{display:table!important}.prime-d-lg-table-row{display:table-row!important}.prime-d-lg-table-cell{display:table-cell!important}.prime-d-lg-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-lg-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media (min-width:1200px){.prime-d-xl-none{display:none!important}.prime-d-xl-inline{display:inline!important}.prime-d-xl-inline-block{display:inline-block!important}.prime-d-xl-block{display:block!important}.prime-d-xl-table{display:table!important}.prime-d-xl-table-row{display:table-row!important}.prime-d-xl-table-cell{display:table-cell!important}.prime-d-xl-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-xl-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media print{.prime-d-print-none{display:none!important}.prime-d-print-inline{display:inline!important}.prime-d-print-inline-block{display:inline-block!important}.prime-d-print-block{display:block!important}.prime-d-print-table{display:table!important}.prime-d-print-table-row{display:table-row!important}.prime-d-print-table-cell{display:table-cell!important}.prime-d-print-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-print-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}.prime-flex-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}@media (min-width:576px){.prime-flex-sm-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-sm-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-sm-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-sm-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-sm-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-sm-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-sm-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-sm-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-sm-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-sm-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-sm-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-sm-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-sm-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-sm-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-sm-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-sm-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-sm-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-sm-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-sm-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-sm-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-sm-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-sm-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-sm-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-sm-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-sm-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-sm-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-sm-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-sm-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-sm-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-sm-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-sm-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-sm-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-sm-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-sm-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}@media (min-width:768px){.prime-flex-md-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-md-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-md-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-md-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-md-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-md-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-md-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-md-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-md-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-md-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-md-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-md-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-md-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-md-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-md-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-md-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-md-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-md-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-md-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-md-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-md-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-md-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-md-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-md-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-md-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-md-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-md-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-md-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-md-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-md-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-md-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-md-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-md-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-md-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}@media (min-width:992px){.prime-flex-lg-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-lg-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-lg-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-lg-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-lg-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-lg-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-lg-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-lg-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-lg-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-lg-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-lg-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-lg-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-lg-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-lg-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-lg-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-lg-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-lg-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-lg-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-lg-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-lg-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-lg-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-lg-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-lg-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-lg-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-lg-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-lg-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-lg-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-lg-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-lg-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-lg-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-lg-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-lg-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-lg-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-lg-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}@media (min-width:1200px){.prime-flex-xl-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-xl-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-xl-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-xl-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-xl-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-xl-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-xl-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-xl-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-xl-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-xl-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-xl-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-xl-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-xl-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-xl-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-xl-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-xl-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-xl-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-xl-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-xl-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-xl-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-xl-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-xl-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-xl-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-xl-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-xl-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-xl-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-xl-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-xl-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-xl-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-xl-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-xl-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-xl-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-xl-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-xl-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}


  /* .prime-d-block{display:block;}
  .primehImage{margin-right: 2%;} */
                    
  </style>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
@section('content')
@parent

<?php
if( !empty( $highlightstyle ) ) {
  $display       = json_decode( $highlightstyle['display'] ,true );
  $highlight_style    = json_decode( $highlightstyle['style'] ,true );
  // print_r($highlight_style);exit;
}

$arrNameById = array( 'Etc/GMT+12' =>'International Date Line West','Pacific/Pago_Pago'=>'American Samoa','Pacific/Midway'=>'Midway Island','Pacific/Honolulu' =>'Hawaii','America/Juneau'=>'Alaska','America/Los_Angeles'=>'Pacific Time','America/Tijuana' =>'Tijuana','America/Phoenix'=>'Arizona','America/Chihuahua'=>'Chihuahua','America/Mazatlan'=>'Mazatlan','Denver'=>'Mountain Time','America/Guatemala'=>'Central America','America/Chicago'=>'Central Time','America/Mexico_City'=>'Mexico City','America/Monterrey'=>'Monterrey','America/Regina'=>'Saskatchewan','America/Bogota'=>'Bogota','America/New_York'=>'Eastern Time','America/Indiana/Indianapolis'=>'Indiana','America/Lima'=>'Quito','America/Halifax'=>'Atlantic Time','America/Caracas'=>'Caracas','America/Guyana'=>'Georgetown','America/La_Paz'=>'La Paz','America/Puerto_Rico'=>'Puerto Rico','America/Santiago'=>'Santiago','America/St_Johns'=>'Newfoundland','America/Sao_Paulo'=>'Brasilia','America/Argentina/Buenos_Aires'=>'Buenos Aires','America/Godthab'=>'Greenland','America/Montevideo'=>'Montevideo','Atlantic/South_Georgia'=>'Mid-Atlantic','Atlantic/Azores'=>'Azores','Atlantic/Cape_Verde'=>'Cape Verde Is.','Africa/Casablanca'=>'Casablanca','Europe/Dublin'=>'Dublin','Europe/London'=>'London','Europe/Lisbon'=>'Lisbon','Africa/Monrovia'=>'Monrovia','Etc/UTC'=>'UTC','Europe/Amsterdam'=>'Amsterdam','Europe/Belgrade'=>'Belgrade','Europe/Berlin'=>'Berlin','Europe/Zurich'=>'Zurich','Europe/Bratislava'=>'Bratislava','Europe/Brussels'=>'Brussels','Europe/Budapest'=>'Budapest','Europe/Copenhagen'=>'Copenhagen','Europe/Ljubljana'=>'Ljubljana','Europe/Madrid'=>'Madrid','Europe/Paris'=>'Paris','Europe/Prague'=>'Prague','Europe/Rome'=>'Rome','Europe/Sarajevo'=>'Sarajevo','Europe/Skopje'=>'Skopje','Europe/Stockholm'=>'Stockholm','Europe/Vienna'=>'Vienna','Europe/Warsaw'=>'Warsaw','Africa/Algiers'=>'West Central Africa','Europe/Zagreb'=>'Zagreb','Europe/Athens'=>'Athens','Europe/Bucharest'=>'Bucharest','Africa/Cairo'=>'Cairo','Africa/Harare'=>'Harare','Europe/Helsinki'=>'Helsinki','Asia/Jerusalem'=>'Jerusalem','Europe/Kaliningrad'=>'Kaliningrad','Europe/Kiev'=>'Kyiv','Africa/Johannesburg'=>'Pretoria','Europe/Riga'=>'Riga','Europe/Sofia'=>'Sofia','Europe/Tallinn'=>'Tallinn','Europe/Vilnius'=>'Vilnius','Asia/Baghdad'=>'Baghdad','Europe/Istanbul'=>'Istanbul','Asia/Kuwait'=>'Kuwait','Europe/Minsk'=>'Minsk','Europe/Moscow'=>'Moscow','Africa/Nairobi'=>'Nairobi','Asia/Riyadh'=>'Riyadh','Europe/Volgograd'=>'Volgograd','Asia/Tehran'=>'Tehran','Asia/Muscat'=>'Muscat','Asia/Baku'=>'Baku','Europe/Samara'=>'Samara','Asia/Tbilisi'=>'Tbilisi','Asia/Yerevan'=>'Yerevan','Asia/Kabul'=>'Kabul','Asia/Yekaterinburg'=>'Ekaterinburg','Asia/Karachi'=>'Karachi','Asia/Tashkent'=>'Tashkent','Asia/Kolkata'=>'New Delhi','Asia/Calcutta'=>'New Delhi','Asia/Colombo'=>'Sri Jayawardenepura','Asia/Kathmandu'=>'Kathmandu','Asia/Almaty'=>'Almaty','Asia/Dhaka'=>'Dhaka','Asia/Urumqi'=>'Urumqi','Asia/Rangoon'=>'Rangoon','Asia/Bangkok'=>'Hanoi','Asia/Jakarta'=>'Jakarta','Asia/Krasnoyarsk'=>'Krasnoyarsk','Asia/Novosibirsk'=>'Novosibirsk','Asia/Shanghai'=>'Beijing','Asia/Chongqing'=>'Chongqing','Asia/Hong_Kong'=>'Hong Kong','Asia/Irkutsk'=>'Irkutsk','Asia/Kuala_Lumpur'=>'Kuala Lumpur','Australia/Perth'=>'Perth','Asia/Singapore'=>'Singapore','Asia/Taipei'=>'Taipei','Asia/Ulaanbaatar'=>'Ulaanbaatar','Asia/Tokyo'=>'Tokyo','Asia/Seoul'=>'Seoul','Asia/Yakutsk'=>'Yakutsk','Australia/Adelaide'=>'Adelaide','Australia/Darwin'=>'Darwin','Australia/Brisbane'=>'Brisbane','Australia/Melbourne'=>'Melbourne','Pacific/Guam'=>'Guam','Australia/Hobart'=>'Hobart','Pacific/Port_Moresby'=>'Port Moresby','Australia/Sydney'=>'Sydney','Asia/Vladivostok'=>'Vladivostok','Asia/Magadan'=>'Magadan','Pacific/Noumea'=>'New Caledonia','Pacific/Guadalcanal'=>'Solomon Is.','Australia/Srednekolymsk'=>'Srednekolymsk','Pacific/Auckland'=>'Wellington','Pacific/Fiji'=>'Fiji','Asia/Kamchatka'=>'Kamchatka','Pacific/Majuro'=>'Marshall Is.','Pacific/Chatham'=>'Chatham Is.','Pacific/Tongatapu'=>'Nuku alofa','Pacific/Apia'=>'Samoa','Pacific/Fakaofo'=>'Tokelau Is.');

if(isset($arrNameById[Auth::User()->timezone])){
  $mytimezone=Auth::User()->timezone;
}else{
  $mytimezone='Etc/UTC';
}

?>

<!-- @if($datediff >= 7 && Auth::User()->planid >= 3)
    <div class="col-12 col-sm-8 offset-sm-1 mb-2 ">
      <div class="card bg-light">
        @if(Auth::User()->isintegrated == 0)
          <div id="integrationmessage" class="card-body border-top">
            
            <div class="small">          
              The Prime app is optimized for loading badges quickly. For this, a one-time manual integration is required. Apps that automate integration most likely slow down the store and make the experience sloppy in the long run. 
            </div>
            <div class="mt-3">
              <a class="btn btn-outline-primary" target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000279766-highlight-integration-guide">
                Integration Guide
                <div class="small text-secondary border-top mt-1"><span class="small">Do it yourself</span></div>
              </a>
              <span class="mx-2">OR</span>
              <a id="requestintegrationbutton" class="btn btn-outline-primary"  onclick="openrequestmodal();" href="javascript:void(0)">
                Request Integration
                <div class="small text-secondary border-top mt-1"><span class="small">Support team integrates</span></div>
              </a>
            </div>
            
          </div>
        @else 
        
          <div id="integrationdone" class="p-3 text-success ">
              Integration completed successfully<span class="ml-2 fa fa-check-square"></span>
          </div>

        @endif
      </div>
    </div>
  @endif    -->

<div class="row">
    <div class="col-12 col-sm-8 offset-sm-1 mb-2 d-none ">
       <div class="card bg-light">
            <div class="card-body border-top">
                <div class="small">
                    To display the highlights on your store you need to add a line of app installation code in your Shopify store theme.
                </div>
                <a class="btn btn-outline-primary mt-3" href="{{asset('help')}}">Install Guide</a>
                <div class="small mt-2 font-italic">
                    Apps that automate installation most likely slow down the store and make the experience sloppy in the long run. Our app is designed for speed and hence one-time manual installation is required. 
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4 d-none">
        <div class="card bg-light">
            <div class="card-body ">
                <h5>
                    App Switch
                </h5>
                <div class="small">          
                        Use this switch to disable the app in case you are not ready yet.
                </div>
            </div>
        </div>  
    </div>
    
    <div class="col-12 col-sm-8 offset-sm-1">
        <div class="row"><div class="col">
            <div class="float-left mt-4">
                <a href="{{asset('createhighlights')}} "class="btn btn-primary float-right">Add Highlight</a>
            </div>
            <div class="float-right">
            <div classs="">
            <span class="h6">Master Switch</span>
            </div>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-outline-primary {{ Auth::User()->ishighlightshow == 1 ? "active":""}}">

                    <input type="radio" name="options" onchange="activeHighlight( 1 );" autocomplete="off" checked=""> ON
                </label>
                <label class="btn btn-outline-primary {{ Auth::User()->ishighlightshow == 0 ? "active":""}}">
                    <input type="radio" name="options" onchange="activeHighlight( 0 );" id="option3" autocomplete="off"> OFF
                </label>
                </div>    
            </div>
            </div>
        </div>

    @if( !empty( count($highlights) ) )
    <div class="w-100 mt-3">
        <table id="myTable" class="table table-responsive table-bordered" style="max-height:600px;">
          <thead class="thead-light">
          <tr>
            <th class="text-center">#</th>
            <th  class="text-center"> Highlights</th>
            <th class="text-center">Actions</th>
            <th class="text-center">Group</th>
            <th class="text-center">Priority</th>
          </tr>
        </thead>
        <colgroup>
          <col width="5%">
          <col width="40%">
          <col width="35%">
          <col width="15%">
          <col width="5%">
        </colgroup>
        <tbody style="cursor: move;">
          @php
          $apprerance = json_decode( $highlightstyle['appearance'] , true );
          $highlightPriority = array();
          @endphp

          @foreach( $highlights as $index =>  $highlight )
          @php

            $highlightPriority[$highlight['producthighlightid']] = $highlight['priority'];
            
            $badgRules  = json_decode( $highlight['displayrules'] , true );

            if(strpos($highlight['title'], 'product.metafields') !== false){
              $highlight['title'] = str_replace("product.metafields","primehm",$highlight['title']);
            }

          @endphp
          <tr highlightid="{{$highlight['producthighlightid']}}" id="row_{{$highlight['producthighlightid']}}">
            <td class="text-center index" highlightid="{{$highlight['producthighlightid']}}">
              {{$index+1}}
            </td>
            <td class="text-center">
              {{-- <a class="text-left" id="title" href="#" target="_blank" title="Black Box Grooming Kit">{{$highlight['title']}}</a> --}}
              <div class="d-inline-block align-middle">

                <div class="" id="badgeStyle_old" style="">
                  <span class="spanStyleleft" style=""></span>
                  <div id="textStyle_old" class="highlighttitle" style="">
                  @if( !isset($highlight['image']) )
                    @php $imgsrc=env('AWS_PATH').'h'.$highlight['producthighlightid']; @endphp
                    <img src="{{ env('AWS_PATH').'h'.$highlight['producthighlightid'] }}" class="responsive-imagep" style="height: 40px;" /> <span class="mydynamicvariable"> {!! isset( $highlight['title'] ) ? $highlight['title'] :''  !!}</span>
                  
                  @else 
                   @php $imgsrc=$highlight['image']; @endphp
                    <img class="dashboardimage" src="{{ isset( $highlight['image'] ) ? $highlight['image'] :''  }}" style="max-height:23px;"/> <span class="mydynamicvariable"> {!! isset( $highlight['title'] ) ? $highlight['title'] :''  !!}</span>
                  @endif
                  </div>
                  <span id="spanStyle" style=""></span>
                </div>

              </div>
              <div class="small text-muted">

              </div>

            </td>
            <td  class="text-center" >
              <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-sm btn-default "  onclick="changeStaus('{{$highlight['producthighlightid']}}' , '{{$highlight['isactive'] }}' );" >
                  <i class="fa fa-toggle-on {{ $highlight['isactive'] == 1 ? 'text-primary':'text-muted' }} "></i>
                </button>
 
                <button type="button" class="btn btn-sm btn-default"  onclick="location.href='{{asset('edithighlight')}}/{{$highlight['producthighlightid']}}'">Edit</button>
                {{-- <button type="button" class="btn btn-sm btn-default">View</button> --}}
                <button type="button" class="btn btn-sm btn-default" onclick="copyPop('{{$highlight['producthighlightid']}}', '{{$imgsrc}}');">Copy</button>
                <button type="button" class="btn btn-sm btn-default" onclick="deletePop('{{$highlight['producthighlightid']}}');">Delete</button>
                
              </div>
            </td>
            <td class="text-center">
              <div class="text-center text-muted">
                @if( Auth::User()->planid > 2 )
                  <select name="highlightgroup" id="grouphighlight" class="form-control metafieldcond form-control-sm" onchange="changegroup(this,'{{$highlight['producthighlightid']}}');" >
                    <option {{ $highlight['highlightgroup'] == 1 ? 'selected':'' }} value="1">1</option>
                    <option {{ $highlight['highlightgroup'] == 2 ? 'selected':'' }} value="2" >2</option>
                    <option {{ $highlight['highlightgroup'] == 3 ? 'selected':'' }} value="3" >3</option>
                  </select>
                @else
                  <select name="highlightgroup" id="grouphighlight" class="form-control metafieldcond form-control-sm" disabled >
                    <option {{ $highlight['highlightgroup'] == 1 ? 'selected':'' }} value="1">1</option>
                    <option {{ $highlight['highlightgroup'] == 2 ? 'selected':'' }} value="2" >2</option>
                    <option {{ $highlight['highlightgroup'] == 3 ? 'selected':'' }} value="3" >3</option>
                  </select>
                @endif

              </div>
            </td>
            <td class="Priority">
              <div class="text-center text-muted">
                <a href="#"><i class="fa fa-bars"></i></a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-12 justify-content-center">
      <center>
        {{-- {{$highlights->render("pagination::bootstrap-4")}} --}}
        {{-- Total - {{ $highlights->total() }} --}}
        Total -{{ count($highlights)}}
        <br> <br>
      </center>
    </div>

    @endif
        <!-- copy Model start -->
        <div id="modalCopyYesNo" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Copy highlight</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="deleteproduct" >
            <div class="font-weight-bold">
              Are your sure you want copy the highlight?
            </div>
            
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesCopyYesNo" type="button" class="btn btn-primary text-right">Confirm</button>
            <button id="btnNoCopyYesNo" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end copy model -->
    <div id="modalConfirmYesNo" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Delete highlight</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="deleteproduct" >
            <div class="font-weight-bold">
              Are your sure you want delete the highlight?
            </div>
            <div class="small mt-2">
              This will remove the highlight from Shopify store.
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesConfirmYesNo" type="button" class="btn btn-primary text-right">Confirm</button>
            <button id="btnNoConfirmYesNo" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if( !empty( count($highlights) ) )
  <div class="accordion my-2" id="accordionExample">
    <div class="card">
      <div class="card-header" id="headingOne">
        <!-- <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          
        </button> -->
        <a class="btn btn-link" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Settings</a>
      </div>
      <!-- <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
        

        <div class="card-body "> -->
      <div class="collapse multi-collapse" id="multiCollapseExample1">
        <div class="card card-body pb-0">

          <form id="formRule" name="formRule" enctype="multipart/form-data">
            {{csrf_field()}}
            <!-- @if( !empty( $highlights_style ) )
              <input type="hidden" name="highlight_style_id" value="{{$highlight_style['id']}}">
            @endif -->
            <div class="form-group row badge_text_show">
              <label for="staticEmail" class="col-3 col-sm-3 col-md-3 col-form-label ">Style</label>
              <div class="col-sm">
                <select id="textposition" name="highlightstyle[highlighttextposition]" class="form-control" onchange="applytextposition();">
                  <option value="1" {{ !empty( $highlight_style['highlighttextposition'] ) && $highlight_style['highlighttextposition'] == '1' ? 'selected' :''}} >Text Right of Icon</option>
                  <option value="2" {{ !empty( $highlight_style['highlighttextposition'] ) && $highlight_style['highlighttextposition'] == '2' ? 'selected' :''}}>Text Below Icon</option>
                </select>
              </div>

              <div class="col-sm">
              
              </div>
              
            </div>
          
              <div class="form-group row badge_text_show">
                <label for="staticEmail" class="col-3 col-sm-3 col-md-3 col-form-label ">Columns</label>
                <div class="col-sm">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Desktop</label>
                    </div>
                    <select id="highlightstyle" name="highlightstyle[highlightstylecolumn]" class="form-control" name="" onchange="">
                      <option value="1" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '1' ? 'selected' :''}}>1</option>
                      <option value="2" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '2' ? 'selected' :''}}>2</option>
                      <option value="3" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '3' ? 'selected' :''}}>3</option>
                      <option value="4" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '4' ? 'selected' :''}}>4</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Tablet</label>
                    </div>
                    @if(!empty( $highlight_style['highlightstyletabcolumn'] ))
                    <select id="highlightstyle2" name="highlightstyle[highlightstyletabcolumn]" class="form-control" name="" onchange="">
                      <option value="1" {{ !empty( $highlight_style['highlightstyletabcolumn'] ) && $highlight_style['highlightstyletabcolumn'] == '1' ? 'selected' : '' }}>1</option>

                      <option value="2" {{ !empty( $highlight_style['highlightstyletabcolumn'] ) && $highlight_style['highlightstyletabcolumn'] == '2' ? 'selected' : '' }}>2</option>

                      <option value="3" {{ !empty( $highlight_style['highlightstyletabcolumn'] ) && $highlight_style['highlightstyletabcolumn'] == '3' ? 'selected' : '' }}>3</option>

                      <option value="4" {{ !empty( $highlight_style['highlightstyletabcolumn'] ) && $highlight_style['highlightstyletabcolumn'] == '4' ? 'selected' : '' }}>4</option>
                    </select>
                    @else
                    <select id="highlightstyle2" name="highlightstyle[highlightstyletabcolumn]" class="form-control" name="" onchange="">
                      <option value="1" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '1' ? 'selected' :'' }}>1</option>

                      <option value="2" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '2' ? 'selected' :'' }}>2</option>

                      <option value="3" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '3' ? 'selected' :'' }}>3</option>

                      <option value="4" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '4' ? 'selected' :'' }}>4</option>
                    </select>
                    @endif
                  </div>
                </div>

                <div class="col-sm">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Mobile</label>
                    </div>
                    @if(!empty( $highlight_style['highlightstylemobcolumn'] ))
                    <select id="highlightstyle3" name="highlightstyle[highlightstylemobcolumn]" class="form-control" name="" onchange="">
                      <option value="1" {{ !empty( $highlight_style['highlightstylemobcolumn'] ) && $highlight_style['highlightstylemobcolumn'] == '1' ? 'selected' : '' }}>1</option>

                      <option value="2" {{ !empty( $highlight_style['highlightstylemobcolumn'] ) && $highlight_style['highlightstylemobcolumn'] == '2' ? 'selected' : '' }}>2</option>

                      <option value="3" {{ !empty( $highlight_style['highlightstylemobcolumn'] ) && $highlight_style['highlightstylemobcolumn'] == '3' ? 'selected' : '' }}>3</option>

                      <option value="4" {{ !empty( $highlight_style['highlightstylemobcolumn'] ) && $highlight_style['highlightstylemobcolumn'] == '4' ? 'selected' : '' }}>4</option>
                    </select>
                    @else
                    <select id="highlightstyle3" name="highlightstyle[highlightstylemobcolumn]" class="form-control" name="" onchange="">
                      <option value="1" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '1' ? 'selected' : '' }}>1</option>

                      <option value="2" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '2' ? 'selected' : '' }}>2</option>

                      <option value="3" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '3' ? 'selected' : '' }}>3</option>

                      <option value="4" {{ !empty( $highlight_style['highlightstylecolumn'] ) && $highlight_style['highlightstylecolumn'] == '4' ? 'selected' : '' }}>4</option>
                    </select>
                    @endif
                  </div>
                </div>

              </div>
            
          <div class="form-group row badge_text_show">
            <label for="staticEmail" class="col-3 col-sm-3 col-md-3 col-form-label ">Body</label>
            <div class="col-sm">
              <select id="highlightshape" class="form-control" name="highlightstyle[highlightshape]" onchange="applyHighlightStyle(this,'Shape');" >
                <option value="1" {{ !empty( $highlight_style['highlightshape'] ) && $highlight_style['highlightshape'] == 1 ? 'selected' :''}} >Rectangle</option>
                <option value="3" {{ !empty( $highlight_style['highlightshape'] ) && $highlight_style['highlightshape'] == 3 ? 'selected' :''}} >Curved Rectangle</option>
                <!-- <option value="2" {{ !empty( $highlight_style['highlightshape'] ) && $highlight_style['highlightshape'] == 2 ? 'selected' :''}} >Oval</option> -->
              </select>
            </div>

              <div class="col-sm">
                    <select id="highlightinternalpadding" class="form-control" name="highlightstyle[highlightshape_padding]" onchange="applyHighlightStyle(this,'Shape_padding');" >
                      <option value="space1"  {{ !empty( $highlight_style['highlightshape_padding'] ) && $highlight_style['highlightshape_padding'] == 'space1' ? 'selected' :''}} >Spacing Small</option>
                      <option value="space2"  {{ !empty( $highlight_style['highlightshape_padding'] ) && $highlight_style['highlightshape_padding'] == 'space2' ? 'selected' :''}} >Spacing Medium</option>
                      <option value="space3"  {{ !empty( $highlight_style['highlightshape_padding'] ) && $highlight_style['highlightshape_padding'] == 'space3' ? 'selected' :''}} >Spacing Large</option>
                      <!-- <option value="space4"  {{ !empty( $highlight_style['highlightshape_padding'] ) && $highlight_style['highlightshape_padding'] == 'space4' ? 'selected' :''}} >Spacing xl</option>
                      <option value="space5"  {{ !empty( $highlight_style['highlightshape_padding'] ) && $highlight_style['highlightshape_padding'] == 'space5' ? 'selected' :''}} >Spacing xxl</option> -->
                    </select>
                  </div>
          

            <div class="col-sm">
              <select id="highlightshadow" class="form-control" name="highlightstyle[shadow]">
                <option {{  empty( $highlight_style['shadow'] ) ? 'selected':'' }} value="" selected="">No Shadow</option>
                <option {{  isset( $highlight_style['shadow'] ) && $highlight_style['shadow'] == 'light'  ? 'selected':'' }} value="light">Shadow</option>
              </select>
            </div>

          </div>

          <div class="form-group row badge_text_show">
            <label for="text" class="col-3 col-sm-3 col-md-3 col-form-label">Border</label>
            <div class="col-sm">
              <select id="highlightbordersize" class="form-control" name="highlightstyle[bordersize]">
                <option {{  empty( $highlight_style['bordersize'] ) ? 'selected':'' }} value="" selected="">No</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '1px'  ? 'selected':'' }} value="1px">1px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '1.5px'  ? 'selected':'' }} value="1.5px">1.5px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '2px'  ? 'selected':'' }} value="2px">2px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '2.5px'  ? 'selected':'' }} value="2.5px">2.5px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '3px'  ? 'selected':'' }} value="3px">3px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '3.5px'  ? 'selected':'' }} value="3.5px">3.5px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '4px'  ? 'selected':'' }} value="4px">4px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '4.5px'  ? 'selected':'' }} value="4.5px">4.5px</option>
                <option {{  isset( $highlight_style['bordersize'] ) && $highlight_style['bordersize'] == '5px'  ? 'selected':'' }} value="5px">5px</option>
              </select>
            </div>

            <div class="col-sm">
              <div id="border-color-picker" class="input-group colorpicker-element">
                <input type="text" name="highlightstyle[bordercolor]" class="form-control borderBgcolhighlight" id="borderBgcolhighlight" value="{{ !empty( $highlight_style['bordercolor'] ) ? $highlight_style['bordercolor'] :'#000000'}}">
              </div>
            </div>

            <div class="col-sm">
              <select id="highlightborderstyle" class="form-control" name="highlightstyle[borderstyle]">
                <option {{  isset( $highlight_style['borderstyle'] ) && $highlight_style['borderstyle'] == 'solid'  ? 'selected':'' }} value="solid" >Solid</option>
                <option {{  isset( $highlight_style['borderstyle'] ) && $highlight_style['borderstyle'] == 'dotted'  ? 'selected':'' }} value="dotted" >Dotted</option>
                <option {{  isset( $highlight_style['borderstyle'] ) && $highlight_style['borderstyle'] == 'dashed'  ? 'selected':'' }} value="dashed" >Dashed</option>
              </select>
            </div>
          </div>

          <div class="form-group row badge_text_show">
            <label for="text" class="col-xl-3 col-3 col-sm-3 col-md-3 col-form-label">Text</label>

            <div class="col-sm">
              <div class="input-group">
                <input class="form-control" type="number" maxlength="2" placeholder="Enter size in px" name="highlightstyle[textsize]" id="badgetextsize" value="{{ isset( $highlight_style['textsize'] ) ?  $highlight_style['textsize'] :'' }}" onfocusout="applyHighlightStyle(this,'Text');" >
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">px</span>
                </div>
              </div>
              <div class="small text-muted">(Max 50px)</div>
            </div>

          
            <div class="col-sm">
              <div id="text-color-picker" class="input-group colorpicker-element">
                <input name="display[textcolor]" type="text" class="form-control buttonsettingtextcolhighlight override" id="buttonsettingtextcolhighlight" value="{{ !empty( $display['textcolor'] ) ? $display['textcolor'] :'#212529'}}">
              </div>
            </div>

            <div class="col-sm">
              <div class="btn-group mr-2" role="group" aria-label="First group">
                <button type="button" id="bold" value="2" onclick="applyHighlightStyle(this,'Font','2');" class="btn btn-outline-secondary btn-lg"><span class="fa fa-bold"></span></button>
                <button type="button" id="underline" value="4" onclick="applyHighlightStyle(this,'Font','4');" class="btn btn-outline-secondary btn-lg"><span class="fa fa-underline"></span></button>
                <button type="button" id="italic" value="3" onclick="applyHighlightStyle(this,'Font','3');" class="btn btn-outline-secondary btn-lg"><span class="fa fa-italic"></span></button>
                <!-- <button type="button" id="strike" value="5" onclick="applyHighlightStyle(this,'Font','5');" class="btn btn-outline-secondary"><span class="fa fa-strikethrough"></span></button> -->
              </div>
              <input type="hidden" id="fontstyle_highlight" name="highlightstyle[fontstyle]" value="{{ !empty( $highlight_style['fontstyle'] ) ? $highlight_style['fontstyle'] :'1'}}">
            </div>

          </div>

          <!-- THIS IS FOR BADGE WITH IMAGE TYPE... -->
          <div class="form-group row  badge_image_show ">
            <label for="text" class="col-3 col-sm-3 col-md-3 col-form-label">Image</label>
            <div class="col-sm-3">
              <div class="input-group">
                <input class="form-control height" type="text" maxlength="3" placeholder="Enter height in px" name="display[height]" id="height" value="{{ isset( $display['height'] ) ?  $display['height'] :'' }}" onfocusout="check_badge_height()" >
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">px</span>
                </div>
              </div>
              <!--<input class="form-control height" type="text" maxlength="2" placeholder="Enter height in px" name="display[height]" id="height" value="{{ isset( $display['height'] ) ?  $display['height'] :'' }}" onfocusout="check_badge_height()" >-->
              <div class="small text-muted">(Max height 150px)</div>
            </div>

            <div class="col-3 col-md-3">
            <select id="highlightImageshadow" class="form-control" name="display[shadow]" style="display: none;">
                <option {{  empty( $display['shadow'] ) ? 'selected':'' }} value="" selected="">No Shadow</option>
                <option {{  isset( $display['shadow'] ) && $display['shadow'] == 'light'  ? 'selected':'' }} value="light">Shadow</option>
              </select>
            </div>

          </div>

          <div class="form-group row badge_image_show d-none">
            <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label">Spacing (px)</label>
            <div class="col-12 col-sm-7 col-md-8 col-lg-8">
              <div class="input-group">
                <input type="text" class="form-control rounded-0" placeholder="Top" value="Top" disabled="">
                <input type="text" class="form-control" placeholder="Top" value="Bottom" disabled="">
                <input type="text" class="form-control" placeholder="Top" value="Left" disabled="">
                <input type="text" class="form-control rounded-0" placeholder="Top" value="Right" disabled="">
              </div>
              <div class="input-group">
                <input style="border-top-left-radius: 0;" type="number" class="form-control " name="highlightstyle[margintop]" id="badgeleftStyleMargintop" placeholder="" onchange="applyHighlightStyle(this,'Margin');" value="{{ isset( $highlightstyle['margintop'] ) ? $highlightstyle['margintop'] : '0' }}">
                <input type="number" class="form-control" name="highlightstyle[marginbottom]" id="badgeleftStyleMarginbottom" placeholder="" onchange="applyHighlightStyle(this,'Margin');" value="{{ isset( $highlightstyle['marginbottom'] ) ? $highlightstyle['marginbottom'] : '0' }}">
                <input type="number" class="form-control" name="highlightstyle[marginleft]" id="badgeleftStyleMarginleft" placeholder="" onchange="applyHighlightStyle(this,'Margin');" value="{{ isset( $highlightstyle['marginleft'] ) ? $highlightstyle['marginleft'] : '0' }}">
                <input style="border-top-right-radius: 0;" type="number" class="form-control" name="highlightstyle[marginright]" id="badgeleftStyleMarginright" placeholder="" onchange="applyHighlightStyle(this,'Margin');" value="{{ isset( $highlightstyle['marginright'] ) ? $highlightstyle['marginright'] : '0' }}">
              </div>
            </div>
          </div>
          <!-- UPTO HERE FOR BADGE WITH IMAGE TYPE... --> 

          <div class="form-group row badge_text_show d-none">
            <label for="staticEmail" class="col-6 col-sm-5 col-md-4 col-form-label ">Left Shape</label>
            <div class="col-12 col-sm-7 col-md-8 col-lg-8">
              <select id="badgeleftStyle" class="form-control" name="highlightstyle[left]" onchange="applyHighlightStyle(this,'Left');">
                <option value="1" {{ isset( $highlightstyle['left'] ) && $highlightstyle['left'] == 1 ? 'selected':'' }} >Square</option>
                <option {{ isset( $highlightstyle['left'] ) && $highlightstyle['left'] == 2 ? 'selected':'' }} value="2" {{ empty( $highlightstyle ) ? 'selected':'' }}>Round</option>
                {{-- <option {{ isset( $highlightstyle['left'] ) && $highlightstyle['left'] == 3 ? 'selected':'' }} value="3">Angled</option>
                <option {{ isset( $highlightstyle['left'] ) && $highlightstyle['left'] == 4 ? 'selected':'' }} value="4">Ribbon</option> --}}
              </select>
            </div>
          </div>

          <div class="form-group row badge_text_show d-none">
            <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Right Shape</label>
            <div class="col-12 col-sm-7 col-md-8">
              <select id="badgerightStyle" class="form-control" name="highlightstyle[right]" onchange="applyHighlightStyle(this,'Right');">
                <option {{ isset( $highlightstyle['right'] ) && $highlightstyle['right'] == 1 ? 'selected':'' }} value="1">Square</option>
                <option {{ isset( $highlightstyle['right'] ) && $highlightstyle['right'] == 2 ? 'selected':'' }} value="2" {{ empty( $highlightstyle ) ? 'selected':'' }} >Round</option>
                {{-- <option {{ isset( $highlightstyle['right'] ) && $highlightstyle['right'] == 3 ? 'selected':'' }} value="3">Angled</option>
                <option {{ isset( $highlightstyle['right'] ) && $highlightstyle['right'] == 4 ? 'selected':'' }} value="4" {{ empty( $highlightstyle ) ? 'selected':'' }} >Ribbon</option> --}}
              </select>
            </div>
          </div>

          <div class="form-group row badge_text_show d-none">
            <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Background</label>
            <div class="col-6 col-md-4">

              <div class="row d-none">
                <div class="col-4 col-md-3 d-none">
                  <div class="form-check">
                    <input class="form-check-input bgcolortype" type="radio" name="display[bgcolortype]" id="bgcolortype" value="2" {{ !empty( $display['bgcolortype'] ) && $display['bgcolortype'] == 2 ? 'checked' :''}}>
                    <label class="form-check-label" for="inlineRadio1">Simple</label>
                  </div>
                </div>
                <!--<div class="col-8 col-md-9">-->
                <div class="col-12">
                  <div id="bg-color-picker" class="input-group colorpicker-element">
                    <input type="text" name="display[bgcolor]" class="form-control buttonsettingBgcol " id="buttonsettingBgcol" value="{{ !empty( $display['bgcolor'] ) ? $display['bgcolor'] :'#ffc107'}}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-4 col-md-3 d-none">
                  <div class="form-check">
                    <input class="form-check-input bgcolortype" type="radio" name="display[bgcolortype]" id="bgcolortype" value="2" {{ !empty( $display['bgcolortype'] ) && $display['bgcolortype'] == 2 ? 'checked' :''}} {{ empty( $display['bgcolortype'] ) ? 'checked' :''}}>
                    <label class="form-check-label" for="inlineRadio2">Gradient</label>
                  </div>
                </div>

                <div class="col-12">
                  <div id="bg-color-picker" class="input-group colorpicker-element">
                    <div class="w-50 pr-3">
                    <input type="text" name="display[gradbgcolorfirst]" class="form-control gradientBgcol_first " id="gradientBgcol_first" value="{{ !empty( $display['gradbgcolorfirst'] ) ? $display['gradbgcolorfirst'] :'#ffc107'}}">
                    </div>
                    <div class="w-50 pl-3">
                      <input type="text" name="display[gradbgcolorsecond]" class="form-control gradientBgcol_second " id="gradientBgcol_second" value="{{ !empty( $display['gradbgcolorsecond'] ) ? $display['gradbgcolorsecond'] :'#ffc107'}}">
                    </div>
                  </div>
                </div>
                
              </div>            
            </div>

            <!-- <div class="col-6 col-md-3">
              <select id="highlightshadow" class="form-control" name="highlightstyle[shadow]">
                <option {{  empty( $highlightstyle['shadow'] ) ? 'selected':'' }} value="" selected="">No Shadow</option>
                <option {{  isset( $highlightstyle['shadow'] ) && $highlightstyle['shadow'] == 'light'  ? 'selected':'' }} value="light">Shadow</option>
              </select>
            </div> -->

          </div>

          <div class="form-group row badge_text_show d-none" id="animationdiv" > 
            <label for="text" class="col-12 col-sm-5 col-md-4 col-form-label">Effects</label>
            <div class="col-4 ">
              <select id="badgeanimation" class="form-control" name="highlightstyle[animation]" onchange="seteffect();">
                <option {{  empty( $highlightstyle['animation'] ) ? 'selected':'' }} value="No Animation" selected="">No Animation</option>
                <option {{  isset( $highlightstyle['animation'] ) && $highlightstyle['animation'] == 'slide'  ? 'selected':'' }} value="slide">Slide In</option>
                <option {{  isset( $highlightstyle['animation'] ) && $highlightstyle['animation'] == 'pop'  ? 'selected':'' }} value="pop">Pop</option>
                <option {{  isset( $highlightstyle['animation'] ) && $highlightstyle['animation'] == 'groove'  ? 'selected':'' }} value="groove">Groove</option>
                <option {{  isset( $highlightstyle['animation'] ) && $highlightstyle['animation'] == 'fly'  ? 'selected':'' }} value="fly">Fly</option>
                <option {{  isset( $highlightstyle['animation'] ) && $highlightstyle['animation'] == 'swerve'  ? 'selected':'' }} value="swerve">Swerve</option>
              </select>
            </div>
          </div>

          <div class="form-group row d-none">
            <label for="staticEmail" class="col-12 col-sm-5 col-md-4 col-form-label ">Mobile Text Size</label>
            <div class="col-12 col-sm-7 col-md-8 col-lg-8">
              <select id="mobiletextsize" class="form-control" name="highlightstyle[mtextsize]" onchange="applyMobleBadgeStyle(this,'Text');">
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '12px' ? 'selected':'' }} value="12px">12px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '13px' ? 'selected':'' }} {{ empty( $highlightstyle ) ? 'selected':'' }}  value="13px">13px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '14px' ? 'selected':'' }} value="14px">14px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '15px' ? 'selected':'' }} value="15px">15px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '16px' ? 'selected':'' }} value="16px" >16px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '17px' ? 'selected':'' }} value="17px">17px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '18px' ? 'selected':'' }} value="18px" >18px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '19px' ? 'selected':'' }} value="19px" >19px</option>
                <option {{ isset( $highlightstyle['mtextsize'] ) && $highlightstyle['mtextsize'] == '20px' ? 'selected':'' }} value="20px" >20px</option>
              </select>
            </div>
          </div>


        </div>  


        <div class="card">
          <div class="card-body text-right">
            <button type="button" onclick="location.href='{{asset('highlights')}}'" class="btn btn-default">Cancel</button>
            <button type="button" onclick="saveRule('#formRule');" class="btn btn-primary savebtn">Save</button>
          </div>
        </div>
        </form>

      </div>
    </div>

    <div class="card">
      <div class="card-header" id="headingThree">
        <h2 class="mb-0">
          <!-- <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            2. Preview
          </button> -->
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2">Preview</button>
        </h2>
      </div>

      <!-- <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
        <div class="card-body"> -->
      <div class="collapse multi-collapse" id="multiCollapseExample2">
        <div class="card card-body">

          <div class="col-12">
            <div class="mb-2 mt-2 bg-white" style="">
              <!-- <div class="d-inline-block"> -->
              <div class="">
                @if( !empty( count($highlights_active) ) )
                

                  <div class="primeHighlights prime-container " id="highlightStyle" style="box-sizing: border-box; color: rgb(33, 37, 41);  position: relative;" >
                  <div class="primehWrapper prime-pb-1 prime-row prime-pt-2 prime-pl-2 prime-pr-2">

                      @foreach( $highlights_active as $index =>  $highlight )

                        @php
                        
                          if(strpos($highlight['title'], 'product.metafields') !== false){
                            $highlight['title'] = str_replace("product.metafields","primehm",$highlight['title']);
                          }
                        @endphp

                        <!-- <div class="primehDesktop primehTooltip prime-px-0" onmouseover="tooltip('{{ isset( $highlight['tooltip'] ) ? $highlight['tooltip'] :''  }}',this);" > -->

                        

                        <!-- <div class="primehDesktop primehTooltip prime-px-0" data-tippy-content ="{{ isset( $highlight['tooltip'] ) ? $highlight['tooltip'] :''  }}" > -->

                        <div class="primehDesktop primehTooltip prime-px-0" >

                          <div class="primehImageOuter prime-px-2 prime-py-1  prime-d-table-cell prime-align-middle" >  
                              @if( !isset($highlight['image']) )
                                <img  class="primehImage prime-d-block prime-mx-auto" src="{{ env('AWS_PATH').'h'.$highlight['producthighlightid'] }}" style="height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};width:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }}; max-height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};max-width:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};" /> 
                              
                              @else 
                                <img class="primehImage prime-d-block prime-mx-auto" src="{{ isset( $highlight['image'] ) ? $highlight['image'] :''  }}"  style="height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};width:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }}; max-height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};max-width:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};">
                              @endif
                          </div>
                          <div class="primehText prime-d-table-cell prime-text-center prime-align-middle " style="white-space: normal; overflow: hidden; font-size: 16px;">

                          {{ isset( $highlight['title'] ) ? $highlight['title'] :''  }}</div>
                        </div>


                      @endforeach
                    </div>
                  </div>
                @else
                 

                  <div class="primeHighlights prime-container " id="highlightStyle" style="box-sizing: border-box; color: rgb(33, 37, 41);  position: relative;" >
                     <div class="primehWrapper prime-pb-1 prime-row prime-pt-2 prime-pl-2 prime-pr-2">
                     
                        <div class="primehDesktop prime-px-0" >
                          <div class="primehImageOuter prime-px-2 prime-py-1  prime-d-table-cell prime-align-middle" >  
                            <img class="primehImage prime-d-block prime-mx-auto" src="https://img.icons8.com/color/3x/free-shipping.png"  style="height:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};width:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }}; max-height: {{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};max-width:{{ isset( $display['height'] ) ?  $display['height'].'px' :'' }};" >
                          </div>

                          
                          <div class="primehText prime-d-table-cell prime-text-center prime-align-middle " style="white-space: normal; overflow: hidden; font-size: 16px;">24-hour Fast Free Shipping</div>
                        </div>
                      </div>
                     
                  </div>
                @endif

              </div>
            </div>
          </div>

        </div>


      </div>
    </div>
  </div>
  @endif
  



  

  </div>

<div class="col-12 col-sm-3 mb-2 ">
<div class="card bg-light">
<div class="card-body border-top">
  <div>
  <div class="font-weight-bold">Highlights</div>
<div class="small mt-1">
Increase conversions by displaying the main features of the product in a graphical information box.
<div class="mt-1"><a href="https://getinstashop.freshdesk.com/support/solutions/articles/81000279765-what-are-highlights" target="_blank" class="">Know More<span class="fa fa-caret-right ml-1"></span></a></div>
</div></div>
<hr>
<div>
  <div class="font-weight-bold">Installation</div>
  <div class="small mt-1">To display the highlight box you need to add a line of app installation code in your Shopify store theme. <div class="mt-1"><a target="_blank" href="https://getinstashop.freshdesk.com/support/solutions/articles/81000279766-highlight-integration-guide" class="">Install Guide<span class="fa fa-caret-right ml-1"></span></a></div>
    <div class="mt-1">
      <a onclick="openrequestmodal();" href="javascript:void(0)" class="">Request Installation<span class="fa fa-caret-right ml-1"></span></a>
    </div>
    <div class="mt-1 text-success p-2 border bg-white isIntegrationComplete">Integration completed<span class="ml-2 fa fa-check-square"></span></div>

</div></div>
  </div>
</div>
    @if(Auth::user()->planid < 3)
    <div id="upgradenotice" class="mt-3">
        <div class="alert alert-danger">Highlights will only be visible for users on Pro Plan and above.<br>
            <a href="/plans" class="btn btn-sm btn-danger mt-1">Upgrade Now</a>
        </div>
    </div>
@endif
</div>
  <!-- End of Desgin Here  -->

   


  </div>

  <div id="integrationrequest" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Request Integration</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="" >
            <div class="">
              Shopify experts will analyze your theme and add the integration code for you. 
              This is typically done in a few hours. You will get a notification email as soon as the integration is complete. 
              Meanwhile, you can continue adding Highlights in the dashboard.
              <br>
              <div class="mt-3 text-muted">This is a free service.</div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesIntegrate" type="button" class="btn btn-primary text-right">Request Integration</button>
            <button id="btnNoIntegrate" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  @include('dashboard.highlightshadow')
   
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js" ></script> 

  <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
  <script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>

  <script>

    $(document).ready(function (){
      // const instances = tippy('[data-tippy-content]');
			// instances.destroy();
    }); 

    // function tooltip(tooltip,newelement) {
    //   if(tooltip != ''){
    //       newelement.setAttribute("data-tippy-content", tooltip);
    //       tippy('[data-tippy-content]');
          
    //     }
    // }

  </script>
  
  <script type="text/javascript">

    $(document).ready(function(){
      var integrationCom = <?php echo Auth::user()->isintegratedhighlight ; ?>;
      if(integrationCom == 0){
        $('.isIntegrationComplete').hide();
      }else{
        $('.isIntegrationComplete').show();
      }
    });

    function openrequestmodal(){
      $('#integrationrequest').modal({
          backdrop: 'static',
          keyboard: false
      });
    }

    $("#btnYesIntegrate").click(function () {

      $('#integrationrequest').modal("hide");
      $('#loadingalert').show();
      $.ajax({
          url:"{{asset('sendintegratemailhighlights')}}",
          method:'POST',
          data :{'_token':'{{csrf_token()}}'},
          success:function( response ){
            $('#loadingalert').hide();
            $('#requestmessage').text(response.msg);
            toggleAlert();
          }
      })
    });

    $("#btnNoIntegrate").click(function () {
      $('#integrationrequest').modal("hide");
    });
    

    var primehighlightid = '';
    var primemetaid = '';
    var $confirm = $("#modalConfirmYesNo");
    var $copy = $("#modalCopyYesNo");
    var highlightPriority = @json( isset($highlightPriority) ? $highlightPriority : '' );

    var stylevalue = $('#highlightstyle').val();
    if( stylevalue == 1 ){

      $('.primehDesktop').addClass('prime-col-md-12');

      } else if ( stylevalue == 2) {

      $('.primehDesktop').addClass('prime-col-md-6');

      } else if ( stylevalue == 3 ) {

      $('.primehDesktop').addClass('prime-col-md-4');

      }else if ( stylevalue == 4 ) {

      $('.primehDesktop').addClass('prime-col-md-3');

    }
    

    $("#highlightstyle").change(function(){
      
      var stylevalue = $(this).val();
      if( stylevalue == 1 ){

        $('.primehDesktop').addClass('prime-col-md-12');
        $('.primehDesktop').removeClass('prime-col-md-6');
        $('.primehDesktop').removeClass('prime-col-md-4');
        $('.primehDesktop').removeClass('prime-col-md-3');

        } else if ( stylevalue == 2) {

        $('.primehDesktop').addClass('prime-col-md-6');
        $('.primehDesktop').removeClass('prime-col-md-12');
        $('.primehDesktop').removeClass('prime-col-md-4');
        $('.primehDesktop').removeClass('prime-col-md-3');

        } else if ( stylevalue == 3 ) {

        $('.primehDesktop').addClass('prime-col-md-4');
        $('.primehDesktop').removeClass('prime-col-md-6');
        $('.primehDesktop').removeClass('prime-col-md-12');
        $('.primehDesktop').removeClass('prime-col-md-3');

        }
        else if ( stylevalue == 4 ) {

        $('.primehDesktop').addClass('prime-col-md-3');
        $('.primehDesktop').removeClass('prime-col-md-6');
        $('.primehDesktop').removeClass('prime-col-md-12');
        $('.primehDesktop').removeClass('prime-col-md-4');


      }


    });

    $("#myTable tbody").sortable({
      helper: fixHelperModified,
      stop: updateIndex
    }).disableSelection();

    $("tbody").sortable({
      distance: 5,
      delay: 100,
      opacity: 0.6,
      cursor: 'move',
      update: function( data ) {

      }
    });


    

    function getPriorityOrder( ){
      let objBadgeOrder = {};
      $('table tr ').each(function(){
        let badgeid = $(this).attr('producthighlightid');
        if( typeof badgeid != 'undefined'){
          let index = $(this).find('td.index').text();
          objBadgeOrder[badgeid] = parseInt( index );
        }
        
      });

      if( JSON.stringify( badgePriority ) === JSON.stringify(objBadgeOrder) ){
        return false;
      }
      $.ajax( {
        url:"{{asset('updatehighlight-priority')}}",
        method:'POST',
        data :{'highlight':JSON.stringify( objBadgeOrder),'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);
          badgePriority = objBadgeOrder;
          toggleAlert();
        }
      });

    }

    $(document).ready(function (){
      font_style();
      updateHighlightstyle();
      set_height();
      set_padding_class();
      applytextposition();
    }); 

    $("#highlightinternalpadding").change(function(){
      set_padding_class();
    });

    // function applytextposition(){
    //   var TextPosition = $('#textposition').val();
    //   if(TextPosition == '1'){

    //     $(".primehImageOuter").removeClass("prime-d-table-cell");
    //     $(".primehImageOuter").addClass("prime-d-block"); 

    //     $(".primehText").removeClass("prime-d-table-cell prime-text-center");
    //     $(".primehText").addClass("prime-d-block prime-text-left"); 

    //   }else{
        
    //     $(".primehImageOuter").removeClass("prime-d-block");
    //     $(".primehImageOuter").addClass("prime-d-table-cell"); 

    //     $(".primehText").removeClass("prime-d-block prime-text-left");
    //     $(".primehText").addClass("prime-d-table-cell prime-text-center"); 

    //   }

    // }

    function applytextposition(){
      var TextPosition = $('#textposition').val();
      if(TextPosition == '1'){

        $(".primehImageOuter").removeClass("prime-d-block");
        $(".primehImageOuter").addClass("prime-d-table-cell"); 

        $(".primehText").removeClass("prime-d-block prime-text-center");
        $(".primehText").addClass("prime-d-table-cell prime-text-left"); 

      }else{
        
        $(".primehImageOuter").removeClass("prime-d-table-cell");
        $(".primehImageOuter").addClass("prime-d-block"); 

        $(".primehText").removeClass("prime-d-table-cell prime-text-left");
        $(".primehText").addClass("prime-d-block prime-text-center"); 

      }

    }

    function set_padding_class(){
      
      var space = $('#highlightinternalpadding').val();
      if(space == 'space1'){

        $(".primehWrapper").addClass("prime-pt-1 prime-pl-1 prime-pr-1"); 
        $(".primehWrapper").removeClass("prime-pt-2 prime-pl-2 prime-pr-2 prime-pt-3 prime-pl-3 prime-pr-3 prime-pt-4 prime-pl-4 prime-pr-4 prime-pt-5 prime-pl-5 prime-pr-5");


        $(".primehDesktop").addClass("prime-pb-1"); 
        $(".primehDesktop").removeClass("prime-pb-2 prime-pb-3 prime-pb-4 prime-pb-5");
        
        // $(".primehImageOuter").addClass("prime-pr-2");
        // $(".primehImageOuter").removeClass("prime-pr-1 prime-pr-3 prime-pr-4 prime-pr-5"); 

      }else if(space == 'space2'){
        $(".primehWrapper").addClass("prime-pt-2 prime-pl-2 prime-pr-2"); 
        $(".primehWrapper").removeClass("prime-pt-1 prime-pl-1 prime-pr-1 prime-pt-3 prime-pl-3 prime-pr-3 prime-pt-4 prime-pl-4 prime-pr-4 prime-pt-5 prime-pl-5 prime-pr-5");

        $(".primehDesktop").addClass("prime-pb-2"); 
        $(".primehDesktop").removeClass("prime-pb-1 prime-pb-3 prime-pb-4 prime-pb-5");

        // $(".primehImageOuter").addClass("prime-pr-3");
        // $(".primehImageOuter").removeClass("prime-pr-1 prime-pr-2 prime-pr-4 prime-pr-5"); 

      }else if(space == 'space3'){
        $(".primehWrapper").addClass("prime-pt-3 prime-pl-3 prime-pr-3"); 
        $(".primehWrapper").removeClass("prime-pt-1 prime-pl-1 prime-pr-1 prime-pt-2 prime-pl-2 prime-pr-2 prime-pt-4 prime-pl-4 prime-pr-4 prime-pt-5 prime-pl-5 prime-pr-5");

        $(".primehDesktop").addClass("prime-pb-3"); 
        $(".primehDesktop").removeClass("prime-pb-1 prime-pb-2 prime-pb-4 prime-pb-5");

        // $(".primehImageOuter").addClass("prime-pr-4");
        // $(".primehImageOuter").removeClass("prime-pr-1 prime-pr-2 prime-pr-3 prime-pr-5"); 

      }else if(space == 'space4'){
        $(".primehWrapper").addClass("prime-pt-4 prime-pl-4 prime-pr-4"); 
        $(".primehWrapper").removeClass("prime-pt-1 prime-pl-1 prime-pr-1 prime-pt-2 prime-pl-2 prime-pr-2 prime-pt-3 prime-pl-3 prime-pr-3 prime-pt-5 prime-pl-5 prime-pr-5");

        $(".primehDesktop").addClass("prime-pb-4"); 
        $(".primehDesktop").removeClass("prime-pb-1 prime-pb-2 prime-pb-3 prime-pb-5");

        // $(".primehImageOuter").addClass("prime-pr-5");
        // $(".primehImageOuter").removeClass("prime-pr-1 prime-pr-2 prime-pr-3 prime-pr-4"); 

      }else if(space == 'space5'){
        $(".primehWrapper").addClass("prime-pt-5 prime-pl-5 prime-pr-5"); 
        $(".primehWrapper").removeClass("prime-pt-1 prime-pl-1 prime-pr-1 prime-pt-2 prime-pl-2 prime-pr-2 prime-pt-3 prime-pl-3 prime-pr-3 prime-pt-4 prime-pl-4 prime-pr-4");

        $(".primehDesktop").addClass("prime-pb-5"); 
        $(".primehDesktop").removeClass("prime-pb-1 prime-pb-2 prime-pb-3 prime-pb-4");

        // $(".primehImageOuter").addClass("prime-pr-5");
        // $(".primehImageOuter").removeClass("prime-pr-1 prime-pr-2 prime-pr-3 prime-pr-4"); 

      }

    }

    function set_height(){
      var height = $('#height').val();
      if(height){
        if(height > 150){
          height = 150;
        }
        // $('#highlightImageStyle').attr('style','max-height:'+ height +'px');
        $('.primehDesktop').find('img.primehImage').css('max-height', height +'px');
        $('.primehDesktop').find('img.primehImage').css('height', height +'px');
        $('.primehDesktop').find('img.primehImage').css('max-width', height +'px');
        $('.primehDesktop').find('img.primehImage').css('width', height +'px');
      }else{
        // $('#highlightImageStyle').attr('style','max-height:25px');
        $('.primehDesktop').find('img.primehImage').css('max-height','25px');
        $('.primehDesktop').find('img.primehImage').css('height', height +'px');
        $('.primehDesktop').find('img.primehImage').css('max-width', height +'px');
        $('.primehDesktop').find('img.primehImage').css('width', height +'px');
      }
    }

    $("#height").change(function(){
      var height = $('#height').val();
      if(height){
        if(height > 150){
          height = 150;
        }
        // $('#highlightImageStyle').attr('style','max-height:'+ height +'px');
        $('.primehDesktop').find('img.primehImage').css('max-height', height +'px');
        $('.primehDesktop').find('img.primehImage').css('height', height +'px');
        $('.primehDesktop').find('img.primehImage').css('max-width', height +'px');
        $('.primehDesktop').find('img.primehImage').css('width', height +'px');
      }else{
        // $('#highlightImageStyle').attr('style','max-height:25px');
        $('.primehDesktop').find('img.primehImage').css('max-height','25px');
        $('.primehDesktop').find('img.primehImage').css('height', '25px');
        $('.primehDesktop').find('img.primehImage').css('max-width', '25px');
        $('.primehDesktop').find('img.primehImage').css('width', '25px');
      }
      
    });

    function check_badge_height(){
      var height = $('#height').val();
      if(height > 150){
        var text = "Height cannot be more than 150px";
        alert_error_message(text);
        $('#height').val('150');
        return false;
      }else if(height == ''){
        var text = "Height should not be empty";
        alert_error_message(text);
        $('#height').val('25');
        return false;
      }
      return true;
    }

    
    
    function alert_error_message(text){
      $('#requestalert').show();
      $('#requestmessage').text(text);
      $("#requestalert").delay(2500).hide(0);
    }

    function saveRule( id ){
      $('#loadingalert').show();
      var font_style_class = $(".btn-secondary");
      var font_values = [];
      for(var i = 0; i < font_style_class.length; i++){
        if( !$(font_style_class[i]).val() ) {                    
    
        }else{
          font_values.push($(font_style_class[i]).val() );
        }
      }
      if(!font_values){
        $('input[name="highlightstyle[fontstyle]"]').val('1');
      }else{
        $('input[name="highlightstyle[fontstyle]"]').val(font_values);
      }
    
      let formData = $(id).serialize();
      // console.log($(id).serializeArray());
      // return false;
      // console.log($('#primehDesktop').attr('style'));return false;
      let appearance = {
                      'desktop':{
                      // 'spanStyleleft': $('.spanStyleleft').attr('style'),
                      'textStyle'    : $('.primehText').attr('style'),
                      'spanStyle'    : $('#spanStyle').attr('style'),
                      'highlightStyle'   : $('#highlightStyle').attr('style'),
                        // 'title'        : $('#primehDesktop').text(),
                      },
                      'mobile':{
                      // 'spanStyleleft': $('.spanStyleleft').attr('style'),
                      // 'mobiprimehDesktop'    : $('#mobiprimehDesktop').attr('style'),
                      // 'mobispanStyle'    : $('#mobispanStyle').attr('style'),
                      // 'mobihighlightStyle'   : $('#mobihighlightStyle').attr('style')
                      },
                    };
      formData +='&appearance='+JSON.stringify( appearance );
      // formData +='&highlightlink='+highlightlink;

      // console.log(formData);return false;

      $('.savebtn').prop('disabled', true);  
      // console.log( formData );
      //   return false;
 
      $.ajax( {
        url:"{{asset('stylehighlight')}}",
        method:'POST',
        data :formData,
        success:function( response ){
        
          let productHtml= '';
          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);

          //console.log(response.submitType);
          if( response.submitType == 'created' ){
            setTimeout(function(){

              //  $('.savebtn').prop('disabled', true);  
              location.href = '{{asset('highlights')}}';
              $('.savebtn').prop('disabled', false);
            },2000);
          
          } else {

            $('.savebtn').prop('disabled', false);
          }
          
          toggleAlert();
        }
      });
    }

    function font_style(){
      var checkvalue = $('#fontstyle_highlight').val();
      var real_values = checkvalue.split(",");
      for(var i = 0; i < real_values.length; i++){
        applyHighlightStyle(this,'Font',real_values[i]);
      }
    }

    var objDynamincMapper = {
                            'Inventory'      :5,
                            // 'product.price_min'   :"$5.00",
                            // 'product.price_max'   :"$6.00",
                            'SaleAmount'  :"$6.00",
                            // 'primebMaxSaleAmount'  :"$7.00",
                            'SalePercent' :"10",
                            'Vendor': 'Allen Solly',
                            'Type': 'Tshirt',
                            'variantCount':2,
                            'OrderCount':10,
                            'CountdownTimer:':'',
                            'RecentSoldCount':'15',
                            'ProductSKU':"ABCD1234",
                            'VisitorCounter':15,
                            // 'primebMaxSalePercent' :"20",
                      } ;

  $(function() {
    
    $('.highlighttitle').each(function(i, obj) {
      var titletest = $(this).find('.mydynamicvariable').text();
      $(this).find('.mydynamicvariable').text(updateTextValue(titletest));
    });

    $('.primehText').each(function(i, obj) {
      var primetest = $(this).text();
      $(this).text(updateTextValue(primetest));
    });

  });

  function updateTextValue( value ){
    // console.log(value);
    if( value.indexOf("Inventory") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("Inventory", objDynamincMapper['Inventory'] );
    }
     if( value.indexOf("SaleAmount") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("SaleAmount", objDynamincMapper['SaleAmount'] );
    }
     if( value.indexOf("SalePercent") != -1 ){
      let percent = "";
      if( value.indexOf("%") != -1){
        percent = "%";
      }
      value = replaceSpecialSym( value );
      value = value.replace("SalePercent", objDynamincMapper['SalePercent']+percent );
    
    } 
     if( value.indexOf("product.vendor") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("product.vendor", objDynamincMapper['Vendor'] );
    
    }  
     if( value.indexOf("product.type") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("product.type", objDynamincMapper['Type'] );
    
    } 
    if( value.indexOf("VariantCount") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("VariantCount", objDynamincMapper['variantCount'] );
    }
    if( value.indexOf("RecentSoldCount") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("RecentSoldCount", objDynamincMapper['RecentSoldCount'] );
    }
    if( value.indexOf("OrderCount") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("OrderCount", objDynamincMapper['OrderCount'] );
    }
    if( value.indexOf("ProductSKU") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("ProductSKU", objDynamincMapper['ProductSKU'] );
    }
    if( value.indexOf("primehm") != -1  ){
      value = replaceSpecialSym( value );
      // value = value.replace("product.metafields", objDynamincMapper['product.metafields'] );
    }
    if( value.indexOf("VisitorCounter") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("VisitorCounter", objDynamincMapper['VisitorCounter'] );
    }
    if( value.indexOf("CountdownTimer:") != -1  ){
      var timezone = <?php echo json_encode($arrNameById[$mytimezone]); ?>;
      // value = value.replace("CountdownTimer:", objDynamincMapper['CountdownTimer:'] );
      // value = value.replace("]]", timezone+" ]]" );

      var pos = value.indexOf("CountdownTimer:");
      var res = value.slice(pos+15, pos+34);
      value = value.replace("[[ CountdownTimer:", "" );
      value = value.replace("]]", "" );

      let end = new Date(toValidDate(res));
      let start = new Date();
      var remaintime = end - start;
      var days = Math.floor(remaintime / (1000 * 60 * 60 * 24));
      var hours = Math.floor((remaintime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((remaintime % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((remaintime % (1000 * 60)) / 1000);
      var realtimeleft = days + "d "+hours + "h "+minutes + "m "+seconds + "s";
      if(seconds < 0){
          realtimeleft = "0d 0h 0m 0s";
      }
      value = value.replace(res, realtimeleft );
          

      // value = value.replace(res, realtimeleft );
      
    }
    if( value.indexOf("[[today") != -1  ){
      var weekdays = <?php echo json_encode(Auth::User()->weekdays); ?>;
      var dateformatselected = '<?php echo Auth::User()->dateformat; ?>';
      var realvalue = value; 

      var pos = realvalue.indexOf("[[today");
      var pos2 = realvalue.indexOf("]]");
      var startres = realvalue.slice(pos+8, pos2);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );
      var pos3 = realvalue.indexOf("[[today");
      var pos4 = realvalue.indexOf("]]");
      var endres = realvalue.slice(pos3+8, pos4);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );

      var wrongdays = weekdays.split(",");
      // var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
      var startDate = new Date(new Date().getTime()+(parseInt(startres)*24*60*60*1000));
      var today = new Date(new Date().getTime());
      var firstinc=0;
      while (today <= startDate) {
        var getday = String(today.getDay());
        if(wrongdays.indexOf(getday) != -1){
          startDate.setDate(startDate.getDate() + 1);
          firstinc++;
        }
        today.setDate(today.getDate() + 1);
      }
      value = value.replace("[[today,"+startres+"]]", moment(startDate).format(dateformatselected));

      var extradays = firstinc+parseInt(endres);
      // console.log(extradays);
      var endDate = new Date(new Date().getTime()+(extradays*24*60*60*1000));
      var todayend = startDate;
      var secinc=0;
      while (todayend <= endDate) {
        var getday = String(todayend.getDay());
        if(wrongdays.indexOf(getday) != -1){
          endDate.setDate(endDate.getDate() + 1);
          secinc++;
        }
        todayend.setDate(todayend.getDate() + 1);
      }
      value = value.replace("[[today,"+endres+"]]", moment(endDate).format(dateformatselected));
      
    }

    return value;
  }

  function toValidDate(datestring){
    return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
  }
  
  function replaceSpecialSym( value ){
    value = value.replace("\{\{","");
    value = value.replace("\}\}","");
    value = value.replace("|","");
    value = value.replace("money","");
    value = value.replace("round","");
    value = value.replace("%",'');
    return value;
  }

    var objTextSizeMapper = { "12":19.5, "13":21, "14":23, "15":24, "16":27, "17":28, "18":29, "19":32, "20":33.5 };
    var objLineHeightMapper = { "12":17.5, "13":19, "14":21, "15":22, "16":25, "17":26, "18":27, "19":30, "20":31.5 };
    var bgcolor   = $( "#buttonsettingBgcol" ).val();
    var textColor = $( "#buttonsettingtextcolhighlight" ).val();
    var objBadgeStyle     = '';
    var objBadgeSpanStyle = '';
    var badgeStyleLeft    = '';
    var objBadgeTextStyle = { 'white-space': 'normal', 'overflow': 'hidden' };
    var lineHeight = 16;

    function applyBadgefontStyle( value ){
    
    if( value == 2 ){
      if($("#bold").hasClass("btn-outline-secondary")){
        $("#bold").removeClass("btn-outline-secondary");
        $("#bold").addClass("btn-secondary");
        objBadgeTextStyle['font-weight'] = 'bold';
      }else{
        $("#bold").removeClass("btn-secondary");
        $("#bold").addClass("btn-outline-secondary");
        objBadgeTextStyle['font-weight'] = '';
      }
    }
    if( value == 3 ){
      if($("#italic").hasClass("btn-outline-secondary")){
        $("#italic").removeClass("btn-outline-secondary");
        $("#italic").addClass("btn-secondary");
        objBadgeTextStyle['font-style']  =  'italic';
      }else{
        $("#italic").removeClass("btn-secondary");
        $("#italic").addClass("btn-outline-secondary");
        objBadgeTextStyle['font-style']  =  '';
      }
    }
    if( value == 4 ){
      if($("#underline").hasClass("btn-outline-secondary")){
        $("#underline").removeClass("btn-outline-secondary");
        $("#underline").addClass("btn-secondary");
        objBadgeTextStyle['text-decoration']  =  'underline';
      }else{
        $("#underline").removeClass("btn-secondary");
        $("#underline").addClass("btn-outline-secondary");
        objBadgeTextStyle['text-decoration']  =  'none';
      }
    }
    // console.log(objBadgeTextStyle);
    // if( value == 5 ){
    //   if($("#strike").hasClass("btn-outline-secondary")){
    //     $("#strike").removeClass("btn-outline-secondary");
    //     $("#strike").addClass("btn-secondary");
    //     objBadgeTextStyle['text-decoration']  =  'line-through';
    //   }else{
    //     $("#strike").removeClass("btn-secondary");
    //     $("#strike").addClass("btn-outline-secondary");
    //     objBadgeTextStyle['text-decoration']  =  '';
    //   }
    // }

  }

    function badgeCssConfig( bcolor , tcolor , lineHeight , height , textsize, margintop= 0, marginright= 0, marginbottom= 0,marginleft=0, shape_padding='space2' ){


      if(typeof tcolor === "undefined" || tcolor == ''){
        tcolor =$('#buttonsettingtextcolhighlight').val();
      }
    
      // var highlight_padding = 'padding: .5rem';

      // if(shape_padding == 'space1'){
      //   highlight_padding = 'padding: .25rem';
      // }else if(shape_padding == 'space3'){
      //   highlight_padding = 'padding: 1em';
      // }else if(shape_padding == 'space4'){
      //   highlight_padding = 'padding: 1.5rem';
      // }else if(shape_padding == 'space5'){
      //   highlight_padding = 'padding: 3rem';
      // }


      var InText =  {
            '11':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';position: relative;padding-left: 12px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },

            '21':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '12':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;padding-left: 12px;border-top-right-radius:1em;border-bottom-right-radius:1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '22':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            // Temporary Changes for Rectangle and Oval
            // '1':{
            //   'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';position: relative;padding-left: 12px;padding-right: 12px;',
            //   'spanStyle':'',
            //   'spanStyleleft':''
            // },
            // '2':{
            //   'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
            //   'spanStyle':'',
            //   'spanStyleleft':''
            // }, 

            '1':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';position: relative;padding-left: 12px;padding-right: 12px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';position: relative;',
                'spanStyle':'',
                'spanStyleleft':''
              },
            '2':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 2em;border-bottom-left-radius: 2em;border-top-right-radius: 2em;border-bottom-right-radius: 2em;',
                'spanStyle':'',
                'spanStyleleft':''
            },
            '3':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 0.3em;border-bottom-left-radius: 0.3em;border-top-right-radius: 0.3em;border-bottom-right-radius: 0.3em;',
                'spanStyle':'',
                'spanStyleleft':''
            }, 
                  
          };

    var InImage =  {
            
            '11':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';position: relative;line-height:'+lineHeight+'px;padding-left: 12px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''

            },
            '21':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;line-height:'+lineHeight+'px;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;padding-right: 12px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '12':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;line-height:'+lineHeight+'px;padding-left: 12px;border-top-right-radius:1em;border-bottom-right-radius:1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '22':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;line-height:'+lineHeight+'px;border-top-left-radius: 1em;border-bottom-left-radius: 1em;padding-left: 15px;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-right: 15px;margin: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            // Temporary Changes for Rectangle and Oval
            '1':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color:'+bcolor+';position: relative;line-height:'+lineHeight+'px;padding-left: 12px;padding-right: 12px;padding: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '2':{
              'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;line-height:'+lineHeight+'px;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;padding: '+margintop+'px '+marginright+'px '+marginbottom+'px '+marginleft+'px;',
              'spanStyle':'',
              'spanStyleleft':''
            },
            '3':{
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 1em;border-bottom-left-radius: 1em;border-top-right-radius: 1em;border-bottom-right-radius: 1em;padding-left: 15px;padding-right: 15px;',
                'highlightStyle':'box-sizing: border-box;color: '+tcolor+';background-color: '+bcolor+';position: relative;border-top-left-radius: 0.3em;border-bottom-left-radius: 0.3em;border-top-right-radius: 0.3em;border-bottom-right-radius: 0.3em;padding:0.5em 1em;',
                'spanStyle':'',
                'spanStyleleft':''
            },  

          };

          // console.log(InText);return false;

    // if(badge_type == 'image'){
      // return InImage;
    // }else{
      return InText;
    // }     

  }

    function updateHighlightstyle( bcolor , tcolor ,lineHeight,height ){
      // let badgeleftStyle  = $("#badgeleftStyle").val();
      // let badgerightStyle = $("#badgerightStyle").val();
      // let indexCss        = badgeleftStyle+''+badgerightStyle;

      var badge_text = $('#badge_text').prop('checked');
      var badge_image = $('#badge_image').prop('checked');
      var badge_type = 'text';
      if(badge_text == true){
        badge_type = 'text';
      }else if(badge_image == true){
        badge_type = 'image';
      }
      
      let highlightStyle  = $("#highlightshape").val();
      let indexCss        = highlightStyle;

      let textsize        = $("#badgetextsize").val();
      let highlightbordersize =  $("#highlightbordersize").val();
      let highlightBorderStyle =  $("#highlightborderstyle").val();
      
      height = objTextSizeMapper[height];
      lineHeight = objLineHeightMapper[lineHeight];

      if( highlightbordersize != "" ){
        highlightbordersize = highlightbordersize.replace("px");
        highlightbordersize = parseInt( highlightbordersize );
        highlightbordersize = highlightbordersize*2;
        height += highlightbordersize;
      }
      
      let margintop  = $("#badgeleftStyleMargintop").val();
      let marginright  = $("#badgeleftStyleMarginright").val();
      let marginbottom  = $("#badgeleftStyleMarginbottom").val();
      let marginleft  = $("#badgeleftStyleMarginleft").val();
      

      let shape_padding  = $("#highlightinternalpadding").val();
    
      let objCssBadge     = badgeCssConfig( bcolor, tcolor, lineHeight, height, textsize, margintop, marginright, marginbottom, marginleft, shape_padding);
      // let objCssBadge     = badgeCssConfig( bcolor , tcolor ,lineHeight ,height,textsize );
      let objBadgeStyle     = objCssBadge[indexCss]['highlightStyle'];
      let objBadgeSpanStyle = objCssBadge[indexCss]['spanStyle'];
      let badgeStyleLeft    = objCssBadge[indexCss]['spanStyleleft'];


      $("#highlightStyle").attr('style', objBadgeStyle);
      $(".primehText").css( objBadgeTextStyle );
      $("#spanStyle").attr('style', objBadgeSpanStyle);
      // $("#highlightStyle").css('font-size',textsize);
      $(".primehText").css('font-size',textsize);
      $(".spanStyleleft").attr( 'style',badgeStyleLeft );
      // $("#mobibadgeStyle").attr( 'style', objBadgeStyle);
      // $("#mobitextStyle").attr( 'style', objBadgeTextStyle);
      // $("#mobispanStyle").attr( 'style', objBadgeSpanStyle);
      updateBadgeGradientwithBorderShadow("desktop");
      
      //reset image bgcolor, color and textsize
      if(badge_type == 'image')
      {
          $("#highlightStyle").css('background-color','transparent');
            $("#highlightStyle").css('color','transparent');
            $("#highlightStyle").css('font-size',0);
      }
      
    }

    function applyHighlightStyle( instance , type , value='' ){
    
      if( value == ''){
        value = $(instance).val();
      }
      if( type == 'Left' || type == 'Right' || type == 'Shape' || type ==  'Margin' || type ==  'Shape_padding' ){
        updateHighlightstyle( bgcolor , textColor ,lineHeight, height );
      }else if( type == 'Font' ){
        applyBadgefontStyle( value );
      }else if( type == 'Text' ){
        if(value > 50 || value < 5 || value == ''){
          var text = "Text Range should be between 5 to 50 px";
          alert_error_message(text);
          value = '20px';
          $("#badgetextsize").val( 20 );
          
          applyBadgeTextStyle( value );

        }else {
          applyBadgeTextStyle( value );
        }
        
      }
      // else if( type == 'Margin' ){
      //   applyBadgeTextStyle( value );
      // }

      $(".primehText").css( objBadgeTextStyle );
      $("#mobitextStyle").css( objBadgeTextStyle );
    }

    function applyBadgeTextStyle( value ){
      // let value = $(instance).val();
      // if( typeof value == 'undefined' ){
      //    console.log(" text value "+ value);
      //   return; 
      // }
      let oldvalue = value;
      value = value.replace("px","");
      value = parseInt( value );
      lineHeight = value;
      height   = value;
      // console.log(">>>>"+lineHeight );
      updateHighlightstyle( bgcolor , textColor ,lineHeight, height );

      // $("#highlightStyle").css('font-size',oldvalue);
      $(".primehText").css('font-size',oldvalue);
      
    }

    
  

    $("#buttonsettingtextcolhighlight").spectrum({
      flat: false,
      showInput: true,
      allowEmpty:true,
      showButtons: false,
      //className: "full-spectrum",
      showInitial: true,
      showPalette: true,
      showSelectionPalette: true,
      maxSelectionSize: 10,
      preferredFormat: "hex",
      clickoutFiresChange: true,
      // showAlpha :true,
      //localStorageKey: "spectrum.homepage",
      // togglePaletteMoreText: 'more',
      // togglePaletteLessText: 'less',
      //selectionPalette: [textColor],
      move: function (color) {
        textColor = color.toHexString();
        $("#highlightStyle").css('color',color.toHexString() );
        $("#mobibadgeStyle").css('color',color.toHexString());
      },
      // show: function () {
      // },
      // beforeShow: function () {
      // },
      hide: function () {
        // el.spectrum("reflow");
      },
      // change: function( value ) {
      // },
      localStorageKey: false,
      // color: textColor,
      palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
      ]
    });

    $(".buttonsettingtextcolhighlight").change(function(){
      textColor  = $(this).val();
      $("#highlightStyle").css('color',textColor );
      // $("#mobibadgeStyle").css('color',textColor);
    });

    function deletePop( highlightid ){
      
      primehighlightid = highlightid;
      // primemetaid = metaid;
      $confirm.modal('show');
    }

    $("#btnYesConfirmYesNo").click(function () {

      $confirm.modal("hide");
      $('#loadingalert').show();

      $.ajax( {
        url:"{{asset('deletehighlight')}}",
        method:'POST',
        data :{'highlightid':primehighlightid,'primemetaid':primemetaid,'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);

          $("#row_"+primehighlightid).remove();
          toggleAlert();

        }

      })

    });


    $("#btnNoConfirmYesNo").click(function () {
      $confirm.modal("hide");
    });

    function copyPop( highlightid, highlightimg ){
      
      primehighlightid = highlightid;
      primehighlightimg = highlightimg;
      $copy.modal('show');
    }

    $("#btnYesCopyYesNo").click(function () {

      $copy.modal("hide");
      $('#loadingalert').show();

      $.ajax( {
        url:"{{asset('copyhighlight')}}",
        method:'POST',
        data :{'highlightid':primehighlightid,'highlightimg':primehighlightimg,'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);

          window.location.replace("edithighlight/"+response.id);

         // $("#row_"+primehighlightid).remove();
          toggleAlert();

        }

      })

    });


    $("#btnNoCopyYesNo").click(function () {
      $copy.modal("hide");
    });


    var fixHelperModified = function(e, tr) {
      var $originals = tr.children();
      var $helper = tr.clone();
      $helper.children().each(function(index) {
        $(this).width($originals.eq(index).width())
      });
      return $helper;
    },
    updateIndex = function(e, ui) {

      $('td.index', ui.item.parent()).each(function (i) {

        $(this).html(i+1);
      });

      getPriorityOrder( );

    };

    $("#myTable tbody").sortable({
      helper: fixHelperModified,
      stop: updateIndex
    }).disableSelection();

    $("tbody").sortable({
      distance: 5,
      delay: 100,
      opacity: 0.6,
      cursor: 'move',
      update: function( data ) {

      }
    });


    function getPriorityOrder( ){

      let objHighlightOrder = {};
      $('table tr ').each(function(){
        let highlightid = $(this).attr('highlightid');
        if( typeof highlightid != 'undefined'){
          let index = $(this).find('td.index').text();
          objHighlightOrder[highlightid] = parseInt( index );
        }
      });

      if( JSON.stringify( highlightPriority ) === JSON.stringify(objHighlightOrder) ){
        return false;
      }

      $.ajax( {

        url:"{{asset('updatehighlight-priority')}}",
        method:'POST',
        data :{'highlight':JSON.stringify( objHighlightOrder),'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);
          highlightPriority = objHighlightOrder;
          //   if( response.status == true ){

          //     setTimeout(function(){
          //     location.reload();

          //   },2000);

          // }

          toggleAlert();

        }

      });


    }

    function changegroup( full, producthighlightid ){

      var group = full.value;  
      let objhighlight = {'producthighlightid':producthighlightid,'group':group, '_token':'{{csrf_token()}}'};
      $('#loadingalert').show();
      $.ajax( {

            url:"{{asset('updatehighlight-group')}}",
            method:'POST',
            data :objhighlight,
            success:function( response ){

            $('#loadingalert').hide();
            $('#requestmessage').text(response.msg);
            // highlightPriority = objhighlightOrder;
          toggleAlert();

          setTimeout( function(){
            location.reload();
          }, 2000);

        }

      });

    }

    function changeStaus( producthighlightid , isactive  ){

      let objHighlight = {'producthighlightid':producthighlightid, 'isactive':isactive, '_token':'{{csrf_token()}}'};
      $('#loadingalert').show();
      $.ajax( {

        url:"{{asset('updatehighlight-status')}}",
        method:'POST',
        data :objHighlight,
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);
          // badgePriority = objBadgeOrder;
          toggleAlert();

          setTimeout( function(){
            location.reload();
          }, 2000);

        }

      });

    }


    function activeHighlight( type  ){
      postData( {'isShowHighlight': type,'_token':'{{csrf_token()}}' } , '{{asset('activatehighlight')}}' );
    }

    function postData( requestData , url ){
    
      $('#loadingalert').show();

      $.ajax({
        method:"POST",
        url : url,
        data :requestData ,
        success : function( data ){
          $('#loadingalert').hide();
          $('#requestmessage').text(data.msg);
          toggleAlert();
          setTimeout(function(){
            location.reload();
          }, 2000);

        }

      });
    }

  </script>


@stop
