@extends('layouts.master')

@section('title')
@parent
<title>Banner</title>
@stop

@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop
<style type="text/css">
  @media (max-width: 576px) { 
    .responsive-imagep{
      height: 22px;
    }
    #groupbanner{
      width: 60px;
    }
  }

  .primenWrapper ul, ol {
    display: block;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    padding-inline-start: 40px;
    list-style-type: revert;
}
.primenWrapper li {
    list-style: inherit;
    list-style-position: inside;
}



.prime-d-block{display:block;}
.prime-d-inline-block{display:inline-block;}

.prime-align-middle{vertical-align:middle;}
  
.prime-m-0{margin: 0rem;} 
.prime-mr-0{margin-right: 0rem;} 
.prime-ml-0{margin-left: 0rem;}
.prime-mt-0{margin-top: 0rem;}
.prime-mb-0{margin-bottom: 0rem;}
  
.prime-m-1{margin: .25rem;} 
.prime-mr-1{margin-right: .25rem;} 
.prime-ml-1{margin-left: .25rem;}
.prime-mt-1{margin-top: .25rem;}
.prime-mb-1{margin-bottom: .25rem;}

.prime-m-2{margin: .5rem;} 
.prime-mr-2{margin-right: .5rem;} 
.prime-ml-2{margin-left: .5rem;}
.prime-mt-2{margin-top: .5rem;}
.prime-mb-2{margin-bottom: .5rem;}

.prime-m-3{margin: 1em;} 
.prime-mr-3{margin-right: 1rem;} 
.prime-ml-3{margin-left: 1rem;}
.prime-mt-3{margin-top: 1rem;}
.prime-mb-3{margin-bottom: 1rem;}

.prime-m-4{margin: 1.5rem;} 
.prime-mr-4{margin-right: 1.5rem;} 
.prime-ml-4{margin-left: 1.5rem;}
.prime-mt-4{margin-top: 1.5rem;}
.prime-mb-4{margin-bottom: 1.5rem;}

.prime-m-5{margin: 3rem;} 
.prime-mr-5{margin-right: 3rem;} 
.prime-ml-5{margin-left: 3rem;}
.prime-mt-5{margin-top: 3rem;}
.prime-mb-5{margin-bottom: 3rem;}

.prime-p-0{padding: 0rem;} 
.prime-pr-0{padding-right: 0rem;} 
.prime-pl-0{padding-left: 0rem;}
.prime-pt-0{padding-top: 0rem;}
.prime-pb-0{padding-bottom: 0rem;}
  
.prime-p-1{padding: .25rem;} 
.prime-pr-1{padding-right: .25rem;} 
.prime-pl-1{padding-left: .25rem;}
.prime-pt-1{padding-top: .25rem;}
.prime-pb-1{padding-bottom: .25rem;}

.prime-p-2{padding: .5rem;} 
.prime-pr-2{padding-right: .5rem;} 
.prime-pl-2{padding-left: .5rem;}
.prime-pt-2{padding-top: .5rem;}
.prime-pb-2{padding-bottom: .5rem;}

.prime-p-3{padding: 1em;} 
.prime-pr-3{padding-right: 1rem;} 
.prime-pl-3{padding-left: 1rem;}
.prime-pt-3{padding-top: 1rem;}
.prime-pb-3{padding-bottom: 1rem;}

.prime-p-4{padding: 1.5rem;} 
.prime-pr-4{padding-right: 1.5rem;} 
.prime-pl-4{padding-left: 1.5rem;}
.prime-pt-4{padding-top: 1.5rem;}
.prime-pb-4{padding-bottom: 1.5rem;}

.prime-p-5{padding: 3rem;} 
.prime-pr-5{padding-right: 3rem;} 
.prime-pl-5{padding-left: 3rem;}
.prime-pt-5{padding-top: 3rem;}
.prime-pb-5{padding-bottom: 3rem;}


.prime-px-2{padding-left:.5rem;padding-right:.5rem;}
.prime-py-1{padding-top:.25rem;padding-bottom:.25rem;}
.prime-mx-auto{margin-left: auto;margin-right: auto;}
.prime-text-center{text-align:center;}
.prime-text-left{text-align:left;}

.prime-px-0{padding-left:0px !important;padding-right:0px !important;}
.primebanPhotoOuter {line-height: 0px;}

  @-ms-viewport{width:device-width}html{box-sizing:border-box;-ms-overflow-style:scrollbar}*,::after,::before{box-sizing:inherit}.prime-container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.prime-container{max-width:540px}}@media (min-width:768px){.prime-container{max-width:720px}}@media (min-width:992px){.prime-container{max-width:960px}}@media (min-width:1200px){.prime-container{max-width:1140px}}.prime-container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.prime-row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.prime-no-gutters{margin-right:0;margin-left:0}.prime-no-gutters>.col,.prime-no-gutters>[class*=col-]{padding-right:0;padding-left:0}.prime-col,.prime-col-1,.prime-col-10,.prime-col-11,.prime-col-12,.prime-col-2,.prime-col-3,.prime-col-4,.prime-col-5,.prime-col-6,.prime-col-7,.prime-col-8,.prime-col-9,.prime-col-auto,.prime-col-lg,.prime-col-lg-1,.prime-col-lg-10,.prime-col-lg-11,.prime-col-lg-12,.prime-col-lg-2,.prime-col-lg-3,.prime-col-lg-4,.prime-col-lg-5,.prime-col-lg-6,.prime-col-lg-7,.prime-col-lg-8,.prime-col-lg-9,.prime-col-lg-auto,.prime-col-md,.prime-col-md-1,.prime-col-md-10,.prime-col-md-11,.prime-col-md-12,.prime-col-md-2,.prime-col-md-3,.prime-col-md-4,.prime-col-md-5,.prime-col-md-6,.prime-col-md-7,.prime-col-md-8,.prime-col-md-9,.prime-col-md-auto,.prime-col-sm,.prime-col-sm-1,.prime-col-sm-10,.prime-col-sm-11,.prime-col-sm-12,.prime-col-sm-2,.prime-col-sm-3,.prime-col-sm-4,.prime-col-sm-5,.prime-col-sm-6,.prime-col-sm-7,.prime-col-sm-8,.prime-col-sm-9,.prime-col-sm-auto,.prime-col-xl,.prime-col-xl-1,.prime-col-xl-10,.prime-col-xl-11,.prime-col-xl-12,.prime-col-xl-2,.prime-col-xl-3,.prime-col-xl-4,.prime-col-xl-5,.prime-col-xl-6,.prime-col-xl-7,.prime-col-xl-8,.prime-col-xl-9,.prime-col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.prime-col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-first{-ms-flex-order:-1;order:-1}.prime-order-last{-ms-flex-order:13;order:13}.prime-order-0{-ms-flex-order:0;order:0}.prime-order-1{-ms-flex-order:1;order:1}.prime-order-2{-ms-flex-order:2;order:2}.prime-order-3{-ms-flex-order:3;order:3}.prime-order-4{-ms-flex-order:4;order:4}.prime-order-5{-ms-flex-order:5;order:5}.prime-order-6{-ms-flex-order:6;order:6}.prime-order-7{-ms-flex-order:7;order:7}.prime-order-8{-ms-flex-order:8;order:8}.prime-order-9{-ms-flex-order:9;order:9}.prime-order-10{-ms-flex-order:10;order:10}.prime-order-11{-ms-flex-order:11;order:11}.prime-order-12{-ms-flex-order:12;order:12}.prime-offset-1{margin-left:8.333333%}.prime-offset-2{margin-left:16.666667%}.prime-offset-3{margin-left:25%}.prime-offset-4{margin-left:33.333333%}.prime-offset-5{margin-left:41.666667%}.prime-offset-6{margin-left:50%}.prime-offset-7{margin-left:58.333333%}.prime-offset-8{margin-left:66.666667%}.prime-offset-9{margin-left:75%}.prime-offset-10{margin-left:83.333333%}.prime-offset-11{margin-left:91.666667%}@media (min-width:576px){.prime-col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-sm-first{-ms-flex-order:-1;order:-1}.prime-order-sm-last{-ms-flex-order:13;order:13}.prime-order-sm-0{-ms-flex-order:0;order:0}.prime-order-sm-1{-ms-flex-order:1;order:1}.prime-order-sm-2{-ms-flex-order:2;order:2}.prime-order-sm-3{-ms-flex-order:3;order:3}.prime-order-sm-4{-ms-flex-order:4;order:4}.prime-order-sm-5{-ms-flex-order:5;order:5}.prime-order-sm-6{-ms-flex-order:6;order:6}.prime-order-sm-7{-ms-flex-order:7;order:7}.prime-order-sm-8{-ms-flex-order:8;order:8}.prime-order-sm-9{-ms-flex-order:9;order:9}.prime-order-sm-10{-ms-flex-order:10;order:10}.prime-order-sm-11{-ms-flex-order:11;order:11}.prime-order-sm-12{-ms-flex-order:12;order:12}.prime-offset-sm-0{margin-left:0}.prime-offset-sm-1{margin-left:8.333333%}.prime-offset-sm-2{margin-left:16.666667%}.prime-offset-sm-3{margin-left:25%}.prime-offset-sm-4{margin-left:33.333333%}.prime-offset-sm-5{margin-left:41.666667%}.prime-offset-sm-6{margin-left:50%}.prime-offset-sm-7{margin-left:58.333333%}.prime-offset-sm-8{margin-left:66.666667%}.prime-offset-sm-9{margin-left:75%}.prime-offset-sm-10{margin-left:83.333333%}.prime-offset-sm-11{margin-left:91.666667%}}@media (min-width:768px){.prime-col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-md-first{-ms-flex-order:-1;order:-1}.prime-order-md-last{-ms-flex-order:13;order:13}.prime-order-md-0{-ms-flex-order:0;order:0}.prime-order-md-1{-ms-flex-order:1;order:1}.prime-order-md-2{-ms-flex-order:2;order:2}.prime-order-md-3{-ms-flex-order:3;order:3}.prime-order-md-4{-ms-flex-order:4;order:4}.prime-order-md-5{-ms-flex-order:5;order:5}.prime-order-md-6{-ms-flex-order:6;order:6}.prime-order-md-7{-ms-flex-order:7;order:7}.prime-order-md-8{-ms-flex-order:8;order:8}.prime-order-md-9{-ms-flex-order:9;order:9}.prime-order-md-10{-ms-flex-order:10;order:10}.prime-order-md-11{-ms-flex-order:11;order:11}.prime-order-md-12{-ms-flex-order:12;order:12}.prime-offset-md-0{margin-left:0}.prime-offset-md-1{margin-left:8.333333%}.prime-offset-md-2{margin-left:16.666667%}.prime-offset-md-3{margin-left:25%}.prime-offset-md-4{margin-left:33.333333%}.prime-offset-md-5{margin-left:41.666667%}.prime-offset-md-6{margin-left:50%}.prime-offset-md-7{margin-left:58.333333%}.prime-offset-md-8{margin-left:66.666667%}.prime-offset-md-9{margin-left:75%}.prime-offset-md-10{margin-left:83.333333%}.prime-offset-md-11{margin-left:91.666667%}}@media (min-width:992px){.prime-col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-lg-first{-ms-flex-order:-1;order:-1}.prime-order-lg-last{-ms-flex-order:13;order:13}.prime-order-lg-0{-ms-flex-order:0;order:0}.prime-order-lg-1{-ms-flex-order:1;order:1}.prime-order-lg-2{-ms-flex-order:2;order:2}.prime-order-lg-3{-ms-flex-order:3;order:3}.prime-order-lg-4{-ms-flex-order:4;order:4}.prime-order-lg-5{-ms-flex-order:5;order:5}.prime-order-lg-6{-ms-flex-order:6;order:6}.prime-order-lg-7{-ms-flex-order:7;order:7}.prime-order-lg-8{-ms-flex-order:8;order:8}.prime-order-lg-9{-ms-flex-order:9;order:9}.prime-order-lg-10{-ms-flex-order:10;order:10}.prime-order-lg-11{-ms-flex-order:11;order:11}.prime-order-lg-12{-ms-flex-order:12;order:12}.prime-offset-lg-0{margin-left:0}.prime-offset-lg-1{margin-left:8.333333%}.prime-offset-lg-2{margin-left:16.666667%}.prime-offset-lg-3{margin-left:25%}.prime-offset-lg-4{margin-left:33.333333%}.prime-offset-lg-5{margin-left:41.666667%}.prime-offset-lg-6{margin-left:50%}.prime-offset-lg-7{margin-left:58.333333%}.prime-offset-lg-8{margin-left:66.666667%}.prime-offset-lg-9{margin-left:75%}.prime-offset-lg-10{margin-left:83.333333%}.prime-offset-lg-11{margin-left:91.666667%}}@media (min-width:1200px){.prime-col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.prime-col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.prime-col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.prime-col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.prime-col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.prime-col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.prime-col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.prime-col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.prime-col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.prime-col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.prime-col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.prime-col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.prime-col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.prime-col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.prime-order-xl-first{-ms-flex-order:-1;order:-1}.prime-order-xl-last{-ms-flex-order:13;order:13}.prime-order-xl-0{-ms-flex-order:0;order:0}.prime-order-xl-1{-ms-flex-order:1;order:1}.prime-order-xl-2{-ms-flex-order:2;order:2}.prime-order-xl-3{-ms-flex-order:3;order:3}.prime-order-xl-4{-ms-flex-order:4;order:4}.prime-order-xl-5{-ms-flex-order:5;order:5}.prime-order-xl-6{-ms-flex-order:6;order:6}.prime-order-xl-7{-ms-flex-order:7;order:7}.prime-order-xl-8{-ms-flex-order:8;order:8}.prime-order-xl-9{-ms-flex-order:9;order:9}.prime-order-xl-10{-ms-flex-order:10;order:10}.prime-order-xl-11{-ms-flex-order:11;order:11}.prime-order-xl-12{-ms-flex-order:12;order:12}.prime-offset-xl-0{margin-left:0}.prime-offset-xl-1{margin-left:8.333333%}.prime-offset-xl-2{margin-left:16.666667%}.prime-offset-xl-3{margin-left:25%}.prime-offset-xl-4{margin-left:33.333333%}.prime-offset-xl-5{margin-left:41.666667%}.prime-offset-xl-6{margin-left:50%}.prime-offset-xl-7{margin-left:58.333333%}.prime-offset-xl-8{margin-left:66.666667%}.prime-offset-xl-9{margin-left:75%}.prime-offset-xl-10{margin-left:83.333333%}.prime-offset-xl-11{margin-left:91.666667%}}.prime-d-none{display:none!important}.prime-d-inline{display:inline!important}.prime-d-inline-block{display:inline-block!important}.prime-d-block{display:block!important}.prime-d-table{display:table!important}.prime-d-table-row{display:table-row!important}.prime-d-table-cell{display:table-cell!important}.prime-d-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}@media (min-width:576px){.prime-d-sm-none{display:none!important}.prime-d-sm-inline{display:inline!important}.prime-d-sm-inline-block{display:inline-block!important}.prime-d-sm-block{display:block!important}.prime-d-sm-table{display:table!important}.prime-d-sm-table-row{display:table-row!important}.prime-d-sm-table-cell{display:table-cell!important}.prime-d-sm-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-sm-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media (min-width:768px){.prime-d-md-none{display:none!important}.prime-d-md-inline{display:inline!important}.prime-d-md-inline-block{display:inline-block!important}.prime-d-md-block{display:block!important}.prime-d-md-table{display:table!important}.prime-d-md-table-row{display:table-row!important}.prime-d-md-table-cell{display:table-cell!important}.prime-d-md-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-md-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media (min-width:992px){.prime-d-lg-none{display:none!important}.prime-d-lg-inline{display:inline!important}.prime-d-lg-inline-block{display:inline-block!important}.prime-d-lg-block{display:block!important}.prime-d-lg-table{display:table!important}.prime-d-lg-table-row{display:table-row!important}.prime-d-lg-table-cell{display:table-cell!important}.prime-d-lg-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-lg-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media (min-width:1200px){.prime-d-xl-none{display:none!important}.prime-d-xl-inline{display:inline!important}.prime-d-xl-inline-block{display:inline-block!important}.prime-d-xl-block{display:block!important}.prime-d-xl-table{display:table!important}.prime-d-xl-table-row{display:table-row!important}.prime-d-xl-table-cell{display:table-cell!important}.prime-d-xl-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-xl-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}@media print{.prime-d-print-none{display:none!important}.prime-d-print-inline{display:inline!important}.prime-d-print-inline-block{display:inline-block!important}.prime-d-print-block{display:block!important}.prime-d-print-table{display:table!important}.prime-d-print-table-row{display:table-row!important}.prime-d-print-table-cell{display:table-cell!important}.prime-d-print-flex{display:-ms-flexbox!important;display:flex!important}.prime-d-print-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important}}.prime-flex-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}@media (min-width:576px){.prime-flex-sm-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-sm-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-sm-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-sm-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-sm-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-sm-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-sm-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-sm-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-sm-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-sm-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-sm-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-sm-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-sm-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-sm-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-sm-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-sm-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-sm-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-sm-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-sm-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-sm-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-sm-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-sm-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-sm-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-sm-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-sm-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-sm-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-sm-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-sm-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-sm-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-sm-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-sm-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-sm-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-sm-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-sm-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}@media (min-width:768px){.prime-flex-md-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-md-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-md-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-md-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-md-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-md-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-md-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-md-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-md-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-md-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-md-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-md-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-md-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-md-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-md-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-md-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-md-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-md-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-md-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-md-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-md-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-md-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-md-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-md-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-md-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-md-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-md-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-md-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-md-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-md-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-md-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-md-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-md-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-md-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}@media (min-width:992px){.prime-flex-lg-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-lg-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-lg-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-lg-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-lg-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-lg-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-lg-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-lg-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-lg-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-lg-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-lg-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-lg-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-lg-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-lg-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-lg-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-lg-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-lg-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-lg-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-lg-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-lg-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-lg-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-lg-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-lg-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-lg-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-lg-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-lg-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-lg-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-lg-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-lg-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-lg-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-lg-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-lg-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-lg-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-lg-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}@media (min-width:1200px){.prime-flex-xl-row{-ms-flex-direction:row!important;flex-direction:row!important}.prime-flex-xl-column{-ms-flex-direction:column!important;flex-direction:column!important}.prime-flex-xl-row-reverse{-ms-flex-direction:row-reverse!important;flex-direction:row-reverse!important}.prime-flex-xl-column-reverse{-ms-flex-direction:column-reverse!important;flex-direction:column-reverse!important}.prime-flex-xl-wrap{-ms-flex-wrap:wrap!important;flex-wrap:wrap!important}.prime-flex-xl-nowrap{-ms-flex-wrap:nowrap!important;flex-wrap:nowrap!important}.prime-flex-xl-wrap-reverse{-ms-flex-wrap:wrap-reverse!important;flex-wrap:wrap-reverse!important}.prime-flex-xl-fill{-ms-flex:1 1 auto!important;flex:1 1 auto!important}.prime-flex-xl-grow-0{-ms-flex-positive:0!important;flex-grow:0!important}.prime-flex-xl-grow-1{-ms-flex-positive:1!important;flex-grow:1!important}.prime-flex-xl-shrink-0{-ms-flex-negative:0!important;flex-shrink:0!important}.prime-flex-xl-shrink-1{-ms-flex-negative:1!important;flex-shrink:1!important}.prime-justify-content-xl-start{-ms-flex-pack:start!important;justify-content:flex-start!important}.prime-justify-content-xl-end{-ms-flex-pack:end!important;justify-content:flex-end!important}.prime-justify-content-xl-center{-ms-flex-pack:center!important;justify-content:center!important}.prime-justify-content-xl-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.prime-justify-content-xl-around{-ms-flex-pack:distribute!important;justify-content:space-around!important}.prime-align-items-xl-start{-ms-flex-align:start!important;align-items:flex-start!important}.prime-align-items-xl-end{-ms-flex-align:end!important;align-items:flex-end!important}.prime-align-items-xl-center{-ms-flex-align:center!important;align-items:center!important}.prime-align-items-xl-baseline{-ms-flex-align:baseline!important;align-items:baseline!important}.prime-align-items-xl-stretch{-ms-flex-align:stretch!important;align-items:stretch!important}.prime-align-content-xl-start{-ms-flex-line-pack:start!important;align-content:flex-start!important}.prime-align-content-xl-end{-ms-flex-line-pack:end!important;align-content:flex-end!important}.prime-align-content-xl-center{-ms-flex-line-pack:center!important;align-content:center!important}.prime-align-content-xl-between{-ms-flex-line-pack:justify!important;align-content:space-between!important}.prime-align-content-xl-around{-ms-flex-line-pack:distribute!important;align-content:space-around!important}.prime-align-content-xl-stretch{-ms-flex-line-pack:stretch!important;align-content:stretch!important}.prime-align-self-xl-auto{-ms-flex-item-align:auto!important;align-self:auto!important}.prime-align-self-xl-start{-ms-flex-item-align:start!important;align-self:flex-start!important}.prime-align-self-xl-end{-ms-flex-item-align:end!important;align-self:flex-end!important}.prime-align-self-xl-center{-ms-flex-item-align:center!important;align-self:center!important}.prime-align-self-xl-baseline{-ms-flex-item-align:baseline!important;align-self:baseline!important}.prime-align-self-xl-stretch{-ms-flex-item-align:stretch!important;align-self:stretch!important}}

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
@section('js')
@parent

@stop

@section('content')
@parent

<?php
  $imgsrc = '';

  $arrNameById = array( 'Etc/GMT+12' =>'International Date Line West','Pacific/Pago_Pago'=>'American Samoa','Pacific/Midway'=>'Midway Island','Pacific/Honolulu' =>'Hawaii','America/Juneau'=>'Alaska','America/Los_Angeles'=>'Pacific Time','America/Tijuana' =>'Tijuana','America/Phoenix'=>'Arizona','America/Chihuahua'=>'Chihuahua','America/Mazatlan'=>'Mazatlan','Denver'=>'Mountain Time','America/Guatemala'=>'Central America','America/Chicago'=>'Central Time','America/Mexico_City'=>'Mexico City','America/Monterrey'=>'Monterrey','America/Regina'=>'Saskatchewan','America/Bogota'=>'Bogota','America/New_York'=>'Eastern Time','America/Indiana/Indianapolis'=>'Indiana','America/Lima'=>'Quito','America/Halifax'=>'Atlantic Time','America/Caracas'=>'Caracas','America/Guyana'=>'Georgetown','America/La_Paz'=>'La Paz','America/Puerto_Rico'=>'Puerto Rico','America/Santiago'=>'Santiago','America/St_Johns'=>'Newfoundland','America/Sao_Paulo'=>'Brasilia','America/Argentina/Buenos_Aires'=>'Buenos Aires','America/Godthab'=>'Greenland','America/Montevideo'=>'Montevideo','Atlantic/South_Georgia'=>'Mid-Atlantic','Atlantic/Azores'=>'Azores','Atlantic/Cape_Verde'=>'Cape Verde Is.','Africa/Casablanca'=>'Casablanca','Europe/Dublin'=>'Dublin','Europe/London'=>'London','Europe/Lisbon'=>'Lisbon','Africa/Monrovia'=>'Monrovia','Etc/UTC'=>'UTC','Europe/Amsterdam'=>'Amsterdam','Europe/Belgrade'=>'Belgrade','Europe/Berlin'=>'Berlin','Europe/Zurich'=>'Zurich','Europe/Bratislava'=>'Bratislava','Europe/Brussels'=>'Brussels','Europe/Budapest'=>'Budapest','Europe/Copenhagen'=>'Copenhagen','Europe/Ljubljana'=>'Ljubljana','Europe/Madrid'=>'Madrid','Europe/Paris'=>'Paris','Europe/Prague'=>'Prague','Europe/Rome'=>'Rome','Europe/Sarajevo'=>'Sarajevo','Europe/Skopje'=>'Skopje','Europe/Stockholm'=>'Stockholm','Europe/Vienna'=>'Vienna','Europe/Warsaw'=>'Warsaw','Africa/Algiers'=>'West Central Africa','Europe/Zagreb'=>'Zagreb','Europe/Athens'=>'Athens','Europe/Bucharest'=>'Bucharest','Africa/Cairo'=>'Cairo','Africa/Harare'=>'Harare','Europe/Helsinki'=>'Helsinki','Asia/Jerusalem'=>'Jerusalem','Europe/Kaliningrad'=>'Kaliningrad','Europe/Kiev'=>'Kyiv','Africa/Johannesburg'=>'Pretoria','Europe/Riga'=>'Riga','Europe/Sofia'=>'Sofia','Europe/Tallinn'=>'Tallinn','Europe/Vilnius'=>'Vilnius','Asia/Baghdad'=>'Baghdad','Europe/Istanbul'=>'Istanbul','Asia/Kuwait'=>'Kuwait','Europe/Minsk'=>'Minsk','Europe/Moscow'=>'Moscow','Africa/Nairobi'=>'Nairobi','Asia/Riyadh'=>'Riyadh','Europe/Volgograd'=>'Volgograd','Asia/Tehran'=>'Tehran','Asia/Muscat'=>'Muscat','Asia/Baku'=>'Baku','Europe/Samara'=>'Samara','Asia/Tbilisi'=>'Tbilisi','Asia/Yerevan'=>'Yerevan','Asia/Kabul'=>'Kabul','Asia/Yekaterinburg'=>'Ekaterinburg','Asia/Karachi'=>'Karachi','Asia/Tashkent'=>'Tashkent','Asia/Kolkata'=>'New Delhi','Asia/Calcutta'=>'New Delhi','Asia/Colombo'=>'Sri Jayawardenepura','Asia/Kathmandu'=>'Kathmandu','Asia/Almaty'=>'Almaty','Asia/Dhaka'=>'Dhaka','Asia/Urumqi'=>'Urumqi','Asia/Rangoon'=>'Rangoon','Asia/Bangkok'=>'Hanoi','Asia/Jakarta'=>'Jakarta','Asia/Krasnoyarsk'=>'Krasnoyarsk','Asia/Novosibirsk'=>'Novosibirsk','Asia/Shanghai'=>'Beijing','Asia/Chongqing'=>'Chongqing','Asia/Hong_Kong'=>'Hong Kong','Asia/Irkutsk'=>'Irkutsk','Asia/Kuala_Lumpur'=>'Kuala Lumpur','Australia/Perth'=>'Perth','Asia/Singapore'=>'Singapore','Asia/Taipei'=>'Taipei','Asia/Ulaanbaatar'=>'Ulaanbaatar','Asia/Tokyo'=>'Tokyo','Asia/Seoul'=>'Seoul','Asia/Yakutsk'=>'Yakutsk','Australia/Adelaide'=>'Adelaide','Australia/Darwin'=>'Darwin','Australia/Brisbane'=>'Brisbane','Australia/Melbourne'=>'Melbourne','Pacific/Guam'=>'Guam','Australia/Hobart'=>'Hobart','Pacific/Port_Moresby'=>'Port Moresby','Australia/Sydney'=>'Sydney','Asia/Vladivostok'=>'Vladivostok','Asia/Magadan'=>'Magadan','Pacific/Noumea'=>'New Caledonia','Pacific/Guadalcanal'=>'Solomon Is.','Australia/Srednekolymsk'=>'Srednekolymsk','Pacific/Auckland'=>'Wellington','Pacific/Fiji'=>'Fiji','Asia/Kamchatka'=>'Kamchatka','Pacific/Majuro'=>'Marshall Is.','Pacific/Chatham'=>'Chatham Is.','Pacific/Tongatapu'=>'Nuku alofa','Pacific/Apia'=>'Samoa','Pacific/Fakaofo'=>'Tokelau Is.');
  
  if(isset($arrNameById[Auth::User()->timezone])){
    $mytimezone=Auth::User()->timezone;
  }else{
    $mytimezone='Etc/UTC';
  }

?>

<div class="row ">
  
  <div class="col-12 col-sm-9 ">

<div class="row"><div class="col">
    <div class="float-left mt-4">
        <a href="{{asset('createbanners')}} "class="btn btn-primary float-right">Create New Banner</a>
      </div>
    <div class="float-right">
       <div classs="">
    <span class="h6">Master Switch</span>
       </div>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-outline-primary {{ Auth::User()->isbannershow == 1 ? "active":""}}">

            <input type="radio" name="options" onchange="activeBanner( 1 );" autocomplete="off" checked=""> ON
          </label>
          <label class="btn btn-outline-primary {{ Auth::User()->isbannershow == 0 ? "active":""}}">
            <input type="radio" name="options" onchange="activeBanner( 0 );" id="option3" autocomplete="off"> OFF
          </label>
        </div>    
      </div>
     </div>
</div>

    @if( !empty( count($banners) ) )
    <div class="mt-3">
      <table id="myTable" class="table table-responsive table-bordered" style="max-height:600px;">
          <thead class="thead-light">
          <tr>
            <th class="text-center">#</th>
            <th  class="text-center"> Banner</th>
            <th class="text-center">Actions</th>
            <th class="text-center">Group</th>
            <th class="text-center">Priority</th>
          </tr>
        </thead>
        <colgroup>
          <col width="5%">
          <col width="40%">
          <col width="35%">
          <col width="15%">
          <col width="5%">
        </colgroup>
        <tbody style="cursor: move;">
          @php

          $bannerPriority = array();
          @endphp

          @foreach( $banners as $index =>  $banner )
          @php

            $bannerPriority[$banner['productbannerid']] = $banner['priority'];
            $apprerance = json_decode( $banner['appearance'] , true );
            $bannerstyle = json_decode( $banner['bannerstyle'] , true );
            if(isset($bannerstyle['bordersize']) && !empty($bannerstyle['bordersize']) ){
              $imgstyle = 'border:'.$bannerstyle['bordersize'].' '.$bannerstyle['bordercolor'].' '.$bannerstyle['borderstyle'];
            }else{
              $imgstyle = '';
            }
            
            $badgRules  = json_decode( $banner['displayrules'] , true );
            if(strpos($banner['title'], 'product.metafields') !== false){
              $banner['title'] = str_replace("product.metafields","primebm",$banner['title']);
            }
          @endphp
          <tr bannerid="{{$banner['productbannerid']}}" id="row_{{$banner['productbannerid']}}">
            <td class="text-center index" bannerid="{{$banner['productbannerid']}}">
              {{$index+1}}
            </td>
            <td class="">

            <div class="primeBanners  prime-d-block" id="" style="">

              <div class="primebanDesktop prime-d-block" style="{{isset( $apprerance['desktop']['bannerStyle'] ) ? $apprerance['desktop']['bannerStyle'] :''  }}">

                
                @if( $banner['banner_type'] == 'text' )
                  <div class="primebanImageOuter prime-d-table-cell prime-align-middle ">
                    @if( !isset($banner['imgsrc']) )
                      <img src="{{ env('AWS_PATH').'ban'.$banner['productbannerid'] }}" class="primebanImage prime-d-block  prime-mx-auto banner_image_preview" style="height: 50px;" />
                      @php $imgsrc=env('AWS_PATH').'ban'.$banner['productbannerid']; @endphp
                    @else 
                      <img src="{{ $banner['imgsrc'] }}" class="primebanImage prime-d-block  prime-mx-auto banner_image_preview" style="height: 50px;" />
                      @php $imgsrc=$banner['imgsrc']; @endphp
                    @endif
                  </div>
                  
                  <div class="primebanTexts prime-d-table-cell prime-align-middle" style="">	
                    <div class="primebanTitle prime-mb-1" style="{{ isset( $apprerance['desktop']['bannertitleStyle'] ) ? $apprerance['desktop']['bannertitleStyle'] :'' }}">{{isset( $banner['title'] ) ? $banner['title'] :''  }}
                    </div>
                    <div class="primebanSubTitle" style="{{ isset( $apprerance['desktop']['bannersubtitleStyle'] ) ? $apprerance['desktop']['bannersubtitleStyle'] :'' }}">{{isset( $banner['subtitle'] ) ? $banner['subtitle'] :''  }}
                    </div>
                  </div>
                @else
                  <div class="primebanPhotoOuter">
                    @if( !isset($banner['imgsrc']) )
                      <img src="{{ env('AWS_PATH').'ban'.$banner['productbannerid'] }}" class="primebanPhoto banner_image_preview" style="height:50px;max-width:100%; {{$imgstyle}}" />
                      @php $imgsrc=env('AWS_PATH').'ban'.$banner['productbannerid']; @endphp
                    @else 
                      <img src="{{ $banner['imgsrc'] }}" class="primebanPhoto banner_image_preview" style="height:50px;max-width:100%; {{$imgstyle}}" />
                      @php $imgsrc=$banner['imgsrc']; @endphp
                    @endif
                  </div>
                @endif

              </div>
            </div>


            </td>
            <td  class="text-center" >
              <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-sm btn-outline-secondary "  onclick="changeStaus('{{$banner['productbannerid']}}' , '{{$banner['isactive'] }}' );" >
                <i class="fa fa-toggle-on {{ $banner['isactive'] == 1 ? 'text-primary':'text-muted' }} "></i>
                </button>

                <button type="button" class="btn btn-sm btn-outline-secondary"  onclick="location.href='{{asset('editbanner')}}/{{$banner['productbannerid']}}'">Edit</button>
                {{-- <button type="button" class="btn btn-sm btn-outline-secondary">View</button> --}}
                <button type="button" class="btn btn-sm btn-outline-secondary" onclick="copyPop('{{$banner['productbannerid']}}', '{{$imgsrc}}');">Copy</button>
                <button type="button" class="btn btn-sm btn-outline-secondary" onclick="deletePop('{{$banner['productbannerid']}}');">Delete</button>
              </div>
            </td>

            <td class="text-center">
              <div class="text-center text-muted">
                @if( Auth::User()->planid > 2 )
                  <select name="bannergroup" id="groupbanner" class="form-control metafieldcond form-control-sm" onchange="changegroup(this,'{{$banner['productbannerid']}}');" >
                    <option {{ $banner['bannergroup'] == 1 ? 'selected':'' }} value="1">1</option>
                    <option {{ $banner['bannergroup'] == 2 ? 'selected':'' }} value="2" >2</option>
                    <option {{ $banner['bannergroup'] == 3 ? 'selected':'' }} value="3" >3</option>
                  </select>
                @else
                  <select name="bannergroup" id="groupbanner" class="form-control metafieldcond form-control-sm" disabled >
                    <option {{ $banner['bannergroup'] == 1 ? 'selected':'' }} value="1">1</option>
                    <option {{ $banner['bannergroup'] == 2 ? 'selected':'' }} value="2" >2</option>
                    <option {{ $banner['bannergroup'] == 3 ? 'selected':'' }} value="3" >3</option>
                  </select>
                @endif

              </div>
            </td>

           <td class="Priority">
              <div class="text-center text-muted">
                <a href="#"><i class="fa fa-bars"></i></a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-12 justify-content-center">
      <center>
        {{-- {{$banners->render("pagination::bootstrap-4")}} --}}
        {{-- Total - {{ $banners->total() }} --}}
        Total -{{ count($banners)}}
        <br> <br>
      </center>
    </div>
    @endif
   


  </div>
  
    <div class="col-12 col-sm-3">
      <div class="card bg-light">
        <div class="card-body border-top">
          <div>
            <div class="font-weight-bold">Banners</div>
              <div class="small mt-1">
              Increase conversions by displaying the #1 reason why customers should buy from you.
                <div class="mt-1"><a href="https://getinstashop.freshdesk.com/a/solutions/articles/81000390276" target="_blank" class="">Know More<span class="fa fa-caret-right ml-1"></span></a></div>
              </div>
            </div>
            <hr>
            <div>
              <div class="font-weight-bold">Installation</div>
              <div class="small mt-1">
                  To display the Banners you need to add a line of app installation code in your Shopify store theme. <div class="mt-1"><a target="_blank" href="https://getinstashop.freshdesk.com/a/solutions/articles/81000390277" class="">Install Guide (Do it yourself)<span class="fa fa-caret-right ml-1"></span></a></div>
                    @if(Auth::User()->isintegratedbanner == 0)
                   <div class="mt-1">
                  <a onclick="openrequestmodal();" href="javascript:void(0)" class="">Request Installation<span class="fa fa-caret-right ml-1"></span></a>
                </div>
              @else 
                  <div class="mt-1 text-success p-2 border bg-white isIntegrationComplete">Integration completed<span class="ml-2 fa fa-check-square"></span></div>
                @endif
              </div>            
              
               </div>
        </div>
      </div>
      @if(Auth::user()->planid < 3)
        <div id="upgradenotice" class="mt-3">
            <div class="alert alert-danger">Banners will only be visible for users on Pro Plan and above.<br>
                <a href="/plans" class="btn btn-sm btn-danger mt-1">Upgrade Now</a>
            </div>
        </div>
      @endif
    </div>




</div> 

<!-- copy Model start -->
<div id="modalCopyYesNo" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Copy Banner</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="deleteproduct" >
            <div class="font-weight-bold">
              Are your sure you want copy the Banner?
            </div>
            
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesCopyYesNo" type="button" class="btn btn-primary text-right">Confirm</button>
            <button id="btnNoCopyYesNo" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end copy model -->

<div id="modalConfirmYesNo" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Delete banner</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="deleteproduct" >
            <div class="font-weight-bold">
              Are your sure you want delete the banner?
            </div>
            <div class="small mt-2">
              This will remove the banner from Shopify store.
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesConfirmYesNo" type="button" class="btn btn-primary text-right">Confirm</button>
            <button id="btnNoConfirmYesNo" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
<div id="integrationrequest" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Request Integration</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="" >
            <div class="">
              Shopify experts will analyze your theme and add the integration code for you. 
              This is typically done in a few hours. You will get a notification email as soon as the integration is complete. 
              Meanwhile, you can continue adding banners in the dashboard.
              <br>
              <div class="mt-3 text-muted">This is a free service.</div>
            </div>
<!--            <div class="small mt-2">
              This will remove the banner from Shopify store.
            </div>-->
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesIntegrate" type="button" class="btn btn-primary text-right">Request Integration</button>
            <button id="btnNoIntegrate" type="button" class="btn btn-default">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">

  $(document).ready(function(){
    var integrationCom = <?php echo Auth::user()->isintegratedbanner ; ?>;
    if(integrationCom == 0){
      $('.isIntegrationComplete').hide();
    }else{
      $('.isIntegrationComplete').show();
    }
  });

  var primebannerid = '';
  var $confirm = $("#modalConfirmYesNo");
  var $copy = $("#modalCopyYesNo");
  var bannerPriority = @json( isset($bannerPriority) ? $bannerPriority : '' );

  // console.log( bannerPriority );
  function deletePop( bannerid ){
    
    primebannerid = bannerid;

    $confirm.modal('show');

  }

function openrequestmodal(){

    $('#integrationrequest').modal({
        backdrop: 'static',
        keyboard: false
    });
   
  }

  $("#btnYesConfirmYesNo").click(function () {

    $confirm.modal("hide");
    $('#loadingalert').show();

     $.ajax( {
        url:"{{asset('deletebanner')}}",
        method:'POST',
        data :{'bannerid':primebannerid,'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);

          $("#row_"+primebannerid).remove();
          toggleAlert();

        }

  })

  });


  $("#btnYesIntegrate").click(function () {

    $('#integrationrequest').modal("hide");
    //return false;
    
    $('#loadingalert').show();

    $.ajax({
        url:"{{asset('sendintegratemailbanners')}}",
        method:'POST',
        data :{'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);
          toggleAlert();

        }

    })

  });

  $("#btnNoIntegrate").click(function () {
      $('#integrationrequest').modal("hide");
  });

 $("#btnNoConfirmYesNo").click(function () {
      $confirm.modal("hide");
  });


var img = '';
function copyPop( bannerid, imgsrc){
      
      primebannerid = bannerid;
      console.log(primebannerid);
      img = imgsrc;
      $copy.modal('show');
    }


 $("#btnYesCopyYesNo").click(function () {
      
      $copy.modal("hide");
      $('#loadingalert').show();
      console.log(primebannerid);
      console.log(img);
        $.ajax( {
        url:"{{asset('copybanner')}}",
        method:'POST',
        data :{'bannerid':primebannerid,'image':img,'_token':'{{csrf_token()}}'},
        success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);

          window.location.replace("editbanner/"+response.id);

         // $("#row_"+primebannerid).remove();
          toggleAlert();

        }

      })

    });

 $("#btnNoCopyYesNo").click(function () {
      $copy.modal("hide");
    });
  
    var fixHelperModified = function(e, tr) {
    var $originals = tr.children();
    var $helper = tr.clone();
    $helper.children().each(function(index) {
      $(this).width($originals.eq(index).width())
    });
    return $helper;
  },
    updateIndex = function(e, ui) {

      $('td.index', ui.item.parent()).each(function (i) {

        $(this).html(i+1);
      });

      getPriorityOrder( );

    };

  $("#myTable tbody").sortable({
    helper: fixHelperModified,
    stop: updateIndex
  }).disableSelection();

    $("tbody").sortable({
    distance: 5,
    delay: 100,
    opacity: 0.6,
    cursor: 'move',
    update: function( data ) {


    }
      });


    function getPriorityOrder( ){

      let objBannerOrder = {};

      $('table tr ').each(function(){

        let bannerid = $(this).attr('bannerid');
        if( typeof bannerid != 'undefined'){
          let index = $(this).find('td.index').text();
          objBannerOrder[bannerid] = parseInt( index );

        }
        
      });

      if( JSON.stringify( bannerPriority ) === JSON.stringify(objBannerOrder) ){
        return false;
      }

      $.ajax( {

            url:"{{asset('updatebanner-priority')}}",
            method:'POST',
            data :{'banner':JSON.stringify( objBannerOrder),'_token':'{{csrf_token()}}'},
            success:function( response ){

            $('#loadingalert').hide();
            $('#requestmessage').text(response.msg);
            bannerPriority = objBannerOrder;
          //   if( response.status == true ){

          //     setTimeout(function(){
          //     location.reload();

          //   },2000);

          // }

          toggleAlert();

        }

    });


  }

  function changegroup( full, productbannerid ){

    var group = full.value;  
    let objBanner = {'productbannerid':productbannerid,'group':group, '_token':'{{csrf_token()}}'};
    $('#loadingalert').show();
    $.ajax( {

          url:"{{asset('updatebanner-group')}}",
          method:'POST',
          data :objBanner,
          success:function( response ){

          $('#loadingalert').hide();
          $('#requestmessage').text(response.msg);
          // bannerPriority = objBannerOrder;
        toggleAlert();

        setTimeout( function(){
          location.reload();
        }, 2000);

      }

    });

  }
  

  function changeStaus( productbannerid , isactive  ){

      let objBanner = {'productbannerid':productbannerid, 'isactive':isactive, '_token':'{{csrf_token()}}'};
      $('#loadingalert').show();
      $.ajax( {

            url:"{{asset('updatebanner-status')}}",
            method:'POST',
            data :objBanner,
            success:function( response ){

            $('#loadingalert').hide();
            $('#requestmessage').text(response.msg);
            // bannerPriority = objBannerOrder;
          toggleAlert();

          setTimeout( function(){
            location.reload();
          }, 2000);

        }

    });

  }


  function activeBanner( type  ){


    postData( {'isShowBanner': type,'_token':'{{csrf_token()}}' } , '{{asset('activatebanner')}}' );
  }

  function postData( requestData , url ){
    
    $('#loadingalert').show();

    $.ajax({
        method:"POST",
        url : url,
        data :requestData ,
        success : function( data ){
            $('#loadingalert').hide();
            $('#requestmessage').text(data.msg);
            toggleAlert();
            setTimeout(function(){
              location.reload();
            }, 2000);

        }

    });
  }

  var objDynamincMapper = {
                            'Inventory'      :5,
                            // 'product.price_min'   :"$5.00",
                            // 'product.price_max'   :"$6.00",
                            'SaleAmount'  :"$6.00",
                            // 'primebMaxSaleAmount'  :"$7.00",
                            'SalePercent' :"10",
                            'Vendor': 'Allen Solly',
                            'Type': 'Tshirt',
                            'variantCount':2,
                            'OrderCount':10,
                            'CountdownTimer:':'',
                            'RecentSoldCount':'15',
                            'ProductSKU':"ABCD1234",
                            'VisitorCounter':15,
                            // 'primebMaxSalePercent' :"20",
                      } ;

  $(function() {
    
    $('.primebanTitle').each(function(i, obj) {
      var titletest = $(this).text();
      $(this).text(updateTextValue(titletest));
    });

    $('.primebanSubTitle').each(function(i, obj) {
      var titletest = $(this).text();
      $(this).text(updateTextValue(titletest));
    });

    // $('.primebanTexts').each(function(i, obj) {
    //   var primetest = $(this).text();
    //   $(this).text(updateTextValue(primetest));
    // });

  });

  function updateTextValue( value ){
    // console.log(value);
    if( value.indexOf("Inventory") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("Inventory", objDynamincMapper['Inventory'] );
    }
     if( value.indexOf("SaleAmount") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("SaleAmount", objDynamincMapper['SaleAmount'] );
    }
     if( value.indexOf("SalePercent") != -1 ){
      let percent = "";
      if( value.indexOf("%") != -1){
        percent = "%";
      }
      value = replaceSpecialSym( value );
      value = value.replace("SalePercent", objDynamincMapper['SalePercent']+percent );
    
    } 
     if( value.indexOf("product.vendor") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("product.vendor", objDynamincMapper['Vendor'] );
    
    }  
     if( value.indexOf("product.type") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("product.type", objDynamincMapper['Type'] );
    
    } 
    if( value.indexOf("VariantCount") != -1 ){
      value = replaceSpecialSym( value );
      value = value.replace("VariantCount", objDynamincMapper['variantCount'] );
    }
    if( value.indexOf("OrderCount") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("OrderCount", objDynamincMapper['OrderCount'] );
    }
    if( value.indexOf("ProductSKU") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("ProductSKU", objDynamincMapper['ProductSKU'] );
    }
    if( value.indexOf("RecentSoldCount") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("RecentSoldCount", objDynamincMapper['RecentSoldCount'] );
    }
    if( value.indexOf("VisitorCounter") != -1  ){
      value = replaceSpecialSym( value );
      value = value.replace("VisitorCounter", objDynamincMapper['VisitorCounter'] );
    }
    if( value.indexOf("primebm") != -1  ){
      value = replaceSpecialSym( value );
      // value = value.replace("product.metafields", objDynamincMapper['product.metafields'] );
    }
    if( value.indexOf("CountdownTimer:") != -1  ){
      var timezone = <?php echo json_encode($arrNameById[$mytimezone]); ?>;
      // value = value.replace("CountdownTimer:", objDynamincMapper['CountdownTimer:'] );
      // value = value.replace("]]", timezone+" ]]" );

      var pos = value.indexOf("CountdownTimer:");
      var res = value.slice(pos+15, pos+34);
      value = value.replace("[[ CountdownTimer:", "" );
      value = value.replace("]]", "" );

      let end = new Date(toValidDate(res));
      let start = new Date();
      var remaintime = end - start;
      var days = Math.floor(remaintime / (1000 * 60 * 60 * 24));
      var hours = Math.floor((remaintime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((remaintime % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((remaintime % (1000 * 60)) / 1000);
      var realtimeleft = days + "d "+hours + "h "+minutes + "m "+seconds + "s";
      if(seconds < 0){
          realtimeleft = "0d 0h 0m 0s";
      }
      value = value.replace(res, realtimeleft );
          

      // value = value.replace(res, realtimeleft );
      
    }
    if( value.indexOf("[[today") != -1  ){
      var weekdays = <?php echo json_encode(Auth::User()->weekdays); ?>;
      var dateformatselected = '<?php echo Auth::User()->dateformat; ?>';
      var realvalue = value; 

      var pos = realvalue.indexOf("[[today");
      var pos2 = realvalue.indexOf("]]");
      var startres = realvalue.slice(pos+8, pos2);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );
      var pos3 = realvalue.indexOf("[[today");
      var pos4 = realvalue.indexOf("]]");
      var endres = realvalue.slice(pos3+8, pos4);
      realvalue = realvalue.replace("[[today,", "" );
      realvalue = realvalue.replace("]]", "" );

      var wrongdays = weekdays.split(",");
      // var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
      var startDate = new Date(new Date().getTime()+(parseInt(startres)*24*60*60*1000));
      var today = new Date(new Date().getTime());
      var firstinc=0;
      while (today <= startDate) {
        var getday = String(today.getDay());
        if(wrongdays.indexOf(getday) != -1){
          startDate.setDate(startDate.getDate() + 1);
          firstinc++;
        }
        today.setDate(today.getDate() + 1);
      }
      value = value.replace("[[today,"+startres+"]]", moment(startDate).format(dateformatselected));

      var extradays = firstinc+parseInt(endres);
      // console.log(extradays);
      var endDate = new Date(new Date().getTime()+(extradays*24*60*60*1000));
      var todayend = startDate;
      var secinc=0;
      while (todayend <= endDate) {
        var getday = String(todayend.getDay());
        if(wrongdays.indexOf(getday) != -1){
          endDate.setDate(endDate.getDate() + 1);
          secinc++;
        }
        todayend.setDate(todayend.getDate() + 1);
      }
      value = value.replace("[[today,"+endres+"]]", moment(endDate).format(dateformatselected));
      
    }

    return value;
  }

  function toValidDate(datestring){
    return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
  }

  function replaceSpecialSym( value ){
    value = value.replace("\{\{","");
    value = value.replace("\}\}","");
    value = value.replace("|","");
    value = value.replace("money","");
    value = value.replace("round","");
    value = value.replace("%",'');
    return value;
  }

</script>

@stop