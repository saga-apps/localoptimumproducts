<!DOCTYPE html>
<html>
<head>
  <title>Bootstrap First Project</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>
  <style>
  .container{
   max-width:1000px;
 }
</style>
<body>
  <div class="container">
    <div class="card mt-4 mx-4">
      <div class="card-header">
        Account Details 
      </div>
      <div class="card-body text-right">
       <div class="form-group row">
        <label for="staticEmail" class=" font-weight-bold col-sm-4 col-form-label">Name</label>
        <div class="col-sm-8">
          <input type="password" class="form-control" id="inputPassword">
        </div>
      </div>
      <div class="form-group row">
        <label for="staticEmail" class=" font-weight-bold col-sm-4 col-form-label">Email ID</label>
        <div class="col-sm-8">
          <input type="password" class="form-control" id="inputPassword">
        </div>
      </div>
      <fieldset disabled>
        <div class="form-group row">
          <label for="disabledTextInput"class="font-weight-bold col-sm-4 col-form-label">Website</label>
          <div class="col-sm-8">
            <input type="text" id="disabledTextInput" class="form-control" placeholder="abc.myshopify.com">
          </div>
        </fieldset>
        <div class="form-group row">
          <label for="staticEmail" class="font-weight-bold col-sm-4 col-form-label">Plan</label>
          <div class="col-sm-8">
           <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2">
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="button" id="button-addon2">Change Plan</button>
            </div>
          </div>
          <div class="alert alert-danger text-left">
           You are on a free trial 
         </div>
         <button type="button" class="btn btn-primary">Save</button>
       </div>
     </div>