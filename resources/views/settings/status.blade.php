@extends('layouts.admin')
@section('main')
		<div class="col-xs-12 col-sm-10 ">
			<div class="card mt-3">
				<div class="card-header">
					Task List
				</div>
				<div class="card mx-5 my-5">
					<div class="card-header">
						<div class="row">
							<div class="col-6">
								<p class="font-weight-bold">In Progress</h3>
								</div>
								<div class="col-6 text-right">
									<button type="button" class="btn btn-warning">Pause</button>
									<button type="button" class="btn btn-danger">Stop</button>
								</div>
							</div>
						</div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Task</th>
									<th>Pull Up</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<td>Start Time</td>
									<td>8th Oct 2018, 14:00</td>
								</tr>
								<tr>
									<td> Products</td>
									<td>50</td>
								</tr>
								<tr>
									<td> Progress</td>
									<td>25% Complete</td>
								</tr>
								<tr>
									<td> Estimated Time End</td>
									<td>8th Oct 2018, 14:00</td>
								</t
							</tbody>
						</table>
					</div>
					<div class="card mx-5 my-5 ">
						<div class="card-header">
							<p class="font-weight-bold">Complete </p>
						</div>
						<div class="card-body ">
							
							<div class="row">
								<div class="col-sm-3">
									<p class="font-weight-bold"> Task </p>
									<p class= "font-weight-bold"> Start Time  </p>
									<p class= "font-weight-bold"> Products</p>
									<p class= "font-weight-bold"> Progress</p>
									<p class= "font-weight-bold"> Estimated End Time </p>
								</div>
								<div class="col-sm-6">
									<p> Pull all </p>  
									<p> 28th Oct 2018, 14:00</p>
									<p> 50</p>
									<p> 25% Complete</p>
									<p> 28th Oct 2018, 18:00</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection

