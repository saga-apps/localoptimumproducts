@section('title')
@parent
<title>Push Settings</title>
@stop
<div class="col-12 col-sm-10 col-md-8 col-lg-7">

  <div class="card mt-3">
    <div class="card-header">
      Settings for pushing products into Shopify
    </div>
    <div class="card-body">
      <form onsubmit="event.preventDefault();  return saveSetting('#form-default-settings','{{asset('defaultsetting')}}');" id="form-default-settings" name="form-button-settings">
        {{csrf_field() }}
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Show on Store</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <select name="isshowonstore" class="form-control" id="exampleFormControlSelect1">
              <option value="1" {{$usersettings->isshowonstore == 1 ? 'selected' : ''}} >Visible</option>
              <option value="0" {{$usersettings->isshowonstore == 0 ? 'selected' : ''}}>Hide</option>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
            Product Type
            <div class="small">
              <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="App will auto-fill product type field with the product group value derived from Amazon. If product group is not available then blank value will be saved. Alternatively, you can set your own default product type or keep it blank.">
                Know more
              </a>
            </div>
          </label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <div class="form-check">
              <input class="form-check-input" type="checkbox"  name="isautoproducttype"  id="isautoproducttype" value="1" id="Check1" {{$usersettings->isautoproducttype ? 'checked' : ''}}>
              <label class="form-check-label" for="disabledFieldsetCheck">
                Auto-fill from Amazon
              </label>
            </div>
            <input name="producttype" {{$usersettings->isautoproducttype ? 'readonly' : ''}} id="producttype" class="form-control" placeholder="Shirts" value="{{$usersettings->producttype or ''}}">
          </div>
        </div>
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
            Vendor
            <div class="small">
              <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="App will auto-fill vendor field with the item brand name derived from Amazon. If brand name is not available then blank value will be saved. Alternatively, you can set your own default vendor value.">
                Know more
              </a>
            </div>
          </label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="isautoamazonvendor"  id="isautoamazonvendor" value="1" id="Check1" {{$usersettings->isautoamazonvendor ? 'checked' : ''}} >
              <label class="form-check-label" for="disabledFieldsetCheck">
                Auto-fill from Amazon
              </label>
            </div>
            <input name="vendor" {{$usersettings->isautoamazonvendor ? 'readonly' : ''}} id="vendor" class="form-control" placeholder="Shirts" value="{{$usersettings->vendor or ''}}">
          </div>
        </div>
         <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
            Product Tags
            <div class="small">
              <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="App will auto-fill product type field with the product group value derived from Amazon. If product group is not available then blank value will be saved. Alternatively, you can set your own default product type or keep it blank.">
                Separate tags with commas
              </a>
            </div>
          </label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <div class="input-group mt-2">
              <input name="tags" class="form-control" placeholder="Shirts"  value="{{$usersettings->tags or ''}}">
              <div class="input-group-append">
                <!--<button class="btn btn-outline-secondary" type="button" id="changeLocation" onclick="changeInventoryLocation();">Change</button>-->
                <a data-toggle="modal" href="" onclick="openTagForm();" class="btn btn-outline-secondary" >Auto-tags</a>
              </div>
              <!--<div class="mt-1 ml-1">-->

            </div>
            <a target="_blank" class="small float-right" href="https://connectr.freshdesk.com/support/solutions/articles/44000329495-what-is-auto-tagging-">
              Auto-tags
              <span class="fa fa-caret-right"></span>
            </a>
          </div>
        </div>
        <div class="form-group row">
      <label for="tags" class=" col-12 col-sm-4 col-md-4 col-form-label">
        Description
        <div class="small">
          <a target="_blank" class="" href="https://connectr.freshdesk.com/support/solutions/articles/44001405296-description-data-fields">
            Know more&nbsp;<span class="fa fa-caret-right"></span>
          </a>
        </div>
      </label>
      <div class="col-12 col-sm-8 col-md-6 col-lg-6">
        <button data-toggle="modal" data-target="#descriptionOptionModal" class="mt-2 btn btn-outline-secondary" type="button">Setup</button>
      </div>
    </div>
       
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Barcode</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <select name="barcode" class="form-control" id="exampleFormControlSelect1">
              <option value="1" {{$usersettings->barcode == 1 ? 'selected' : ''}} >ASIN</option>
              <option value="2" {{$usersettings->barcode == 2 ? 'selected' : ''}} >SKU</option>
              <option value="3" {{$usersettings->barcode == 3 ? 'selected' : ''}} >UPC</option>
              <option value="4" {{$usersettings->barcode == 4 ? 'selected' : ''}} >EAN</option>
              <option value="5" {{$usersettings->barcode == 5 ? 'selected' : ''}} >ISBN</option>
              <option value="0" {{$usersettings->barcode == 0 ? 'selected' : ''}} >(Empty)</option>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label for="usr" class=" col-12 col-sm-4 col-md-4 col-form-label">Inventory Policy</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <a id="inventorypolicy"  href="" data-toggle="modal" data-target="#inventorypolicymodal" class="btn btn-outline-secondary">Advanced Setup</a>
          </div>
        </div>

        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
            Inventory Quantity
            <div class="small">
              <a class="text-primary" target="_blank" href="https://connectr.freshdesk.com/support/solutions/articles/44000464595-updating-inventory-quantity">
                Know more <span class="fa fa-caret-right"></span>
              </a>
            </div>
          </label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-check mt-1">
              <input class="form-check-input inventorytype" type="radio" name="inventorytype" id="qty1" value="1" {{$usersettings->inventorytype == 1 ? 'checked' : ''}} >
              <label class="form-check-label" for="exampleRadios1">
                As available on Amazon
              </label>
            </div>
            <div class="form-check mb-1">
              <input class="form-check-input inventorytype" type="radio" name="inventorytype" id="qty2" value="2" {{$usersettings->inventorytype == 2 ? 'checked' : ''}}>
              <label class="form-check-label" for="exampleRadios2">
                Fixed count
              </label>
            </div>
            <input  name="quantity" id="quantity" {{$usersettings->inventorytype == 1 ? 'readonly' : ''}} type="number" class="form-control" placeholder="1" min="1" value="{{$usersettings->quantity or 1}}">
          </div>
        </div>



        <div class="form-group row">
          <label for="Offset" class="col-12 col-sm-4 col-md-4 col-form-label">Inventory Location
            <div class="small">
              <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="Item inventory will be assigned to this location by default.">
                know more
              </a>
            </small>
          </small>
        </div>
      </label>
      <div class="col-12 col-sm-8 col-md-6 col-lg-6">
        <div class="input-group mb-1">
          <input readonly="" id="locationtmp" value="{{$usersettings->location}}" type="text" class="form-control" placeholder="ASKJDKLJASKLDJAKSLD" aria-label="locationtmp" aria-describedby="button-addon2">
          <input  id="locationidtmp" value="{{$usersettings->locationid}}" type="hidden">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button" id="changeLocation" onclick="changeInventoryLocation();">Change</button>
          </div>
        </div>

        {{-- {{
          Form::select('locationid', $locations, isset($usersettings->locationid) ? $usersettings->locationid : '', array('id' => 'locationid', 'class' => 'form-control', 'type' => 'select'))
        }} --}}
      </div>

    </div>



    <div class="form-group row">
      <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
        Product Weight Unit
      </label>
      <div class="col-12 col-sm-8 col-md-6 col-lg-6">
        <select name="weighttype" class="form-control" id="weighttype">
          <option value="2"{{isset($useroptions['weighttype']) && $useroptions['weighttype']  == 2 ? 'selected' : ''}}>Pound (lb)</option>
          <option value="1" {{isset($useroptions['weighttype']) && $useroptions['weighttype']  == 1 ? 'selected' : ''}} >Kilogram (kg)</option>
        </select>
      </div>

    </div>

    <div class="form-group row">
      <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
        Duplicate Variant Preference
        <div class="small">
          <a class="text-primary" target="_blank" href="https://connectr.freshdesk.com/support/solutions/articles/44000884036-duplicate-variations">
            Know more <span class="fa fa-caret-right"></span>
          </a>
        </div>
      </label>
      <div class="col-12 col-sm-8 col-md-6 col-lg-6">

        <div class="form-check mt-1">
          <input class="form-check-input duplicatepref" type="radio" name="duplicatepreference" id="duplicatepref1" value="0" {{$usersettings->duplicatepreference == 0 ? 'checked' : ''}} >
          <label class="form-check-label" for="duplicatepref1">
            Merchant Fulfilled
          </label>
        </div>
        <div class="form-check mb-1">
          <input class="form-check-input duplicatepref" type="radio" name="duplicatepreference" id="duplicatepref2" value="1" {{$usersettings->duplicatepreference == 1 ? 'checked' : ''}}>
          <label class="form-check-label" for="duplicatepref2">
            Amazon Fulfilled
          </label>
        </div>
      </div>
    </div>
    

    <div class="form-group row">
      <label for="usr" class=" col-12 col-sm-4 col-md-4 col-form-label">Product Template Suffix</label>
      <div class="col-12 col-sm-8 col-md-6 col-lg-6">
        <input  name="templatesuffix" class="form-control" placeholder="alternate" value="{{$usersettings->templatesuffix or ''}}">
      </div>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-primary text-left ">Save</button>
    </div>

  </form>
</div>
</div>
</div>

<div id="tag-list-Modal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-light">
        <p class="modal-title">Select values from Amazon to be added as product tags in Shopify
          <br>

          <a class="text-primary small" href="https://connectr.freshdesk.com/support/solutions/articles/44000329495-auto-tagging" target="_blank">Know More
            <span class="fa fa-caret-right"></span>
          </a>
        </p>

        <button type="button" class="close" data-dismiss="modal"  aria-label="Close">
          <span aria-hidden="true">&times;</span></button>

        </div>

        <div class="modal-body">
          <div id="tag-success" role="alert" class="alert alert-dismissible alert-success text-center" style="display:none">
            Updated Successfully
          </div>
          <form name="form-tags" id="form-tags">
            {{ csrf_field() }}
            <div class="form-row" id="tag_list">

            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button onclick="saveTags();" value="save" class="btn btn-primary text-right" type="button">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>

    </div>
  </div>

  <div id="inventorypolicymodal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-light">
          <p class="modal-title">Inventory Policy Settings</p>



          <button type="button" class="close" data-dismiss="modal"  aria-label="Close">
            <span aria-hidden="true">&times;</span></button>

          </div>

          <div class="modal-body">
            <div id="policy-tag-success" role="alert" class="alert alert-dismissible alert-success text-center" style="display:none">
              Updated Successfully
            </div>
            <div class="col-12">
              <form name="inventory-policy" id="inventory-policy" onsubmit="event.preventDefault();  return saveSetting('#inventory-policy','{{asset('defaultsetting')}}');" >
                {{csrf_field() }}
                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">For Merchant Fulfilled SKUs</label>
                  <div class="col-12 col-sm-8 col-md-8">
                    <select name="isshowonstore" class="form-control" id="exampleFormControlSelect1">
                      <option>Shopify Tracks Inventory</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">For Amazon Fulfilled SKUs</label>
                  <div class="col-12 col-sm-8 col-md-8">
                    <select name="awsinventorypolicy" class="form-control" id="awsinventorypolicy">
                      <option value="0"  {{$usersettings->awsinventorypolicy == 0 ? 'selected' : ''}} >Shopify Tracks Inventory</option>
                      <option value="1"  {{$usersettings->awsinventorypolicy == 1 ? 'selected' : ''}} > Amazon Marketplace Web Tracks Inventory</option>
                    </select>
                    <div id="mwsinfo" class="card bg-light mt-2">
                      <div class="card-body small card-info">
                        <p>
                          Before selecting 'Amazon Marketplace Web Tracks Inventory' you need to activate Fulfillment by Amazon on your Shopify store.
                          More info available <a target="_blank" class="" href="https://help.shopify.com/en/manual/shipping/rates-and-methods/fulfillment-services/amazon">here</a>.
                        </p>
                        <div class="w-100 inline-block">
                          <a class="btn btn-sm btn-primary float-right text-white " onclick="verifyInventoryPolicy();">Verify Amazon Fulfillment <span id="loader" class="fas fa-circle-notch fa-spin ml-2 fulfillmentpiner" ></span></a>
                        </div>

                        <div class="w-100 d-inline-block text-right mt-2" >
                          <span id="awsinventoryconnected" class="text-success">
                            <span class="fa fa-check"></span>&nbsp;
                            Active
                          </span>
                          <span id="awsinventorynotconnected" class="text-danger">
                            <span class="fa fa-ban"></span>&nbsp;
                            Inactive
                          </span>
                        </div>


                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="modal-footer">
              <div class="pull-right">
                <button id="saveInventoryPolicy"  value="save" class="btn btn-primary text-right" type="submit">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>

            </div>
          </div>
        </form>
      </div>
    </div>

    <div id="inventoryLocation-Modal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header bg-light">
            <p class="modal-title">Select your Shopify default inventory location
              <!--                <br>

              <a class="text-primary small" href="https://spreadr.freshdesk.com/solution/articles/24000020475-what-is-auto-tagging-" target="_blank">Know More
              <span class="fa fa-caret-right"></span>
            </a>-->
          </p>



          <button type="button" class="close" data-dismiss="modal"  aria-label="Close">
            <span aria-hidden="true">&times;</span></button>

          </div>

          <div class="modal-body">
            <div id="tag-success" role="alert" class="alert alert-dismissible alert-success text-center" style="display:none">
              Updated Successfully
            </div>
            <form name="inventory-locations" id="inventory-locations">
              {{ csrf_field() }}
              <div class="container" id="location_list">

              </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="pull-right">
              <button onclick="saveInventoryLocation();" value="save" class="btn btn-primary text-right" type="button">Save</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

          </div>
        </div>

      </div>
    </div>
    <div id="descriptionOptionModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header bg-light">
            <span class="">Description Data Fields -
              <small>
                <a href="https://connectr.freshdesk.com/support/solutions/articles/44001405296-description-data-fields/" target="_blank">Know More&nbsp;
                <span class="fa fa-caret-right"></span></a>
              </small>
            </span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
          </div>

          <div class="modal-body">




            <div id="description-success" role="alert" class="alert alert-dismissible alert-success text-center" style="display:none">
              Updated Successfully
            </div>
            <div class="row">
              <div class="col-12"><div class="col-12">
                <?php
                $descriptionoptions = array('Actor','Artist','AspectRatio','AudienceRating','AudioFormat','Author','Brand','CEROAgeRating','Color','Director','Edition','EISBN','EpisodeSequence','ESRBAgeRating','Features','Format','Genre','HardwarePlatform','HazardousMaterialType','ISBN','IssuesPerYear','LegalDisclaimer','ManufacturerPartsWarrantyDescription','MediaType','NumberOfDiscs','NumberOfIssues','NumberOfPages','OperatingSystem','Platform','Publisher','RunningTime','SubscriptionLength','Warranty','Details','UPC','EAN','ReleaseDate','PackageDimensions','Languages');
                $descriptionoptionsdefault = $descriptionoptions;

                $descriptionoptionraw = isset($useroptions['descriptionoption']) ? $useroptions['descriptionoption'] : null;
                if(isset($useroptions['descriptionoption'])) {

                  $settingsdescriptionoptions = json_decode($useroptions['descriptionoption'],true);
                } else {
                  $settingsdescriptionoptions = array();
                }


                $selectedoptions = array();
                if(!empty($settingsdescriptionoptions)) {
                  $selectedoptions = array_keys($settingsdescriptionoptions);
                }

                $descriptionoptions = array_diff($descriptionoptions, $selectedoptions);

                ?>
                <ul id="defaultDescriptionOptions" style="display:none">
                  @foreach($descriptionoptionsdefault as $descriptionoption)
                  <li class="ui-state-default list-group-item" style="cursor: grab;">
                    <span style="margin-top: 8px;" class="fa fa-bars float-right text-muted small mt-2 mr-1"></span>
                    <div class="checkbox">
                      <label class="m-0"><input  class="descriptionoption mr-1" name="descriptionoption[{{$descriptionoption}}]" type="checkbox" checked value="{{$descriptionoption}}">{{$descriptionoption}}</label>
                    </div>
                  </li>

                  @endforeach
                </ul>


                <form class="form-horizontal" method='POST' id="settings-descriptionoption" action='settings-descriptionoption' enctype="multipart/form-data">
                  <ul id="sortable" class="list-group">
                    @foreach($selectedoptions as $selectedoption)
                    <li class="ui-state-default list-group-item p-1 m-0" style="cursor: grab;">
                      <span style="margin-top: 8px;" class="fa fa-bars float-right text-muted small mt-2 mr-1"></span>
                      <div class="checkbox">
                        <label class="m-0"><input class="descriptionoption mr-1" name="descriptionoption[{{$selectedoption}}]" type="checkbox" value="{{$selectedoption}}" checked>{{$selectedoption}}</label>
                      </div>

                    </li>
                    @endforeach

                    @foreach($descriptionoptions as $descriptionoption)
                    <li class="ui-state-default list-group-item" style="cursor: grab;">
                      <span style="margin-top: 8px;" class="fa fa-bars float-right text-muted small mt-2 mr-1"></span>
                      <div class="checkbox">
                        <label class="m-0"><input  class="descriptionoption mr-1" name="descriptionoption[{{$descriptionoption}}]" type="checkbox" @if(empty($descriptionoptionraw)) checked @endif value="{{$descriptionoption}}">{{$descriptionoption}}</label>
                      </div>
                    </li>
                    @endforeach

                  </ul>

                </form>
              </div> </div></div>
            </div>
            <div class="modal-footer">
              <p><img id="loadermark" class="loadermark" src="assets/img/ajax-loader.gif" style="display: none;" align="middle" ></p>
              <span id="successverify" class="text-right text-success" style="display: none;">
                <p>
                  <i class="fa fa-check" aria-hidden="true"></i>
                </p>
              </span>

              <span id="failsverify" class="text-right text-danger" style="display: none;">
                <p>
                  <i class="fa fa-times" aria-hidden="true"></i>

                </p>
              </span>
              <div class="pull-right">
                <button onclick="saveDescriptionOptions();" value="save" class="btn btn-primary text-right" type="button">Save</button>

                <button onclick="uncheckDescriptionOptions();" value="save" class="btn btn-default text-right" type="button">Uncheck All</button>
                <button onclick="resetDescriptionOptions();" value="save" class="btn btn-default text-right" type="button">Reset</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>

            </div>
          </div>

        </div>
      </div>


      <script>
      $(function() {
        $( "#sortable" ).sortable({
          placeholder: "ui-sortable-placeholder"
        });
      });
    </script>


    <script type="text/javascript">
    $(function () {
      $('.fulfillmentpiner').hide();
      $('#awsinventorynotconnected').hide();
      @if($usersettings->awsinventorypolicy == 0 )
      $('#awsinventoryconnected').hide();
      $('#awsinventorynotconnected').show();
      $('#mwsinfo').hide();
      @endif
      $('#awsinventorypolicy').change(function() {
        if($(this).val() == 1) {
          $("#saveInventoryPolicy").attr("disabled", true);
          $('#mwsinfo').show();
        } else {
          $("#saveInventoryPolicy").attr("disabled", false);
          $('#mwsinfo').hide();
        }

      });


      $('[data-toggle="tooltip"]').tooltip()
    })
    function openTagForm( ) {
      $("#tag-success").hide();
      $('#tag-list-Modal').modal('show');
      $('#tag_list').html( '<div class="text-center">Loading</div>' );

      $.ajax({

        method  :'GET',
        url     :"{{ asset('setting-taglist') }}",
        success: function( response ){

          var tagList = '';

          //            tagList += '<div class="form-group col-12 col-sm-6 col-md-4">\
          //                        <span class=" col-10 text-right">All</span> \
          //                        <div class=" col-2">\
          //                        <div class="checkbox">\
          //                        <input name="all_tag" id="all_tag" type="checkbox" value="1"></div>\
          //                        </div></div>';

          for( let index in response ) {





            tagList += '<div class="col-12 col-sm-6">\
            <div class="form-row">	<div class="form-group col-auto ml-5">\
            <input class="form-check-input" type="checkbox" name="tags['+index+']" '+(response[index] == 1 ? 'checked=checked' :'')+' value="1">\
            </div>\
            <div class="form-group col-5">\
            <label class="form-check-label">'+index+'</label>\
            </div>\
            </div></div>';
          }

          $('#tag_list').html( tagList );
        }
      });

    }


    function changeInventoryLocation( ) {
      $("#tag-success").hide();
      $('#inventoryLocation-Modal').modal('show');
      $('#location_list').html( '<div class="text-center">Loading</div>' );
      let locationidtmp = $('#locationidtmp').val();
      $.ajax({

        method  :'GET',
        url     :"{{ asset('getinventorylocations') }}",
        success: function( response ){

          var locationList = '';

          for( let index in response ) {





            locationList += ' <div class="form-check">\
            <label class="form-check-label" for="locationref">\
            <input type="radio" class="form-check-input" id="locationref" name="locationref" '+(response[index]['id'] == locationidtmp ? 'checked=checked' :'')+' data-location="'+response[index]['name']+'"  value="'+response[index]['id']+'">'+response[index]['name']+' \
            </label>\
            </div>';
          }

          $('#location_list').html( locationList );
        }
      });

    }

    function saveInventoryLocation() {

      $('#locationtmp').val($('input[name=locationref]:checked').data('location'));
      $('#locationidtmp').val($('input[name=locationref]:checked').val());
      let locationData = {
        'locationid' : $('input[name=locationref]:checked').val(),
        'location' : $('input[name=locationref]:checked').data('location'),
        '_token':'{{csrf_token()}}'
      };
      $.ajax({
        method  :'POST',
        url     :"{{ asset('defaultsetting') }}",
        data    : locationData,
        success: function( response ){

          $('#inventoryLocation-Modal').modal('hide');
        }

      });
    }

    function verifyInventoryPolicy() {
      $('.fulfillmentpiner').show();
      $.ajax({
        method  :'GET',
        url     :"{{ asset('verifyamazonfulfillment') }}",
        success: function( response ){
          $('.fulfillmentpiner').hide();
          if(response.status == false){
            $('#awsinventorynotconnected').show();
            $('#awsinventoryconnected').hide();
            $("#saveInventoryPolicy").attr("disabled", true);
          }else {
            $('#awsinventorynotconnected').hide();
            $('#awsinventoryconnected').show();
            $("#saveInventoryPolicy").attr("disabled", false);
          }
        }

      });
    }

    function saveInventoryPolicy() {
      let data = $('#form-tags').serialize();

      $.ajax({
        method  :'POST',
        url     :"{{ asset('defaultsetting') }}",
        data    : data,
        success: function( response ){

          if( response == 1 ) {
            $("#tag-success").show();
            setTimeout(function(){
              $('#tag-list-Modal').modal('hide');
            },3000);

          }else{
            $("#tag-success").hide();
          }
        }

      });
    }

    function saveTags( ) {

      let data = $('#form-tags').serialize();

      $.ajax({
        method  :'POST',
        url     :"{{ asset('setting-taglist') }}",
        data    : data,
        success: function( response ){

          if( response == 1 ) {
            $("#tag-success").show();
            setTimeout(function(){
              $('#tag-list-Modal').modal('hide');
            },3000);

          }else{
            $("#tag-success").hide();
          }
        }

      });
    }

    $('#isautoproducttype').change(function() {
      if($(this).is(":checked")) {
        $("#producttype").attr("readonly", true);
      } else {
        $("#producttype").attr("readonly", false);
      }


    });

    $('#isautoamazonvendor').change(function() {
      if($(this).is(":checked")) {
        $("#vendor").attr("readonly", true);
      } else {
        $("#vendor").attr("readonly", false);
      }


    });


    $('.inventorytype').change(function() {
      console.log();

      if($(this).val() == 1) {
        $("#quantity").attr("readonly", true);
      } else {
        $("#quantity").attr("readonly", false);
      }


    });



    function saveDescriptionOptions( ) {

      let data = $('#settings-descriptionoption').serialize();
      $('#loadermark').show();
      $.ajax({
        method  :'POST',
        url     :"{{ Config::get('app.base_url') }}settings-descriptionoption",
        data    : data,
        success: function( response ){
          $('#loadermark').hide();
          if( response == 1 ) {


            $("#successverify").show();
            $("#failsverify").hide();
            setTimeout(function(){
              $('#descriptionOptionModal').modal('hide');
            },3000);

          }else{
            $("#successverify").hide();
            $("#failsverify").show();
          }
        }

      });
    }

    function resetDescriptionOptions( ) {


      $('#sortable').html( $('#defaultDescriptionOptions').html());
    }

    function uncheckDescriptionOptions() {
      $('#sortable .descriptionoption').removeAttr('checked');
    }
  </script>
