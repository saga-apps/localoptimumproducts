<?php

$arrRegion = array(

                  'ATVPDKIKX0DER'   =>  array('name'=> 'US','developerid'=> '309244739554'),
                  'A2EUQ1WTGCTBG2'  =>  array('name'=> 'Canada','developerid'=> '309244739554'),
                  'A1AM78C64UM0Y8'  =>  array('name'=> 'Mexico','developerid'=> '309244739554'),
                  'A2Q3Y263D00KWC'  =>  array('name'=> 'Brazil','developerid'=> '309244739554'),
                  'A1RKKUPIHCS9HS'  =>  array('name'=> 'Spain','developerid'=> '591307861212'),
                  'A1PA6795UKMFR9'  =>  array('name'=> 'Germany','developerid'=> '591307861212'),
                  'A13V1IB3VIYZZH'  =>  array('name'=> 'France','developerid'=> '591307861212'),
                  'A1F83G8C2ARO7P'  =>  array('name'=> 'UK','developerid'=> '591307861212'),
                  'A21TJRUUN4KGV'   =>  array('name'=> 'India','developerid'=> '591307861212'),
                  'APJ6JRA9NG5V4'   =>  array('name'=> 'Italy','developerid'=> '591307861212'),
//                  'A39IBJ37TRP1C6'  =>  'Australia',
//                  'A1VC38T7YXB528'  =>  'Japan',
//                  'A33AVAJ2PDY3EV'  =>  'Turkey'
              );


?>
@section('title')
@parent
<title>Amazon Settings</title>
@stop
<div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
    <div class="card mt-3">
      <div class="card-header">
       Connect your Amazon Seller Account
       @if( empty( $data->isverified ) )
       <span id="notconnected" class="text-danger float-right">
            <span class="fa fa-ban"></span>&nbsp;
              Not connected
              </span>
        @else
       <span id="connected" class="text-success float-right">
            <span class="fa fa-check"></span>&nbsp;
              Connected
              </span>
        @endif
     </div>
        <form name="mwscredential" id="mwscredential" class="text-left">
       <div class="card-body">
            <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Region</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <select id="Marketplace" class="form-control" name="marketplace">
              @foreach( $arrRegion as $marketplace => $region )
                <option {{!empty( $data->marketplaceid ) && $data->marketplaceid ==$marketplace ?'selected'  :''}} value="{{$marketplace}}" marketplace="{{$marketplace}}" developerid="{{$region['developerid']}}">{{$region['name']}}</option>
              @endforeach
            </select>
          </div>
        </div>
            <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Developer ID</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
              <div class="input-group mb-1">
                  <input readonly id="developerid" value="ASKJDKLJASKLDJAKSLD" type="text" class="form-control" placeholder="ASKJDKLJASKLDJAKSLD" aria-label="developerid" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-outline-secondary" type="button" id="copyDeveloperId">copy</button>
                </div>
              </div>
              <div class="mt-1 ml-1">
<!--              <a target="_blank" class="small" href="https://thewatchlyst.freshdesk.com/support/solutions/articles/19000000644-how-much-does-the-watchlyst-cost-" >
                     Instructions to setup Developer ID
                    <span class="fa fa-caret-right"></span>
              </a>-->
            </div>
               </div>
        </div>
           <hr class="py-1">
         <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Seller ID</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <input type="text" required value="{{!empty( $data->sellerid ) ? $data->sellerid :''}}" name="sellerid" class="form-control" placeholder="ACE3S9F286J00">
          </div>
        </div>
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Auth Token</label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <input required value="{{!empty( $data->authtoken ) ? $data->authtoken :''}}" name="authtoken" type="text" class="form-control mb-1" placeholder="amzn.mws.acb398d1-659c-91c8-c102-b138f3cf000">
             <div class="mt-1 ml-1">
               <a target="_blank" class="small" href="https://connectr.freshdesk.com/support/solutions/articles/44000328756-how-do-i-find-my-seller-id-marketplace-id-mws-access-token-with-screenshots-" >
                      Instructions to setup Seller ID and Auth Token
                    <span class="fa fa-caret-right"></span>
              </a>
            </div>
          </div>

        </div>

      </br>
        <div class="text-right">
          <button type="button" id="savemwscredential" class="btn btn-primary text-left ">Save and Connect</button>

        </div>


      </div>
      <form>
    </div>
  </div>
  <script type="text/javascript">
    var arrRegion = '<?php echo json_encode($arrRegion );?>';
    arrRegion = JSON.parse( arrRegion );
    $("#developerid").val(arrRegion[$("#Marketplace").val()]['developerid'] );

    $("#Marketplace").on("change", function(){
      $("#developerid").val(arrRegion[$(this).val()]['developerid'] );
    });

    $(document).on("click","#copyDeveloperId",function(){
        var copyText = document.getElementById("developerid");
        copyText.select();
        document.execCommand("copy");
    });
  $(document).on("click","#savemwscredential",function(){

    $('#loadingalert').show();

    mwscredentialData = $("#mwscredential").serialize();

    var mwscredential = {
                          'mwscredential':mwscredentialData,
                          '_token':'{{csrf_token()}}'
                                //.//'actiontype':actiontype
                          };

      $.ajax({
              method:"POST",
              url : '{{asset('settings-amazonmws')}}',
              data:mwscredential,
              dataType: 'json',
              success : function( data ){

                  $('#loadingalert').hide();
                  $('#requestmessage').text(data.msg);
                  toggleAlert();
                  //if( data.status == true ){

                    setTimeout( function(){
                      location.reload();
                    }, 2000);

                  //}

              }

          });
      });

  </script>
