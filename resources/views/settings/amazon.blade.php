<div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
    <div class="card mt-3">
      <div class="card-header">
       Enter your Amazon affiliate ID
     </div>
     <div class="card-body text-center">
       <div class="form-group row">
        <label for="staticEmail" class="col-sm-4 col-4 text-right col-form-label font-weight-bold">Affiliate/Associate ID</label>
        <div class="col-8 col-sm-5 col-md-4 col-lg-4">
          <input type="password" class="form-control" placeholder="amztag-20">
        </div>
      </div>
      <div class="form-group row">
        <label for="staticEmail" class="col-sm-4 col-4 text-right col-form-label font-weight-bold">Country</label>
        <div class="col-8 col-sm-5 col-md-4 col-lg-4">
          <select id="inputState" class="form-control">
            <option selected>Canada</option>
            <option>United Kingdom</option>
            <option>Germany</option>
            <option>France</option>
            <option>Spain</option>
          </select>
        </div>
      </div>
      <div class="text-right">
        <button type="button" class="btn btn-primary text-left ">Save</button>
      </div>

    </div>
    </div>
  </div>