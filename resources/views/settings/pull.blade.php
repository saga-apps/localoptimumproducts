@section('title')
@parent
<title>Pull Settings</title>
@stop
<div class="col-12 col-sm-10 col-md-8 col-lg-7">

  <div class="card mt-3">
    <div class="card-header">
      Settings for pulling products from Amazon
    </div>
    <div class="card-body">
      <form onsubmit="event.preventDefault();  return savePullingProductSetting('#form-pull-settings','{{asset('pullsetting')}}');" id="form-pull-settings" name="form-button-settings">
        {{csrf_field() }}
       
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
            Listing Status
<!--            <div class="small">
                <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="App will auto-fill product type field with the product group value derived from Amazon. If product group is not available then blank value will be saved. Alternatively, you can set your own default product type or keep it blank.">
                Know more
              </a>
            </div>-->
          </label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="listing" id="Listing1" value="1" {{ !empty( $usersettings->listingstatus ) && $usersettings->listingstatus == 1 ?'checked':''  }} >
                <label class="form-check-label" for="exampleRadios1">
                  All
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="listing" id="Listing2" value="2" {{ !empty( $usersettings->listingstatus ) && $usersettings->listingstatus == 2 ?'checked':empty($usersettings->listingstatus ) ? 'checked':''  }} >
                <label class="form-check-label" for="exampleRadios2">
                    Active <span class="text-muted small">(recommended)</span>
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="listing" id="Listing3" value="3" {{ !empty( $usersettings->listingstatus ) && $usersettings->listingstatus == 3 ?'checked':''  }} >
                <label class="form-check-label" for="exampleRadios3">
                  Inactive
                </label>
              </div>
          </div>
      </div>
        <div class="form-group row">
          <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">
            Fulfillment Type
<!--            <div class="small">
                <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="App will auto-fill product type field with the product group value derived from Amazon. If product group is not available then blank value will be saved. Alternatively, you can set your own default product type or keep it blank.">
                Know more
              </a>
            </div>-->
          </label>
          <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fulfillment" id="Fulfillment1" value="1" {{ !empty( $usersettings->fulfillmenttype ) && $usersettings->fulfillmenttype == 1 ?'checked':'checked'  }} >
                <label class="form-check-label" for="exampleRadios1">
                  All
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="fulfillment" id="Fulfillment2" value="2" {{ !empty( $usersettings->fulfillmenttype ) && $usersettings->fulfillmenttype == 2 ?'checked':''  }}>
                <label class="form-check-label" for="exampleRadios2">
                  Merchant
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="fulfillment" id="Fulfillment3" value="3" {{ !empty( $usersettings->fulfillmenttype ) && $usersettings->fulfillmenttype == 3 ?'checked':''  }}>
                <label class="form-check-label" for="exampleRadios3">
                  Amazon (FBA)
                </label>
              </div>
          </div>
      </div>


<div class="text-right">
  <button type="submit" class="btn btn-primary text-left ">Save</button>
</div>

</form>
</div>
</div>
</div>

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

    function savePullingProductSetting( formId , route ) {
      
      $('#loadingalert').show();
      $.ajax({
            method  :'POST',
            url     :route,
            data    : $(formId).serialize(),
            success: function( response ){
              $('#loadingalert').hide();
              $('#requestmessage').text(response.msg);
                toggleAlert();
            }

        });
    }


   
</script>
