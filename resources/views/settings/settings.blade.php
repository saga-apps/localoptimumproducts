@extends('layouts.master')


@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')
@parent

@stop

@section('content')
@parent

<div class="row">

@include('partials.sidebar')

  {{-- @if( $page == 'amazon' )
  @include('settings.amazon')
  @elseif( $page == 'pull' )
  @include('settings.pull')
  @elseif( $page == 'default' )
  @include('settings.default')
  @elseif( $page == 'button' )
  @include('settings.buttons')
  @elseif( $page == 'reviews' )
  @include('settings.reviews')
  @elseif( $page == 'amazonmws' )
  @include('settings.amazonmws')
  @elseif( $page == 'sync' )
  @include('settings.sync')
  @elseif( $page == 'fba' )
  @include('settings.fba') --}}
  
{{-- @endif --}}
  
</div>

@stop

@section('scripts')
@parent

<script type="text/javascript">


    function saveSetting( fromId , Url ){

        // var fromData = $(fromId).serialize();
        $('#loadingalert').show();
        var form = $(fromId)[0];
         // var formData = new FormData($(fromId)[0]);
         var objformData = new FormData(form);
            console.log(objformData);
        $.ajax({
                method:"POST",
                url : Url,
                data : objformData,
                contentType: false,
                processData: false,

                success : function( data ){
                    $('#loadingalert').hide();
                    $('#requestmessage').text(data.msg);
                    if(data.logoimg) {
                        $('#emailLogo').attr("src", data.logoimg);
                    }
                    toggleAlert();
                }

            });

    }


  function importProductReviews( instance ){

  $('#loadingalert').show();
    
    $.ajax({
          method:"GET",
          url : '{{asset('importproduct-reviewsaction')}}/'+$(instance).val(),
          success : function( data ){
            
            $('#loadingalert').hide();
            if( true == data.status ){
              inProgressModal();
            }else{
              $('#requestmessage').text(data.msg);
              toggleAlert();
            }

          }
    });
  }


</script>

@stop