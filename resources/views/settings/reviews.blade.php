@section('title')
@parent
<title>Review Settings</title>
@stop



<style>
  .bg-warning {
  background-color: #fcf8e3 !important;
}
.bg-danger {
  background-color: #ffe5e5 !important;
}
.btn-warning {
  color: #fff !important;
  background-color: #f0ad4e;
    border-color: #eea236;
}
</style>


<div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">

<!--  <div class="float-left mt-3">
    <button type="button" class="float-right btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Bulk Actions
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item actionall" actiontype="4" href="#">Import Reviews All Products</a>
      <a class="dropdown-item actionall" actiontype="5" href="#">Import Reviews New Products</a>
    </div>
  </div>
  <br>-->

        <div class="mt-3">
          <div class="card">
              <div class="card-header">
                Review Import Pricing
                <a target="_blank" class="float-right" href="https://connectr.freshdesk.com/support/solutions/articles/44001215931-review-pricing">
                Details&nbsp;
                <span class="fa fa-caret-right"></span>
                </a>
              </div>
              <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                      <div class="bg-light text-center card p-2 mb-3">
                          <h2>
                            {{$reviewsettings->reviewcredit or 0}}
                          </h2>
                          <p class="card-text small">
                            Review Credits
                          </p>
                      </div>
                    </div>
                    <div class=" col-md-8 col-sm-8 col-xs-12">
                      <div class="">
                          <p>
                            USD 1 = 10 credits (Import reviews for 10 products)
                          </p>
                      </div>
                      <form method="POST" action="{{asset('purchasereviewcredits')}}">
                          {{csrf_field() }}
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Credits</span>
                            </div>
                            <input id="reviewproducts" name="reviewproducts" type="number" class="form-control" value="10" placeholder="10" min="1" max="600" step="1" maxlength="3" onkeypress="return event.charCode >= 48">
                            <div class="input-group-append">
                                <span class="input-group-text" id="reviewcredits">$ 1</span>
                            </div>
                          </div>
                          <button class="btn btn-primary" type="submit">Buy Credits</button>
                      </form>
                    </div>
                </div>
              </div>
          </div>
        </div>









            <div class="card mt-3">
              <div class="card-header">
                Settings for displaying customer reviews on Shopify
                <a target="_blank" class="float-right" href="https://connectr.freshdesk.com/support/solutions/articles/44000527342-importing-customer-reviews">
                Help&nbsp;
                <span class="fa fa-caret-right"></span>
                </a>
              </div>
              <div class="card-body ">
                <form id="reviewSettingForm" onsubmit="event.preventDefault();  return saveSetting('#reviewSettingForm','{{asset('settings-reviews')}}');" >
                    {{csrf_field() }}
                  <div class="form-group row">
                  <label for="staticEmail" class="col-sm-4 col-12 col-md-4 text-left col-form-label">Reviews Box
                   <div class="small">
                <a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Show or hide reviews on Shopify product pages.">
                Know more
              </a>
            </div>
                  </label>
                 
                  <div class="col-8 col-sm-8 col-md-6 col-lg-6">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                      <label class="btn btn-outline-primary {{isset($reviewsettings->isreviewon) && $reviewsettings->isreviewon == 1 ? 'active' : ''}} {{!isset($reviewsettings->isreviewon) ? 'active' : ''}}" >
                        <input type="radio" value="1" name="isreviewon" {{isset($reviewsettings->isreviewon) && $reviewsettings->isreviewon == 1 ? 'checked' : ''}}> Show
                      </label>

                      <label class="btn btn-outline-primary {{isset($reviewsettings->isreviewon) && $reviewsettings->isreviewon == 0 ? 'active' : ''}}">
                        <input type="radio" value="0" name="isreviewon" {{isset($reviewsettings->isreviewon) && $reviewsettings->isreviewon == 0 ? 'checked' : ''}} > Hide
                      </label>
                    </div>

                  </div>
                </div>


                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Display Reviews</label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                    <select name="filterstar" class="form-control" id="exampleFormControlSelect1">

                      <option value="1" {{isset($reviewsettings->filterstar) && $reviewsettings->filterstar == 1 ? 'selected' : ''}} >All</option>
                      <option  value="2" {{isset($reviewsettings->filterstar) && $reviewsettings->filterstar == 2 ? 'selected' : ''}} >2 stars and above</option>
                      <option  value="3" {{isset($reviewsettings->filterstar) && $reviewsettings->filterstar == 3 ? 'selected' : ''}} >3 stars and above</option>
                      <option  value="4" {{isset($reviewsettings->filterstar) && $reviewsettings->filterstar == 4 ? 'selected' : ''}} >4 stars and above</option>
                      <option  value="5" {{isset($reviewsettings->filterstar) && $reviewsettings->filterstar == 5 ? 'selected' : ''}} >Only 5 Stars</option>

                    </select>
                  </div>
                </div>


                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Histogram
                  <div class="small"><a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Show or hide histogram stats inside the reveiw box.">
                Know more
              </a></div>
                  </label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <label class="btn btn-outline-primary {{isset($reviewsettings->showhistogram) && $reviewsettings->showhistogram == 1 ? 'active' : ''}} {{!isset($reviewsettings->showhistogram) ? 'active' : ''}}">
                            <input type="radio" value="1" name="showhistogram" {{isset($reviewsettings->showhistogram) && $reviewsettings->showhistogram == 1 ? 'checked' : ''}}> Show
                          </label>

                          <label class="btn btn-outline-primary {{isset($reviewsettings->showhistogram) && $reviewsettings->showhistogram == 0 ? 'active' : ''}} ">
                            <input type="radio" value="0" name="showhistogram" {{isset($reviewsettings->showhistogram) && $reviewsettings->showhistogram == 0 ? 'checked' : ''}} > Hide
                          </label>
                        </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">When No Reviews Found
                   <div class="small"><a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Show or hide review box for any product when when zero reviews found on Amazon.">
                Know more
              </a></div>
                  </label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <label class="btn btn-outline-primary {{isset($reviewsettings->noreview) && $reviewsettings->noreview == 1 ? 'active' : ''}} {{!isset($reviewsettings->noreview) ? 'active' : ''}}">
                            <input type="radio" value="1" name="noreview" {{isset($reviewsettings->noreview) && $reviewsettings->noreview == 1 ? 'checked' : ''}}> Show
                          </label>

                          <label class="btn btn-outline-primary {{isset($reviewsettings->noreview) && $reviewsettings->noreview == 0 ? 'active' : ''}} ">
                            <input type="radio" value="0" name="noreview" {{isset($reviewsettings->noreview) && $reviewsettings->noreview == 0 ? 'checked' : ''}} > Hide
                          </label>
                        </div>
                  </div>
                </div>


                {{-- <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Reviewer</label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                    <select class="form-control" id="exampleFormControlSelect1">
                      <option>All</option>
                      <option> Verified only</option>
                    </select>
                  </div>
                </div> --}}
                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Custom CSS
                   <div class="small"><a class="text-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add CSS code to customize the design of the review box. For advanced users only.">
                Know more
              </a></div>
                  </label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6">

                    <button type="button" class=" btn btn-outline-secondary" data-toggle="modal" data-target="#customCssModal">
                      Edit
                    </button>


                  </div>
                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-primary text-left ">Save</button>
                </div>
              </form>
              </div>

            </div>


 <div class="alert alert-warning mt-3 text-left">
          <div class="row">
            <div class="col-12">
              <h6>Add two-line integration code to display the review box on your store&nbsp;
                  <a class="btn btn-warning mt-2" target="_blank" href="https://connectr.freshdesk.com/a/solutions/articles/44001250789-integrating-customer-reviews/">View integration guide</a>
            </h6>
            </div>
            <div class=" col-12 col-sm-6  text-left">
              </div>
          </div>
        </div>
            <!--<div class="card mt-3">
              <div class="card-header">
                Action
              </div>
              <div class="card-body">
                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label font-weight-bold ">Review Limit</label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6 mt-2">
                    <span class="label">{{$planProductReviewsLimit}}</span>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label font-weight-bold">Review Imported</label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6 mt-2">
                    <span class="label">{{$reviewsimported}}</span>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label font-weight-bold">Bulk Actions</label>
                  <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                    <select class="form-control" onchange="importProductReviews(this);">
                    <option value="0" >Select Action</option>
                    <option value="1"> Import Reviews for all Products</option>
                    <option value="2" >Import Reviews for new Products</option>
                  </select>
                  </div>
                </div>
              </div>

            </div>-->


</div>
</div>


<div class="modal fade" id="customCssModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form id="reviewCustomCss" onsubmit="event.preventDefault();  return saveSetting('#reviewCustomCss','{{asset('settings-reviews')}}');">
            {{csrf_field() }}
          <div class="modal-header bg-light">
          <p class="modal-title" id="exampleModalLabel">Edit CSS</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            	&lt;style&gt;
          <textarea name="customcss" rows="8" class="w-100 mt-2">{{$reviewsettings->customcss or ''}}</textarea>
          &lt;/style&gt;
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
</div>

<div id="modalConfirmYesNo" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-light">
        <p class="modal-title" >
          <div class="reviewsall hideall"> Import Reviews for all Products</div>
          <div class="reviewsnew hideall">Import Reviews for new Products</div>
        </p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="reviewsall hideall">
            <div class="font-weight-bold">Are your sure you want to import Reviews for all Products?</div>
             <div class="small mt-2">
                  This will ..............

             </div>
        </div>

        <div class="reviewsnew hideall">
            <div class="font-weight-bold">Are your sure you want to import Reviews for new Products?</div>
             <div class="small mt-2">
                  This will ..............

             </div>
        </div>
      </div>
      <div class="modal-footer">
          <div class="pull-right">
            <button id="btnYesConfirmYesNo" type="button" class="btn btn-primary text-right">Confirm</button>
            <button id="btnNoConfirmYesNo" type="button" class="btn btn-default">Cancel</button>
          </div>

        </div>
    </div>
  </div>
</div>

<script>
    $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
$(document).ready(function(){

  $("#reviewproducts").keyup(function() {
    var reviewproducts = $('#reviewproducts').val();

    var credits = Math.ceil(reviewproducts/10);

    $('#reviewcredits').text('$ '+credits );

  });




  $('#reviewproducts').bind('click keyup', function(){
      var reviewproducts = $('#reviewproducts').val();

  var credits = Math.ceil(reviewproducts/10);

  $('#reviewcredits').text('$ '+credits );
  });

});

var actiontype = '';

$(document).on('click','.actionall',function(){
  $('.hideall').hide();
    var actiontype = $(this).attr('actiontype');
   $('#btnYesConfirmYesNo').removeAttr( 'disabled');
  if( actiontype == 4) {
      $('.reviewsall').show();

    $('#btnYesConfirmYesNo').prop('{{Auth::User()->productcredit <= 0 ? "disabled":''}}', true);
    //$('#btnYesConfirmYesNo').attr( '' );
  }else if (actiontype == 5) {
      $('.reviewsnew').show();
      $('#btnYesConfirmYesNo').prop('{{Auth::User()->productcredit <= 0 ? "disabled":''}}', true);
  }
  var $confirm = $("#modalConfirmYesNo");
  $confirm.modal('show');


  let objProductAction = {
                     'actiontype' : $(this).attr('actiontype'),
                     '_token':'{{csrf_token()}}'
                };
  $("#btnYesConfirmYesNo").off('click').click(function () {
      $confirm.modal("hide");
      $('#loadingalert').show();

    $.ajax({
            method:"POST",
            url : '{{asset('product-pullpush-all')}}',
            data:objProductAction,
            dataType: 'json',
            success : function( data ){

              $('#loadingalert').hide();
                $('#requestmessage').text(data.msg);


              if( data.status == false ){
                  toggleAlert();
              } else {
                  inProgressModal();
              }



                if( data.status == true ){
                    loadProgressInventory();
                  }
               //$('#variationData').html( data );
            }

        });

      });

        $("#btnNoConfirmYesNo").off('click').click(function () {
          $confirm.modal("hide");
      });

});

</script>
