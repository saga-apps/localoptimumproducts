@section('title')
@parent
<title>Button Settings</title>
@stop


<div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
 
    
  <div class="card mt-3">
    <div class="card-header">
     Button link to your Amazon product page
     <a target="_blank" class="float-right" href="https://connectr.freshdesk.com/support/solutions/articles/44001035963-redirect-button-to-send-users-to-your-amazon-listing">
            More info&nbsp;
            <span class="fa fa-caret-right"></span>
        </a>
    </div>


    <div class="card-body">
      <form onsubmit="event.preventDefault();  return saveRedirecSetting('#form-redirect-settings','{{asset('settings-redirect')}}');" id="form-redirect-settings" name="form-redirect-settings">
                {{csrf_field() }}
       <div class="form-group row">
        <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Button Link</label>
        <div class="col-12 col-sm-8 col-md-6 col-lg-6">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-outline-primary {{ isset($useroptions['isredirect']) && $useroptions['isredirect'] == 1 ? 'active' : ''  }}">
            <input type="radio" name="isredirect" value="1" id="issyncinventory" autocomplete="off" {{ isset($useroptions['isredirect']) && $useroptions['isredirect'] == 1 ? 'checked' : ''  }}> SHOW
          </label>
          <label class="btn btn-outline-primary {{ isset($useroptions['isredirect']) && $useroptions['isredirect'] == 1 ? '' : 'active'  }}">
            <input type="radio" name="isredirect" value="0" id="issyncinventory" autocomplete="off" {{ isset($useroptions['isredirect']) && $useroptions['isredirect'] == 1 ? '' : 'checked'  }}> HIDE
          </label>
        </div>
      </div>
      </div>




     <div class="form-group row">
      <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Button Text</label>
      <div class="col-12 col-sm-8 col-md-6 col-lg-6">
      <input id="buttontext" for="buttontext" type="text" name="buttontext" maxlength="30" value="{{$useroptions['buttontext'] or 'View On Amazon'}}" class="form-control" placeholder="Buy on Amazon" required="true"></input>                                   
      </div>
  	</div>

   

  	<div class="text-right">
    <button type="submit" class="btn btn-primary text-left " >Save</button>
  </div>
</form>
    </div>
  </div>
</div>
 

<script type="text/javascript">
   function saveRedirecSetting( formId , route ) {

          $('#loadingalert').show();
          $.ajax({
                method  :'POST',
                url     :route,
                data    : $(formId).serialize(),
                success: function( response ){
                  $('#loadingalert').hide();
                  $('#requestmessage').text(response.msg);
                    toggleAlert();
                }

            });
        }

</script>