@section('title')
@parent
<title>Sync Settings</title>
@stop

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

<div class="col-12 col-sm-10 col-md-8 col-lg-7">

  <div class="card mt-3">
          <div class="card-header">
            Sync settings
            
            <a target="_blank" class="float-right" href="https://connectr.freshdesk.com/support/solutions/articles/44000984541-sync">
            More info&nbsp;
            <span class="fa fa-caret-right"></span>
        </a>
         </div>
         <div class="card-body">
        <form onsubmit="event.preventDefault();  return saveSyncSetting('#form-sync-settings','{{asset('settings-sync')}}');" id="form-sync-settings" name="form-sync-settings">
                {{csrf_field() }}
          <div class="form-group row">
            <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Sync Price</label>
            <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-outline-primary {{ $usersettings->issyncprice == 1 ? 'active':''  }} ">
                <input type="radio" name="issyncprice" value="1" id="issyncprice" autocomplete="off" {{ $usersettings->issyncprice == 1 ? 'checked':''  }}> ON
              </label>
              <label class="btn btn-outline-primary {{ $usersettings->issyncprice == 0 ? 'active':''  }}">
                <input type="radio" name="issyncprice" value="0" id="issyncprice" autocomplete="off" {{ $usersettings->issyncprice == 0 ? 'checked':''  }}> OFF
              </label>
            </div>
          </div>
         </div>
          <div class="form-group row">
            <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Sync Inventory</label>
            <div class="col-12 col-sm-8 col-md-6 col-lg-6">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-outline-primary {{ $usersettings->issyncinventory == 1 ? 'active':''  }}">
                <input type="radio" name="issyncinventory" value="1" id="issyncinventory" autocomplete="off" {{ $usersettings->issyncinventory == 1 ? 'checked':''  }}> ON
              </label>
              <label class="btn btn-outline-primary {{ $usersettings->issyncinventory == 0 ? 'active':''  }}">
                <input type="radio" name="issyncinventory" value="0" id="issyncinventory" autocomplete="off" {{ $usersettings->issyncinventory == 0 ? 'checked':''  }}> OFF
              </label>
            </div>
          </div>
          </div>
                <hr>
          <div class="form-group row">
              <label for="staticEmail" class="col-12 col-sm-4 col-md-4 col-form-label">Auto-sync
               <div class="small">
                <a class="text-primary" target="_blank" href="https://connectr.freshdesk.com/support/solutions/articles/44000918310-auto-sync-coming-soon-">
                    Know more <span class="fa fa-caret-right"></span>
                  </a>
            </div>
              </label>
             
              <div class="col-12 col-sm-8 col-md-6 col-lg-6">
              <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-outline-primary {{ $usersettings->issync == 1 ? 'active':''  }}">
                  <input type="radio" name="issync" value="1" id="issync" autocomplete="off" {{ $usersettings->issync == 1 ? 'checked':''  }}> ON
                </label>
                <label class="btn btn-outline-primary {{ $usersettings->issync == 0 ? 'active':''  }}">
                  <input type="radio" name="issync" value="0" id="issync" autocomplete="off" {{ $usersettings->issync == 0 ? 'checked':''  }}> OFF
                </label>
              </div>
            </div>
        </div>
          {{-- <hr class="mt-5 mb-5">

          Timezone {{$timezones[Auth::User()->timezone]}}
          <div class="form-group row">
              <label for="staticEmail" class="col-4 col-sm-6 col-form-label font-weight-bold">Sync Time</label>
            <div class="input-group date" id="autosynctime" data-target-input="nearest">
                <input name="autosynctime" type="text" class="form-control datetimepicker-input" data-target="#autosynctime"/>
                <div class="input-group-append" data-target="#autosynctime" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                </div>
            </div>
          </div> --}}


          {{-- <div class="form-group row">
            <label for="staticEmail" class="col-12 col-sm-6 col-md-4 col-form-label font-weight-bold">Product is Unavailable</label>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
              <select class="form-control" name="issyncproduct" id="exampleFormControlSelect1">
                <option {{ $usersettings->issyncproduct == 0 ? 'selected':''  }} value="0">Take no Action</option>
                <option {{ $usersettings->issyncproduct == 1 ? 'selected':''  }} value="1">Make Inventory Zero</option>
                <option {{ $usersettings->issyncproduct == 2 ? 'selected':''  }} value="2">Hide product from Shopify</option>
                <option {{ $usersettings->issyncproduct == 3 ? 'selected':''  }} value="3">Delete product from Shopify</option>
              </select>
            </div>
          </div> --}}

          <div class="text-right">
            <button type="submit" class="btn btn-primary text-left ">Save</button>
          </div>
        </div>
        </form>
      </div>

 <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

        function saveSyncSetting( formId , route ) {

          $('#loadingalert').show();
          $.ajax({
                method  :'POST',
                url     :route,
                data    : $(formId).serialize(),
                success: function( response ){
                  $('#loadingalert').hide();
                  $('#requestmessage').text(response.msg);
                    toggleAlert();
                }

            });
        }


        $(function () {
                $('#autosynctime').datetimepicker({
                    @if($usersettings->autosynctime != "")
                    defaultDate: moment({
                      hour: {{$hour}},
                      minute:{{$minute}}
                    }),
                    @endif
                    format: 'HH:mm'
                });
            });



    //         $(function () {
    //             $('#autosynctime').datetimepicker({
    //               defaultDate: moment({
    //   hour: 16,
    //   minute:32
    // }),
    //                 format: 'HH:mm'
    //             });
    //         });
    </script>
