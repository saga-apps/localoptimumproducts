<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      @yield('title')
      @yield('description')
      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
      <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
      
      <script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.0.3/dist/index.min.js"></script>

      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
      <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_blue.css">
      <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

      <style type="text/css">
        .greensignal{
          color:green;
        }
        .redsignal{
          color:red;
        }
        .label-info {
          background-color: #ffc107;
        }
        .label {
            display: inline;
            padding: .2em .6em .3em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }
        pickr .pcr-button:after, .pickr .pcr-button:before {
            border-radius: 0px;
            border: 1px solid #6c757d;
            border-left: 0;
         }
      </style>
     
      @yield('js')
    </head>
    <body>
    <nav class="navbar navbar-expand-sm navbar navbar-dark bg-dark mb-3">

      <div class="container">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item mr-3 {{$activeMenu =='dashboard' || $activeMenu=='createbadges' || Request::is('editbadge*') || Request::is('createbadges') ? 'active':'' }}">
            <a class="nav-link pl-2 pr-2" href="{{asset('dashboard')}}"><span class="fa fa-tags mr-2"></span>Badges <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mr-3 {{$activeMenu =='highlights' || $activeMenu =='createhighlights' || Request::is('edithighlights*') || Request::is('createhighlights') ? 'active':'' }}">
            <a class="nav-link pl-2 pr-2" href="{{asset('highlights')}}"><span class="fa fa-star mr-2"></span>Highlights <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mr-3 {{$activeMenu =='notes' || $activeMenu=='createnotes' || Request::is('editnote*') || Request::is('createnotes') ? 'active':'' }}">
            <a class="nav-link pl-2 pr-2" href="{{asset('note')}}"><span class="fa fa-bars mr-2"></span>Notes <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mr-3 {{$activeMenu =='banner' || $activeMenu=='createbanners' || Request::is('editbanner*') || Request::is('createbanners') ? 'active':'' }}">
            <a class="nav-link pl-2 pr-2" href="{{asset('banner')}}"><span class="fa fa-flag mr-2"></span>Banners <span class="sr-only">(current)</span><sup class="text-danger d-none" style="font-weight: 900">new</sup></a>
            </li>
            <li class="nav-item mr-3 ">
            <a class="nav-link pl-2 pr-2 border" href="https://prime.kampsite.co" target="_blank"><span class="fa fa-comments mr-2"></span>Suggestions <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item mr-3 {{$activeMenu =='account' || Request::is('account')  ? 'active':'' }}">
              <ul class="navbar-nav mr-autoR pl-2 pr-2 pt-0 headway">
                <span class="nav-item dropdown {{$activeMenu=='account' ? 'active':''}}">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-user"></span> &nbsp;<span id="menuuserName">{{Auth::
                        user()->name}}</span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ asset('account') }}">Account</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" target="_blank"  href="https://getinstashop.freshdesk.com/support/solutions/81000120286">Help</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ asset('logout') }}">Logout</a>
                  </div>
                </span>
              </ul>
            </li> 

            <!-- <li class="nav-item mr-3 {{$activeMenu =='showcase' ? 'active':'' }}">
            <a class="nav-link pl-2 pr-2" href="{{asset('showcasebadge')}}"><span class="fa fa-star mr-2"></span>Showcases <span class="sr-only">(current)</span></a>
            </li> -->

            <!-- <li class="nav-item mr-3 {{$activeMenu =='account' ? 'active':'' }}">
            <a class="nav-link pl-2 pr-2" href="{{asset('account')}}"><span class="fa fa-user mr-2"></span>Account</a>
            </li>
            <li class="nav-item mr-3" >
            <a class="nav-link pl-2 pr-2" href="{{asset('help')}}" target="_blank"><span class="fa fa-life-ring mr-2"></span>Help</a>
            </li>
                <li class="nav-item mr-3">
            <a class="nav-link pl-2 pr-2" href="{{asset('logout')}}"><span class="fa fa-sign-out-alt mr-2"></span>Logout</a>
            </li>-->
          </ul>

          <!-- <ul class="navbar-nav mr-autoR">
            <span class="nav-item dropdown {{$activeMenu=='account' ? 'active':''}}">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-user"></span> &nbsp;<span id="menuuserName">{{Auth::
                    user()->name}}</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ asset('account') }}">Account</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" target="_blank"  href="https://getinstashop.freshdesk.com/support/solutions/81000120286">Help</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ asset('logout') }}">Logout</a>
              </div>
            </span>
          </ul> -->
        </div>
      </div>
    </nav>

      <div class="container">
        @yield('content')
      </div>
      <div class="text-center fixed-bottom p-1 m-auto text-center p3 col-12 col-md-8 col-md-offset-2 collapse" id="requestalert" style="bottom:20px;" >
        <div class="alert bg-dark">
          <h5 class="text-white" id="requestmessage"></h5>
        </div>
      </div>
      <div id="loadingalert" style="top:0; left:0; height: 100vh; width: 100vw; z-index: 999999999999;opacity: 0.8;" class="position-fixed navbar collapse">
        <div class="alert bg-dark m-auto text-center m-auto" style=" width:200px; z-index: 999999999999; top:20%;">
          <h5 class="text-white mb-0">
            <span class="fa fa-sync-alt fa-2x fa-spin"></span>
          </h5>
        </div>
      </div>

     
      <br><br><br><br>
      <script type="text/javascript">
        function toggleAlert(){
          $( "#requestalert" ).show(function() {
            console.log("show")
            setTimeout(function() {
              $("#requestalert").hide(1000);
            }, 2000);
          });
        }
        function inProgressModal() {
          var $inProgressModal = $("#inProgressModal");
          $inProgressModal.modal('show');
        }
      </script>
      <script>
        window.fwSettings={
        'widget_id':81000001484
        };
        !function(){if("function"!=typeof window.FreshworksWidget){var n=function(){n.q.push(arguments)};n.q=[],window.FreshworksWidget=n}}() 
      </script>
      <script type='text/javascript' src='https://ind-widget.freshworks.com/widgets/81000001484.js' async defer></script>
      
      <script>
  // @see https://docs.headwayapp.co/widget for more configuration options.
  var HW_config = {
    selector: ".headway", // CSS selector where to inject the badge
    account:  "7QbX5J"
  }
    </script>
<script async src="https://cdn.headwayapp.co/widget.js"></script>
<style>
    #HW_badge_cont{margin-top: .25rem;}
</style>
      
      @yield('scripts')
    </body>
  </html>
