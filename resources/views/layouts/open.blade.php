<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      @yield('title')
      @yield('description')
      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
      <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
      <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_blue.css">
      <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

      <!-- Global site tag (gtag.js) - Google Analytics -->

      @yield('js')
    </head>
    <body>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div class="container">
             <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <span class="nav-link pl-2 pr-2">Prime Product Badges</span>
              </li>

            </ul>
        </div>
      </nav>
      <div class="container">
        @yield('content')
      </div>
     
      <br><br><br><br>

      @yield('scripts')
    </body>
  </html>
