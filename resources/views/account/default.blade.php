@extends('layouts.master')



@section('description')
@parent
<meta content="" name="description" />
@stop

@section('css')
@parent

@stop

@section('js')

@parent

@stop
 
@section('content')
@parent


<div class="row ">

    <!--/.col-xs-12.col-sm-9-->

    {{-- @include('account.account-menu') --}}

    <div id="sidebar" class="col-xs-12 col-sm-2">
        <div class="list-group mt-3">
            <a class="list-group-item {{ Request::is('account')?'active':'' }}" href="{{asset('account')}}">Account</a>
            <a class="list-group-item {{ Request::is('plans')?'active':'' }}" href="{{asset('plans')}}">Plans</a>

        </div>
    </div>

    @if($subpage == 'account')
    @include('account.account')
    @elseif($subpage == 'plans')
    @include('account.plans')
    @endif


</div>

@stop

@section('scripts')
@parent

<script type="text/javascript">


    function saveAccount( fromId , Url ){

        var fromData = $(fromId).serializeArray();
        $('#loadingalert').show();
        
        // console.log( fromData );
        $.ajax({
                method:"POST",
                url : Url,
                data : fromData,
                
                success : function( data ){
                    $('#loadingalert').hide();
                    $('#requestmessage').text(data.msg);
                    toggleAlert();
                }

            });

    }

    // function saveusertheme( userid , Url ){

    //     var themeid = $('#usertheme').val();
        
    //     $.ajax( {
    //         url : Url,
    //         method:'POST',
    //         data :{'userid':userid,'themeid':themeid,'_token':'{{csrf_token()}}'},
    //         success:function( response ){
    //             $('#requestalert').show();
    //             $('#requestmessage').text(response.msg);
    //             $("#requestalert").delay(3000).hide(0);
    //             location.reload(true);
    //         }
    //     }); 

    // }


</script>

@stop
