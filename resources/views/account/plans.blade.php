


<div class="col-12 col-sm-10 col-md-3">
    <div class="card text-center mt-3" >
        <div class="card-header "><div class="h4">Basic Plan</div></div>
            <div class="card-body">
                <div class="h2">$5</div>
                <p class="text-center text-muted">per month</p>
                <p class="card-text">
                Badges
                </p>
                 <p class="card-text">
                     <s>Highlights</s>
                </p>
                  <p class="card-text">
                     <s>Country Restriction</s>
                </p>
                <p class="card-text">
                     <s>Language Restriction</s>
                </p>
               <p class="card-text">
                     <s>Order Count</s>
                </p>
                <p class="card-text">
                     <s>Recent Sale Count</s>
                </p>
                <p class="card-text">
                     <s>Dynamic Metafields</s>
                </p>
                <p class="card-text">
                     <s>Badge Group</s>
                </p>
                <p class="card-text">
                    <s>Countdown Timer</s>
                </p>
                <p class="card-text">
                    <s>Notes</s>
                </p>
                <p class="card-text">
                    <s>Banners</s>
                </p>
                <p class="card-text">
                    <s>LiveVisitorsCounter</s>
                </p>
            </div>
        <div class="card-footer">
                  @if(Auth::user()->planid != 2)
                    <a href="javascript:void(0)" class="btn btn-primary" data-success="/plans/success"  onclick = "changePlan(2);" data-success="/plans/success" data-passthrough="{{ Auth::id() }}" data-theme="none" >Select</a>
                @endif
                 @if(Auth::user()->planid == 2)
                    <span class="btn text-success">Current Plan</span>
                    {{-- <sup><span class="label bg-black " style="background: black;padding: 2%;">current</span></sup> --}}
                @endif
        </div>
    </div>
</div>

<div class="col-12 col-sm-10 col-md-3">
    <div class="card text-center mt-3" >
        <div class="card-header"><div class="h4">Pro Plan</div></div>
            <div class="card-body">
               
                <div class="h2">$10</div>
                <p class="text-center text-muted">per month</p>
               <p class="card-text">
                Badges
                </p>
                 <p class="card-text">
                     Highlights
                </p>
                  <p class="card-text">
                     Country Restriction
                </p>
                <p class="card-text">
                     Language Restriction
                </p>
                <p class="card-text">
                     Order Count
                </p>
                <p class="card-text">
                    Recent Sale Count
                </p>
                <p class="card-text">
                     Dynamic Metafields
                </p>
                <p class="card-text">
                    Badge Group
                </p>
                <p class="card-text">
                    Countdown Timer
                </p>
                <p class="card-text">
                    Notes
                </p>
                <p class="card-text">
                    Banners
                </p>
                <p class="card-text">
                    <s>LiveVisitorsCounter</s>
                </p>
            </div>
                <div class="card-footer">
                      @if(Auth::user()->planid != 3)
                    <a href="javascript:void(0)" class="btn btn-warning text-white" data-success="/plans/success"  onclick = "changePlan(3);" data-success="/plans/success" data-passthrough="{{ Auth::id() }}" data-theme="none" >Select</a>
                @endif
                 @if(Auth::user()->planid == 3)
                 <span class="btn text-success">Current Plan</span>
                    {{-- <sup><span class="label bg-black " style="background: black;padding: 2%;">current</span></sup> --}}
                @endif
               </div>
    </div>
</div>


<div class="col-12 col-sm-10 col-md-3">
    <div class="card text-center mt-3" >
        <div class="card-header"><div class="h4">Gold Plan</div></div>
        <div class="card-body">
            
            <div class="h2">$20</div>
            <p class="text-center text-muted">per month</p>
            <p class="card-text">
            Badges
            </p>
                <p class="card-text">
                    Highlights
            </p>
                <p class="card-text">
                    Country Restriction
            </p>
            <p class="card-text">
                    Language Restriction
            </p>
            <p class="card-text">
                    Order Count
            </p>
            <p class="card-text">
                Recent Sale Count
            </p>
            <p class="card-text">
                    Dynamic Metafields
            </p>
            <p class="card-text">
                Badge Group
            </p>
            <p class="card-text">
                Countdown Timer
            </p>
            <p class="card-text">
                Notes
            </p>
            <p class="card-text">
                Banners
            </p>
            <p class="card-text">
                LiveVisitorsCounter
            </p>
        </div>
        <div class="card-footer">
                @if(Auth::user()->planid != 4)
            <a href="javascript:void(0)" class="btn btn-warning text-white" data-success="/plans/success"  onclick = "changePlan(4);" data-success="/plans/success" data-passthrough="{{ Auth::id() }}" data-theme="none" >Select</a>
        @endif
            @if(Auth::user()->planid == 4)
            <span class="btn text-success">Current Plan</span>
            {{-- <sup><span class="label bg-black " style="background: black;padding: 2%;">current</span></sup> --}}
        @endif
        </div>
    </div>
</div>

    
<script>

  function changePlan(planId){

           $.ajax({
                method: "POST",
                url: "{{asset("updatecharge")}}",
                data: "&_token={{csrf_token()}}&plan_id="+planId,
            }) .done(function( data ) {
                console.log(data);

                window.location.href = data;
            });

        // });

        //  $("#btnNoConfirmYesNo").off('click').click(function () {
        //     $confirm.modal("hide");
        // });


    }

</script>