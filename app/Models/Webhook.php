<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use DB;

class Webhook extends Model
{
    public   $timestamps = false;
	protected $primaryKey = 'id';
    
    protected $fillable = ['id', 'userid','shop','webhookid','topic','time'];

}
