<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usersetting extends Model
{
    public   $timestamps = false;

    protected $fillable = ['userid','themeid','isshowonstore','locationid','basecurrency','isautoamazonvendor','vendor','location','tags','isautoproducttype','producttype','quantity','timezone','listingstatus','fulfillmenttype','issyncprice','issyncinventory','issyncfulfillment','issyncproduct','issync','autosynctime','templatesuffix','inventorytype','duplicatepreference','barcode','awsinventorypolicy'];
}

