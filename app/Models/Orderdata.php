<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orderdata extends Model
{
  public $timestamps  = false;
  protected $primaryKey = 'id';
  protected $fillable = [
      'userid', 'jsondata', 'topic', 'orderid', 'isread'
  ];
}
