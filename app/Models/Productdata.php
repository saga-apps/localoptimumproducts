<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productdata extends Model
{
    public $timestamps  = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'userid', 'orderid', 'name', 'quantity', 'productid', 'variantid'
    ];
}
