<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Errorlog extends Model
{
    public   $timestamps = false;
    protected $fillable = ['taskid', 'productid','error','method'];

}
