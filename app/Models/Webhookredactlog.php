<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use DB;

class Webhookredactlog extends Model
{
    public   $timestamps = false;
	protected $primaryKey = 'id';
    
    protected $fillable = ['id', 'shop','topic','data','createdat'];

}
