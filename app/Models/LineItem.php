<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    public $timestamps  = false;
    protected $table = 'lineitems';

    protected $fillable = [
        'userid','orderid','productid','variantid','quantity','orderdate',
    ];
}
