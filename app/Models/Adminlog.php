<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adminlog extends Model
{
    public   $timestamps = false;
    protected $fillable = ['userid', 'adminid', 'file', 'shop', 'oldchanges', 'newchanges', 'created_at'];

}
