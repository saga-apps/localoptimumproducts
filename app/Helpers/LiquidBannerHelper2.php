<?php

namespace App\Helpers;

use Log;

class LiquidBannerHelper2 {

	public static function getLiquidProductCondition( $displayrules , $condition ,$appearance ,  $banner, $display, $excludeproduct, $bannerstyle, $CountryClass, $customer_location, $displayprorules, $countdowntitle, $countdownsubtitle, $shop_language, $LangClass ){
		$arrIfConditon = array();

		$bannerjson = json_decode($banner['bannerstyle']);
		$animationstyle='';
		
		if(isset($bannerjson->animation)){
			$animationstyle .='
				:root {
					--animate-duration: 1s;
					--animate-delay: 1s;
					--animate-repeat: 1;
				  }
				  .animate__animated {
					-webkit-animation-duration: 1s;
					animation-duration: 1s;
					-webkit-animation-duration: var(--animate-duration);
					animation-duration: var(--animate-duration);
					-webkit-animation-fill-mode: both;
					animation-fill-mode: both;
				  }

				  @-webkit-keyframes primeslide {
					from {
					  -webkit-transform: translate3d(0, -100%, 0);
					  transform: translate3d(0, -100%, 0);
					  visibility: visible;
					}
				  
					to {
					  -webkit-transform: translate3d(0, 0, 0);
					  transform: translate3d(0, 0, 0);
					}
				  }
				  @keyframes primeslide {
					from {
					  -webkit-transform: translate3d(0, -100%, 0) !important;
					  transform: translate3d(0, -100%, 0);
					  visibility: visible;
					}
				  
					to {
					  -webkit-transform: translate3d(0, 0, 0) !important;
					  transform: translate3d(0, 0, 0);
					}
				  }
				  .prime-slide {
					
					 animation-name: primeslide !important; 
				  }

				  @-webkit-keyframes primepop {
					from {
					  opacity: 0;
					  -webkit-transform: scale3d(0.3, 0.3, 0.3);
					  transform: scale3d(0.3, 0.3, 0.3);
					}
				  
					50% {
					  opacity: 1;
					}
				  }
				  @keyframes primepop {
					from {
					  opacity: 0;
					  -webkit-transform: scale3d(0.3, 0.3, 0.3);
					  transform: scale3d(0.3, 0.3, 0.3);
					}
				  
					50% {
					  opacity: 1;
					}
				  }
				  .prime-pop {
					-webkit-animation-name: primepop;
					animation-name: primepop;
				  }

				  @-webkit-keyframes groove {
					from {
					  opacity: 0;
					  -webkit-transform: scale(0.1) rotate(30deg);
					  transform: scale(0.1) rotate(30deg);
					  -webkit-transform-origin: center bottom;
					  transform-origin: center bottom;
					}
				  
					50% {
					  -webkit-transform: rotate(-10deg);
					  transform: rotate(-10deg);
					}
				  
					70% {
					  -webkit-transform: rotate(3deg);
					  transform: rotate(3deg);
					}
				  
					to {
					  opacity: 1;
					  -webkit-transform: scale(1);
					  transform: scale(1);
					}
				  }
				  @keyframes groove {
					from {
					  opacity: 0;
					  -webkit-transform: scale(0.1) rotate(30deg);
					  transform: scale(0.1) rotate(30deg);
					  -webkit-transform-origin: center bottom;
					  transform-origin: center bottom;
					}
				  
					50% {
					  -webkit-transform: rotate(-10deg);
					  transform: rotate(-10deg);
					}
				  
					70% {
					  -webkit-transform: rotate(3deg);
					  transform: rotate(3deg);
					}
				  
					to {
					  opacity: 1;
					  -webkit-transform: scale(1);
					  transform: scale(1);
					}
				  }
				  .prime-groove {
					-webkit-animation-name: groove;
					animation-name: groove;
				  }

				  @-webkit-keyframes fly {
					0% {
					  -webkit-transform: translateY(-1200px) scale(0.7);
					  transform: translateY(-1200px) scale(0.7);
					  opacity: 0.7;
					}
				  
					80% {
					  -webkit-transform: translateY(0px) scale(0.7);
					  transform: translateY(0px) scale(0.7);
					  opacity: 0.7;
					}
				  
					100% {
					  -webkit-transform: scale(1);
					  transform: scale(1);
					  opacity: 1;
					}
				  }
				  @keyframes fly {
					0% {
					  -webkit-transform: translateY(-1200px) scale(0.7);
					  transform: translateY(-1200px) scale(0.7);
					  opacity: 0.7;
					}
				  
					80% {
					  -webkit-transform: translateY(0px) scale(0.7);
					  transform: translateY(0px) scale(0.7);
					  opacity: 0.7;
					}
				  
					100% {
					  -webkit-transform: scale(1);
					  transform: scale(1);
					  opacity: 1;
					}
				  }
				  .prime-fly {
					-webkit-animation-name: fly;
					animation-name: fly;
				  }
				  
				  @-webkit-keyframes swerve {
					20% {
					  -webkit-transform: rotate3d(0, 0, 1, 15deg);
					  transform: rotate3d(0, 0, 1, 15deg);
					}
				  
					40% {
					  -webkit-transform: rotate3d(0, 0, 1, -10deg);
					  transform: rotate3d(0, 0, 1, -10deg);
					}
				  
					60% {
					  -webkit-transform: rotate3d(0, 0, 1, 5deg);
					  transform: rotate3d(0, 0, 1, 5deg);
					}
				  
					80% {
					  -webkit-transform: rotate3d(0, 0, 1, -5deg);
					  transform: rotate3d(0, 0, 1, -5deg);
					}
				  
					to {
					  -webkit-transform: rotate3d(0, 0, 1, 0deg);
					  transform: rotate3d(0, 0, 1, 0deg);
					}
				  }
				  @keyframes swerve {
					20% {
					  -webkit-transform: rotate3d(0, 0, 1, 15deg);
					  transform: rotate3d(0, 0, 1, 15deg);
					}
				  
					40% {
					  -webkit-transform: rotate3d(0, 0, 1, -10deg);
					  transform: rotate3d(0, 0, 1, -10deg);
					}
				  
					60% {
					  -webkit-transform: rotate3d(0, 0, 1, 5deg);
					  transform: rotate3d(0, 0, 1, 5deg);
					}
				  
					80% {
					  -webkit-transform: rotate3d(0, 0, 1, -5deg);
					  transform: rotate3d(0, 0, 1, -5deg);
					}
				  
					to {
					  -webkit-transform: rotate3d(0, 0, 1, 0deg);
					  transform: rotate3d(0, 0, 1, 0deg);
					}
				  }
				  .prime-swerve{
					-webkit-transform-origin: top center;
					transform-origin: top center;
					-webkit-animation-name: swerve;
					animation-name: swerve;
				  }
				 
				  
				  ';
			
			$animationType = $bannerjson->animation;
			

		} else {

			$animationType ='';
		}

		$strAssignGlobal     = '
		{% assign primebanMobileSize = primebanMobileSize | default: 100 %}
			{% assign primebanTabletSize = primebanTabletSize | default: 100 %}
			{% assign primebanProduct = product %}
			{% assign primebanCustId = customer.id %}
			{% assign primebanVarient = product.variants.size %}
			{%- assign primebanmoneyformat = shop.money_format | split: "{{" | last | remove: "}}" | strip | strip_html -%}
			{% if primebanmoneyformat == "amount_with_comma_separator" or primebanmoneyformat == "amount_no_decimals_with_comma_separator" %}
				{% assign primebanProdPrice = product.price | money_without_currency | remove: "." | replace: ",", "." %}
			{% else %} 
				{% assign primebanProdPrice = product.price | money_without_currency | remove: "," %}
			{% endif %}
			{% assign primebanProdPrice = product.price | divided_by: 100.00 %}
			{% assign primebanCustomerTag = customer.tags | downcase %}
			{% assign primebanPublishedDate = product.published_at | date: "%s" %}
			{% assign primebanPublishDateDiff = "now" | date: "%s" | minus: primebanPublishedDate %}
			{% assign primebanTemplate = template | downcase %}
			{% assign primebanLanguage = request.locale.iso_code %}
			{% assign primemetafield = primebanProduct.metafields %}
			{% assign primebanProductTitle = primebanProduct.title | downcase %}
			{% assign primebanProductType = primebanProduct.type | downcase %}
			{% assign primebanProductVendor = primebanProduct.vendor | downcase %}
			{% assign primebanShopifyTags = primebanProduct.tags | downcase %}
			{% assign primebanProductWeight = product.variants.first.weight %}
			{% assign primebanUTC = "now" | date: "%s" %}
			{% assign primebanstartTime = "'.$banner['starttime'].'"  %}
			{% assign primebanEndTime = "'.$banner['endtime'].'"  %}
			{% assign primebanMaxDisplayBanner = primebanMaxDisplayBanner | default: 10 %}	
			{% assign primebanCreatedDateSec = primebanProduct.created_at | date: "%s" %}
			{% assign primebanDateDiff = "now" | date: "%s" | minus: primebanCreatedDateSec %}
			{% assign primebanCollectionIds = primebanProduct.collections | map: "id" | default: 0 %}			
			{% assign primebanBannerCounter = 1 %}
			{% assign VariantCount = product.variants.size %}
			{% assign SaleAmount    = 0 %}	
			{% assign SalePercent   = 0 %}
			{% assign ProductSKU = product.selected_or_first_available_variant.sku | default: 0 %}
			{% assign primebannerLinks = primebanLinks | default: 0 %}
			{% assign primeGroup = primebanGroup | default: "1" %}
			{% if primebanProduct.price and primebanProduct.compare_at_price %}
				{% assign SaleAmount = primebanProduct.compare_at_price | minus: primebanProduct.price %}	
				{% if SaleAmount > 0 %}
					{% assign SalePercent = SaleAmount | times: 100.0 | divided_by: primebanProduct.compare_at_price %}
				{% endif %}
        	{% endif %}
        	{% assign Inventory = 0 %}
			{% assign Inventory2 = 0 %}
        	{% for productVariant in primebanProduct.variants %}
				{% assign Inventory2 = productVariant.inventory_quantity | at_least: 0 %}
				{% assign Inventory = Inventory | plus: Inventory2 %}			
			{% endfor %}
            ' ;

		$strIfConditionInventory = '';
		$strIfConditionSalePercentValue = '';
		$strIfProductCondition   = '';
		$strIfCollectionCondition   = '';
		$arrPrepareifCondition    = [];

		$strIfConditionTags       = '';
		$strIfCustConditionTags       = '';
		$strIfConditionmetafield='';
		$strIfConditionvendor='';
		$strIfConditiontype='';
		$strIfConditionprice='';
		$strIfPageType = '';
		$strIfLangType = '';
		
	
		$arrProductId = array_column( $displayrules, 'product' );

		$strProductId = implode(', ', array_map(function ($arrProductId) {

		if( empty( $arrProductId ) ) {
			return '';
		} 

			return  $arrProductId[0] ;

		}, $arrProductId ) );

		
		// prepare Productid condition;
		if( !empty( $strProductId ) ){

			$strIfProductCondition = '{% assign primebanProductIds = "'.$strProductId.'" | split: "," %}
						{% assign isprimebanProductFound = false %}
						{% assign primebanProductId = primebanProduct.id | downcase %}
                        {% if primebanProductIds contains primebanProductId %}
                            {% assign isprimebanProductFound = true %}
                        {% endif %}';

			$arrPrepareifCondition[] = ' true == isprimebanProductFound ';
		}

		$arrCollectionId = array_column( $displayrules, 'collection' );
		$collectioncount=0;
		foreach($arrCollectionId as $collectionkey){
			if(!empty( $collectionkey ) && isset($collectionkey['type'])){
				$strCollectionId = $collectionkey['value'];
				if( !empty( $strCollectionId ) ){
					$strIfCollectionCondition  .= '{% assign primebanProductCollections = "'.$strCollectionId.'" | split: "," %}';
					if($collectionkey['type'] == '1'){
						$strIfCollectionCondition  .= '{% assign isprimebanProductCollectionFound'.$collectioncount.' = false %}
						{% for collectionid in primebanProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}
							{% for shopifyrealcollection in primebanCollectionIds %}
								{% if shopifyrealcollection == collectionid2 %}
									{% assign isprimebanProductCollectionFound'.$collectioncount.' = true %}
									{% break %}
								{% endif %}
							{% endfor %}
						{% endfor %}';
					}else{
						$strIfCollectionCondition  .= '{% assign break_loop = false %}{% assign isprimebanProductCollectionFound'.$collectioncount.' = false %}
						{% if primebanCollectionIds == 0 %}
							{% assign isprimebanProductCollectionFound'.$collectioncount.' = true %}
						{% endif %}
						{% for collectionid in primebanProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}
							{% for shopifyrealcollection in primebanCollectionIds %}				
								{% if shopifyrealcollection != collectionid2 %}
									{% assign isprimebanProductCollectionFound'.$collectioncount.' = true %}
								{% else %}
									{% assign isprimebanProductCollectionFound'.$collectioncount.' = false %}
									{% assign break_loop = true %}
									{% break %}
							  	{% endif %}
							{% endfor %}
							{% if break_loop %}
								{% break %}
							{% endif %}
						{% endfor %}';
					}
					$arrPrepareifCondition[] = ' true == isprimebanProductCollectionFound'.$collectioncount.' ';
				}	
			}else{
				if( !empty( $collectionkey ) ){
					$strCollectionId = $arrCollectionId[0][0];
				}
				if( !empty( $strCollectionId ) ){
					$strIfCollectionCondition  .= '{% assign primebanProductCollections = "'.$strCollectionId.'" | split: "," %}
						{% assign isprimebanProductCollectionFound'.$collectioncount.' = false %}
						{% for collectionid in primebanProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}			
							{% if primebanCollectionIds contains collectionid2 %}
								{% assign isprimebanProductCollectionFound'.$collectioncount.' = true %}
							{% break %}
							  {% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebanProductCollectionFound'.$collectioncount.'';
				}		
			}
			$collectioncount++;
		}

		$arrproductmetafield = array_column( $displayrules, 'productmetafield' );
		if( !(empty($arrproductmetafield)) ){


			for($i=0; $i< count($arrproductmetafield); $i++) {
				
				$strIfConditionmetafield .='{% assign newstringmetaval'.$i.' = "'.$arrproductmetafield[$i]['metaval'].'" | downcase %} {% assign metastring'.$i.' = primebanProduct.metafields.'.$arrproductmetafield[$i]['metaname'].' | downcase %}';
			}
		}

		$arrproductproducttype = array_column( $displayrules, 'title' );
		if( !(empty($arrproductproducttype)) ){

			for($i=0; $i< count($arrproductproducttype); $i++) {

				$strIfConditiontype .='{% assign newstringproducttitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | downcase %} ';

				if($arrproductproducttype[$i]['type'] == '1'){

					$strIfConditiontype .='{% assign valuesizeproductitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %} 
					{% assign newnumberproducttitle'.$i.' = primebanProductTitle | slice: 0,valuesizeproductitle'.$i.' %}';

				} else if ($arrproductproducttype[$i]['type'] == '2'){

					$strIfConditiontype .='{% assign valuesizeproducttitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %}{% assign mulproducttitle'.$i.' = valuesizeproducttitle'.$i.' | times:2 %}
					{% assign newminusproducttitle'.$i.' = valuesizeproducttitle'.$i.' | minus:mulproducttitle'.$i.' %}
				{% assign newnumberproducttitle'.$i.' = primebanProductTitle | slice: newminusproducttitle'.$i.', valuesizeproducttitle'.$i.' %}';
				
				} else if ($arrproductproducttype[$i]['type'] == '4'){

					$strIfConditiontype .='{% unless primebanProductTitle contains newstringproducttitle'.$i.' %}{% assign valueproducttitle'.$i.' = true %}{% endunless %}';
				}
			
			}

		}

		$arrproductproductprice = array_column( $displayrules, 'product_price' );
	 		
		if( !(empty($arrproductproductprice)) ){

			for($i=0; $i< count($arrproductproductprice); $i++) {

				$strIfConditionprice .='{% assign newstringproductprice'.$i.' = "'.$arrproductproductprice[$i]['value'].'" | downcase %}
				{% assign primebanProdPrices = primebanProdPrice | plus: 0 %}
				{% assign newstringproductprices'.$i.' =  newstringproductprice'.$i.' | plus: 0 %} ';

			}

		}

		


		$arrproductproducttype = array_column( $displayrules, 'product_type' );
		
		if( !(empty($arrproductproducttype)) ){

			for($i=0; $i< count($arrproductproducttype); $i++) {

				$strIfConditiontype .='{% assign newstringproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | downcase %} ';

				if($arrproductproducttype[$i]['type'] == '3'){

					$strIfConditiontype .='{% assign valuesizeproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %} 
					{% assign newnumberproducttype'.$i.' = primebanProductType | slice: 0,valuesizeproducttype'.$i.' %}';

				} else if ($arrproductproducttype[$i]['type'] == '4'){

					$strIfConditiontype .='{% assign valuesizeproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %}{% assign mulproducttype'.$i.' = valuesizeproducttype'.$i.' | times:2 %}
					{% assign newminusproducttype'.$i.' = valuesizeproducttype'.$i.' | minus:mulproducttype'.$i.' %}
				{% assign newnumberproducttype'.$i.' = primebanProductType | slice: newminusproducttype'.$i.', valuesizeproducttype'.$i.' %}
				';
				
				} else if ($arrproductproducttype[$i]['type'] == '6'){

					$strIfConditiontype .='{% unless primebanProductType contains newstringproducttype'.$i.' %}{% assign valueproducttype'.$i.' = true %}{% endunless %}';
				}
			
			}

		}

		$arrproductvendor= array_column( $displayrules, 'product_vendor' );

		if( !(empty($arrproductvendor)) ){

			for($i=0; $i< count($arrproductvendor); $i++) {
				
				$strIfConditionvendor .='{% assign newstringvendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | downcase %}';

				if($arrproductvendor[$i]['type'] == '3'){
					
					$strIfConditionvendor .='{% assign valuesizevendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | size %}
					{% assign newnumbervendor'.$i.' = primebanProductVendor | slice: 0,valuesizevendor'.$i.' %}';
				
				} else if ($arrproductvendor[$i]['type'] == '4'){

					$strIfConditionvendor .='{% assign valuesizevendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | size %}
					{% assign mulvendor'.$i.' = valuesizevendor'.$i.' | times:2 %}
					{% assign newminusvendor'.$i.' = valuesizevendor'.$i.' | minus:mulvendor'.$i.' %}
					{% assign newnumbervendor'.$i.' = primebanProductVendor | slice: newminusvendor'.$i.', valuesizevendor'.$i.' %}';

				} else if ($arrproductvendor[$i]['type'] == '6'){

					$strIfConditionvendor .='{% unless primebanProductVendor contains newstringvendor'.$i.' %}{% assign valuevendor'.$i.' = true %}{% endunless %}';

				}
			}

		}

		$arrTagsId = array_column( $displayrules, 'tags' );
		$tagcount=0;
		foreach($arrTagsId as $tagkey){
			if(!empty( $tagkey ) && isset($tagkey['type'])){
				$strTagsId = $tagkey['value'];
				if( !empty( $strTagsId ) ){
					$strIfConditionTags  .= '{% assign primebanProductTags = "'.$strTagsId.'" | split: "," %}';
					if($tagkey['type'] == '1'){
						$strIfConditionTags  .= '{% assign isprimebanProductTagFound'.$tagcount.' = false %}
						{% for tagName in primebanProductTags %}
							{% for shopifyrealtag in product.tags %}
								{% if shopifyrealtag == tagName %}
									{% assign isprimebanProductTagFound'.$tagcount.' = true %}
									{% break %}
								{% endif %}
							{% endfor %}
						{% endfor %}';
						
					}else{
						$strIfConditionTags  .= '{% assign primebanProductTags = "'.$strTagsId.'" | split: "," %}';
						$strIfConditionTags  .= '{% assign isprimebanProductTagFound'.$tagcount.' = false %}
						{% for tagName in primebanProductTags %}
							{% for shopifyrealtag in product.tags %}
								{% assign primebanProductTag = tagName | downcase %}					
								{% if shopifyrealtag != tagName %}
									{% assign isprimebanProductTagFound'.$tagcount.' = true %}
								{% else %}
									{% assign isprimebanProductTagFound'.$tagcount.' = false %}
									{% break %}
							  	{% endif %}
							{% endfor %}
						{% endfor %}';
					}
					$arrPrepareifCondition[] = ' true == isprimebanProductTagFound'.$tagcount.' ';
				}	
				// print_r($strIfConditionTags);exit;
			}else{
				if( !empty( $tagkey ) ){
					$strTagsId = implode(",",$arrTagsId );
				}
				if( !empty( $strTagsId ) ){
					$strIfConditionTags  .= '{% assign primebanProductTags = "'.$strTagsId.'" | split: "," %}
						{% assign isprimebanProductTagFound'.$tagcount.' = false %}
						{% for tagName in primebanProductTags %}
							  {% assign primebanProductTag = tagName | downcase %}					
							{% if primebanShopifyTags contains primebanProductTag %}
								{% assign isprimebanProductTagFound'.$tagcount.' = true %}
							{% break %}
							  {% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebanProductTagFound'.$tagcount.'';
				}		
			}
			$tagcount++;
		}
		// print_r($strIfConditionTags);exit;

		$arrCustTagsId = array_column( $displayrules, 'customer_tag' );
		// echo "<pre>";print_r($arrCustTagsId[0]);exit;
		if(!empty( $arrCustTagsId ) && isset($arrCustTagsId[0]['type'])){
			$strCustTagsId = $arrCustTagsId[0]['value'];
			if( !empty( $strCustTagsId ) ){
				$strIfCustConditionTags  = '{% assign primebanCustTags = "'.$strCustTagsId.'" | split: "," %}';
				if($arrCustTagsId[0]['type'] == '1'){
					$strIfCustConditionTags  .= '{% assign isprimebanCustTagFound = false %}{% for tagName in primebanCustTags %}
						  {% assign primebanCustTag = tagName | downcase %}					
						{% if primebanCustomerTag contains primebanCustTag %}
							{% assign isprimebanCustTagFound = true %}
							{% break %}
						  {% endif %}
					{% endfor %}';
				}else{
					$strIfCustConditionTags  .= '{% assign isprimebanCustTagFound = false %}{% for tagName in primebanCustTags %}
						  {% assign primebanCustTag = tagName | downcase %}					
						{% unless primebanCustomerTag contains primebanCustTag %}
							{% assign isprimebanCustTagFound = true %}
							{% break %}
						  {% endunless %}
					{% endfor %}';
				}
				$arrPrepareifCondition[] = ' true == isprimebanCustTagFound ';
			}	
		}else{
			if( !empty( $arrCustTagsId ) ){
				$strCustTagsId = implode(",",$arrCustTagsId );
			}
			if( !empty( $strCustTagsId ) ){
				$strIfCustConditionTags  = '{% assign primebanCustTags = "'.$strCustTagsId.'" | split: "," %}
					{% assign isprimebanCustTagFound = false %}
					{% for tagName in primebanCustTags %}
						  {% assign primebanCustTag = tagName | downcase %}					
						{% if primebanCustomerTag contains primebanCustTag %}
							{% assign isprimebanCustTagFound = true %}
						{% break %}
						  {% endif %}
					{% endfor %}';
				$arrPrepareifCondition[] = ' true == isprimebanCustTagFound ';
			}		
		}
		
		$metacounter=0;
		$productitlecounter=0;
		$producttypecounter=0;
		$productpricecounter=0;
		$productvendorcounter=0;
		foreach( $displayrules as $index => $rule  ) {
			
			$ruleKey = key( $rule );
			// print_r($ruleKey);

			switch( $ruleKey ) {
			    
				case 'saleprice' :
					
					if( strpos($rule['saleprice'], ',') !== false ) {
						$newsalerule = explode(",",$rule['saleprice']);
						$sale_start = $newsalerule[0];
						$sale_end = $newsalerule[1];
					}else{
						$sale_start = $rule['saleprice'];
						$sale_end = '100';
					}
					$strIfConditionSalePercentValue ='{% assign primebanSalePercentValue = false %}
														{% if '.$sale_start.' <= SalePercent and SalePercent <= '.$sale_end.' %}
															{% assign primebanSalePercentValue = true %}
														{% endif %}';
					// $arrPrepareifCondition[] = $sale_start.' <= SalePercent and SalePercent <= '.$sale_end.'';
					$arrPrepareifCondition[] = 'primebanSalePercentValue';

			        break;

			   	case 'inventory':
			    		
		    		if( !empty( $rule['inventory'] ) ){
		    			$rule = $rule['inventory'];
		    			
		    			if( $rule['type'] == 1 ){

		    				$strIfConditionInventory .='{% assign primebanProductInventory'.$index.' = false %}	
		    				{% if 0 < Inventory and Inventory <= '.$rule['value'].' %}
		    				{% assign primebanProductInventory'.$index.' = true %}
							{% endif %}'; 
		    				$rule = ' true == primebanProductInventory'.$index.'';
		    				
						}else if( $rule['type'] == 2 ){
							$strIfConditionInventory .='{% assign primebanInventoryTrack = false %}	
		    				{% if 0 < Inventory %}
		    				{% assign primebanInventoryTrack = true %}
							{% endif %}';
		    				$rule = ' true == primebanInventoryTrack ';
		    			}else if( $rule['type'] == 3 ){
		    				$rule = ' false == product.available ';
		    			}else if( $rule['type'] == 4 ){
		    				$strIfConditionInventory .='{% assign primebanInventoryTrack = false %}	
		    				{% if 0 >= Inventory and product.available %}
		    				{% assign primebanInventoryTrack = true %}
							{% endif %}';
							$rule = ' true == primebanInventoryTrack ';
		    			}

		    			// print_r( $rule );
		    			// die;
		    			$arrPrepareifCondition[] = $rule;	
		    		}
			    	break;

			    case 'createddate':

			    	$rule['createddate'] = $rule['createddate']*86400;
			      	
			      	$arrPrepareifCondition[] = ' primebanDateDiff > 0 and primebanDateDiff <= '.$rule['createddate'].'';
					break;

				case 'publishdate':

					$rule['publishdate'] = $rule['publishdate']*86400;
						
						$arrPrepareifCondition[] = ' primebanPublishDateDiff > 0 and primebanPublishDateDiff <= '.$rule['publishdate'].'';
					break;

				case 'all_products':

					$arrPrepareifCondition[] = 'true';

				break;

				case 'title':
					
					// Starts With ........
					if($rule['title']['type'] == '1'){

						$arrPrepareifCondition[] = 'newnumberproducttitle'.$productitlecounter.' == newstringproducttitle'.$productitlecounter;
						$productitlecounter++;
				
					}
					// Ends With ........
					else if($rule['title']['type'] == '2'){

						$arrPrepareifCondition[] = ' newnumberproducttitle'.$productitlecounter.' == newstringproducttitle'.$productitlecounter;
						$productitlecounter++;

					}
					// Contains ........
					else if($rule['title']['type'] == '3'){

						$arrPrepareifCondition[] = ' primebanProductTitle contains newstringproducttitle'.$productitlecounter;
						$productitlecounter++;
					}
					// Not Contains ........
					else{

						$arrPrepareifCondition[] = ' valueproducttitle'.$productitlecounter.' == true';
						//$productitlecounter;
						$productitlecounter++;
					}

				break;

				case 'product_price':
					// Is Equal To ........

					
					if($rule['product_price']['type'] == '1' ){

					//	print_r('condition1');
						$arrPrepareifCondition[] =  'primebanProdPrices == newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Not Equal To ........
					else if($rule['product_price']['type'] == '2'){
					//	print_r('condition2');
						$arrPrepareifCondition[] = 'primebanProdPrices != 0  and primebanProdPrices != newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Is greter than ........
					else if($rule['product_price']['type'] == '3'){
					//	print_r('condition3');
						$arrPrepareifCondition[] = 'primebanProdPrices > newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Is less than ........
					else if($rule['product_price']['type'] == '4'){
						//print_r('condition4');
						$arrPrepareifCondition[] = 'primebanProdPrices != 0 and primebanProdPrices < newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}

				break;

				case 'product_type':
					// Is Equal To ........
					if($rule['product_type']['type'] == '1' ){

						$arrPrepareifCondition[] =  'primebanProductType == newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Not Equal To ........
					else if($rule['product_type']['type'] == '2'){

						$arrPrepareifCondition[] = ' primebanProductType != newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Starts With ........
					else if($rule['product_type']['type'] == '3'){

						$arrPrepareifCondition[] = 'newnumberproducttype'.$producttypecounter.' == newstringproducttype'.$producttypecounter;
						$producttypecounter++;
				
					}
					// Ends With ........
					else if($rule['product_type']['type'] == '4'){

						$arrPrepareifCondition[] = ' newnumberproducttype == newstringproducttype'.$producttypecounter;
						$producttypecounter++;

					}
					// Contains ........
					else if($rule['product_type']['type'] == '5'){

						$arrPrepareifCondition[] = ' primebanProductType contains newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Not Contains ........
					else{

						$arrPrepareifCondition[] = ' valueproducttype'.$producttypecounter.' == true';
						$producttypecounter;
						$producttypecounter++;
					}

				break;

				case 'page_type':

					$arrPageType = $rule['page_type'];
					if( !empty( $arrPageType ) ){
						$strPageType = implode(",",$arrPageType );
					}
					$strIfPageType = '{% assign primebanPageType = "'.$strPageType.'" | split: "," %}
						{% assign isprimebanpageFound = false %}
						{% for page in primebanPageType %}
							{% assign primebanRealpage = page | downcase %}					
							{% if primebanTemplate contains primebanRealpage %}
								{% assign isprimebanpageFound = true %}
							{% break %}
							{% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebanpageFound ';

				break;

				case 'productmetafield':
					// Is Equal To ........
					if($rule['productmetafield']['metacondition'] == '1' ){

						// $arrPrepareifCondition[] = 'metastring == newstringmetaval';
						$arrPrepareifCondition[] = 'metastring'.$metacounter.' == newstringmetaval'.$metacounter;
						$metacounter++;
	
					}
					// Found ........
					else if($rule['productmetafield']['metacondition'] == '2'){

					

						if(isset($rule['productmetafield']['metaname'])){

							$arrPrepareifCondition[] =  ' primebanProduct.metafields.'.$rule['productmetafield']['metaname'].' != blank';
						}

						if(isset($rule['productmetafield']['metaval']) && isset($rule['productmetafield']['metaname']) ){

							$arrPrepareifCondition[] =  'primebanProduct.metafields.'.$rule['productmetafield']['metaname'].'.'.$rule['productmetafield']['metaval'].' != blank';
						}
						$metacounter++;
					}
					// not found...........
					else{
						

						if(isset($rule['productmetafield']['metaname'])){

							$arrPrepareifCondition[] =  'primebanProduct.metafields.'.$rule['productmetafield']['metaname'].' == blank';
						}

						if(isset($rule['productmetafield']['metaval']) && isset($rule['productmetafield']['metaname']) ){

							$arrPrepareifCondition[] =  'primebanProduct.metafields.'.$rule['productmetafield']['metaname'].'.'.$rule['productmetafield']['metaval'].' == blank';
						}
						$metacounter++;
					}

				break;

				case 'customer':
					if($rule['customer']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebanCustId != null';
					}else if($rule['customer']['type'] == '2' ){
						$arrPrepareifCondition[] =  'primebanCustId == null';
					} 
				break;

				case 'product_option':
					if($rule['product_option']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebanVarient != 1';
					}else if($rule['product_option']['type'] == '2' ){
						$arrPrepareifCondition[] =  'primebanVarient == 1';
					}  
				break;

				case 'product_vendor':
					// Is Equal To ........
					if($rule['product_vendor']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebanProductVendor == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Not Equal To ........
					else if($rule['product_vendor']['type'] == '2'){
						$arrPrepareifCondition[] = 'primebanProductVendor != newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Starts With ........
					else if($rule['product_vendor']['type'] == '3'){

						$arrPrepareifCondition[] = 'newnumbervendor'.$productvendorcounter.' == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
						
					}
					// Ends With ........
					else if($rule['product_vendor']['type'] == '4'){

						$arrPrepareifCondition[] = 'newnumbervendor'.$productvendorcounter.' == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
						
					}
					// Contains ........
					else if($rule['product_vendor']['type'] == '5'){
						
						$arrPrepareifCondition[] = 'primebanProductVendor contains newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Not Contains ........
					else{
						$arrPrepareifCondition[] = ' valuevendor'.$productvendorcounter.' == true';
						$productvendorcounter++;
					}

				break;
				
				case 'weight':
					// Is Equal To ........
					if($rule['weight']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebanProductWeight == '. $rule['weight']['value'];
					}
					// Less Than ........
					else if($rule['weight']['type'] == '2'){
						$arrPrepareifCondition[] = 'primebanProductWeight < '. $rule['weight']['value'];
					}
					// Greater Than ........
					else if($rule['weight']['type'] == '3'){
						$arrPrepareifCondition[] = 'primebanProductWeight > '. $rule['weight']['value'];
					}
					else{
						$arrPrepareifCondition[] = 'false';
					}

					break;
					

			    }
		}

		if($displayprorules != ''){
			foreach( $displayprorules as $index => $prorule  ) {
			
				$proruleKey = key( $prorule );
	
				switch( $proruleKey ) {
					
					case 'order_count':
						// Is greter than ........
						if($prorule['order_count']['value'] == ''){

							// $arrPrepareifCondition[] = 'true';

						}elseif($prorule['order_count']['type'] == '1' ){
	
							$arrPrepareifCondition[] = 'OrderCount > '. $prorule['order_count']['value'];
							
						}
						// Is less than ........
						else if($prorule['order_count']['type'] == '2'){
							
							$arrPrepareifCondition[] = 'OrderCount < '. $prorule['order_count']['value'];
							
						}
	
					break;

					case 'recentorder_count':
						// Is greter than ........
						if($prorule['recentorder_count']['value'] == ''){

							// $arrPrepareifCondition[] = 'true';

						}elseif($prorule['recentorder_count']['type'] == '1' ){
	
							$arrPrepareifCondition[] = 'RecentSoldCount > '. $prorule['recentorder_count']['value'];
							
						}
						// Is less than ........
						else if($prorule['recentorder_count']['type'] == '2'){
							
							$arrPrepareifCondition[] = 'RecentSoldCount < '. $prorule['recentorder_count']['value'];
							
						}
	
					break;
				
				}

			}
		}

		if($shop_language != ''){
			$strIfLangType = '{% assign primebanapplang = "'.$shop_language.'" | split: "," %}
						{% assign isprimebanlangFound = false %}
						{% for lang in primebanapplang %}
							{% assign primebanReallang = lang | downcase %}					
							{% if primebanLanguage contains primebanReallang %}
								{% assign isprimebanlangFound = true %}
							{% break %}
							{% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebanlangFound ';
		}

		if( !empty( $arrPrepareifCondition ) && is_array( $arrPrepareifCondition ) ){
			$arrPrepareifCondition = implode("".$condition."", $arrPrepareifCondition );

		}else{

			$arrPrepareifCondition = '';
		}
 
		if ( strpos($banner['title'], '{{VariantCount}}') !== false ) {


			$newTitle = $banner['title'];
			// $banner['title']="  {% assign VariantCount = product.variants.size %} {% if VariantCount == 1 %} {% assign VariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}  {% else %} {% assign VariantText = '.$newTitle.' %} {% endif %}";

			// $banner['title']="  {% assign VariantCount = product.variants.size %} {% if VariantCount == 1 %} {% assign VariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}  {% else %} {% assign VariantText = '.$newTitle.' %} {% endif %}";

			// $banner['title']=htmlspecialchars(" {% assign VariantCount = product.variants.size %}    
			// {% assign VariantText =    '".$newTitle."' | replace: '{{VariantCount}}', '' %}
			// {% assign noVariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}
			// {% assign VariantCount = product.variants.size %} 
			// {% capture newVariantText %}{{ VariantCount }}  {{ VariantText }}{% endcapture %}
			// {% if VariantCount == 1 %} {{ noVariantText }}  {% else %} {{ newVariantText }}  {% endif %} ");
			
		}

		if(isset($banner['tooltip']) && $banner['tooltip'] != ''){
			$tooltip = "data-tippy-content= '".$banner['tooltip']."'";
		}else{
			$tooltip = '';
		}
		
		// $defaultSize = "data-defaultsize= '".$bannerstyle['textsize']."'";
		// $MobileSize = "data-mobilesize= '".(isset( $bannerstyle['textmobilesize'] ) ?  $bannerstyle['textmobilesize'] :'0' )."'";
		// $TabletSize = "data-tabletsize= '".(isset( $bannerstyle['texttabletsize'] ) ?  $bannerstyle['texttabletsize'] :'0' )."'";
		
		$primebanDefaultHeight = "data-defaultheight= '".(isset( $display['height'] ) ?  $display['height'] :'50' )."'";
		// $primebanMobileHeight = "data-mobileheight= '".(isset( $bannerstyle['imagemobilesize'] ) ?  $bannerstyle['imagemobilesize'] :'0' )."'";
		// $primebanTabletHeight = "data-tabletheight= '".(isset( $bannerstyle['imagetabletsize'] ) ?  $bannerstyle['imagetabletsize'] :'0' )."'";

		$primebanCountrySelected = "data-countryselectedb= '".(isset( $CountryClass ) ?  $customer_location :'' )."'";

		if(isset( $display['height'] )){
			if(isset($banner['imgsrc'])){
				$imgStyle = "max-height:".$display['height']."px;";
			}else{
				$imgStyle = "max-height:".$display['height']."px;";
			}

			if($banner['banner_type'] == 'text'){
				$imgStyle .= "min-height:".$display['height']."px;";
				$imgStyle .= "max-width:".$display['height']."px;";
				$imgStyle .= "min-width:".$display['height']."px;";
			}
			
		}else{
			$imgStyle = "max-height:30px";

			if($banner['banner_type'] == 'text'){
				$imgStyle .= "min-height:30px;";
				$imgStyle .= "max-width:30px;";
				$imgStyle .= "min-width:30px;";
			}
		}

		$realtitle = '{% if product.metafields.prime.myordercount != blank %}
							{% assign OrderCount = product.metafields.prime.myordercount.ordercount | default: 0 %}
						    {% assign RecentSoldCount = product.metafields.prime.myordercount.recentcount | default: 0 %}
						{% else  %} 
							{% assign OrderCount = product.metafields.prime.ordercount | default: 0 %}
							{% assign RecentSoldCount = product.metafields.prime.recentcount | default: 0 %}
                        {% endif %}';
                        
        if( $bannerstyle['spacing'] == 1 ){
            $primebanDesktopclass = 'prime-p-1';
            $primebanImageOuterclass = 'prime-pr-1';
        }else if($bannerstyle['spacing'] == 2){
            $primebanDesktopclass = 'prime-p-2';
            $primebanImageOuterclass = 'prime-pr-2';
        }else if($bannerstyle['spacing'] == 3){
            $primebanDesktopclass = 'prime-p-3';
            $primebanImageOuterclass = 'prime-pr-3';
        }else if($bannerstyle['spacing'] == 4){
            $primebanDesktopclass = 'prime-p-4';
            $primebanImageOuterclass = 'prime-pr-4';
        }else if($bannerstyle['spacing'] == 5){
            $primebanDesktopclass = 'prime-p-5';
            $primebanImageOuterclass = 'prime-pr-5';
		}
		
		if($banner['banner_type'] == 'image'){
			$primebanDesktopclass = '';
			$primebanImageOuterclass = '';
			$imgStyle .='max-width: 100%;';
			if(isset($bannerstyle['bordersize'])){
				$imgStyle .='border:'.$bannerstyle['bordersize'].' '.$bannerstyle['bordercolor'].' solid; ';
			}
			if($bannerstyle['shadowimage'] == 'light'){
				$imgStyle .= 'box-shadow: rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px, rgba(0, 0, 0, 0.2) 0px 1px 5px 0px;';
			}
		}

		$downtitlesty = '';
		$downsubtitlesty = '';
		
		if($countdowntitle == 1){
			$downtitlesty = 'prime-counter';
		}
		if($countdownsubtitle == 1){
			$downsubtitlesty = 'prime-counter';
		}

		if (strpos($banner['title'], '<span class="primebantitleloop"') !== false ) {
			$nodisplay = 'visibility:hidden;';
		}else{
			$nodisplay = '';
		}

		if (strpos($banner['subtitle'], '<span class="primebansubtitleloop"') !== false ) {
			$nodisplaysub = 'visibility:hidden;';
		}else{
			$nodisplaysub = '';
		}
		
		$banneridclass = 'primeban-'.$banner['productbannerid']; 
		if( !isset($banner['imgsrc']) ) {
			$alttext = 'banner';
		} else {
			$pos = strrpos($banner['imgsrc'],"/");
			$alttextold = substr($banner['imgsrc'],$pos+1);
			$alttext = str_replace('.png', '', $alttextold);
		} 
		$buttonHtml = View('liquid.bannerstyle', [
									'type'    		=> $banner['banner_type'],
									'image'			=> 'prime_ban'.$banner['productbannerid'].'.png',
                                    'imgStyle'  		=> $imgStyle,
                                    'primebanDesktopclass'  		=> $primebanDesktopclass,
									'primebanImageOuterclass'  		=> $primebanImageOuterclass,
									'id'			=> $banner['productbannerid'],
									'height' 		=> isset( $display['height'] ) ?  $display['height'].'px' :'50px' ,
									'bannerstyle' 	=> $appearance['desktop']['bannerStyle'],
									'bannertitlestyle' 	=> $appearance['desktop']['bannertitleStyle'],
									'bannersubtitlestyle' 	=> $appearance['desktop']['bannersubtitleStyle'],
									'spanStyleleft' => isset( $appearance['desktop']['spanStyleleft'] ) ?  $appearance['desktop']['spanStyleleft'] :'' ,
									'title'    		=> $banner['title'],
									'subtitle'    		=> $banner['subtitle'],
									'tooltip'    	=> $banner['tooltip'],
									'animation'		=> $animationType,
									'linktab'      => $banner['linktab'],
									'modalsize'		=>   $banner['modalsize'],
									'primebannerid' =>$banner['productbannerid'],
									'banneridclass' => $banneridclass,
									'bannerlink'      => $banner['bannerlink'],
									'primebantooltip' => $tooltip,

									'downtitlesty' => $downtitlesty,
									'downsubtitlesty' => $downsubtitlesty,

									'CountyClass' 	=> $CountryClass,
									'SelectedCountry' => $primebanCountrySelected,
									
									// 'primebanDefaultSize' => $defaultSize,
									// 'primebanMobileSize' => $MobileSize,
									// 'primebanTabletSize' => $TabletSize,

									'primebanDefaultHeight' => $primebanDefaultHeight,
									// 'primebanMobileHeight' => $primebanMobileHeight,
									// 'primebanTabletHeight' => $primebanTabletHeight,
									'alttext' => $alttext,
									'nodisplay'			 => $nodisplay,
									'nodisplaysub'		 => $nodisplaysub,
									'imgformat'			 => $banner['imageformat'],
									] )->render();
		 

		if( !empty( $arrPrepareifCondition ) ){

			$arrPrepareifCondition = $strIfPageType.$strIfLangType.$realtitle.'{% if '.$arrPrepareifCondition.' %}';
			$arrPrepareifCondition = ' {% assign isStartbShowBanner = false %} '.$strIfProductCondition.$strIfCollectionCondition.$strIfConditionInventory.$strIfConditionprice.$strIfConditionmetafield.$strIfConditionvendor.$strIfConditiontype.$strIfConditionTags.$strIfCustConditionTags.$strIfConditionSalePercentValue.$arrPrepareifCondition;
			$arrPrepareifCondition .= '{% assign isStartbShowBanner = true %}';
			if( !empty( $excludeproduct ) ){
				$arrPrepareifCondition .= '{% assign primebanExProductIds = "'.$excludeproduct.'" | split: "," %}
				{% assign primebanExProductId = primebanProduct.id | downcase %}
				{% if primebanExProductIds contains primebanExProductId %}
					{% assign isStartbShowBanner = false %}
				{% endif %}';
			}
			$arrPrepareifCondition  .= '{% endif %}';

			$arrPrepareifCondition .= '{% assign primebanUTC 		= "now" | date: "%s" %}
			{% assign primebanUTC 		= primebanUTC | plus: 0 %}
			{% assign primebanStartTime = '.$banner['starttime'].'  %}
			{% assign primebanEndTime = '.$banner['endtime'].'  %}
			{% if primebanStartTime <= primebanUTC and primebanEndTime >= primebanUTC %}';	
			
			// if($banner['banner_type'] == 'image'){

				if( !isset($banner['imgsrc']) ) {
					$arrPrepareifCondition .= "{% assign Image = 'prime_ban".$banner['productbannerid'].".".$banner['imageformat']."'  %}";
					if( isset($display['height']) ) {
						$arrPrepareifCondition .= "{% assign ImageHeight = 'x".$display['height']."' %}";
					}else{
						$arrPrepareifCondition .= "{% assign ImageHeight = 'x22' %}";
					}

					if($banner['imageformat'] == 'svg'){
						$arrPrepareifCondition .= "{% assign Imagesize = Image | asset_url %}";
					}else{
						$arrPrepareifCondition .= "{% assign Imagesize = Image | asset_img_url: 'master' %}";
					}

				} else {

					$arrPrepareifCondition .= "{% assign Image = '".$banner['imgsrc']."'  %}";
					$arrPrepareifCondition .= "{% assign Imagesize = Image  %}";
				} 
			// }
			
			$arrPrepareifCondition .= "{% if isStartbShowBanner == true %} 
				{% assign primebannerGroup = '".$banner['bannergroup']."' %} 
				{% if primeGroup == primebannerGroup %}
				{% assign primebanBannerCounter = primebanBannerCounter | plus: 1 %}
					".$buttonHtml."
				{% endif %}
				{% endif %}{% endif %}";
		}

		return array( 'global_variable'=>$strAssignGlobal ,'condition'=>$arrPrepareifCondition );

		
	}

}

?>
