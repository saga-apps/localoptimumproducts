<?php

namespace App\Helpers;

use Log;

class LiquidHelper {

	public static function getLiquidProductCondition( $displayrules , $condition ,$appearance ,  $badge, $display, $excludeproduct, $badgestyle, $CountryClass, $customer_location, $displayprorules, $countdowntitle, $shop_language, $LangClass ){
		$arrIfConditon = array();

		$badgejson = json_decode($badge['badgestyle']);
		$animationstyle='';
		
		if(isset($badgejson->animation)){

			$animationstyle .='
				:root {
					--animate-duration: 1s;
					--animate-delay: 1s;
					--animate-repeat: 1;
				  }
				  .animate__animated {
					-webkit-animation-duration: 1s;
					animation-duration: 1s;
					-webkit-animation-duration: var(--animate-duration);
					animation-duration: var(--animate-duration);
					-webkit-animation-fill-mode: both;
					animation-fill-mode: both;
				  }

				  @-webkit-keyframes primeslide {
					from {
					  -webkit-transform: translate3d(0, -100%, 0);
					  transform: translate3d(0, -100%, 0);
					  visibility: visible;
					}
				  
					to {
					  -webkit-transform: translate3d(0, 0, 0);
					  transform: translate3d(0, 0, 0);
					}
				  }
				  @keyframes primeslide {
					from {
					  -webkit-transform: translate3d(0, -100%, 0) !important;
					  transform: translate3d(0, -100%, 0);
					  visibility: visible;
					}
				  
					to {
					  -webkit-transform: translate3d(0, 0, 0) !important;
					  transform: translate3d(0, 0, 0);
					}
				  }
				  .prime-slide {
					
					 animation-name: primeslide !important; 
				  }

				  @-webkit-keyframes primepop {
					from {
					  opacity: 0;
					  -webkit-transform: scale3d(0.3, 0.3, 0.3);
					  transform: scale3d(0.3, 0.3, 0.3);
					}
				  
					50% {
					  opacity: 1;
					}
				  }
				  @keyframes primepop {
					from {
					  opacity: 0;
					  -webkit-transform: scale3d(0.3, 0.3, 0.3);
					  transform: scale3d(0.3, 0.3, 0.3);
					}
				  
					50% {
					  opacity: 1;
					}
				  }
				  .prime-pop {
					-webkit-animation-name: primepop;
					animation-name: primepop;
				  }

				  @-webkit-keyframes groove {
					from {
					  opacity: 0;
					  -webkit-transform: scale(0.1) rotate(30deg);
					  transform: scale(0.1) rotate(30deg);
					  -webkit-transform-origin: center bottom;
					  transform-origin: center bottom;
					}
				  
					50% {
					  -webkit-transform: rotate(-10deg);
					  transform: rotate(-10deg);
					}
				  
					70% {
					  -webkit-transform: rotate(3deg);
					  transform: rotate(3deg);
					}
				  
					to {
					  opacity: 1;
					  -webkit-transform: scale(1);
					  transform: scale(1);
					}
				  }
				  @keyframes groove {
					from {
					  opacity: 0;
					  -webkit-transform: scale(0.1) rotate(30deg);
					  transform: scale(0.1) rotate(30deg);
					  -webkit-transform-origin: center bottom;
					  transform-origin: center bottom;
					}
				  
					50% {
					  -webkit-transform: rotate(-10deg);
					  transform: rotate(-10deg);
					}
				  
					70% {
					  -webkit-transform: rotate(3deg);
					  transform: rotate(3deg);
					}
				  
					to {
					  opacity: 1;
					  -webkit-transform: scale(1);
					  transform: scale(1);
					}
				  }
				  .prime-groove {
					-webkit-animation-name: groove;
					animation-name: groove;
				  }

				  @-webkit-keyframes fly {
					0% {
					  -webkit-transform: translateY(-1200px) scale(0.7);
					  transform: translateY(-1200px) scale(0.7);
					  opacity: 0.7;
					}
				  
					80% {
					  -webkit-transform: translateY(0px) scale(0.7);
					  transform: translateY(0px) scale(0.7);
					  opacity: 0.7;
					}
				  
					100% {
					  -webkit-transform: scale(1);
					  transform: scale(1);
					  opacity: 1;
					}
				  }
				  @keyframes fly {
					0% {
					  -webkit-transform: translateY(-1200px) scale(0.7);
					  transform: translateY(-1200px) scale(0.7);
					  opacity: 0.7;
					}
				  
					80% {
					  -webkit-transform: translateY(0px) scale(0.7);
					  transform: translateY(0px) scale(0.7);
					  opacity: 0.7;
					}
				  
					100% {
					  -webkit-transform: scale(1);
					  transform: scale(1);
					  opacity: 1;
					}
				  }
				  .prime-fly {
					-webkit-animation-name: fly;
					animation-name: fly;
				  }
				  
				  @-webkit-keyframes swerve {
					20% {
					  -webkit-transform: rotate3d(0, 0, 1, 15deg);
					  transform: rotate3d(0, 0, 1, 15deg);
					}
				  
					40% {
					  -webkit-transform: rotate3d(0, 0, 1, -10deg);
					  transform: rotate3d(0, 0, 1, -10deg);
					}
				  
					60% {
					  -webkit-transform: rotate3d(0, 0, 1, 5deg);
					  transform: rotate3d(0, 0, 1, 5deg);
					}
				  
					80% {
					  -webkit-transform: rotate3d(0, 0, 1, -5deg);
					  transform: rotate3d(0, 0, 1, -5deg);
					}
				  
					to {
					  -webkit-transform: rotate3d(0, 0, 1, 0deg);
					  transform: rotate3d(0, 0, 1, 0deg);
					}
				  }
				  @keyframes swerve {
					20% {
					  -webkit-transform: rotate3d(0, 0, 1, 15deg);
					  transform: rotate3d(0, 0, 1, 15deg);
					}
				  
					40% {
					  -webkit-transform: rotate3d(0, 0, 1, -10deg);
					  transform: rotate3d(0, 0, 1, -10deg);
					}
				  
					60% {
					  -webkit-transform: rotate3d(0, 0, 1, 5deg);
					  transform: rotate3d(0, 0, 1, 5deg);
					}
				  
					80% {
					  -webkit-transform: rotate3d(0, 0, 1, -5deg);
					  transform: rotate3d(0, 0, 1, -5deg);
					}
				  
					to {
					  -webkit-transform: rotate3d(0, 0, 1, 0deg);
					  transform: rotate3d(0, 0, 1, 0deg);
					}
				  }
				  .prime-swerve{
					-webkit-transform-origin: top center;
					transform-origin: top center;
					-webkit-animation-name: swerve;
					animation-name: swerve;
				  }
				 
				  
				  ';
			
			$animationType = $badgejson->animation;
			

		} else {

			$animationType ='';
		}

		// print_r($animationstyle);

		$strAssignGlobal = "{% assign HideAssets = hideAssets | default: 0 %}{% if HideAssets != '1' %}{% render 'prime-css' %}{% endif %}";

		$strAssignGlobal     .= '
			{% assign primebMobileSize = primebMobileSize | default: 100 %}
			{% assign primebTabletSize = primebTabletSize | default: 100 %}
			{% assign primebProduct = product %}
			{% assign primebCustId = customer.id %}
			{% assign primebVarient = product.variants.size %}
			{%- assign primebmoneyformat = shop.money_format | split: "{{" | last | remove: "}}" | strip | strip_html -%}
			{% if primebmoneyformat == "amount_with_comma_separator" or primebmoneyformat == "amount_no_decimals_with_comma_separator" %}
				{% assign primebProdPrice = product.price | money_without_currency | remove: "." | replace: ",", "." %}
			{% else %} 
				{% assign primebProdPrice = product.price | money_without_currency | remove: "," %}
			{% endif %}
			{% assign primebProdPrice = product.price | divided_by: 100.00 %}
			{% assign primebCustomerTag = customer.tags | downcase %}
			{% assign primebPublishedDate = product.published_at | date: "%s" %}
			{% assign primebPublishDateDiff = "now" | date: "%s" | minus: primebPublishedDate %}
			{% assign primebTemplate = primePageType | default: template | downcase %}
			{% assign primebLanguage = request.locale.iso_code %}
			{% assign primemetafield = primebProduct.metafields %}
			{% assign primebProductTitle = primebProduct.title | downcase %}
			{% assign primebProductType = primebProduct.type | downcase %}
			{% assign primebProductVendor = primebProduct.vendor | downcase %}
			{% assign primebShopifyTags = primebProduct.tags | downcase %}
			{% assign primebProductWeight = product.variants.first.weight %}
			{% assign primebUTC = "now" | date: "%s" %}
			{% assign primebstartTime = "'.$badge['starttime'].'"  %}
			{% assign primebEndTime = "'.$badge['endtime'].'"  %}
			{% assign primebMaxDisplayBadge = primebMaxDisplayBadge | default: 10 %}	
			{% assign primebCreatedDateSec = primebProduct.created_at | date: "%s" %}
			{% assign primebDateDiff = "now" | date: "%s" | minus: primebCreatedDateSec %}
			{% assign primebCollectionIds = primebProduct.collections | map: "id" | default: 0  %}			
			{% assign primebBadgeCounter = 1 %}
			{% assign VariantCount = product.variants.size %}
			{% assign SaleAmount    = 0 %}	
			{% assign SalePercent   = 0 %}
			{% assign primeGroup =  primebGroup %}
			{% assign primebadgeLinks = primebLinks | default: 0 %} 
			{% assign ProductSKU = product.selected_or_first_available_variant.sku | default: 0 %}
			{% if primeGroup %} 
				{% assign primeGroup =  primebGroup %} 
			{% else %} 
				{% assign primeGroup =  "1" %} 
			{% endif %}
            {% if primebProduct.price and primebProduct.compare_at_price %}
				{% assign SaleAmount = primebProduct.compare_at_price | minus: primebProduct.price %}	
				{% if SaleAmount > 0 %}
                	{% assign SalePercent = SaleAmount | times: 100.0 | divided_by: primebProduct.compare_at_price %}
           		{% endif %}
        	{% endif %}
			{% assign Inventory = 0 %}
			{% assign Inventory2 = 0 %}
        	{% for productVariant in primebProduct.variants %}
				{% assign Inventory2 = productVariant.inventory_quantity | at_least: 0 %}
				{% assign Inventory = Inventory | plus: Inventory2 %}			
			{% endfor %}
			<div class="primeBadges {{primebOuterClass}} prime-d-block" style="{{ primebOuterStyle }}">' ;

		$strIfConditionInventory = '';
		$strIfConditionSalePercentValue = '';
		$strIfProductCondition   = '';
		$strIfCollectionCondition   = '';
		$arrPrepareifCondition    = [];

		$strIfConditionTags       = '';
		$strIfCustConditionTags       = '';
		$strIfConditionmetafield='';
		$strIfConditionvendor='';
		$strIfConditiontype='';
		$strIfConditionprice='';
		$strIfPageType = '';
		$strIfLangType = '';
		
		
	
		$arrProductId = array_column( $displayrules, 'product' );

		$strProductId = implode(', ', array_map(function ($arrProductId) {

		if( empty( $arrProductId ) ) {
			return '';
		} 

			return  $arrProductId[0] ;

		}, $arrProductId ) );

		
		// prepare Productid condition;
		if( !empty( $strProductId ) ){

			$strIfProductCondition = '{% assign primebProductIds = "'.$strProductId.'" | split: "," %}
						{% assign isprimebProductFound = false %}
						{% assign primebProductId = primebProduct.id | downcase %}
                        {% if primebProductIds contains primebProductId %}
                            {% assign isprimebProductFound = true %}
                        {% endif %}';

			$arrPrepareifCondition[] = ' true == isprimebProductFound ';
		}

		$arrCollectionId = array_column( $displayrules, 'collection' );
		$collectioncount=0;
		foreach($arrCollectionId as $collectionkey){
			if(!empty( $collectionkey ) && isset($collectionkey['type'])){
				$strCollectionId = $collectionkey['value'];
				if( !empty( $strCollectionId ) ){
					$strIfCollectionCondition  .= '{% assign primebProductCollections = "'.$strCollectionId.'" | split: "," %}';
					if($collectionkey['type'] == '1'){
						$strIfCollectionCondition  .= '{% assign isprimebProductCollectionFound'.$collectioncount.' = false %}
						{% for collectionid in primebProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}
							{% for shopifyrealcollection in primebCollectionIds %}
								{% if shopifyrealcollection == collectionid2 %}
									{% assign isprimebProductCollectionFound'.$collectioncount.' = true %}
									{% break %}
								{% endif %}
							{% endfor %}
						{% endfor %}';
					}else{
						$strIfCollectionCondition  .= '{% assign break_loop = false %}{% assign isprimebProductCollectionFound'.$collectioncount.' = false %}
						{% if primebCollectionIds == 0 %}
							{% assign isprimebProductCollectionFound'.$collectioncount.' = true %}
						{% endif %}
						{% for collectionid in primebProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}
							{% for shopifyrealcollection in primebCollectionIds %}				
								{% if shopifyrealcollection != collectionid2 %}
									{% assign isprimebProductCollectionFound'.$collectioncount.' = true %}
								{% else %}
									{% assign isprimebProductCollectionFound'.$collectioncount.' = false %}
									{% assign break_loop = true %}
									{% break %}
							  	{% endif %}
							{% endfor %}
							{% if break_loop %}
								{% break %}
							{% endif %}
						{% endfor %}';
					}
					$arrPrepareifCondition[] = ' true == isprimebProductCollectionFound'.$collectioncount.' ';
				}	
			}else{
				if( !empty( $collectionkey ) ){
					$strCollectionId = $arrCollectionId[0][0];
				}
				if( !empty( $strCollectionId ) ){
					$strIfCollectionCondition  .= '{% assign primebProductCollections = "'.$strCollectionId.'" | split: "," %}
						{% assign isprimebProductCollectionFound'.$collectioncount.' = false %}
						{% for collectionid in primebProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}			
							{% if primebCollectionIds contains collectionid2 %}
								{% assign isprimebProductCollectionFound'.$collectioncount.' = true %}
							{% break %}
							  {% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebProductCollectionFound'.$collectioncount.'';
				}		
			}
			$collectioncount++;
		}

		$arrproductmetafield = array_column( $displayrules, 'productmetafield' );
		if( !(empty($arrproductmetafield)) ){


			for($i=0; $i< count($arrproductmetafield); $i++) {
				
				$strIfConditionmetafield .='{% assign newstringmetaval'.$i.' = "'.$arrproductmetafield[$i]['metaval'].'" | downcase %} {% assign metastring'.$i.' = primebProduct.metafields.'.$arrproductmetafield[$i]['metaname'].' | downcase %}';
			}
		}

		$arrproductproducttype = array_column( $displayrules, 'title' );
		if( !(empty($arrproductproducttype)) ){

			for($i=0; $i< count($arrproductproducttype); $i++) {

				$strIfConditiontype .='{% assign newstringproducttitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | downcase %} ';

				if($arrproductproducttype[$i]['type'] == '1'){

					$strIfConditiontype .='{% assign valuesizeproductitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %} 
					{% assign newnumberproducttitle'.$i.' = primebProductTitle | slice: 0,valuesizeproductitle'.$i.' %}';

				} else if ($arrproductproducttype[$i]['type'] == '2'){

					$strIfConditiontype .='{% assign valuesizeproducttitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %}{% assign mulproducttitle'.$i.' = valuesizeproducttitle'.$i.' | times:2 %}
					{% assign newminusproducttitle'.$i.' = valuesizeproducttitle'.$i.' | minus:mulproducttitle'.$i.' %}
				{% assign newnumberproducttitle'.$i.' = primebProductTitle | slice: newminusproducttitle'.$i.', valuesizeproducttitle'.$i.' %}
				';
				
				} else if ($arrproductproducttype[$i]['type'] == '4'){

					$strIfConditiontype .='{% unless primebProductTitle contains newstringproducttitle'.$i.' %}{% assign valueproducttitle'.$i.' = true %}{% endunless %}';
				}
			
			}

		}

		$arrproductproductprice = array_column( $displayrules, 'product_price' );

		if( !(empty($arrproductproductprice)) ){

			for($i=0; $i< count($arrproductproductprice); $i++) {

				$strIfConditionprice .='{% assign newstringproductprice'.$i.' = "'.$arrproductproductprice[$i]['value'].'" | downcase %}
				{% assign primebProdPrices = primebProdPrice | plus: 0 %}
				{% assign newstringproductprices'.$i.' =  newstringproductprice'.$i.' | plus: 0 %} ';

			}

		}


		$arrproductproducttype = array_column( $displayrules, 'product_type' );
	
		if( !(empty($arrproductproducttype)) ){

			for($i=0; $i< count($arrproductproducttype); $i++) {

				$strIfConditiontype .='{% assign newstringproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | downcase %} ';

				if($arrproductproducttype[$i]['type'] == '3'){

					$strIfConditiontype .='{% assign valuesizeproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %} 
					{% assign newnumberproducttype'.$i.' = primebProductType | slice: 0,valuesizeproducttype'.$i.' %}';

				} else if ($arrproductproducttype[$i]['type'] == '4'){

					$strIfConditiontype .='{% assign valuesizeproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %}{% assign mulproducttype'.$i.' = valuesizeproducttype'.$i.' | times:2 %}
					{% assign newminusproducttype'.$i.' = valuesizeproducttype'.$i.' | minus:mulproducttype'.$i.' %}
				{% assign newnumberproducttype'.$i.' = primebProductType | slice: newminusproducttype'.$i.', valuesizeproducttype'.$i.' %}
				';
				
				} else if ($arrproductproducttype[$i]['type'] == '6'){

					$strIfConditiontype .='{% unless primebProductType contains newstringproducttype'.$i.' %}{% assign valueproducttype'.$i.' = true %}{% endunless %}';
				}
			
			}

		}

		$arrproductvendor= array_column( $displayrules, 'product_vendor' );

		if( !(empty($arrproductvendor)) ){

			for($i=0; $i< count($arrproductvendor); $i++) {
				
				$strIfConditionvendor .='{% assign newstringvendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | downcase %}';

				if($arrproductvendor[$i]['type'] == '3'){
					
					$strIfConditionvendor .='{% assign valuesizevendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | size %}
					{% assign newnumbervendor'.$i.' = primebProductVendor | slice: 0,valuesizevendor'.$i.' %}';
				
				} else if ($arrproductvendor[$i]['type'] == '4'){

					$strIfConditionvendor .='{% assign valuesizevendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | size %}
					{% assign mulvendor'.$i.' = valuesizevendor'.$i.' | times:2 %}
					{% assign newminusvendor'.$i.' = valuesizevendor'.$i.' | minus:mulvendor'.$i.' %}
					{% assign newnumbervendor'.$i.' = primebProductVendor | slice: newminusvendor'.$i.', valuesizevendor'.$i.' %}';

				} else if ($arrproductvendor[$i]['type'] == '6'){

					$strIfConditionvendor .='{% unless primebProductVendor contains newstringvendor'.$i.' %}{% assign valuevendor'.$i.' = true %}{% endunless %}';

				}
			}

		}
		
		// $arrTagsId = array_column( $displayrules, 'tags' );
		// if( !empty( $arrTagsId ) ){
		// 	$strTagsId = implode(",",$arrTagsId );
		// 	$strTagsId = str_replace('"','&quot;',$strTagsId);
		// }	
		// if( !empty( $strTagsId ) ){
		// 	$strIfConditionTags = '{% assign primebProductTags = "'.$strTagsId.'" | split: "," %}
		// 		{% assign isprimebProductTagFound = false %}
		// 		{% for tagName in primebProductTags %}
		// 	      	{% assign primebProductTag = tagName | downcase %}					
		// 			{% if primebShopifyTags contains primebProductTag %}
		// 				{% assign isprimebProductTagFound = true %}
		// 	        {% break %}
		// 	      	{% endif %}
		// 	    {% endfor %}';
		// 	$arrPrepareifCondition[] = ' true == isprimebProductTagFound ';
		// }


		$arrTagsId = array_column( $displayrules, 'tags' );
		$tagcount=0;
		foreach($arrTagsId as $tagkey){
			if(!empty( $tagkey ) && isset($tagkey['type'])){
				$strTagsId = $tagkey['value'];
				if( !empty( $strTagsId ) ){
					$strIfConditionTags  .= '{% assign primebProductTags = "'.$strTagsId.'" | split: "," %}';
					if($tagkey['type'] == '1'){
						$strIfConditionTags  .= '{% assign isprimebProductTagFound'.$tagcount.' = false %}
						{% for tagName in primebProductTags %}
							{% for shopifyrealtag in product.tags %}
								{% if shopifyrealtag == tagName %}
									{% assign isprimebProductTagFound'.$tagcount.' = true %}
									{% break %}
								{% endif %}
							{% endfor %}
						{% endfor %}';
						
					}else{
						$strIfConditionTags  .= '{% assign primebProductTags = "'.$strTagsId.'" | split: "," %}';
						$strIfConditionTags  .= '{% assign isprimebProductTagFound'.$tagcount.' = false %}
						{% for tagName in primebProductTags %}
							{% for shopifyrealtag in product.tags %}
								{% assign primebProductTag = tagName | downcase %}					
								{% if shopifyrealtag != tagName %}
									{% assign isprimebProductTagFound'.$tagcount.' = true %}
								{% else %}
									{% assign isprimebProductTagFound'.$tagcount.' = false %}
									{% break %}
							  	{% endif %}
							{% endfor %}
						{% endfor %}
						{% if product.tags == blank %}{% assign isprimebProductTagFound'.$tagcount.' = true %}{% endif %}';
					}
					$arrPrepareifCondition[] = ' true == isprimebProductTagFound'.$tagcount.' ';
				}	
				// print_r($strIfConditionTags);exit;
			}else{
				
				if( !empty( $tagkey ) ){
					$strTagsId = implode(",",$arrTagsId );
				}
				if( !empty( $strTagsId ) ){
					$strIfConditionTags  .= '{% assign primebProductTags = "'.$strTagsId.'" | split: "," %}
						{% assign isprimebProductTagFound'.$tagcount.' = false %}
						{% for tagName in primebProductTags %}
							  {% assign primebProductTag = tagName | downcase %}					
							{% if primebShopifyTags contains primebProductTag %}
								{% assign isprimebProductTagFound'.$tagcount.' = true %}
							{% break %}
							  {% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebProductTagFound'.$tagcount.'';
				}		
			}
			$tagcount++;
		}



		$arrCustTagsId = array_column( $displayrules, 'customer_tag' );
		if(!empty( $arrCustTagsId ) && isset($arrCustTagsId[0]['type'])){
			$strCustTagsId = $arrCustTagsId[0]['value'];
			if( !empty( $strCustTagsId ) ){
				$strIfCustConditionTags  = '{% assign primebCustTags = "'.$strCustTagsId.'" | split: "," %}';
				if($arrCustTagsId[0]['type'] == '1'){
					$strIfCustConditionTags  .= '{% assign isprimebCustTagFound = false %}{% for tagName in primebCustTags %}
						  {% assign primebCustTag = tagName | downcase %}					
						{% if primebCustomerTag contains primebCustTag %}
							{% assign isprimebCustTagFound = true %}
							{% break %}
						  {% endif %}
					{% endfor %}';
				}else{
					$strIfCustConditionTags  .= '{% assign isprimebCustTagFound = false %}{% for tagName in primebCustTags %}
						  {% assign primebCustTag = tagName | downcase %}					
						{% unless primebCustomerTag contains primebCustTag %}
							{% assign isprimebCustTagFound = true %}
							{% break %}
						  {% endunless %}
					{% endfor %}';
				}
				$arrPrepareifCondition[] = ' true == isprimebCustTagFound ';
			}	
		}else{
			if( !empty( $arrCustTagsId ) ){
				$strCustTagsId = implode(",",$arrCustTagsId );
			}
			if( !empty( $strCustTagsId ) ){
				$strIfCustConditionTags  = '{% assign primebCustTags = "'.$strCustTagsId.'" | split: "," %}
					{% assign isprimebCustTagFound = false %}
					{% for tagName in primebCustTags %}
						  {% assign primebCustTag = tagName | downcase %}					
						{% if primebCustomerTag contains primebCustTag %}
							{% assign isprimebCustTagFound = true %}
						{% break %}
						  {% endif %}
					{% endfor %}';
				$arrPrepareifCondition[] = ' true == isprimebCustTagFound ';
			}		
		}
		// if( !empty( $strCustTagsId ) ){
		// 	$strIfCustConditionTags  = '{% assign primebCustTags = "'.$strCustTagsId.'" | split: "," %}
		// 		{% assign isprimebCustTagFound = false %}
		// 		{% for tagName in primebCustTags %}
		// 	      	{% assign primebCustTag = tagName | downcase %}					
		// 			{% if primebCustomerTag contains primebCustTag %}
		// 				{% assign isprimebCustTagFound = true %}
		// 	        {% break %}
		// 	      	{% endif %}
		// 	    {% endfor %}';
		// 	$arrPrepareifCondition[] = ' true == isprimebCustTagFound ';
		// }		
		
		$metacounter=0;
		$productitlecounter=0;
		$producttypecounter=0;
		$productpricecounter=0;
		$productvendorcounter=0;
		foreach( $displayrules as $index => $rule  ) {
			
			$ruleKey = key( $rule );

			switch( $ruleKey ) {
			    
				case 'saleprice' :
					
					if( strpos($rule['saleprice'], ',') !== false ) {
						$newsalerule = explode(",",$rule['saleprice']);
						$sale_start = $newsalerule[0];
						$sale_end = $newsalerule[1];
					}else{
						$sale_start = $rule['saleprice'];
						$sale_end = '100';
					}
					$strIfConditionSalePercentValue ='{% assign primebSalePercentValue = false %}
														{% if '.$sale_start.' <= SalePercent and SalePercent <= '.$sale_end.' %}
															{% assign primebSalePercentValue = true %}
														{% endif %}';
					// $arrPrepareifCondition[] = $sale_start.' <= SalePercent and SalePercent <= '.$sale_end.'';
					$arrPrepareifCondition[] = 'primebSalePercentValue';

			        break;

			   	case 'inventory':
			    		
		    		if( !empty( $rule['inventory'] ) ){
		    			$rule = $rule['inventory'];
		    			
		    			if( $rule['type'] == 1 ){

							$strIfConditionInventory .='{% assign primebProductInventory'.$index.' = false %}';
							if(isset($rule['start']) && $rule['start'] !='' ){
								$strIfConditionInventory .='{% if '.$rule['start'].' <= Inventory and Inventory <= '.$rule['value'].' %}
								{% assign primebProductInventory'.$index.' = true %}
								{% endif %}';
							}else{
								$strIfConditionInventory .='{% if 0 < Inventory and Inventory <= '.$rule['value'].' %}
								{% assign primebProductInventory'.$index.' = true %}
								{% endif %}';
							}	
		    				$rule = ' true == primebProductInventory'.$index.'';
		    				
						}else if( $rule['type'] == 2 ){
							$strIfConditionInventory .='{% assign primebInventoryTrack = false %}	
		    				{% if 0 < Inventory %}
		    				{% assign primebInventoryTrack = true %}
							{% endif %}';
		    				$rule = ' true == primebInventoryTrack ';
		    			}else if( $rule['type'] == 3 ){
		    				$rule = ' false == product.available ';
		    			}else if( $rule['type'] == 4 ){
		    				$strIfConditionInventory .='{% assign primebInventoryTrack = false %}	
		    				{% if 0 >= Inventory and product.available %}
		    				{% assign primebInventoryTrack = true %}
							{% endif %}';
							$rule = ' true == primebInventoryTrack ';
		    			}

		    			$arrPrepareifCondition[] = $rule;	
		    		}
			    	break;

			    case 'createddate':

			    	$rule['createddate'] = $rule['createddate']*86400;
			      	
			      	$arrPrepareifCondition[] = ' primebDateDiff > 0 and primebDateDiff <= '.$rule['createddate'].'';
					break;
				
				case 'publishdate':

					$rule['publishdate'] = $rule['publishdate']*86400;
						
						$arrPrepareifCondition[] = ' primebPublishDateDiff > 0 and primebPublishDateDiff <= '.$rule['publishdate'].'';
					break;

				case 'all_products':

					$arrPrepareifCondition[] = 'true';

				break;

				case 'title':
					
					// Starts With ........
					if($rule['title']['type'] == '1'){

						$arrPrepareifCondition[] = 'newnumberproducttitle'.$productitlecounter.' == newstringproducttitle'.$productitlecounter;
						$productitlecounter++;
				
					}
					// Ends With ........
					else if($rule['title']['type'] == '2'){

						$arrPrepareifCondition[] = ' newnumberproducttitle'.$productitlecounter.' == newstringproducttitle'.$productitlecounter;
						$productitlecounter++;

					}
					// Contains ........
					else if($rule['title']['type'] == '3'){

						$arrPrepareifCondition[] = ' primebProductTitle contains newstringproducttitle'.$productitlecounter;
						$productitlecounter++;
					}
					// Not Contains ........
					else{

						$arrPrepareifCondition[] = ' valueproducttitle'.$productitlecounter.' == true';
						//$productitlecounter;
						$productitlecounter++;
					}

				break;

				case 'product_price':
					// Is Equal To ........

					
					if($rule['product_price']['type'] == '1' ){

					//	print_r('condition1');
						$arrPrepareifCondition[] =  'primebProdPrices == newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Not Equal To ........
					else if($rule['product_price']['type'] == '2'){
					//	print_r('condition2');
						$arrPrepareifCondition[] = 'primebProdPrices != 0  and primebProdPrices != newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Is greter than ........
					else if($rule['product_price']['type'] == '3'){
					//	print_r('condition3');
						$arrPrepareifCondition[] = 'primebProdPrices > newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Is less than ........
					else if($rule['product_price']['type'] == '4'){
						//print_r('condition4');
						$arrPrepareifCondition[] = 'primebProdPrices != 0 and primebProdPrices < newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}

				break;

				case 'product_type':
					// Is Equal To ........
					if($rule['product_type']['type'] == '1' ){

						$arrPrepareifCondition[] =  'primebProductType == newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Not Equal To ........
					else if($rule['product_type']['type'] == '2'){

						$arrPrepareifCondition[] = ' primebProductType != newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Starts With ........
					else if($rule['product_type']['type'] == '3'){

						$arrPrepareifCondition[] = 'newnumberproducttype'.$producttypecounter.' == newstringproducttype'.$producttypecounter;
						$producttypecounter++;
				
					}
					// Ends With ........
					else if($rule['product_type']['type'] == '4'){

						$arrPrepareifCondition[] = ' newnumberproducttype == newstringproducttype'.$producttypecounter;
						$producttypecounter++;

					}
					// Contains ........
					else if($rule['product_type']['type'] == '5'){

						$arrPrepareifCondition[] = ' primebProductType contains newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Not Contains ........
					else{

						$arrPrepareifCondition[] = ' valueproducttype'.$producttypecounter.' == true';
						$producttypecounter;
						$producttypecounter++;
					}

				break;

				case 'page_type':

					$arrPageType = $rule['page_type'];
					if( !empty( $arrPageType ) ){
						$strPageType = implode(",",$arrPageType );
					}
					$strIfPageType = '{% assign primebPageType = "'.$strPageType.'" | split: "," %}
						{% assign isprimebpageFound = false %}
						{% for page in primebPageType %}
							{% assign primebRealpage = page | downcase %}					
							{% if primebTemplate contains primebRealpage %}
								{% assign isprimebpageFound = true %}
							{% break %}
							{% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimebpageFound ';

				break;

				case 'productmetafield':
					// Is Equal To ........
					if($rule['productmetafield']['metacondition'] == '1' ){

						// $arrPrepareifCondition[] = 'metastring == newstringmetaval';
						$arrPrepareifCondition[] = 'metastring'.$metacounter.' == newstringmetaval'.$metacounter;
						$metacounter++;
	
					}
					// Found ........
					else if($rule['productmetafield']['metacondition'] == '2'){

					

						if(isset($rule['productmetafield']['metaname'])){

							$arrPrepareifCondition[] =  ' primebProduct.metafields.'.$rule['productmetafield']['metaname'].' != blank';
						}

						if(isset($rule['productmetafield']['metaval']) && isset($rule['productmetafield']['metaname']) ){

							$arrPrepareifCondition[] =  'primebProduct.metafields.'.$rule['productmetafield']['metaname'].'.'.$rule['productmetafield']['metaval'].' != blank';
						}
						$metacounter++;
					}
					// not found...........
					else{
						

						if(isset($rule['productmetafield']['metaname'])){

							$arrPrepareifCondition[] =  'primebProduct.metafields.'.$rule['productmetafield']['metaname'].' == blank';
						}

						if(isset($rule['productmetafield']['metaval']) && isset($rule['productmetafield']['metaname']) ){

							$arrPrepareifCondition[] =  'primebProduct.metafields.'.$rule['productmetafield']['metaname'].'.'.$rule['productmetafield']['metaval'].' == blank';
						}
						$metacounter++;
					}

				break;

				case 'customer':
					if($rule['customer']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebCustId != null';
					}else if($rule['customer']['type'] == '2' ){
						$arrPrepareifCondition[] =  'primebCustId == null';
					} 
				break;

				case 'product_option':
					if($rule['product_option']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebVarient != 1';
					}else if($rule['product_option']['type'] == '2' ){
						$arrPrepareifCondition[] =  'primebVarient == 1';
					}  
				break;

				case 'product_vendor':
					// Is Equal To ........
					if($rule['product_vendor']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebProductVendor == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Not Equal To ........
					else if($rule['product_vendor']['type'] == '2'){
						$arrPrepareifCondition[] = 'primebProductVendor != newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Starts With ........
					else if($rule['product_vendor']['type'] == '3'){

						$arrPrepareifCondition[] = 'newnumbervendor'.$productvendorcounter.' == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
						
					}
					// Ends With ........
					else if($rule['product_vendor']['type'] == '4'){

						$arrPrepareifCondition[] = 'newnumbervendor'.$productvendorcounter.' == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
						
					}
					// Contains ........
					else if($rule['product_vendor']['type'] == '5'){
						
						$arrPrepareifCondition[] = 'primebProductVendor contains newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Not Contains ........
					else{
						$arrPrepareifCondition[] = ' valuevendor'.$productvendorcounter.' == true';
						$productvendorcounter++;
					}

				break;
				
				case 'weight':
					// Is Equal To ........
					if($rule['weight']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primebProductWeight == '. $rule['weight']['value'];
					}
					// Less Than ........
					else if($rule['weight']['type'] == '2'){
						$arrPrepareifCondition[] = 'primebProductWeight < '. $rule['weight']['value'];
					}
					// Greater Than ........
					else if($rule['weight']['type'] == '3'){
						$arrPrepareifCondition[] = 'primebProductWeight > '. $rule['weight']['value'];
					}
					else{
						$arrPrepareifCondition[] = 'false';
					}

					break;
					

			    }
		}

		if($displayprorules != ''){
			foreach( $displayprorules as $index => $prorule  ) {
			
				$proruleKey = key( $prorule );
	
				switch( $proruleKey ) {
					
					case 'order_count':
						// Is greter than ........
						if($prorule['order_count']['value'] == ''){

							// $arrPrepareifCondition[] = 'true';

						}elseif($prorule['order_count']['type'] == '1' ){
	
							$arrPrepareifCondition[] = 'OrderCount > '. $prorule['order_count']['value'];
							
						}
						// Is less than ........
						else if($prorule['order_count']['type'] == '2'){
							
							$arrPrepareifCondition[] = 'OrderCount < '. $prorule['order_count']['value'];
							
						}
	
					break;

					case 'recentorder_count':
						// Is greter than ........
						if($prorule['recentorder_count']['value'] == ''){

							// $arrPrepareifCondition[] = 'true';

						}elseif($prorule['recentorder_count']['type'] == '1' ){
	
							$arrPrepareifCondition[] = 'RecentSoldCount > '. $prorule['recentorder_count']['value'];
							
						}
						// Is less than ........
						else if($prorule['recentorder_count']['type'] == '2'){
							
							$arrPrepareifCondition[] = 'RecentSoldCount < '. $prorule['recentorder_count']['value'];
							
						}
	
					break;
					
				
				}

			}
		}

		if($shop_language != ''){
			$strIfLangType = '{% assign primebapplang = "'.$shop_language.'" | split: "," %}
						{% assign isprimeblangFound = false %}
						{% for lang in primebapplang %}
							{% assign primebReallang = lang | downcase %}					
							{% if primebLanguage contains primebReallang %}
								{% assign isprimeblangFound = true %}
							{% break %}
							{% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimeblangFound ';
		}
		

		if( !empty( $arrPrepareifCondition ) && is_array( $arrPrepareifCondition ) ){
			$arrPrepareifCondition = implode("".$condition."", $arrPrepareifCondition );

		}else{

			$arrPrepareifCondition = '';
		}
 
		
		if ( strpos($badge['title'], '{{VariantCount}}') !== false ) {


			$newTitle = $badge['title'];
			// $badge['title']="  {% assign VariantCount = product.variants.size %} {% if VariantCount == 1 %} {% assign VariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}  {% else %} {% assign VariantText = '.$newTitle.' %} {% endif %}";

			// $badge['title']="  {% assign VariantCount = product.variants.size %} {% if VariantCount == 1 %} {% assign VariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}  {% else %} {% assign VariantText = '.$newTitle.' %} {% endif %}";


			// $badge['title']=htmlspecialchars(" {% assign VariantCount = product.variants.size %}    
			// {% assign VariantText =    '".$newTitle."' | replace: '{{VariantCount}}', '' %}
			// {% assign noVariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}
			// {% assign VariantCount = product.variants.size %} 
			// {% capture newVariantText %}{{ VariantCount }}  {{ VariantText }}{% endcapture %}
			// {% if VariantCount == 1 %} {{ noVariantText }}  {% else %} {{ newVariantText }}  {% endif %} ");;
			
		}

		if(isset($badge['tooltip']) && $badge['tooltip'] != ''){
			$tooltip = "data-tippy-content= '".$badge['tooltip']."'";
		}else{
			$tooltip = '';
		}
		
		$defaultSize = "data-defaultsize= '".$badgestyle['textsize']."'";
		$MobileSize = "data-mobilesize= '".(isset( $badgestyle['textmobilesize'] ) ?  $badgestyle['textmobilesize'] :'0' )."'";
		$TabletSize = "data-tabletsize= '".(isset( $badgestyle['texttabletsize'] ) ?  $badgestyle['texttabletsize'] :'0' )."'";

		
		$primebDefaultHeight = "data-defaultheight= '".(isset( $display['height'] ) ?  $display['height'] :'50' )."'";
		$primebMobileHeight = "data-mobileheight= '".(isset( $badgestyle['imagemobilesize'] ) ?  $badgestyle['imagemobilesize'] :'0' )."'";
		$primebTabletHeight = "data-tabletheight= '".(isset( $badgestyle['imagetabletsize'] ) ?  $badgestyle['imagetabletsize'] :'0' )."'";

		$primebCountrySelected = "data-countryselected= '".(isset( $CountryClass ) ?  $customer_location :'' )."'";

		// $primebLangSelected = "data-langselected= '".(isset( $LangClass ) ?  $shop_language :'' )."'";

		// $realtitle = '{% assign OrderCount = product.metafields.prime.ordercount | default: 0 %}';
		$realtitle = '{% if product.metafields.prime.myordercount != blank %}
							{% assign OrderCount = product.metafields.prime.myordercount.ordercount | default: 0 %}
						    {% assign RecentSoldCount = product.metafields.prime.myordercount.recentcount | default: 0 %}
						{% else  %} 
							{% assign OrderCount = product.metafields.prime.ordercount | default: 0 %}
							{% assign RecentSoldCount = product.metafields.prime.recentcount | default: 0 %}
						{% endif %}';

		$downtitlesty = '';
		
		if($countdowntitle == 1){
			$downtitlesty = 'prime-counter';
		}
		$badgeidclass = 'primeb-'.$badge['productbadgeid'];

		if($badge['badge_type'] == 'image'){
			if( !isset($badge['imgsrc']) ) {
				$alttext = 'badge';
			} else {
				$pos = strrpos($badge['imgsrc'],"/");
				$alttextold = substr($badge['imgsrc'],$pos+1);
				$alttext = str_replace('.png', '', $alttextold);
			} 
		}else{
			$alttext = 'badge';
		}

		if (strpos($badge['title'], '<span class="primebloop"') !== false ) {
			$nodisplay = 'visibility:hidden;';
		}else{
			$nodisplay = '';
		}

		// $old = str_replace(array('{{ OrderCount }}', '{{OrderCount}}'),'{{product.metafields.prime.ordercount }}',$badge['title']);
		$buttonHtml = View('liquid.badgestyle', [
									'type'    		=> $badge['badge_type'],
									'image'			=> 'prime_'.$badge['productbadgeid'].'.png',
									'id'			=> $badge['productbadgeid'],
									'height' 		=> isset( $display['height'] ) ?  $display['height'].'px' :'50px' ,
									'badgestyle' 	=> $appearance['desktop']['badgeStyle'],
									'spanStyleleft' => isset( $appearance['desktop']['spanStyleleft'] ) ?  $appearance['desktop']['spanStyleleft'] :'' ,
									'textStyle'	 	=> isset( $appearance['desktop']['textStyle'] ) ?  $appearance['desktop']['textStyle'] :'',
									'spanStyle'		=> isset( $appearance['desktop']['spanStyle'] ) ?  $appearance['desktop']['spanStyle'] :'',
									'title'    		=> $badge['title'],
									'tooltip'    	=> $badge['tooltip'],
									'animation'		=> $animationType,
									'CountyClass' 	=> $CountryClass,
									// 'LangClass' 	=> $LangClass,
									'SelectedCountry' => $primebCountrySelected,
									// 'Selectedlang' => $primebLangSelected,
									'linktab'		=>   $badge['linktab'],
									'modalsize'		=>   $badge['modalsize'],
									'downtitlesty' => $downtitlesty,
									'primebadgeid' =>$badge['productbadgeid'],
									'badgeidclass' => $badgeidclass,
									'badgelink'      => $badge['badgelink'],
									'primebtooltip' => $tooltip,
									'primebDefaultSize' => $defaultSize,
									'primebMobileSize' => $MobileSize,
									'primebTabletSize' => $TabletSize,
									'primebDefaultHeight' => $primebDefaultHeight,
									'primebMobileHeight' => $primebMobileHeight,
									'primebTabletHeight' => $primebTabletHeight,
									'alttext'			 => $alttext,
									'nodisplay'			 => $nodisplay,
									'imgformat'			 => $badge['imageformat'],
									] )->render();


		if( !empty( $arrPrepareifCondition ) ){

			$arrPrepareifCondition = $strIfPageType.$strIfLangType.$realtitle.'{% if '.$arrPrepareifCondition.' %}';
			$arrPrepareifCondition = ' {% assign isStartbShowBadge = false %} '.$strIfProductCondition.$strIfCollectionCondition.$strIfConditionInventory.$strIfConditionprice.$strIfConditionmetafield.$strIfConditionvendor.$strIfConditiontype.$strIfConditionTags.$strIfCustConditionTags.$strIfConditionSalePercentValue.$arrPrepareifCondition;
			$arrPrepareifCondition .= '{% assign isStartbShowBadge = true %}';
			if( !empty( $excludeproduct ) ){
				$arrPrepareifCondition .= '{% assign primebExProductIds = "'.$excludeproduct.'" | split: "," %}
				{% assign primebExProductId = primebProduct.id | downcase %}
				{% if primebExProductIds contains primebExProductId %}
					{% assign isStartbShowBadge = false %}
				{% endif %}';
			}
			$arrPrepareifCondition  .= '{% endif %}';

			$arrPrepareifCondition .= '{% assign primebUTC 		= "now" | date: "%s" %}
			{% assign primebUTC 		= primebUTC | plus: 0 %}
			{% assign primebStartTime = '.$badge['starttime'].'  %}
			{% assign primebEndTime = '.$badge['endtime'].'  %}
			{% if primebStartTime <= primebUTC and primebEndTime >= primebUTC %}';	
			
			// print_R($badge['imgsrc']);exit;
			if($badge['badge_type'] == 'image'){

				if( !isset($badge['imgsrc']) ) {
					$arrPrepareifCondition .= "{% assign Image = 'prime_".$badge['productbadgeid'].".".$badge['imageformat']."'  %}";
					if( isset($display['height']) ) {
						$arrPrepareifCondition .= "{% assign ImageHeight = 'x".$display['height']."' %}";
					}else{
						$arrPrepareifCondition .= "{% assign ImageHeight = 'x22' %}";
					}
					if($badge['imageformat'] == 'svg'){
						$arrPrepareifCondition .= "{% assign Imagesize = Image | asset_url %}";
					}else{
						$arrPrepareifCondition .= "{% assign Imagesize = Image | asset_img_url: 'master' %}";
					}
					

				} else {

					$arrPrepareifCondition .= "{% assign Image = '".$badge['imgsrc']."'  %}";

					$arrPrepareifCondition .= "{% assign Imagesize = Image  %}";
				} 
			}
			
			$arrPrepareifCondition .= "{% if isStartbShowBadge == true %} 
				{% assign primebadgeGroup = '".$badge['badgegroup']."' %} 
					{% if primeGroup == primebadgeGroup %}
					{% assign primebBadgeCounter = primebBadgeCounter | plus: 1 %}
						".$buttonHtml."
					{% endif %}
				{% endif %}
			{% endif %}";
		}

		return array( 'global_variable'=>$strAssignGlobal ,'condition'=>$arrPrepareifCondition );
		
	}

}

?>