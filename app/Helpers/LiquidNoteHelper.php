<?php

namespace App\Helpers;

use Log;
 
class LiquidNoteHelper {

	public static function getLiquidProductCondition( $appearance, $note, $style_note ){
		$arrIfConditon = array(); 

		$strAssignGlobal     = "{% assign HideAssets = hideAssets | default: 0 %}{% if HideAssets != '1' %}{% render 'prime-css' %}{% endif %}";
		$strAssignGlobal     .= '
			{% assign primenProduct = product %}
			{% assign primenCustId = customer.id %}
			{% assign primenVarient = product.variants.size %}
			{%- assign primenmoneyformat = shop.money_format | split: "{{" | last | remove: "}}" | strip | strip_html -%}
			{% if primenmoneyformat == "amount_with_comma_separator" or primenmoneyformat == "amount_no_decimals_with_comma_separator" %}
				{% assign primenProdPrice = product.price | money_without_currency | remove: "." | replace: ",", "." %}
			{% else %} 
				{% assign primenProdPrice = product.price | money_without_currency | remove: "," %}
			{% endif %}
			{% assign primenCustomerTag = customer.tags | downcase %}
			{% assign primenPublishedDate = product.published_at | date: "%s" %}
			{% assign primenPublishDateDiff = "now" | date: "%s" | minus: primenPublishedDate %}
			{% assign primenTemplate = template | downcase %}
			{% assign primemetafield = primenProduct.metafields %}
			{% assign primenProductTitle = primenProduct.title | downcase %}
			{% assign primenProductType = primenProduct.type | downcase %}
			{% assign primenProductVendor = primenProduct.vendor | downcase %}
			{% assign primenShopifyTags = primenProduct.tags | downcase %}
			{% assign primenProductWeight = product.variants.first.weight %}
			{% assign primenUTC = "now" | date: "%s" %}
			{% assign primenMaxDisplaynote = primenMaxDisplaynote | default: 10 %}	
			{% assign primenCreatedDateSec = primenProduct.created_at | date: "%s" %}
			{% assign primenDateDiff = "now" | date: "%s" | minus: primenCreatedDateSec %}
			{% assign primenCollectionIds = primenProduct.collections | map: "id" %}			
			{% assign primennoteCounter = 1 %}
			{% assign SaleAmount    = 0 %}	
			{% assign VariantCount = product.variants.size %}
			{% assign SalePercent   = 0 %}
			{% assign primeGroup = primenGroup | default: "1" %}
			{% assign primenDesktop = primeGroup | append: "primenDesktop" %}
			{% assign primenotes = "primeNotes" %}
			{% assign primenactive = product.metafields.primen.noteon | default: 0 %}
            {% if primenProduct.price and primenProduct.compare_at_price %}
				{% assign SaleAmount = primenProduct.compare_at_price | minus: primenProduct.price %}	
				{% if SaleAmount > 0 %}
                	{% assign SalePercent = SaleAmount | times: 100.0 | divided_by: primenProduct.compare_at_price %}
           		{% endif %}
        	{% endif %}
        	{% assign Inventory = 0 %}
        	{% for productVariant in primenProduct.variants %}
                {% assign Inventory = Inventory | plus: productVariant.inventory_quantity %}			
            {% endfor %}' ;

		
		$arrPrepareifCondition    = [];
		
		
		$buttonHtml = View('liquid.notestyle', [
									'noteStyle'	 	=> $appearance['desktop']['noteStyle'],
									] )->render();
									
		// if( !empty( $arrPrepareifCondition ) ){
			
			$arrPrepareifCondition = "{% if primenactive == 1 %} ".$buttonHtml."{% endif %}";
		// }

		return array( 'global_variable'=>$strAssignGlobal ,'condition'=>$arrPrepareifCondition );

		
	}

}

?>