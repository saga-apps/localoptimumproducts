<?php

namespace App\Helpers;

use Log;
 
class LiquidHighlightHelper {

	public static function getLiquidProductCondition( $displayrules , $condition ,$appearance ,  $highlight, $display ,$style_highlight,$excludeproduct, $CountryClass, $customer_location,  $displayprorules, $countdowntitle, $shop_language, $LangClass ){
		$arrIfConditon = array(); 

		$TextPosition = '1';
		
		$OuterTextPos = 'prime-d-table-cell';
		$TextPos = 'prime-d-table-cell prime-text-left';

		$TextPosition = $style_highlight['highlighttextposition'];

		if($TextPosition == '1'){
			$OuterTextPos = 'prime-d-table-cell';
			$TextPos = 'prime-d-table-cell prime-text-left';
	
		}else{
			$OuterTextPos = 'prime-d-block';
			$TextPos = 'prime-d-block prime-text-center';
		}

		$space_padding = 'space2';
		$primehWrapper = 'prime-pt-2 prime-pl-2 prime-pr-2';
		$primehDesktop = 'prime-pb-2';

		$stylecol = $style_highlight['highlightstylecolumn'];

		$styletabcol = isset( $style_highlight['highlightstyletabcolumn'] ) ?  $style_highlight['highlightstyletabcolumn'] :$style_highlight['highlightstylecolumn'];

		$stylemobcol = isset( $style_highlight['highlightstylemobcolumn'] ) ?  $style_highlight['highlightstylemobcolumn'] :$style_highlight['highlightstylecolumn'];
		
		$space_padding = $style_highlight['highlightshape_padding'];

		if($space_padding == 'space1'){
			$primehWrapper = 'prime-pt-1 prime-pl-1 prime-pr-1';
			$primehDesktop = 'prime-pb-1';
	
		}else if($space_padding == 'space2'){
			$primehWrapper = 'prime-pt-2 prime-pl-2 prime-pr-2';
			$primehDesktop = 'prime-pb-2';

		}else if($space_padding == 'space3'){
			$primehWrapper = 'prime-pt-3 prime-pl-3 prime-pr-3';
			$primehDesktop = 'prime-pb-3';
	
		}else if($space_padding == 'space4'){
			$primehWrapper = 'prime-pt-4 prime-pl-4 prime-pr-4';
			$primehDesktop = 'prime-pb-4';
	
		}else if($space_padding == 'space5'){
			$primehWrapper = 'prime-pt-5 prime-pl-5 prime-pr-5';
			$primehDesktop = 'prime-pb-5';
	
		}

		if($stylecol == '1'){
			$primehDesktop .= ' prime-col-md-12';
		} else if($stylecol == '2'){
			$primehDesktop .= ' prime-col-md-6';
		} else if($stylecol == '3'){
			$primehDesktop .= ' prime-col-md-4';
		}else if($stylecol == '4'){
			$primehDesktop .= ' prime-col-md-3';
		}

		if($styletabcol == '1'){
			$primehDesktop .= ' prime-col-sm-12';
		} else if($styletabcol == '2'){
			$primehDesktop .= ' prime-col-sm-6';
		} else if($styletabcol == '3'){
			$primehDesktop .= ' prime-col-sm-4';
		}else if($styletabcol == '4'){
			$primehDesktop .= ' prime-col-sm-3';
		}

		if($stylemobcol == '1'){
			$primehDesktop .= ' prime-col-12';
		} else if($stylemobcol == '2'){
			$primehDesktop .= ' prime-col-6';
		} else if($stylemobcol == '3'){
			$primehDesktop .= ' prime-col-4';
		}else if($stylemobcol == '4'){
			$primehDesktop .= ' prime-col-3';
		}
	
		$strAssignGlobal     = "{% assign HideAssets = hideAssets | default: 0 %}{% if HideAssets != '1' %}{% render 'prime-css' %}{% endif %}";

		$strAssignGlobal     .= '
			{% assign primehProduct = product %}
			{% assign primehCustId = customer.id %}
			{% assign primehVarient = product.variants.size %}
			{%- assign primehmoneyformat = shop.money_format | split: "{{" | last | remove: "}}" | strip | strip_html -%}
			{% if primehmoneyformat == "amount_with_comma_separator" or primehmoneyformat == "amount_no_decimals_with_comma_separator" %}
				{% assign primehProdPrice = product.price | money_without_currency | remove: "." | replace: ",", "." %}
			{% else %} 
				{% assign primehProdPrice = product.price | money_without_currency | remove: "," %}
			{% endif %}
			{% assign primehProdPrice = product.price | divided_by: 100.00 %}
			{% assign primehCustomerTag = customer.tags | downcase %}
			{% assign primehPublishedDate = product.published_at | date: "%s" %}
			{% assign primehPublishDateDiff = "now" | date: "%s" | minus: primehPublishedDate %}
			{% assign primehTemplate = template | downcase %}
			{% assign primehLanguage = request.locale.iso_code %}
			{% assign primemetafield = primehProduct.metafields %}
			{% assign primehProductTitle = primehProduct.title | downcase %}
			{% assign primehProductType = primehProduct.type | downcase %}
			{% assign primehProductVendor = primehProduct.vendor | downcase %}
			{% assign primehShopifyTags = primehProduct.tags | downcase %}
			{% assign primehProductWeight = product.variants.first.weight %}
			{% assign primehUTC = "now" | date: "%s" %}
			{% assign primehstartTime = "'.$highlight['starttime'].'"  %}
			{% assign primehEndTime = "'.$highlight['endtime'].'"  %}
			{% assign primehMaxDisplayHighlight = primehMaxDisplayHighlight | default: 10 %}	
			{% assign primehCreatedDateSec = primehProduct.created_at | date: "%s" %}
			{% assign primehDateDiff = "now" | date: "%s" | minus: primehCreatedDateSec %}
			{% assign primehCollectionIds = primehProduct.collections | map: "id" | default: 0  %}			
			{% assign primehHighlightCounter = 1 %}
			{% assign SaleAmount    = 0 %}	
			{% assign VariantCount = product.variants.size %}
			{% assign SalePercent   = 0 %}
			{% assign primehighlightLinks = primehLinks | default: 0 %} 
			{% assign primeGroup = primehGroup | default: "1" %}
			{% assign primehDesktop = primeGroup | append: "primehDesktop" %}
			{% assign primeHighlights = primeGroup | append: "primeHighlights" %}
			{% assign ProductSKU = product.selected_or_first_available_variant.sku | default: 0 %}
            {% if primehProduct.price and primehProduct.compare_at_price %}
				{% assign SaleAmount = primehProduct.compare_at_price | minus: primehProduct.price %}	
				{% if SaleAmount > 0 %}
                	{% assign SalePercent = SaleAmount | times: 100.0 | divided_by: primehProduct.compare_at_price %}
           		{% endif %}
        	{% endif %}
        	{% assign Inventory = 0 %}
			{% assign Inventory2 = 0 %}
        	{% for productVariant in primehProduct.variants %}
				{% assign Inventory2 = productVariant.inventory_quantity | at_least: 0 %}
				{% assign Inventory = Inventory | plus: Inventory2 %}			
			{% endfor %}
			<div class="{{primeHighlights}} prime-container  {{primehOuterClass}}" style="{{ primehOuterStyle }}'.$appearance['desktop']['highlightStyle'].'">
			<div class="primehWrapper prime-pb-1 prime-row '.$primehWrapper.'"> ';

		$strIfConditionInventory = '';
		$strIfConditionSalePercentValue = '';
		$strIfProductCondition   = '';
		$strIfCollectionCondition   = '';
		$arrPrepareifCondition    = [];

		$strIfConditionTags       = '';
		$strIfCustConditionTags       = '';
		$strIfConditionmetafield='';
		$strIfConditionvendor='';
		$strIfConditiontype='';
		$strIfConditionprice='';
		$strIfPageType = '';
		$strIfLangType = '';
	
		$arrProductId = array_column( $displayrules, 'product' );

		$strProductId = implode(', ', array_map(function ($arrProductId) {

		if( empty( $arrProductId ) ) {
			return '';
		} 

			return  $arrProductId[0] ;

		}, $arrProductId ) );

		
		// prepare Productid condition;
		if( !empty( $strProductId ) ){

			if($highlight['shopprodmeta'] == 0){
				$strIfProductCondition = '{% assign primehProductIds = "'.$strProductId.'" | split: "," %}
				{% assign isprimehProductFound = false %}
				{% assign primehProductId = primehProduct.id | downcase %}
				{% if primehProductIds contains primehProductId %}
					{% assign isprimehProductFound = true %}
				{% endif %}';
			}else{
				$strIfProductCondition = '{% assign primehProductIds = shop.metafields.primeh.highlight_'.$highlight['producthighlightid'].' | split: "," %}
				{% assign isprimehProductFound = false %}
				{% assign primehProductId = primehProduct.id | downcase %}
				{% if primehProductIds contains primehProductId %}
					{% assign isprimehProductFound = true %}
				{% endif %}';
			}

			$arrPrepareifCondition[] = ' true == isprimehProductFound ';
		}

		$arrCollectionId = array_column( $displayrules, 'collection' );
		$collectioncount=0;
		foreach($arrCollectionId as $collectionkey){
			if(!empty( $collectionkey ) && isset($collectionkey['type'])){
				$strCollectionId = $collectionkey['value'];
				if( !empty( $strCollectionId ) ){
					$strIfCollectionCondition  .= '{% assign primehProductCollections = "'.$strCollectionId.'" | split: "," %}';
					if($collectionkey['type'] == '1'){
						$strIfCollectionCondition  .= '{% assign isprimehProductCollectionFound'.$collectioncount.' = false %}
						{% for collectionid in primehProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}
							{% for shopifyrealcollection in primehCollectionIds %}
								{% if shopifyrealcollection == collectionid2 %}
									{% assign isprimehProductCollectionFound'.$collectioncount.' = true %}
									{% break %}
								{% endif %}
							{% endfor %}
						{% endfor %}';
					}else{
						$strIfCollectionCondition  .= '{% assign break_loop = false %}{% assign isprimehProductCollectionFound'.$collectioncount.' = false %}
						{% if primehCollectionIds == 0 %}
							{% assign isprimehProductCollectionFound'.$collectioncount.' = true %}
						{% endif %}
						{% for collectionid in primehProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}
							{% for shopifyrealcollection in primehCollectionIds %}				
								{% if shopifyrealcollection != collectionid2 %}
									{% assign isprimehProductCollectionFound'.$collectioncount.' = true %}
								{% else %}
									{% assign isprimehProductCollectionFound'.$collectioncount.' = false %}
									{% assign break_loop = true %}
									{% break %}
							  	{% endif %}
							{% endfor %}
							{% if break_loop %}
								{% break %}
							{% endif %}
						{% endfor %}';
					}
					$arrPrepareifCondition[] = ' true == isprimehProductCollectionFound'.$collectioncount.' ';
				}	
			}else{
				if( !empty( $collectionkey ) ){
					$strCollectionId = $arrCollectionId[0][0];
				}
				if( !empty( $strCollectionId ) ){
					$strIfCollectionCondition  .= '{% assign primehProductCollections = "'.$strCollectionId.'" | split: "," %}
						{% assign isprimehProductCollectionFound'.$collectioncount.' = false %}
						{% for collectionid in primehProductCollections %}
							{% assign collectionid2 = collectionid | plus: 0 %}			
							{% if primehCollectionIds contains collectionid2 %}
								{% assign isprimehProductCollectionFound'.$collectioncount.' = true %}
							{% break %}
							  {% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimehProductCollectionFound'.$collectioncount.'';
				}		
			}
			$collectioncount++;
		}

		$arrproductmetafield = array_column( $displayrules, 'productmetafield' );
		if( !(empty($arrproductmetafield)) ){

			for($i=0; $i< count($arrproductmetafield); $i++) {
				
				$strIfConditionmetafield .='{% assign newstringmetaval'.$i.' = "'.$arrproductmetafield[$i]['metaval'].'" | downcase %} {% assign metastring'.$i.' = primehProduct.metafields.'.$arrproductmetafield[$i]['metaname'].' | downcase %}';
			}
		}

		$arrproductproducttype = array_column( $displayrules, 'title' );
		if( !(empty($arrproductproducttype)) ){

			for($i=0; $i< count($arrproductproducttype); $i++) {

				$strIfConditiontype .='{% assign newstringproducttitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | downcase %} ';

				if($arrproductproducttype[$i]['type'] == '1'){

					$strIfConditiontype .='{% assign valuesizeproductitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %} 
					{% assign newnumberproducttitle'.$i.' = primehProductTitle | slice: 0,valuesizeproductitle'.$i.' %}';

				} else if ($arrproductproducttype[$i]['type'] == '2'){

					$strIfConditiontype .='{% assign valuesizeproducttitle'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %}{% assign mulproducttitle'.$i.' = valuesizeproducttitle'.$i.' | times:2 %}
					{% assign newminusproducttitle'.$i.' = valuesizeproducttitle'.$i.' | minus:mulproducttitle'.$i.' %}
				{% assign newnumberproducttitle'.$i.' = primehProductTitle | slice: newminusproducttitle'.$i.', valuesizeproducttitle'.$i.' %}';
				
				} else if ($arrproductproducttype[$i]['type'] == '4'){

					$strIfConditiontype .='{% unless primehProductTitle contains newstringproducttitle'.$i.' %}{% assign valueproducttitle'.$i.' = true %}{% endunless %}';
				}
			
			}

		}

		$arrproductproductprice = array_column( $displayrules, 'product_price' );

		if( !(empty($arrproductproductprice)) ){

			for($i=0; $i< count($arrproductproductprice); $i++) {

				$strIfConditionprice .='{% assign newstringproductprice'.$i.' = "'.$arrproductproductprice[$i]['value'].'" | downcase %}
				{% assign primehProdPrices = primehProdPrice | plus: 0 %}
				{% assign newstringproductprices'.$i.' =  newstringproductprice'.$i.' | plus: 0 %} ';

			}

		}

		$arrproductproducttype = array_column( $displayrules, 'product_type' );

		if( !(empty($arrproductproducttype)) ){

			for($i=0; $i< count($arrproductproducttype); $i++) {

				$strIfConditiontype .='{% assign newstringproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | downcase %} ';

				if($arrproductproducttype[$i]['type'] == '3'){

					$strIfConditiontype .='{% assign valuesizeproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %} 
					{% assign newnumberproducttype'.$i.' = primehProductType | slice: 0,valuesizeproducttype'.$i.' %}';

				} else if ($arrproductproducttype[$i]['type'] == '4'){

					$strIfConditiontype .='{% assign valuesizeproducttype'.$i.' = "'.$arrproductproducttype[$i]['value'].'" | size %}{% assign mulproducttype'.$i.' = valuesizeproducttype'.$i.' | times:2 %}
					{% assign newminusproducttype'.$i.' = valuesizeproducttype'.$i.' | minus:mulproducttype'.$i.' %}
				{% assign newnumberproducttype'.$i.' = primehProductType | slice: newminusproducttype'.$i.', valuesizeproducttype'.$i.' %}
				';
				
				} else if ($arrproductproducttype[$i]['type'] == '6'){

					$strIfConditiontype .='{% unless primehProductType contains newstringproducttype'.$i.' %}{% assign valueproducttype'.$i.' = true %}{% endunless %}';
				}
			
			}

		}

		$arrproductvendor= array_column( $displayrules, 'product_vendor' );

		if( !(empty($arrproductvendor)) ){

			for($i=0; $i< count($arrproductvendor); $i++) {
				
				$strIfConditionvendor .='{% assign newstringvendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | downcase %}';

				if($arrproductvendor[$i]['type'] == '3'){
					
					$strIfConditionvendor .='{% assign valuesizevendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | size %}
					{% assign newnumbervendor'.$i.' = primehProductVendor | slice: 0,valuesizevendor'.$i.' %}';
				
				} else if ($arrproductvendor[$i]['type'] == '4'){

					$strIfConditionvendor .='{% assign valuesizevendor'.$i.' = "'.$arrproductvendor[$i]['value'].'" | size %}
					{% assign mulvendor'.$i.' = valuesizevendor'.$i.' | times:2 %}
					{% assign newminusvendor'.$i.' = valuesizevendor'.$i.' | minus:mulvendor'.$i.' %}
					{% assign newnumbervendor'.$i.' = primehProductVendor | slice: newminusvendor'.$i.', valuesizevendor'.$i.' %}';

				} else if ($arrproductvendor[$i]['type'] == '6'){

					$strIfConditionvendor .='{% unless primehProductVendor contains newstringvendor'.$i.' %}{% assign valuevendor'.$i.' = true %}{% endunless %}';

				}
			}

		}
		
		// $arrTagsId = array_column( $displayrules, 'tags' );
		// if( !empty( $arrTagsId ) ){
		// 	$strTagsId = implode(",",$arrTagsId );
		// 	$strTagsId = str_replace('"','&quot;',$strTagsId);
		// }	
		// if( !empty( $strTagsId ) ){
		// 	$strIfConditionTags = '{% assign primehProductTags = "'.$strTagsId.'" | split: "," %}
		// 		{% assign isprimehProductTagFound = false %}
		// 		{% for tagName in primehProductTags %}
		// 	      	{% assign primehProductTag = tagName | downcase %}					
		// 			{% if primehShopifyTags contains primehProductTag %}
		// 				{% assign isprimehProductTagFound = true %}
		// 	        {% break %}
		// 	      	{% endif %}
		// 	    {% endfor %}';
		// 	$arrPrepareifCondition[] = ' true == isprimehProductTagFound ';
		// }

		$arrTagsId = array_column( $displayrules, 'tags' );
		$tagcount=0;
		foreach($arrTagsId as $tagkey){
			if(!empty( $tagkey ) && isset($tagkey['type'])){
				$strTagsId = $tagkey['value'];
				if( !empty( $strTagsId ) ){
					$strIfConditionTags  .= '{% assign primehProductTags = "'.$strTagsId.'" | split: "," %}';
					if($tagkey['type'] == '1'){
						$strIfConditionTags  .= '{% assign isprimehProductTagFound'.$tagcount.' = false %}
						{% for tagName in primehProductTags %}
							{% for shopifyrealtag in product.tags %}
								{% if shopifyrealtag == tagName %}
									{% assign isprimehProductTagFound'.$tagcount.' = true %}
									{% break %}
								{% endif %}
							{% endfor %}
						{% endfor %}';
						
					}else{
						$strIfConditionTags  .= '{% assign primehProductTags = "'.$strTagsId.'" | split: "," %}';
						$strIfConditionTags  .= '{% assign isprimehProductTagFound'.$tagcount.' = false %}
						{% for tagName in primehProductTags %}
							{% for shopifyrealtag in product.tags %}
								{% assign primehProductTag = tagName | downcase %}					
								{% if shopifyrealtag != tagName %}
									{% assign isprimehProductTagFound'.$tagcount.' = true %}
								{% else %}
									{% assign isprimehProductTagFound'.$tagcount.' = false %}
									{% break %}
							  	{% endif %}
							{% endfor %}
						{% endfor %}';
					}
					$arrPrepareifCondition[] = ' true == isprimehProductTagFound'.$tagcount.' ';
				}	
				// print_r($strIfConditionTags);exit;
			}else{
				if( !empty( $tagkey ) ){
					$strTagsId = implode(",",$arrTagsId );
				}
				if( !empty( $strTagsId ) ){
					$strIfConditionTags  .= '{% assign primehProductTags = "'.$strTagsId.'" | split: "," %}
						{% assign isprimehProductTagFound'.$tagcount.' = false %}
						{% for tagName in primehProductTags %}
							  {% assign primehProductTag = tagName | downcase %}					
							{% if primehShopifyTags contains primehProductTag %}
								{% assign isprimehProductTagFound'.$tagcount.' = true %}
							{% break %}
							  {% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimehProductTagFound'.$tagcount.'';
				}		
			}
			$tagcount++;
		}


		$arrCustTagsId = array_column( $displayrules, 'customer_tag' );
		// echo "<pre>";print_r($arrCustTagsId[0]);exit;
		if(!empty( $arrCustTagsId ) && isset($arrCustTagsId[0]['type'])){
			$strCustTagsId = $arrCustTagsId[0]['value'];
			if( !empty( $strCustTagsId ) ){
				$strIfCustConditionTags  = '{% assign primehCustTags = "'.$strCustTagsId.'" | split: "," %}';
				if($arrCustTagsId[0]['type'] == '1'){
					$strIfCustConditionTags  .= '{% assign isprimehCustTagFound = false %}{% for tagName in primehCustTags %}
						  {% assign primehCustTag = tagName | downcase %}					
						{% if primehCustomerTag contains primehCustTag %}
							{% assign isprimehCustTagFound = true %}
							{% break %}
						  {% endif %}
					{% endfor %}';
				}else{
					$strIfCustConditionTags  .= '{% assign isprimehCustTagFound = false %}{% for tagName in primehCustTags %}
						  {% assign primehCustTag = tagName | downcase %}					
						{% unless primehCustomerTag contains primehCustTag %}
							{% assign isprimehCustTagFound = true %}
							{% break %}
						  {% endunless %}
					{% endfor %}';
				}
				$arrPrepareifCondition[] = ' true == isprimehCustTagFound ';
			}	
		}else{
			if( !empty( $arrCustTagsId ) ){
				$strCustTagsId = implode(",",$arrCustTagsId );
			}
			if( !empty( $strCustTagsId ) ){
				$strIfCustConditionTags  = '{% assign primehCustTags = "'.$strCustTagsId.'" | split: "," %}
					{% assign isprimehCustTagFound = false %}
					{% for tagName in primehCustTags %}
						  {% assign primehCustTag = tagName | downcase %}					
						{% if primehCustomerTag contains primehCustTag %}
							{% assign isprimehCustTagFound = true %}
						{% break %}
						  {% endif %}
					{% endfor %}';
				$arrPrepareifCondition[] = ' true == isprimehCustTagFound ';
			}		
		}

		// $arrCustTagsId = array_column( $displayrules, 'customer_tag' );
		// if( !empty( $arrCustTagsId ) ){
		// 	$strCustTagsId = implode(",",$arrCustTagsId );
		// }		 
		// if( !empty( $strCustTagsId ) ){
		// 	$strIfCustConditionTags = '{% assign primehCustTags = "'.$strCustTagsId.'" | split: "," %}
		// 		{% assign isprimehCustTagFound = false %}
		// 		{% for tagName in primehCustTags %}
		// 	      	{% assign primehCustTag = tagName | downcase %}					
		// 			{% if primehCustomerTag contains primehCustTag %}
		// 				{% assign isprimehCustTagFound = true %}
		// 	        {% break %}
		// 	      	{% endif %}
		// 	    {% endfor %}';
		// 	$arrPrepareifCondition[] = ' true == isprimehCustTagFound ';
		// }		
		
		$metacounter=0;
		$productitlecounter=0;
		$producttypecounter=0;
		$productpricecounter=0;
		$productvendorcounter=0;
		foreach( $displayrules as $index => $rule  ) {
			
			$ruleKey = key( $rule );
			// print_r($ruleKey);

			switch( $ruleKey ) {
			    
				case 'saleprice' :
					
					if( strpos($rule['saleprice'], ',') !== false ) {
						$newsalerule = explode(",",$rule['saleprice']);
						$sale_start = $newsalerule[0];
						$sale_end = $newsalerule[1];
					}else{
						$sale_start = $rule['saleprice'];
						$sale_end = '100';
					}

					$strIfConditionSalePercentValue ='{% assign primehSalePercentValue = false %}
														{% if '.$sale_start.' <= SalePercent and SalePercent <= '.$sale_end.' %}
															{% assign primehSalePercentValue = true %}
														{% endif %}';
					// $arrPrepareifCondition[] = $sale_start.' <= SalePercent and SalePercent <= '.$sale_end.'';
					$arrPrepareifCondition[] = 'primehSalePercentValue';
			        
			        break;

			   	case 'inventory':
			    		
		    		if( !empty( $rule['inventory'] ) ){
		    			$rule = $rule['inventory'];
		    			
		    			if( $rule['type'] == 1 ){

		    				$strIfConditionInventory .='{% assign primehProductInventory'.$index.' = false %}';
							if(isset($rule['start']) && $rule['start'] !='' ){
								$strIfConditionInventory .='{% if '.$rule['start'].' <= Inventory and Inventory <= '.$rule['value'].' %}
								{% assign primehProductInventory'.$index.' = true %}
								{% endif %}';
							}else{
								$strIfConditionInventory .='{% if 0 < Inventory and Inventory <= '.$rule['value'].' %}
								{% assign primehProductInventory'.$index.' = true %}
								{% endif %}';
							}	
		    				$rule = ' true == primehProductInventory'.$index.'';
		    				
						}else if( $rule['type'] == 2 ){
							$strIfConditionInventory .='{% assign primehInventoryTrack = false %}	
		    				{% if 0 < Inventory %}
		    				{% assign primehInventoryTrack = true %}
							{% endif %}';
		    				$rule = ' true == primehInventoryTrack ';
		    				// $rule = ' true == product.available ';
		    			}else if( $rule['type'] == 3 ){
		    				$rule = ' false == product.available ';
		    			}else if( $rule['type'] == 4 ){
		    				$strIfConditionInventory .='{% assign primehInventoryTrack = false %}	
		    				{% if 0 >= Inventory and product.available %}
		    				{% assign primehInventoryTrack = true %}
							{% endif %}';
							$rule = ' true == primehInventoryTrack ';
		    			}

		    			$arrPrepareifCondition[] = $rule;	
		    		}
			    	break;

			    case 'createddate':

			    	$rule['createddate'] = $rule['createddate']*86400;
			      	
			      	$arrPrepareifCondition[] = ' primehDateDiff > 0 and primehDateDiff <= '.$rule['createddate'].'';
					break;

				case 'publishdate':

					$rule['publishdate'] = $rule['publishdate']*86400;
						
						$arrPrepareifCondition[] = ' primehPublishDateDiff > 0 and primehPublishDateDiff <= '.$rule['publishdate'].'';
					break;

				case 'all_products':

					$arrPrepareifCondition[] = 'true';

				break;

				case 'title':
					
					// Starts With ........
					if($rule['title']['type'] == '1'){

						$arrPrepareifCondition[] = 'newnumberproducttitle'.$productitlecounter.' == newstringproducttitle'.$productitlecounter;
						$productitlecounter++;
				
					}
					// Ends With ........
					else if($rule['title']['type'] == '2'){

						$arrPrepareifCondition[] = ' newnumberproducttitle'.$productitlecounter.' == newstringproducttitle'.$productitlecounter;
						$productitlecounter++;

					}
					// Contains ........
					else if($rule['title']['type'] == '3'){

						$arrPrepareifCondition[] = ' primehProductTitle contains newstringproducttitle'.$productitlecounter;
						$productitlecounter++;
					}
					// Not Contains ........
					else{

						$arrPrepareifCondition[] = ' valueproducttitle'.$productitlecounter.' == true';
						//$productitlecounter;
						$productitlecounter++;
					}

				break;

				case 'product_price':
					// Is Equal To ........

					
					if($rule['product_price']['type'] == '1' ){

					//	print_r('condition1');
						$arrPrepareifCondition[] =  'primehProdPrices == newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Not Equal To ........
					else if($rule['product_price']['type'] == '2'){
					//	print_r('condition2');
						$arrPrepareifCondition[] = 'primehProdPrices != 0  and primehProdPrices != newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Is greter than ........
					else if($rule['product_price']['type'] == '3'){
					//	print_r('condition3');
						$arrPrepareifCondition[] = 'primehProdPrices > newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}
					// Is less than ........
					else if($rule['product_price']['type'] == '4'){
						//print_r('condition4');
						$arrPrepareifCondition[] = 'primehProdPrices != 0 and primehProdPrices < newstringproductprices'.$productpricecounter;
						$productpricecounter++;
						
					}

				break;

				case 'product_type':
					// Is Equal To ........
					if($rule['product_type']['type'] == '1' ){

						$arrPrepareifCondition[] =  'primehProductType == newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Not Equal To ........
					else if($rule['product_type']['type'] == '2'){

						$arrPrepareifCondition[] = ' primehProductType != newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Starts With ........
					else if($rule['product_type']['type'] == '3'){

						$arrPrepareifCondition[] = 'newnumberproducttype'.$producttypecounter.' == newstringproducttype'.$producttypecounter;
						$producttypecounter++;
				
					}
					// Ends With ........
					else if($rule['product_type']['type'] == '4'){

						$arrPrepareifCondition[] = ' newnumberproducttype'.$producttypecounter.' == newstringproducttype'.$producttypecounter;
						$producttypecounter++;

					}
					// Contains ........
					else if($rule['product_type']['type'] == '5'){

						$arrPrepareifCondition[] = ' primehProductType contains newstringproducttype'.$producttypecounter;
						$producttypecounter++;
					}
					// Not Contains ........
					else{

						$arrPrepareifCondition[] = ' valueproducttype'.$producttypecounter.' == true';
						$producttypecounter;
						$producttypecounter++;
					}

				break;

				case 'page_type':

					$arrPageType = $rule['page_type'];
					if( !empty( $arrPageType ) ){
						$strPageType = implode(",",$arrPageType );
					}
					$strIfPageType = '{% assign primehPageType = "'.$strPageType.'" | split: "," %}
						{% assign isprimehpageFound = false %}
						{% for page in primehPageType %}
							{% assign primehRealpage = page | downcase %}					
							{% if primehTemplate contains primehRealpage %}
								{% assign isprimehpageFound = true %}
							{% break %}
							{% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimehpageFound ';

				break;

				case 'productmetafield':
					// Is Equal To ........
					if($rule['productmetafield']['metacondition'] == '1' ){

						// $arrPrepareifCondition[] = 'metastring == newstringmetaval';
						$arrPrepareifCondition[] = 'metastring'.$metacounter.' == newstringmetaval'.$metacounter;
						$metacounter++;
	
					}
					// Found ........
					else if($rule['productmetafield']['metacondition'] == '2'){

					

						if(isset($rule['productmetafield']['metaname'])){

							$arrPrepareifCondition[] =  ' primehProduct.metafields.'.$rule['productmetafield']['metaname'].' != blank';
						}

						if(isset($rule['productmetafield']['metaval']) && isset($rule['productmetafield']['metaname']) ){

							$arrPrepareifCondition[] =  'primehProduct.metafields.'.$rule['productmetafield']['metaname'].'.'.$rule['productmetafield']['metaval'].' != blank';
						}
						$metacounter++;
					}
					// not found...........
					else{
						

						if(isset($rule['productmetafield']['metaname'])){

							$arrPrepareifCondition[] =  'primehProduct.metafields.'.$rule['productmetafield']['metaname'].' == blank';
						}

						if(isset($rule['productmetafield']['metaval']) && isset($rule['productmetafield']['metaname']) ){

							$arrPrepareifCondition[] =  'primehProduct.metafields.'.$rule['productmetafield']['metaname'].'.'.$rule['productmetafield']['metaval'].' == blank';
						}
						$metacounter++;
					}

				break;

				case 'customer':
					if($rule['customer']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primehCustId != null';
					}else if($rule['customer']['type'] == '2' ){
						$arrPrepareifCondition[] =  'primehCustId == null';
					} 
				break;

				case 'product_option':
					if($rule['product_option']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primehVarient != 1';
					}else if($rule['product_option']['type'] == '2' ){
						$arrPrepareifCondition[] =  'primehVarient == 1';
					}  
				break;

				case 'product_vendor':
					// Is Equal To ........
					if($rule['product_vendor']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primehProductVendor == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Not Equal To ........
					else if($rule['product_vendor']['type'] == '2'){
						$arrPrepareifCondition[] = 'primehProductVendor != newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Starts With ........
					else if($rule['product_vendor']['type'] == '3'){

						$arrPrepareifCondition[] = 'newnumbervendor'.$productvendorcounter.' == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
						
					}
					// Ends With ........
					else if($rule['product_vendor']['type'] == '4'){

						$arrPrepareifCondition[] = 'newnumbervendor'.$productvendorcounter.' == newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
						
					}
					// Contains ........
					else if($rule['product_vendor']['type'] == '5'){
						
						$arrPrepareifCondition[] = 'primehProductVendor contains newstringvendor'.$productvendorcounter;
						$productvendorcounter++;
					}
					// Not Contains ........
					else{
						$arrPrepareifCondition[] = ' valuevendor'.$productvendorcounter.' == true';
						$productvendorcounter++;
					}

				break;
				
				case 'weight':
					// Is Equal To ........
					if($rule['weight']['type'] == '1' ){
						$arrPrepareifCondition[] =  'primehProductWeight == '. $rule['weight']['value'];
					}
					// Less Than ........
					else if($rule['weight']['type'] == '2'){
						$arrPrepareifCondition[] = 'primehProductWeight < '. $rule['weight']['value'];
					}
					// Greater Than ........
					else if($rule['weight']['type'] == '3'){
						$arrPrepareifCondition[] = 'primehProductWeight > '. $rule['weight']['value'];
					}
					else{
						$arrPrepareifCondition[] = 'false';
					}

					break;
					

			    }
		}

		if($displayprorules != ''){
			foreach( $displayprorules as $index => $prorule  ) {
			
				$proruleKey = key( $prorule );
	
				switch( $proruleKey ) {
					
					case 'order_count':
						// Is greter than ........
						if($prorule['order_count']['value'] == ''){

							// $arrPrepareifCondition[] = 'true';

						}elseif($prorule['order_count']['type'] == '1' ){
	
							$arrPrepareifCondition[] = 'OrderCount > '. $prorule['order_count']['value'];
							
						}
						// Is less than ........
						else if($prorule['order_count']['type'] == '2'){
							
							$arrPrepareifCondition[] = 'OrderCount < '. $prorule['order_count']['value'];
							
						}
	
					break;

					case 'recentorder_count':
						// Is greter than ........
						if($prorule['recentorder_count']['value'] == ''){

							// $arrPrepareifCondition[] = 'true';

						}elseif($prorule['recentorder_count']['type'] == '1' ){
	
							$arrPrepareifCondition[] = 'RecentSoldCount > '. $prorule['recentorder_count']['value'];
							
						}
						// Is less than ........
						else if($prorule['recentorder_count']['type'] == '2'){
							
							$arrPrepareifCondition[] = 'RecentSoldCount < '. $prorule['recentorder_count']['value'];
							
						}
	
					break;
				
				}

			}
		}

		if($shop_language != ''){
			$strIfLangType = '{% assign primehapplang = "'.$shop_language.'" | split: "," %}
						{% assign isprimehlangFound = false %}
						{% for lang in primehapplang %}
							{% assign primehReallang = lang | downcase %}					
							{% if primehLanguage contains primehReallang %}
								{% assign isprimehlangFound = true %}
							{% break %}
							{% endif %}
						{% endfor %}';
					$arrPrepareifCondition[] = ' true == isprimehlangFound ';
		}

		if( !empty( $arrPrepareifCondition ) && is_array( $arrPrepareifCondition ) ){
			$arrPrepareifCondition = implode("".$condition."", $arrPrepareifCondition );

		}else{

			$arrPrepareifCondition = '';
		}

		if ( strpos($highlight['title'], '{{VariantCount}}') !== false ) {

			
			$newTitle = $highlight['title'];
			// $highlight['title']=' {% assign VariantText = '.$newTitle.' %} {% assign VariantCount = product.variants.size %} {% if VariantCount == 1 %} No color available  {% else %} {{ VariantCount }} colors available {% endif %} ';
			

			// $highlight['title']=htmlspecialchars(" {% assign VariantCount = product.variants.size %}    
			// {% assign VariantText =    '".$newTitle."' | replace: '{{VariantCount}}', '' %}
			// {% assign noVariantText =   '".$newTitle."' | replace: '{{VariantCount}}', 'No' %}
			// {% assign VariantCount = product.variants.size %} 
			// {% capture newVariantText %}{{ VariantCount }}  {{ VariantText }}{% endcapture %}
			// {% if VariantCount == 1 %} {{ noVariantText }}  {% else %} {{ newVariantText }}  {% endif %} ");

		}

		if( !isset($highlight['image']) ) {
			$highlight['image']= env('AWS_PATH').'h'.$highlight['producthighlightid'];
			$alttext = 'highlight';
		} else {
			$highlight['image']= $highlight['image'];
			$pos = strrpos($highlight['image'],"/");
			$alttextold = substr($highlight['image'],$pos+1);
			$alttext = str_replace('.png', '', $alttextold);
		} 
		
		if(isset($highlight['tooltip']) && $highlight['tooltip'] != ''){
			$tooltip = "data-tippy-content= '".$highlight['tooltip']."'";
		}else{
			$tooltip = '';
		}
		
		$primehCountrySelected = "data-countryselectedh= '".(isset( $CountryClass ) ?  $customer_location :'' )."'";

		if(isset( $display['height'] )){
			$imgStyle = "height:".$display['height']."px; width:".$display['height']."px; max-height:".$display['height']."px; max-width:".$display['height']."px;";
		}else{
			$imgStyle = "height:30px; width:30px; max-height:30px; max-width:30px;";
		}

		// $realtitle = '{% assign OrderCount = product.metafields.prime.ordercount | default: 0 %}';
		$realtitle = '{% if product.metafields.prime.myordercount != blank %}
							{% assign OrderCount = product.metafields.prime.myordercount.ordercount | default: 0 %}
						    {% assign RecentSoldCount = product.metafields.prime.myordercount.recentcount | default: 0 %}
						{% else  %} 
							{% assign OrderCount = product.metafields.prime.ordercount | default: 0 %}
							{% assign RecentSoldCount = product.metafields.prime.recentcount | default: 0 %}
						{% endif %}';

		// $old = str_replace(array('{{ OrderCount }}', '{{OrderCount}}'),'{{product.metafields.prime.ordercount }}',$highlight['title']);
		$downtitlesty = '';
		if($countdowntitle == 1){
			$downtitlesty = 'prime-counter';
		}

		if (strpos($highlight['title'], '<span class="primehloop"') !== false ) {
			$nodisplay = 'visibility:hidden;';
		}else{
			$nodisplay = '';
		}

		$highlightidclass = 'primeh-'.$highlight['producthighlightid'];
		$buttonHtml = View('liquid.highlightstyle', [
									// 'height' 		=> isset( $display['height'] ) ?  $display['height'].'px' :'25px' ,
									'highlightstyle' 	=> $appearance['desktop']['highlightStyle'],
									'textStyle'	 	=> $appearance['desktop']['textStyle'],
									'image'  			=> $highlight['image'],
									'imgStyle'  		=> $imgStyle,
									'title'				=> $highlight['title'],
									// 'title'    			=> $old,
									'tooltip'    	=> $highlight['tooltip'],
									'primehDesktop'    	=> $primehDesktop,

									'OuterTextPos'   => $OuterTextPos,
									'TextPos'    	=> $TextPos,
									'primehtooltip' => $tooltip,
									
									'CountyClass' 	=> $CountryClass,
									'SelectedCountry' => $primehCountrySelected,
									'downtitlesty' => $downtitlesty,
									'modalsize'		=>   $highlight['modalsize'],
									'primehighlightid' =>$highlight['producthighlightid'],
									'highlightidclass' => $highlightidclass,
									'linktab'      => $highlight['linktab'],
									'highlightlink' => $highlight['highlightlink'],
									'alttext'			 => $alttext,
									'nodisplay'			 => $nodisplay,
									] )->render();
		 

		if( !empty( $arrPrepareifCondition ) ){

			$arrPrepareifCondition = $strIfPageType.$strIfLangType.$realtitle.'{% if '.$arrPrepareifCondition.' %}';
			$arrPrepareifCondition = ' {% assign isStartbShowHighlight = false %} '.$strIfProductCondition.$strIfCollectionCondition.$strIfConditionInventory.$strIfConditionprice.$strIfConditionmetafield.$strIfConditionvendor.$strIfConditiontype.$strIfConditionTags.$strIfCustConditionTags.$strIfConditionSalePercentValue.$arrPrepareifCondition;
			$arrPrepareifCondition .= '{% assign isStartbShowHighlight = true %}';

			if( !empty( $excludeproduct ) ){
				$arrPrepareifCondition .= '{% assign primehExProductIds = "'.$excludeproduct.'" | split: "," %}
				{% assign primehExProductId = primehProduct.id | downcase %}
				{% if primehExProductIds contains primehExProductId %}
					{% assign isStartbShowHighlight = false %}
				{% endif %}';
			}

			$arrPrepareifCondition  .= '{% endif %}';

			$arrPrepareifCondition .= '{% assign primehUTC 		= "now" | date: "%s" %}
			{% assign primehUTC 		= primehUTC | plus: 0 %}
			{% assign primehStartTime = '.$highlight['starttime'].'  %}
			{% assign primehEndTime = '.$highlight['endtime'].'  %}
			{% if primehStartTime <= primehUTC and primehEndTime >= primehUTC %}';			
			
			
			$arrPrepareifCondition .= "{% if isStartbShowHighlight == true %} 
			{% assign primehighlightGroup = '".$highlight['highlightgroup']."' %} 
				{% if primeGroup == primehighlightGroup %}
				{% assign primehHighlightCounter = primehHighlightCounter | plus: 1 %}".$buttonHtml."
				{% endif %}{% endif %}
			{% endif %}";
		}

		return array( 'global_variable'=>$strAssignGlobal ,'condition'=>$arrPrepareifCondition );
		
	}

}

?>