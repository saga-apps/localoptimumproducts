<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Order in 5 min';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $variant= array('31945627959330','31615608586274','32153542623266','31615531221026','32268699435042','32213360967714','31615580176418','31615608553506','32153545801762','32268700221474');
        $quantity= 2;
        for ($i=0; $i < 4; $i++) { 
            // print_r($variant[$i]);
            $shopifyorders =  app('\App\Http\Controllers\ShopifyController')->shopifyADDOrders('ankit-staging.myshopify.com', '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', $variant[$i], $quantity);
            print_r($shopifyorders);
        }

    }
}
