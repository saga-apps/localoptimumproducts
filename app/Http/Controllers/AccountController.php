<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
// use App\Models\Usersetting;
use App\Models\Plan;
use App\Models\Productbadge;
use App\Models\Producthighlight;
use App\Models\Productbanner;

class AccountController extends Controller
{   
    
    public function getAccount()
    {
        $data['shownavbar'] = true;
        $data['activeMenu'] = 'account';
        $data['subpage'] = 'account';
        $data['themes'] = app('\App\Http\Controllers\ShopifyController')->getusertheme(Auth::User()->shop, Auth::User()->accesstoken, Auth::User()->userid);
        if(empty($data['themes'])){
            $data['themes'] =array();
        }
        // $usersetting = Usersetting::where('userid',Auth::Id())->first();
        // $data['usersetting'] = $usersetting;
        return View('account.default',$data);
    }

    public function getPlans()
    {
        $data['shownavbar'] = true;
        $data['activeMenu'] = 'account';
        $data['subpage']    = 'plans';
        // $data['currentplan'] = Plan::find(Auth::User()->planid);
        // $data['currentplan'] = Plan::all();

        $data['user'] = User::where(array('userid'=>Auth::User()->userid) )->orderby('userid','desc')->first();
        
        $data['currentplan'] = Plan::where(array('id'=>Auth::User()->planid,'isactive'=> 1 ) )->orderby('id','desc')->first();

        return View('account.default',$data );
    } 

    public function postAccount( Request $request ){
        
        $requestData = $request->all();

        if( !empty( $requestData['account'] ) ){

            $userinfo = User::where( 'userid', '=' , Auth::Id() )->first();
            $userThemeId = $userinfo->themeid;

            if($userThemeId != $requestData['account']['themeid'] ){

                $badges = Productbadge::where('userid','=',Auth::Id() )->get()->toArray();
                $highlights = Producthighlight::where('userid','=',Auth::Id() )->get()->toArray();
                $banners = Productbanner::where('userid','=',Auth::Id() )->get()->toArray();
                $bannerfileval = 'banner';
                $hfileval = 'highlight';
                $fileval = 'badge';

                $this->checkthemeliquid($userinfo->shop , $userinfo->accesstoken,$requestData['account']['themeid']);
                
                foreach($badges as $badge){
                    if($badge['badge_type'] == 'image'){

                        $fullpath = env('AWS_PATH').$badge['productbadgeid'];
                        $Productbadge_id = $badge['productbadgeid'];
                        $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( $userinfo->shop , $userinfo->accesstoken,  $fullpath, $Productbadge_id,$fileval,$requestData['account']['themeid'],$badge['imageformat']);

                        
                        
                    }
                }

                foreach($banners as $banner){
                    if($banner['imgsrc'] == ''){

                        $fullpath = env('AWS_PATH').'ban'.$banner['productbannerid'];

                        $banner_id = $banner['productbannerid'];
                        $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( $userinfo->shop , $userinfo->accesstoken,  $fullpath, $banner_id,$hfileval,$requestData['account']['themeid'],$banner['imageformat']);
                        
                    }
                }
                
                foreach($highlights as $highlight){
                    if($highlight['image'] == ''){

                        $fullpath = env('AWS_PATH').'h'.$highlight['producthighlightid'];

                        $highlight_id = $highlight['producthighlightid'];
                        $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( $userinfo->shop , $userinfo->accesstoken,  $fullpath, $highlight_id,$hfileval,$requestData['account']['themeid'],$highlight['imageformat']);
                        
                    }
                }
                
            }
            
            User::where( 'userid', '=' , Auth::Id() )->update( $requestData['account'] );

            $updateliquid = app('\App\Http\Controllers\BadgeController')->updateShopifyLiquidContent();
            if($userinfo->planid > 2){
                $updatehighlightliquid = app('\App\Http\Controllers\HighlightController')->updateShopifyLiquidContent();
                $updateNoteliquid = app('\App\Http\Controllers\NoteController')->updateShopifyLiquidContent('','','',0);
                $updateBannerliquid = app('\App\Http\Controllers\BannerController')->updateShopifyLiquidContent();
            }
            

            //Usersetting::where( array( 'userid'=> Auth::Id() ) )->update( array('timezone'=> $requestData['account']['timezone'] ) );
        }

        return array( 'status'=> true , 'msg'=> 'Updated successfully');
    }

    public function selectedtheme( Request $request ){
        
        $requestData = $request->all();
        if( !empty( $requestData['themeid'] ) ){
            $theme = array(
                'themeid' => $requestData['themeid'],
            );

            User::where( array( 'userid'=> Auth::Id() ) )->update( $theme );
            //Usersetting::where( array( 'userid'=> Auth::Id() ) )->update( array('timezone'=> $requestData['account']['timezone'] ) );
        }

        return array( 'status'=> true , 'msg'=> 'Theme Updated successfully');
    }

    public function redirectPlan()
    {   
       
        setcookie("planbannerclicked", true);
        return redirect('plans');
    }

}
