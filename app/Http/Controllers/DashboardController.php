<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Orderdata;
use App\Models\Productdata;
use App\Models\Country;
use App\Models\Language;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\User;
use Mail;
use Log;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Config;

class DashboardController extends Controller {

  public function getLogout(Request $request){
    Auth::logout();
    return redirect()->to('/');
  }

  public function home(){
    $date = date('Y-m-d');
    $Orders = Orderdata::where( array('isread'=> 1 ) )->whereDate('created_at', '>=', date('Y-m-d', strtotime($date. '- 60 days')))->get()->toArray();
    $Products = Productdata::whereDate('created_at', '>=', date('Y-m-d', strtotime($date. '- 60 days')))->get()->toArray();
    $totalquantity=0;
    $productarray=[];
    $duplicatearray=array();

    $orderarray=[];
    $duplicateorderarray=array();

    foreach($Orders as $myorder){
      $date = date('dS M y',strtotime($myorder['created_at']));
      if(in_array( $date, $duplicateorderarray )){
        $orderarray[$date] = $orderarray[$date] + 1;
      }else{
        $orderarray[$date]=1;
      }
      array_push($duplicateorderarray,$date);
    }

    foreach($Products as $mydata){
      $totalquantity+=$mydata['quantity'];

      if(in_array ( $mydata['name'], $duplicatearray )){
        $productarray[$mydata['name']] = $productarray[$mydata['name']] + $mydata['quantity'];
      }else{
        $productarray[$mydata['name']]=$mydata['quantity'];
      }
      array_push($duplicatearray,$mydata['name']);
    }

    // echo "<pre>";print_r($productarray);exit;

    $totalproducts=count($Products);
    $totalorders=count($Orders);
    $param = array(
      'activeMenu' => 'home',
      'userid' => Auth::User()->userid,
      'totalorders' => $totalorders,
      'totalproducts' => $totalproducts,
      'totalquantity' => $totalquantity,
      'productarray' => $productarray,
      'orderarray' => $orderarray,
    );
    return view('dashboard.home', $param);
  }

  public function ActivateOrderSync( Request $Request ){

    $requestData = $Request->all();
    $badgestatus = 1;
    if( $requestData['isordersync'] == 0 ){
      $badgestatus = 0;
    }
    User::where( array('userid'=> Auth::User()->userid ) )->update( array( 'isordersync'=> $badgestatus  ) );

    // $arrRuleMapper['userid'] = '1';
    // $arrRuleMapper['orderid'] = '4261480267937';
    // $arrRuleMapper['topic'] = 'orders/create';
    // $arrRuleMapper['jsondata'] = '';
    // $new_id = Orderdata::create( $arrRuleMapper );   
    
    $msg  = $badgestatus == true ? 'activated' :'deactivated';
    return array('status'=> true, 'msg'=> 'Badges '.$msg.' successfully.' );

  }

  public function ReadWebhookOrder(){
    $orderdatas=Orderdata::where( array('isread'=> 0 ) )->orderby('id','asc')->limit(15)->get()->toArray();
    $arrRuleMapper['isread'] = '1';
    $myqueryarray=array();
    foreach($orderdatas as $mydata){
      array_push($myqueryarray,$mydata['id']);
    }
    Orderdata::whereIn('id',$myqueryarray )->update($arrRuleMapper);

    foreach($orderdatas as $data){
      $orderid = $data['orderid'];
      $userid = $data['userid'];
      $jsondata = json_decode($data['jsondata'],true);
      // echo "<pre>";print_r($jsondata);exit;
      foreach($jsondata['line_items'] as $lineitem){
        // echo "<pre>";print_r($lineitem);exit;
        $quantity = $lineitem['quantity'];
        $title = $lineitem['title'];
        $productid = $lineitem['product_id'];
        $variantid = $lineitem['variant_id'];

        $arrMapper['userid'] = $userid;
        $arrMapper['orderid'] = $orderid;
        $arrMapper['name'] = $title;
        $arrMapper['quantity'] = $quantity;
        $arrMapper['productid'] = $productid;
        $arrMapper['variantid'] = $variantid;
        Productdata::create( $arrMapper );  
      }
    }
    return "ok";

  }

  public function getBadges ( $number=0 ){
    $param = array(
              'activeMenu' => 'dashboard',
              'queryparams' => '',
            );

    $objBades = Productbadge::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0 ) )->orderby('priority','asc');

    $badgeActiveCount = Productbadge::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0,'isactive'=>1 ) )->count();
    $createdate = Auth::User()->createtime;

    $date1=strToTime($createdate);
    $date1=new DateTime( date("Y-m-d", $date1));


    $date2= new DateTime(date('Y-m-d'));
    // $date2= new DateTime(date('2020-04-06'));

    $diff=date_diff($date1,$date2);
    // print_r($diff->format("%a") );exit;
    $param['datediff'] = $diff->format("%a");
    $param['isreview'] = Auth::User()->isreview;

    $param['badges'] = $objBades->get()->toArray();
    $param['badgecount'] = count($param['badges']);
    $param['number'] = $number;
    $param['badgeActiveCount'] = $badgeActiveCount;
    // echo "<pre>";
    // print_r($param);exit;
    // print_r( $param['badges'] );
    // die;
      return view('dashboard.home', $param);
  }

    public function getHighlights ( Request $request){

      $param = array(
                'activeMenu' => 'highlights',
                'queryparams' => '',
              );

      $country = Country::all()->toArray();

      $highlight = Highlightstyle::where(array('userid' => Auth::User()->userid))->first();
      if (isset($highlight) && !empty($highlight)) {
        $highlightid = $highlight['id'];
      } else {
        $arrRuleMapper = array();
        $arrRuleMapper['userid']     = Auth::User()->userid;
        $arrRuleMapper['display']    =  '{"textcolor":"#666666","height":"30","shadow":null,"bgcolor":"#ffc107","bgcolortype":"2","gradbgcolorfirst":"#ffc107","gradbgcolorsecond":"#ffc107"}';

        $arrRuleMapper['style'] =  '{"highlightstylecolumn":"2","highlightstyletabcolumn":"2","highlightstylemobcolumn":"1","highlighttextposition":"1","highlightshape":"3","highlightshape_padding":"space2","shadow":"light","bordersize":null,"bordercolor":"#d9d9d9","borderstyle":"solid","textsize":"13","fontstyle":null,"margintop":"0","marginbottom":"0","marginleft":"0","marginright":"0","left":"1","right":"1","animation":"No Animation","mtextsize":"12px"}';

        $arrRuleMapper['appearance']   = '{"desktop":{"textStyle":"white-space: normal; overflow: hidden; font-size: 13px;","spanStyle":"","highlightStyle":"box-sizing: border-box; color: rgb(102, 102, 102); position: relative; border-radius: 0.3em; box-shadow: rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px, rgba(0, 0, 0, 0.2) 0px 1px 5px 0px;"},"mobile":{}}';

        $highlight_new = Highlightstyle::create($arrRuleMapper);
        $highlightid = $highlight_new['id'];
      }

      $objBades = Producthighlight::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0 ) )->orderby('priority','asc');

      $obj_style = Highlightstyle::where(array('userid'=>Auth::User()->userid) )->first()->toArray();

      $arrHighlight = Producthighlight::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0,'isactive'=>1 ) )->orderby('priority','asc');

      $highlightActiveCount = Producthighlight::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0,'isactive'=>1 ) )->count();
      $createdate = Auth::User()->createtime;
      $param['planid'] = Auth::User()->planid;

      $date1=strToTime($createdate);
      $date1=new DateTime( date("Y-m-d", $date1));
      $date2= new DateTime(date('Y-m-d'));
      $diff=date_diff($date1,$date2);

      $param['datediff'] = $diff->format("%a");
      $param['highlights'] = $objBades->get()->toArray();
      $param['highlights_active'] = $arrHighlight->get()->toArray();
      $param['highlightstyle'] = $obj_style;
      $param['highlightActiveCount'] = $highlightActiveCount;
      $param['countries'] = $country;

      return view('dashboard.highlight', $param);

    }

    public function UpdateUserField ( Request $request){

      $user['userid'] = Auth::User()->userid;
      $order['orderscope'] = 1;
      $query = User::updateOrCreate($user, $order);
      return redirect('/settings-order-count');

    }

    public function deliverydatesetting ( Request $request){
      $requestData = $request->all();
      $user['userid'] = Auth::User()->userid;
      $order['weekdays'] = $requestData['week'];
      $order['dateformat'] = $requestData['dateformat'];
      User::where( array('userid' => Auth::Id() ) )->update( $order );

      $userBadgeActive = User::where(array('userid'=> Auth::Id(),'isbadgeshow'=> 1 ) )->first();
      $userHighlightActive = User::where(array('userid'=> Auth::Id(),'ishighlightshow'=> 1 ) )->first();
      $userBannerActive = User::where(array('userid'=> Auth::Id(),'isbannershow'=> 1 ) )->first();
      if( !empty( $userBadgeActive ) ){
        $userBadges =  Productbadge::where( array('isactive'=>1,'isdeleted'=> 0,'userid'=> Auth::User()->userid ) )->orderby('priority','asc')->get()->toArray();

        $strLiquidContentJS = View('liquid.primebjs',array('userBadges'=> $userBadges,'livecounttime'=>$userBadgeActive['livecounttime'],'setlivevisitcount'=>$userBadgeActive['setlivevisitcount'],'weekdays'=>$userBadgeActive['weekdays'],'dateformat'=>$userBadgeActive['dateformat'] ) )->render();

        $pageContent = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidbadgeJs( $userBadgeActive['shop'] , $userBadgeActive['accesstoken'],  $strLiquidContentJS, $userBadgeActive['themeid']);
      }

      if(Auth::User()->planid > 2){
        if( !empty( $userHighlightActive ) ){
          $userHighlights =  Producthighlight::where( array('isactive'=>1,'isdeleted'=> 0,'userid'=> Auth::User()->userid ) )->orderby('priority','asc')->get()->toArray();
          $HighlightStyle =  Highlightstyle::where( array('userid'=> Auth::User()->userid ) )->get()->first();

          if(!empty( $userHighlights )){
            $strLiquidContentJS = View('liquid.primehjs',array('userHighlights'=> $userHighlights,'HighlightStyle'=> $HighlightStyle,'livecounttime'=>$userHighlightActive['livecounttime'],'setlivevisitcount'=>$userHighlightActive['setlivevisitcount'],'weekdays'=>$userHighlightActive['weekdays'],'dateformat'=>$userHighlightActive['dateformat'] ) )->render();

            $pageContent = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidJs( $userHighlightActive['shop'] , $userHighlightActive['accesstoken'],  $strLiquidContentJS, $userHighlightActive['themeid']);
          }
        }

        if( !empty( $userBannerActive ) ){
          $userbanners =  Productbanner::where( array('isactive'=>1,'userid'=> Auth::User()->userid ) )->orderby('priority','asc')->get()->toArray();
          if(!empty( $userbanners )){
            $strLiquidContentJS = View('liquid.primebanjs',array('userBanners'=> $userbanners,'livecounttime'=>$userBannerActive['livecounttime'],'setlivevisitcount'=>$userBannerActive['setlivevisitcount'],'weekdays'=>$userBannerActive['weekdays'],'dateformat'=>$userBannerActive['dateformat'] ) )->render();
            $pageContent = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBannerJs( $userBannerActive['shop'] ,   $userBannerActive['accesstoken'],  $strLiquidContentJS, $userBannerActive['themeid']);
          }
        }

      }

      return "done";

    }

    public function replybackclever()
    {
      ob_start();
      echo "ok";
      $size = ob_get_length();
      header('Content-type: application/json');
      header("Content-Length: {$size}");
      header("Connection: close");
      ob_end_flush();
      ob_flush();
      flush();
    }

    public function replyback($msg)
    {
      if (headers_sent()) {
        return false;
      }
      if (empty($_REQUEST)) {
        return false;
      }
      ignore_user_abort(true);
      set_time_limit(0);

      ob_start();
      echo json_encode(array('status' => true, 'msg' => $msg)); // send the response
      header('Connection: close');
      header('Content-type: application/json');
      header('Content-Length: ' . ob_get_length());
      ob_end_flush();
      ob_flush();
      flush();
    }

    public function gethelp ( Request $request){
      $param = array(
                'activeMenu' => 'help',
                'queryparams' => '',
              );
        return view('dashboard.help', $param);
    }

      public function getprivacy ( Request $request){
           $param = array(
                'activeMenu' => '',
                'queryparams' => '',
              );
        return view('web.privacy', $param);
    }

    public function getDescription( Request $request ){

        $param = array(
                    'activeMenu' => 'dashboard1',
                    'queryparams' => '',
            );


        return View('dashboard.home1',$param);


    }

    public function sendintegratemail(Request $request){

    $data = [
      'usershop' => Auth::User()->shop,
      'userid' => Auth::User()->userid
    ];

    $subject = 'New integration request for Prime Badges, Userid- '.Auth::User()->userid;
    $email_id = 'gaurav@prime.getinstashop.com';

    $emailsArr = ['support@getinstashop.freshdesk.com'];

    // $emailsArr = ['shagufta@thaliatechnologies.com'];



     if( strpos(env('APP_URL'), 'localhost') == FALSE ){

        Mail::send('mail.integrate', array('param' => array($data)), function($message) use($subject, $email_id, $emailsArr ) {
            $message->from($email_id)->subject($subject);
            $message->to($emailsArr);

        });

        return array('msg'=> 'Request Send Successfully');
     }

  }

  public function sendintegratemailhighlights(Request $request)
  {

    $data = [
      'usershop' => Auth::User()->shop,
      'userid' => Auth::User()->userid
    ];

    $subject = 'New integration request for Prime Highlights, Userid- '.Auth::User()->userid;
    $email_id = 'gaurav@prime.getinstashop.com';

    $emailsArr = ['support@getinstashop.freshdesk.com'];

    // $emailsArr = ['shagufta@thaliatechnologies.com'];



     if( strpos(env('APP_URL'), 'localhost') == FALSE ){

        Mail::send('mail.integrate', array('param' => array($data)), function($message) use($subject, $email_id, $emailsArr ) {
            $message->from($email_id)->subject($subject);
            $message->to($emailsArr);

        });

        return array('msg'=> 'Request Send Successfully');
     }

  }

  public function sendintegratemailNotes(Request $request)
  {

    $data = [
      'usershop' => Auth::User()->shop,
      'userid' => Auth::User()->userid
    ];

    $subject = 'New integration request for Prime Notes, UserId- '.Auth::User()->userid;
    $email_id = 'gaurav@prime.getinstashop.com';

    $emailsArr = ['support@getinstashop.freshdesk.com'];

    // $emailsArr = ['shagufta@thaliatechnologies.com'];

     if( strpos(env('APP_URL'), 'localhost') == FALSE ){

        Mail::send('mail.integrate', array('param' => array($data)), function($message) use($subject, $email_id, $emailsArr ) {
            $message->from($email_id)->subject($subject);
            $message->to($emailsArr);

        });

        return array('msg'=> 'Request Send Successfully');
     }

  }

  public function sendintegratemailBanners(Request $request)
  {

    $data = [
      'usershop' => Auth::User()->shop,
      'userid' => Auth::User()->userid
    ];

    $subject = 'New integration request for Prime Banners, UserId- '.Auth::User()->userid;
    $email_id = 'gaurav@prime.getinstashop.com';

    $emailsArr = ['support@getinstashop.freshdesk.com'];

    // $emailsArr = ['shagufta@thaliatechnologies.com'];

     if( strpos(env('APP_URL'), 'localhost') == FALSE ){

        Mail::send('mail.integrate', array('param' => array($data)), function($message) use($subject, $email_id, $emailsArr ) {
            $message->from($email_id)->subject($subject);
            $message->to($emailsArr);

        });

        return array('msg'=> 'Request Send Successfully');
     }

  }

    public function createHighlights( $highlightid=''){
       // dd('hii i m in create Hightlight');
        $customCollections_old =  (array)app('App\Http\Controllers\ShopifyController')->getCustomCollections();
        $smartCollections =  (array)app('App\Http\Controllers\ShopifyController')->getSmartCollections();

        if(empty($customCollections_old)){
          $customCollections_old = array();
        }
        if(empty($smartCollections)) {
          $smartCollections = array();
        }

        $country = Country::all()->sortBy('countryname')->toArray();
        $language = Language::all()->sortBy('languagename')->toArray();

        $obj_style = array();

        $customCollections = array_merge($customCollections_old,$smartCollections);

        $obj_style = Highlightstyle::where(array('userid'=>Auth::User()->userid) )->first()->toArray();

        $emojis = [128512,128513,128514,128515,128516,128517,128518,128519,128520,128521,128522,128523,128524,128525,128526,128527,128528,128529,128530,128531,128532,128533,128534,128535,128536,128537,128538,128539,128540,128541,128542,128543,128544,128545,128546,128547,128548,128549,128550,128551,128552,128553,128554,128555,128556,128557,128558,128559,128560,128561,128562,128563,128564,128565,128566,128567,128577,128578,128579,128580,129296,129297,129298,129299,129300,129301,129312,129313,129314,129315,129316,129317,129319,129320,129321,129322,129323,129324,129325,129326,129327,129488];

        if( empty( $highlightid ) ){

             $param = array(
                    'activeMenu' => 'createhighlights',
                    'queryparams' => '',
                    'highlight'         => '',
                    'dynamicVariables'=> self::ARR_DYNAMIC_VARIABLES,
                    'customCollections'=>$customCollections,
                    'countries'=>$country,
                    'languages'=>$language,
                    'emojis'=>$emojis
            );

          return View('dashboard.createhighlights',$param);
        }


        $arrHighlight = Producthighlight::where(array('userid'=>Auth::User()->userid,'producthighlightid'=>  $highlightid ) )->first()->toArray();
        // $date_utc = new \DateTime($arrBadge['starttime'], new \DateTimeZone(Auth::User()->timezone));
        // date_default_timezone_set (Auth::User()->timezone);
        // $userTimezone = new \DateTimeZone(Auth::User()->timezone);
        $date_from = date('Y-m-d H:i:s',strtotime($arrHighlight['starttime']));
        // print_r($arrBadge);exit;
        if( isset($arrHighlight['excludeproduct']) && $arrHighlight['excludeproduct'] != ''){
          $arrHighlight['excludeproduct'] = explode(",",$arrHighlight['excludeproduct']);
        }

        if( isset($arrHighlight['shop_language']) && $arrHighlight['shop_language'] != ''){
          $arrHighlight['shop_language'] = explode(",",$arrHighlight['shop_language']);
        }

        if( isset($arrHighlight['customer_location']) && $arrHighlight['customer_location'] != ''){
          $arrHighlight['customer_location'] = explode(",",$arrHighlight['customer_location']);
        }

        date_default_timezone_set("Asia/Bangkok");

// dd(date('Y-m-d H:i:s'));
        // print_r($arrHighlight);exit;
        $param = array(
                    'activeMenu' => 'createhighlights',
                    'queryparams' => '',
                    'highlight'     => $arrHighlight,
                    'highlight_style' => $obj_style,
                    'dynamicVariables'=> self::ARR_DYNAMIC_VARIABLES,
                    'customCollections'=>$customCollections,
                    'countries'=>$country,
                    'languages'=>$language,
                    'emojis'=>$emojis
        );


        return View('dashboard.createhighlights',$param);


    }

    public function createBanners($bannerid=''){

      $customCollections_old =  (array)app('App\Http\Controllers\ShopifyController')->getCustomCollections();
      $smartCollections =  (array)app('App\Http\Controllers\ShopifyController')->getSmartCollections();
      // dd($smartCollections);
      if(empty($customCollections_old)){
        $customCollections_old = array();
      }
      if(empty($smartCollections)) {
        $smartCollections = array();
      }

      $country = Country::all()->sortBy('countryname')->toArray();

      $language = Language::all()->sortBy('languagename')->toArray();

      $customCollections = array_merge($customCollections_old,$smartCollections);

      $emojis = [];

      if( empty( $bannerid ) ){

           $param = array(
                  'activeMenu' => 'createbanners',
                  'queryparams' => '',
                  'banner'         => '',
                  'dynamicVariables'=> self::ARR_DYNAMIC_VARIABLES,
                  'customCollections'=>$customCollections,
                  'countries'=>$country,
                  'languages'=>$language,
                  'emojis'=>$emojis
          );

        return View('dashboard.createbanners',$param);
      }


      $arrBanner = Productbanner::where(array('userid'=>Auth::User()->userid,'productbannerid'=>  $bannerid ) )->first()->toArray();
      // $date_utc = new \DateTime($arrBanner['starttime'], new \DateTimeZone(Auth::User()->timezone));
      // date_default_timezone_set (Auth::User()->timezone);
      // $userTimezone = new \DateTimeZone(Auth::User()->timezone);
      $date_from = date('Y-m-d H:i:s',strtotime($arrBanner['starttime']));
      // print_r($arrBanner);exit;

      if( isset($arrBanner['excludeproduct']) && $arrBanner['excludeproduct'] != ''){
        $arrBanner['excludeproduct'] = explode(",",$arrBanner['excludeproduct']);
      }

      if( isset($arrBanner['shop_language']) && $arrBanner['shop_language'] != ''){
        $arrBanner['shop_language'] = explode(",",$arrBanner['shop_language']);
      }

      if( isset($arrBanner['customer_location']) && $arrBanner['customer_location'] != ''){
        $arrBanner['customer_location'] = explode(",",$arrBanner['customer_location']);
      }

      date_default_timezone_set("Asia/Bangkok");

      $param = array(
                  'activeMenu' => 'createbanners',
                  'queryparams' => '',
                  'banner'     => $arrBanner,
                  'dynamicVariables'=> self::ARR_DYNAMIC_VARIABLES,
                  'customCollections'=>$customCollections,
                  'countries'=>$country,
                  'languages'=>$language,
                  'emojis'=>$emojis
      );

      return View('dashboard.createbanners',$param);
    }


    public function createBadges( $badgeid=''){
      // dd(date_default_timezone_get());
        $customCollections_old =  (array)app('App\Http\Controllers\ShopifyController')->getCustomCollections();
        $smartCollections =  (array)app('App\Http\Controllers\ShopifyController')->getSmartCollections();

        $country = Country::all()->sortBy('countryname')->toArray();

        $language = Language::all()->sortBy('languagename')->toArray();
        // $country = Country::orderBy('countryname','Desc')->toArray();

        if(empty($customCollections_old)){
          $customCollections_old = array();
        }
        if(empty($smartCollections)) {
          $smartCollections = array();
        }

        $customCollections = array_merge($customCollections_old,$smartCollections);

        $emojis = [128512,128513,128514,128515,128516,128517,128518,128519,128520,128521,128522,128523,128524,128525,128526,128527,128528,128529,128530,128531,128532,128533,128534,128535,128536,128537,128538,128539,128540,128541,128542,128543,128544,128545,128546,128547,128548,128549,128550,128551,128552,128553,128554,128555,128556,128557,128558,128559,128560,128561,128562,128563,128564,128565,128566,128567,128577,128578,128579,128580,129296,129297,129298,129299,129300,129301,129312,129313,129314,129315,129316,129317,129319,129320,129321,129322,129323,129324,129325,129326,129327,129488];

        if( empty( $badgeid ) ){

             $param = array(
                    'activeMenu' => 'createbadges',
                    'queryparams' => '',
                    'badge'         => '',
                    'dynamicVariables'=> self::ARR_DYNAMIC_VARIABLES,
                    'customCollections'=>$customCollections,
                    'countries'=>$country,
                    'languages'=>$language,
                    'emojis'=>$emojis
            );

          return View('dashboard.createbadges',$param);
        }


        $arrBadge = Productbadge::where(array('userid'=>Auth::User()->userid,'productbadgeid'=>  $badgeid ) )->first()->toArray();
        // $date_utc = new \DateTime($arrBadge['starttime'], new \DateTimeZone(Auth::User()->timezone));
        // date_default_timezone_set (Auth::User()->timezone);
        // $userTimezone = new \DateTimeZone(Auth::User()->timezone);
        $date_from = date('Y-m-d H:i:s',strtotime($arrBadge['starttime']));
        // print_r($arrBadge);exit;

        if( isset($arrBadge['excludeproduct']) && $arrBadge['excludeproduct'] != ''){
          $arrBadge['excludeproduct'] = explode(",",$arrBadge['excludeproduct']);
        }

        if( isset($arrBadge['shop_language']) && $arrBadge['shop_language'] != ''){
          $arrBadge['shop_language'] = explode(",",$arrBadge['shop_language']);
        }

        if( isset($arrBadge['customer_location']) && $arrBadge['customer_location'] != ''){
          $arrBadge['customer_location'] = explode(",",$arrBadge['customer_location']);
        }
        // print_r($arrBadge['customer_location']);exit;

        date_default_timezone_set("Asia/Bangkok");

// dd(date('Y-m-d H:i:s'));
        // print_r($arrBadge);exit;
        $param = array(
                    'activeMenu' => 'createbadges',
                    'queryparams' => '',
                    'badge'     => $arrBadge,
                    'dynamicVariables'=> self::ARR_DYNAMIC_VARIABLES,
                    'customCollections'=>$customCollections,
                    'countries'=>$country,
                    'languages'=>$language,
                    'emojis'=>$emojis
        );


        return View('dashboard.createbadges',$param);


    }

    public function createDescription( Request $request ){

        $param = array(
                    'activeMenu' => 'createdescription',
                    'queryparams' => '',
            );


        return View('dashboard.createdescription',$param);


    }
  


}
