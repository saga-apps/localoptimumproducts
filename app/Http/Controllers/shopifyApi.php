<?php

// namespace App\Libraries;
// namespace App;

namespace App\Http\Controllers;
use App;
use Auth;
use Session;


use Osiset\BasicShopifyAPI;
use App\Libraries\ShopifyApiException;
use Exception;
use Illuminate\Support\Facades\Log;
// use App\Models\orderDetail;

use App\User;
use App\Models\Orderdetail;
// use App\Models\orderdetail;
use App\Models\Productdetail;
use Config;

class shopifyApi extends Controller {

  public $shop_domain;
	private $token;
	private $api_key;
	private $secret;
	private $last_response_headers = null;

	// public function __construct($shop_domain, $token, $api_key, $secret) {
    public function __construct() {
        
      // echo "test";
      $this->name = "ShopifyClient";
      // $user = Auth::User();
      // $shopDetails = User::get()->toArray();
      // print_r(end($shopDetails));exit;
		  // $this->shop_domain = $shop_domain;
		  // $this->token = $token;
		  // $this->api_key = $api_key;
      // $this->secret = $secret;
        
      // $this->shop_domain = 'dev38.myshopify.com';
      // $this->token ='f4b89e93432a4ea1fffd306fca5f0c20';
      // $this->api_key = '338af54bf86993759173e70b923ab80b';
      // $this->secret ='f19e1bc14c3474677497528e62c6f10b';

      // $this->shop_domain = $shop_domain;
      // $this->token = $token;
      // $this->api_key = $api_key;
      // $this->secret = $secret;
    }

  
  
    //********************************************************************************************* */
 
  public function getProductDetails($userid, $title){

    $shopDetails = User::find($userid)->toArray();

    // print_r($shopDetails);exit;
    $this->shop_domain = $shopDetails['shop'];
    $this->token =$shopDetails['accesstoken'];
    // $this->api_key = env('shopify_api_key_graphql');
    // $this->secret =env('shopify_secret_graphql');

    $api = new BasicShopifyAPI();
    $api->setVersion(Config::get('api.version')); // "YYYY-MM" or "unstable"
    $api->setShop($this->shop_domain);
    $api->setAccessToken($this->token);
    // $var=["title:".$title];
    // $title = $title;
    // print_r($var);

    $resultVariantVal = $api->graph(' { 
      shop {
      name
      products(first: 250 query:"title:'.$title.'*" ) {
        pageInfo {
          hasNextPage
         
        }
        edges {
          cursor
          node {
            id
            title
            handle
            featuredImage{
              originalSrc
            }
           
          }
        }
      }
      
     }
    }
    ');

    // echo "<pre>";
    // print_r($resultVariantVal->body->shop->products->edges);exit;
    $productlist = $resultVariantVal->body->shop->products->edges;
    $data=[];
    $productdetails=[];
    foreach($productlist as $productval){

        $productnode = $productval->node;
        $data['id']= str_replace("gid://shopify/Product/","",$productnode->id);
       
        $data['title']=$productnode->title;
        $data['handle']=$productnode->handle;

        if(isset($productnode->featuredImage)){
            

            if (strpos($productnode->featuredImage->originalSrc, '_50x50') == false) {

                $imgsrcval = str_replace(array('.jpg','.png','.jpeg', '.gif'), array('_50x50.jpg', '_50x50.png','_50x50.jpeg','_50x50.gif'),$productnode->featuredImage->originalSrc);

              } else {

                $imgsrcval =$productnode->featuredImage->originalSrc;
            }

            // $data['images'][0]['src']=$productnode->featuredImage->originalSrc;
            $data['images'][0]['src']=$imgsrcval;
        }


        array_push($productdetails,$data);

        
    }

    // print_r($productdetails);exit;
    return $productdetails;

  }


    


}
