<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productbadge;
use App\Models\Producthighlight;
use App\Models\Highlightstyle;
use App\Models\OrderCount;
use App\Models\Jsonorder;
use App\Models\LineItem;
use Auth;
use Storage;
use App\User;
use Log;
use Carbon\Carbon;
use DateTime;
use DB;
use Redirect;

class HighlightController extends Controller
{
    public function deleteHighlight(  Request $request ){
        $requestData = $request->all();
        // print_r($requestData);exit;
        $themeid = Auth::User()->themeid;
        $highlight = Producthighlight::where('producthighlightid','=',$requestData['highlightid'] )->get()->first();

        $filePath = env('S3_PATH').'h'.$requestData['highlightid'];
        if(Storage::disk('s3')->exists( $filePath )){
            Storage::disk('s3')->delete($filePath);
            $response = app('\App\Http\Controllers\ShopifyController')->deleteThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken, $requestData['highlightid'],$themeid,$fileval='highlight',$highlight['imageformat']);
        }

        $response = Producthighlight::where('producthighlightid','=',$requestData['highlightid'] )->delete();

        $this->updateShopifyLiquidContent( );

        return array( 'status'=> true ,'msg'=> 'Highlight deleted successfully.' );
    }
    public function copyHighlight(  Request $request ){
        
        $requestData = $request->all();
        $response = Producthighlight::where('producthighlightid','=',$requestData['highlightid'] )->get()->toArray();
        
        unset($response[0]['producthighlightid']);
        $newArray = $response[0];
       // print_r($newArray);exit();
        $new_highlight_id = Producthighlight::create( $newArray ); 
        $id = $new_highlight_id->producthighlightid;
        $themeid = Auth::User()->themeid;
        if( strpos($requestData['highlightimg'] , 'img.icons8.com') == false ){

            $img2 = (file_get_contents($requestData['highlightimg']));
            $img = 'data:image/png;base64,' . base64_encode($img2);
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace('data:image/gif;base64,', '', $img);   
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $filePath = env('S3_PATH').'h'.$id;
            $image = Storage::disk('s3')->put($filePath, $data);
            Storage::disk('s3')->setVisibility($filePath, 'public');
            $fullpath = Storage::disk('s3')->url($filePath);
            $fileval = 'highlight';

            // print_R($fullpath);exit;
            $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $id,$fileval,$themeid);

        }


        return array( 'id'=> $id , 'msg'=> 'Highlight Copy successfully.');
       
        
    }
 

    public function gethelphighlight ( Request $request){
        $param = array(
                  'activeMenu' => 'help',
                  'queryparams' => '',
                );
          return view('dashboard.helphighlight', $param);
      }
    
    public function updateHighlightGroup( Request $Request ){
        $requestData = $Request->all();
        Producthighlight::where(array('producthighlightid'=>$requestData['producthighlightid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'highlightgroup'=> $requestData['group']) ); 
        $this->updateShopifyLiquidContent( );
        // $msg  = $requestData['isactive'] == 1 ? 'deactivated' :'activated';
        return array('status'=> true, 'msg'=> 'Group updated successfully.' );
    }
    
      
    public function updateShopifyLiquidContent( $ishighlightshow=true){
        
        //Log::info( Auth::User()->ishighlightshow );
        $userHighlightActive = User::where(array('userid'=> Auth::User()->userid,'ishighlightshow'=> 1 ) )->first();
        if( !empty( $userHighlightActive ) ){
            if(Auth::user()->planid >= 3){

            
                // $date_utc = new \DateTime("now", new \DateTimeZone());
                // $date_from = $date_utc->format('Y-m-d H:i:s');
                $userHighlightActive= $userHighlightActive->toArray();
                // $date_from = time();
                $userHighlights =  Producthighlight::where( array('isactive'=>1,'isdeleted'=> 0,'userid'=> Auth::User()->userid ) )
                                            // ->where('starttime', '<=', $date_from)
                                            // ->where('endtime', '>=', $date_from)
                                            ->orderby('priority','asc')->get()->toArray();

            // print_R($userHighlights);exit; 

                foreach( $userHighlights as $highlights => $values ) {
                    $userHighlights[$highlights]["timezone"] = $userHighlightActive['timezone'];
                    $userHighlights[$highlights]["planid"] = $userHighlightActive['planid'];

                }

                $HighlightStyle =  Highlightstyle::where( array('userid'=> Auth::User()->userid ) )->get()->first();
            }else {
                $userHighlights  = array();
                $HighlightStyle = array();
            } 
        }else{
            $userHighlights  = array();
            $HighlightStyle = array();
        } 

        if(!empty( $userHighlightActive )){
            $themeid = $userHighlightActive['themeid'];
        }else{
            $themeid = '';
        }
        $highlightcount = count($userHighlights);

        $strLiquidContentCSS = View('liquid.primecss')->render();

        $response1 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidCss( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentCSS,$themeid);


        $strLiquidContentJS = View('liquid.primehjs',array('userHighlights'=> $userHighlights,'HighlightStyle'=> $HighlightStyle,'livecounttime'=>Auth::user()->livecounttime,'setlivevisitcount'=>Auth::user()->setlivevisitcount,'weekdays'=>Auth::user()->weekdays,'dateformat'=>Auth::user()->dateformat ))->render();

        $response2 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidJs( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentJS,$themeid);

        
        // $strLiquidContent = View('liquid.primeh',array('userHighlights'=> $userHighlights,'HighlightStyle'=> $HighlightStyle ) )->render();
        // $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid);

        if( $highlightcount > 100 ) {

            $userHighlights1 = array_slice($userHighlights,0,100);
            $userHighlights2 = array_slice($userHighlights,100,100);
            

            $strLiquidContent = View('liquid.primeh',array('userHighlights'=> $userHighlights1,'HighlightStyle'=> $HighlightStyle,'highlightcount'=> $highlightcount ) )->render();
            $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid);

            $strLiquidContent2nd = View('liquid.primeh2',array('userHighlights'=> $userHighlights2,'HighlightStyle'=> $HighlightStyle,'highlightcount'=> $highlightcount ) )->render();
            $response2nd = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight2( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent2nd,$themeid);

            if( $highlightcount > 200 ) {
                $userHighlights3 = array_slice($userHighlights,200,100);

                $strLiquidContent3rd = View('liquid.primeh3',array('userHighlights'=> $userHighlights3,'HighlightStyle'=> $HighlightStyle,'highlightcount'=> $highlightcount ) )->render();
                $response3rd = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight3( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent3rd,$themeid);
            }

            if( $highlightcount > 300 ) {
                $userHighlights4 = array_slice($userHighlights,300,100);

                $strLiquidContent4th = View('liquid.primeh4',array('userHighlights'=> $userHighlights4,'HighlightStyle'=> $HighlightStyle,'highlightcount'=> $highlightcount ) )->render();
                $response4th = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight4( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent4th,$themeid);
            }

            if( $highlightcount > 400 ) {
                $userHighlights5 = array_slice($userHighlights,400,100);

                $strLiquidContent5th = View('liquid.primeh5',array('userHighlights'=> $userHighlights5,'HighlightStyle'=> $HighlightStyle,'highlightcount'=> $highlightcount ) )->render();
                $response5th = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight5( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent5th,$themeid);
            }

        }else{
            $strLiquidContent = View('liquid.primeh',array('userHighlights'=> $userHighlights,'HighlightStyle'=> $HighlightStyle,'highlightcount'=> $highlightcount ) )->render();
            $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidHighlight( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid);
        }

        
        // return array( 'status'=> $response ,'msg'=> 'Theme \'starb.liquid\' updated.');
        return array( 'status'=> $response ,'msg'=> 'Theme \'primeh.liquid\' updated.');
        
    }

    public function updateHighlightPriority( Request $Request ){
        $requestData = $Request->all();

        // print_r($requestData);exit;

        if( empty( $requestData['highlight'] ) ){

            return array('status'=> false , 'msg'=> 'something went wrong.' );
        }
        
        $requestData = json_decode( $requestData['highlight'] , true );

        foreach( $requestData as $badgeid => $priorityIndex ) {
            
            Producthighlight::where(array('producthighlightid'=>$badgeid ,'userid'=> Auth::User()->userid ) )->update( array( 'priority'=> $priorityIndex ) );  
        }
       
       $this->updateShopifyLiquidContent( );


        return array( 'status'=> true , 'msg'=> 'Highlight priority updated successfully.');

    }


    public function postActivateHighlight( Request $Request  ){

        $requestData = $Request->all();
        
        $highlightstatus = 1;
        if( $requestData['isShowHighlight'] == 0 ){
            $highlightstatus = 0;
        }

        $strLiquidContent = View('liquid.primemeta')->render();
        $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidPrimeMeta( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,Auth::User()->themeid);

        User::where( array('userid'=> Auth::User()->userid ) )->update( array( 'ishighlightshow'=> $highlightstatus  ) );

        $this->updateShopifyLiquidContent( $highlightstatus );

        $msg  = $highlightstatus == true ? 'activated' :'deactivated';
        return array('status'=> true, 'msg'=> 'Highlights '.$msg.' successfully.' );

    }

    public function updateHighlightStatus( Request $Request ){

        $requestData = $Request->all();

        Producthighlight::where(array('producthighlightid'=>$requestData['producthighlightid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'isactive'=> $requestData['isactive'] == 1 ? 0 :1 ) ); 
        
        $this->updateShopifyLiquidContent( );
        $msg  = $requestData['isactive'] == 1 ? 'Deactivated' :'Activated';
        return array('status'=> true, 'msg'=> 'Highlight '.$msg.' Successfully.' );
    }

    public function searchiconresult(  Request $request ){
        
        $requestData = $request->all();
        
        if(!empty($requestData['searchval']) && !empty($requestData['imgstyle']) ){

            $html = file_get_contents('https://icons8.com/icon/set/'.$requestData['searchval'].'/'.$requestData['imgstyle']); //get the html returned from the following url

        }

        if( !empty($requestData['searchval']) && empty($requestData['imgstyle']) ){

            $html = file_get_contents('https://icons8.com/icons/set/'.$requestData['searchval']);
        }


        $htmlcontent = new \DOMDocument();
        libxml_use_internal_errors(TRUE); //disable libxml errors

        if(!empty($html)){ //if any html is actually returned

            $htmlcontent->loadHTML($html);
            libxml_clear_errors(); 
            $html_xpath = new \DOMXPath($htmlcontent);

            $html_row = $html_xpath->query('//a[@class="grid-icon__link"]');
            $iconnames = [];


            if( !empty($requestData['searchval']) && empty($requestData['imgstyle']) ){

                $html_row = $html_xpath->query('//a[@class="grid-icon__link"]//img');
            }

            
            if($html_row->length > 0){

                foreach($html_row as $row){
                

                    if( !empty($requestData['searchval']) && empty($requestData['imgstyle']) ){

                        $data = $row->getAttribute('src');
                        $icon= $data; 
                    
                    } else {

                        $data = $row->getAttribute('href');
                        $icon= substr($data, strrpos($data, "/") + 1); 

                    } 

                    if(!in_array($icon,$iconnames)) {
                        array_push($iconnames,$icon);
                    }
                  
                }
                //print_r($iconnames);exit;
                return array( 'status'=> true , 'iconlistname'=>$iconnames  );
            
            } else {

                return array( 'status'=> true , 'iconexist'=>'No Icons found'  );
            } 
        }
      
    }

    public function postHighlight( Request $Request  ){

        $requestData = $Request->all();
        
        // print_r($requestData);exit;

    	ksort( $requestData['rule'] );
        date_default_timezone_set(Auth::User()->timezone);
    	// if( empty( $requestData['rule'] ) || empty( $requestData['display']['highlighttitle'] )  ){
        if( empty( $requestData['rule'] ) || empty( $requestData['highlighttitle']) || empty( $requestData['highlightimg'] )  ){
    		return array('status'=> false,'msg'=> 'Rules, Icon and title required.');
        }

        if($requestData['linktab'] == '2'){
            if($requestData['highlightdescription'] == ''){
                return array('status'=> false,'msg'=> 'Modal Content is required.');
            }
        }

        $arrRuleMapper = array( );
        $arr_new = array( );
        //$arrRuleMapper['title'] 	 =  $requestData['display']['highlighttitle'];
        //$arrRuleMapper['image'] 	 =  $requestData['highlightimg'];
        $arrRuleMapper['title'] 	 =  $requestData['highlighttitle'];

        if(strpos($arrRuleMapper['title'], 'primehm') !== false){
            $arrRuleMapper['title'] = str_replace("primehm","product.metafields",$arrRuleMapper['title']);
        }

        $arrRuleMapper['condition']  =  $requestData['condition'];
      //  $arrRuleMapper['highlight_type']  =  $requestData['highlight_type'];

        if(  strpos($requestData['highlightimg'], 'img.icons8.com') !== false ){

            $arrRuleMapper['image'] = $requestData['highlightimg'];
        
        } else {

            $arrRuleMapper['image'] = null;
        }
        
        if(empty($requestData['highlight']['tooltip'])) {
            $arrRuleMapper['tooltip'] = '';
        }else{
            if(strpos($requestData['highlight']['tooltip'], "'") !== false){
                $arrRuleMapper['tooltip'] = str_replace("'","&#039;",$requestData['highlight']['tooltip']);
            }else{
                $arrRuleMapper['tooltip'] = $requestData['highlight']['tooltip'];
            }
        }

     
       // $arrRuleMapper['highlightstyle'] =  json_encode( $requestData['highlightstyle'] );
        // if(isset($requestData['enabledate']) && !empty($requestData['enabledate']) && $requestData['enabledate'] == 'on' ){
        //     $arrRuleMapper['enabledate']  =  '1';
        // }else{
        //     $arrRuleMapper['enabledate']  =  '0';
        // }
        $arrRuleMapper['enabledate']  =  $requestData['enabledate'];
        if(isset($requestData['starttime']) && !empty($requestData['starttime'])){
            // $arrRuleMapper['starttime']  =  date("Y-m-d H:i:s", strtotime($requestData['starttime']));
            $arrRuleMapper['starttime']  =  strtotime($requestData['starttime']);
        }else{
            // $arrRuleMapper['starttime']  =  date("Y-m-d H:i:s", strtotime('01-02-1990 12:00 PM'));
            $arrRuleMapper['starttime']  =  strtotime('01-02-1990 12:00 PM');
        }
        if(isset($requestData['endtime']) && !empty($requestData['endtime'])){
            // $arrRuleMapper['endtime']  =  date("Y-m-d H:i:s", strtotime($requestData['endtime']));
            $arrRuleMapper['endtime']  =  strtotime($requestData['endtime']);
        }else{
            // $arrRuleMapper['endtime']  =  date("Y-m-d H:i:s", strtotime('01-02-2035 12:00 PM'));
            $arrRuleMapper['endtime']  =  strtotime('01-02-2035 12:00 PM');
        }
    	$arrRule = array();
        
        foreach( $requestData['rule'] as $index => $rule ) {
            if( empty( $rule ) ){

                continue;
            }
            $rule = $this->getRule(  $rule );
            

			if( !empty( $rule ) ){

    			$arrRule[] = $rule ;
    		}
    	}
        $arrRuleMapper['displayrules'] =  json_encode( $arrRule );

        $arrProRule = array();
        if(Auth::User()->planid > 2){
            if(isset($requestData['prorule'])){
                foreach( $requestData['prorule'] as $index => $rule ) {
                    if( empty( $rule ) ){
                        continue;
                    }
                    $rule = $this->getRule(  $rule );
                    if( !empty( $rule ) ){
                        $arrProRule[] = $rule ;
                    }
                }
                $arrRuleMapper['displayprorules'] =  json_encode( $arrProRule );
            }
        }else{
            $arrRuleMapper['displayprorules'] = '';
        }
        
        // print_r($arrRuleMapper['displayrules']);exit;
    	$arrRuleMapper['userid'] 		= Auth::User()->userid;
    	$arrRuleMapper['createtime'] 	= date("Y-m-d H:i:s");
        // $arrRuleMapper['appearance'] 	= $requestData['appearance'];
        $arrRuleMapper['isactive']      = $requestData['active'];
        $arrRuleMapper['excludeproduct'] =  $requestData['excludeproduct'];
        if(isset($requestData['customer_location']) && !empty($requestData['customer_location'])){
            $arrRuleMapper['customer_location'] =  $requestData['customer_location'];
        }else{
            $arrRuleMapper['customer_location']  =  '';
        }

        if(isset($requestData['shop_language']) && !empty($requestData['shop_language'])){
            $arrRuleMapper['shop_language'] =  $requestData['shop_language'];
        }else{
            $arrRuleMapper['shop_language']  =  '';
        }

        if(!empty($requestData['highlightlink']) && ( strlen($requestData['highlightlink']) > 7 ) ){
           // print_r('test');exit;
            $arrRuleMapper['highlightlink'] = $requestData['highlightlink'];
        
        } else {
           // print_r('test1111');exit;
            $arrRuleMapper['highlightlink'] =null;
        }

        $arrRuleMapper['linktab'] = $requestData['linktab'];

        if(isset($requestData['modalsize'])){
            $arrRuleMapper['modalsize'] = $requestData['modalsize'];
        }

        if($requestData['linktab'] == '2'){
            $arrRuleMapper['highlightdescription'] = $requestData['highlightdescription'];
        }
        
        if(isset($requestData['highlightgroup']) && !empty($requestData['highlightgroup'])){
            $arrRuleMapper['highlightgroup'] = $requestData['highlightgroup'];
        }else{
            $arrRuleMapper['highlightgroup']  =  "1";
        }

        if(isset($requestData['shopproductmeta'])){
            $shopproductmeta =  $requestData['shopproductmeta'];
            if(isset($shopproductmeta) && !empty($shopproductmeta)){
                $arrRuleMapper['shopprodmeta'] = 1;
            }
        }

        $strText = 'created';
        if( !empty( $requestData['highlightid'] ) ){

            Producthighlight::where( array('producthighlightid'=>(int)$requestData['highlightid'],'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
            $strText = 'updated';
            $Producthighlight_id = $requestData['highlightid'];

        }else{

            $priorityhighlight = Producthighlight::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0 ) )->orderby('priority','desc')->first();
            if( !empty( $priorityhighlight->priority ) ){
                $arrRuleMapper['priority'] = $priorityhighlight->priority+1;
            }
           // print_r($arrRuleMapper['highlightlink']);exit;
           //print_r($arrRuleMapper);exit;
           
            $new_highlight_id = Producthighlight::create( $arrRuleMapper );   
            // print_R($new_highlight_id);exit;

            $Producthighlight_id = $new_highlight_id->producthighlightid;
        }

        if(isset($requestData['shopproductmeta'])){
            $shopproductmeta =  $requestData['shopproductmeta'];
            if(isset($shopproductmeta) && !empty($shopproductmeta)){
                $highid = "highlight_".$Producthighlight_id;
                $arrMetaFiled = array("namespace"=>"primeh","key"=> $highid,"value"=> $shopproductmeta,"value_type"=> "string");
                $params = array( 'metafield'=> $arrMetaFiled );
                $shopproductmeta =  app('\App\Http\Controllers\ShopifyController')->set_shopproduct_metafield(Auth::User()->shop, Auth::User()->accesstoken,$params);
            }
        }

        $metakey = "highlight-popup-".$Producthighlight_id;
        $arrMetaFiled = array("namespace"=>"prime","key"=> $metakey,"value"=> $requestData['highlightdescription'],"value_type"=> "string");
        $newsparams = array( 'metafield'=> $arrMetaFiled );
        if($requestData['linktab'] == '2'){
            $nresponse = app('\App\Http\Controllers\ShopifyController')->set_shopproduct_metafield( Auth::user()->shop , Auth::user()->accesstoken, $newsparams);
            $Mapper = array( );
            $Mapper['contentmetaid'] =  $nresponse['id'];
            Producthighlight::where( array('producthighlightid'=>$Producthighlight_id,'userid'=> Auth::User()->userid ) )->update($Mapper );
        }

        $themeid = Auth::User()->themeid;

        if(isset($requestData['highlightimg'])  ){
            
            if( strpos($requestData['highlightimg'], 's3.amazonaws.com/files.thalia-apps.com') == true ){

                $img2 = (file_get_contents($requestData['highlightimg']));
                $img = 'data:image/png;base64,' . base64_encode($img2);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/gif;base64,', '', $img);   
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').'h'.$Producthighlight_id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'highlight';

                // print_R($fullpath);exit;
                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Producthighlight_id,$fileval,$themeid);

            } else if( strpos($requestData['highlightimg'], 'img.icons8.com') == false ){

                $Mapper2 = array( );
                $imgformat = 'png';
                $img = $requestData['highlightimg'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                if(  strpos($requestData['highlightimg'], 'data:image/gif;base64,') !== false ){
                    $img = str_replace('data:image/gif;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'gif';
                    $imgformat = 'gif';
                    Producthighlight::where( array('producthighlightid'=>$Producthighlight_id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }

                if(  strpos($requestData['highlightimg'], 'data:image/svg+xml;base64') !== false ){
                    $img = str_replace('data:image/svg+xml;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'svg';
                    $imgformat = 'svg';
                    Producthighlight::where( array('producthighlightid'=>$Producthighlight_id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }

                // $img = str_replace('data:image/gif;base64,', '', $img);   
                // $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').'h'.$Producthighlight_id;
                
                $image = Storage::disk('s3')->put($filePath, $data);
                
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'highlight';
                // print_r($fullpath);exit;

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Producthighlight_id,$fileval,$themeid,$imgformat);

            }
        }    
            /* elseif( $requestData['image'] != env('AWS_PATH').$Producthighlight_id ){
                
                $img = $requestData['image'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/gif;base64,', '', $img);   
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').$Producthighlight_id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Producthighlight_id);

            }else{}
        } */
        $this->updateShopifyLiquidContent( );

    	return array( 'status'=> true , 'msg'=>'Highlight '.$strText.' successfully.', 'submitType' => $strText );
    }

    public function getRule( $rule ){
     	
        $array = array();
        // print_r( $rule );
     //   die;
       // if( !is_array( $rule['conditiontype'] ) ){

       //        return array( );
       // }


        foreach( $rule['conditiontype'] as $key => $value ) {


            if( is_array($value) && count(array_filter($value)) == count($value) ){


                $array[$key] = $value;
            }else{
                $array[$key] = $value;
                // print_r( $value );
                // die;	
            }
        }

        return $array;
    }

    public function styleHighlight( Request $request ) {
        
        $requestData = $request->all();
        $arrRuleMapper = array( );
        $arr_new = array( );
        // print_r($requestData);exit;
        // if(empty($requestData['display']['height'])) {
        //     $requestData['display']['height'] = '22';
        // }
        $arrRuleMapper['display']    =  json_encode( $requestData['display'],  JSON_UNESCAPED_UNICODE );
        $arrRuleMapper['style'] =  json_encode( $requestData['highlightstyle'] );
    	$arrRuleMapper['userid'] 		= Auth::User()->userid;
        $arrRuleMapper['appearance'] 	= $requestData['appearance'];
        
        $strText = 'created';
        // if( !empty( $requestData['highlight_style_id'] ) ){
            Highlightstyle::where( array('userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
            $strText = 'updated';
            // $Producthighlight_id = $requestData['id'];
        // }else{
        //     $new_badge_id = Highlightstyle::create( $arrRuleMapper );   
        //     // $Producthighlight_id = $new_badge_id->productbadgeid;
        // }

        $this->updateShopifyLiquidContent( );

    	return array( 'status'=> true , 'msg'=>'Design '.$strText.' successfully.', 'submitType' => $strText );
    }



}
