<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Log;
use Session;
use Redirect;
use Mail;
use URL;
use Response;
use Config;
use DateTimeZone;
use DateTime;

class SettingController extends Controller
{

    public function getSettings( ) {
        
        $param = array(
                'activeMenu' => 'settings',
                'queryparams' => '',
                'page'        => 'amazonmws'
        );
        
        return View('settings.settings',$param);
    }

}