<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;

use Log;
use Mail;
use Auth;

class GuzzleController extends Controller
{   
      public function postKinvey($data, $table) {

        try
        {

            $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Basic a2lkX0hKcUQ4LXlVZjo3NTdmNmIwOTYyZjQ0YWJkOWQ2OGU2NDhkZDdkMzNjNg==', 'X-Kinvey-API-Version' => '3']]);

            $res = $client->request('POST', 'https://baas.kinvey.com/appdata/kid_HJqD8-yUf/'.$table, [
                'json' => $data
                ]);
            //print_r($res);
            return $res->getStatusCode();

        } catch (Exception $e)
        {
            $subject = 'postkinvey() error ';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );

            //print_r($error);
            //ErrorLog::postErrorLog($error);

            return;
        }

         return;
    }


    public function getKinvey($query, $table) {

        try
        {
             $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Basic a2lkX0hKcUQ4LXlVZjo3NTdmNmIwOTYyZjQ0YWJkOWQ2OGU2NDhkZDdkMzNjNg==', 'X-Kinvey-API-Version' => '3']]);


            $request = $client->request('GET', 'https://baas.kinvey.com/appdata/kid_HJqD8-yUf/'.$table.'/?query='.$query);

            return (string)$request->getBody();

        } catch (Exception $e)
        {
            log::info('kinvey get error');
            $subject = 'postkinvey() error ';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );
            ErrorLog::postErrorLog($error);

            return;
        }
    }




    public function cleverTapEvent($data) {

        try
        {

           /* $data = array();
            $data['identity'] = 1003;
            $data['type'] = 'event';
            $data['evtName'] = 'Uninstall';*/

            $clevertapData = array("d" => array($data));

            $client = new \GuzzleHttp\Client(['headers' => ['X-CleverTap-Account-Id' => '4KW-W64-4Z5Z
', 'X-CleverTap-Passcode' => 'AHW-QIA-AHKL','Content-Type' => 'application/json']]);

            $request = $client->post('https://api.clevertap.com/1/upload',  ['json'=>$clevertapData]);



            return $request->getBody();

        } catch (Exception $e)
        {
            $subject = 'postkinvey() error ';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );


            //ErrorLog::postErrorLog($error);

            return;
        }
    }

   

    public function apiCall( $url ) {

        try{
            //Log::info( $url );
            $client  = new \GuzzleHttp\Client();
            $res     = $client->request('GET', $url , ['verify' => false]);
            $result  = $res->getBody();
            return json_decode($result,true);

        } catch (Exception $e) {

          $response = $e->getResponse();
          Log::info( 'Exception :'.$response );
          return false;
          //$jsonBody = (string) $response->getBody();
          //Log::info( "keepa Api Exception" );
          //log::info('big-error--'.$url.'--error--'.$jsonBody.'--data--'.json_encode($data));

            //Log::info( 'Exception :'.$response );
            // $result = $e->getResponse()->getBody(true);
            // return json_decode($result,true);
        }
        catch (ServerException $e) {
                $response = $e->getResponse();
          //$jsonBody = (string) $response->getBody();
          //log::info('big-error--'.$url.'--error--'.$jsonBody.'--data--'.json_encode($data));
           //Log::info( "keepa Api ServerException" );
                Log::info( 'ServerException :'.$response );
                 return false;
            // $result = $e->getResponse()->getBody(true);
            // return json_decode($result,true);
               
        }catch(ClientException $e) {
           //Log::info( "keepa Api ClientException" );
               $response = $e->getResponse();
                Log::info( 'ClientException :'.$response );
                 return false;
          //$jsonBody = (string) $response->getBody();
          //$result = $e->getResponse()->getBody(true);
            //return json_decode($result,true);
               
        }catch( RequestException $e ) {
            //Log::info( "keepa Api RequestException" );
          $response = $e->getResponse();
          Log::info( 'RequestException :'.$response );
           return false;

          
          //$jsonBody = (string) $response->getBody();
          //$result = $e->getResponse()->getBody(true);
          //return json_decode($result,true);
        }
    }

  }
