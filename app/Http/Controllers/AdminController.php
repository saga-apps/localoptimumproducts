<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Admin;
use App\User;
use Auth;
use Carbon;
use Mail;
use Redirect;
use App\Models\Producthighlight;
use App\Models\Productbadge;
use App\Models\Showcase;
use App\Models\Adminlog;
// use App\Models\Autosynctask;
// use App\Models\Task;
// use App\Models\Reportrequest;
// use App\Models\Reviewcssversion;
// use App\Models\Mwscredential;
use Storage;    
// use App\Helpers\CommonHelper;
use Response;
use File;
use ZipArchive;
use Session;
use Log;

class AdminController extends Controller
{
   public function getAdminLogin()
    {
    	 $param = array(
            'activeMenu' => '',
            'usersList' => ''
        );
        return View('admin.index',array('param' => $param));
    }

    public function postAdminLogin(Request $request)
    {   
    	 $this->validate($request,[
         'email'=>'required',
         'password'=>'required'
      ]);


    	 $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        $admin = Admin::where($credentials)->first();
        
        if (empty($admin)) {

        	 return redirect()->back()->withErrors('Email or Password not mached');
        }

        session(['isAdmin' => $request->input('email')]);
        session(['admin' => $admin]);
        session(['email' => $admin->email]);
       return redirect('admin/dashboard');


    }


    public function getAdminLogout(Request $request) { 
        Auth::logout();
    	return redirect('admin');
    }

    public function getAutoLogin($userId)
    {   
        if (Auth::loginUsingId($userId, true)) {
            return redirect('/dashboard');
        }
    	
    }

    public function getautologinhighlight($userId)
    {   
        if (Auth::loginUsingId($userId, true)) {
            return redirect('/highlights');
        }
    	
    }

    public function getautologinbadge($userId)
    {   
        if (Auth::loginUsingId($userId, true)) {
            return redirect('/dashboard');
        }
    	
    }

    public function updateReview($userid,$isreview)
    {
         $user = User::find($userid);
         $user->isreview = $isreview;
         $user->save();
         return redirect()->back();
    }

    public function updateIntegration($userid,$isintegrated)
    {
         $user = User::find($userid);
         $user->isintegrated = $isintegrated;
         $user->save();
         return redirect()->back();
    }

    public function updateIntegrationhighlight($userid,$isintegratedhighlight)
    {
         $user = User::find($userid);
         $user->isintegratedhighlight = $isintegratedhighlight;
         $user->save();
         return redirect()->back();
    }

    public function updateIntegrationNote($userid,$isintegratednote)
    {
         $user = User::find($userid);
         $user->isintegratednote = $isintegratednote;
         $user->save();
         return redirect()->back();
    }

    public function updateIntegrationBanner($userid,$isintegratedbanner)
    {
        $user = User::find($userid);
        $user->isintegratedbanner = $isintegratedbanner;
        $user->save();
        return redirect()->back();
    }

    public function getUserList(Request $request)
    {	

        $queryparams = $request->except('url');

    	 if (empty($queryparams['query']) || is_null($queryparams['query'])) {
            $queryparams['query'] = '';  
           $users = User::orderBy('userid','Desc')->paginate(10);

        } else {
        
        	$query = $queryparams['query'];
        	$query = '%'.$query.'%';
        	 $users = User::where('email', 'LIKE', $query)
        	 				->orWhere('shop', 'LIKE', $query)
                             ->orWhere('userid', 'LIKE', $query)
                             ->orderBy('userid','Desc')
							->paginate(10);

			$users->appends(request()->input())->links();
					
        }        
        
        foreach($users as $uservalue){
            if (!empty($uservalue)) {
                $theme = app('\App\Http\Controllers\ShopifyController')->getusertheme($uservalue->shop, $uservalue->accesstoken, $uservalue->userid); 
                if(isset($theme) && !empty($theme)){
                    $uservalue->themes = $theme;
                }else{
                    $uservalue->themes = array();
                }
                
            }
        }
    	$param = array(
            'activeMenu' => 'userlist',
            'usersList' => $users,
            'query' => urldecode($queryparams['query']) 
        );  
        
    	$data['param'] = $param;
    	$data['users'] = $users; 
        
		return View('admin.buserslist',$data);
    }

    // public function downloadTheme(Request $request)
    public function downloadTheme($themeid,$themeName,$userid)
    {	
        // ini_set('max_execution_time', 600);
        $userInfo = User::where(array('userid'=> $userid ) )->first();
        File::deleteDirectory(storage_path("app/userthemes"));
        if (!empty($userInfo)) {
            $theme = app('\App\Http\Controllers\ShopifyController')->downloadAssets($userid,$themeid,$themeName); 
            return $theme;
        }
    }

    public function updateUser(){
        
       
        $arraData = array( );
        parse_str( $_POST['data'] ,$arraData );

        
        $arraData['isinstalled'] = !isset( $arraData['isinstalled'] ) ? 0 : $arraData['isinstalled'];
        $arraData['ispaymentok'] = !isset( $arraData['ispaymentok'] ) ? 0 : $arraData['ispaymentok'];
        
        $data = User::whereRaw('userid ='.trim( $_POST["userid"] ) )->first();

       
        if( !empty( $data ) ){
            User::whereRaw('userid ='.$data->userid )->update(array('isinstalled'=> $arraData['isinstalled'] ,'ispaymentok'=> $arraData['ispaymentok'] ) );
        }
        return array( 'status'=>  true );
    }

    public function getHighlights(Request $request)
    {	
        $queryparams = $request->except('url');
    	 if (empty($queryparams['query']) || is_null($queryparams['query'])) {
            $queryparams['query'] = '';  
            $highlights = Producthighlight::orderBy('producthighlightid','Desc')->paginate(10);
        } else {
        	$query = $queryparams['query'];
        	$query = '%'.$query.'%';
        	$highlights = Producthighlight::orderBy('producthighlightid','Desc')->paginate(10);
			$highlights->appends(request()->input())->links();
        }        
        
    	$param = array(
            'activeMenu' => 'highlights',
            'highlightsList' => $highlights,
            'query' => urldecode($queryparams['query']) 
        );  
        
    	$data['param'] = $param;
    	$data['highlights'] = $highlights; 
        // echo "<pre>";print_r($data);exit;
		return View('admin.highlightlist',$data);
    }

    public function getBadges(Request $request)
    {	
        $queryparams = $request->except('url');
    	 if (empty($queryparams['query']) || is_null($queryparams['query'])) {
            $queryparams['query'] = '';  
            $bagdes = Productbadge::orderBy('Productbadgeid','Desc')->paginate(10);
        } else {
        	$query = $queryparams['query'];
        	$query = '%'.$query.'%';
        	$bagdes = Productbadge::orderBy('Productbadgeid','Desc')->paginate(10);
			$bagdes->appends(request()->input())->links();
        }        
        
    	$param = array(
            'activeMenu' => 'bagdes',
            'bagdesList' => $bagdes,
            'query' => urldecode($queryparams['query']) 
        );  
        
    	$data['param'] = $param;
    	$data['bagdes'] = $bagdes; 
        // echo "<pre>";print_r($data);exit;
		return View('admin.badgelist',$data);
    }

    public function getShowcasesbadge()
    {	
        $queryparams['query'] = '';  
        $showcase = Showcase::where(array('type'=> 'badge' ) )->orderby('priority','asc')->get()->toArray();
    	$param = array(
            'activeMenu' => 'showcase',
            'showcaseList' => $showcase,
            'type' => 'badge',
            'query' => urldecode($queryparams['query']) 
        );        

        $data['type'] = 'badge';
    	$data['param'] = $param;
    	$data['showcases'] = $showcase;
        
		return View('admin.showcase',$data);
    }

    public function getShowcaseshighlight()
    {	
        $queryparams['query'] = '';  
        $showcase = Showcase::where(array('type'=> 'highlight' ) )->orderby('priority','asc')->get()->toArray();
    	$param = array(
            'activeMenu' => 'showcase',
            'showcaseList' => $showcase,
            'type' => 'highlight',
            'query' => urldecode($queryparams['query']) 
        );        

        $data['type'] = 'highlight';
    	$data['param'] = $param;
    	$data['showcases'] = $showcase;
        
		return View('admin.showcase',$data);
    }

    // public function getBadges(Request $request)
    // {	
    //     $queryparams['query'] = '';  
    //     $bagdes = Productbadge::orderBy('productbadgeid','Desc')->get()->toArray();
    // 	$param = array(
    //         'activeMenu' => 'badges',
    //         'bagdesList' => $bagdes,
    //         'query' => urldecode($queryparams['query']) 
    //     );        
        
    // 	$data['param'] = $param;
    // 	$data['bagdes'] = $bagdes;
        
	// 	return View('admin.badgelist',$data);
    // }

    public function getuserreviews(Request $request)
    {	
        $queryparams['query'] = '';  
        $userreview = User::orderBy('userid','Desc')->get()->toArray();
    	$param = array(
            'activeMenu' => 'userreview',
            'reviewList' => $userreview,
            'query' => urldecode($queryparams['query']) 
        );        
        
    	$data['param'] = $param;
    	$data['userreview'] = $userreview;
        
		return View('admin.userreviewlist',$data);
        
    }

    public function changeuserplan(Request $request)
    {	
        $userid = $request['userid'];
        $Data = array(
            'planid' => $request['plan'],
        );
        User::updateOrCreate(array('userid' => $userid),$Data);
		return 'true';
    }

    public  function changeKinveyCollectionName()
    {
        $path = base_path('.env');
      
        $key = 'KINVEY_COLLECTION';
        
        $now = Carbon::now();
       
        $collection = 'cron-'.$now->weekOfYear;



        if(is_bool(env($key)))
        {
            $old = env($key)? 'true' : 'false';
        }
        elseif(env($key)===null){
            $old = 'null';
        }
        else{
            $old = env($key);
        }

        if (file_exists($path)) {
            
            file_put_contents($path, str_replace(
                "$key=".$old, "$key=".$collection, file_get_contents($path)
            ));

            die('Updated');
        }
    }

    // public function testMail()
    // {

    // Mail::send('emails.welcome',array(), function ($message) {

    //     $message->from('sagashopifyapps@gmail.com', 'Saga');

    //     $message->to('sagashopifyapps@gmail.com')->subject('Welcome to Saga');

    //     $message->replyTo('sagashopifyapps@gmail.com');

    // });

    // echo "Your email has been sent successfully";

    // }

    public function getAwscredentials(){
        
        $awscredentials = Awscredential::getRegioncount();
      
        $param = array(
            'activeMenu' => 'awscredentials',
            'awscredentials' => $awscredentials          
        );
        unset(  $awscredentials );
        return View('admin.awscredentials', array('param' => $param));
    }
    
    public function getCronStatus(){
        
        $currentcron = Autosynctask::where('isprogress','!=',2)->orderby('taskid','desc')->get();
        $cronstatus = Autosynctask::where('isprogress',2)->orderby('taskid','desc')->get();

        $param = array(
            'activeMenu' => 'cronstatus',
            'cronstatus' => $cronstatus,
            'currentcron' => $currentcron
        );

        unset(  $currentcron,$cronstatus );

        return View('admin.cronstatus', array('param' => $param));
    }

     public function getWidgettypes(){
        
        $widgettypes = Widgettype::all();

        $param = array(
            'activeMenu' => 'widgettypes',
            'widgettypes' => $widgettypes ,
        );

        return View('admin.widgettypes', array('param' => $param ) );
    }

    public function MonthlyBillingUsageCharge( ){
        
        log::info( 'Monthly Billing starting..' );
        
        User::chunk( 10 , function( $users ) {

            foreach ( $users as $key => $objUser ) {
               self::createMonthlyUsageChargeByUser( $objUser );
            }

        });

        log::info( 'Monthly Billing done..' );
    }

    public static function createMonthlyUsageChargeByUser( $objUser ){
        
        $lastMonthStartDate = date( 'Y-m-d', strtotime('first day of last month') );
        $lastMonthEndDate   = date( 'Y-m-d', strtotime('last day of last month') );
        
        $alertCount = Alert::whereRaw('userid ='.$objUser->userid.' AND createdate BETWEEN "'.$lastMonthStartDate.' 00:00:00" AND "'.$lastMonthEndDate.' 23:59:59" ')->count();
        // $alertCount = 420;
        # if user alert count zero than skip user.
        if( $alertCount == 0 ){
            return false;
        }

        $intMonth = (int)date('m',strtotime( $lastMonthStartDate ) );
        $intYear  = date('Y',strtotime( $lastMonthEndDate ) );

        #Check billing done for this month than skip this user;
        $isBillingDone = Billing::whereRaw('userid ='.$objUser->userid.' AND ispaid=1 AND month= '.$intMonth.' AND year="'.$intYear.'"')->first();

        // $userData = Billing::whereRaw('userid ='.$objUser->userid.'')->orderby('billingid','desc')->first();

        if( !empty( $isBillingDone ) ){
            log::info( 'Billing already done this month for userid:'.$objUser->userid );
            return true;
        }

        $arrBilling = [
                        'userid'      => $objUser->userid,
                        'month'       => (int)date('m',strtotime( $lastMonthStartDate ) ),
                        'year'        => (int)date('Y',strtotime( $lastMonthEndDate ) ),
                        'alertcount'  => $alertCount,
                        'ispaid'      => 1,
                        'amount'      => 0,
                        'createtime'  => date( 'Y-m-d H:i:s') 
                    ];



        # if alert count free limit 
        if( self::ALERT_FREE_LIMIT >= $alertCount ){
            
            if( !Billing::insert( $arrBilling ) ){
                log::info( 'insert Failed'.json_encode( $arrBilling ) );
            }

            return true;
        }

        $actualAlertCount = $alertCount;
        $alertCount       = ( $alertCount - self::ALERT_FREE_LIMIT );
        $amount           = ( $alertCount/50 );
        
        if( is_float( $amount ) ){
          $amount  = ( (int)$amount)+0.99;  
        }
        #twl 75$ cap logic
        if( $amount > 75  ){
            $amount = 75;
        }
        # if user uninstalled our app before billing date. 
        if( $objUser->isinstalled == 0  ){
            $arrBilling['amount'] = $amount;
            $arrBilling['ispaid'] = 0; 

            if( !Billing::insert( $arrBilling ) ){
                log::info( 'insert Failed'.json_encode( $arrBilling ) );
            }

            return true;
        }
        

        $reponseData = app('\App\Http\Controllers\ShopifyController')->usageBiling( $objUser, $amount , $actualAlertCount );

        $arrBilling['amount'] = $amount;

        if( false == $reponseData['status'] ) {
            $arrBilling['ispaid'] = 0;
            $arrBilling['billinglogs'] = json_encode( $reponseData['response'] );
            
        }else{

            $arrBilling['ispaid'] = 1;
            $arrBilling['billinglogs'] = json_encode( $reponseData['response'] );
        }

        if( !Billing::insert( $arrBilling ) ){
            log::info( 'insert Failed'.json_encode( $arrBilling ) );
        }

        unset( $arrBilling ,$reponseData,$alertCount,$lastMonthStartDate,$lastMonthEndDate, $objUser, $actualAlertCount );

        return true; 
    }


    public function getAnalyzetheme(){
        $param = array(
            'activeMenu' => 'analyzetheme'
        );
        return View('admin.analyzetheme', array('param' => $param));
    }

    public function postAnalyzetheme( ){
        $htmldata = '<p>done</p>';  

        
         $User = array(
                'userid' => trim($_POST['userid'])               
            );
                  
         $result = User::where($User)->first();
         
            if (!empty($result)) {
                $themeid = $_POST['themeid'];
                $themename = $_POST['themename'];
                $htmldata .= app('\App\Http\Controllers\ShopifyController')->getAssetData($result->shop, $result->accesstoken, $themeid, $themename);
            }
            
        return $htmldata;
    }

    public function getusertheme(){

        $User = array(
            'userid' => trim($_POST['userid'])               
        );
        
        $result = User::where($User)->first();
        
        if (!empty($result)) {
            
            $data = app('\App\Http\Controllers\ShopifyController')->getusertheme($result->shop, $result->accesstoken, $_POST['userid']); 
         
        }
            
        return $data;
    }

    public function postGetthemedata(){


//          log::info('get theme data function');
        $returndata['htmldata'] = '<p>done</p>';          

        $User = array(
            'userid' => trim($_POST['userid'])
            );
        $result = User::where($User)->first();
        
        if (!empty($result)) {
            $themeid = $_POST['themeid'];
            $themename = $_POST['themename'];
            $returndata['htmldata'] .= app('\App\Http\Controllers\ShopifyController')->getFiledata($result->shop, $result->accesstoken, $_POST['assetkey'], $themeid, $themename); 
            $forlogs = app('\App\Http\Controllers\ShopifyController')->getFiledataforAdminlogs($result->shop, $result->accesstoken, $_POST['assetkey'], $themeid, $themename);
            $returndata['id'] = $this->saveAdminGetlogs($forlogs, $_POST['userid'], $_POST['assetkey']);
        }
            
        return $returndata;
    }


    public function putNewdata(){
//          log::info('get theme data function');
         $htmldata = '<p>done</p>';          
         
          $User = array(
                'userid' => trim($_POST['userid'])               
            );
                  
         $result = User::where($User)->first();
         
            if (!empty($result)) {
                $themeid = $_POST['themeid'];
                $themename = $_POST['themename'];
                  
                //log::info($_POST['newdata']);
                $htmldata .= app('\App\Http\Controllers\ShopifyController')->putFiledata($result->shop, $result->accesstoken, $_POST['assetkey'], $_POST['newdata'], $themeid, $themename); 
                log::info($htmldata);
                log::info($_POST['newdata']);

                $adminputlogs = $this->adminPutDatalogs($_POST['newdata'], $_POST['userid'], $_POST['assetkey'],  $_POST['adminlogid']);
            }     
         
        return $htmldata;
    }

    public function adminPutDatalogs($newdata, $userid, $assetkey, $adminlogid)
    {
        Adminlog::where('id',$adminlogid)->update(['newchanges'=>$newdata]);
        return;
    }

    public function saveAdminGetlogs($data, $userid, $assetkey)
    {
        $adminloggedin = Session::get('admin');
        $user = User::find($userid);
        $savingdata = array(
            'adminid' => $adminloggedin->id,
            'userid' => $userid,
            'shop' => $user->shop,
            'oldchanges' => $data,
            'file' => $assetkey,
            'created_at' => date('Y-m-d H:i:s')
        );

        $id = Adminlog::create($savingdata );
       
        return $id->id;
    }

    public function getAdminHistory()
    {
        $param['param'] = array(
            'activeMenu' => 'history'
        );
        if(isset($_GET['userid'])){
            $param['logs'] = Adminlog::where('userid', $_GET['userid'])->orderby('id','desc')->paginate(10);
            foreach($param['logs'] as $log){
                $admindata = Admin::where('id',$log->adminid)->first();
                // dd($admindata);
                if(!empty($admindata)){
                    $log->adminid = $admindata->email;
                    // dd($log);
                }
            }
            
        }
        // dd($param);
        return View('admin.history', $param);
    }

    public function getAdminlog(Request $request){
        $logdata = Adminlog::where('id', $request->get('adminlogid'))->first();
        // $logdata->oldchanges = json_decode($logdata->oldchanges, true);
        // $logdata->newchanges = json_decode($logdata->newchanges, true);
        return $logdata;
    }

    public function pauseCron(){
        
        print_r( $_POST );
        die;

        // Currentcron::incrementProcessConditionally(["cronid"=>(int)trim($_POST['cronid'])]);
          
       return;
    }
    
    public function deleteCron(){
        print_r( $_POST );
        die;
       // Currentcron::deleteCurrentcron(["cronid"=>(int)trim($_POST['cronid'])]);
         
       return;
    }

    public function getReviewPreview()
    {
        
        return view('admin.reviewpreview');
    }

    public function runningTasks()
    {   
        $data['param'] = array(
            'activeMenu' => '',
            'usersList' => ''
        );
     
       $tasks = Task::where('isprogress',1)->paginate(10);
     
       $data['tasks'] = $tasks;
       return view('admin.runningtasks',$data);
    }

    public function pullTaskaction( $userid , $taskid ){

       Task::where( array('userid'=>$userid ,'taskid'=>$taskid ) )->update( array( 'isprogress'=> 1 ) );

        Reportrequest::where( array('userid'=> $userid,'taskid'=> $taskid ) )->update( array( 'isrunning'=> 0,'iscomplete'=> 0 ) );

        // app('App\Http\Controllers\ImportController')->replyback('Activity start');

        app('App\Http\Controllers\ImportController')->importProductReport( $userid  );

    }

    public function updateStore(Request $request)
    {
        $user = User::find($request->input('userid'));
       $user->productcredit = $request->input('productcredit');
       $user->productcount = $request->input('productcount');
       $user->save();
       return redirect()->back();
    }

    public function reviewCssVersion( $userid='' ){

        $reviewcssversion = Reviewcssversion::where( array('userid'=>$userid ) )->orderBy('reviewcssid','desc')->paginate(5);

        $users = User::where('userid','=',$userid)->first();

        // $param = array(
        //     'activeMenu' => '',
        //     'usersList' => ''
        // );

        $param = array(
            'activeMenu' => 'reviewcssversion',
            'reviewcssversion' => $reviewcssversion,
            'user'             => $users
        );        
       
        $data['param'] = $param;
        
        return View('admin.reviewcssversion',$data );
    }

    public function downloadUserReport( $userid = '' ){

        $users = User::where('userid','=',$userid)->first();

        if( empty( $users->userid )  ){
            return "User Invalid";
            exit(0);
        }

        $mwsCredential = Mwscredential::where( array( 'userid'=> $users->userid,'isverified'=>1 ) )->first();

        if( empty( $mwsCredential->userid ) ){
            return "User mws Invalid";
            exit(0);
        }

        $reportRequestData = Reportrequest::where( array('userid'=> $users->userid ) )->orderBy('reportrequestsid','desc')->first();
        
        if( empty( $reportRequestData->reportid ) ){
            return "User reportRequestData not found.";
            exit(0);
        }
        $userReportProductFilePath = storage_path('app/admin_'.$users->userid.'.txt');
        

        $reportData = $reportRequestData->toArray();

        $response = app('App\Http\Controllers\MwsController')->getProductsByReportid( $mwsCredential , $reportData['reportid'] , $reportData['userid'] );

        if( false === $response ){
            return "User report not prepared try after few sec.";
            exit(0);
        }
        
        Storage::disk('local')->put('admin_'.$users->userid.'.txt', '');
        Storage::disk('local')->put('admin_'.$users->userid.'.csv', '');

        $isReportAddedFile = app('App\Http\Controllers\ImportController')->putReportDataFile( $reportData['userid'] , $response['reportid'] , $mwsCredential,'admin_' );

        $userReportProductFilePath = storage_path('app/admin_'.$reportData['userid'].'.txt');

        $iterator = app('App\Http\Controllers\ImportController')->readTheFile( $userReportProductFilePath );
        $counter = 1;
        
        $filename = storage_path('app/admin_'.$reportData['userid'].'.csv');
        $file     = fopen($filename,"w");
        

        foreach( $iterator as $reportDataTxt ) {

            $reportDataTxt = explode("\t" , $reportDataTxt );

            if( $counter == 1 ){

                $arrayHeader = $reportDataTxt;
                $arrayHeader = CommonHelper::mapReportHeader( $arrayHeader ,  $mwsCredential->region );

                fputcsv($file,$arrayHeader);
                $counter++;

                continue;
            }else{

                if( count( $arrayHeader ) !== count( $reportDataTxt )  ){
                    continue;
                }

                //$product = array_combine( $arrayHeader, $reportDataTxt  );

                fputcsv($file,$reportDataTxt);
            }
        }

        fclose( $file );
        
            $headers = array(
                'Content-Type' => 'text/csv',
            );

        Storage::disk('local')->delete('admin_'.$reportData['userid'].'.txt');

        return Response::download( $filename, 'admin_'.$reportData['userid'].'.csv', $headers);

    }

    public function addshowcase ( Request $request){
        $requestData = $request->all();
        $arrRuleMapper = array( );
        $arrRuleMapper['storeurl'] 	 =  $requestData['storeurl'];
        $arrRuleMapper['storename'] 	 =  $requestData['storename'];
        $arrRuleMapper['type'] 	 =  $requestData['showcaseaddtype'];
        $arrRuleMapper['homeurl'] 	 =  $requestData['homeurl'];
        $arrRuleMapper['producturl'] 	 =  $requestData['producturl'];
        $arrRuleMapper['collectionurl'] 	 =  $requestData['collectionurl'];
        $arrRuleMapper['miscurl'] 	 =  $requestData['miscurl'];

        if(!empty($requestData['showcaseid'])){
            Showcase::where( array('id'=> $requestData['showcaseid'] ) )->update( $arrRuleMapper );
        }else{
            $priorityhighlight = Showcase::where(array('isactive'=>1,'type'=>$requestData['showcaseaddtype'] ) )->orderby('priority','desc')->first();
            if( !empty( $priorityhighlight->priority ) ){
                $arrRuleMapper['priority'] = $priorityhighlight->priority+1;
            }
            Showcase::create($arrRuleMapper);
        }
        
        return array('status'=> true);
    }

    public function updateShowcaseStatus( Request $Request ){

        $requestData = $Request->all();

        Showcase::where(array('id'=>$requestData['id']) )->update( array( 'isactive'=> $requestData['isactive'] == 1 ? 0 :1 ) ); 
        
        // $this->updateShopifyLiquidContent( );
        $msg  = $requestData['isactive'] == 1 ? 'Deactivated' :'Activated';
        return array('status'=> true, 'msg'=> 'Highlight '.$msg.' Successfully.' );
    }

    public function deleteShowcase(  Request $request ){
        $requestData = $request->all();
        $response = Showcase::where('id','=',$requestData['id'] )->delete();

        // $this->updateShopifyLiquidContent( );

        return array( 'status'=> true ,'msg'=> 'Highlight deleted successfully.' );
    }

    public function updateShowcasePriority( Request $Request ){
        $requestData = $Request->all();

        // print_r($requestData);exit;
        $type = $requestData['type'];

        if( empty( $requestData['showcase'] ) ){

            return array('status'=> false , 'msg'=> 'something went wrong.' );
        }
        
        $requestData = json_decode( $requestData['showcase'] , true );

        foreach( $requestData as $id => $priorityIndex ) {
            
            Showcase::where(array('id'=>$id ,'type'=> $type ) )->update( array( 'priority'=> $priorityIndex ) );  
        }
       
    //    $this->updateShopifyLiquidContent( );


        return array( 'status'=> true , 'msg'=> 'Highlight priority updated successfully.');

    }

} 
