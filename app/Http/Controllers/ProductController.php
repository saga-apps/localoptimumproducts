<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orderdata;
use Auth;
use Storage;
use App\User;
use Log;
use Carbon\Carbon;
use DateTime;
use DB;

class ProductController extends Controller
{
    
    public function OrderSetting()
    {
        
        if (empty(Auth::User())) {
            $user = User::orderBy('userid', 'DESC')->first();
        } else {
            $user = User::where('userid', Auth::Id())->first();
        }
        
        if($user->orderscope == '0'){
            $shopifyorders =  app('\App\Http\Controllers\ShopifyController')->shopifygetOrderAccess($user->shop);
            
        }else{
            $currentPage = 1;$plus = '0';$shopifyParams = array();$strQuery = '&page='.$currentPage;$productid = '';
            $shopifyproducts = app('App\Http\Controllers\ShopifyController')->getProductsByFilter($shopifyParams, $strQuery , $productid  );
            $ordercount = OrderCount::where('userid',Auth::User()->userid)->get()->toArray();

            foreach( $shopifyproducts as $shopprod => $prodvalues){
                foreach( $ordercount as $shoporder => $ordervalues){
                    if($prodvalues['id'] == $ordervalues['productid']){
                        $shopifyproducts[$shopprod]['ordercount'] = $ordervalues['ordercount'];
                    }
                }
            }
            if( count( $shopifyproducts ) >= 250 ){
                $shopifyproducts = array_slice($shopifyproducts,0,250);
            }
            $arrProductId = array();
            foreach ( $shopifyproducts  as $key => $shopifyproduct ) {
                $arrProductId[] = $shopifyproduct['id'];
            }
            
            $responseTemplate = $shopifyproducts;
            $param = array(
                'activeMenu' => 'ordersetting',
                'table' => $responseTemplate,
                
            );
            return view('dashboard.ordersetting', $param);
        }

    }

    public function LiveVisitCountSetting()
    {
        $param = array(
            'activeMenu' => 'livecount',
        );
        return view('dashboard.livecountsetting', $param);

    }

    public function DeliveryDateSetting()
    {
        $param = array(
            'activeMenu' => 'deliverydate',
            'weekdays' => Auth::User()->weekdays,
        );
        return view('dashboard.deliverydatesetting', $param);

    }

    public function postBadgeRule( Request $request ) {
        
        $requestData = $request->all();
        // echo "<pre>";print_r($requestData);exit;
    	ksort( $requestData['rule'] );
        date_default_timezone_set(Auth::User()->timezone);
    	// if( empty( $requestData['rule'] ) || empty( $requestData['display']['badgetitle'] )  ){
        if( empty( $requestData['rule'] ) || empty( $requestData['badgetitle'] )  ){
    		return array('status'=> false,'msg'=> 'Rules or title required.');
        }

        if($requestData['linktab'] == '2'){
            if($requestData['badgedescription'] == ''){
                return array('status'=> false,'msg'=> 'Modal Content is required.');
            }
        }

        $arrRuleMapper = array( );
        $arr_new = array( );
        // $arrRuleMapper['title'] 	 =  $requestData['display']['badgetitle'];
        $arrRuleMapper['title'] 	 =  $requestData['badgetitle'];
        
        if(strpos($arrRuleMapper['title'], 'primebm') !== false){
            $arrRuleMapper['title'] = str_replace("primebm","product.metafields",$arrRuleMapper['title']);
        }

        $arrRuleMapper['condition']  =  $requestData['condition'];
        $arrRuleMapper['badge_type']  =  $requestData['badge_type'];
        
        if(empty($requestData['badge']['tooltip'])) {
            $arrRuleMapper['tooltip'] = '';
        }else{
            if(strpos($requestData['badge']['tooltip'], "'") !== false){
                $arrRuleMapper['tooltip'] = str_replace("'","&#039;",$requestData['badge']['tooltip']);
            }else{
                $arrRuleMapper['tooltip'] = $requestData['badge']['tooltip'];
            }
        }

        if(empty($requestData['display']['height'])) {
            $requestData['display']['height'] = '22';
        }
        if($arrRuleMapper['badge_type'] == 'image'){
            // $requestData['display']['bgcolor'] = 'none';
            $arr_new['appearance'] 	= json_decode($requestData['appearance'],true) ;
            $arr_style = $arr_new['appearance']['desktop']['badgeStyle'];
            
            $arr_style_value = explode(";",$arr_style);
            $arr_style_value[0] = '';
            //$arr_style_value[0] = 'display : inline-block';
            unset($arr_style_value[3]);
            $implode_array = implode(";",$arr_style_value);
            $arr_new['appearance']['desktop']['badgeStyle'] = $implode_array;
            $requestData['appearance'] =  json_encode($arr_new['appearance']);
        }
        $arrRuleMapper['display']    =  json_encode( $requestData['display'],  JSON_UNESCAPED_UNICODE );

        $arrRuleMapper['badgestyle'] =  json_encode( $requestData['badgestyle'] );
        // if(isset($requestData['enabledate']) && !empty($requestData['enabledate']) && $requestData['enabledate'] == 'on' ){
        //     $arrRuleMapper['enabledate']  =  '1';
        // }else{
        //     $arrRuleMapper['enabledate']  =  '0';
        // }
        $arrRuleMapper['enabledate']  =  $requestData['enabledate'];
        if(isset($requestData['starttime']) && !empty($requestData['starttime'])){
            // $arrRuleMapper['starttime']  =  date("Y-m-d H:i:s", strtotime($requestData['starttime']));
            $arrRuleMapper['starttime']  =  strtotime($requestData['starttime']);
        }else{
            // $arrRuleMapper['starttime']  =  date("Y-m-d H:i:s", strtotime('01-02-1990 12:00 PM'));
            $arrRuleMapper['starttime']  =  strtotime('01-02-1990 12:00 PM');
        }
        if(isset($requestData['endtime']) && !empty($requestData['endtime'])){
            // $arrRuleMapper['endtime']  =  date("Y-m-d H:i:s", strtotime($requestData['endtime']));
            $arrRuleMapper['endtime']  =  strtotime($requestData['endtime']);
        }else{
            // $arrRuleMapper['endtime']  =  date("Y-m-d H:i:s", strtotime('01-02-2035 12:00 PM'));
            $arrRuleMapper['endtime']  =  strtotime('01-02-2035 12:00 PM');
        }
    	$arrRule = array();
        
        foreach( $requestData['rule'] as $index => $rule ) {
            if( empty( $rule ) ){

                continue;
            }
            $rule = $this->getRule(  $rule );
            

			if( !empty( $rule ) ){

    			$arrRule[] = $rule ;
    		}
    	}
        $arrRuleMapper['displayrules'] =  json_encode( $arrRule );

        $arrProRule = array();
        if(Auth::User()->planid > 2){
            if(isset($requestData['prorule'])){
                foreach( $requestData['prorule'] as $index => $rule ) {
                    if( empty( $rule ) ){
                        continue;
                    }
                    $rule = $this->getRule(  $rule );
                    if( !empty( $rule ) ){
                        $arrProRule[] = $rule ;
                    }
                }
                $arrRuleMapper['displayprorules'] =  json_encode( $arrProRule );
            }
        }else{
            $arrRuleMapper['displayprorules'] = '';
        }
        
    	$arrRuleMapper['userid'] 		= Auth::User()->userid;
    	$arrRuleMapper['createtime'] 	= date("Y-m-d H:i:s");
        $arrRuleMapper['appearance'] 	= $requestData['appearance'];
        $arrRuleMapper['isactive']      = $requestData['active'];
        $arrRuleMapper['excludeproduct'] =  $requestData['excludeproduct'];

        if(isset($requestData['badgegroup']) && !empty($requestData['badgegroup'])){
            $arrRuleMapper['badgegroup'] = $requestData['badgegroup'];
        }else{
            $arrRuleMapper['badgegroup']  =  "1";
        }

        if(isset($requestData['customer_location']) && !empty($requestData['customer_location'])){
            $arrRuleMapper['customer_location'] =  $requestData['customer_location'];
        }else{
            $arrRuleMapper['customer_location']  =  '';
        }

        if(isset($requestData['shop_language']) && !empty($requestData['shop_language'])){
            $arrRuleMapper['shop_language'] =  $requestData['shop_language'];
        }else{
            $arrRuleMapper['shop_language']  =  '';
        }
        
        if(!empty($requestData['badgelink']) && ( strlen($requestData['badgelink']) > 7 ) ){

            $arrRuleMapper['badgelink'] = $requestData['badgelink'];
        
        } else {

            $arrRuleMapper['badgelink'] =null;
        }

        $arrRuleMapper['linktab'] = $requestData['linktab'];

        if(isset($requestData['modalsize'])){
            $arrRuleMapper['modalsize'] = $requestData['modalsize'];
        }

        $strText = 'created';

        if(  strpos($requestData['image'], 'img.icons8.com') !== false ){

            $arrRuleMapper['imgsrc'] = $requestData['image'];
        
        } else {

            $arrRuleMapper['imgsrc'] = null;
        }

        if($requestData['linktab'] == '2'){
            $arrRuleMapper['badgedescription'] = $requestData['badgedescription'];
        }
        

        if( !empty( $requestData['badgeid'] ) ){
            Productbadge::where( array('productbadgeid'=>(int)$requestData['badgeid'],'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
            $strText = 'updated';
            $Productbadge_id = $requestData['badgeid'];
        }else{
            $priorityBadge = Productbadge::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0 ) )->orderby('priority','desc')->first();
            if( !empty( $priorityBadge->priority ) ){
                $arrRuleMapper['priority'] = $priorityBadge->priority+1;
            }
            
            $new_badge_id = Productbadge::create( $arrRuleMapper );   
            $Productbadge_id = $new_badge_id->productbadgeid;
        }

        $metakey = "badge-popup-".$Productbadge_id;
        $arrMetaFiled = array("namespace"=>"prime","key"=> $metakey,"value"=> $requestData['badgedescription'],"value_type"=> "string");
        $newsparams = array( 'metafield'=> $arrMetaFiled );
        if($requestData['linktab'] == '2'){
            $nresponse = app('\App\Http\Controllers\ShopifyController')->set_shopproduct_metafield( Auth::user()->shop , Auth::user()->accesstoken, $newsparams);
            $Mapper = array( );
            $Mapper['contentmetaid'] =  $nresponse['id'];
            Productbadge::where( array('productbadgeid'=>$Productbadge_id,'userid'=> Auth::User()->userid ) )->update($Mapper );
        }
        

        $themeid = Auth::User()->themeid;

        if(isset($requestData['image']) && !empty($requestData['image']) && $arrRuleMapper['badge_type'] == 'image' ){
            
            if($requestData['image'] == 'https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/badge_image.png'){

                $img2 = (file_get_contents($requestData['image']));
                $img = 'data:image/png;base64,' . base64_encode($img2);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/gif;base64,', '', $img);   
                $img = str_replace(' ', '+', $img);
                
                $data = base64_decode($img);
                $filePath = env('S3_PATH').$Productbadge_id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'badge';
                

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Productbadge_id,$fileval,$themeid);

            }elseif(  strpos($requestData['image'], 'img.icons8.com') !== false ){
                
                // $img2 = (file_get_contents($requestData['image']));
                // $img = 'data:image/png;base64,' . base64_encode($img2);
                // $img = str_replace('data:image/png;base64,', '', $img);
                // $img = str_replace('data:image/jpeg;base64,', '', $img);
                // $img = str_replace('data:image/gif;base64,', '', $img);   
                // $img = str_replace(' ', '+', $img);
                // $data = base64_decode($img);
                // $filePath = env('S3_PATH').$Productbadge_id;
                // $image = Storage::disk('s3')->put($filePath, $data);
                // Storage::disk('s3')->setVisibility($filePath, 'public');
                // $fullpath = Storage::disk('s3')->url($filePath);

                // //print_R($fullpath);exit;
                // $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Productbadge_id);

            }
            
            elseif( $requestData['image'] != env('AWS_PATH').$Productbadge_id ){
                $Mapper2 = array( );
                $imgformat = 'png';
                $img = $requestData['image'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                if(  strpos($requestData['image'], 'data:image/gif;base64,') !== false ){
                    $img = str_replace('data:image/gif;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'gif';
                    $imgformat = 'gif';
                    Productbadge::where( array('productbadgeid'=>$Productbadge_id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }
                // $img = str_replace('data:image/gif;base64,', '', $img);   
                // $img = str_replace(' ', '+', $img);
                if(  strpos($requestData['image'], 'data:image/svg+xml;base64') !== false ){
                    $img = str_replace('data:image/svg+xml;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'svg';
                    $imgformat = 'svg';
                    Productbadge::where( array('productbadgeid'=>$Productbadge_id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }
                $data = base64_decode($img);
                $filePath = env('S3_PATH').$Productbadge_id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'badge';

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Productbadge_id,$fileval,$themeid,$imgformat);

            }else{}
        } 
        $this->updateShopifyLiquidContent();

    	return array( 'status'=> true , 'msg'=>'Badge '.$strText.' successfully.', 'submitType' => $strText );
    }
     
     public function getRule( $rule ){
     	
     	$array = array();
     	// print_r( $rule );
      //   die;
        // if( !is_array( $rule['conditiontype'] ) ){

        //        return array( );
        // }


     	foreach( $rule['conditiontype'] as $key => $value ) {


     		if( is_array($value) && count(array_filter($value)) == count($value) ){


     			$array[$key] = $value;
     		}else{
     			$array[$key] = $value;
     			// print_r( $value );
     			// die;	
     		}
     	}

     	return $array;
     }


    public function deleteBadge(  Request $request ){
        $requestData = $request->all();
        $themeid = Auth::User()->themeid;
        $badge = Productbadge::where('productbadgeid','=',$requestData['badgeid'] )->get()->first();
        $filePath = env('S3_PATH').$requestData['badgeid'];
        if(Storage::disk('s3')->exists( $filePath )){
            Storage::disk('s3')->delete($filePath);
            $response = app('\App\Http\Controllers\ShopifyController')->deleteThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken, $requestData['badgeid'], $themeid,$fileval='badge',$badge['imageformat']);
        }
        // if($requestData['primemetaid'] != ''){
        //     $res = app('\App\Http\Controllers\ShopifyController')->deletePrimeMeta( Auth::user()->shop , Auth::user()->accesstoken, $requestData['primemetaid']);
        // }
        
        $response = Productbadge::where('productbadgeid','=',$requestData['badgeid'] )->delete();

        

        $this->updateShopifyLiquidContent( );

        return array( 'status'=> true ,'msg'=> 'Badge deleted successfully.' );
     }

    public function reviewfeedback(  Request $request ){
        $requestData = $request->all();
        // $themeid = Auth::User()->themeid;
        $feedback =$requestData['feedback'];
        $star =$requestData['star'];
        $today =date('Y-m-d');
        $user = User::where( array('userid'=> Auth::User()->userid ) )->update( array( 'reviewfeedback'=> $feedback,'lastseenreview'=> $today, 'isreview'=> 0, 'reviewstar'=> $star ) );
        // if($star == 5){
        //     return array( 'status'=> true ,'msg'=> 'Feedback submited successfully' );
        // }else{
            return array( 'status'=> true ,'msg'=> 'Feedback submited successfully' );
        // }

        // return array( 'status'=> true ,'msg'=> 'Badge deleted successfully.' );
    }

    public function updatereviewdate(  Request $request ){
        $requestData = $request->all();
        $today =date('Y-m-d');
        $user = User::where( array('userid'=> Auth::User()->userid ) )->update( array( 'lastseenreview'=> $today ) );

    }

     public function copyBadge(  Request $request ){
        
       // print_r($request);exit;
        $requestData = $request->all();
        // print_r($requestData['image']);exit;
        $response = Productbadge::where('productbadgeid','=',$requestData['badgeid'] )->get()->toArray();
        
        unset($response[0]['productbadgeid']);
        $newArray = $response[0];
        $new_badge_id = Productbadge::create( $newArray ); 
        $id = $new_badge_id->productbadgeid;
        $themeid = Auth::User()->themeid;
        if( isset($requestData['image'])) {
            if( strpos($requestData['image'] , 'img.icons8.com') == false ){
                $Mapper2 = array( );
                $imgformat = 'png';
                $img2 = (file_get_contents($requestData['image']));
                $img = 'data:image/png;base64,' . base64_encode($img2);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);

                if(  strpos($requestData['image'], 'data:image/gif;base64,') !== false ){
                    $img = str_replace('data:image/gif;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'gif';
                    $imgformat = 'gif';
                    Productbadge::where( array('productbadgeid'=>$id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }
                $img = str_replace(' ', '+', $img);
                if(  strpos($requestData['image'], 'data:image/svg xml;base64') !== false ){
                    $img = str_replace('data:image/svg+xml;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'svg';
                    $imgformat = 'svg';
                    Productbadge::where( array('productbadgeid'=>$id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }
                $data = base64_decode($img);
                $filePath = env('S3_PATH').$id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'badge';

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $id,$fileval,$themeid,$imgformat);

            }
        }

        return array( 'id'=> $id , 'msg'=> 'Badge Copy successfully.');
       
        
    }


     public function updateShopifyLiquidContent( $isbadgeshow=true){
        
        //Log::info( Auth::User()->isbadgeshow );
        $userBadgeActive = User::where(array('userid'=> Auth::User()->userid,'isbadgeshow'=> 1 ) )->first();
        // print_r($userBadgeActive);exit;
        if( !empty( $userBadgeActive ) ){
            // $date_utc = new \DateTime("now", new \DateTimeZone());
            // $date_from = $date_utc->format('Y-m-d H:i:s');
            $userBadgeActive= $userBadgeActive->toArray();
            // $date_from = time();
            $userBadges =  Productbadge::where( array('isactive'=>1,'isdeleted'=> 0,'userid'=> Auth::User()->userid ) )
                                        // ->where('starttime', '<=', $date_from)
                                        // ->where('endtime', '>=', $date_from)
                                        ->orderby('priority','asc')->get()->toArray();

            // print_R($userBadges);exit;

            foreach( $userBadges as $badges => $values ) {
                $userBadges[$badges]["timezone"] = $userBadgeActive['timezone'];
                $userBadges[$badges]["planid"] = $userBadgeActive['planid'];
            } 
            $themeid = $userBadgeActive['themeid'];
            
        }else{
            $userBadges  = array();
            $themeid = '';
        } 
        
        $strLiquidContentCSS = View('liquid.primecss')->render();

        $response1 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidCss( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentCSS,$themeid);

        $strLiquidContentJS = View('liquid.primebjs',array('userBadges'=> $userBadges,'livecounttime'=>Auth::user()->livecounttime,'setlivevisitcount'=>Auth::user()->setlivevisitcount,'weekdays'=>Auth::user()->weekdays,'dateformat'=>Auth::user()->dateformat))->render();

        $response2 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidbadgeJs( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentJS,$themeid);

        $strLiquidContent = View('liquid.primeb',array('userBadges'=> $userBadges ) )->render();
        // $filetype='badge';
        // $themeid = $userBadgeActive['themeid'];
        $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquid( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid );
        
        return array( 'status'=> $response ,'msg'=> 'Theme \'primeb.liquid\' updated.');
    }


    public function updateBadgePriority( Request $Request ){
        // print_r($Request);exit;
        $requestData = $Request->all();

        if( empty( $requestData['badge'] ) ){

            return array('status'=> false , 'msg'=> 'something went wrong.' );
        }
        
        $requestData = json_decode( $requestData['badge'] , true );

        foreach( $requestData as $badgeid => $priorityIndex ) {
            
            Productbadge::where(array('productbadgeid'=>$badgeid ,'userid'=> Auth::User()->userid ) )->update( array( 'priority'=> $priorityIndex ) );  
        }
       
       $this->updateShopifyLiquidContent( );


        return array( 'status'=> true , 'msg'=> 'Badge priority updated successfully.');

    }

    public function updateBadgeStatus( Request $Request ){

        $requestData = $Request->all();

        Productbadge::where(array('productbadgeid'=>$requestData['productbadgeid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'isactive'=> $requestData['isactive'] == 1 ? 0 :1 ) ); 
        
        $this->updateShopifyLiquidContent( );
        $msg  = $requestData['isactive'] == 1 ? 'deactivated' :'activated';
        return array('status'=> true, 'msg'=> 'Badge '.$msg.' successfully.' );
    }

    public function updateBadgeGroup( Request $Request ){

        $requestData = $Request->all();

        Productbadge::where(array('productbadgeid'=>$requestData['productbadgeid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'badgegroup'=> $requestData['group']) ); 
        
        $this->updateShopifyLiquidContent( );
        // $msg  = $requestData['isactive'] == 1 ? 'deactivated' :'activated';
        return array('status'=> true, 'msg'=> 'Group updated successfully.' );
    }
    


}
