<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Productbanner;
use App\Models\LineItem;
use App\Models\Jsonorder;
use App\Models\OrderCount;
use App\Models\Currentusercron;
use App\Models\Lastusercron;
use App\Models\Theme;
use App\Models\Country;
use App\Models\Showcase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\User;
use Storage;
use Mail;
use Log;
use DateTime;
use GuzzleHttp\Client;

class BannerController extends Controller
{

    public function postBannerRule( Request $request ) {
        
        $requestData = $request->all();
        // echo "<pre>";print_r($requestData);exit;
    	ksort( $requestData['rule'] );
        date_default_timezone_set(Auth::User()->timezone);

        if($requestData['banner_type'] == 'text'){
            if( empty( $requestData['rule'] ) || empty( $requestData['bannertitle']) || empty( $requestData['bannersubtitle'] )  ){
                return array('status'=> false,'msg'=> 'Rules or title and subtitle required.');
            }
        }

        if($requestData['linktab'] == '2'){
            if($requestData['bannerdescription'] == ''){
                return array('status'=> false,'msg'=> 'Modal Content is required.');
            }
        }

        $arrRuleMapper = array( );
        $arr_new = array( );
        // $arrRuleMapper['title'] 	 =  $requestData['display']['bannertitle'];
        $arrRuleMapper['title'] 	 =  $requestData['bannertitle'];
        $arrRuleMapper['subtitle'] 	 =  $requestData['bannersubtitle'];

        if(strpos($arrRuleMapper['title'], 'primebanm') !== false){
            $arrRuleMapper['title'] = str_replace("primebanm","product.metafields",$arrRuleMapper['title']);
        }

        $arrRuleMapper['condition']  =  $requestData['condition'];
        $arrRuleMapper['banner_type']  =  $requestData['banner_type'];

        if(empty($requestData['banner']['tooltip'])) {
            $arrRuleMapper['tooltip'] = '';
        }else{
            if(strpos($requestData['banner']['tooltip'], "'") !== false){
                $arrRuleMapper['tooltip'] = str_replace("'","&#039;",$requestData['banner']['tooltip']);
            }else{
                $arrRuleMapper['tooltip'] = $requestData['banner']['tooltip'];
            }
        }

        if(empty($requestData['display']['height'])) {
            $requestData['display']['height'] = '22';
        }
        if($arrRuleMapper['banner_type'] == 'image'){
            // $requestData['display']['bgcolor'] = 'none';
            $arr_new['appearance'] 	= json_decode($requestData['appearance'],true) ;
            $arr_style = $arr_new['appearance']['desktop']['bannerStyle'];
            
            $arr_style_value = explode(";",$arr_style);
            // $arr_style_value[0] = '';
            //$arr_style_value[0] = 'display : inline-block';
            unset($arr_style_value[3]);
            $implode_array = implode(";",$arr_style_value);
            $arr_new['appearance']['desktop']['bannerStyle'] = $implode_array;
            $requestData['appearance'] =  json_encode($arr_new['appearance']);
        }
        $arrRuleMapper['display']    =  json_encode( $requestData['display'],  JSON_UNESCAPED_UNICODE );

        $arrRuleMapper['bannerstyle'] =  json_encode( $requestData['bannerstyle'] );
        
        $arrRuleMapper['enabledate']  =  $requestData['enabledate'];
        if(isset($requestData['starttime']) && !empty($requestData['starttime'])){
            // $arrRuleMapper['starttime']  =  date("Y-m-d H:i:s", strtotime($requestData['starttime']));
            $arrRuleMapper['starttime']  =  strtotime($requestData['starttime']);
        }else{
            // $arrRuleMapper['starttime']  =  date("Y-m-d H:i:s", strtotime('01-02-1990 12:00 PM'));
            $arrRuleMapper['starttime']  =  strtotime('01-02-1990 12:00 PM');
        }
        if(isset($requestData['endtime']) && !empty($requestData['endtime'])){
            // $arrRuleMapper['endtime']  =  date("Y-m-d H:i:s", strtotime($requestData['endtime']));
            $arrRuleMapper['endtime']  =  strtotime($requestData['endtime']);
        }else{
            // $arrRuleMapper['endtime']  =  date("Y-m-d H:i:s", strtotime('01-02-2035 12:00 PM'));
            $arrRuleMapper['endtime']  =  strtotime('01-02-2035 12:00 PM');
        }
    	$arrRule = array();
        
        foreach( $requestData['rule'] as $index => $rule ) {
            if( empty( $rule ) ){

                continue;
            }
            $rule = $this->getRule(  $rule );
            

			if( !empty( $rule ) ){

    			$arrRule[] = $rule ;
    		}
    	}
        $arrRuleMapper['displayrules'] =  json_encode( $arrRule );

        $arrProRule = array();
        if(Auth::User()->planid > 2){
            if(isset($requestData['prorule'])){
                foreach( $requestData['prorule'] as $index => $rule ) {
                    if( empty( $rule ) ){
                        continue;
                    }
                    $rule = $this->getRule(  $rule );
                    if( !empty( $rule ) ){
                        $arrProRule[] = $rule ;
                    }
                }
                $arrRuleMapper['displayprorules'] =  json_encode( $arrProRule );
            }
        }else{
            $arrRuleMapper['displayprorules'] = '';
        }
        
        // print_r($arrRuleMapper['displayrules']);exit;
    	$arrRuleMapper['userid'] 		= Auth::User()->userid;
    	$arrRuleMapper['createtime'] 	= date("Y-m-d H:i:s");
        $arrRuleMapper['appearance'] 	= $requestData['appearance'];
        $arrRuleMapper['isactive']      = $requestData['active'];
        // $arrRuleMapper['bannergroup']      = $requestData['bannergroup'];
        $arrRuleMapper['excludeproduct'] =  $requestData['excludeproduct'];

        if(isset($requestData['bannergroup']) && !empty($requestData['bannergroup'])){
            $arrRuleMapper['bannergroup'] = $requestData['bannergroup'];
        }else{
            $arrRuleMapper['bannergroup']  =  "1";
        }
        
        if(isset($requestData['customer_location']) && !empty($requestData['customer_location'])){
            $arrRuleMapper['customer_location'] =  $requestData['customer_location'];
        }else{
            $arrRuleMapper['customer_location']  =  '';
        }

        if(isset($requestData['shop_language']) && !empty($requestData['shop_language'])){
            $arrRuleMapper['shop_language'] =  $requestData['shop_language'];
        }else{
            $arrRuleMapper['shop_language']  =  '';
        }

        if(!empty($requestData['bannerlink']) && ( strlen($requestData['bannerlink']) > 7 ) ){

            $arrRuleMapper['bannerlink'] = $requestData['bannerlink'];
        
        } else {

            $arrRuleMapper['bannerlink'] =null;
        }

        $arrRuleMapper['linktab'] = $requestData['linktab'];

        if(isset($requestData['modalsize'])){
            $arrRuleMapper['modalsize'] = $requestData['modalsize'];
        }

        if($requestData['linktab'] == '2'){
            $arrRuleMapper['bannerdescription'] = $requestData['bannerdescription'];
        }

        $strText = 'created';

        if(  strpos($requestData['image'], 'img.icons8.com') !== false ){

            $arrRuleMapper['imgsrc'] = $requestData['image'];
        
        } else {

            $arrRuleMapper['imgsrc'] = null;
        }

        if( !empty( $requestData['bannerid'] ) ){
            Productbanner::where( array('productbannerid'=>(int)$requestData['bannerid'],'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
            $strText = 'updated';
            $Productbanner_id = $requestData['bannerid'];
        }else{
            $priorityBanner = Productbanner::where(array('userid'=>Auth::User()->userid,'isdeleted'=>0 ) )->orderby('priority','desc')->first();
            if( !empty( $priorityBanner->priority ) ){
                $arrRuleMapper['priority'] = $priorityBanner->priority+1;
            }
            //print_r($arrRuleMapper);exit;
            $new_banner_id = Productbanner::create( $arrRuleMapper );   
            $Productbanner_id = $new_banner_id->productbannerid;
        }

        $metakey = "banner-popup-".$Productbanner_id;
        $arrMetaFiled = array("namespace"=>"prime","key"=> $metakey,"value"=> $requestData['bannerdescription'],"value_type"=> "string");
        $newsparams = array( 'metafield'=> $arrMetaFiled );
        if($requestData['linktab'] == '2'){
            $nresponse = app('\App\Http\Controllers\ShopifyController')->set_shopproduct_metafield( Auth::user()->shop , Auth::user()->accesstoken, $newsparams);
            $Mapper = array( );
            $Mapper['contentmetaid'] =  $nresponse['id'];
            Productbanner::where( array('productbannerid'=>$Productbanner_id,'userid'=> Auth::User()->userid ) )->update($Mapper );
        }

        $themeid = Auth::User()->themeid;

        if(isset($requestData['image']) && !empty($requestData['image']) ){
            
            // if($requestData['image'] == 's3.amazonaws.com/files.thalia-apps.com'){
            if($requestData['image'] == 'https://s3.us-east-1.amazonaws.com/files.thalia-apps.com/prime/banner.png'){

                $img2 = (file_get_contents($requestData['image']));
                $img = 'data:image/png;base64,' . base64_encode($img2);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/gif;base64,', '', $img);   
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').'ban'.$Productbanner_id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'banner';
                
                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Productbanner_id,$fileval,$themeid);

            }
            else if( strpos($requestData['image'], 's3.amazonaws.com/files.thalia-apps.com') == true ){

                $img2 = (file_get_contents($requestData['image']));
                $img = 'data:image/png;base64,' . base64_encode($img2);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/gif;base64,', '', $img);   
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').'ban'.$Productbanner_id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'banner';
                

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Productbanner_id,$fileval,$themeid);

            }elseif(  strpos($requestData['image'], 'img.icons8.com') == false ){
                $Mapper2 = array( );
                $imgformat = 'png';
                $img = $requestData['image'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                if(  strpos($requestData['image'], 'data:image/gif;base64,') !== false ){
                    $img = str_replace('data:image/gif;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'gif';
                    $imgformat = 'gif';
                    Productbanner::where( array('productbannerid'=>$Productbanner_id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }

                if(  strpos($requestData['image'], 'data:image/svg+xml;base64') !== false ){
                    $img = str_replace('data:image/svg+xml;base64,', '', $img);  
                    $Mapper2['imageformat'] = 'svg';
                    $imgformat = 'svg';
                    Productbanner::where( array('productbannerid'=>$Productbanner_id,'userid'=> Auth::User()->userid ) )->update($Mapper2);
                }
                // $img = str_replace('data:image/gif;base64,', '', $img); 
                // $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').'ban'.$Productbanner_id;
                //print_r($filePath);exit;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'banner';

                $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $Productbanner_id,$fileval,$themeid,$imgformat);

            }
            
            
        } 
        $this->updateShopifyLiquidContent( );

    	return array( 'status'=> true , 'msg'=>'Banner '.$strText.' successfully.', 'submitType' => $strText );
    }

    public function getRule( $rule ){
     	
        $array = array();

        foreach( $rule['conditiontype'] as $key => $value ) {

            if( is_array($value) && count(array_filter($value)) == count($value) ){

                $array[$key] = $value;
            }else{
                $array[$key] = $value;
                
            }
        }

        return $array;
    }

    public function copyBanners(Request $request){
        $requestData = $request->all();
        // dd( $requestData);
        // print_r($requestData['image']);exit;
        // dd($requestData);
        $response = Productbanner::where('productbannerid','=',$requestData['bannerid'] )->get()->toArray();
        // print_r($response);exit;
        unset($response[0]['productbannerid']);
        $newArray = $response[0];
       // print_r($newArray);exit();
        $new_banner_id = Productbanner::create( $newArray ); 
        $id = $new_banner_id->productbannerid;
        $themeid = Auth::User()->themeid;
        if( isset($requestData['image'])) {
            if( strpos($requestData['image'] , 'img.icons8.com') == false ){
                //dd($requestData['image']);
                $img2 = (file_get_contents($requestData['image']));
                $img = 'data:image/png;base64,' . base64_encode($img2);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace('data:image/gif;base64,', '', $img);   
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $filePath = env('S3_PATH').'ban'.$id;
                $image = Storage::disk('s3')->put($filePath, $data);
                Storage::disk('s3')->setVisibility($filePath, 'public');
                $fullpath = Storage::disk('s3')->url($filePath);
                $fileval = 'banner';

                // // $response = app('\App\Http\Controllers\ShopifyController')->createThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken,  $fullpath, $id,$fileval,$themeid);

            }
        }

        return array( 'id'=> $id , 'msg'=> 'Banner Copy successfully.');
    }

    public function deleteBanner(Request $request ){
        $requestData = $request->all();
        $themeid = Auth::User()->themeid;
        $banner = Productbanner::where('productbannerid','=',$requestData['bannerid'] )->get()->first();
        $filePath = env('S3_PATH').'ban'.$requestData['bannerid'];
        log::info($filePath);
        if(Storage::disk('s3')->exists( $filePath )){
            Storage::disk('s3')->delete($filePath);
            $response = app('\App\Http\Controllers\ShopifyController')->deleteThemeLiquidImage( Auth::user()->shop , Auth::user()->accesstoken, $requestData['bannerid'], $themeid,$fileval='banner',$banner['imageformat']);
        }
        $response = Productbanner::where('productbannerid','=',$requestData['bannerid'] )->delete();

        $this->updateShopifyLiquidContent( );

        return array( 'status'=> true ,'msg'=> 'Banner deleted successfully.' );
    }

    public function updateShopifyLiquidContent( $isbannershow=true){
        
        $userBannerActive = User::where(array('userid'=> Auth::User()->userid,'isbannershow'=> 1 ) )->first();
        if( !empty( $userBannerActive ) ){
            if(Auth::user()->planid > 2){

                $userBannerActive= $userBannerActive->toArray();
                // $date_from = time();
                $userBanners =  Productbanner::where( array('isactive'=>1,'isdeleted'=> 0,'userid'=> Auth::User()->userid ) )
                                            ->orderby('priority','asc')->get()->toArray();

                // print_R($userBanners);exit;

                foreach( $userBanners as $banners => $values ) {
                    $userBanners[$banners]["timezone"] = $userBannerActive['timezone'];
                    $userBanners[$banners]["planid"] = $userBannerActive['planid'];
                } 
                // echo "<pre>";
                // print_r($userBanners);exit;
                $themeid = $userBannerActive['themeid'];
            }else{
                $userBanners  = array();
                $themeid = $userBannerActive['themeid'];
            }

        }else{
            $userBanners  = array();
            $themeid = '';
        } 

        $bannercount = count($userBanners);

        $strLiquidContentCSS = View('liquid.primecss')->render();

        $response1 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidCss( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentCSS,$themeid);

        // print_r($response1);

        $strLiquidContentJS = View('liquid.primebanjs',array('userBanners'=> $userBanners,'livecounttime'=>Auth::user()->livecounttime,'setlivevisitcount'=>Auth::user()->setlivevisitcount,'weekdays'=>Auth::user()->weekdays,'dateformat'=>Auth::user()->dateformat))->render();

        $response2 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidbannerJs( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentJS,$themeid);

        if( $bannercount > 50 ) {

            $userBanners1 = array_slice($userBanners,0,50);
            $userBanners2 = array_slice($userBanners,50,50);
            
            $strLiquidContent = View('liquid.primeban',array('userBanners'=> $userBanners1,'bannercount'=> $bannercount  ) )->render();
            $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBanner( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid );
            

            $strLiquidContent2nd = View('liquid.primeban2',array('userBanners'=> $userBanners2,'bannercount'=> $bannercount ) )->render();
            $response2nd = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBanner2( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent2nd,$themeid);

            if( $bannercount > 100 ) {
                $userBanners3 = array_slice($userBanners,100,50);

                $strLiquidContent3rd = View('liquid.primeban3',array('userBanners'=> $userBanners3,'bannercount'=> $bannercount ) )->render();
                $response3rd = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBanner3( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent3rd,$themeid);
            }

            if( $bannercount > 150 ) {
                $userBanners4 = array_slice($userBanners,150,50);

                $strLiquidContent4th = View('liquid.primeban4',array('userBanners'=> $userBanners4,'bannercount'=> $bannercount ) )->render();
                $response4th = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBanner4( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent4th,$themeid);
            }

            if( $bannercount > 200 ) {
                $userBanners5 = array_slice($userBanners,200,50);

                $strLiquidContent5th = View('liquid.primeban5',array('userBanners'=> $userBanners5,'bannercount'=> $bannercount ) )->render();
                $response5th = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBanner5( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent5th,$themeid);
            }

        }else{
            $strLiquidContent = View('liquid.primeban',array('userBanners'=> $userBanners,'bannercount'=> $bannercount ) )->render();
            $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidBanner( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid);
        }

        

        

        return array( 'status'=> $response ,'msg'=> 'Theme \'primeban.liquid\' updated.');
    }

    public function updateBannerGroup( Request $Request ){

        $requestData = $Request->all();

        Productbanner::where(array('productbannerid'=>$requestData['productbannerid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'bannergroup'=> $requestData['group']) ); 
        
        $this->updateShopifyLiquidContent( );
        // $msg  = $requestData['isactive'] == 1 ? 'deactivated' :'activated';
        return array('status'=> true, 'msg'=> 'Group updated successfully.' );
    }

    public function postActivateBanner( Request $Request  ){

        $requestData = $Request->all();

        $bannerstatus = 1;
        if( $requestData['isShowBanner'] == 0 ){
            $bannerstatus = 0;
        }

        $strLiquidContent = View('liquid.primemeta')->render();
        $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidPrimeMeta( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,Auth::User()->themeid);

        User::where( array('userid'=> Auth::User()->userid ) )->update( array( 'isbannershow'=> $bannerstatus  ) );

        $this->updateShopifyLiquidContent( $bannerstatus );

        $msg  = $bannerstatus == true ? 'activated' :'deactivated';
        return array('status'=> true, 'msg'=> 'Banners '.$msg.' successfully.' );

    }

    public function updateBannerStatus( Request $Request ){

        $requestData = $Request->all();

        Productbanner::where(array('productbannerid'=>$requestData['productbannerid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'isactive'=> $requestData['isactive'] == 1 ? 0 :1 ) ); 
        
        $this->updateShopifyLiquidContent( );
        $msg  = $requestData['isactive'] == 1 ? 'deactivated' :'activated';
        return array('status'=> true, 'msg'=> 'Banner '.$msg.' successfully.' );
    }

    public function updateBannerPriority( Request $Request ){
        // print_r($Request);exit;
        $requestData = $Request->all();

        if( empty( $requestData['banner'] ) ){

            return array('status'=> false , 'msg'=> 'something went wrong.' );
        }
        
        $requestData = json_decode( $requestData['banner'] , true );

        foreach( $requestData as $bannerid => $priorityIndex ) {
            
            Productbanner::where(array('productbannerid'=>$bannerid ,'userid'=> Auth::User()->userid ) )->update( array( 'priority'=> $priorityIndex ) );  
        }
       
       $this->updateShopifyLiquidContent( );


        return array( 'status'=> true , 'msg'=> 'Banner priority updated successfully.');

    }

}