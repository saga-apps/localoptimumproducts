<?php
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Productbadge;
use App\Models\Producthighlight;
use App\Models\Highlightstyle;
use App\Models\LineItem;
use App\Models\Jsonorder;
use App\Models\OrderCount;
use App\Models\Currentusercron;
use App\Models\Lastusercron;
use App\Models\Theme;
use App\Models\Country;
use App\Models\Showcase;
use App\Models\Productnote;
use App\Models\Notestyle;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\User;
use Mail;
use Log;
use DateTime;
use GuzzleHttp\Client;


class NoteController extends Controller {

    public function postNote( Request $Request  ){

        $requestData = $Request->all();
        date_default_timezone_set(Auth::User()->timezone);
        if( empty( $requestData['description'] ) || empty( $requestData['productid']) ){
    		return array('status'=> false,'msg'=> 'Please Fill all the fields.');
        }
        
        $arrRuleMapper = array( );
        $arrRuleMapper['productid'] 	 =  $requestData['productid'];

        $arrRuleMapper['description'] 	 =  $requestData['description'];

        $arrRuleMapper['productname'] 	 =  $requestData['productname'];
        $arrRuleMapper['productimg'] 	 =  $requestData['productimg'];
        $arrRuleMapper['producthandle'] 	 =  $requestData['producthandle'];
        $arrRuleMapper['isactive']      = $requestData['active'];
    	$arrRuleMapper['userid'] 		= Auth::User()->userid;
    	$arrRuleMapper['createtime'] 	= date("Y-m-d H:i:s");

        $strText = 'created';
        $product = Productnote::where(array('userid'=>Auth::User()->userid,'productid'=>$requestData['productid'] ) )->first();
        // dd($arrRuleMapper);
        if( !empty( $requestData['productnoteid'] ) ){

            $existing = Productnote::where(array('productnoteid'=>(int)$requestData['productnoteid'],'userid'=> Auth::User()->userid ) )->first();
            if($existing->productid != $requestData['productid'] ){

                $total = Productnote::where(array('productid'=>$requestData['productid'],'userid'=> Auth::User()->userid ) )->get()->toArray();
                if(count($total) > 0){
                    $responsedel = Productnote::where('productnoteid','=',$requestData['productnoteid'] )->delete();
                    $responsedel2 = Productnote::where('productid','=',$requestData['productid'] )->delete();
                    $new_note_id = Productnote::create( $arrRuleMapper );
                    $Productnote_id = $new_note_id->productproductnoteid;

                }else{
                    Productnote::where( array('productnoteid'=>(int)$requestData['productnoteid'],'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
                }
                $delres = app('\App\Http\Controllers\ShopifyController')->deleteShopifyProductMetafiled( Auth::user()->shop , Auth::user()->accesstoken, $existing->noteonmetaid , $existing->contentmetaid);
            }else{
                Productnote::where( array('productnoteid'=>(int)$requestData['productnoteid'],'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
            }
            
            $strText = 'updated';
            $Productnote_id = $requestData['productnoteid'];
        }elseif(!empty( $product )){
            Productnote::where( array('productid'=>$requestData['productid'],'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
            $strText = 'created';
            $Productnote_id = $product->productnoteid;
        }
        else{

            $prioritynote = Productnote::where(array('userid'=>Auth::User()->userid,'productid'=>$requestData['productid'] ) )->orderby('priority','desc')->first();
            if( !empty( $prioritynote->priority ) ){
                $arrRuleMapper['priority'] = $prioritynote->priority+1;
            }
            
            $new_note_id = Productnote::create( $arrRuleMapper );
            
            $Productnote_id = $new_note_id->productproductnoteid;
        }
        $updated = 1;
        $this->updateShopifyLiquidContent($requestData['description'],$requestData['productid'],$requestData['active'], $updated );

    	return array( 'status'=> true , 'msg'=>'Note '.$strText.' successfully.', 'submitType' => $strText );
    }

    public function updateShopifyLiquidContent($description='' ,$productid='' ,$isactive=1, $updated=0 ){

        if($updated==1){
            $arrMetaFiled = array("namespace"=>"primen","key"=> "notecontent","value"=> $description,"value_type"=> "string");
            $params = array( 'metafield'=> $arrMetaFiled );

            $response = app('\App\Http\Controllers\ShopifyController')->putShopifyProductMetafiled( Auth::user()->shop , Auth::user()->accesstoken, $productid, $params);
            $arrRuleMapper = array( );
            $arrRuleMapper['contentmetaid'] =  $response['id'];

            Productnote::where( array('productid'=>(int)$productid,'userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
    
            $arrMetaFiled2 = array("namespace"=>"primen","key"=> "noteon","value"=> $isactive,"value_type"=> "integer");
            $params2 = array( 'metafield'=> $arrMetaFiled2 );
    
            $response2 = app('\App\Http\Controllers\ShopifyController')->putShopifyProductMetafiled( Auth::user()->shop , Auth::user()->accesstoken, $productid, $params2);

            $arrRuleMapper2 = array( );
            $arrRuleMapper2['noteonmetaid'] =  $response2['id'];

            Productnote::where( array('productid'=>(int)$productid,'userid'=> Auth::User()->userid ) )->update($arrRuleMapper2 );
        }

        $userNoteActive = User::where(array('userid'=> Auth::User()->userid,'isnoteshow'=> 1 ) )->first();
        if( !empty( $userNoteActive ) ){

            $userNoteActive= $userNoteActive->toArray();
            $userNotes =  Productnote::where( array('isactive'=>1,'userid'=> Auth::User()->userid ) )
                                ->orderby('priority','asc')->get()->toArray();

            foreach( $userNotes as $notes => $values ) {
                $userNotes[$notes]["timezone"] = $userNoteActive['timezone'];
                $userNotes[$notes]["planid"] = $userNoteActive['planid'];
            }
            $NoteStyle =  Notestyle::where( array('userid'=> Auth::User()->userid ) )->get()->first();
            
        }else{
            $userNotes  = array();
            $NoteStyle = array();
        } 
        if(!empty( $userNoteActive )){
            $themeid = $userNoteActive['themeid'];
        }else{
            $themeid = '';
        }

        $strLiquidContentCSS = View('liquid.primecss')->render();

        $response1 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidCss( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentCSS,$themeid);

        $strLiquidContentJS = View('liquid.primenjs',array('userNotes'=> $userNotes,'NoteStyle'=> $NoteStyle ))->render();

        $response2 = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidNoteJs( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContentJS,$themeid);
        
        $strLiquidContent = View('liquid.primen',array('userNotes'=> $userNotes,'NoteStyle'=> $NoteStyle ) )->render();
        $response = app('\App\Http\Controllers\ShopifyController')->updateThemeLiquidNote( Auth::user()->shop , Auth::user()->accesstoken,  $strLiquidContent,$themeid);
        
        return array( 'status'=> $response ,'msg'=> 'Theme \'primen.liquid\' updated.');
    
    }

    public function checkProductId( Request $Request  ){

        $requestData = $Request->all();
        $Productnote =  Productnote::where( array('productid'=>$requestData['productid'],'userid'=> Auth::User()->userid ) )
                                ->orderby('priority','asc')->get()->first();
        if(!empty($Productnote)){
            return array('status'=> true, 'msg'=> 'Notes exist' );
        }else{
            return array('status'=> true, 'msg'=> 'Notes not created' );
        }
        

    }

    public function checkEditProductId( Request $Request  ){
        $requestData = $Request->all();
        $Productnote =  Productnote::where( array('productid'=>$requestData['productid'],'userid'=> Auth::User()->userid ) )->where('productnoteid', '!=', $requestData['productnoteid'])->orderby('priority','asc')->get()->toArray();
        if(!empty($Productnote)){
            return array('status'=> true, 'msg'=> 'Notes exist' );
        }else{
            return array('status'=> true, 'msg'=> 'Notes not created' );
        }
        

    }

    public function deletenote(  Request $request ){
        $requestData = $request->all();
        
        $themeid = Auth::User()->themeid;
        $metaid = Productnote::where(array('userid'=>Auth::User()->userid,'productnoteid'=>$requestData['noteid'] ) )->first();
        $response = Productnote::where('productnoteid','=',$requestData['noteid'] )->delete();
        
        $delres = app('\App\Http\Controllers\ShopifyController')->deleteShopifyProductMetafiled( Auth::user()->shop , Auth::user()->accesstoken, $metaid->noteonmetaid , $metaid->contentmetaid);

        return array( 'status'=> true ,'msg'=> 'Note deleted successfully.' );
    }
    
    public function postActivateNote( Request $Request  ){

        $requestData = $Request->all();
        $highlightstatus = 1;
        if( $requestData['isShownote'] == 0 ){
            $highlightstatus = 0;
        }
        User::where( array('userid'=> Auth::User()->userid ) )->update( array( 'isnoteshow'=> $highlightstatus  ) );

        $updated = 0;
        $this->updateShopifyLiquidContent('','','', $updated );

        $msg  = $highlightstatus == true ? 'activated' :'deactivated';
        return array('status'=> true, 'msg'=> 'Notes '.$msg.' successfully.' );

    }

    public function updateNoteStatus( Request $Request ){
        $requestData = $Request->all();
        Productnote::where(array('productnoteid'=>$requestData['productnoteid'] ,'userid'=> Auth::User()->userid ) )->update( array( 'isactive'=> $requestData['isactive'] == 1 ? 0 :1 ) ); 

        $metadata =  Productnote::where( array('productnoteid'=>$requestData['productnoteid'] ,'userid'=> Auth::User()->userid ) )->get()->first();
                                
        $this->updateShopifyLiquidContent($metadata->description, $metadata->productid, $metadata->isactive, 1 );
        $msg  = $requestData['isactive'] == 1 ? 'Deactivated' :'Activated';
        return array('status'=> true, 'msg'=> 'Note '.$msg.' Successfully.' );
    }

    public function styleNote( Request $request ) {
        
        $requestData = $request->all();
        $arrRuleMapper = array( );
        $arrRuleMapper['style'] =  json_encode( $requestData['notestyle'] );
    	$arrRuleMapper['userid'] 		= Auth::User()->userid;
        $arrRuleMapper['appearance'] 	= $requestData['appearance'];
        
        Notestyle::where( array('userid'=> Auth::User()->userid ) )->update($arrRuleMapper );
        $strText = 'updated';

        $updated = 0;
        $this->updateShopifyLiquidContent('','','', $updated );

    	return array( 'status'=> true , 'msg'=>'Design '.$strText.' successfully.', 'submitType' => $strText );
    }

}