<?php

namespace App\Http\Controllers;

use App\Libraries\ShopifyClient;
use Illuminate\Http\Request;
use Auth;
use Log;
use Session;
use Redirect;
use Mail;
use URL;
use Response;
use Config;
use App\User;
use App\Models\LineItem;
use App\Models\Jsonorder;
use App\Models\OrderCount;
use App\Libraries\ShopifyApiException;
use Exception;
use Hash;
use App\Models\Webhook;
use App\Models\Errorlog;
use DateTime;
use App\Models\Plan;
use App;
use App\Models\Webhookredactlog;
use App\Models\Productbadge;
use App\Models\Producthighlight;
use App\Models\Productnote;
use App\Models\Highlightstyle;
use App\Models\Notestyle;
use App\Models\Productbanner;
use ZipArchive;
use File;


class ShopifyController extends Controller
{

    protected $shopifyClient = null;

    public function __construct() {

        if( !empty( Auth::user()->shop ) ){

            $this->shopifyClient = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken, env('shopify_api_key'), env('shopify_secret') );
        }

    }

    public function shopifymetauserprod($shop, $accesstoken, $orderid)
    {

        $sc = $this->setShopifyCredential( $shop, $accesstoken );
        return $sc->call('GET', '/admin/api/2021-01/products.json?limit=20');

        // $sc = $this->setShopifyCredential( $shop, $accesstoken );
        // return $sc->call('GET', '/admin/api/2021-01/products.json?since_id=6562452504712&limit=20');
        
    }

    public function shopifymetafileduserprod($shop, $accesstoken, $prodid)
    {
        $sc = $this->setShopifyCredential( $shop, $accesstoken );
        return $sc->call('GET', '/admin/products/'.$prodid.'/metafields.json');
    }

    public function shopifydelmetauserprod($shop, $accesstoken, $metaid)
    {
        $sc = $this->setShopifyCredential( $shop, $accesstoken );
        return $sc->call('DELETE', '/admin/api/2021-04/metafields/'.$metaid.'.json');
    }

    public function postShopifyProduct( $params,$taskid = null,$productid = null ,$user = array()){
        
        if(isset($params['product']['variants']) && count($params['product']['variants']) > 50 ) {
            log::info('Guzzle Post Pro');
        $url  = 'https://'.$user->shop.'/admin/api/'.Config::get('api.version').'/products.json';
        $request = $this->guzzleShopify($url,'POST',['json'=>$params] ,$user->accesstoken);//$sc->
        return $request;

        }
        try{
            
            if (empty(Auth::User())) {
               
                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                return $sc->call('POST', '/admin/api/'.Config::get('api.version').'/products.json', $params );

            }
            return $this->shopifyClient->call('POST', '/admin/api/'.Config::get('api.version').'/products.json', $params );

        } catch (Exception $e) {

            $error['taskid'] = $taskid;
            $error['productid'] = $productid;
            $error['error'] = method_exists($e, 'getResponse') ?  json_encode($e->getResponse()) : '';
            $error['method'] = 'postShopifyProduct';
            Errorlog::create($error);
            log::info('postShopifyProduct : '.json_encode($error));

            return false;
        }

    }

    public function deleteSmartCollection($smartcollectid,$user = array())
    {
        try{
            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
            }

            $response = $this->shopifyClient->call('DELETE', '/admin/api/'.Config::get('api.version').'/smart_collections/'.$smartcollectid.'.json');
            return $response;
        }catch (Exception $e) {
            log::info('----------------errror-----------------------'.$smartcollectid);
        }

    }

    //start get product collection
   public function getProductsbyCollection($params = array(), $collectionId) {
        try
        {
            try{

                // sleep(1);

                // echo "after delay";
                $sc = new ShopifyClient(Auth::user()->shop, Auth::user()->accesstoken, env('shopify_api_key'), env('shopify_secret'));
                $product = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/products.json?collection_id=' . $collectionId . '', $params);
                // $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken,env('shopify_api_key'), env('shopify_secret'));
                // $product = $sc->call('GET', '/admin/products.json?collection_id='.$collectionId.'', $params);
               
            } catch (ShopifyApiException $e) {
                return false;
            }
        }catch (Exception $e){
                Log::info('getProductInfo() - '.$e->getMessage());
            return false;
        }
        return $product;
    }
//end product collection


    public function putShopifyProduct( $productid, $params,$taskid = null,$dbproductid = null ,$user = array() ){
        
        if(isset($params['product']['variants']) && count($params['product']['variants']) > 50 ) {
            
        $url  = 'https://'.$user->shop.'/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json';
        log::info('guzzlepreputshopify'.$productid);
        $request = $this->guzzleShopify($url,'PUT',['json'=>$params] ,$user->accesstoken);//$sc->
        return $request;

        }

        try{

            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                return $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json', $params );

            }

            return $this->shopifyClient->call('PUT', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json', $params );

        } catch (Exception $e) {
            $error['taskid'] = $taskid;
            $error['productid'] = $dbproductid;
            $error['error'] = json_encode($e->getResponse());
            $error['method'] = 'putShopifyProduct';
            Errorlog::create($error);
            log::info('putShopifyProduct : '.json_encode($error));

            return false;
        }


    }

    public function setShopifyInventory($params,$taskid = null,$dbproductid = null ,$user = array())
    {

        try{
            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                 return $sc->call('POST', '/admin/api/'.Config::get('api.version').'/inventory_levels/set.json', $params );
            }

            return $this->shopifyClient->call('POST', '/admin/api/'.Config::get('api.version').'/inventory_levels/set.json', $params );
        } catch (Exception $e) {
            $error['taskid'] = $taskid;
            $error['productid'] = $dbproductid;
            $error['error'] = json_encode($e->getResponse());
            $error['method'] = 'putShopifyProduct';
            Errorlog::create($error);
            log::info('setShopifyInventory : '.json_encode($error));

            return false;
        }

    }

    public function putShopifyVariant($variantid, $params ,$user = array())
    {
        try{
            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                return $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/variants/'.$variantid.'.json', $params );
            }

            return $this->shopifyClient->call('PUT', '/admin/api/'.Config::get('api.version').'/variants/'.$variantid.'.json', $params );
        }catch (Exception $e) {
            
            $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
            
            log::info($error);

            if(isset($error['errors']) && $error['errors'] == 'Not Found') {
              
               Product::where('shopifyvariantid',$variantid)->update(array('isdeleted'=>1));
                
            }
            return false;
        }    

        

    }

    public function deleteShopifyVariant($productid,$variantid ,$user = array())
    {
        try{
            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
            }

            return $this->shopifyClient->call('DELETE', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'/variants/'.$variantid.'.json' );

        }catch (Exception $e) {
            log::info('----------------errror-----------------------'.$productid);
        }

    }

    public function deleteShopifyProduct($productid,$user = array())
    {
        try{
            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
            }

            return $this->shopifyClient->call('DELETE', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json' );

        }catch (Exception $e) {
            log::info('----------------errror-----------------------'.$productid);
            return false;
        }

    }


    public function getShopifyProduct( $productid ,$user = array()){
        try{

            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                return $sc->call('GET', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json' );
            }

            return $this->shopifyClient->call('GET', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json' );

        }catch (Exception $e) {
           
            
            $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
            
            if(isset($error['errors']) && $error['errors'] == 'Not Found') {

               Product::where('shopifyid',$productid)->update(array('isdeleted'=>1));
                
            }
            

            return false;
        }

    }

    public function redirectShopify($shopifyid ) {
        try
        {
            try{

                $sc = $this->setShopifyCredential( Auth::user()->shop, Auth::user()->accesstoken );

                $params = array();

                $product = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/products/'.$shopifyid.'.json?fields=handle', $params);
//log::info('redirect success');

//log::info($product);

            } catch (ShopifyApiException $e) {
            	return 'The product seems to have been deleted from Shopify.';
//log::info('redirect shopify error');
//                    $subject = 'redirectShopify function from shopifycontroller';
//                    $error = array(
//                        'name' => $subject,
//                        'error' => $e->getMessage(),
//                        'time' => time()
//                    );
//                    ErrorLog::postErrorLog($error);
                $prod = Product::where( array('userid' => Auth::user()->userid, 'shopifyid' => $shopifyid ) )->get( );

                $pid = $prod[0]->productid;

                if(empty($pid))
                {
                    return 'The product seems to have been deleted from Shopify.';
                }
                else
                {
                    return Response::view('errors.productdeleted',array('productid' => $pid, 'products'=>$prod[0]));
                }

//                    return Redirect::to('error.404');
//                    return '<p>The product seems to have been deleted from Shopify.</p>'
//                    . '<p>Do you want to remove it from Spreadr dashboard as well? <a class="btn btn-link" href="https://aa.com">Yes, remove</></p>';
            }

            return Redirect::to('http://'.Auth::user()->shop.'/products/'.$product['handle']);
        }  catch (Exception $ex)
        {

            $subject = 'redirectShopify main from shopifycontroller';
            Log::info( $subject );
// $error = array(
//     'name' => $subject,
//     'error' => $ex->getMessage(),
//     'time' => time()
// );
// ErrorLog::postErrorLog($error);
// return 'error - try again later'.$ex->getMessage();
        }
    }


//=== shopify app installation code ===


    
  public function sopifyInstallApp() {
    //log::info('sopifyInstallApp()');

    //die("gdgdf");

    //if code confirm page
    if (isset($_GET['code'])) {


      //log::info('sopifyInstallApp() code confirm');
      $code = isset($_GET['code']) ? $_GET['code'] : $_GET['code'];
      // Step 2: do a form POST to get the access token
      // $shopifyClient = new ShopifyClient($_GET['shop'], "", Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

      $shopifyClient = $this->setShopifyCredential( $_GET['shop'] ,'');
      // Now, request the token and store it in your session.
      $token = $shopifyClient->getAccessToken($code);
      Session::put('token', $token);

      if (Session::get('token') != '') {
        // log::info('token accepted');
        Session::put('shop', $_GET['shop'] );
        $userLogin = $this->startInstall();
      }
    }

    //if charge confirm page
    else if(isset($_GET['chargeshop'])){


      //check valid request
      if(true)
      {
        $success = $this->activateCharge($_GET['charge_id'], $_GET['chargeshop'], $_GET['user'], $_GET['tid']);

        //if charge activated
        if($success)
        {
          // log::info('test');
          log::info($_GET);
          // update current plan
          $businessUserData = array(
            'chargeid' => $_GET['charge_id'],
            'ispaymentok' => 1, // 1 = true, 0 =  false
            //'iswidsetupShopFilesetok' => 1,
            //'isadmsetupShopFilesnok' => 1,
            'istrial' => 0,
            'planid'  => 2,
            'planamount'  => $_GET['planamount']
          );

          User::where( array('userid' => $_GET['user'] ) )->update( $businessUserData );

          //log::info('charge confirm page - 1');
          //$this->setupShopFiles($_GET['user'], $_GET['chargeshop'], $_GET['tid']);
          //log::info('after setupShopFiles - 2');

          //log::info('initiate webhook create');

        //   $this->activateWebhook($_GET['user'], $_GET['chargeshop'], $_GET['tid'], 'themes/publish');

        //   $this->activateWebhook($_GET['user'], $_GET['chargeshop'], $_GET['tid'], 'app/uninstalled');

        //   $this->activateWebhook( $_GET['user'], $_GET['chargeshop'], $_GET['tid'], 'orders/paid');
        //   $this->activateWebhook( $_GET['user'], $_GET['chargeshop'], $_GET['tid'], 'orders/create');
        //   $this->activateWebhook( $_GET['user'], $_GET['chargeshop'], $_GET['tid'], 'orders/fulfilled');
          //log::info('webhook create function done');
          // update  js
          //                App::make('SettingsController')->postUpdateBusinessJs(array('user_id' => $_GET['user']));

          //goto dashboard
          $userLogin = User::find($_GET['user']);
          if (!empty($userLogin)) {
            Auth::login($userLogin);
            //fetch and update default locationID of new shop
             
            return Redirect::to('home');
          } else {
            return Redirect::to('/');
          }

          exit;

        }
        else
        {

          //log::info('charge rejected');
          // update current plan
          $data = array(
            'ispaymentok' => 0 // 1 = true, 0 =  false
          );

          User::where( array('userid' => $_GET['user'] ) )->update( $data );
          //Billing::putCurrentPlan(array('buser_id' => $_GET['user']), $data);

          return Redirect::to('/');

        }
      }

      //check if charge accepted or rejected
    }

    // else if install/reinstall click or banner click
    else {

      //                    foreach ($_GET as $key => $value){
      //                            log::info($key.":". $value);
      //                        };

      $shop = $_GET['shop'];


      //check if new shop?
      $userShop = User::where( array('shop' => $shop ) )->get()->toArray();

      if( !empty( $userShop ) )
      {
        //log::info('shop exists');
        //log::info('shop name'.$shop);
        $User = User::where( array('shop' => $shop ) )->first(); log::info("tetete"); log::info($User['userid']);
        // $this->activateWebhook(  $User['userid'],  $User['shop'], $User['accesstoken'], 'orders/paid');
        // $this->activateWebhook(  $User['userid'],  $User['shop'], $User['accesstoken'], 'orders/fulfilled');
        // $this->activateWebhook(  $User['userid'],  $User['shop'], $User['accesstoken'], 'orders/create');
        // //log::info('user id '.$User['userid']);
        //$this->addscriptexit($User['userid']);
        $isInstalled = true;
        $accessinfo =$User; //App::make('UserController')->getToken($User['userid']);

        //if installed but charge not accepted

        if($accessinfo['ispaymentok'] == 0 && $accessinfo['istrial'] != 1)
        {
          $isInstalled = false;
        }
        else
        {
          try{

            // $shopifyClient = new ShopifyClient($shop, $accessinfo['accesstoken'], Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

            $shopifyClient = $this->setShopifyCredential( $shop, $accessinfo['accesstoken'] );

            $customerInfo = $shopifyClient->call('GET', '/admin/api/'.Config::get('app.shopify_api_version').'/shop.json');
            if(empty($customerInfo['id']))
            {
              $isInstalled = false;
            }

          } catch (ShopifyApiException $e) {

            $isInstalled = false;
          }

        }

        //check if un installed
        if($isInstalled)
        {
          // log::info('ininstallpm');
          //goto dashboard
          $userLogin = User::find($User['userid']);
          $userLogin->isinstalled = 1;
          $userLogin->save();

          // $products = Product::where('userid',$userLogin->userid)->where('producttype','!=',0)->where('mwsstatus','!=',2 )->update(['isautosyncon' => 0]);
          // $products = Product::where('userid',$userLogin->userid)->whereIn('pushsyncflag',array(1,3) )->where('producttype','!=',0)->where('mwsstatus','!=',2 )->take($userLogin->productcount)->update(['isautosyncon' => 1]);


          //log::info($userLogin->toArray());

          if (!empty($userLogin)) {

            if($userLogin->istrial == 1) {

              // $sc = new ShopifyClient( $userLogin->shop, $userLogin->accesstoken,Config::get('app.shopify_api_key'), Config::get('app.shopify_secret') );

              $sc = $this->setShopifyCredential( $userLogin->shop, $userLogin->accesstoken );

              $customerInfo = $sc->call('GET', '/admin/api/'.Config::get('app.shopify_api_version').'/shop.json');
              //paymentflow - if user db istrial is true check for shopify plan
              // && $customerInfo['plan_name'] != 'affiliate'
              //for free app if($customerInfo['plan_name'] != 'trial'){
              if(0){

                //paymentflow - if istrial true and shopify plan name is not trial charge user

                $charge = $this->createCharge($userLogin->userid,strtotime($userLogin->createtime) , $customerInfo ,$userLogin);


              } else {

                Auth::login($userLogin);
             
                return Redirect::to('home');

              }
            } else {

              Auth::login($userLogin);
       
              return Redirect::to('home');
            }

          } else {
            return Redirect::to('/');
          }

        }
        else
        {
          // log::info('inmaybeunistall');
          //goto dashboard
          $userLogin = User::find($User['userid']);
          $userLogin->planamount = 0;
          $userLogin->istrial = 1;
          $userLogin->planid = 1;
          $userLogin->save();

          //reinstall
          // $shopifyClient = new ShopifyClient($shop, "", Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

          $shopifyClient = $this->setShopifyCredential( $shop, "" );

          // Step 1: verify shop and Authorize login
          // get the URL to the current page
          //$pageURL = Request::url();
          $pageURL =  URL::current();
          $redirecturl = $shopifyClient->getAuthorizeUrl(env('shopify_scope'), $pageURL);


          header("Location: " . $shopifyClient->getAuthorizeUrl(env('shopify_scope'), $pageURL) );

        }

      }

      else
      {
        //create new shop
        //log::info('create new shop');

        // $shopifyClient = new ShopifyClient($shop, "", Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

        $shopifyClient = $this->setShopifyCredential( $shop, "" );

        // Step 1: verify shop and Authorize login
        // get the URL to the current page
        $pageURL = URL::current();
        //log::info('page url - '.$pageURL);

        header("Location: " . $shopifyClient->getAuthorizeUrl(env('shopify_scope'), $pageURL));
      }
      exit;

    }
  }


public function startInstall() {
    log::info('start install function');
    $sc = $this->setShopifyCredential( Session::get('shop') ,Session::get('token') );
    // 1 Get Shop owner details
    $customerInfo = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/shop.json');
    $bsuer = array();
    $createddate;

    //check shop exists in db
    $shopData = User::Where( array('shop'=> $customerInfo['myshopify_domain'] ) )->first();
    if( !empty( $shopData ) ){
        $bsuer = $this->updateShopData($customerInfo);
        $createddate = strtotime( $bsuer["createtime"] );
    }else{
        $bsuer = $this->insertShopData($customerInfo);
        $createddate = time();
        $currency['basecurrency'] = $customerInfo['currency'];
        $currency['timezone'] = $customerInfo['iana_timezone'];

        //send welcome email
        // $subject = 'Welcome to Saga Product Badges & Labels';
        // $email_id = 'sagashopifyapps@gmail.com';

        // if( strpos(env('APP_URL'), 'localhost') == FALSE ){

        //    Mail::send('mail.welcome', array('param' => array()), function($message) use($subject, $email_id, $customerInfo) {
        //                                 $message->from($email_id, 'Saga')->subject($subject);
        //                                 $message->to($customerInfo['email']);
        //                                 $message->replyTo('sagashopifyapps@gmail.com');
        //                             }); 
        // }
        
    }

    $user = User::find($bsuer['userid']);
    if($customerInfo['plan_name'] == 'trial' ){
        $userid = $user->userid;
        Auth::loginUsingId($userid, true);
        $user->istrial = 1;
        $user->ispaymentok = 0;
        $user->isinstalled = 1;
        $user->planid = 2;
        $user->save();
        $pageURL = URL::to('/dashboard');
        $shopifyClient = $this->setShopifyCredential( $user->shop, "" );
        $pageURL = URL::current();
        $pageURL = $pageURL.'?shop='.$user->shop;
        $this->activateWebhook( $user->userid, $user->shop, $user->accesstoken, 'app/uninstalled');
        header("Location: " . $pageURL);
        exit();
    }else {
        $user = User::find($bsuer['userid']);
        $user->istrial = 0;
        $user->ispaymentok = 0;
        $user->save();
        $this->createCharge($bsuer["userid"],$createddate , $customerInfo );
    }

}


public function createCharge($userid, $createddate , $customerInfo=null,$default = null) {
 try {


//log::info('create charge function');
// $sc = new ShopifyClient(, Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

    $sc = $this->setShopifyCredential( Session::get('shop'), Session::get('token') );

//            $sc = new ShopifyClient(Auth::user()->domain, Auth::user()->access_token, Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

    if($default == null)  {
// $sc = new ShopifyClient(Session::get('shop'), Session::get('token'), Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));
        $shop  = Session::get('shop');
        $accesstoken  = Session::get('token');

        $sc = $this->setShopifyCredential( Session::get('shop'), Session::get('token') );

    } else {
// $sc = new ShopifyClient($default->shop, $default->accesstoken, Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));
$shop  = $default->shop;
$accesstoken  = $default->accesstoken;

        $sc = $this->setShopifyCredential( $default->shop, $default->accesstoken );
    }

    if (strpos(env('APP_URL'), 'localhost') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
        $test = true;
    }else if( !empty( $customerInfo['plan_name'] ) && $customerInfo['plan_name'] =='staff_business' ) {
        $test = true;
    }else{
        $test = false;
    }

  



$user = User::where('userid',$userid)->first();

$price = '5';

    
   //get trial days
   $now = time(); // or your date as well
   //$your_date = strtotime("2010-01-01");
   $datediff = $now - $createddate;
   
   $days = floor($datediff / (60 * 60 * 24));
   
   $trialdays = 7 - $days;
   $terms = "$".$price." per month after ".$trialdays." days";
   if($trialdays < 0)
   {
       $trialdays = 0;
       $terms = "$".$price." per month (trial expired)";
   }


$params = array(
    "recurring_application_charge" => array(
        "name" => "Basic Plan",
        "price" => $price,
        "return_url" =>env('APP_URL') ."/shopifyappinstall?&action=rcharge&chargeshop=".$shop.'&user='.$userid.'&tid='.$accesstoken.'&planamount='.$price,
        "test" => $test,
        "trial_days" => $trialdays, 
       
    )
);
$charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/recurring_application_charges.json', $params);

log::info('charge post success');
            //  $json = json_decode($charges, true);

            //    foreach ($charges as $key => $value){
            //        log::info($key.":". $value);
            //    };

// echo "<script>window.top.location.href = '" . $charges['confirmation_url'] . "'</script>";

            // Redirect::to($charges['confirmation_url']);

        } catch (ShopifyApiException $e) {
            log::info('first charge unsuccessful');
        $subject = 'getShopifyRecurringPayment function from shopifycontroller';
        $error = array(
            'name' => $subject,
            'error' => $e->getMessage(),
            'time' => time()
        );
        // ErrorLog::postErrorLog($error);
        return false;
        }
        //echo('shubhangi');
        header("Location: " . $charges['confirmation_url']);
        exit();
    }

public function insertShopData($customerInfo){

    try{
//log::info('insert customer info');

        $genPassword = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $baredomain = preg_split('/(?=\.[^.]+$)/', $customerInfo['domain']);

        $businessUserData = array(
            'email'       => $customerInfo['email'],
            'shop'        => $customerInfo['myshopify_domain'],
            'name'        => $customerInfo['shop_owner'],
            'timezone'       => $customerInfo['iana_timezone'],
            'accesstoken' => Session::get('token'),
            'password'    => Hash::make($genPassword),
            'createtime'  => date('Y-m-d H:i:s'),
            'isinstalled' => 1,
            'shopifyplanname' => $customerInfo['plan_name']
        );
//log::info('success first insert into business user table');

        $getLastId = User::create( $businessUserData );

        $locationData = $this->getLocations( Session::get('token') , $customerInfo['myshopify_domain'] );

             // print_r( );
             // die;

             // if( !empty( $locationData ) && is_array( $locationData ) ){

             //    $arrSettingData = array('location'=>$locationData['name'], 'locationid'=> $locationData['id'] );

             //    Usersetting::updateOrCreate( array( 'userid'=>$getLastId->userid ), $arrSettingData );
             // }

//insert into autosync table
//  $autosyncData = array(
//        'userid' => $getLastId['userid']
//    );

// $autosyncid = Autosync::postAutosync($autosyncData);

// $markupid = Markup::postMarkup($autosyncData);

        return array('userid' => $getLastId->userid );

    } catch (ShopifyApiException $e) {
        log::info('error in insert user function');
// $subject = 'Shopify App installion';
// $error = array(
//     'name' => $subject,
//     'error' => $e->getMessage(),
//     'time' => time()
// );
// ErrorLog::postErrorLog($error);

    }
}

public function updateShopData($customerInfo){

    try{

        $http = 'http://';
        if ($customerInfo['force_ssl'] == true) {
            $http = 'https://';
        }

// update user / shop data
        $businessUserData = array(
            'email' => $customerInfo['email'],
            'name' => $customerInfo['shop_owner'],
            'accesstoken' => Session::get('token'),
            'planid' => 2,
            'ispaymentok' => 0,
            'isinstalled' => 1,
            'shopifyplanname' => $customerInfo['plan_name']
        );

// User::putUser(array('userid' => $User['userid']), $businessUserData);

        User::Where( array('shop'=> $customerInfo['myshopify_domain'] ) )->update( $businessUserData );

        $User = User::Where( array('shop'=> $customerInfo['myshopify_domain'] ) )->first()->toArray();

        $locationData = $this->getLocations( Session::get('token') , $customerInfo['myshopify_domain'] );

        // if( !empty( $locationData ) && is_array( $locationData ) ){

        //     $arrSettingData = array('location'=>$locationData['name'], 'locationid'=> $locationData['id'] );

        //     Usersetting::updateOrCreate( array( 'userid'=>$User['userid'] ), $arrSettingData );
        //  }

        return array('userid' => $User['userid'], 'createtime' =>$User['createtime'] );

    } catch (ShopifyApiException $e) {
        $subject = 'updateShopData';
        
// $error = array(
//     'name' => $subject,
//     'error' => $e->getMessage(),
//     'time' => time()
// );
// ErrorLog::postErrorLog($error);

    }
}

public function setupShopFiles($user_id, $shop, $token){
    try{
        $tags = array();
        $sc = $this->setShopifyCredential( $shop, $token );

        // Get all themes
        $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
        $themeID = $theme[0]['id']; 
        $mobileThemeID = 0;
        
        //find the current theme and get theme id
        foreach ($theme as $uniquetheme) {

            if($uniquetheme['role']=='main')
            {
                $themeID = $uniquetheme['id'];
            }
            elseif ($uniquetheme['role']=='mobile')
            {
                $mobileThemeID = $uniquetheme['id'];
            }
        }
        log::info("themeid=".$themeID);

    }
    catch (ShopifyApiException $e) {
        log::info('error in setupShopFiles function');
        $subject = 'Shopify App installion - setupShopFiles function';
        

        return false;

    }
    return true;
}

public function webhookreplyback() {
    
    ob_start();
    echo "Ok";
    $size = ob_get_length();
    header('Content-type: application/json');
    header("Content-Length: {$size}");
    header("Connection: close");
    ob_end_flush();
    ob_flush();
    flush();
}

public function activateCharge($charge_id, $shopname, $user_id, $token) {


    try {
        if (strpos(env('APP_URL'), 'localhost') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
            $test = true;
        }else{
            $test = false;
        }

//$arrUser = User::getUser( array('userid'=> trim($user_id) ) );

        $arrUser = User::Where( array('userid'=> trim($user_id) ) )->get();

        if( !empty( $arrUser[0]->shopifyplanname ) && $arrUser[0]->shopifyplanname == 'staff_business') {
            $test = true;
        }

//$User = App::make('UserController')->getToken($user_id);

// $sc = new ShopifyClient($shopname, $token, Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));
        $sc = $this->setShopifyCredential( $shopname, $token );

//Active shopify recurring payment
        $params = array(
            "recurring_application_charge" => array(
                "return_url" => env('APP_URL') . "dashboard",
                "status" => "accepted",
                "test" => $test
            )
        );
        $charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/recurring_application_charges/' . $charge_id . '/activate.json', $params);

// log::info('charge activated success - '.$charges['status']);
//              $json = json_decode($charges, true);

//                foreach ($charges as $key => $value){
//                    log::info($key.":". $value);
//                };

        if($charges['status'] == 'active')
        {
            return true;
        }
        else
        {
            return false;
        }

    } catch (ShopifyApiException $e) {
        $subject = 'charge not activated';
        Log::info('Charge not activated');
// $error = array(
//     'name' => $subject,
//     'error' => $e->getMessage(),
//     'time' => time()
// );
// ErrorLog::postErrorLog($error);
        return false;
    }
}

public function getUpdatecharge(Request $request) {
            
    
    $planId = $request->input('plan_id');
    return ($this->createupdateCharge($planId));

}

   
public function setShopifyCredential( $shop , $accesstoken ){

    return $this->shopifyClient = new ShopifyClient($shop, $accesstoken, env('shopify_api_key'), env('shopify_secret') );
}


public function activateWebhook($userid, $shopname, $token, $webhookname) {
    try {
    log::info('start activateWebhook');
        try{

// $sc = new ShopifyClient($shopname, $token, Config::get('app.shopify_api_key'), Config::get('app.shopify_secret'));

            $sc = $this->setShopifyCredential( $shopname, $token );

            $webhookParams = array(
                "webhook" => array(
                    "topic" => $webhookname,
                    "address" => env('APP_URL') . "shopify/webhooks/" . $webhookname,
                    "format" => "json"
                )
            );
// 
            $webhook = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/webhooks.json', $webhookParams);
            $webhookId = $webhook['id'];

            //log::info($webhook);

           

        } catch (ShopifyApiException $e) {
            log::info('webhook creating error');
// $subject = 'webhook creating error';
// $error = array(
//     'name' => $subject,
//     'error' => $e->getMessage(),
//     'time' => time()
// );
// ErrorLog::postErrorLog($error);
            return false;
        }
 //log::info('$webhookId - '.$webhookId.','.$userid.','.$shopname.','.$webhookname.','.time());

        
	

	$Webhookdata = array(
            'userid' => $userid,
            'shop' => $shopname,
            'webhookid' => $webhookId,
            'topic' => $webhookname,
	    'time' => time()
        );  
 

	//log::info( $Webhookdata );
        Webhook::create($Webhookdata); 

        return true;

    } catch (Exception $exc) {
        Log::error('webhook add error');
        $subject = 'webhook add error';
// $error = array(
//             'name' => $subject,
//             'error' => $exc->getMessage(),
//             'time' => time()
//         );
//  ErrorLog::postErrorLog($error);
        return false;
    } 
}

    public function getLocations( $accesstoken, $shop,$isSingleLocation=true ){

        try {

            $sc = new ShopifyClient( $shop,$accesstoken, env('shopify_api_key'), env('shopify_secret') );

            $location = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/locations.json');

            if( !empty( $location[0]['id'] ) && true == $isSingleLocation ){
                return $location[0];
            }
            return $location;

        }catch (Exception $exc) {

          return false;
        }
    }

    public function putShopMetafield($userid =  null)
    {
        if($userid == '') {
            $userid = Auth::Id();
        }

        $user = User::find($userid);
        
        $mwscredential = Mwscredential::where('userid',$userid)->first();
        if(!empty($mwscredential)) {
            
            $metafield['namespace'] = 'connectr';
            $metafield['key'] = 'sellerid';
            $metafield['value'] = $mwscredential->sellerid;
            $metafield['value_type'] = 'string';
           


            $params['metafield'] = $metafield;
            try{

        
                if (empty(Auth::User())) {
                
                    $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                     $sc->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $params );

                }
                 $this->shopifyClient->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $params );

            }catch (Exception $e) {
            
                
                $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
                
                return false;
                    
            }

            //region

            $metafield['namespace'] = 'connectr';
            $metafield['key'] = 'region';
            $metafield['value'] = $mwscredential->region;
            $metafield['value_type'] = 'string';
           


            $params['metafield'] = $metafield;
            try{

        
                if (empty(Auth::User())) {
                
                    $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                     $sc->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $params );

                }
                 $this->shopifyClient->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $params );

            }catch (Exception $e) {
            
                
                $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
                
                return $false;
                    
            }


            //region

            $metafield['namespace'] = 'connectr';
            $metafield['key'] = 'userid';
            $metafield['value'] = $userid;
            $metafield['value_type'] = 'string';
           


            $params['metafield'] = $metafield;
            try{

        
                if (empty(Auth::User())) {
                
                    $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                     $sc->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $params );

                }
                 $this->shopifyClient->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $params );

            }catch (Exception $e) {
            
                
                $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
                
                return $false;
                    
            }



            //js

            $script_tag['event'] = 'onload';
            $script_tag['src'] = 'https://files.thalia-apps.com/connectr/'.env('APP_ENV').'/js/connectr.js';

            $params['script_tag'] = $script_tag;
            try{

                

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                $scripts =  $sc->call('GET', '/admin/api/'.Config::get('api.version').'/script_tags.json');

                
                if(!empty($scripts)) {
                    
                    foreach($scripts as $script) {
                        $script =  $sc->call('DELETE', '/admin/api/'.Config::get('api.version').'/script_tags/'.$script['id'].'.json' );
                    }
                   
                }


            }catch (Exception $e) {
            
                
                $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
                log::info($error);
                return $false;
                    
            }

            return true;

        }
    }



    
    public function putProductMetafield($productid,$userid =  null)
    {
       

        $metafield = array();
        $metafield['namespace'] = 'connectr';
        $metafield['key'] = 'isconnectr';
        $metafield['value'] = true;
        $metafield['value_type'] = 'string';

        $metafields[] = $metafield;


        $productasin = Product::where('producttype','!=',1)->where('shopifyid',$productid)->first();
      

        $metafield = array();
        $metafield['namespace'] = 'connectr';
        $metafield['key'] = 'parentasin';
        $metafield['value'] = $productasin->asin;
        $metafield['value_type'] = 'string';

        $metafields[] = $metafield;

        if($userid != '') {
            $user = User::find($userid);
        }else {
            $user = Auth::User();
        }
       
        
      
        $params['product']['metafields'] = $metafields;
        try{

    
            $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
            $d = $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'.json', $params );
          
        }catch (Exception $e) {
        
            
            $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
            \Log::info($error);
            return false;
                
        }
    }

    public function putVairantMetafield($productid,$userid = null)
    {   
        log::info('1146'.$productid);

        if($userid != '') {
            $user = User::find($userid);
        }else {
            $user = Auth::User();
        }


        $products = Product::select('shopifyvariantid','asin')->where('shopifyvariantid','!=',null)->where('shopifyid',$productid)->get()->toArray();
       
        if(!empty($products)) {
           foreach($products as $pkey => $product) {
               $fproducts[$product['shopifyvariantid']] = $product['asin'];

               
           }

            $metafield['namespace'] = 'connectr';
            $metafield['key'] = 'variantasins';
            $metafield['value'] = json_encode($fproducts);
            $metafield['value_type'] = 'string';
            
            

            $params['metafield'] = $metafield;
            try{
                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
                $sc->call('POST', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'/metafields.json', $params );
          
            }catch (Exception $e) {
        
            
                $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
                \Log::info($error);
                return false;
                    
            }
            
           
        }
        
    }

    public function deleteShopifyProductVarient($productid,$variantid, $user = array())
    {
        try{
            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential( $user->shop, $user->accesstoken );
            }

            return $this->shopifyClient->call('DELETE', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'/variants/'.$variantid.'.json' );

        }catch (Exception $e) {
            
            return false;
        }

    }

    public function getWebhooktAppUninstalled() {

        try {
            log::info('firing webhook theme uninstall');
            header('HTTP/1.0 200 OK');
            
            if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
                $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
                $header_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
                $User = User::where( array('shop' => $header_domain ) )->first()->toArray();

                $userdata = array(
                    'isinstalled'=>0,
                    'ispaymentok'=>0,
                    'istrial' => 0
                );

                User::where( array('shop' => $header_domain ) )->update(  $userdata );
                $user =  User::where( array('shop' => $header_domain ) )->first();
                $userdata = array('issync'=>0);
                //send uninstall email

                // $subject = 'Oops! We goofed up :(';
                // $email_id = 'gaurav@prime.getinstashop.com';
                // $customerInfo = array(
                //     'email' => $User['email']
                // );
               
		        // Mail::send('mail.uninstall', array('param' => array()), function($message) use($subject, $email_id, $customerInfo) {
                //                                          $message->from($email_id, 'Gaurav')->subject($subject);
                //                                          $message->to($customerInfo['email']);
                //                                          $message->replyTo('support@getinstashop.freshdesk.com');
                //                                      }); 

                return 'success';
            }
            else{
                return 'Request not from shopify';
            }
            exit();
        } catch (Exception $exc) {
            $subject = 'webhook app uninstalled';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
            Log::info( $subject.'==='.json_encode( $error ) );
        } 
    }

    public function getWebhookthemepublish() {
     
        try {

            header('HTTP/1.0 200 OK');
            log::info('firing webhook theme publish');
           

            if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
                
                // log::info($_SERVER);
                //log::info('----------------------');
                $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
                $header_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
 

                // $User = App::make('UserController')->getUser($header_domain);
                $User = User::where( array('shop' => trim($header_domain) ) )->first();
                // log::info('userid - '.$User['userid']);
                if( empty( $User->userid ) ){
                   return 'success'; 
                }

                $this->webhookreplyback("send response 200");
                $this->setupShopFiles( $User->userid, $header_domain, $User->accesstoken );
                 //log::info('done');
                return 'success';
            }
            else {
                    return 'Request not from shopify';
            }


            exit();

        } catch (Exception $exc) {
//            Log::error('error');
            $subject = 'webhook theme publish';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
           Log::info( $subject.'==='.json_encode( $error ) );
        }

        return 'success';
    }

    public function customersRedact( ){
        header('HTTP/1.0 200 OK');

         $requestData = $_POST;
        //$_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] = "dev35.myshopify.com";
        if( !empty( $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] ) ){

            $header_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
            $subject  = 'Customers Redact Webhooks | Star app ';
            $email_id = 'sagashopifyapps@gmail.com';
            $toEmail  = 'sagashopifyapps@gmail.com';

            if( !empty( $requestData ) ){
                $requestData = json_encode( $requestData );
            }else{
             $requestData = '';   
            }
            $arrrData = array( 
                                'topic'      => 'Customers Redact',
                                'shop'       => $header_domain,
                                'data'       => $requestData,
                                'createdat' => date("Y-m-d H:i:s")
                                );

            Webhookredactlog::create( $arrrData );

            $param    = array( 
                                'topic'=> 'Customers/redact',
                                'shop'=> $header_domain,
                            );

            Mail::send('mail.shopredact', $param, function($message) use($subject, $email_id ,$toEmail ) {

                $message->from($email_id, 'Star Customers/redact ')->subject($subject);
                $message->to( $toEmail );
                $message->replyTo('sagashopifyapps@gmail.com');
            });

        }

        return 'success';
        exit();
    }

    public function shopRedact( ){
        
        header('HTTP/1.0 200 OK');
        $requestData = $_POST;

        if( !empty( $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] ) ){

            $header_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
            // $subject  = 'Shop Redact Webhooks | Spreadr';
            // $email_id = 'sagashopifyapps@gmail.com';
            // $toEmail  = 'sagashopifyapps@gmail.com';

            if( !empty( $requestData ) ){
                $requestData = json_encode(  $requestData );
            }else{
             $requestData = '';   
            }
            $arrrData = array( 
                                'topic'      => 'Shop Redact',
                                'shop'       => $header_domain,
                                'data'       => $requestData,
                                'createdat' => date("Y-m-d H:i:s")
                                );

            Webhookredactlog::create( $arrrData );

            // $param    = array( 
            //                     'topic'=> 'shop/redact',
            //                     'shop'=> $header_domain,
            //                 );

            // Mail::send('shopredact', $param, function($message) use($subject, $email_id ,$toEmail ) {

            //     $message->from($email_id, 'Saga')->subject($subject);
            //     $message->to( $toEmail );
            //     $message->replyTo('support@connectr.freshdesk.com');
            // });

        }
        

        return 'success';
        exit();
    }


     public function orderPaidWebhook( ){
        
        header('HTTP/1.0 200 OK');
        $requestData = $_POST;

        Log::info("Order paid webhook fired");

        // if( !empty( $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] ) ){

        //     $header_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
        //     // $subject  = 'Shop Redact Webhooks | Spreadr';
        //     // $email_id = 'sagashopifyapps@gmail.com';
        //     // $toEmail  = 'sagashopifyapps@gmail.com';

        //     if( !empty( $requestData ) ){
        //         $requestData = json_encode(  $requestData );
        //     }else{
        //      $requestData = '';   
        //     }
        //     $arrrData = array( 
        //                         'topic'      => 'Shop Redact',
        //                         'shop'       => $header_domain,
        //                         'data'       => $requestData,
        //                         'createdat' => date("Y-m-d H:i:s")
        //                         );

        //     Webhookredactlog::create( $arrrData );

        //     // $param    = array( 
        //     //                     'topic'=> 'shop/redact',
        //     //                     'shop'=> $header_domain,
        //     //                 );

        //     // Mail::send('shopredact', $param, function($message) use($subject, $email_id ,$toEmail ) {

        //     //     $message->from($email_id, 'Saga')->subject($subject);
        //     //     $message->to( $toEmail );
        //     //     $message->replyTo('support@connectr.freshdesk.com');
        //     // });

        // }
        

        return 'success';
        exit();
    }


    public function updateLiquid($userid) {

        header('Access-Control-Allow-Origin: *');
        try {
                
                $objUser = User::where( array('userid' => $userid ) )->first();

                    try {
                        
                        $sc = $this->setShopifyCredential( $objUser->shop, $objUser->accesstoken );

                        $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
                        $themeID = $theme[0]['id'];
                        $mobileThemeID = 0;

                        //find the current theme
                        foreach ($theme as $uniquetheme) {

                            if($uniquetheme['role']=='main')
                            {
                                $themeID = $uniquetheme['id'];
                            }
                            elseif ($uniquetheme['role']=='mobile')
                            {
                                $mobileThemeID = $uniquetheme['id'];
                            }
                        }

                        $usersetting = Usersetting::where('userid',$userid)->first();
                        $tags   = array('amazontag'=>!empty( $usersetting->affiliatetag ) ? $usersetting->affiliatetag :'','buttontext'=> !empty( $usersetting->button ) ? $usersetting->button :''  );

                        $tags['connectr-enable'] = 0;

                        if( !empty( $usersetting->isaffiliate ) ){
                           $tags['connectr-enable'] = 1; 
                        }
                        
                        //create spreadr.liquid file from shpify
                        $pageContent =  $this->getShopifyLiquidContainer($userid, $tags );

                        $spreadrCustomContent = '';//$this->getSpreadrCustomContainer($user_id,$tags);

                         //log::info($pageContent);
                        $params1 = array(
                            "asset" => array
                                (
                                "key" => "snippets/connectr.liquid",
                                "value" => $pageContent
                            )
                        );
                        $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $params1);

                        $params2 = array(
                            "asset" => array
                                (
                                "key" => "snippets/connectr-custom.liquid",
                                "value" => $spreadrCustomContent
                            )
                        );
                        $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $params2);

                         //update if seperate mobile theme
                        if($mobileThemeID > 0)
                        {
                            $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $mobileThemeID . '/assets.json', $params1);
                            $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $mobileThemeID . '/assets.json', $params2);
                        }

                    } catch (Exception $e) {

                        $subject = 'updateLiquid() error '.$objUser->shop;
                        $error = array(
                            'name' => $subject,
                            'error' => $e->getMessage(),
                            'time' => time()
                        );
                       Log::info( json_encode( $error ) );
                    }
                } catch (ShopifyApiException $e) {
                $subject = 'Update Twl liquidPage update';
                $error = array(
                    'name' => $subject,
                    'error' => $e->getMessage(),
                    'time' => time()
                );

                Log::info( json_encode( $error ) );
                //ErrorLog::postErrorLog($error);
                return;
            }
        
    }

    public function getShopifyLiquidContainer($buserid,$tags) {
        
        $cartadd = "'/cart/add'";
        $submit = "'submit'";
        //log::info('tags - '. $tags);
        $container = '{% if template contains "product" %}

        {% assign connectr-enable = "'.$tags["isredirect"].'" %}

         <button id="ConnectrLink" type="button" style="display:none;" name="ConnectrLink" class="btn"
                            onclick="ConnectrButtonClick()"
                            >
                      <span id="ConnectrLinkText">'.$tags["buttontext"].'</span>
        </button>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js" ></script>
        <script>
        var connectrRedirectURL;
        var ConnectrWrapper = document.getElementById("ConnectrLink");
        setTimeout(function waitConnectrWrapper() {
            if (document.body.contains(ConnectrWrapper)) {
                ConnectrInit();
            } else {
                setTimeout(waitConnectrWrapper, 100);
            }
        }, 100);

        function ConnectrInit(){

            {% if connectr-enable ==  "1"%}
            ConnectrShowButton();
            {% endif %}
        }

        var product = {{ product | json }};
  
  var variants = product.variants;


function rediectConnectrUrl(value,optionMap) {

    var variation = {{product.metafields.connectr["variantasins"]}};
    let variationid = value.id;
    asin = variation[variationid];
  if(asin != undefined){
    connectrProductAsin = asin;
  }

    connectrAmazonUrl ="https://www.amazon."+connectrRegion+"/dp/"+connectrProductAsin; 
    connectrRedirectURL = connectrAmazonUrl+"?m="+connectrSellerid+"&tag="+connectrAffiliateTag;
    window.open(connectrRedirectURL);



}


var optionMap = [];

$(".single-option-selector").each(function(key) {
  optionMap[key] = $(this).val();

});

   var found = 0;
  var fvalue = [];
  var foptionMap = [];
  var nvalue = [];
  var noptionMap = [];
  var available = false;



        function ConnectrShowButton()
        {
          var Connectraddtocartform = document.querySelector("form[action='.$cartadd.']");
          var Connectraddtocartbutton = Connectraddtocartform.querySelector("[type='.$submit.']");
          var Connectrlinkbutton = document.getElementById("ConnectrLink");

        //code for special themes
          if((Connectraddtocartbutton === undefined) || (Connectraddtocartbutton === null) || (Connectraddtocartbutton === ""))
          {
            Connectraddtocartbutton = document.getElementById("add-to-cart");
            if((Connectraddtocartbutton === undefined) || (Connectraddtocartbutton === null) || (Connectraddtocartbutton === ""))
            {
                Connectraddtocartbutton = document.getElementById("product-add-to-cart");
            }
            else
            {
                Connectraddtocartbutton.classList.remove("add-to-cart");
            }
          }

          Connectrlinkbutton.className += " " + Connectraddtocartbutton.classList;

          Connectraddtocartbutton.style.display = "none";
          Connectrlinkbutton.style.display = "block";

            //remove dynamic payment button
            //$( ".shopify-payment-button" ).remove();
            var shopifypaymentbutton = document.getElementsByClassName("shopify-payment-button");
            while(shopifypaymentbutton.length > 0){
                shopifypaymentbutton[0].parentNode.removeChild(shopifypaymentbutton[0]);
            }

            setInterval(function () {
                Connectraddtocartbutton.style.display = "none";
                Connectrlinkbutton.style.display = "block";

                //remove dynamic payment button

                var shopifypaymentbutton = document.getElementsByClassName("shopify-payment-button");
                while(shopifypaymentbutton.length > 0){
                    shopifypaymentbutton[0].parentNode.removeChild(shopifypaymentbutton[0]);
                }

            }, 4000);

            //SetAffiliateTags();
        }

        var connectrRegion       = "{{shop.metafields.connectr["region"]}}";
        var connectrProductAsin ="{{product.metafields.connectr["parentasin"]}}";
        var connectrSellerid     = "{{shop.metafields.connectr["sellerid"]}}";
        var connectrAffiliateTag = "'.$tags['amazontag'].'";

       

        function ConnectrButtonClick()
        {   
            var optionMap = [];

            $(".single-option-selector").each(function(key) {
            optionMap[key] = $(this).val();

            });

            

            variants.forEach(function(value, index) {
    

                if(JSON.stringify(value.options) == JSON.stringify(optionMap)) {
                      found = 1;
                       fvalue = value;
                      foptionMap = optionMap;
                      variantid = value.id;
                      console.log("fvalue",fvalue);
                       
              
                } else {
                  
                  
                  nvalue = value;
                  noptionMap = value.options;
                  
                  
                }
              
                if (optionMap.length === 0) {
                 found = 1;
                  fvalue = value;
                  variantid = value.id;
                  foptionMap = value.options;
              
              }
              });

                if (found) {
        
                    rediectConnectrUrl(fvalue,foptionMap);
                
                } else {
                    
                    rediectConnectrUrl(nvalue,noptionMap);
                    
                }

        }
        </script>

        {% endif %}';
        return $container;
    }


    public function getSpreadrCustomContainer($buserid,$tags) {

        $container = '
{% assign spreadr_comtag = "'.$tags["comtag"].'" %}
{% assign spreadr_intag = "'.$tags["intag"].'" %}
{% assign spreadr_uktag = "'.$tags["uktag"].'" %}
{% assign spreadr_detag = "'.$tags["detag"].'" %}
{% assign spreadr_frtag = "'.$tags["frtag"].'" %}
{% assign spreadr_estag = "'.$tags["estag"].'" %}
{% assign spreadr_catag = "'.$tags["catag"].'" %}
{% assign spreadr_cntag = "'.$tags["cntag"].'" %}
{% assign spreadr_ittag = "'.$tags["ittag"].'" %}
{% assign spreadr_jptag = "'.$tags["jptag"].'" %}
{% assign spreadr_mxtag = "'.$tags["mxtag"].'" %}
{% assign spreadr_autag = "'.$tags["autag"].'" %}
{% assign spreadr_brtag = "'.$tags["brtag"].'" %}
{% assign spreadr_button_text = "'.$tags["buttontext"].'" %}
{% assign enable_spreadr = false %}
{% assign enable_spreadr_cart = true %}
{% assign spreadr_userid = '.$buserid.' %}
{% assign isLocalized = shop.metafields.global["spreadr-localize"] | escape %}

  {% if product.metafields.global["spreadr-tag"] == "spreadr-affiliate" %}
    {% assign enable_spreadr = true %}
    {% for tag in product.tags %}
          {% if tag == "spreadr-hidden" %}
              {% assign enable_spreadr = false %}
          {% endif %}
          {% if tag == "spreadr-cart-hidden" %}
              {% assign enable_spreadr_cart = false %}
          {% endif %}
    {% endfor %}
  {% endif %}

{% if enable_spreadr == true %}

  {% case product.metafields.global["spreadr-region"] %}
    {% when "ca" %}
      {% assign spreadr_region_tag = spreadr_catag %}
    {% when "in" %}
      {% assign spreadr_region_tag = spreadr_intag %}
    {% when "de" %}
      {% assign spreadr_region_tag = spreadr_detag %}
    {% when "fr" %}
      {% assign spreadr_region_tag = spreadr_frtag %}
    {% when "es" %}
      {% assign spreadr_region_tag = spreadr_estag %}
   {% when "co.uk" %}
        {% assign spreadr_region_tag = spreadr_uktag %}
    {% when "cn" %}
      {% assign spreadr_region_tag = spreadr_cntag %}
    {% when "it" %}
      {% assign spreadr_region_tag = spreadr_ittag %}
    {% when "co.jp" %}
      {% assign spreadr_region_tag = spreadr_jptag %}
    {% when "com.mx" %}
      {% assign spreadr_region_tag = spreadr_mxtag %}
    {% when "com.au" %}
      {% assign spreadr_region_tag = spreadr_autag %}
    {% when "com.br" %}
      {% assign spreadr_region_tag = spreadr_brtag %}
    {% else %}
      {% assign spreadr_region_tag = spreadr_comtag %}
  {% endcase %}

    {% assign spreadr_redirect_url = product.metafields.global["spreadr-url"] | append: "?tag=" | append: spreadr_region_tag %}

    {% assign spreadr_link_id = "SpreadrLink-" | append: product.id %}

    {% assign spreadr_title = product.metafields.global["spreadr-title"] | escape %}
    {% if spreadr_title == blank %}
        {% assign spreadr_title = product.title  | escape %}
    {% endif %}

        <input type="hidden" id="SpreadrHidden-{{product.id}}" name="spreadr_hidden" class="spreadr_hidden"
       data-redirect-url="{{product.metafields.global["spreadr-url"] | escape }}"
       data-spreadr-tag="{{product.metafields.global["spreadr-tag"] | escape }}"
       data-region="{{product.metafields.global["spreadr-region"] | escape }}"
       data-product-tag="{{product.tags | escape }}"
       data-product="{{product.id  | escape}}"
       data-brand = "{{product.metafields.global["spreadr-brand"] | escape }}"
       data-title = "{{spreadr_title}}"
    >

<script>

spreadrdefer{{product.id}}();

    //wait for jquery to load
    function spreadrdefer{{product.id}}(method) {
    if (window.jQuery) {
        spreadrjQready{{product.id}}();
    } else {
        setTimeout(function() { spreadrdefer{{product.id}}(method) }, 50);
    }
}

  function spreadrjQready{{product.id}}()
  {
        //if analytics is ON
    if('.$tags["isganalyticson"].')
        {
          //attach click event listener
          $(document).on("click", "#SpreadrLink-{{product.id}}", function(){
                if(typeof ga !== "undefined")
                 {
                   ga("send", "event", { eventCategory: "Spreadr Link", eventAction: "Click", eventLabel: document.getElementById("SpreadrLink-{{product.id}}").href});
                 }
          });
        }


        if('.$tags["isfacebookpixel"].')
       {
         $(document).on("click", "#SpreadrLink-{{product.id}}", function(){
                if(typeof fbq !== "undefined") {

                    var spreadrRedirectURL = document.getElementById("SpreadrLink-{{product.id}}").href;
                    fbq("trackCustom", "SpreadrClick", {Amazonlink: spreadrRedirectURL});
                  }

          });



       }
  }

 var notfirstrun;

 //if geo localize is enabled
if('.$tags["islocalizeon"].')
{
    if(notfirstrun != "false")
    {
        if (window.addEventListener) {
            window.addEventListener("load", findlocation_custom, false)
        } else {
            window.attachEvent("onload", findlocation_custom)
        }
        notfirstrun = "false";
    }
}

 function findlocation_custom()
 {
    jQuery.ajax({
        url: "//extreme-ip-lookup.com/json/",
        type: "POST",
        dataType: "jsonp",
        success: function(location) {
            if(location.countryCode == undefined){
                 jQuery.ajax({
                    url: "//api.wipmania.com/json/",
                    type: "POST",
                    dataType: "jsonp",
                    success: function(location) {
                      localize_custom(location.address.country_code.toLowerCase());
                    }
                });
            } else {
                localize_custom(location.countryCode.toLowerCase());
            }

        }
    });
 }

function localize_custom(country_code)
{
      var arrLinks = document.getElementsByName("SpreadrLink");
      for (var i = 0, j = arrLinks.length; i < j; i++) {

        var spid = arrLinks[i].id;
        spid = spid.substr(12,spid.length);

        if(spid != "")
        {
            var hiddenf = document.getElementById("SpreadrHidden-"+spid);

            switch(country_code) {
              case "us":
                  if(hiddenf.getAttribute("data-region") != "com")
                  {
                    arrLinks[i].href = "http://amazon.com/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_comtag}}";
                  }
                  break;
              case "ca":
                  if(hiddenf.getAttribute("data-region") != "ca")
                  {
                    arrLinks[i].href = "http://amazon.ca/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_catag}}";
                  }
                  break;
              case "gb":
                  if(hiddenf.getAttribute("data-region") != "co.uk")
                  {
                    arrLinks[i].href = "http://amazon.co.uk/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_uktag}}";
                  }
                  break;
              case "in":
                  if(hiddenf.getAttribute("data-region") != "in")
                  {
                    arrLinks[i].href = "http://amazon.in/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_intag}}";
                  }
                  break;
              case "de":
                  if(hiddenf.getAttribute("data-region") != "de")
                  {
                    arrLinks[i].href = "http://amazon.de/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_detag}}";
                  }
                  break;
              case "fr":
                  if(hiddenf.getAttribute("data-region") != "fr")
                  {
                    arrLinks[i].href = "http://amazon.fr/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_frtag}}";
                  }
                  break;
               case "es":
                  if(hiddenf.getAttribute("data-region") != "es")
                  {
                    arrLinks[i].href = "http://amazon.es/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_estag}}";
                  }
                  break;
              case "it":
                  if(hiddenf.getAttribute("data-region") != "it")
                  {
                    arrLinks[i].href = "http://amazon.it/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_ittag}}";
                  }
                  break;
               case "mx":
                  if(hiddenf.getAttribute("data-region") != "com.mx")
                  {
                    arrLinks[i].href = "http://amazon.com.mx/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_mxtag}}";
                  }
                  break;
                case "au":
                  if(hiddenf.getAttribute("data-region") != "com.au")
                  {
                    arrLinks[i].href = "http://amazon.com.au/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_autag}}";
                  }
                  break;
                case "br":
                  if(hiddenf.getAttribute("data-region") != "com.br")
                  {
                    arrLinks[i].href = "http://amazon.com.br/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_brtag}}";
                  }
                  break;
                case "jp":
                  if(hiddenf.getAttribute("data-region") != "co.jp")
                  {
                    arrLinks[i].href = "http://amazon.co.jp/s/?field-keywords=" + encodeURIComponent(hiddenf.getAttribute("data-title")) + "&tag={{spreadr_brtag}}";
                  }
                  break;
              default:
                  break;
              }
          }
      }

      {% if spreadr_userid < 2285 and isLocalized == blank %}
      //remove after initial few days - only for spreadr custom
        jQuery.ajax({
            url: "'.Config::get("app.cron_url") .'updateforlocalize/{{spreadr_userid}}",
            type: "GET",
            dataType: "string",
            success: function(result) {
              console.log("result - "+result);
            }
        });
      {% endif %}
  }
</script>
{% endif %}';

        return $container;
    }


    public function test( )
    {
        try{
            

                $sc = $this->setShopifyCredential( Auth::User()->shop, Auth::User()->accesstoken );
            

            return $this->shopifyClient->call('GET', '/admin/api/'.Config::get('api.version').'/products/1834845470769/metafields.json' );

        }catch (Exception $e) {
            log::info('----------------errror-----------------------'.$productid);
            return false;
        }

    }

    public function updateConnectrReviewLiquid($userid = null)
    {
        $data = array();

        if($userid == null){
            $userid = Auth::Id();
        }
        $objUser = User::find($userid);

        $sc = $this->setShopifyCredential( $objUser->shop, $objUser->accesstoken );

        $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
        $themeID = $theme[0]['id'];
        $mobileThemeID = 0;

        //find the current theme
        foreach ($theme as $uniquetheme) {

            if($uniquetheme['role']=='main')
            {
                $themeID = $uniquetheme['id'];
            }
            elseif ($uniquetheme['role']=='mobile')
            {
                $mobileThemeID = $uniquetheme['id'];
            }
        }

        $usersetting = Usersetting::where('userid',$userid)->first();
        $mwscredentials = Mwscredential::where('userid',$userid)->first();
       
        $reviewsettings = Reviewsetting::updateOrCreate( array( 'userid'=>$userid ) , array() );
        App::setLocale('en');

        if(!empty($mwscredentials)) {
            
            $regionmapper['com'] = 'en';
            $regionmapper['in'] = 'en';
            $regionmapper['co.uk'] = 'en';
            
            $regionmapper['es'] = 'es';
            $regionmapper['com.mx'] = 'es';
            
            $regionmapper['com.br'] = 'pr';
            $regionmapper['de'] = 'de';
            $regionmapper['fr'] = 'fr';
            $regionmapper['it'] = 'it';
            
            App::setLocale($regionmapper[$mwscredentials->region]);
            
        }
        


        $data['reviewsettings'] = $reviewsettings->toArray();
        
        $pageContent =  view('connectrreview',$data)->render();

        $params1 = array(
            "asset" => array
                (
                "key" => "snippets/connectr-reviews.liquid",
                "value" => $pageContent
            )
        );
        $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $params1);

        if($mobileThemeID > 0)
        {
            $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $mobileThemeID . '/assets.json', $pageParams1);
            
        }

    }


    //New one
    public function createupdateCharge($planId) {

        //print_r($planId);exit;
        try {
            
            $User = Auth::User();
           // print_r($User->userid);exit;
            $sc = new ShopifyClient($User->shop, $User->accesstoken, env('shopify_api_key'), env('shopify_secret') );

            if (strpos(env('APP_URL'), 'local') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
                $test = true;
            }else{
                $test = false;
            }
            if( !empty( $User->shopifyplanname ) && $User->shopifyplanname == 'staff_business') {
                $test = true;
            }


            if($planId == '2')
            {
                $amount = '5';
                $planstr = 'Basic';
            }
            elseif ($planId == '3')
            {
                $amount = '10';
                $planstr = 'Pro';
            }elseif ($planId == '4')
            {
                $amount = '20';
                $planstr = 'Gold';
            }
           
            // $time = "2016-09-1 04:45:54";
            $time = strtotime($User->createtime);
            $currentTime = new DateTime(date('Y-m-d H:i:s'));
            $expirationTime = new DateTime(date("Y-m-d H:i:s",$time));
            $countdown = $expirationTime->diff($currentTime);
            $countdown1 = $countdown->format("%R%a");
            if($countdown1<0){
                $countdown1 = 0;
            }
            $trialdays = 7 - $countdown1;
            $terms = "$".$amount." per month after ".$trialdays." days";
            if($trialdays < 0)
            {
                $trialdays = 0;
                $terms = "$".$amount." per month (trial expired)";
            }           
            $params = array(
                "recurring_application_charge" => array(
                    "name" => $planstr." Account",
                    "price" => $amount.".00",
                    "return_url" => env('APP_URL') . "upgradeplansuccess?userid=".$User->userid."&newplan=".$planId."&planamount=".$amount,              
                    "terms" => $terms,
                    "trial_days" => $trialdays, 
                    "test" => $test
                )
            );
             
            $charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/recurring_application_charges.json', $params);
            
            $data['data'] = $charges;

            $success = $this->setupShopFiles($User->id, $User->shop, $User->accesstoken);
           
            return $charges['confirmation_url'];
            die($charges['confirmation_url']);
            

        } catch (ShopifyApiException $e) {
            log::info('upgrade charge unsuccessful');
            $subject = 'updateCharge function from shopifycontroller';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );
            log::info($error);
            return false;
        }
          
            header("Location: " . $charges['confirmation_url']);
            exit();
    }


    public function getUpgradeplansuccess() {

       // log::info('getUpgradeplansuccess');

    //    echo "test";exit;

            $User = User::where('userid',$_GET['userid'])->first();

         // $User = Auth::User();
         // ptint_r($User);exit;
          $success = $this->activateCharge($_GET['charge_id'], $User->shop, $_GET['userid'], $User->accesstoken);

          if($success)
          {          
              //log::info('charge accespted');
            $plan = Plan::find($_GET['newplan']);
                $businessUserData = array(
                      'planid' => $_GET['newplan'],
                      'ispaymentok' => 1,
                      'isinstalled' => 1,
                      'istrial' => 0,
                      'planamount'=>$_GET['planamount'],
                      'chargeid' => $_GET['charge_id']
                  );
                
              User::updateOrCreate(array('userid' => $_GET['userid']), $businessUserData);
              $success = $this->setupShopFiles($User->userid, $User->shop, $User->accesstoken);

              return Redirect::to('plans')->with('success', ['Plan changed']);
          }
           else
            {
                //log::info('charge rejected');
                 // update current plan
                 $data = array(
                     'ispaymentok' => 0 // 1 = true, 0 =  false
                 );

                User::updateOrCreate(array('userid' => $_GET['userid']), $data);
                //Billing::putCurrentPlan(array('buser_id' => $_GET['user']), $data);

                return Redirect::to('/');

            }

     }


    public function purchaseCredits() {
        try {
//           
            $amount = $_POST['amountcharge'];
             $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken, env('shopify_api_key'), env('shopify_secret') );

            if (strpos(env('APP_URL'), 'localhost') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
                $test = true;
            }else{
                $test = false;
            }

            $arrUser = User::Where( array('userid'=> Auth::Id() ) )->get();

            if( !empty( $arrUser[0]->shopifyplanname ) && $arrUser[0]->shopifyplanname == 'staff_business') {
                $test = true;
            }

            $params = array(
                "application_charge" => array(
                    "name" => "Buying credits for pulling products",
                    "price" => $amount,
//                    "return_url" => Config::get('app.base_url') . "shopifyappinstall?&action=rcharge&chargeshop=".Session::get('shop').'&user='.$userid.'&tid='.Session::get('token'),
                    "return_url" => env('APP_URL') . "returnapplicationcharge",
                    "test" => $test
                )
            );
            $charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/application_charges.json', $params);


        } catch (ShopifyApiException $e) {
//            log::info('bulk charge unsuccessful');
            $subject = 'createApplicationCharge function from shopifycontroller';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );
            // ErrorLog::postErrorLog($error);
            return false;
        }

        header("Location: " . $charges['confirmation_url']);
        exit();
    }



    public function purchaseReviewCredits() {
        try {
//           
            $reviewproducts = $_POST['reviewproducts'];
            $amount = ceil($reviewproducts/10);
             $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken, env('shopify_api_key'), env('shopify_secret') );

            if (strpos(env('APP_URL'), 'localhost') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
                $test = true;
            }else{
                $test = false;
            }

            $arrUser = User::Where( array('userid'=> Auth::Id() ) )->get();

            if( !empty( $arrUser[0]->shopifyplanname ) && $arrUser[0]->shopifyplanname == 'staff_business') {
                $test = true;
            }

            $params = array(
                "application_charge" => array(
                    "name" => "Buying credits for importing reviews",
                    "price" => $amount,
//                    "return_url" => Config::get('app.base_url') . "shopifyappinstall?&action=rcharge&chargeshop=".Session::get('shop').'&user='.$userid.'&tid='.Session::get('token'),
                    "return_url" => env('APP_URL') . "returnreviewcharge?reviewproducts=".$reviewproducts,
                    "test" => $test
                )
            );
            $charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/application_charges.json', $params);


        } catch (ShopifyApiException $e) {
//            log::info('bulk charge unsuccessful');
            $subject = 'createApplicationCharge function from shopifycontroller';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );
            // ErrorLog::postErrorLog($error);
            return false;
        }

        header("Location: " . $charges['confirmation_url']);
        exit();
    }

    



    public function returnApplicationCharge()
    {
        if(isset($_GET['charge_id']))
        {



            $success =  $this->activateApplicationCharge($_GET['charge_id'], Auth::user()->shop, Auth::user()->userid, Auth::user()->accesstoken);

            if($success)
            {
                $message = 'chargestatus';
                $text = 'Product credits added successfully.';
                $type = 'success';

                Session::flash($message, array('text' => $text, 'type' => $type));

                return Redirect::to('plans');
            }
     }

    }


    public function returnReviewCharge()
    {
        if(isset($_GET['charge_id']))
        {



            $success =  $this->activateReviewCharge($_GET['charge_id'], Auth::user()->shop, Auth::user()->userid, Auth::user()->accesstoken);

            if($success)
            {
                $message = 'chargestatus';
                $text = 'Product credits added successfully.';
                $type = 'success';

                Session::flash($message, array('text' => $text, 'type' => $type));

                return Redirect::to('settings-reviews');
            }
     }

    }

    public function activateReviewCharge($charge_id, $shopname, $user_id, $token) {
        try {
          
            $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken, env('shopify_api_key'), env('shopify_secret') );

            if (strpos(env('APP_URL'), 'localhost') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
                $test = true;
            }else{
                $test = false;
            }

            $arrUser = User::Where( array('userid'=> $user_id ) )->get();

            if( !empty( $arrUser[0]->shopifyplanname ) && $arrUser[0]->shopifyplanname == 'staff_business') {
                $test = true;
            }


            $sc = new ShopifyClient($shopname, $token, env('shopify_api_key'), env('shopify_secret'));

            $params1 = array(
                "application_charge" => array(
                    "test" => $test
                )
            );
            $charges1 = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/application_charges/' . $charge_id. '.json', $params1);

            if($charges1['status'] == 'active')
              {
                  return false;
              }

            $params = array(
                "application_charge" => array(
                    "return_url" => env('APP_URL') . "settings-reviews",
                    "status" => "accepted",
                    "test" => $test
                )
            );
            $charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/application_charges/' . $charge_id . '/activate.json', $params);

          

             if($charges['status'] == 'active')
             {
                $reviewsettings = Reviewsetting::updateOrCreate( array( 'userid'=>Auth::Id() ) , array() );
                $credit = $reviewsettings->reviewcredit+($_GET['reviewproducts']);
                $reviewsettings->reviewcredit = $credit;
                $reviewsettings->save();
                return true;
             }
             else
             {
                 return false;
             }

        } catch (ShopifyApiException $e) {
            $subject = 'charge not activated';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );
            // ErrorLog::postErrorLog($error);
            return false;
        }
    }


    public function activateApplicationCharge($charge_id, $shopname, $user_id, $token) {
        try {
           
            $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken, env('shopify_api_key'), env('shopify_secret') );

            if (strpos(env('APP_URL'), 'localhost') == TRUE || strpos(env('APP_URL'), 'staging') == TRUE) {
                $test = true;
            }else{
                $test = false;
            }

            $arrUser = User::Where( array('userid'=> $user_id ) )->get();

            if( !empty( $arrUser[0]->shopifyplanname ) && $arrUser[0]->shopifyplanname == 'staff_business') {
                $test = true;
            }


            //$User = App::make('UserController')->getToken($user_id);

            $sc = new ShopifyClient($shopname, $token, env('shopify_api_key'), env('shopify_secret'));

             //Get charge to test if it is alreadr activated
            $params1 = array(
                "application_charge" => array(
                    "test" => $test
                )
            );
            $charges1 = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/application_charges/' . $charge_id. '.json', $params1);
//             log::info($charges1);
//             foreach ($charges1 as $key => $value){
//                    log::info($key.":". $value);
//                };

            //if charge alreadr activated
            if($charges1['status'] == 'active')
              {
                  return false;
              }

            //Activate charge
            $params = array(
                "application_charge" => array(
                    "return_url" => env('APP_URL') . "plans",
                    "status" => "accepted",
                    "test" => $test
                )
            );
            $charges = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/application_charges/' . $charge_id . '/activate.json', $params);

//            log::info('charge activated success - '.$charges['status']);
//              $json = json_decode($charges, true);

//                foreach ($charges as $key => $value){
//                    log::info($key.":". $value);
//                };

             if($charges['status'] == 'active')
             {
                $user = User::where('userid',$user_id)->first();
                $credit = $user->productcredit+($charges['price']*100);
                $user->productcredit = $credit;
                $user->save();
                return true;
             }
             else
             {
                 return false;
             }

        } catch (ShopifyApiException $e) {
            $subject = 'charge not activated';
            $error = array(
                'name' => $subject,
                'error' => $e->getMessage(),
                'time' => time()
            );
            // ErrorLog::postErrorLog($error);
            return false;
        }
    }


    public function getShopifyConnectrLiquidContainer($buserid,$tags) {
    // public function getShopifyStarLiquidContainer($buserid) {    
        
        $cartadd = "'/cart/add'";
        $submit = "'submit'";
        //log::info($tags);
        // exit;

       
        $container = '{% if template contains "product" %}

        {% assign connectr-enable = "'.$tags["isredirect"].'" %}
         
         <button id="ConnectrLink" type="button" style="display:none;" name="ConnectrLink" class="btn"
                            onclick="ConnectrButtonClick()"
                            >
                      <span id="ConnectrLinkText">'.$tags["buttontext"].'</span>
        </button>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js" ></script>
        <script>
        var connectrRedirectURL;
        var ConnectrWrapper = document.getElementById("ConnectrLink");
        setTimeout(function waitConnectrWrapper() {
            if (document.body.contains(ConnectrWrapper)) {
                ConnectrInit();
            } else {
                setTimeout(waitConnectrWrapper, 100);
            }
        }, 100);

        function ConnectrInit(){
           
            {% if connectr-enable ==  "1"%}
            ConnectrShowButton();
            {% endif %}
        }

        var product = {{ product | json }};
  
        var variants = product.variants;


        function rediectConnectrUrl() {

        
            connectrAmazonUrl ="https://www.amazon."+connectrRegion+"/dp/"+connectrProductAsin; 
            //connectrRedirectURL = connectrAmazonUrl+"?m="+connectrSellerid+"&tag="+connectrAffiliateTag;
            connectrRedirectURL = connectrAmazonUrl+"?m="+connectrSellerid;
            window.open(connectrRedirectURL);

        }




        function ConnectrShowButton()
        {
          var Connectraddtocartform = document.querySelector("form[action='.$cartadd.']");
          var Connectraddtocartbutton = Connectraddtocartform.querySelector("[type='.$submit.']");
          var Connectrlinkbutton = document.getElementById("ConnectrLink");

        //code for special themes
          if((Connectraddtocartbutton === undefined) || (Connectraddtocartbutton === null) || (Connectraddtocartbutton === ""))
          {
            Connectraddtocartbutton = document.getElementById("add-to-cart");
            if((Connectraddtocartbutton === undefined) || (Connectraddtocartbutton === null) || (Connectraddtocartbutton === ""))
            {
                Connectraddtocartbutton = document.getElementById("product-add-to-cart");
            }
            else
            {
                Connectraddtocartbutton.classList.remove("add-to-cart");
            }
          }

          Connectrlinkbutton.className += " " + Connectraddtocartbutton.classList;

          //Connectraddtocartbutton.style.display = "none";
          Connectrlinkbutton.style.display = "block";

            //remove dynamic payment button
            //$( ".shopify-payment-button" ).remove();
            var shopifypaymentbutton = document.getElementsByClassName("shopify-payment-button");
           

            setInterval(function () {
                //Connectraddtocartbutton.style.display = "none";
                Connectrlinkbutton.style.display = "block";

                //remove dynamic payment button

                var shopifypaymentbutton = document.getElementsByClassName("shopify-payment-button");
              

            }, 4000);

            //SetAffiliateTags();
        }

        var connectrRegion       = "{{shop.metafields.connectr["region"]}}";
        var connectrProductAsin ="{{product.metafields.connectr["parentasin"]}}";
        var connectrSellerid     = "{{shop.metafields.connectr["sellerid"]}}";
        

       

        function ConnectrButtonClick()
        {   
            rediectConnectrUrl();

        }
        </script>

        {% endif %}';

      
        return $container;
    }

    public function getOrdersCount($user = array())
    {
        try {

            if (empty(Auth::User())) {

                $sc = $this->setShopifyCredential($user->shop, $user->accesstoken);
            }

            $a = $this->shopifyClient->call('GET', '/admin/api/'.Config::get('api.version').'/orders/count.json?status=any');

            return $a;
        } catch (Exception $e) {

            return false;
        }
    }

    public function shopifygetOrderAccess($shop)
    {
        $shopifyClient = $this->setShopifyCredential($shop, "");
        $pageURL =  env('SHOPIFY_URL');
        // ob_start();
        // ini_set('display_errors', TRUE);
        // print_r($shopifyClient->getAuthorizeUrl(env('shopify_scope'), $pageURL));exit;
        header("Location: " . $shopifyClient->getAuthorizeUrl(env('shopify_scope'), $pageURL));exit;
        
    }

    public function shopifyADDOrders($shop, $accesstoken, $variant, $quantity)
    {
        
        $myarray[] =  array( 'variant_id'=> $variant, 'quantity'=> $quantity); 
        $sc = $this->setShopifyCredential( $shop, $accesstoken );
        $params = array(
            "order" => array(
                "line_items" => $myarray
                
            )
        );
        $add = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/orders.json', $params);
        
        return 'ok';
        
    }

    public function shopifygetallOrders($token, $tkdata, $shop, $accesstoken, $orderid)
    {

        try{
            if ($token == 0) {
                if ($orderid == '') {
                    // $sc = $this->setShopifyCredential( $shop, $accesstoken );
                    $url = 'https://'.$shop.'/admin/api/'.Config::get('api.version').'/orders.json?status=any';
                }else{
                    $url = 'https://'.$shop.'/admin/api/'.Config::get('api.version').'/orders.json?since_id='.$orderid.'&status=any';
                }

                $data['limit'] = 9000;
                $a = $this->orderguzzleShopify($url, 'GET', $data, $accesstoken);
                // dd($a);exit;
                $call_limit = $a['header']['X-Shopify-Shop-Api-Call-Limit'][0];
                if ($call_limit == '39/40') {
                    sleep(3);
                }
                return $a;
            } elseif ($token == 1) {

                if (strpos($tkdata, 'rel="next"') == true && strpos($tkdata, 'rel="previous"') == true) {
                    $orders = explode(",", $tkdata);
                    $orders = explode("<", $orders[1]);
                    $orders = explode(">", $orders[1]);


                    $url = $orders[0];

                    $data['limit'] = 3;
                    $a = $this->orderguzzleShopify($url, 'GET', $data, $accesstoken);
                    return $a;
                } elseif (strpos($tkdata, 'rel="next"') == true) {
                    
                    $orders = explode(">", $tkdata);
                    
                    $orders = explode("<", $orders[0]);

                    $url = $orders[1];

                    $data['limit'] = 3;
                    $a = $this->orderguzzleShopify($url, 'GET', $data, $accesstoken);

                    return $a;
                } else {
                    return "done";
                }
            } else {
                echo "nothing";
            }
        } catch (Exception $e) {
            log::info('order cron check');
        }
    }

    public function shopifygetallOrdersmore($token, $tkdata, $shop, $accesstoken, $orderid)
    {
        try{
            if ($token == 0) {
                if ($orderid == '') {
                    // $sc = $this->setShopifyCredential( $shop, $accesstoken );
                    $url = 'https://'.$shop.'/admin/api/'.Config::get('api.version').'/orders.json?status=any';
                }else{
                    $url = 'https://'.$shop.'/admin/api/'.Config::get('api.version').'/orders.json?since_id='.$orderid.'&status=any';
                }

                $data['limit'] = 9000;
                $a = $this->orderguzzleShopify($url, 'GET', $data, $accesstoken);
                // dd($a);exit;
                $call_limit = $a['header']['X-Shopify-Shop-Api-Call-Limit'][0];
                if ($call_limit == '39/40') {
                    sleep(3);
                }
                return $a;
            } elseif ($token == 1) {

                if (strpos($tkdata, 'rel="next"') == true && strpos($tkdata, 'rel="previous"') == true) {
                    $orders = explode(", ", $tkdata);
                    $orders2 = explode("<", $orders[1]);
                    $orders3 = explode(">", $orders2[0]);
                    // $url = $orders3[0];
                    $url = 'https://'.$shop.'/admin/api/'.Config::get('api.version').'/orders.json?page_info='.$orders3[0];

                    $data['limit'] = 3;
                    $a = $this->orderguzzleShopify($url, 'GET', $data, $accesstoken);
                    return $a;
                } elseif (strpos($tkdata, 'rel="next"') == true) {
                    // log::info('nexturl');
                    $orders = explode(">", $tkdata);

                    $orders = explode("<", $orders[0]);
                    
                    // $orders = explode(";", $tkdata);

                    $url = $orders[0];
                    // log::info($url);
                    $data['limit'] = 3;
                    $a = $this->orderguzzleShopify($url, 'GET', $data, $accesstoken);
                    // print_r($a);exit;
                    return $a;
                } else {
                    return "done";
                }
            } else {
                echo "nothing";
            }  
        } catch (Exception $e) {
            log::info('order cron check2');
        } 
    }


    public function getShopifyConnectrLiquidCustomContainer($user_id,$tags)
    {
        $container = '
        {% assign connectr_button_text = "'.$tags["buttontext"].'" %}
        {% assign enable_connectr = false %}
        {% assign connectr_redirect_url = "https://www.amazon." | append: shop.metafields.connectr["region"] | append: "/dp/" | append: product.metafields.connectr["parentasin"] | append: "?m=" | append: shop.metafields.connectr["sellerid"]%} 
        {% assign connectr-redirect = "'.$tags["isredirect"].'" %}


        {% assign connectr_link_id = "ConnectrLink-" | append: product.id %}
        {% if connectr-redirect ==  "1"%}
        {% if product.metafields.connectr["isconnectr"]  == "true" %}
            {% assign enable_connectr = true %}
        {% endif %}
        {% endif %}

        ';

        return $container;
    }

    public function getFiledataforAdminlogs($shopname, $token, $assetkey, $themeid , $themename) {
        try {
            log::info('start activateWebhook');
            try{

       $sc = new ShopifyClient($shopname, $token,  env('shopify_api_key'), env('shopify_secret') );
       
       if(empty($themeid) || $themeid == ''){
           $theme = $sc->call('GET', '/admin/api/2021-01/themes.json');

           $themeID = $theme[0]['id'];
           $themeName = $theme[0]['name'];

           //find the current theme and get theme id
           foreach ($theme as $uniquetheme) {
               if($uniquetheme['role']=='main')
               {
                   $themeID = $uniquetheme['id'];
                   $themeName = $uniquetheme['name'];
               }
           }
       }else{
           $themeID = $themeid;
           $themeName = $themename;
       }
       
//            log::info('theme id - '.$themeID);
        $pageParams = array(
               "asset" => array
                   (
                   "key" => $assetkey
//                        "value" => $pageContent
                   )
           );

           $assets = $sc->call('GET', '/admin/api/2021-01/themes/' . $themeID . '/assets.json', $pageParams);
//                 log::info('success call');
//                   log::info($assets);
           $text = htmlentities($assets['value']);
//                foreach($assets as $asset)
//                {
//                 log::info('before array loop print');
////                    foreach($assets as $key => $val)
////                    {
////                        log::info($key . ' - ' . $val);
////                        $text .= '<p>'.$key.' = '.$val.'</p>';
////                    }
//                     log::info('after array loop print');
                //$text .= '<br>';
//                }
           return $text;

                       } catch (ShopifyApiException $e) {
                               //log::info('getFileData error');
                               $subject = 'getFileData error';
                               $error = array(
                                   'name' => $subject,
                                   'error' => $e->getMessage(),
                                   'time' => time()
                               );
                               // ErrorLog::postErrorLog($error);
                               return false;
                     }



        } catch (Exception $exc) {
                //Log::error('getFileData error');
            $subject = 'getFileData error';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
            //  ErrorLog::postErrorLog($error);
            return false;
        }
    }

    public function getThemeFileData($shopname, $token, $assetkey, $themeid) {
        try {
            try{
                $sc = new ShopifyClient($shopname, $token,  env('shopify_api_key'), env('shopify_secret') );
                
                if(empty($themeid) || $themeid == ''){
                    $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
                    $themeID = $theme[0]['id'];
                    //find the current theme and get theme id
                    foreach ($theme as $uniquetheme) {
                        if($uniquetheme['role']=='main')
                        {
                            $themeID = $uniquetheme['id'];
                        }
                    }
                }else{
                    $themeID = $themeid;
                }
                
                $pageParams = array(
                    "asset" => array("key" => $assetkey)
                );
                $assets = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $pageParams);
                return $assets;
            } catch (ShopifyApiException $e) {
                $subject = 'getFileData error';
                $error = array('name' => $subject,'error' => $e->getMessage(),'time' => time() );
                return false;
            }
        } catch (Exception $exc) {
            $subject = 'getFileData error';
            $error = array('name' => $subject,'error' => $exc->getMessage(),'time' => time() );
            return false;
        }
    }

    public function putThemeFileData($shopname, $token, $assetkey, $newdata, $themeid) {
        try {
            try{
                $sc = new ShopifyClient($shopname, $token, env('shopify_api_key'), env('shopify_secret') );
                if(empty($themeid) || $themeid == ''){
                    $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
                    $themeID = $theme[0]['id'];
                    //find the current theme and get theme id
                    foreach ($theme as $uniquetheme) {
                        if($uniquetheme['role']=='main')
                        {
                            $themeID = $uniquetheme['id'];
                        }
                    }
                }else{
                    $themeID = $themeid;
                }
                $pageParams = array(
                    "asset" => array("key" => $assetkey,"value" => $newdata,"content_type" => "text\/x-liquid"));

                $assets = $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $pageParams);

                $text = "done adding new data";
                return $text;
            } catch (ShopifyApiException $e) {
                $subject = '1putFileData | code -'.$e->getCode();
                $error = array('name' => $subject,'error' => $e->getCode().' - '.$e->getMessage(),'time' => time());
                return false;
            }
        } catch (Exception $exc) {
            $subject = '2 putFileData';
            $error = array('name' => $subject,'error' => $exc->getMessage(),'time' => time() );
            return false;
        }
    }

    public function getFileData($shopname, $token, $assetkey, $themeid , $themename) {
        try {
                 log::info('start activateWebhook');
                 try{

            $sc = new ShopifyClient($shopname, $token,  env('shopify_api_key'), env('shopify_secret') );
            
            if(empty($themeid) || $themeid == ''){
                $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');

                $themeID = $theme[0]['id'];
                $themeName = $theme[0]['name'];
    
                //find the current theme and get theme id
                foreach ($theme as $uniquetheme) {
                    if($uniquetheme['role']=='main')
                    {
                        $themeID = $uniquetheme['id'];
                        $themeName = $uniquetheme['name'];
                    }
                }
            }else{
                $themeID = $themeid;
                $themeName = $themename;
            }
            
//            log::info('theme id - '.$themeID);
             $pageParams = array(
                    "asset" => array
                        (
                        "key" => $assetkey
//                        "value" => $pageContent
                        )
                );

                $assets = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $pageParams);
//                 log::info('success call');
//                   log::info($assets);
                $text = 'Theme Name - '.$themeName.' | Theme ID - '.$themeID.'<pre id="filedatapre">'.htmlentities($assets['value']).'</pre>';
//                foreach($assets as $asset)
//                {
//                 log::info('before array loop print');
////                    foreach($assets as $key => $val)
////                    {
////                        log::info($key . ' - ' . $val);
////                        $text .= '<p>'.$key.' = '.$val.'</p>';
////                    }
//                     log::info('after array loop print');
                     //$text .= '<br>';
//                }
                return $text;

                            } catch (ShopifyApiException $e) {
                                    //log::info('getFileData error');
                                    $subject = 'getFileData error';
                                    $error = array(
                                        'name' => $subject,
                                        'error' => $e->getMessage(),
                                        'time' => time()
                                    );
                                    // ErrorLog::postErrorLog($error);
                                    return false;
                          }



        } catch (Exception $exc) {
             //Log::error('getFileData error');
            $subject = 'getFileData error';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
            //  ErrorLog::postErrorLog($error);
            return false;
        }
    }

    public function putFileData($shopname, $token, $assetkey, $newdata, $themeid , $themename) {
        try {
                 //log::info('start activateWebhook');
                 try{

            $sc = new ShopifyClient($shopname, $token, env('shopify_api_key'), env('shopify_secret') );
            if(empty($themeid) || $themeid == ''){
                $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
                $themeID = $theme[0]['id'];

                //find the current theme and get theme id
                foreach ($theme as $uniquetheme) {
                    if($uniquetheme['role']=='main')
                    {
                        $themeID = $uniquetheme['id'];
                        $themeName = $uniquetheme['name'];
                    }
                }
            }else{
                $themeID = $themeid;
                $themeName = $themename;
            }
            //log::info($newdata);
             $pageParams = array(
                    "asset" => array
                        (
                        "key" => $assetkey,
                        "value" => $newdata,
                        "content_type" => "text\/x-liquid"
                        )
                );

                $assets = $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $pageParams);
//                 log::info('success call');
                   //log::info($assets);
//                $text = '<pre>'.htmlentities($assets['value']).'</pre>';
//                foreach($assets as $asset)
//                {
//                 log::info('before array loop print');
                $text = "done adding new data";
//                foreach($assets as $key => $val)
//                    {
//                        log::info($key . ' - ' . $val);
//                        $text .= '<p>'.$key.' = '.$val.'</p>';
//                    }
//                     log::info('after array loop print');
                     //$text .= '<br>';
//                }
                return $text;

                            } catch (ShopifyApiException $e) {
//                                    log::info('1error putFileData');
                                    $subject = '1putFileData | code -'.$e->getCode();
                                    $error = array(
                                        'name' => $subject,
                                        'error' => $e->getCode().' - '.$e->getMessage(),
                                        'time' => time()
                                    );
                                    // ErrorLog::postErrorLog($error);
                                    return false;
                          }



        } catch (Exception $exc) {
//             Log::info('2 error putFileData');
            $subject = '2 putFileData';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
            //  ErrorLog::postErrorLog($error);
            return false;
        }
    }

    public function getAssetData($shopname, $token , $themeid , $themename) {
        try {

            try{

                $sc = new ShopifyClient($shopname, $token,env('shopify_api_key'), env('shopify_secret') );
                if(empty($themeid) || $themeid == ''){
                    $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
                    $themeID = $theme[0]['id'];
    
                    //find the current theme and get theme id
                    foreach ($theme as $uniquetheme) {
                        if($uniquetheme['role']=='main')
                        {
                            $themeID = $uniquetheme['id'];
                            $themeName = $uniquetheme['name'];
                        }
                    }
                }else{
                   
                    $themeID = $themeid;
                    $themeName = $themename;
                }
                
                    $pageParams = array(
                        "asset" => array
                            (
                            //  "key" => "snippets/spreadr.liquid",
                            //  "value" => $pageContent
                            )
                    );
                

                $assets = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $pageParams);
                //  log::info('success call');
                //  log::info($assets);
                $text = '';
                $text .= '<h4>Theme Name - '.$themeName.' | Theme ID - '.$themeID.'</h4><br><br>';
                $text .= '<div class="col-md-6"><table id="example" class="row-border" style="width:100%">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Key</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>';
                $pointer = 1;
                
                foreach($assets as $asset)
                {

                    $text .= '<tr>
                                <th scope="row">'.$pointer.'</th>
                                <td class="assetvalue_'.$pointer.'" >'.$asset["key"].'</td>
                                <td><button onclick="AnalyzeKeySelect('.$pointer.')" class="pull-left btn btn-primary btn-xs">Select</button></td>
                            </tr>';
                            $pointer += 1;
                    // $text .= '<p>'.'key'.' = '.$asset["key"].'</p>';
                    // foreach($asset as $key => $val)
                    // {
                    //     $text .= '<p>'.$key.' = '.$val.'</p>';
                    // }
                    
                }
                $text .= '</tbody>
                    </table></div>';
                return $text;

                            } catch (ShopifyApiException $e) {
//                                    log::info('getAssetData error');
                                    $subject = 'getAssetData error';
                                    $error = array(
                                        'name' => $subject,
                                        'error' => $e->getMessage(),
                                        'time' => time()
                                    );
                                    // ErrorLog::postErrorLog($error);
                                    return false;
                          }



        } catch (Exception $exc) {
//             Log::error('getAssetData error');
            $subject = 'getAssetData error';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
            //  ErrorLog::postErrorLog($error);
            return false;
        }
    }

    public function getThemeAssetData($shop, $token , $themeid) {
        try {

            try{

                $sc = new ShopifyClient($shop, $token,env('shopify_api_key'), env('shopify_secret') );
                if(empty($themeid) || $themeid == ''){
                    // $sc = $this->setShopifyCredential( $shop, $token );
                    $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
                    $themeID = $theme[0]['id'];
    
                    //find the current theme and get theme id
                    foreach ($theme as $uniquetheme) {
                        if($uniquetheme['role']=='main')
                        {
                            $themeID = $uniquetheme['id'];
                            // $themeName = $uniquetheme['name'];
                        }
                    }
                }else{
                   
                    $themeID = $themeid;
                    // $themeName = $themename;
                }
                
                    $pageParams = array(
                        "asset" => array()
                    );

                $assets = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes/' . $themeID . '/assets.json', $pageParams);
                $text = array();
                $pointer = 0;
                // echo "<pre>";print_r($assets);exit;
                foreach($assets as $asset)
                {
                    $text[$pointer]= $asset["key"];
                    $pointer++;
                }
                return $text;
            } catch (ShopifyApiException $e) {
                $subject = 'getAssetData error';
                $error = array(
                    'name' => $subject,
                    'error' => $e->getMessage(),
                    'time' => time()
                );
                return false;
            }
        } catch (Exception $exc) {
            $subject = 'getAssetData error';
            $error = array(
                        'name' => $subject,
                        'error' => $exc->getMessage(),
                        'time' => time()
                    );
            return false;
        }
    }




    public function guzzleShopify($url,$method,$data,$token) {

        try
        {
            
            $client = new \GuzzleHttp\Client(['headers' => ['X-Shopify-Access-Token' => $token,'Content-Type' => 'application/json']]);
            $data['timeout'] = 180;
            $res = $client->request($method, $url ,$data);

            $headers = $res->getHeaders();


           /* $limit['HTTP_X_SHOPIFY_SHOP_API_CALL_LIMIT'] = $headers['HTTP_X_SHOPIFY_SHOP_API_CALL_LIMIT'];
            $limit['X-Shopify-Shop-Api-Call-Limit'] = $headers['X-Shopify-Shop-Api-Call-Limit'];
            log::info(json_encode($limit));*/
            // dd($headers);

            $response =  json_decode($res->getBody()->getContents(), true); dd($response);
     
            return (is_array($response) and (count($response) > 0)) ? array_shift($response) : $response;



        } catch (\GuzzleHttp\Ring\Exception\ConnectException $e) {
            print_r($e->getMessage());
             log::info(json_encode($e->getMessage()));
             return false;
        } 
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            print_r($e->getMessage());
             log::info(json_encode($e->getMessage()));
             return false;
        }catch (\GuzzleHttp\Exception\ServerException $e) {
            print_r($e->getMessage());
             log::info(json_encode($e->getMessage()));
             return false;
        }catch (Exception $e) {
            print_r($e->getMessage());
             log::info(json_encode($e->getMessage()));
             return false;
        }



    }

    public function orderguzzleShopify($url, $method, $data, $token)
    {

        // try
        // {
        $client = new \GuzzleHttp\Client(['headers' => ['X-Shopify-Access-Token' => $token, 'Content-Type' => 'application/json']]);
        $data['timeout'] = 180;
        $res = $client->request($method, $url, $data);

        $headers = $res->getHeaders();
        $response =  json_decode($res->getBody()->getContents(), true);

        $response;

        $data['response'] = (is_array($response) and (count($response) > 0)) ? array_shift($response) : $response;
        $data['header'] =  $headers;

        return $data;
    }



    public function getCustomCollections($search="") {
          
       try
       {
           try{       
                
                $params = array();
                $product = array();

                $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken,env('shopify_api_key'), env('shopify_secret'));
                
                $product = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/custom_collections.json?limit=250&title='.$search, $params);
                $total_count = count($product);
                $increment = 1;
                
                for($i = 0; $i < $increment; $i++){
                    
                    if($total_count >= 250 ){
                        $last_element = end($product);
                        $product_2 = array();
                        $sc_2 = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken,env('shopify_api_key'), env('shopify_secret'));
                        $product_2 = $sc_2->call('GET', '/admin/api/'.Config::get('api.version').'/custom_collections.json?limit=250&since_id='.$last_element['id'], $params);
                        
                        if(empty($product_2) ){
                            break;
                        }else{

                            $total_count = count($product_2);
                            $product = array_merge($product,$product_2);
                            $increment++;
                        
                        }
                    }else{
                        break;
                    }

                }
                

            } catch (ShopifyApiException $e) {                    
                    
                    return false;
            }
       }  catch (Exception $e){
            Log::info('getProductInfo() - '.$e->getMessage());         
           return false;
       }
        // return $product;
       return array_unique($product, SORT_REGULAR);
    }

    public function getSmartCollections($search="") {
          
        try
        {
            try{ 

                $params = array();
                $product = array();

                $sc = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken,env('shopify_api_key'), env('shopify_secret'));
                $product = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/smart_collections.json?limit=250&title='.$search, $params);

                $total_count = count($product);
                $increment = 1;

                for($i = 0; $i < $increment; $i++){

                    if($total_count >= 250 ){
                        $last_element = end($product);
                        $product_2 = array();
                        $sc_2 = new ShopifyClient( Auth::user()->shop, Auth::user()->accesstoken,env('shopify_api_key'), env('shopify_secret'));
                        $product_2 = $sc_2->call('GET', '/admin/api/'.Config::get('api.version').'/smart_collections.json?limit=250&since_id='.$last_element['id'], $params);
                        
                        if(empty($product_2) ){
                            break;
                        }else{

                            $total_count = count($product_2);
                            $product = array_merge($product,$product_2);
                            $increment++;
                        }
                    }else{
                        break;
                    }

                }
 
                
 
             } catch (ShopifyApiException $e) {                    
                     
                     return false;
             }
        }  catch (Exception $e){
             Log::info('getProductInfo() - '.$e->getMessage());         
            return false;
        }
           
        // return $product;
        return array_unique($product, SORT_REGULAR);
    }

    public function getusertheme( $shop, $token ,$userid){
        try{
            $sc = $this->setShopifyCredential( $shop, $token );
            $theme = $sc->call('GET', '/admin/api/'.Config::get('api.version').'/themes.json');
            // print_r($theme);exit;
            return $theme;

        }catch( Exception $e ){
            
            return false; 
        }
        
    }

    public function putShopifyProductMetafiled( $shop , $accesstoken ,$productid , $arrMetaFiled ){

        // try{
            // $params = array( 'metafield'=> $arrMetaFiled );
    
            $sc = $this->setShopifyCredential( $shop, $accesstoken );
            $metafields = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/products/'.$productid.'/metafields.json', $arrMetaFiled );
            // echo "<pre>";print_r($metafields);exit;
            //Log::info( 'metafields=='. json_encode( $metafields ) );
            return $metafields;
        // }catch( Exception $e ) {
        //     $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
        //     return false;
        // }
    
    }

    public function set_shopproduct_metafield( $shop , $accesstoken , $arrMetaFiled ){

        // try{
            // $params = array( 'metafield'=> $arrMetaFiled );
    
            $sc = $this->setShopifyCredential( $shop, $accesstoken );
            $metafields = $sc->call('POST', '/admin/api/'.Config::get('api.version').'/metafields.json', $arrMetaFiled );
            // echo "<pre>";print_r($metafields);exit;
            //Log::info( 'metafields=='. json_encode( $metafields ) );
            return $metafields;
        // }catch( Exception $e ) {
        //     $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
        //     return false;
        // }
    
    }

    public function deleteShopifyProductMetafiled( $shop , $accesstoken ,$noteonmetaid , $contentmetaid ){

        // try{
    
            $sc = $this->setShopifyCredential( $shop, $accesstoken );
            $metafields = $sc->call('DELETE', '/admin/api/'.Config::get('api.version').'/metafields/'.$noteonmetaid.'.json');

            $sc2 = $this->setShopifyCredential( $shop, $accesstoken );
            $metafields2 = $sc->call('DELETE', '/admin/api/'.Config::get('api.version').'/metafields/'.$contentmetaid.'.json');
            
            return $metafields2;
        // }catch( Exception $e ) {
        //     $error = method_exists($e, 'getResponse') ?  $e->getResponse() : '';
        //     return false;
        // }
    
    }


    public function updateProductCount($productid, $metafield_id, $userid =  null)
    {

        $productcount = OrderCount::where('productid', $productid)->first();
        $metafield = array();
        $myordercount = $productcount->ordercount;
        $recentcount = $productcount->recentcount;
        // $metafield['value'] = $productcount->ordercount;
        $metafield['key'] = 'myordercount';
        $metafield['value'] = '{"ordercount":'.$myordercount.',"recentcount":'.$recentcount.'}' ;
        $metafield['value_type'] = 'json_string';
        // $metafield['value_type'] = 'integer';

        $metafields[] = $metafield;

        if ($userid != '') {
            $user = User::find($userid);
        } else {
            $user = Auth::User();
        }



        $params['product']['metafields'] = $metafields;

        try {

            foreach ($params['product']['metafields'] as $metafield) {

                $metaparams['metafield'] = $metafield;
                $sc = $this->setShopifyCredential($user->shop, $user->accesstoken);
                $d = $sc->call('PUT', '/admin/api/'.Config::get('api.version').'/products/' . $productid . '/metafields/' . $metafield_id . '.json', $metaparams);
            }
        } catch (Exception $e) {

            $error = method_exists($e, 'getMessage') ?  $e->getMessage() . ' | line - ' . $e->getLine() : '';
            $error = method_exists($e, 'getResponse') ?  $e->getResponse() : $error;
            \Log::info('Error shop 1315:' . $error);
            return false;
        }
    }


    public function getProductsByFilter($params = array() , $query , $productid ) {
          
        try {

            if( !empty( $productid ) ){
                $productid = '/'.$productid;
            }
            $client = $this->getShopifyClient();
            $request = $client->request('GET', 'https://'.Auth::User()->shop.'/admin/api/'.Config::get('api.version').'/products'.$productid.'.json?limit=250&fields=id,images,title,handle');
                
            $response =  $request->getBody()->getContents();
            $response = json_decode($response,true);

           if( !empty( $response['products'] ) ){
            return $response['products'] ;
           }else{
             return array( 0 => $response['product'] );
           }

        } catch (\Exception $e) {
            log::info('Error '.$e->getMessage().'Line'.$e->getLine());
            return false;
        }
    }

    public function getProductsByFile2($params = array() , $query , $productid ) {
          
        try {

            
            $client = $this->getShopifyClient();
            $request = $client->request('GET', 'https://'.Auth::User()->shop.'/admin/api/'.Config::get('api.version').'/products'.$productid.'.json?limit=250&fields=id,images,title,handle'. $query, [
                            'form_params' => $params,
                    ]);

            $response =  $request->getBody()->getContents();
            $response = json_decode($response,true);

           if( !empty( $response['products'] ) ){
            return $response['products'] ;
           }else{
             return array( 0 => $response['product'] );
           }

        } catch (\Exception $e) {
            log::info('Error '.$e->getMessage().'Line'.$e->getLine());
            return false;
        }
    }


    public function getProductsCountByFilter($params = array() , $query ) {
          
        try {

            // print_r($query);exit;
            $client = $this->getShopifyClient();
            $request = $client->request('GET', 'https://'.Auth::User()->shop.'/admin/api/'.Config::get('api.version').'/products/count.json?'.$query);

            $response =  $request->getBody()->getContents();
            $response = json_decode($response,true);

            return $response['count'];

        } catch (\Exception $e) {
            log::info('Error '.$e->getMessage().'Line'.$e->getLine());
            return false;
        }
    }


    public function getShopifyClient($token = '')
    {
        try {
            if ($token == '') {
                $token =  Auth::User()->accesstoken;
            }


            $config['headers']['X-Shopify-Access-Token'] = $token;
            $config['defaults']['headers']['Content-Type'] = "application/json; charset=utf-8;";
            $client = new \GuzzleHttp\Client($config);
            return $client;

        } catch (Exception $e) {

        }
    }


    // public function getProductsByFilterIds($params = array() , $query , $productid ) {
          
    //     try {
            
    //         // if( !empty( $productid ) ){
    //         //     $productid = '/'.$productid;
    //         // }
    //         $client = $this->getShopifyClient();
    //         $request = $client->request('GET', 'https://'.Auth::User()->shop.'/admin/api/'.Config::get('api.version').'/products'.$productid.'.json?fields=id,images,title,handle'. $query, [
    //                         'form_params' => $params,
    //                 ]);

    //         $response =  $request->getBody()->getContents();
    //         $response = json_decode($response,true);

    //        if( !empty( $response['products'] ) ){
    //         return $response['products'] ;
    //        }else{
    //          return array( 0 => $response['product'] );
    //        }

    //     } catch (\Exception $e) {
    //         log::info('Error '.$e->getMessage().'Line'.$e->getLine());
    //         return false;
    //     }
    // }

    public function getProductsByFilterIds2($params = array() , $query='' , $productid ) {

        try { 
            
            $real_array = explode(",",$query);
            $count = count($real_array);
            if($count <= 250){
                $strQuery1 ='&ids='.$query;
            }else{
                
                $remain = $count - 250;
                $make_1 = array_slice($real_array, 0, 250);
                $old_query = implode(',',$make_1);
                $strQuery1 ='&ids='.$old_query;

                $make_2 = array_slice($real_array, 250, $remain);
                $new_query = implode(',',$make_2);
                $strQuery2 ='&ids='.$new_query;
            }
           
            $client = $this->getShopifyClient();
            $request = $client->request('GET', 'https://'.Auth::User()->shop.'/admin/api/'.Config::get('api.version').'/products'.$productid.'.json?limit=250&fields=id,images,title,handle'. $strQuery1, [
                            'form_params' => $params,
                    ]);
            $response =  $request->getBody()->getContents();
            $response = json_decode($response,true);
            

            if(isset($new_query)){

                $client2 = $this->getShopifyClient();
                $request2 = $client2->request('GET', 'https://'.Auth::User()->shop.'/admin/api/'.Config::get('api.version').'/products'.$productid.'.json?limit=250&fields=id,images,title,handle'. $strQuery2, [
                                'form_params' => $params,
                        ]);
                
                $response2 =  $request2->getBody()->getContents();
                $response2 = json_decode($response2,true);
            }

           if( !empty( $response['products'] ) ){
            
               if(!empty( $response2['products']) ){
                // dd(array_merge($response['products'],$response2['products']));
                return (array_merge($response['products'],$response2['products']) );

               }
            return $response['products'] ;
           }else{
             return array( 0 => $response['product'] );
           }

        } catch (\Exception $e) {
            log::info('Error '.$e->getMessage().'Line'.$e->getLine());
            return false;
        }
    }

    public function replyback( $msg )
    {
      if(headers_sent())
      {
        return false;
      }


      if(empty($_REQUEST)) {
        return false;
      }

      ignore_user_abort(true);
      set_time_limit(0);

      ob_start();
      echo json_encode( array( 'status'=> true ,'msg'=>$msg ) ); // send the response
      header('Connection: close');
      header('Content-type: application/json');
      header('Content-Length: '.ob_get_length());
      ob_end_flush();
      ob_flush();
      flush();
    }

    public function headerconnection()
    {
      ob_end_clean();
      header("Connection: close");
      ignore_user_abort(); // optional
      set_time_limit(0);
      ob_start();
    //   echo ('done');
      $size = ob_get_length();
      header("Content-Length: $size");
      ob_end_flush(); // Strange behaviour, will not work
      flush();            // Unless both are called !
      // Do processing here
    }


    public function zipredirect($url) {
        ob_start();
        header('Location: '.$url);
        ob_end_flush();
        die();
    }


}
