var canvas = new fabric.Canvas('c', {
  preserveObjectStacking: true
});

canvas.setWidth(400);
canvas.setHeight(400);

var canvasHeight = canvas.height;
var canvasWidth = canvas.width;
var center = canvas.getCenter();




canvas.loadFromJSON(json, function() {
  canvas.renderAll();
},function(o,object){
  object.on('mouseup', function(e) {
    // console.log( e.target );

    // activeObject = e.target;
    //  console.log( 'type'+ activeObject.type );
    // if( activeObject.type == 'i-text' ){
    //    //display text logic

    //    canvas.setActiveObject(canvas.item(2));
    // }
    // else if(activeObject.type == 'i-text'){
    //   //display image
    //    canvas.setActiveObject(canvas.item(0));
    // }else if(activeObject.type == 'rect'){
    //   //display image
    //    canvas.setActiveObject(canvas.item(1));
    // }
    // 
    getActiveObjectPm();
   });

   if(object.id != undefined) {
    if(object.id == 'backimg') {
      var current_width = object.width;

      var current_height = object.height;
      if(object.height < object.width ){
        scaleratio = canvasHeight/object.height;
      }else {
        scaleratio = canvasHeight/object.width;
      }
      backimg = object;
      backimg.set('scaleX', scaleratio);
      backimg.set('scaleY', scaleratio);
      backimg.set('top', 0);
      backimg.set('left', center.left);
      backimg.set('originX', 'center');
      backimg.set('originX', 'center');
      canvas.renderAll();
    }
  }
})









document.addEventListener('keydown', function(event) {
  const key = event.key; // const {key} = event; ES6+
  if (key === "Delete") {
    canvas.remove(canvas.getActiveObject());
  }
});


$('#jsonoutput').val(JSON.stringify(canvas.toDatalessJSON()));

function download(url,name){
  // make the link. set the href and download. emulate dom click
    $('<a>').attr({href:url,download:name})[0].click();
  }
  function downloadFabric(canvas,name,height=800,width=800){
  var resizedCanvas = document.createElement("canvas");
  var resizedContext = resizedCanvas.getContext("2d");

  
  resizedCanvas.height =height;
  resizedCanvas.width = width;
  

  var canvas = document.getElementById("c");
  var context = canvas.getContext("2d");

  resizedContext.drawImage(canvas, 0, 0, resizedCanvas.width, resizedCanvas.height);

    download(resizedCanvas.toDataURL(),name+'.png');
  }





  // /downloadFabric(canvas,'fb');
///test



// filters
(function() {

  // manually initialize 2 filter backend to give ability to switch:

  var $ = function(id){return document.getElementById(id)};

  function applyFilter(index, filter) {
    var obj = canvas.getActiveObject();
    obj.filters[index] = filter;
    obj.applyFilters();
    canvas.renderAll();
  }

  function getFilter(index) {
    var obj = canvas.getActiveObject();
    return obj.filters[index];
  }

  function applyFilterValue(index, prop, value) {

    var obj = canvas.getActiveObject();


    if (obj.filters[index]) {
      obj.filters[index][prop] = value;
      obj.applyFilters();


      canvas.renderAll();
    }
  }

  


      f = fabric.Image.filters;

  canvas.on({
    'object:selected': function() {
      fabric.util.toArray(document.getElementsByTagName('input'))
                          .forEach(function(el){ el.disabled = false; })

      var filters = ['grayscale', 'invert', 'remove-color', 'sepia', 'brownie',
                      'brightness', 'contrast', 'saturation', 'noise', 'vintage',
                      'pixelate', 'blur', 'sharpen', 'emboss', 'technicolor',
                      'polaroid', 'blend-color', 'gamma', 'kodachrome',
                      'blackwhite', 'blend-image', 'hue', 'resize'];

      for (var i = 0; i < filters.length; i++) {
        $(filters[i]) && (
        $(filters[i]).checked = !!canvas.getActiveObject().filters[i]);
      }
    },
    'selection:cleared': function() {
      fabric.util.toArray(document.getElementsByTagName('input'))
                          .forEach(function(el){ el.disabled = true; })
    }
  });


  var indexF;
  //return true;
  
  $('brownie').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Brownie());
  };
  $('vintage').onclick = function() {
     this.checked = true;
    applyFilter(1, this.checked && new f.Vintage());
  };
  $('technicolor').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Technicolor());
  };
  $('polaroid').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Polaroid());
  };
  $('kodachrome').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Kodachrome());
  };
  $('blackwhite').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.BlackWhite());
  };
  $('grayscale').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Grayscale());
  };


  $('sepia').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Sepia());
  };

  $('saturation').onclick = function () {
    this.checked = true;
    applyFilter(1, this.checked && new f.Saturation({
      saturation: parseFloat($('saturation-value').value)
    }));
  };
  $('saturation-value').oninput = function() {
    applyFilterValue(1, 'saturation', parseFloat(this.value));
  };
  $('noise').onclick = function () {
    this.checked = true;
    applyFilter(1, this.checked && new f.Noise({
      noise: parseInt($('noise-value').value, 10)
    }));
  };
  $('noise-value').oninput = function() {
    applyFilterValue(1, 'noise', parseInt(this.value, 10));
  };
  $('pixelate').onclick = function() {
    this.checked = true;
    applyFilter(1, this.checked && new f.Pixelate({
      blocksize: parseInt($('pixelate-value').value, 10)
    }));
  };
  $('pixelate-value').oninput = function() {
    applyFilterValue(1, 'blocksize', parseInt(this.value, 10));
  };
  $('blur').onclick = function() {


    applyFilter(1,  new f.Blur({
      value: parseFloat($('blur-value').value)
    }));
  };
  $('blur-value').oninput = function() {

    applyFilterValue(1, 'blur', parseFloat(this.value, 10));
  };
  $('sharpen').onclick = function() {
    this.checked = true;
    applyFilter(12, this.checked && new f.Convolute({
      matrix: [  0, -1,  0,
                -1,  5, -1,
                 0, -1,  0 ]
    }));
  };
  $('emboss').onclick = function() {
    this.checked = true;
    applyFilter(13, this.checked && new f.Convolute({
      matrix: [ 1,   1,  1,
                1, 0.7, -1,
               -1,  -1, -1 ]
    }));
  };

  $('nofilter').onclick = function() {

    var obj = canvas.getActiveObject();

    obj.filters = [];
    obj.applyFilters();
    canvas.renderAll();
  };




})();

$('.bannrtools').hide();

function getActiveObjectPm() {
  activeObj =  canvas.getActiveObject();
  console.log( activeObj.type );
$('.bannrtools').hide();
  if(activeObj.type == 'image') {
    $('#imageedittool').show();
    $('.filtervalue').hide();
    $('#bannrFilerOptions').show();
  }else if(activeObj.type == 'i-text') {
    $('.filters #textedittool').show();
  }else {
    $('#otheredittool').show();
  }

}




$( "#selectrectangle" ).click(function() {
 canvas.setActiveObject(canvas.item(0));
});

$( "#selectsecond" ).click(function() {
 canvas.setActiveObject(canvas.item(1));
});




// canvas.trigger('after:render', {target: 'text'});



var fontControl = $('#font-control');
$(document.body).on('change', '#font-control', function () {

    canvas.getActiveObject().set('fontFamily', this.value);//setFontFamily(this.value);
    canvas.renderAll();
    activeObj =  canvas.getActiveObject();

});


// fabric.Image.fromURL('https://media-public.canva.com/MADOPpCuYTo/2/0/thumbnail_large-1.jpg', function(oImg) {
//   canvas.add(oImg);
// });


$( "#fontTextColor" ).change(function() {
  canvas.getActiveObject().set('fill', this.value);
  canvas.renderAll();
});

$(document.body).on('click', '#text-bold', function () {
  activeObj =  canvas.getActiveObject();
  if(activeObj.fontWeight == 'bold') {
    fontWeight = 'normal';
  }else {
    fontWeight = 'bold';
  }
  canvas.getActiveObject().set('fontWeight', fontWeight);
  canvas.renderAll();
});


$(document.body).on('click', '#fontStyle', function () {
  activeObj =  canvas.getActiveObject();
  if(activeObj.fontStyle == 'normal') {
    fontStyle = 'italic';
  }else {
    fontStyle = 'normal';
  }
  canvas.getActiveObject().set('fontStyle', fontStyle);
  canvas.renderAll();
});


$(document.body).on('click', '#underline', function () {
  activeObj =  canvas.getActiveObject();
  if(activeObj.underline == true) {
    underline = false;
  }else {
    underline = true;
  }
  canvas.getActiveObject().set('underline', underline);
  canvas.renderAll();
});

$(document.body).on('click', '#textAlign', function () {
  activeObj =  canvas.getActiveObject();
  if(activeObj.textAlign == 'center') {
    textAlign = 'left';
  }else {
    textAlign = 'center';
  }
  canvas.getActiveObject().set('textAlign', textAlign);
  canvas.renderAll();
});

$(document.body).on('change', '#fontSizeRange', function () {
  canvas.getActiveObject().set('fontSize', this.value);
  canvas.renderAll();
  });


$(document.body).on('change', '#lineHeight', function () {
  canvas.getActiveObject().set('lineHeight', this.value);
  canvas.renderAll();
});

$(document.body).on('change', '#charSpacing', function () {
  canvas.getActiveObject().set('charSpacing', this.value);
  canvas.renderAll();
});


$(document.body).on('change', '.bannrotheroptionvalue', function () {
  var id = $(this).attr('id');

  if(id == 'opacity') {
    canvas.getActiveObject().set('opacity', this.value);
  }else if (id == 'shadow' || id == 'shadowblur' || id == 'shadowopacity') {
    var shadow = {
      color: 'rgba(0,0,0,0.6)',
      blur: $('#shadowblur').val(),
      offsetX:  this.value,
      offsetY:  this.value,
      fillShadow: true,
      strokeShadow: true
  }

    canvas.getActiveObject().set('shadow', shadow);
  }else if (id == 'borderradius') {
    canvas.getActiveObject().set('rx', this.value);
    canvas.getActiveObject().set('ry', this.value);
  }else if (id == 'bannrbgcolorvalue') {
    canvas.getActiveObject().set('fill', this.value);
    canvas.renderAll();
  }


  canvas.renderAll();
});


canvas.on("after:render", function (e , index ) {

  // canvas.setActiveObject(canvas.item(0));

  // console.log( canvas.item(0));
  // canvas.setActiveObject(canvas.item(0));
  // //canvas.requestRenderAll();
  // $('#imageedittool').show();
  // $('.filtervalue').hide();
  // $('#bannrFilerOptions').show();
  
});



// $( "#selectrectangle" ).click(function() {
//  canvas.setActiveObject(canvas.item(0));
// });

// $( ".upper-canvas" ).click(function() {
//  canvas.setActiveObject(canvas.item(1));
// });
