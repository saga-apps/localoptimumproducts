<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile', 'ProfileController@update')->name('profile.update');
Route::get('/logout', 'ProfileController@update')->name('profile.update');

Route::get('/about', function () {
    return view('about');
})->name('about');


Route::any('addorders', 'DashboardController@addorders');

Route::get('checkprimemeta', 'DashboardController@checkPrimeMeta');

// Route::any('adduserorders', 'DashboardController@ordersCron');

Route::any('userorderscron/{user}/{pageno}/{link}', 'DashboardController@userorderscron');

// Route::any('userorderscron/{user}/{pageno}', 'DashboardController@userorderscron');

Route::any('lastorderscron', 'DashboardController@lastorderscron');

Route::any('cleardatacron', 'DashboardController@cleardatacron');

// Route::get('/', function () {
// 	// return Redirect::to('https://apps.shopify.com/search?q=prime+product+badges&st_source=');
// 	return Redirect::to('https://apps.shopify.com/partners/instashop-tech');

// });

// Route::get('/',array('as'=>'login',function(){
//     return view('home');
// }));

Route::get('/', 'AdminController@getAdminLogin');

Route::get('/privacy','DashboardController@getprivacy');

Route::get('welcome/{locale}', function ($locale) {
    App::setLocale($locale);
	return view('connectrreview');
    //
});

// Route::get('/pullbulk', function () {
//     return view('dashboard.pullbulk');

// });



// shopify route
Route::get('shopifyappinstall', 'ShopifyController@sopifyInstallApp');
Route::post('customers/redact', 'ShopifyController@customersRedact');
Route::post('shop/redact', 'ShopifyController@shopRedact');

// webhooks routes

Route::post('shopify/webhooks/themes/publish', 'ShopifyController@getWebhookthemepublish');
Route::post('shopify/webhooks/app/uninstalled', 'ShopifyController@getWebhooktAppUninstalled');
// Route::post('shopify/webhooks/orders/paid', 'ShopifyController@orderPaidWebhook');

Route::group(['middleware' => ['auth', 'bannruser']], function () {

	//Products Home Page
	Route::get('/dashboard', 'DashboardController@home')->name('home');
	Route::get('/product', 'DashboardController@product')->name('product');

	Route::any('activateorders', 'DashboardController@ActivateOrderSync');
	Route::any('readwebhookorders', 'DashboardController@ReadWebhookOrder');

//Start of Notes
	Route::get('/createnotes','DashboardController@createNotes');
	Route::get('/note','DashboardController@getNotes');
	// Route::get('/note/{number}','DashboardController@getNotes');
	Route::get('editnote/{noteid}','DashboardController@createNotes');
	Route::post('noterule', 'NoteController@postNote');
	Route::post('checkproductid', 'NoteController@checkProductId');
	Route::post('checkeditproductid', 'NoteController@checkEditProductId');
	Route::post('deletenote', 'NoteController@deletenote');
	Route::post('activatenote', 'NoteController@postActivateNote');
	Route::post('updatenote-status', 'NoteController@updateNoteStatus');
	Route::post('stylenote', 'NoteController@styleNote');
	Route::post('/sendintegratemailnotes','DashboardController@sendintegratemailNotes');
//End of Notes

// Start of Banners

	Route::get('/banner','DashboardController@getBanners');
	Route::get('/createbanners','DashboardController@createBanners');
	Route::post('/copybanner','BannerController@copyBanners');
	Route::get('/editbanner/{id}','DashboardController@createBanners');
	Route::post('/bannerrule', 'BannerController@postBannerRule');
	Route::post('deletebanner', 'BannerController@deleteBanner');
	Route::post('updatebanner-group', 'BannerController@updateBannerGroup');
	Route::post('activatebanner', 'BannerController@postActivateBanner');
	Route::post('updatebanner-status', 'BannerController@updateBannerStatus');
	Route::post('updatebanner-priority', 'BannerController@updateBannerPriority');
	Route::post('/sendintegratemailbanners','DashboardController@sendintegratemailBanners');

// end of Banner


//Start of Highlights

	Route::get('/showcasebadge', 'DashboardController@Showcasesbadge');
	Route::get('/showcasehighlight', 'DashboardController@Showcaseshighlight');

	Route::get('/highlights','DashboardController@getHighlights');
	Route::get('/createhighlights','DashboardController@createHighlights');
	Route::post('deletehighlight', 'HighlightController@deleteHighlight');
	Route::post('copyhighlight', 'HighlightController@copyHighlight');
	Route::post('activatehighlight', 'HighlightController@postActivateHighlight');
	Route::post('updatehighlight-status', 'HighlightController@updateHighlightStatus');
	Route::post('highlightrule', 'HighlightController@postHighlight');
	Route::get('highlightupdate-liquid', 'HighlightController@updateShopifyLiquidContent');
	Route::post('stylehighlight', 'HighlightController@styleHighlight');
	Route::post('searchicons', 'HighlightController@searchiconresult');

	Route::get('upgradeplansuccess', 'ShopifyController@getUpgradeplansuccess');
	Route::post('updatehighlight-priority', 'HighlightController@updateHighlightPriority');

	// Route::get('/createhighlights','DashboardController@createHighlights');
	Route::get('edithighlight/{highlightid}','DashboardController@createHighlights');
	Route::get('/helphighlight','HighlightController@gethelphighlight');

	Route::post('/sendintegratemail','DashboardController@sendintegratemail');

	Route::post('/sendintegratemailhighlights','DashboardController@sendintegratemailhighlights');

// End of Highlights

	Route::post('updatecharge', 'ShopifyController@getUpdatecharge');

	Route::get('/help','DashboardController@gethelp');
	// Route::get('/dashboard','DashboardController@getBadges');
	

	Route::get('/dashboard/{number}','DashboardController@getBadges');

  	Route::get('/dashboard1', 'DashboardController@getDescription');
	Route::get('/createbadges','DashboardController@createBadges');
	Route::get('/createhighlight','DashboardController@createHighlights');

  	Route::get('/createdescription','DashboardController@createDescription');

  	Route::get('logout', 'DashboardController@getLogout');

	Route::get('account', 'AccountController@getAccount');
	Route::get('plans', 'AccountController@getPlans');

	Route::post('selectedtheme', 'AccountController@selectedtheme');

	Route::post('badgerule', 'BadgeController@postBadgeRule');
	Route::get('editbadge/{badgeid}', 'DashboardController@createBadges');

	Route::post('deletebadge', 'BadgeController@deleteBadge');
	Route::post('copybadge', 'BadgeController@copyBadge');

	Route::post('reviewfeedback', 'BadgeController@reviewfeedback');

	Route::post('updatereviewdate', 'BadgeController@updatereviewdate');

	Route::get('badgeupdate-liquid', 'BadgeController@updateShopifyLiquidContent');

	Route::post('updatebadge-priority', 'BadgeController@updateBadgePriority');

	Route::get('shopify-orderproducts', 'DashboardController@getShopifyOrderProduct');

	Route::get('shopify-exproducts', 'DashboardController@getShopifyExProduct');

	Route::get('shopify-products', 'DashboardController@getShopifyProduct');

	Route::get('shopify-collections', 'DashboardController@getShopifyCollections');

	Route::post('csvimport', 'DashboardController@csvimport');

	Route::post('updatebadge-group', 'BadgeController@updateBadgeGroup');

	Route::post('updatehighlight-group', 'HighlightController@updateHighlightGroup');

	Route::post('updatebadge-status', 'BadgeController@updateBadgeStatus');
	Route::post('account', 'AccountController@postAccount');

	Route::post('activatebadge', 'BadgeController@postActivateBadge');

	Route::get('settings-order-count', 'BadgeController@OrderSetting');

	Route::get('settings-livevisitcount', 'BadgeController@LiveVisitCountSetting');

	Route::get('settings-delivery-date', 'BadgeController@DeliveryDateSetting');

	Route::get('ordersettings', 'DashboardController@OrderSettings');

	Route::post('saveproductedit', 'DashboardController@saveProductEdit');

	Route::get('updateuserfield', 'DashboardController@UpdateUserField');

	// Route::any('userorders', 'DashboardController@cronuserOrders');
	Route::any('userorders/{days}', 'DashboardController@cronuserOrders');

	Route::any('livevisitcount/{time}/{limit}', 'DashboardController@setlivecounttime');

	Route::any('deliverydatesetting', 'DashboardController@deliverydatesetting');



});

//admin

Route::get('admin', 'AdminController@getAdminLogin');
Route::post('admin', 'AdminController@postAdminLogin');

Route::post('admin/updateuser', 'AdminController@updateUser');

Route::group(['middleware' => ['admin']], function () {

	Route::get('updatereview/{userid}/{isreview}', 'AdminController@updateReview');
	Route::get('updateintegration/{userid}/{isintegrated}', 'AdminController@updateIntegration');
	Route::get('updateintegrationhighlight/{userid}/{isintegrated}', 'AdminController@updateIntegrationhighlight');
	Route::get('updateintegrationnote/{userid}/{isintegrated}', 'AdminController@updateIntegrationNote');
	Route::get('updateintegrationbanner/{userid}/{isintegrated}', 'AdminController@updateIntegrationBanner');

	Route::get('autologin/{userid}', 'AdminController@getAutoLogin');
	Route::get('autologinhighlight/{userid}', 'AdminController@getautologinhighlight');
	Route::get('autologinbadge/{userid}', 'AdminController@getautologinbadge');

	Route::post('changeplan', 'AdminController@changeuserplan');
	Route::any('downloadtheme/{themeid}/{themeName}/{userid}', 'AdminController@downloadTheme');

	Route::prefix('admin')->group(function () {

		Route::get('/dashboard', 'AdminController@getUserList');
		Route::get('/buserslist', 'AdminController@getUserList');
		Route::get('/plans', 'AdminController@getPlans');
		Route::get('/add-plan', 'AdminController@getAddPlan');
		Route::post('/add-plan', 'AdminController@postAddPlan');
		Route::get('/editplan/{planid}', 'AdminController@getEditPlan');
		Route::post('/editplan/', 'AdminController@postEditPlan');
		Route::get('/analyzetheme', 'AdminController@getAnalyzetheme');
		Route::post('/analyzetheme', 'AdminController@postAnalyzetheme');
		Route::post('/getusertheme', 'AdminController@getusertheme');
        Route::post('/getthemedata', 'AdminController@postGetthemedata');
		Route::post('/putnewdata', 'AdminController@putnewdata');

		Route::post('/addshowcase', 'AdminController@addshowcase');
		Route::post('/updateshowcase-status', 'AdminController@updateShowcaseStatus');
		Route::post('/deleteshowcase', 'AdminController@deleteShowcase');
		Route::get('/showcaselist/badge', 'AdminController@getShowcasesbadge');
		Route::get('/showcaselist/highlight', 'AdminController@getShowcaseshighlight');
		Route::post('updateshowcase-priority', 'AdminController@updateShowcasePriority');


		Route::get('/highlightslist', 'AdminController@getHighlights');
		Route::get('/badgeslist', 'AdminController@getBadges');

		Route::get('/userreviewlist', 'AdminController@getuserreviews');

		Route::get('/history', 'AdminController@getAdminHistory');
		Route::get('/getDiffCode', 'AdminController@getAdminlog');


  //       Route::get('cronstatus', 'AdminController@getCronStatus');
  //       Route::get('awscredentials', 'AdminController@getAwscredentials');
  //       Route::get('reviewstatus', 'AdminController@getReviewStatus');
		// Route::get('logout', 'AdminController@getAdminLogout');

		// Route::get('reviewpreview', 'AdminController@getReviewPreview');

		// Route::get('runningtasks', 'AdminController@runningTasks');

		// Route::get('pulltaskaction/{userid}/{taskid}', 'AdminController@pullTaskaction');


		// Route::post('updatestore', 'AdminController@updateStore');
		// Route::get('reviewcssversion/{userid}', 'AdminController@reviewCssVersion');
		// Route::get('downloaduserreport/{userid}', 'AdminController@downloadUserReport');



		// Route::get('addbannrtype', 'BannrController@getAddType');
		// Route::post('addbannrtype', 'BannrController@postAddType');

	});

});